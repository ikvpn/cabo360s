-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2015 at 07:38 AM
-- Server version: 5.6.13
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kebabuno_visitprocida`
--
CREATE DATABASE IF NOT EXISTS `kebabuno_visitprocida` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `kebabuno_visitprocida`;

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `type` varchar(10) NOT NULL,
  `src` text NOT NULL,
  `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `name`, `type`, `src`, `cdate`) VALUES
(1, '970X90', 'image', 'aHR0cDovL2xvY2FsaG9zdDo4MDgwL3Zpc2l0cHJvY2lkYS91cGxvYWRzL2Fkcy9hZGQ5NzBYOTAuanBn', '2015-04-10 20:25:33'),
(2, '300X250', 'image', 'aHR0cDovL3BsYWNlaG9sZC5pdC8zMDB4MjUwJnRleHQ9MzAwK3grMjUw', '2015-04-10 20:25:33'),
(3, '468X15', 'image', 'aHR0cDovL3BsYWNlaG9sZC5pdC80Njh4MTUmdGV4dD1hZHMrLys0NjgreDE1', '2015-04-10 20:26:09'),
(4, '468X60', 'image', 'aHR0cDovL3BsYWNlaG9sZC5pdC80Njh4NjAmdGV4dD1hZHMrLys0NjgreCs2MA==', '2015-04-11 09:30:28'),
(5, '728X90', 'image', 'aHR0cDovL3BsYWNlaG9sZC5pdC83Mjh4OTAmdGV4dD1hZHMrLys3MjgreCs5MA==', '2015-05-23 22:14:23'),
(6, '234X60', 'image', 'aHR0cDovL3BsYWNlaG9sZC5pdC8yMzR4NjAmdGV4dD1hZHMrLysyMzQreCs2MA==', '2015-05-23 22:14:23');

-- --------------------------------------------------------

--
-- Table structure for table `apartments`
--

CREATE TABLE IF NOT EXISTS `apartments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enName` varchar(100) NOT NULL,
  `itName` varchar(100) NOT NULL,
  `duName` varchar(100) NOT NULL,
  `enDescription` text NOT NULL,
  `itDescription` text NOT NULL,
  `duDescription` text NOT NULL,
  `minPrice` int(11) NOT NULL,
  `maxPrice` int(11) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `photo` varchar(256) NOT NULL,
  `rooms` int(11) NOT NULL,
  `beds` int(11) NOT NULL,
  `pax` int(11) NOT NULL,
  `trypology` varchar(100) NOT NULL,
  `bathrooms` int(11) NOT NULL,
  `terrace` int(11) NOT NULL,
  `rates` varchar(1000) NOT NULL,
  `order_id` int(11) NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bars_cafe`
--

CREATE TABLE IF NOT EXISTS `bars_cafe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enName` varchar(100) NOT NULL,
  `itName` varchar(100) NOT NULL,
  `duName` varchar(100) NOT NULL,
  `enSlogan` varchar(200) NOT NULL,
  `itSlogan` varchar(200) NOT NULL,
  `duSlogan` varchar(200) NOT NULL,
  `enDescription` text NOT NULL,
  `itDescription` text NOT NULL,
  `duDescription` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `website` varchar(200) NOT NULL,
  `min_price` int(11) NOT NULL,
  `max_price` int(11) NOT NULL,
  `cuisine` varchar(200) NOT NULL,
  `creditCards` varchar(100) NOT NULL,
  `rating` int(11) NOT NULL,
  `hours` varchar(100) NOT NULL,
  `closingDay` varchar(30) NOT NULL,
  `Info` varchar(100) NOT NULL,
  `order_id` int(11) NOT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `photo` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `beaches`
--

CREATE TABLE IF NOT EXISTS `beaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `p_thumbnail` varchar(250) NOT NULL,
  `p_cover` varchar(250) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `beaches_t`
--

CREATE TABLE IF NOT EXISTS `beaches_t` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `beach_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `meta_tag` varchar(250) DEFAULT NULL,
  `meta_desc` varchar(1000) DEFAULT NULL,
  `lang` varchar(3) NOT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `y_charges` int(11) NOT NULL,
  `m_charges` int(11) NOT NULL,
  `payment_plans` varchar(10) NOT NULL,
  `table` varchar(30) NOT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `type`, `icon`, `y_charges`, `m_charges`, `payment_plans`, `table`, `cDate`) VALUES
(1, 'Hotel', 'where_to_stay', 'fa-building', 2000, 1000, '1,2', 'hotels', '2015-05-01 02:05:36'),
(2, 'Resturant', 'eat_and_drink', 'fa-cutlery', 15000, 1200, '1,2', 'resturants', '2015-05-01 02:05:36'),
(4, 'Shop', 'what_to_do', 'fa-shopping-cart', 20000, 200, '1,2', 'shopes', '2015-05-01 02:05:36'),
(5, 'Holiday House', 'where_to_stay', 'fa-home', 2000, 300, '1,2', 'apartments', '2015-05-09 18:18:34'),
(6, 'Event', 'event', 'fa-calendar', 5000, 500, '1,2', 'events', '2015-05-21 05:39:28'),
(7, 'Bars and Cafe', 'eat_and_drink', 'fa-coffee', 500, 100, '1,2', 'bars_cafe', '2015-09-09 07:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('1cbed26d655cf545780c807bcd3ccd5d', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36', 1441783784, ''),
('bb2061816837297df53badbef95e1d60', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36', 1441742605, '');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `billingAddress1` text NOT NULL,
  `billingAddress2` text NOT NULL,
  `billingCity` varchar(150) NOT NULL,
  `billingPostcode` int(11) NOT NULL,
  `billingState` varchar(100) NOT NULL,
  `billingCountry` varchar(100) NOT NULL,
  `billingPhone` varchar(20) NOT NULL,
  `company` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `firstName`, `lastName`, `email`, `password`, `billingAddress1`, `billingAddress2`, `billingCity`, `billingPostcode`, `billingState`, `billingCountry`, `billingPhone`, `company`, `status`, `cDate`) VALUES
(1, 'Tester1', 'Tester1', 'innamhunzai@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'Address line 1', 'Addressline 2', 'Islamabad', 44000, 'Fedral Capital', 'Pakistan', '345345', 'Gamun', 'new', '2015-04-27 15:21:11');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enTitle` varchar(250) NOT NULL,
  `duTitle` varchar(250) NOT NULL,
  `itTitle` varchar(250) NOT NULL,
  `enDescription` text NOT NULL,
  `duDescription` text NOT NULL,
  `itDescription` text NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `reservation` varchar(20) NOT NULL,
  `pAdult` int(11) NOT NULL,
  `pChild` int(11) NOT NULL,
  `pGuest` int(11) NOT NULL,
  `web` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `mob` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `cover` varchar(250) NOT NULL,
  `category` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE IF NOT EXISTS `hotels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enName` varchar(150) NOT NULL,
  `itName` varchar(150) NOT NULL,
  `duName` varchar(150) NOT NULL,
  `enDescription` text NOT NULL,
  `itDescription` text NOT NULL,
  `duDescription` text NOT NULL,
  `photo` varchar(250) NOT NULL,
  `price` int(11) NOT NULL,
  `unit` varchar(5) NOT NULL,
  `rating` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `isIntrested` int(11) NOT NULL DEFAULT '1',
  `p_image` varchar(250) NOT NULL,
  `p_cover` varchar(250) NOT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations_t`
--

CREATE TABLE IF NOT EXISTS `locations_t` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `meta_tag` varchar(250) DEFAULT NULL,
  `meta_desc` varchar(1000) DEFAULT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lang` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `category`, `link`) VALUES
(1, 'what_to_visit', ''),
(2, 'where_to_stay', ''),
(3, 'where_to_stay', ''),
(4, 'where_to_stay', ''),
(5, 'eat_and_drink', ''),
(6, 'eat_and_drink', ''),
(7, 'eat_and_drink', ''),
(8, 'what_to_do', ''),
(9, 'what_to_do', ''),
(10, 'what_to_do', ''),
(11, 'what_to_do', ''),
(12, 'what_to_do', ''),
(13, 'events', ''),
(14, 'events', ''),
(15, 'useful_info', ''),
(16, 'useful_info', ''),
(17, 'useful_info', ''),
(18, 'where_to_visit', ''),
(19, 'where_to_visit', '');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items_t`
--

CREATE TABLE IF NOT EXISTS `menu_items_t` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `menu_items_t`
--

INSERT INTO `menu_items_t` (`id`, `name`, `lang`, `menu_id`) VALUES
(1, 'Map of the Island', 'en', 1),
(2, 'Map of the Island', 'it', 1),
(3, 'Hotels', 'en', 2),
(4, 'Holiday Houses', 'en', 3),
(5, 'Special Offers', 'en', 4),
(6, 'Resturants', 'en', 5),
(7, 'Bars and Cafe', 'en', 6),
(8, 'Nightlife', 'en', 7),
(9, 'Shopping', 'en', 8),
(10, 'Boat rentals', 'en', 9),
(11, 'Watersports', 'en', 10),
(12, 'Beach clubs', 'en', 11),
(13, 'Wedding', 'en', 12),
(14, 'Traditional Events', 'en', 13),
(15, 'Upcoming Events', 'en', 14),
(16, 'How to get there', 'en', 15),
(17, 'Ferry time table', 'en', 16),
(18, 'Getting around Procida', 'en', 17),
(19, 'Case vacanza', 'it', 3),
(20, 'vakantieappartementen', 'du', 3),
(21, 'Hotels', 'du', 2),
(22, 'Hotels', 'it', 2),
(23, 'speciale aanbiedingen', 'du', 4),
(24, 'offerte speciali', 'it', 4),
(25, 'Ristoaranti', 'it', 5),
(26, 'restaurants', 'du', 5),
(27, 'bars en cafe', 'du', 6),
(28, 'bar e caffè', 'it', 6),
(29, 'vita notturna', 'it', 7),
(30, 'nachtleven', 'du', 7),
(31, 'het winkelen', 'du', 8),
(32, 'shopping', 'it', 8),
(33, 'noleggio barche', 'it', 9),
(34, 'bootverhuur', 'du', 9),
(35, 'sport acquatici', 'it', 10),
(36, 'watersport', 'du', 10),
(37, 'beachclubs', 'du', 11),
(38, 'beach club', 'it', 11),
(39, 'bruiloft', 'du', 12),
(40, 'matrimonio', 'it', 12),
(41, 'traditionele evenementen', 'du', 13),
(42, 'manifestazioni tradizionali', 'it', 13),
(43, 'eventi upcomming', 'it', 14),
(44, 'aankomende events', 'du', 14),
(45, 'hoe daar te komen', 'du', 15),
(46, 'come arrivarci', 'it', 15),
(47, 'Ferry orario', 'it', 16),
(48, 'Ferry tijdschema', 'du', 16),
(49, 'krijgen rond procida', 'du', 17),
(50, 'muoversi procida', 'it', 17),
(51, 'test5', 'en', 19),
(52, 'test5', 'it', 19),
(53, 'test5', 'du', 19);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `location` int(11) NOT NULL,
  `address` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `eDate` datetime DEFAULT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_plan` varchar(20) NOT NULL,
  `payment_type` varchar(20) NOT NULL,
  `agreement_amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `placesofinterest`
--

CREATE TABLE IF NOT EXISTS `placesofinterest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enTitle` varchar(100) NOT NULL,
  `itTitle` varchar(100) NOT NULL,
  `duTitle` varchar(100) NOT NULL,
  `enDescription` text NOT NULL,
  `itDescription` text NOT NULL,
  `duDescription` text NOT NULL,
  `meta_tag_en` varchar(250) DEFAULT NULL,
  `meta_tag_du` varchar(250) DEFAULT NULL,
  `meta_tag_it` varchar(250) DEFAULT NULL,
  `meta_desc_en` varchar(1000) DEFAULT NULL,
  `meta_desc_du` varchar(1000) DEFAULT NULL,
  `meta_desc_it` varchar(1000) DEFAULT NULL,
  `location_id` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `web` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `timing` varchar(100) NOT NULL,
  `openingDays` varchar(100) NOT NULL,
  `closingDays` varchar(100) NOT NULL,
  `reservation` int(11) NOT NULL,
  `pAdults` int(11) NOT NULL,
  `pGuests` int(11) NOT NULL,
  `pChilds` int(11) NOT NULL,
  `photo` varchar(256) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `p_image` varchar(500) NOT NULL,
  `status` varchar(15) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `posts_t`
--

CREATE TABLE IF NOT EXISTS `posts_t` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `post_id` int(11) NOT NULL,
  `lang` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tags` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `resturants`
--

CREATE TABLE IF NOT EXISTS `resturants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enName` varchar(100) NOT NULL,
  `itName` varchar(100) NOT NULL,
  `duName` varchar(100) NOT NULL,
  `enSlogan` varchar(200) NOT NULL,
  `itSlogan` varchar(200) NOT NULL,
  `duSlogan` varchar(200) NOT NULL,
  `enDescription` text NOT NULL,
  `itDescription` text NOT NULL,
  `duDescription` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `website` varchar(200) NOT NULL,
  `min_price` int(11) NOT NULL,
  `max_price` int(11) NOT NULL,
  `cuisine` varchar(200) NOT NULL,
  `creditCards` varchar(100) NOT NULL,
  `rating` int(11) NOT NULL,
  `hours` varchar(100) NOT NULL,
  `closingDay` varchar(30) NOT NULL,
  `Info` varchar(100) NOT NULL,
  `order_id` int(11) NOT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `photo` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `api_username` varchar(100) NOT NULL,
  `api_password` varchar(100) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `offline_payment` int(11) NOT NULL,
  `instagram` varchar(1000) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `api_username`, `api_password`, `api_key`, `offline_payment`, `instagram`, `date`) VALUES
(1, 'sdk-three_api1.sdk.com', 'QFZCWN5HZM8VBG7Q', 'A-IzJhZZjhg29XQ2qnhapuwxIDzyAZQ92FRP5dqBzVesOkzbdUONzmOU', 1, '<!-- SnapWidget -->\n<script src="http://snapwidget.com/js/snapwidget.js"></script>\n<iframe src="http://snapwidget.com/in/?u=amFtYWxqYW1hbG98aW58MTI1fDV8NHx8eWVzfDV8ZmFkZU91dHxvblN0YXJ0fHllc3x5ZXM=&ve=160515" title="Instagram Widget" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%;"></iframe>', '2015-04-15 19:37:56');

-- --------------------------------------------------------

--
-- Table structure for table `shopes`
--

CREATE TABLE IF NOT EXISTS `shopes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enName` varchar(100) NOT NULL,
  `duName` varchar(100) NOT NULL,
  `itName` varchar(100) NOT NULL,
  `enDescription` text NOT NULL,
  `duDescription` text NOT NULL,
  `itDescription` text NOT NULL,
  `photo` varchar(250) NOT NULL,
  `category` varchar(100) NOT NULL,
  `minPrice` int(11) NOT NULL,
  `maxPrice` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `usertype` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `full_name`, `usertype`) VALUES
(1, 'innamhunzai@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'Innam Ullah Baig', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
