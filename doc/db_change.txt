05/06/2019 by VR
=================================================================================================================================
CREATE TABLE `easyisland`.`transfer` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `enTitle` VARCHAR(255) NOT NULL , `itTitle` VARCHAR(255) NOT NULL , `categories` VARCHAR(255) NOT NULL , `dateStart` DATE NOT NULL , `dateEnd` DATE NOT NULL , `daysAvailability` VARCHAR(255) NOT NULL , `duration` VARCHAR(255) NOT NULL , `enDescription` TEXT NOT NULL , `itDescription` TEXT NOT NULL , `itServicesIncluded` TEXT NOT NULL , `enServicesIncluded` TEXT NOT NULL , `itServicesNotIncluded` TEXT NOT NULL , `enServicesNotIncluded` TEXT NOT NULL , `itPolicy` TEXT NOT NULL , `enPolicy` TEXT NOT NULL , `customerId` INT(11) NOT NULL , `thumbnail` VARCHAR(255) NOT NULL , `cover` VARCHAR(255) NOT NULL , `slug` VARCHAR(255) NOT NULL , `businessPageId` INT(11) NOT NULL , `orderId` INT(11) NOT NULL , `transfer_type` VARCHAR(255) NOT NULL , `boat_type` VARCHAR(255) NOT NULL , `trip_type` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = MyISAM;




06/06/2019 by smj
=================================================================================================================================
ALTER TABLE `transfer` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `customerId` `customerId` INT(11) NULL DEFAULT NULL, CHANGE `thumbnail` `thumbnail` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cover` `cover` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `slug` `slug` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `businessPageId` `businessPageId` INT(11) NULL DEFAULT NULL, CHANGE `orderId` `orderId` INT(11) NULL DEFAULT NULL, CHANGE `transfer_type` `car_type` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `transfer` CHANGE `businessPageId` `businessPageId` INT(11) NULL;

ALTER TABLE `transfer` ADD `pickup` VARCHAR(255) NOT NULL AFTER `trip_type`, ADD `dropoff` VARCHAR(255) NOT NULL AFTER `pickup`, ADD `maxPassenger` INT(20) NOT NULL AFTER `dropoff`;

10/06/2019 by smj
=================================================================================================================================
ALTER TABLE `transfer` ADD `transfer_type` VARCHAR(255) NOT NULL AFTER `trip_type`;

10/06/2019 by smj
=================================================================================================================================
ALTER TABLE `transfer` ADD `startPrice` INT(20) NOT NULL AFTER `orderId`;
