(function($){
    "use strict";

	$(document).ready(function(){
	    jQuery('.tp-banner').revolution({
			delay:5000,
			startwidth:1170,
			startheight:600,
			hideThumbs:10,


			parallax:"scroll",
			parallaxBgFreeze:"off",
			parallaxLevels:[10,7,4,3,2,5,4,3,2,1],

			fullWidth:"off",
			fullScreen:"off",
			fullScreenOffsetContainer: "",
			fullScreenAlignForce:"off",

			navigationType:"off",							
			navigationStyle:"preview1",

			hideArrowsOnMobile:"on",
			
			touchenabled:"on",
			onHoverStop:"off",
			spinner:"spinner4"
		});
	});
	
})(jQuery);