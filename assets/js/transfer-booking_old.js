/*
* Author: Davide Polano
* Email: info@mdslab.org
* Project: Visit Procida
* Version: 1.0
*/

/*
	@param: information
	@info : global object where we store all the booking information
*/

var information;
var returnTrip = false;

/* When user increment or decrement people \ children value we go to Update prices in the view  */

$('.inc').on('click', function() {
	$("#adultsReturn").val($("#adults").val());
	$("#childrenReturn").val($("#children").val());
	if ($(this).prev( "#infants" ).val() > 1) {
		$( "#infants" ).val(1);
	}
	if ($(this).prev( "#pets" ).val() > 1) {
		$( "#pets" ).val(1);
	}
	appendPrice();
});

$('.dec').on('click', function() {
	appendPrice();
});
$("#address, #address_return_2").change(function() {
		appendPrice();
})
$('input[type=radio][name=transfer_type]').change(function() {
	var transfer_type = $(this).val()
	if (transfer_type == 'yacht_transfer') {
		$("#for_car_transfer").hide();
		$("#for_yacht_transfer").show();
	}
	if (transfer_type == 'car_transfer') {
		$("#for_car_transfer").show();
		$("#for_yacht_transfer").hide();
	}
	appendPrice();
});

$('.date-pick').datepicker()
    .on('changeDate', function(e) {
			appendPrice();
    });

/* Scroll down when user click on the optional return */
$('.btn_collapse').on('click', function() {
	$('html, body').animate({
		scrollTop: $('#address').offset().top
	}, 'slow');
	if (returnTrip) {
		returnTrip = false;
	}else{
		returnTrip = true;
	}
	appendPrice();
});

/* get booking date information */
function getBookingDate() {
	var date = $('#date').val();
	var time = $('#time').val();

	var returnDate = $('#returnDate').val();
	var returnTime = $('#returnTime').val();

	var startDateTime = date+' '+time;
	var returnDateTime = returnDate+' '+returnTime;

	if(returnDateTime != " ") {
		var bookingInfo =  { startDateTime : startDateTime, returnDateTime : returnDateTime };
		return bookingInfo;
	} else {
		var bookingInfo =  { startDateTime : startDateTime };
		return bookingInfo;
	}
}

/* Get the price foreach people */
function getPrice(people) {
	var price = 0;
	switch(people) {
		case 0:
			price = '0';
			break;
	    case 1:
	        price = '53';
	        break;
	    case 2:
	        price = '45';
	        break;
	    case 3:
	        price = '42';
	        break;
	    case 4:
	        price = '40';
	        break;
	    default:
	        price = '40';
	        break;
	}

	return price;
}

// for (CIA) Rome Airport Ciampino & (FCO) Rome Airport Fiuminico
function getPriceForRome(people,transfer_type) {
	var price = 0;
	switch (transfer_type) {
		case 'car_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '360';
							break;
					case 2:
							price = '190';
							break;
					case 3:
							price = '140';
							break;
					case 4:
							price = '120';
							break;
					case 5:
							price = '110';
							break;
					case 6:
							price = '95';
							break;
					default:
							price = '95';
							break;
			}
			break;
	
		case 'yacht_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '1200';
							break;
					case 2:
							price = '600';
							break;
					case 3:
							price = '400';
							break;
					case 4:
							price = '300';
							break;
					case 5:
							price = '240';
							break;
					case 6:
							price = '200';
							break;
					default:
							price = '200';
							break;
			}
			break;
		
	}
	

	return price;
}
//  for (NAP) Naples International Airport & Napoli Centrale railway station
function getPriceForNapoli(people,transfer_type) {
	var price = 0;
	switch (transfer_type) {
		case 'car_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '65';
							break;
					case 2:
							price = '45';
							break;
					case 3:
							price = '43';
							break;
					case 4:
							price = '39';
							break;
					case 5:
							price = '35';
							break;
					case 6:
							price = '35';
							break;
					default:
							price = '35';
							break;
			}
			break;
	
		case 'yacht_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '900';
							break;
					case 2:
							price = '450';
							break;
					case 3:
							price = '300';
							break;
					case 4:
							price = '225';
							break;
					case 5:
							price = '180';
							break;
					case 6:
							price = '150';
							break;
					default:
							price = '150';
							break;
			}
			break;
		
	}

	return price;
}

/* Calculate the price, store the new info in the global var information and return bookingData */
function calculatePrice() {
	var adults = $('#adults').val();
	var children = $('#children').val();

	var adultsReturn = $('#adults').val();
	var childrenReturn = $('#children').val();

	var transfer_type =  $("input[name='transfer_type']:checked").val();

	var address = $('#address').val();
	var address_return = $('#address_return_2').val();

	var people = 0,
		price = 0,
		returnPeople = 0,
		returnPrice = 0;

	if(adults != " "){
		 people = parseInt(adults);
		 if (address == 1 || address == 2) {
			 price = getPriceForRome(people,transfer_type) * people;
				if(children != " ") {
					people = people + parseInt(children);
					price = (getPriceForRome(people,transfer_type));
				}
		 }else if (address == 3 || address == 4) {
				price = getPriceForNapoli(people,transfer_type) * people;
				if(children != " ") {
					people = people + parseInt(children);
					price = (getPriceForNapoli(people,transfer_type));
				}
		 }
		
	}

	if(returnTrip) {
		returnPeople = parseInt(adultsReturn);
		// returnPrice = getPrice(returnPeople);
		if (address_return == 1 || address_return == 2) {
			returnPrice = getPriceForRome(returnPeople,transfer_type);
			 if(childrenReturn != " ") {
				returnPeople = returnPeople + parseInt(childrenReturn);
				 returnPrice = (getPriceForRome(returnPeople,transfer_type));
			 }
		}else if (address_return == 3 || address_return == 4) {
			returnPrice = getPriceForNapoli(returnPeople,transfer_type);
			 if(childrenReturn != " ") {
				returnPeople = returnPeople + parseInt(childrenReturn);
				 returnPrice = (getPriceForNapoli(returnPeople,transfer_type));
			 }
		}
		// if( childrenReturn != " "){
		//  	returnPeople = returnPeople + parseInt(childrenReturn);
		//  	returnPrice = (getPrice(returnPeople));
		// }
	}

	var bookingData = {adults : adults, children : children , people : people, price : price, adultsReturn : adultsReturn, childrenReturn : childrenReturn,  returnPeople : returnPeople, returnPrice : returnPrice };

	information = bookingData;

	return bookingData;
}

/* Update the UI adding the new calculated price */
function appendPrice() {

	$('#adults-n').empty();
	$('#children-n').empty();
	$('#total-n').empty();
	$('#price-n').empty();

	var bookingData = calculatePrice();

	var adultsString = '',
		childrenString = '',
		totalString = '',
		priceString = '';

	var returnDate = $('#returnDate').val();
	var totalPeopleA = bookingData.people,
		totalPeopleR = bookingData.returnPeople;

	if (returnDate != " " && returnDate != "" && returnDate != undefined){
		if (bookingData.adults != " " && bookingData.adultsReturn != " ") {
			adultsString = 'A: '+ bookingData.adults  + ', R: '+ bookingData.adultsReturn;
		}
		if (bookingData.children != " " && bookingData.childrenReturn != " ") {
			childrenString = 'A: '+ bookingData.children  + ', R: '+ bookingData.childrenReturn;
		}
		if (totalPeopleR != 0 && totalPeopleA != 0) {
			totalString = 'A: '+ totalPeopleA+ ' x '+ bookingData.price + '€, R: '+ totalPeopleR+ ' x '+ bookingData.returnPrice+'€';
		}
		var priceA = totalPeopleA * bookingData.price;
		var priceR = totalPeopleR * bookingData.returnPrice;
		var totalPrice = priceA + priceR;
		information.totalPrice = totalPrice;
		priceString = totalPrice + '€';

	}
	else {
		if (bookingData.adults != " "){
			adultsString = bookingData.adults;
		}
		if (bookingData.children  != " "){
			childrenString = bookingData.children;
		}
		if (totalPeopleA != 0) {
			totalString = totalPeopleA+ ' x '+ bookingData.price +'€';
		}
		var priceA = totalPeopleA * bookingData.price;
		information.totalPrice = priceA;
		priceString = priceA + '€';
	}

	$('#adults-n').append(adultsString);
	$('#children-n').append(childrenString);
	$('#total-n').append(totalString);
	$('#price-n').append(priceString);

}

function checkStep1() {
	var date = $('#date').val();
	if(date != " " && date != "" && date != undefined) {
		return true;
	} else {
		return false;
	}
}

/* update the global var information with new data and send booking info to backend with ajax */
function sendBooking() {

	information.pickupAddress = $('#address option:selected').text();
	information.dropoffAddress = $('#address_2 option:selected').text();
	information.transferType = true;
	information.date = $('#date').val();
	information.time = $('#time').val();
	information.returnDate = $('#returnDate').val();
	information.returnTime = $('#returnTime').val();
	information.price = $('#price-n').val();
	information.people = $('#adults').val();
	information.children = $('#children').val();
	information.serviceQuantity = 1;
	information.transfer_type =  $("input[name='transfer_type']:checked").val();
	information.flight_train_no =  $("#flight_train_no").val();
	information.special_request =  $("#special_request").val();
	information.infants =  $("#infants").val();
	information.pets =  $("#pets").val();

  var returnDate = $('#returnDate').val();

  if (returnDate != " " && returnDate != "" && returnDate != undefined){
  	information.pickupReturnAddress = $('#address_return option:selected').text();
  	information.dropoffReturnAddress = $('#address_return_2 option:selected').text();
		information.serviceQuantity = 2;
		information.return_flight_train_no =  $("#return_flight_train_no").val();
  }
	
	
	$.ajax({
        url: base_url+'/Cart/add',
        type: 'POST',
        cache: false,
        crossDomain: true,
        data : information,
        headers: { 'Access-Control-Allow-Origin': '*' },
        complete: function(response) {
					console.log(response)
          if(response) {
              try {
                  var data =  JSON.parse(response.responseText);
                  console.log(data);
                  if(data.success == true){
                    window.location = base_url+'Cart/index';
                  }
                  else {
                    toastr.warning(data.message);
                  }
              } catch(e) {
                  toastr.warning(e);

              }
          }
        }
    });

	// $.ajax({
  //       url: '/Cart/add',
  //       type: 'POST',
  //       cache: false,
  //       crossDomain: true,
  //       data : information,
  //       headers: { 'Access-Control-Allow-Origin': '*' },
  //       complete: function(response) {
  //       	if(response.responseText) {
  //       		window.location = '/Cart/index';
  //       	} else {
	// 					$('#fail-msg').fadeIn('slow','linear');
  //       	}
  //       }
  //   });
}
