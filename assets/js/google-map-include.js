/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$(document).ready(function () {
	$('#map-canvas').css({'background':"url('https://ressio.github.io/lazy-load-xt/dist/loading.gif') center center no-repeat"});
	$('#map-canvas-header').css({'background':"url('https://ressio.github.io/lazy-load-xt/dist/loading.gif') center center no-repeat"});
	$('#map-canvas-sidebar').css({'background':"url('https://ressio.github.io/lazy-load-xt/dist/loading.gif') center center no-repeat"});
});


$(".map-info-box .line h2").click(function () {
    $(".map-info-box .line p").slideUp();
    $(this).next("p").slideToggle();
});

var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;

function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();
    var myStyles = [{
            featureType: "poi",
            elementType: "labels",
            stylers: [{
                    visibility: "off"
                }]
        }];
    
    var mainPlace = new google.maps.LatLng(CUR_LAT, CUR_LNG);
    var mapOptions = {
        zoom: 16,
        disableDefaultUI: true,
        disableDoubleClickZoom: true,
        scrollwheel: false,
        center: mainPlace,
        styles: myStyles,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        
    }
    map = new google.maps.Map(document.getElementById(MAP_ID), mapOptions);
        
    if (CUR_LAT === 0 && CUR_LNG === 0) {
        var bounds = new google.maps.LatLngBounds();
		for( i = 0; i < markers.length; i++ ) {
			
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
           marker = new google.maps.Marker({
              position: position,
              map: map,
              title: markers[i][0]
           });
		}
        directionsDisplay.setMap(map);
	 map.fitBounds(bounds);
    } else {
       var marker = new google.maps.Marker({
            position: mainPlace,
            map: map
        });
        directionsDisplay.setMap(map);
    }
    
    google.maps.event.addListenerOnce(map, 'idle', function(){
        //loaded fully
        $('#map-canvas').css({'background':'none'});
        $('#map-canvas-header').css({'background':'none'});
        $('#map-canvas-sidebar').css({'background':'none'});
    });
    
}

function calcRoute(destination) {
    var start = 'Corricella, Procida, Naples, Italy';
    var end = destination;
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addDomListener(window, 'resize', initialize);


