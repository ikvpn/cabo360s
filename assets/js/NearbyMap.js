var NearbyMap = function () {

    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    var map;

    return {

        init: function(lat, lng){
          if(lat === 0 || lng === 0){
            $('.map-section').remove();
          }
          directionsDisplay = new google.maps.DirectionsRenderer();
          var mainPlace = new google.maps.LatLng(lat, lng);
          var myStyles = [{
              featureType: "poi",
              elementType: "labels",
              stylers: [{
                  visibility: "off"
              }]
          }];
          var mapOptions = {
              zoom: 16,
              disableDefaultUI: true,
              disableDoubleClickZoom: true,
              scrollwheel: false,
              center: mainPlace,
              styles: myStyles
          }
          map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
          var marker = new google.maps.Marker({
              position: mainPlace,
              map: map,
              disableDefaultUI: false
          });
          directionsDisplay.setMap(map);
        },
        calcRoute: function(startPlace, destination){
          var start = startPlace+', Procida, Naples, Italy';
          var end = destination;
          var request = {
              origin: start,
              destination: end,
              travelMode: google.maps.TravelMode.DRIVING
          };
          directionsService.route(request, function(response, status) {
            console.log(response, status);
              if (status == google.maps.DirectionsStatus.OK) {
                  directionsDisplay.setDirections(response);
              }
          });
        }

    };

    google.maps.event.addDomListener(window, 'load', initialize);
    google.maps.event.addDomListener(window, 'resize', initialize);
}();

$(".map-info-box .line h2").click(function() {
    $(".map-info-box .line p").slideUp();
    $(this).next("p").slideToggle();
});
