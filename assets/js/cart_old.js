var Cart = function() {

    return {

        init: function() {
          $(".numbers-row").append('<div class="inc button_inc">+</div><div class="dec button_inc">-</div>');

          $('.magnific-gallery').each(function() {
        	    $(this).magnificPopup({
        	        delegate: 'a',
        	        type: 'image',
        	        gallery:{enabled:true}
        	    });
        	});
        },
        transferPrice: function(people){
        	var price = 0;
        	switch(people) {
        		case 0:
        			price = '0';
        			break;
      	    case 1:
    	        price = '53';
    	        break;
      	    case 2:
    	        price = '45';
    	        break;
      	    case 3:
    	        price = '42';
    	        break;
      	    case 4:
    	        price = '40';
    	        break;
      	    default:
    	        price = '40';
    	        break;
          }
        	return price;
        },
        // for (CIA) Rome Airport Ciampino & (FCO) Rome Airport Fiuminico
        getPriceForRome: function(people,transfer_type) {
          var price = 0;
          switch (transfer_type) {
            case 'car_transfer':
              switch(people) {
                case 0:
                  price = '0';
                  break;
                  case 1:
                      price = '360';
                      break;
                  case 2:
                      price = '190';
                      break;
                  case 3:
                      price = '140';
                      break;
                  case 4:
                      price = '120';
                      break;
                  case 5:
                      price = '110';
                      break;
                  case 6:
                      price = '95';
                      break;
                  default:
                      price = '95';
                      break;
              }
              break;
          
            case 'yacht_transfer':
              switch(people) {
                case 0:
                  price = '0';
                  break;
                  case 1:
                      price = '1200';
                      break;
                  case 2:
                      price = '600';
                      break;
                  case 3:
                      price = '400';
                      break;
                  case 4:
                      price = '300';
                      break;
                  case 5:
                      price = '240';
                      break;
                  case 6:
                      price = '200';
                      break;
                  default:
                      price = '200';
                      break;
              }
              break;
            
          }
          

          return price;
        },
        //  for (NAP) Naples International Airport & Napoli Centrale railway station
         getPriceForNapoli: function(people,transfer_type) {
          var price = 0;
          switch (transfer_type) {
            case 'car_transfer':
              switch(people) {
                case 0:
                  price = '0';
                  break;
                  case 1:
                      price = '65';
                      break;
                  case 2:
                      price = '45';
                      break;
                  case 3:
                      price = '43';
                      break;
                  case 4:
                      price = '39';
                      break;
                  case 5:
                      price = '35';
                      break;
                  case 6:
                      price = '35';
                      break;
                  default:
                      price = '35';
                      break;
              }
              break;
          
            case 'yacht_transfer':
              switch(people) {
                case 0:
                  price = '0';
                  break;
                  case 1:
                      price = '900';
                      break;
                  case 2:
                      price = '450';
                      break;
                  case 3:
                      price = '300';
                      break;
                  case 4:
                      price = '225';
                      break;
                  case 5:
                      price = '180';
                      break;
                  case 6:
                      price = '150';
                      break;
                  default:
                      price = '150';
                      break;
              }
              break;
            
          }

          return price;
        },
        paymentView: function(){
          var total = $('.totalcost').text();
          total = parseInt(total.replace('€ ', ''));
          if(total == 0){
            toastr.warning('Please add some items to the cart to proceed to the payment step');
            return;
          }
          $('#step1').fadeOut().addClass('hidden');
          $('#book-now-button').fadeOut().addClass('hidden');
          $('#step2').fadeIn().removeClass('hidden');
          Cart.calculatePrice();
        },
        validateEmail: function(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        checkForm: function(){
          var firstname = $('input[name="firstname"]').val();
          var lastname = $('input[name="lastname"]').val();
          var email = $('input[name="email"]').val();
          var phone = $('input[name="phone"]').val();
          var name_card = $('input[name="name_card"]').val();
          var street = $('input[name="street_1"]').val();
          var city_booking = $('input[name="city_booking"]').val();
          var state_booking = $('#state_booking').val();
          var postal_code = $('input[name="postal_code"]').val();
          var policy_terms = $('input[name="policy_terms"]').prop('checked');
          if(firstname.length < 4 || firstname == undefined || firstname == null){
            return 'Name is required';
          }
          if(lastname.length < 4 || lastname == undefined || lastname == null){
            return 'LastName is required';
          }
          if(email.length < 4 || email == undefined || email == null){
            return 'E-Mail is required';
          } else if(email.length > 0){
            var valid = Cart.validateEmail(email);
            if(!valid)
              return 'E-mail not valid';
          }
          if(phone.length < 4 || phone == undefined || phone == null){
            return 'Phone is required';
          }
          if(name_card.length < 4 || name_card == undefined || name_card == null){
            return 'Name on Card is required';
          }
          if(street.length < 4 || street == undefined || street == null){
            return 'Street is required';
          }
          if(city_booking.length == 0 || city_booking == undefined || city_booking == null){
            return 'City is required';
          }
          if(state_booking.length == 0 || state_booking == undefined || state_booking == null){
            return 'State is required';
          }
          if(postal_code.length == 0 || postal_code == undefined || postal_code == null){
            return 'Postal Code is required';
          }
          if(!policy_terms){
            return 'Policy terms are required';
          }
          return true;
        },
        do_payment: function(){
          var firstCheck = Cart.checkForm();

          if(firstCheck == true){
            Stripe.setPublishableKey('pk_live_rn5knK1KfpCaWSNawZYrbMMM00lX1Hx8NO'); // easyisland Live Key
           // Stripe.setPublishableKey('pk_test_V6OXPhPiNfLfmHetIHpu86qf00Iohrc9Dp'); // easyisland Test Key
            //Stripe.setPublishableKey('pk_test_VaiHbxMGF2Sn0UStN98Yyaqy'); // VPN Test Key
            var $form = $('#payment-form');
            var card = $('#card_number').val();
            var cardTest = Stripe.card.validateCardNumber(card);
            if(cardTest){
              var exp_month = $('#expire_month').val();
              var exp_year = $('#expire_year').val();
              var testExp = Stripe.card.validateExpiry(exp_month, exp_year);
              if(testExp){
                var cvc = $('#cvc').val();
                var testCvc = Stripe.card.validateCVC(cvc);
                if(testCvc){
                  var cardType = Stripe.card.cardType(card);
                  $form.append($('<input type="hidden" name="cardType" />').val(cardType));
                  Stripe.card.createToken({
                      number: $('#card_number').val(),
                      cvc: $('#cvc').val(),
                      exp_month: $('#expire_month').val(),
                      exp_year: $('#expire_year').val(),
                      address_zip: $('#address_zip').val()
                    }, stripeResponseHandler);
                } else {
                  toastr.warning('CVV Code error');
                }
              } else {
                toastr.warning('Expiration date error');
              }
            } else {
              toastr.warning('Wrong Credit Card Number');
            }
          } else {
            toastr.warning(firstCheck);
          }
        },
        calculatePrice: function(){
          var HotelAmount = 0;
          var TransferAmount = 0;
          var TourAmount = 0;
          var peopleTranfer = 0;
          var peopleTour = 0;
          var childrenTour = 0;

          $('.Hprice').each(function(i, v){
            var hPrice = $(v).html();
                hPrice = hPrice.replace('€ ', '');
              HotelAmount += parseInt(hPrice);
          });

          if(isNaN(HotelAmount)) {
            HotelAmount = 0;
          }

          // $('.transferSP').each(function(i, v){
          //   var singleTransferPrice = $(v).text();
          //   console.log('singleTransferPrice', singleTransferPrice);

          //   singleTransferPrice = singleTransferPrice.replace('€ ', '');
          //   var sID = $(v).attr('data-id');
          //   var serviceQuantity = $(v).attr('data-service-quantity');
          //   console.log('serviceQuantity', serviceQuantity);
          //   console.log('sID', sID);
          //    peopleTranfer = $('#transfer-'+sID).val();
          //     console.log('peopleTranfer', peopleTranfer);
          //   if(isNaN(peopleTranfer) || singleTransferPrice == ""){
          //     TransferAmount += 0;
          //   } else {
          //     TransferAmount += (parseInt(singleTransferPrice) * parseInt(peopleTranfer)) * serviceQuantity;

          //     console.log('TransferAmount', TransferAmount);
          //   }
          //   console.log('TransferAmount', TransferAmount);

          // });

          $('.transferTP').each(function(i, v){
              var singleTransferPrice = $(v).text();
              console.log('singleTransferPrice', singleTransferPrice);
  
              singleTransferPrice = singleTransferPrice.replace('€ ', '');
              var sID = $(v).attr('data-id');
              var serviceQuantity = $(v).attr('data-service-quantity');
              console.log('serviceQuantity', serviceQuantity);
              console.log('sID', sID);
               peopleTranfer = $('#transfer-'+sID).val();
                console.log('peopleTranfer', peopleTranfer);
              if(isNaN(peopleTranfer) || singleTransferPrice == ""){
                TransferAmount += 0;
              } else {
                TransferAmount += parseInt(singleTransferPrice);
  
                console.log('TransferAmount', TransferAmount);
              }
              console.log('TransferAmount', TransferAmount);
  
            });

          $('.tourp').each(function(i, v){
            var tourPrice = parseInt($(v).data('price'));
            var tourID = $(v).data('id');
            var discountAdult = $(v).data('discountAdults');
            var discountChildren = $(v).data('discountChildren');
             peopleTour = $('#tour-adults-'+tourID).val();
             childrenTour = $('#tour-children-'+tourID). val();

            var priceUnit = $('#tour-price-unit-'+tourID).val();
            if (priceUnit == 'P'){

              var tourPriceAdults = tourPrice * peopleTour;
              var tourPriceChildren = tourPrice * childrenTour;
              if(parseInt(discountAdult) > 0){
                tourPriceAdults = (tourPrice - (tourPrice * discountAdult / 100) ) * peopleTour;
              }
              if(parseInt(discountAdult) > 0){
                 tourPriceChildren = (tourPrice - (tourPrice * discountChildren / 100) ) * childrenTour;
              }
              tourPrice = tourPriceAdults + tourPriceChildren;
            }

            TourAmount += tourPrice;
          });

          $('#hotelP').val(HotelAmount);
          $('#transferP').val(TransferAmount);
          $('#tourP').val(TourAmount);

          $('#people-tour-cart').val(peopleTour);
          $('#children-tour-cart').val(childrenTour);
          $('#transfer-people-cart').val(peopleTranfer);
          if($('#tour_id').val()){
            $('#tour_suggested_id').val($('#tour_id').val());
          }
          if($('#datepickertour').val()) {
            $('#date_tour').val($('#datepickertour').val());
          }


          var total = HotelAmount + TransferAmount + TourAmount;
          if(isNaN(total)){
            total = 0;
          }
          $('#totalP').val(total);
          $('.totalcost').html('€ '+total);
        }
      };
}();

$(document).ready(function(){
  Cart.init();
  $( ".transfer" ).each(function( index ) {
    console.log( index + ": " + $( this ).text() );
    var tValue = $(this).val();
        tValue = parseInt(tValue);
    var tID = $(this).attr('data-id');
    var serviceQuantity = $(this).attr('data-service-quantity');
    var roundAdd        = $(this).attr('data-round-pickup');
    var returnDropAdd   = $(this).attr('data-return-drop');
    var transfer_type   = $(this).attr('data-trasfer-type');
    if (roundAdd == 1 || roundAdd==2) {
      var newRoundPrice = Cart.getPriceForRome(tValue,transfer_type);
    }
    if (roundAdd == 3 || roundAdd==4) {
      var newRoundPrice = Cart.getPriceForNapoli(tValue,transfer_type);
    }
    if (returnDropAdd == 1 || returnDropAdd==2) {
      var newReturnPrice = Cart.getPriceForRome(tValue,transfer_type);
    }
    if (returnDropAdd == 3 || returnDropAdd==4) {
      var newReturnPrice = Cart.getPriceForNapoli(tValue,transfer_type);
    }
    var newPrice = newRoundPrice + newReturnPrice;
    var subtotal = '';
    var totalPriceTranfer = 0;
    if (newRoundPrice) {
      subtotal += 'A:'+tValue+' * € '+newRoundPrice;
      totalPriceTranfer += newRoundPrice * tValue;
    }
    if (newReturnPrice) {
      subtotal += ', R:'+tValue+' * € '+newReturnPrice;
      totalPriceTranfer += newReturnPrice * tValue;
    }
    // var newPrice = Cart.transferPrice(tValue);
    $('#transferSP-'+tID).html('<strong>'+subtotal+'</strong>');
    // $('#transferSP-'+tID).html('<strong> € '+newPrice+'</strong>');
    // var totalPriceTranfer = (tValue*newPrice)*serviceQuantity;
    // var totalPriceTranfer = (newRoundPrice*tValue) + (newReturnPrice*tValue);
    $('#transferTP-'+tID).html('<strong>€ '+totalPriceTranfer+'</strong>');
  });

  Cart.calculatePrice();

  var hasTransferSuggest = $("#has_suggest_transfer").val();
  if(hasTransferSuggest) {
    $("#suggest_transfer").val("1");
  }

  var checkInTransfer = $('#checkInTransfer').val();

  if(checkInTransfer && checkInTransfer != '' && checkInTransfer != ' '){
      checkInTransfer = checkInTransfer.split(' ');
      checkInTransfer = checkInTransfer[0];
  }
  else {
    checkInTransfer = new Date().toString();
  }

  var checkOutTransfer = $('#checkOutTransfer').val();

  if(checkOutTransfer && checkOutTransfer != '' && checkOutTransfer != ' '){
      checkOutTransfer = checkOutTransfer.split(' ');
      checkOutTransfer = checkOutTransfer[0];
  }
  else {
    checkOutTransfer = '';
  }

  console.log('checkInTransfer', checkInTransfer);
  console.log('checkOutTransfer', checkOutTransfer);

  var checkIn = $('#checkInHotel').val() ? $('#checkInHotel').val() : checkInTransfer;


  var checkOut = $('#checkOutHotel').val() ? $('#checkOutHotel').val() : checkOutTransfer;

  console.log('checkIn', checkIn);
  console.log('checkOut', checkOut);

  $("#datepickertour").datepicker({format: "dd-mm-yyyy",
                                  autoclose: true,
                                  startDate: checkIn,
                                  defaultViewDate: checkIn ,
                                  endDate : checkOut,
                                  weekStart : 1
                                }).datepicker("setDate", checkIn);


  $("#datepickertour").on("change", function(e) {
    console.log($(this).val());
    $('.tourDate').html($(this).val());
    $('#date_tour').val($(this).val());
  });

  $("#address_return_2").on("change", function(e){
    console.log($(this).val());
    $('.destination').html($(this).val());
    $("#destination_transfer").val($(this).val());
  });

  $(".button_inc").on("click", function () {
    var $button = $(this);
    console.log('$button', $button);
    var serviceID = $button.parent().find("input").attr('data-id');
    console.log('serviceID', serviceID);
    var oldValue = $button.parent().find("input").val();
    console.log('oldValue', oldValue);
    var price = $(this).parent().attr('data-price');
    console.log('price', price);

    var actualAmount = $('.totalcost').text();
        actualAmount = parseInt(actualAmount.replace('€ ', ''));

    if(parseInt(oldValue) == 0)
      $button.parent().find("input").next().next().addClass('disabled');

    var newVal = oldValue;
    if ($button.text() == "+") {
      $button.next().removeClass('disabled')
      newVal = parseFloat(oldValue) + 1;
      $button.parent().find("input").val(newVal);
      console.log('newVal +', newVal)
      if($button.prev().hasClass('hotels')){

        var $totalRoomPrice = $button.parent().parent().next().next();
          console.log('$totalRoomPrice', $totalRoomPrice);
        $totalRoomPrice.html('<strong class="Hprice">€ '+ parseInt(price)*parseInt(newVal)+'</strong>');

        $('#rooms-hotel-'+serviceID).text(newVal);
      }

      if($button.prev().hasClass('transfer')){
        var destinations = $("#address_return_2").val();
        $("#destination_transfer").val(destinations);
        if($('#transferTable-'+serviceID).length <= 0){
          console.log('Not transfer');
          var transferTable = "<tr id='transferTable-"+serviceID+"'><td><strong>TRANSFER</strong></td><td></tr>";
          var peopleTranferTable = "<tr><td>People</td><td class='text-right transferTable' id='transferTablePeople-"+serviceID+"'>"+newVal+"</td> </tr>";
          var destinationsranferTable = "<tr><td>Destinations</td><td class='text-right transferTable destination' id='destinationsTablePeople-"+serviceID+"'>"+destinations+"</td> </tr>";
          $('.total').before(transferTable);
          $('.total').before(peopleTranferTable);
          $('.total').before(destinationsranferTable);
        }
        else {
          console.log('transferTablePeople', newVal);
          $('#transferTablePeople-'+serviceID).text(newVal);
          $('#destinationsTablePeople-'+serviceID).text(destinations);
        }
        var input_this = $button.parent().find("input");

        var tValue = parseInt(input_this.val());
        

        var roundAdd        = input_this.attr('data-round-pickup');
        var returnDropAdd   = input_this.attr('data-return-drop');
        var transfer_type   = input_this.attr('data-trasfer-type');
        
        
        if (roundAdd == 1 || roundAdd==2) {
          var newRoundPrice = Cart.getPriceForRome(tValue,transfer_type);
        }
        if (roundAdd == 3 || roundAdd==4) {
          var newRoundPrice = Cart.getPriceForNapoli(tValue,transfer_type);
        }
        if (returnDropAdd == 1 || returnDropAdd==2) {
          var newReturnPrice = Cart.getPriceForRome(tValue,transfer_type);
        }
        if (returnDropAdd == 3 || returnDropAdd==4) {
          var newReturnPrice = Cart.getPriceForNapoli(tValue,transfer_type);
        }
        var newPrice = newRoundPrice + newReturnPrice;
        var subtotal = '';
        var totalPriceTranfer = 0;
        if (newRoundPrice) {
          subtotal += 'A:'+tValue+' * € '+newRoundPrice;
          totalPriceTranfer += newRoundPrice * tValue;
        }
        if (newReturnPrice) {
          subtotal += ', R:'+tValue+' * € '+newReturnPrice;
          totalPriceTranfer += newReturnPrice * tValue;
        }
        

        // $('#transferSP-'+tID).html('<strong>'+subtotal+'</strong>');
        // var totalPriceTranfer = (newRoundPrice*tValue) + (newReturnPrice*tValue);
          //  $('#transferTP-'+tID).html('<strong>€ '+totalPriceTranfer+'</strong>');
           
        // price = Cart.transferPrice(parseInt(newVal));

        // var serviceQuantity = $button.prev().attr('data-service-quantity');
        // console.log('serviceQuantity + ', serviceQuantity);

        // var totalPriceTranfer = price * newVal * serviceQuantity;

        console.log('priceTranfer', price);
        console.log('totalPriceTranfer', totalPriceTranfer);

        $('#transferSP-'+serviceID).html('<strong>'+subtotal+'</strong>');
        $('#transferTP-'+serviceID).html('<strong>€ '+totalPriceTranfer+'</strong>');
      }

      console.log('$button.prev()', $button.prev());
      if($button.prev().hasClass('touradults')){ //adults

        var priceUnit = $('#tour-price-unit-'+serviceID).val();
          console.log('priceUnit', priceUnit);
        if (priceUnit == 'P'){
          var discountAdult = $button.prev().data('discountAdults');
          var discountChildren = $button.prev().data('discountChildren');
          var peopleTour = $('#tour-adults-'+serviceID).val();
          console.log('peopleTour', peopleTour);
          var childrenTour = $('#tour-children-'+serviceID). val();
          console.log('childrenTour', childrenTour);
          var tourPriceAdults = price * peopleTour;
          var tourPriceChildren = price * childrenTour;
          if(parseInt(discountAdult) > 0){
            tourPriceAdults = (tourPrice - (tourPrice * discountAdult / 100) ) * peopleTour;
          }
          if(parseInt(discountAdult) > 0){
             tourPriceChildren = (tourPrice - (tourPrice * discountChildren / 100) ) * childrenTour;
          }
          price = tourPriceAdults + tourPriceChildren;
        }


        if($('#tourTable-'+serviceID).length <= 0){
          console.log('Not tour');
          var date = $("#datepickertour").val();
          var tourTable = "<tr id='tourTable-"+serviceID+"'><td><strong>TOUR</strong></td><td></tr>"
          var adultsTourTable = "<tr><td>Adults</td><td class='text-right transferTable' id='tourTableAdults-"+serviceID+"'>"+newVal+"</td> </tr>"
          var dateTour = "<tr id='tourTabledate-"+serviceID+"'><td >Date</td><td class='text-right transferTable tourDate'>"+date+"</td> </tr>"
          $('.total').before(tourTable);
          $('.total').before(dateTour);
          $('.total').before(adultsTourTable);
        }
        else {
          if($('#tourTableAdults-'+serviceID).length <= 0){
            var adultsTourTable = "<tr><td>Adults</td><td class='text-right transferTable' id='tourTableAdults-"+serviceID+"'>"+newVal+"</td> </tr>"
            $('#tourTabledate-'+serviceID).after(adultsTourTable);
          }
          else {
            console.log('tourTableAdults', newVal);
            $('#tourTableAdults-'+serviceID).text(newVal);
          }

        }

        $('#tour-servicePriceTotal-'+serviceID).html('<strong>€ '+price+'</strong>');
      }

      if($button.prev().hasClass('tourchildren')){ //children

        var priceUnit = $('#tour-price-unit-'+serviceID).val();
        if (priceUnit == 'P'){
          var discountAdult = $button.prev().data('discountAdults');
          var discountChildren = $button.prev().data('discountChildren');
          var peopleTour = $('#tour-adults-'+serviceID).val();
          console.log('peopleTour +', peopleTour);
          var childrenTour = $('#tour-children-'+serviceID). val();

          console.log('childrenTour +', childrenTour);
          var tourPriceAdults = price * peopleTour;
          var tourPriceChildren = price * childrenTour;

          if(parseInt(discountAdult) > 0){
            tourPriceAdults = (tourPrice - (tourPrice * discountAdult / 100) ) * peopleTour;
          }
          if(parseInt(discountAdult) > 0){
             tourPriceChildren = (tourPrice - (tourPrice * discountChildren / 100) ) * childrenTour;
          }
          price = tourPriceAdults + tourPriceChildren;
        }

        if($('#tourTable-'+serviceID).length <= 0){
          console.log('Not tour');
          var date = $("#datepickertour").val();
          var tourTable = "<tr id='tourTable-"+serviceID+"'><td><strong>TOUR</strong></td><td></tr>"
          var childTourTable = "<tr><td>Children</td><td class='text-right transferTable' id='childTourTable-"+serviceID+"'>"+newVal+"</td> </tr>"
          var dateTour = "<tr id='tourTabledate-"+serviceID+"'><td >Date</td><td class='text-right transferTable tourDate' >"+date+"</td> </tr>"
          $('.total').before(tourTable);
          $('.total').before(dateTour);
          $('.total').before(childTourTable);
        }
        else {
          if($('#childTourTable-'+serviceID).length <= 0){
            console.log("there is tour but not children")
            var childTourTable = "<tr><td>Children</td><td class='text-right transferTable' id='childTourTable-"+serviceID+"' class=''>"+newVal+"</td> </tr>"
            $('#tourTabledate-'+serviceID).after(childTourTable);
          }
          else {
            console.log('tourTablechild', newVal);
            $('#childTourTable-'+serviceID).text(newVal);
          }

        }

        $('#tour-servicePriceTotal-'+serviceID).html('<strong>€ '+price+'</strong>');
      }
    }
    else {

      if(!$button.hasClass('disabled')){
        if (oldValue > 1) {
          newVal = parseFloat(oldValue) - 1;
        } else {
          newVal = 0;
          $button.addClass('disabled');
        }
      }
      if(typeof(oldValue) === 'string' && parseInt(oldValue) === 0){
        newVal = 0;
      }
      if(typeof(oldValue) === 'number' || oldValue == ''){
        newVal = 0;
      }
      $button.parent().find("input").val(newVal);
      console.log('newVal -', newVal);
      if($button.prev().prev().hasClass('hotels')){

        var $totalRoomPrice = $button.parent().parent().next().next();
          console.log('$totalRoomPrice', $totalRoomPrice);
        $totalRoomPrice.html('<strong class="Hprice">€ '+ parseInt(price)*parseInt(newVal)+'</strong>');

        $('#rooms-hotel-'+serviceID).text(newVal);
      }

      if($button.prev().prev().hasClass('transfer')){
        var destinations = $("#address_return_2").val();
        $("#destination_transfer").val(destinations);
        if($('#transferTable-'+serviceID).length <= 0){
          console.log('Not transfer');
          var transferTable = "<tr id='transferTable-"+serviceID+"'><td><strong>TRANSFER</strong></td><td></tr>"
          var peopleTranferTable = "<tr><td>People</td><td class='text-right transferTable' id='transferTablePeople-"+serviceID+"'>"+newVal+"</td> </tr>"
          var destinationsranferTable = "<tr><td>Destinations</td><td class='text-right transferTable destination' id='destinationsTablePeople-"+serviceID+"'>"+destinations+"</td> </tr>";

          $('.total').before(transferTable);
          $('.total').before(peopleTranferTable);
          $('.total').before(destinationsranferTable);
        }
        else {
          console.log('transferTablePeople', newVal);
          $('#transferTablePeople-'+serviceID).text(newVal);
          $('#destinationsTablePeople-'+serviceID).text(destinations);
        }


        var input_this = $button.parent().find("input");

        var tValue = parseInt(input_this.val());
        

        var roundAdd        = input_this.attr('data-round-pickup');
        var returnDropAdd   = input_this.attr('data-return-drop');
        var transfer_type   = input_this.attr('data-trasfer-type');
        
        
        if (roundAdd == 1 || roundAdd==2) {
          var newRoundPrice = Cart.getPriceForRome(tValue,transfer_type);
        }
        if (roundAdd == 3 || roundAdd==4) {
          var newRoundPrice = Cart.getPriceForNapoli(tValue,transfer_type);
        }
        if (returnDropAdd == 1 || returnDropAdd==2) {
          var newReturnPrice = Cart.getPriceForRome(tValue,transfer_type);
        }
        if (returnDropAdd == 3 || returnDropAdd==4) {
          var newReturnPrice = Cart.getPriceForNapoli(tValue,transfer_type);
        }
        var newPrice = newRoundPrice + newReturnPrice;
        var subtotal = '';
        var totalPriceTranfer = 0;
        if (newRoundPrice) {
          subtotal += 'A:'+tValue+' * € '+newRoundPrice;
          totalPriceTranfer += newRoundPrice * tValue;
        }
        if (newReturnPrice) {
          subtotal += ', R:'+tValue+' * € '+newReturnPrice;
          totalPriceTranfer += newReturnPrice * tValue;
        }
       
        

        // $('#transferSP-'+tID).html('<strong>'+subtotal+'</strong>');
        // var totalPriceTranfer = (newRoundPrice*tValue) + (newReturnPrice*tValue);

        // price = Cart.transferPrice(parseInt(newVal));

        // var totalPriceTranfer = price * newVal;

        // console.log('priceTranfer -', price);
        // console.log('totalPriceTranfer -', totalPriceTranfer);

        $('#transferSP-'+serviceID).html('<strong>'+subtotal+'</strong>');
        $('#transferTP-'+serviceID).html('<strong>€ '+totalPriceTranfer+'</strong>');
      }

      console.log('$button.prev().prev()', $button.prev().prev());
      if($button.prev().prev().hasClass('touradults')){ //adults

        var priceUnit = $('#tour-price-unit-'+serviceID).val();
          console.log('priceUnit - ', priceUnit);
        if (priceUnit == 'P'){
          var discountAdult = $button.prev().prev().data('discountAdults');
          var discountChildren = $button.prev().prev().data('discountChildren');
          var peopleTour = $('#tour-adults-'+serviceID).val();
            console.log('peopleTour - ', peopleTour);
          var childrenTour = $('#tour-children-'+serviceID). val();
            console.log('childrenTour - ', childrenTour);
          var tourPriceAdults = price * peopleTour;
          var tourPriceChildren = price * childrenTour;
          if(parseInt(discountAdult) > 0){
            tourPriceAdults = (tourPrice - (tourPrice * discountAdult / 100) ) * peopleTour;
          }
          if(parseInt(discountAdult) > 0){
             tourPriceChildren = (tourPrice - (tourPrice * discountChildren / 100) ) * childrenTour;
          }
          price = tourPriceAdults + tourPriceChildren;

        }

        if($('#tourTable-'+serviceID).length <= 0){
          console.log('Not tour');
          var date = $("#datepickertour").val();
          var tourTable = "<tr id='tourTable-"+serviceID+"'><td><strong>TOUR</strong></td><td></tr>"
          var adultsTourTable = "<tr><td>Adults</td><td class='text-right transferTable' id='tourTableAdults-"+serviceID+"'>"+newVal+"</td> </tr>"
          var dateTour = "<tr id='tourTabledate-"+serviceID+"'><td >Date</td><td class='text-right transferTable tourDate'>"+date+"</td> </tr>"
          $('.total').before(tourTable);
          $('.total').before(dateTour);
          $('.total').before(adultsTourTable);
        }
        else {
          if($('#tourTableAdults-'+serviceID).length <= 0){
            var adultsTourTable = "<tr><td>Adults</td><td class='text-right transferTable' id='tourTableAdults-"+serviceID+"'>"+newVal+"</td> </tr>"
            $('#tourTable-'+serviceID).after(adultsTourTable);
          }
          else {
            console.log('tourTableAdults', newVal);
            $('#tourTableAdults-'+serviceID).text(newVal);
          }

        }

        $('#tour-servicePriceTotal-'+serviceID).html('<strong>€ '+price+'</strong>');
      }
      if($button.prev().prev().hasClass('tourchildren')){ //children

        var priceUnit = $('#tour-price-unit-'+serviceID).val();
        if (priceUnit == 'P'){
          var discountAdult = $button.prev().prev().data('discountAdults');
          var discountChildren = $button.prev().prev().data('discountChildren');
          var peopleTour = $('#tour-adults-'+serviceID).val();
            console.log('peopleTour - ', peopleTour);
          var childrenTour = $('#tour-children-'+serviceID). val();
            console.log('childrenTour - ', childrenTour);
          var tourPriceAdults = price * peopleTour;
          var tourPriceChildren = price * childrenTour;
          if(parseInt(discountAdult) > 0){
            tourPriceAdults = (tourPrice - (tourPrice * discountAdult / 100) ) * peopleTour;
          }
          if(parseInt(discountAdult) > 0){
             tourPriceChildren = (tourPrice - (tourPrice * discountChildren / 100) ) * childrenTour;
          }
          price = tourPriceAdults + tourPriceChildren;
        }
        if($('#tourTable-'+serviceID).length <= 0){
          console.log('Not tour');
          var tourTable = "<tr id='tourTable-"+serviceID+"'><td><strong>TOUR</strong></td><td></tr>"
          var childTourTable = "<tr><td>Children</td><td class='text-right transferTable' id='childTourTable-"+serviceID+"'>"+newVal+"</td> </tr>"
          $('.total').before(tourTable);
          $('.total').before(childTourTable);
        }
        else {
          if($('#childTourTable-'+serviceID).length <= 0){
            var childTourTable = "<tr><td>Children</td><td class='text-right transferTable' id='childTourTable-"+serviceID+"'>"+newVal+"</td> </tr>"
            $('#tourTable-'+serviceID).after(childTourTable);
          }
          else {
            console.log('tourTablechild', newVal);
            $('#childTourTable-'+serviceID).text(newVal);
          }

        }
        $('#tour-servicePriceTotal-'+serviceID).html('<strong>€ '+price+'</strong>');
      }

    }

    Cart.calculatePrice();
  });

});

function stripeResponseHandler(status, response) {
  var $form = $('#payment-form');
  if (response.error) {
    $form.find('.payment-errors').text(response.error.message);
    $form.find('button').prop('disabled', false); // Re-enable submission
  } else {
    var token = response.id;
    $form.append($('<input type="hidden" name="stripeToken" />').val(token));
    Cart.calculatePrice();
    $form.get(0).submit();
  }
}
