/*
* Author: Davide Polano
* Email: info@mdslab.org
* Project: Visit Procida
* Version: 1.0
*/

/*
	@param: information
	@info : global object where we store all the booking information
*/

var d = new Date();

  var currDate = d.getDate();
  var currMonth = d.getMonth()+1;
  if(currMonth.toString().length == 1){
    currMonth = '0'+currMonth;
  }
  var currYear = d.getFullYear();

  var dateStr = currYear + "-" + currMonth + "-" + currDate;
  //console.log(dateStr);


  var lang = $('#lang').val();

  lang = lang == 'it' ? 'it-IT' : 'en';
    //console.log(lang);
  var startDateTour = $('#dateStart').val();
  
  var endDateTour = $('#dateEnd').val();
  var daysAvailability = $('#daysAvailability').val();
  var daysUnavailability = [];
  
  if(daysAvailability.length > 0) {
	  daysAvailability = daysAvailability.split(',');
	  for (var i = 0; i < daysAvailability.length; i++) {
	    daysAvailability[i] = (parseInt(daysAvailability[i])  + 1).toString();
	    if(  daysAvailability[i] == '7') {
	        daysAvailability[i] = '0';
	    }
	  }
	  for (var i = 0; i < 7; i++) {
	  
	        var day = i.toString();
	        //console.log(day);
	        if($.inArray(day, daysAvailability) == -1) {
	          daysUnavailability.push(day);
	        }
	    }
	  
  }

  $("input.datepicker").datepicker({format: "yyyy-mm-dd",
                                  autoclose: true,
                                  startDate: startDateTour,
                                  defaultViewDate: dateStr ,
                                  endDate : endDateTour,
                                  daysOfWeekDisabled : daysUnavailability,
                                  todayHighlight : true,
                                  weekStart : 1,
                                  language : lang
                                }).on('changeDate', function(e) {
			appendPrice();
    });



var information;
var returnTrip = false;
$("input.time-pick").timepicker({minuteStep: 15,showInpunts: false, defaultTime: false});
/* When user increment or decrement people \ children value we go to Update prices in the view  */

$('.inc').on('click', function() {
	$("#adultsReturn").val($("#adults").val());
	$("#childrenReturn").val($("#children").val());
	var adults = parseInt($( "#adults" ).val());
	var children = parseInt($( "#children" ).val());
	var total =  adults + children;
	var maxPass = parseInt($("#maxPass").val());
	if (maxPass < total) {
		var number  = parseInt($(this).prev( "input" ).val());
		$(this).prev( "input" ).val(number - 1);
		toastr.warning('Only '+maxPass+' passenger are allowed !');
	}
	
	
	appendPrice();
});

$('.dec').on('click', function() {
	appendPrice();
});
$("#address, #address_return_2, #address_2,#address_3,#address_4, #address_return").change(function() {
	var address = $('#address').val();
    if(address == 5) {
        $('.sjd_price').show(); 
        $('.csl_price').hide(); 
        $('.dropoff').hide(); 
    } else if (address == 6) {
        $('.csl_price').show(); 
    	$('.sjd_price').hide(); 
        $('.dropoff').hide(); 
    } else {
        $('.dropoff').show(); 
    	$('.csl_price').hide(); 
    	$('.sjd_price').hide(); 
    }
	appendPrice();
});
// $('input[type=radio][name=transfer_type]').change(function() {
// 	var transfer_type = $(this).val()
// 	if (transfer_type == 'yacht_transfer') {
// 		$("#for_car_transfer").hide();
// 		$("#for_yacht_transfer").show();
// 	}
// 	if (transfer_type == 'car_transfer') {
// 		$("#for_car_transfer").show();
// 		$("#for_yacht_transfer").hide();
// 	}
// 	appendPrice();
// });
/*
$('.date-pick').datepicker()
    .on('changeDate', function(e) {
			appendPrice();
    });*/

/* Scroll down when user click on the optional return */
$('.btn_collapse').on('click', function() {
	$('html, body').animate({
		scrollTop: $('#address').offset().top
	}, 'slow');
	if (returnTrip) {
		returnTrip = false;
	}else{
		returnTrip = true;
	}
	appendPrice();
});

/* get booking date information */
function getBookingDate() {
	var date = $('#date').val();
	var time = $('#time').val();

	var returnDate = $('#returnDate').val();
	var returnTime = $('#returnTime').val();

	var startDateTime = date+' '+time;
	var returnDateTime = returnDate+' '+returnTime;

	if(returnDateTime != " ") {
		var bookingInfo =  { startDateTime : startDateTime, returnDateTime : returnDateTime };
		return bookingInfo;
	} else {
		var bookingInfo =  { startDateTime : startDateTime };
		return bookingInfo;
	}
}

/* Get the price foreach people */
function getPrice(people) {
	var price = 0;
	switch(people) {
		case 0:
			price = '0';
			break;
	    case 1:
	        price = '53';
	        break;
	    case 2:
	        price = '45';
	        break;
	    case 3:
	        price = '42';
	        break;
	    case 4:
	        price = '40';
	        break;
	    default:
	        price = '40';
	        break;
	}

	return price;
}

// for (CIA) Rome Airport Ciampino & (FCO) Rome Airport Fiuminico
function getPriceForRome(people,transfer_type) {
	var price = 0;
	switch (transfer_type) {
		case 'car_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '360';
							break;
					case 2:
							price = '190';
							break;
					case 3:
							price = '140';
							break;
					case 4:
							price = '120';
							break;
					case 5:
							price = '110';
							break;
					case 6:
							price = '95';
							break;
					default:
							price = '95';
							break;
			}
			break;
	
		case 'yacht_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '1200';
							break;
					case 2:
							price = '600';
							break;
					case 3:
							price = '400';
							break;
					case 4:
							price = '300';
							break;
					case 5:
							price = '240';
							break;
					case 6:
							price = '200';
							break;
					default:
							price = '200';
							break;
			}
			break;
		
	}
	

	return price;
}

function getPriceForSJD(people,transfer_type) {
	var price = 0;
	switch (transfer_type) {
		case 'car_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '0';
							break;
					case 2:
							price = '0';
							break;
					case 3:
							price = '0';
							break;
					case 4:
							price = '0';
							break;
					case 5:
							price = '0';
							break;
					case 6:
							price = '0';
							break;
					default:
							price = '0';
							break;
			}
			break;
	
		case 'yacht_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '0';
							break;
					case 2:
							price = '0';
							break;
					case 3:
							price = '0';
							break;
					case 4:
							price = '0';
							break;
					case 5:
							price = '0';
							break;
					case 6:
							price = '0';
							break;
					default:
							price = '0';
							break;
			}
			break;
		
	}
	return price;
}
//  for (NAP) Naples International Airport & Napoli Centrale railway station
function getPriceForNapoli(people,transfer_type) {
	var price = 0;
	switch (transfer_type) {
		case 'car_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '65';
							break;
					case 2:
							price = '45';
							break;
					case 3:
							price = '43';
							break;
					case 4:
							price = '39';
							break;
					case 5:
							price = '35';
							break;
					case 6:
							price = '35';
							break;
					default:
							price = '35';
							break;
			}
			break;
	
		case 'yacht_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '900';
							break;
					case 2:
							price = '450';
							break;
					case 3:
							price = '300';
							break;
					case 4:
							price = '225';
							break;
					case 5:
							price = '180';
							break;
					case 6:
							price = '150';
							break;
					default:
							price = '150';
							break;
			}
			break;
		
	}

	return price;
}

function getPriceForCSL(people,transfer_type) {
	var price = 0;
	switch (transfer_type) {
		case 'car_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '0';
							break;
					case 2:
							price = '0';
							break;
					case 3:
							price = '0';
							break;
					case 4:
							price = '0';
							break;
					case 5:
							price = '0';
							break;
					case 6:
							price = '0';
							break;
					default:
							price = '0';
							break;
			}
			break;
	
		case 'yacht_transfer':
			switch(people) {
				case 0:
					price = '0';
					break;
					case 1:
							price = '0';
							break;
					case 2:
							price = '0';
							break;
					case 3:
							price = '0';
							break;
					case 4:
							price = '0';
							break;
					case 5:
							price = '0';
							break;
					case 6:
							price = '0';
							break;
					default:
							price = '0';
							break;
			}
			break;
		
	}

	return price;
}

/* Calculate the price, store the new info in the global var information and return bookingData */
function calculatePrice() {
	var adults = $('#adults').val();
	var children = $('#children').val();

	var adultsReturn = $('#adults').val();
	var childrenReturn = $('#children').val();

	var transfer_type =  $("#transType").val();

	var address = $('#address').val();
	var address_return = $('#address_return_2').val();
	if(address == 5)
		var zone_price = $('#address_3').val() != "" ? $('#address_3').val() : 0;

	if(address == 6)
		var zone_price = $('#address_4').val() != "" ? $('#address_4').val() : 0;
	
	var people = 0,
		price = 0,
		returnPeople = 0,
		returnPrice = 0;

	if(adults != " "){
		 people = parseInt(adults);
		 if (address == 1 || address == 2) {
		 	price = getPriceForRome(people,transfer_type) * people;
			if(children != " ") {
				people = people + parseInt(children);
				price = (getPriceForRome(people,transfer_type));
			}
		 }else if (address == 3 || address == 4) {
			price = getPriceForNapoli(people,transfer_type) * people;
			if(children != " ") {
				people = people + parseInt(children);
				price = (getPriceForNapoli(people,transfer_type));
			}
		 }else if (address == 5) {
			price = getPriceForSJD(people,transfer_type) * people;
			if(children != " ") {
				people = people + parseInt(children);
				price = (getPriceForSJD(people,transfer_type));
			}
			price = parseInt(price) + parseInt(zone_price);
		 }else if (address == 6) {
			price = getPriceForCSL(people,transfer_type) * people;
			if(children != " ") {
				people = people + parseInt(children);
				price = (getPriceForCSL(people,transfer_type));
			}
			price = parseInt(price) + parseInt(zone_price);
		 }
	}

	if(returnTrip) {
		returnPeople = parseInt(adultsReturn);
		// returnPrice = getPrice(returnPeople);
		if (address_return == 1 || address_return == 2) {
			 returnPrice = getPriceForRome(returnPeople,transfer_type);
			 if(childrenReturn != " ") {
				returnPeople = returnPeople + parseInt(childrenReturn);
				returnPrice = (getPriceForRome(returnPeople,transfer_type));
			 }
		}else if (address_return == 3 || address_return == 4) {
			 returnPrice = getPriceForNapoli(returnPeople,transfer_type);
			 if(childrenReturn != " ") {
				returnPeople = returnPeople + parseInt(childrenReturn);
				returnPrice = (getPriceForNapoli(returnPeople,transfer_type));
			 }
		}else if (address_return == 5) {
			 returnPrice = getPriceForSJD(returnPeople,transfer_type);
			 if(childrenReturn != " ") {
				returnPeople = returnPeople + parseInt(childrenReturn);
				returnPrice = (getPriceForSJD(returnPeople,transfer_type));
			 }
			returnPrice = parseInt(returnPrice) + parseInt(zone_price);
		}else if (address_return == 6) {
			 returnPrice = getPriceForCSL(returnPeople,transfer_type);
			 if(childrenReturn != " ") {
				returnPeople = returnPeople + parseInt(childrenReturn);
				returnPrice = (getPriceForCSL(returnPeople,transfer_type));
			 }
			returnPrice = parseInt(returnPrice) + parseInt(zone_price);
		}
		// if( childrenReturn != " "){
		//  	returnPeople = returnPeople + parseInt(childrenReturn);
		//  	returnPrice = (getPrice(returnPeople));
		// }
	}

	var bookingData = {adults : adults, children : children , people : people, price : price, adultsReturn : adultsReturn, childrenReturn : childrenReturn,  returnPeople : returnPeople, returnPrice : returnPrice };

	information = bookingData;

	return bookingData;
}

/* Update the UI adding the new calculated price */
function appendPrice() {

	$('#adults-n').empty();
	$('#children-n').empty();
	$('#total-n').empty();
	$('#price-n').empty();

	var bookingData = calculatePrice();

	var adultsString = '',
		childrenString = '',
		totalString = '',
		priceString = '';

	var returnDate = $('#returnDate').val();
	var totalPeopleA = bookingData.people,
		totalPeopleR = bookingData.returnPeople;

	if (returnDate != " " && returnDate != "" && returnDate != undefined){
		if (bookingData.adults != " " && bookingData.adultsReturn != " ") {
			adultsString = 'A: '+ bookingData.adults  + ', R: '+ bookingData.adultsReturn;
		}
		if (bookingData.children != " " && bookingData.childrenReturn != " ") {
			childrenString = 'A: '+ bookingData.children  + ', R: '+ bookingData.childrenReturn;
		}
		if (totalPeopleR != 0 && totalPeopleA != 0) {
			totalString = bookingData.price + '$, R: '+ totalPeopleR+ ' x '+ bookingData.returnPrice+'$';
		}
		var priceA = bookingData.price;
		var priceR = bookingData.returnPrice;
		var totalPrice = priceA + priceR;
		information.totalPrice = totalPrice;
		priceString = totalPrice + '$';

	}
	else {
		if (bookingData.adults != " "){
			adultsString = bookingData.adults;
		}
		if (bookingData.children  != " "){
			childrenString = bookingData.children;
		}
		if (totalPeopleA != 0) {
			totalString = bookingData.price +'$';
		}
		var priceA = bookingData.price;
		information.totalPrice = priceA;
		priceString = priceA + '$';
	}

	$('#adults-n').append(adultsString);
	$('#children-n').append(childrenString);
	$('#total-n').append(totalString);
	$('#price-n').append(priceString);

}

function checkStep1() {
	var date = $('#date').val();
	if(date != " " && date != "" && date != undefined) {
		return true;
	} else {
		return false;
	}
}

/* update the global var information with new data and send booking info to backend with ajax */
function sendBooking() {

	if ($('#address_2').val() == '') {
		toastr.warning($('#address_2 option:selected').text());
		return false;
	}
	
	information.pickupAddress = $('#address option:selected').text();
	information.dropoffAddress = $('#address_2 option:selected').text();
	information.transferType = true;
	information.date = $('#date').val();
	information.time = $('#time').val();
	information.returnDate = $('#returnDate').val();
	information.price = $('#price-n').val();
	information.people = $('#adults').val();
	information.children = $('#children').val();
	information.serviceQuantity = 1;
	information.transfer_type =  $("#transType").val();
	information.flight_train_no =  $("#flight_train_no").val();
	information.special_request =  $("#special_request").val();
	// information.infants =  $("#infants").val();
	// information.pets =  $("#pets").val();
	information.service_id =  $("#service_id").val();

  var returnDate = $('#returnDate').val();

  if (returnDate != " " && returnDate != "" && returnDate != undefined){
		if ($('#address_return').val() == '') {
			toastr.warning($('#address_return option:selected').text());
			return false;
		}
  	information.pickupReturnAddress = $('#address_return option:selected').text();
  	information.dropoffReturnAddress = $('#address_return_2 option:selected').text();
  	information.returnTime = $('#returnTime').val();
		information.serviceQuantity = 2;
		information.return_flight_train_no =  $("#return_flight_train_no").val();
  }
	
console.log(information);
// return;
	$.ajax({
        url: base_url+'/Cart/add',
        type: 'POST',
        cache: false,
        crossDomain: true,
        data : information,
        headers: { 'Access-Control-Allow-Origin': '*' },
        complete: function(response) {
					console.log(response)
          if(response) {
              try {
                  var data =  JSON.parse(response.responseText);
                  console.log(data);
                  if(data.success == true){
                    window.location = base_url+'Cart/index';
                  }
                  else {
                    toastr.warning(data.message);
                  }
              } catch(e) {
                  toastr.warning(e);

              }
          }
        }
    });

	// $.ajax({
  //       url: '/Cart/add',
  //       type: 'POST',
  //       cache: false,
  //       crossDomain: true,
  //       data : information,
  //       headers: { 'Access-Control-Allow-Origin': '*' },
  //       complete: function(response) {
  //       	if(response.responseText) {
  //       		window.location = '/Cart/index';
  //       	} else {
	// 					$('#fail-msg').fadeIn('slow','linear');
  //       	}
  //       }
  //   });
}
