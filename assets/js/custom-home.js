  $(function() {
        $(window).scroll(function() {
            if ($(".navbar").offset().top > 80) {
                $(".navbar-purity").addClass("sticky");
                $("#fixed-booking-form").addClass("sticky");
            }
            else {
                $(".navbar-purity").removeClass("sticky");
                $("#fixed-booking-form").removeClass("sticky");
            }
        });

    });
    
    $('#map-canvas').css({'background':"url('https://ressio.github.io/lazy-load-xt/dist/loading.gif') center center no-repeat"});    	
   
   function onLoadMap() {
   		$('#map-canvas').css({'background':'none'});
   }	