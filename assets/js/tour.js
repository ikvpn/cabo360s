var information = {};

$(document).ready(function(){
  $("input.time-pick").timepicker({minuteStep: 15,showInpunts: false, defaultTime: false});

  var d = new Date();

  var currDate = d.getDate();
  var currMonth = d.getMonth()+1;
  if(currMonth.toString().length == 1){
    currMonth = '0'+currMonth;
  }
  var currYear = d.getFullYear();

  var dateStr = currYear + "-" + currMonth + "-" + currDate;
  //console.log(dateStr);


  var lang = $('#lang').val();

  lang = lang == 'it' ? 'it-IT' : 'en';
    //console.log(lang);
  var startDateTour = $('#dateStart').val();
  
  var endDateTour = $('#dateEnd').val();
  var daysAvailability = $('#daysAvailability').val();
  var daysUnavailability = [];
  
  if(daysAvailability.length > 0) {
	  daysAvailability = daysAvailability.split(',');
	  for (var i = 0; i < daysAvailability.length; i++) {
	    daysAvailability[i] = (parseInt(daysAvailability[i])  + 1).toString();
	    if(  daysAvailability[i] == '7') {
	        daysAvailability[i] = '0';
	    }
	  }
	  for (var i = 0; i < 7; i++) {
	  
	        var day = i.toString();
	        //console.log(day);
	        if($.inArray(day, daysAvailability) == -1) {
	          daysUnavailability.push(day);
	        }
	    }
	  
  }

  $("input.date-pick").datepicker({format: "yyyy-mm-dd",
                                  autoclose: true,
                                  startDate: startDateTour,
                                  defaultViewDate: dateStr ,
                                  endDate : endDateTour,
                                  daysOfWeekDisabled : daysUnavailability,
                                  todayHighlight : true,
                                  weekStart : 1,
                                  language : lang
                                }).datepicker("setDate", "0");

  updatePrice();
});

$('#addTo').on('click', function(){
  sendBooking();
})

$('.inc').on('click', function() {
  var maxPass     = parseInt($("#maxParticipants").val());
  var adultsValue = parseInt($("#adults_count").val());
  var childsValue = parseInt($("#childs_count").val());
  var totalPerson = adultsValue + childsValue;
  if (maxPass < totalPerson) {
    var number  = parseInt($(this).prev( "input" ).val());
    $(this).prev( "input" ).val(number - 1);
    toastr.warning('Only '+maxPass+' passenger are allowed !');
  }else{
    if($(this).parent().hasClass('adults')){
      var adultsCount = $(this).prev().val();
      updateTable('adults', adultsCount)
    } else if($(this).parent().hasClass('child')) {
      var childrenCount = $(this).prev().val();
      updateTable('children', childrenCount)
    }
  }

});

$('.dec').on('click', function() {
  if($(this).parent().hasClass('adults')){
    var countValue = $(this).prev().prev().val();
    updateTable('adults', countValue);
  } else if($(this).parent().hasClass('child')) {
    var countValue = $(this).prev().prev().val();
    updateTable('children', countValue);
  }
});

function updateTable(div, value){
  $('#'+div).text(value);
  updatePrice();
}

// $("#setup_price").change(function() 
// {
//   updatePrice();
// });

function updatePrice(){
  // var setupPrice = $('#setup_price').val();
  // if (setupPrice == 1) 
  // {
  //   var total = $('#fixedPrice').val();
  //   $('#peopleAmount').text('$ '+ total);
  //   $('#total').text('$ '+total);
  // }
  // else
  // {
    var type = $('#sp').data('type');
    if(type == 'P'){
      var amount = $('#sp').val();
      // var aDiscount = parseInt($('#dc').data('adults'));
      // var cDiscount = parseInt($('#dc').data('children'));

      var adults = parseInt($('#adults').text());
      // if(aDiscount > 0) {
      //   var totalAdults = (adults * amount) - (adults * amount * aDiscount) / 100;
      // } else {
      //   var totalAdults = (adults * amount);
      // }

      var children = parseInt($('#children').text());
      // if(cDiscount > 0) {
      //   var totalChildren = (children * amount) - (children * amount * cDiscount) / 100;
      // } else {
      //   var totalChildren = (children * amount);
      // }
      var childPrice = $('#childPrice').val();
      var adultFinalPrice = amount  * adults;
      var childFinalPrice = childPrice  * children;
      var totalPersonPrice = adultFinalPrice + childFinalPrice;

      var people = adults + children;
      // var total = totalAdults + totalChildren;
      var total = totalPersonPrice;
      // $('#peopleAmount').text(people+' x $ '+ amount);
      $('#peopleAmount').text('$ '+ totalPersonPrice);
      $('#total').text('$ '+total);

    } else if(type == 'T') {
      var amount = $('#fixedPrice').val();
      $('#peopleAmount').text('$ '+ amount);
      $('#total').text('$ '+amount);
    }
  // }
}

function sendBooking() {
  
  var type = $('#sp').data('type');
  var tourPrice = parseInt($('#price').val());
  information.serviceId = $('#sId').val();
  information.serviceType = 'tour';
  information.tourType = true;
  information.serviceQuantity = parseInt($('#adults').text()) + parseInt($('#children').text());
  information.servicePrice = tourPrice;
  information.servicePriceUnit = type;
  information.checkIn = $('.date-pick').val();
  if($('.time-pick').val()){
    information.checkOut = $('.time-pick').val();
  }
  else {
    information.checkOut = $('#sessionHour').val();
  }
  information.people = parseInt($('#adults').text());
  information.children = parseInt($('#children').text());
  information.hotel_code = null;
  information.transferType = null;

	$.ajax({
        url: base_url+'/Cart/add',
        type: 'POST',
        cache: false,
        crossDomain: true,
        data : information,
        headers: { 'Access-Control-Allow-Origin': '*' },
        complete: function(response) {
					//console.log(response)
          if(response) {
              try {
                  var data =  JSON.parse(response.responseText);
                  //console.log(data);
                  if(data.success == true){
                    window.location = base_url+'/Cart/index';
                  }
                  else {
                    toastr.warning(data.message);
                  }
              } catch(e) {
                  toastr.warning(e);

              }
          }
        }
    });
}


;(function($){
	$.fn.datepicker.dates['it'] = {
		days: ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"],
		daysShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
		daysMin: ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa"],
		months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
		monthsShort: ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
		today: "Oggi",
		monthsTitle: "Mesi",
		clear: "Cancella",
		weekStart: 1,
		format: "dd/mm/yyyy"
	};
}(jQuery));
