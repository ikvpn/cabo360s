function selectFilter(type, description){
  $('#selectedSort').val(description)
  $('.listing-tour').each(function(i, obj) {
    var element_type = $(this).attr('data-type');
    if (type > 0) {
      if(parseInt(element_type) != type) {
        $(obj).addClass('hidden')
      }
      else {
        $(obj).removeClass('hidden')
      }
    }
    else {
      $(obj).removeClass('hidden')
    }
  });
}
