function modalRoom(div){
  $('.roomModal .modal-body').empty();
  var content = $('.'+div).html();
  $('.'+div).remove();
  $('.roomModal .modal-body').append('<div class="hidden '+div+'">'+content+'</div>');
  $('.listing-rooms .carousel-room-'+div).remove()
  $('.roomModal #carousel-room-'+div).carousel();
  $('.roomModal .modal-body .'+div).removeClass('hidden');
  $('.roomModal').modal('show');
  $('html, body').animate({
        scrollTop: 0
    }, 200);
}

$('.roomModal').on('hidden.bs.modal', function () {
  var actualDiv = $('.roomModal .modal-body').children(":first").attr('class');
  $('.roomModal .modal-body .'+actualDiv).addClass('hidden');
  var htmlDiv = $('.roomModal .modal-body').html();
  var baseDiv = actualDiv.replace('room-', '');
  $('#'+baseDiv).append(htmlDiv);
});
