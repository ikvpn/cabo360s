$(window).load(function() {
    $('.flexslider').flexslider({
        animation: "fade",
        animationLoop: true,
        controlNav: true,
        directionNav: false
    });

});

function NotBeforeToday(date) {
  console.log('fired')
  var now = new Date();//this gets the current date and time
  if (date.getFullYear() == now.getFullYear() && date.getMonth() == now.getMonth() && date.getDate() > now.getDate())
      return [true];
  if (date.getFullYear() >= now.getFullYear() && date.getMonth() > now.getMonth())
     return [true];
   if (date.getFullYear() > now.getFullYear())
     return [true];
  return [false];
}

$('input[name=datastart]').on('change', function(){
	$('input[name=dataend]').datepicker('setDate', $('input[name=datastart]').val());
	$('input[name=dataend]').datepicker('setStartDate', $('input[name=datastart]').val());
});

$('#hotelSearch input[name=start_date]').on('change', function(){
	$('#hotelSearch input[name=end_date]').datepicker('setDate', $('input[name=start_date]').val());
	$('#hotelSearch input[name=end_date]').datepicker('setStartDate', $('input[name=start_date]').val());
});

$(document).ready(function() {
  /* Lazy loading for Images */
  $('.lazy').lazyImage();

  var startDate = new Date();
  var start_day = startDate.getDate();
  var start_month = startDate.getMonth();
  var start_year = startDate.getFullYear();

  $('.ferrypicker').datepicker({ format: "yyyy-mm-dd", autoclose: true, zIndexOffset: 1001, todayBtn: true, todayHighlight: true,

    beforeShowDay: function (date){
      if (date.getMonth() == (new Date()).getMonth()){
        if (date.getDate() < start_day){
          return false;
        }
      }
    },
    beforeShowMonth: function (date){
      if (date.getMonth() < start_month) {
        return false;
      } else {
        return true;
      }
    },
    beforeShowYear: function (date){
      if (date.getFullYear() < start_year) {
        return false;
      }
    }

  });

	$('.datepicker').datepicker({ dateFormat: 'd-m-y', autoclose: true, zIndexOffset: 1001, todayBtn: true, todayHighlight: true,

    beforeShowDay: function (date){
      if (date.getMonth() == (new Date()).getMonth()){
        if (date.getDate() < start_day){
          return false;
        }
      }
    },
    beforeShowMonth: function (date){
      if (date.getMonth() < start_month) {
        return false;
      } else {
        return true;
      }
    },
    beforeShowYear: function (date){
      if (date.getFullYear() < start_year) {
        return false;
      }
    }

  });

  $('.datepicker').datepicker().on('changeDate', function (ev) {
         $(this).datepicker('hide');
    });

  $('#carousel_vertical_slide, #carousel-testimonial-1, #carousel-testimonial-2, #carousel_fade, #carousel_vertical_testimonial, #carousel_fade_icons, #carousel-support, #carousel_fade_2, #carousel_testimonial_2').carousel({
      interval: 3000
  });


  //Responsive Videos
  $("#main-media").fitVids();

  /* Mobile menu Fix by Davide MDSLab */
  var windowWidth = window.innerWidth;

  $( "iframe" ).each(function( index ) {
    	$(this).width(windowWidth - 40);
  });


  var prewSelected = null;

  $('#menu li a').on('click', function(e){

      var arrow = $(this).find('arrow');

       if (arrow != null && arrow != undefined) {
       	arrow = arrow.context.childNodes[3];

       	 if (arrow != null && arrow != undefined) {

       	 	var newIcon = '<i class="fa fa-angle-down fa-3" aria-hidden="true"></i>';
       	 	var oldIcon = '<i class="fa fa-angle-right fa-3" aria-hidden="true"></i>';

       	 	if (prewSelected != null && prewSelected != undefined && prewSelected.get(0) != $(this).get(0)){
       	 		var arrowPrew = prewSelected.find('arrow');
       	 		 arrowPrew = arrowPrew.context.childNodes[3];
       	 		 arrowPrew.innerHTML = oldIcon;
       	 	}

       	 	if (arrow.innerHTML == newIcon){
       	     	arrow.innerHTML = oldIcon;
       	     	prewSelected = null;
       	 	}
       	 	else {
       	 		arrow.innerHTML = newIcon;
       	 		prewSelected = $(this);
       	 	}
       	 }
       }
  });
  /*---END ----*/

});

/* Iframe Mozilla fix  */
var resize_count = 1;
var isFirefox = typeof InstallTrigger !== 'undefined';

$(window).resize(function FirefoxFix() {

	if(isFirefox){
		var booking_iframe = $('#hbb_iframe').height();
		var new_height = booking_iframe + 650;

		if(resize_count === 1) {
			$('#hbb_iframe').height(new_height);
			resize_count++;
		}
	}
});

var clickCheck = false,
	w = $(window).width(),
	h = $(window).height();

$('button.navbar-toggle').on('click', function() {
	if(!clickCheck) {
		$('body').addClass('noScroll');
		$('.megamenu-content').addClass('scrollableSubMenu');
		clickCheck = true;
	} else {
		$('body').removeClass('noScroll');
		$('.megamenu-content').removeClass('scrollableSubMenu');
		clickCheck = false;
	}

	if(w <= 480) {
		$('ul#menu').css('height', h+'px');
	}
});




$(".button_inc").on("click", function () {

	var $button = $(this);
	var oldValue = $button.parent().find("input").val();

	if ($button.text() == "+") {
	   	if(oldValue < 8){
	       	var newVal = parseFloat(oldValue) + 1;
		} else {
	        	newVal = 8;
	    }
	} else {
	        // Don't allow decrementing below zero
		if (oldValue > 1) {
			var newVal = parseFloat(oldValue) - 1;
		} else {
			newVal = 0;
		}
	}
	$button.parent().find("input").val(newVal);
});

$(".fancybox-media").fancybox({
    arrows: true,
    padding: 0,
    closeBtn: true,
    openEffect: 'fade',
    closeEffect: 'fade',
    prevEffect: 'fade',
    nextEffect: 'fade',
    helpers: {
        media: {},
        overlay: {
            locked: false
        },
        buttons: false,
        title: {
            type: 'inside'
        }
    },
    beforeLoad: function() {
        var el, id = $(this.element).data('title-id');
        if (id) {
            el = $('#' + id);
            if (el.length) {
                this.title = el.html();
            }
        }
    }
});
