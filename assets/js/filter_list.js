$('#tags').on('change', function(){
  var tag_selected = $(this).val();

  $('.listing-item').each(function(i, obj) {
    if(tag_selected > -1){
      var tags = $(this).attr('data-tag');
      tags = replaceAll(tags, " ", "");
      tags = tags.split(",");

      if($.inArray(tag_selected, tags) == -1){
        $(obj).addClass('hidden');
      }
      else {
        $(obj).removeClass('hidden');
      }
    }
    else {
      $(obj).removeClass('hidden');
    }
  });
});

$('#services').on('change', function(){
  var service_selected = $(this).val();

  $('.listing-item').each(function(i, obj) {
    if(service_selected > -1){
      var services = $(this).attr('data-services');
      services = replaceAll(services, " ", "");
      services = services.split(",");

      if($.inArray(service_selected, services) == -1){
        $(obj).addClass('hidden');
      }
      else {
        $(obj).removeClass('hidden');
      }
    }
    else {
      $(obj).removeClass('hidden');
    }
  });
});


function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}
