$(document).ready(function() {
    $('#daysAvailability').multiselect({
      onChange: function(option, checked, select) {

        var selection = $('#daysAvailability').val();
        if(selection != null){
          selection = selection.toString();
          $('#selected_days').val(selection);
        }
        else {
          $('#selected_days').val('');
        }

      }
    });

     $('#pickup').multiselect({
      numberDisplayed: 1,
      onChange: function(option, checked, select) {

        var selection = $('#pickup').val();
        if(selection != null){
          selection = selection.toString();
          $('#selected_pickup').val(selection);
        }
        else {
          $('#selected_pickup').val('');
        }

      }
    });

     $('#dropoff').multiselect({
       numberDisplayed: 1,
      onChange: function(option, checked, select) {

        var selection = $('#dropoff').val();
        if(selection != null){
          selection = selection.toString();
          $('#selected_dropoff').val(selection);
        }
        else {
          $('#selected_dropoff').val('');
        }

      }
    });

    $(".slider-range").slider({
        range: true,
        min: 0,
        max: 1440,
        step: 15,
        values: [600, 720],
        slide: function (e, ui) {
            var id = $(this).attr('id');
            var hours1 = Math.floor(ui.values[0] / 60);
            var minutes1 = ui.values[0] - (hours1 * 60);

            if (hours1.toString().length == 1) {
              hours1 = '0' + hours1;
            }
            if (minutes1.toString().length == 1) {

              minutes1 = '0' + minutes1;
            }

            if (minutes1 == 0) minutes1 = '00';
            if (hours1 >= 23) {
                if (hours1 == 23) {
                    hours1 = hours1;
                    minutes1 = minutes1 ;
                } else {
                    hours1 = hours1 - 23;
                    minutes1 = minutes1;
                }
            } else {
                hours1 = hours1;
                minutes1 = minutes1;
            }
            if (hours1 == 0) {
                hours1 = '00';
                minutes1 = minutes1;
            }

          var time1 = hours1 + ':' + minutes1;

            var hours2 = Math.floor(ui.values[1] / 60);
            var minutes2 = ui.values[1] - (hours2 * 60);

            if (hours2.toString().length == 1) {

                hours2 = '0' + hours2;
            }
            if (minutes2.toString().length == 1) {

              minutes2 = '0' + minutes2;
            }
            if (minutes2 == 0) minutes2 = '00';
            if (hours2 >= 23) {
                if (hours2 == 23) {
                    hours2 = hours2;
                    minutes2 = minutes2;
                } else {
                    hours2 = 23;
                    minutes2 = "59";
                }
            } else {
                hours2 = hours2;
                minutes2 = minutes2;
            }
            if (hours2 == 0) {
                hours2 = '00';
                minutes2 = minutes2;
            }

            var time2 = hours2 + ':' + minutes2;
            $('#sessionHour'+id).val(time1+' - '+time2);
            $('#sessionHourH'+id).val(time1+' - '+time2);
        }
    });



});
$(document).on('keyup keypress', 'form input[type="text"]', function(e) {
  if(e.which == 13) {
    e.preventDefault();
    return false;
  }
});
