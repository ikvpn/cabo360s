$(document).ready(function() {
    $('#categories').multiselect({
      onChange: function(option, checked, select) {
        selectMainCategory();
        var selection = $('#categories').val();
        selection = selection.toString();
        $('#selected_categories').val(selection);
      }
    });

    $('#creditCards').multiselect({
      onChange: function(option, checked, select) {
        var selection = $('#creditCards').val();
        if(selection){
          selection = selection.toString();
          selection = replaceAll(selection, ',', ', ');
          $('#creditCards_selected').val(selection);
        }
        else{
          $('#creditCards_selected').val('');
        }

      }
    });

    $('#tags').multiselect({
      onChange: function(option, checked, select) {
        var selection = $('#tags').val();
        if(selection){
          console.log(selection);
          selection = selection.toString();
          selection = replaceAll(selection, ',', ', ');
          $('#selected_tags').val(selection);
        }
        else {
          $('#selected_tags').val('');
        }

      }
    });
    $('#activity_services').multiselect({
      onChange: function(option, checked, select) {
        var selection = $('#activity_services').val();
        if(selection){
          selection = selection.toString();
          selection = replaceAll(selection, ',', ', ');
          $('#selected_services').val(selection);
        }
        else{
            $('#selected_services').val('');
        }
      }
    });

    var open = $('#_openHours').val() != '' ? $('#_openHours').val() : '00:00' ;
    var closed = $('#_closeHours').val() != '' ? $('#_closeHours').val() : '00:00' ;
    var open2 = $('#_openHours2').val() != '' ? $('#_openHours2').val() : '00:00' ;
    var closed2 = $('#_closeHours2').val() != '' ? $('#_closeHours2').val() : '00:00' ;

    $('#openHour').timepicker({
      showSeconds : false,
      showMeridian : false,
      defaultTime : open,
      minuteStep : 1
    }).on('changeTime.timepicker', function(e) {
        var openHour = e.time.value;

        console.log(openHour);
        var closeHour = $('#closeHour').val();

        var hours = openHour + ' - ' + closeHour;

        $('#hours_').val(hours);
    });

    $('#closeHour').timepicker({
      showSeconds : false,
      showMeridian : false,
      defaultTime : closed,
      minuteStep : 1
    }).on('changeTime.timepicker', function(e) {
        var openHour = $('#openHour').val();
        var closedHour = e.time.value;
        console.log(closedHour);
        var hours = openHour + ' - ' + closedHour;
        $('#hours_').val(hours);
    });


    $('#openHour2').timepicker({
      showSeconds : false,
      showMeridian : false,
      defaultTime : open2,
      minuteStep : 1
    }).on('changeTime.timepicker', function(e) {
        var openHour = e.time.value;

        console.log(openHour);
        var closeHour = $('#closeHour2').val();

        var hours = openHour + ' - ' + closeHour;

        $('#hours2_').val(hours);
    });
    $('#closeHour2').timepicker({
      showSeconds : false,
      showMeridian : false,
      defaultTime : closed2,
      minuteStep : 1
    }).on('changeTime.timepicker', function(e) {
        var openHour = $('#openHour2').val();
        var closedHour = e.time.value;
        console.log(closedHour);
        var hours = openHour + ' - ' + closedHour;
        $('#hours2_').val(hours);
    });


});

function selectMainCategory(){
  var main_category = $('#main_category').val();
  $('#categories').multiselect('select', [main_category]);
}

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}
