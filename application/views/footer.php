<!--################ FOOTER START ################-->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <ul class="footer-list list-unstyled">
                    <li class="heading"><?php echo lang("What_to_visit"); ?></li>
                    <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('locations') . '.html' ?>"><?php echo lang('Locations') ?></a></li>
                    <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('places-of-interest') . '.html' ?>"><?php echo lang("Places_of_Interest") ?></a></li>
                    <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('beach') . '.html' ?>"><?php echo lang("Beaches"); ?></a></li>
                    <li><a href="#">Mornings</a></li>
                </ul>
                <ul class="footer-list list-unstyled paddingtop10">
                    <li class="heading"><?php echo lang('Where_to_stay'); ?></li>
                    <?php if (isset($menu_items) && !empty($menu_items)): ?>
                        <?php foreach ($menu_items as $item): ?>
                            <?php if ($item->category == 'where_to_stay'): ?>
                                <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('where-to-stay') . '/' . str_replace(' ', '-', strtolower($item->enName)); ?>.html"><?php echo $item->name; ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="col-md-2">
                <ul class="footer-list list-unstyled ">
                    <li class="heading"><?php echo lang('Eat_and_drink'); ?></li>
                    <?php if (isset($menu_items) && !empty($menu_items)): ?>
                        <?php foreach ($menu_items as $item): ?>
                            <?php if ($item->category == 'eat_and_drink'): ?>
                                <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('eat-and-drink') . '/' . str_replace(" ", "-", strtolower($item->enName)); ?>.html"><?php echo $item->name; ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
                <ul class="footer-list list-unstyled paddingtop10">
                    <li class="heading"><?php echo lang('What_to_do'); ?></li>
                    <?php if (isset($menu_items) && !empty($menu_items)): ?>
                        <?php foreach ($menu_items as $item): ?>
                            <?php if ($item->category == 'what_to_do' && $item->name != 'Watersports'): ?>
                                <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('what-to-do') . '/' . str_replace(" ", "-", strtolower($item->enName)); ?>.html"><?php echo $item->name; ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if (isset($menu_items) && !empty($menu_items)): ?>
                        <?php foreach ($menu_items as $item): ?>
                            <?php if ($item->category == 'events'): ?>
                                <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('events') . '/' . str_replace(" ", "-", strtolower($item->enName)); ?>.html"><?php echo $item->name; ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>

            </div>
            <div class="col-md-2">
                   <ul class="footer-list list-unstyled">
                    <li class="heading">Ferry timetable's</li>
                     <li><a href="http://www.visitprocida.it/it/traghetti-napoli-procida.html">Napoli - Procida</a></li>
                      <li><a href="http://www.visitprocida.it/it/traghetti-procida-napoli.html">Procida - Napoli</a></li>
                      <li><a href="http://www.visitprocida.it/it/traghetti-pozzuoli-procida.html">Pozzuoli - Procida</a></li>
                      <li><a href="http://www.visitprocida.it/it/traghetti-procida-pozzuoli.html">Procida - Pozzuoli</a></li>
                      <li><a href="http://www.visitprocida.it/it/traghetti-ischia-procida.html">Ischia - Procida</a></li>
                      <li><a href="http://www.visitprocida.it/it/traghetti-procida-ischia.html">Procida - Ischia</a></li>
                </ul>
                <ul class="footer-list list-unstyled paddingtop10">
                    <li class="heading"><?php echo lang('Articles'); ?></li>
                    <?php if(isset($articles) && count($articles)>0): ?>
                    <?php foreach($articles as $post): ?>
                      <li><a href="<?php echo base_url() . 'blog/post/' . $post->id.'/'.date('Y/m/d').'/'.  urlencode(str_replace(' ','-',$post->title)).'.html'; ?>"><?php echo $post->title; ?></a></li>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="footer-list list-unstyled">
                    <li class="heading"><?php echo lang("About-us");?></li>
                    <li><a href="#"><?php echo lang('Our-Team');?></a></li>
                    <li><a href="#"><?php echo lang("Add-your-business");?></a></li>
                    <li><a href="#"><?php echo lang("Prices");?></a></li>
                </ul>
            </div>
            <div class="col-md-3">

                <img src="<?php echo base_url() ?>assets/images/logo.png">
                <p class="copyright">© <script>document.write(new Date().getFullYear())</script> Joy Holidays Group
                    <br>Via Flavio Gioia, 13 - Procida, Naples, Italy
                    <br>p.iva 07207821211</p>
                <br>
                <ul class="list-inline">
                    <li><a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin fa-2x"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                    </li>

                </ul>
                <br>

                <div class="input-group col-md-">
                    <input type="text" class="form-control" placeholder="Newsletter">
                    <span class="input-group-btn">
                        <button class="btn  btn-newsletter" type="button"><i class="fa fa-angle-right"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">

                    <ul class="list-inline footer-bottom-menu">
                        <li><a href="<?php echo base_url() . $this->lang->lang(); ?>">home</a>
                        </li>
                        <li>|</li>
                        <li><a href="#">about</a>
                        </li>
                        <li>|</li>
                        <li><a href="http://www.visitprocida.it/it/dove-dormire/hotels.html">hotel procida</a>
                        </li>
                        <li>|</li>
                        <li><a href="#">special offers</a>
                        </li>
                        <li>|</li>
                        <li><a href="<?php echo base_url() . $this->lang->lang() . '/blog' ?>.html"><?php echo lang('Blog'); ?></a>
                        </li>
                        <li>|</li>
                        <li><a href="#">contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<!--################ FOOTER END ################-->
<!--################ JAVASCRIPTS ################-->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/modernizr.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.fitvids.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo base_url(); ?>assets/assets/twitter/jquery.tweet.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox-thumbs.js?v=1.0.2"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox-media.js?v=1.0.0"></script>
<script src="<?php echo base_url(); ?>assets/js/stellar.js"></script>
<script src="<?php echo base_url(); ?>assets/js/nicescroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.isotope.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom-home.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portfolio.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
                    // $(document).load(function(){
                    // $('.datepicker').datepicker({
                    //     autoclose: true,
                    //     zIndexOffset: 1001
                    // });
                    // });
</script>
<!-- ajax contact form script -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mail.js"></script>
<!-- Preloader script -->
<script>
                    $(window).load(function () {
                        "use strict";
                        $('.preloading').fadeOut();
                    });
</script>
<?php if (isset($jsfiles)): ?>
    <?php foreach ($jsfiles as $js): ?>
        <?php if ($js['type'] == 'file'): ?>
            <script src="<?php echo $js['src'] ?>" type="text/javascript"></script>
        <?php else: ?>
            <script type="text/javascript">
            <?php echo $js['src'] ?>
            </script>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
<script type="text/javascript">
    function booking_search_menu() {
        checkin = document.getElementById('bs_checkin_date_menu').value;
        if (checkin == '') {
            alert("Checkin Date is required field");
            return false;
        } else if (!isValidDate(checkin)) {
            alert("Please enter check in date in MM/DD/YYYY format");
            return false;
        }
        checkout = document.getElementById('bs_checkout_date_menu').value;
        if (checkout == '') {
            alert('Checkout Date is required field');
            return false;
        } else if (!isValidDate(checkout)) {
            alert("Please enter check out date in MM/DD/YYYY format");
            return false;
        }
        var r = document.getElementById("bs_rooms_menu");
        rooms = r.options[r.selectedIndex].value;
        if (rooms == '' || rooms == '0') {
            alert('Rooms is a required field');
            return false;
        }

        var parts = checkin.split("/");
        var checkin_day = parseInt(parts[1], 10);
        var checkin_month = parseInt(parts[0], 10);
        var checkin_year = parseInt(parts[2], 10);

        var parts = checkout.split("/");
        var checkout_day = parseInt(parts[1], 10);
        var checkout_month = parseInt(parts[0], 10);
        var checkout_year = parseInt(parts[2], 10);

        window.open('http://www.booking.com/searchresults.html?sid=5b818543a2ad751a35cf1d134baee464&dest_id=2219&dest_type=region&checkin_monthday=' + checkin_day + '&checkin_year_month=' + checkin_month + '-' + checkin_year + '&checkout_monthday=' + checkout_day + '&checkout_year_month=' + checkout_month + '-' + checkout_year + '&aid=363714', '_target');
    }

    function isValidDate(dateString) {
        // First check for the pattern
        if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
            return false;

        // Parse the date parts to integers
        var parts = dateString.split("/");
        var day = parseInt(parts[1], 10);
        var month = parseInt(parts[0], 10);
        var year = parseInt(parts[2], 10);

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12)
            return false;

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        return day > 0 && day <= monthLength[month - 1];
    }
</script>
<script>

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {

            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)

    })(window, document, 'script', '<?php echo base_url(); ?>assets/js/analytics.js', 'ga');

    ga('create', 'UA-57539191-1', 'auto');

    ga('send', 'pageview');

</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Richiesta informazioni</h4>
            </div>
            <div class="modal-body">
                <form action="#" method="post"  id="form">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nome*</label>
                                <input type="text" name="nome" id="nome" placeholder="Nome" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Cognome*</label>
                                <input type="text" name="cognome" id="cognome" placeholder="Cognome" class="form-control" />
                            </div>
                            <div class="form-group"><label>Indirizzo Email*</label>
                                <input type="text" name="email" id="email" placeholder="Indirizzo Email" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Indirizzo</label>
                                <input type="text" name="indirizzo" id="indirizzo" placeholder="Via, Piazza, Civico" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Città/CAP*</label>
                                <input type="text" name="cap" id="cap" placeholder="Città, CAP" class="form-control" /></div><div class="form-group">
                                <label>Tipologia richiesta
                                    <select name="tipoScelto0" id="tipoScelto0" class="form-control">
                                        <option value="scegli">scegli</option>
                                        <option value="informazioni generiche">informazioni generiche</option>
                                        <option value="disponibilità alloggi">disponibilità alloggi</option>
                                        <option value="come raggiungerci">come raggiungerci</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Messaggio</label>
                                <textarea name="richiesta" id="richiesta" placeholder="Message" class="form-control" style="height: 120px;"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-purity">Invia richiesta</button>
            </div>
        </div>
    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    var LANG = '<?php echo $this->lang->lang(); ?>';
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    function validate(data) {
        valid = true;
        if (data.firstname.length < 1) {
            decorateElement("#firstname", true, LANG == "en" ? "First name is required field." : "Nome è richiesto Campi.");
            valid = false;
        } else {
            decorateElement("#firstname", false, "");
        }

        if (data.lastname.length < 1) {
            decorateElement("#lastname", true, LANG == "en" ? "Last name is required field." : "Cognome è richiesto Campi.");
            valid = false;
        } else {
            decorateElement("#lastname", false, "");
        }

        if (data.email.length < 1) {
            decorateElement("#email-se", true, LANG == "en" ? "Email is required field." : "Email è richiesta campo.");
            valid = false;
        } else if (!validateEmail(data.email)) {
            decorateElement("#email-se", true, LANG == "en" ? "Please enter valid email address." : "Inserisci indirizzo email valido.");
            valid = false;
        } else {
            decorateElement("#email-se", false, "");
        }

        if (data.message.length < 1) {
            decorateElement("#message", true, LANG == "en" ? "Message is required field." : "Messaggio è richiesto Campi.");
            valid = false;
        } else {
            decorateElement("#message", false, "");
        }

        if (data.subject.length < 1) {
            decorateElement("#subject-se", true, LANG == "en" ? "Subject is required field." : "Soggetto è richiesto Campi.");
            valid = false;
        } else {
            decorateElement("#subject-se", false, "");
        }
        if (data.g_recaptcha_response.length < 1) {
            decorateElement("#g-recaptcha", true, LANG == "en" ? "Please verify you are not robot." : "Si prega di verificare che non sono robot.");
            valid = false;
        } else {
            decorateElement("#g-recaptcha", false, "");
        }
        return valid;
    }

    function resetSendMailForm() {
        $('#sendmail-form')[0].reset();
        decorateElement("#g-recaptcha", false, "");
        decorateElement("#subject-se", false, "");
        decorateElement("#message", false, "");
        decorateElement("#email-se", false, "");
        decorateElement("#lastname", false, "");
        decorateElement("#firstname", false, "");
    }
    function decorateElement(id, error, message) {
        if (error) {
            if (!$(id).next().is("span")) {
                $(id).closest('div[class^="form-group"]').addClass("has-error");
                $(id).after(function () {
                    return "<span style='color:red'><small>" + message + "</small></span>";
                });
            }
        } else if ($(id).next().is("span")) {
            $(id).closest('div[class^="form-group"]').removeClass("has-error");
            $(id).next("span").remove();
        }
    }

    $(document).ready(function () {
        $(".btn-sendemail").click(function () {
            $("#sendemail-orderid").val($(this).attr("data-orderid"));
            $("#sendemail-type").val($(this).attr("data-type"));
            $("#sendMail").modal("show");
        });

        $('#btn-sendemail').on('click', function () {
            var formdata = {
                firstname: $.trim($("#sendmail-form input[name=firstname]").val()),
                lastname: $.trim($("#sendmail-form input[name=lastname]").val()),
                email: $.trim($("#sendmail-form input[name=email]").val()),
                subject: $.trim($("#sendmail-form select[name=subject]").val()),
                message: $.trim($("#sendmail-form textarea[name=message]").val()),
                orderid: $.trim($("#sendmail-form input[name=orderid]").val()),
                type: $.trim($("#sendmail-form input[name=type]").val()),
                g_recaptcha_response: $("#sendmail-form textarea[name=g-recaptcha-response]").val(),
                page_url: window.location.href
            };
            if (validate(formdata)) {
                $(".preloading").show();
                $.post('<?php echo base_url(); ?>ajax/sendmail',
                        {firstname: formdata.firstname,
                            lastname: formdata.lastname,
                            email: formdata.email,
                            subject: formdata.subject,
                            message: formdata.message,
                            orderid: formdata.orderid,
                            type: formdata.type,
                            g_recaptcha_response: formdata.g_recaptcha_response,
                            page_url: formdata.page_url
                        },
                function (response) {
                    grecaptcha.reset();
                    console.log(response);
                    if (response.status == 200) {
                        resetSendMailForm();
                        $("#sendMail").modal("hide");
                        $(".preloading").hide();
                        alert(LANG == "en" ? "Success, your message is successfully sent to the bussiness owner." : "Successo , il messaggio viene inviato con successo al proprietario bussiness.");
                    } else {
                        $(".preloading").hide();
                        alert("Error: " + response.content);
                    }
                    $(".preloading").hide();
                }
                );
            }
        });
    });


</script>
<!-- Send Modal -->
<div class="modal fade" id="sendMail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" method="post" id="sendmail-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo lang("Request_Information"); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" value="0" name="orderid" id="sendemail-orderid">
                    <input type="hidden" value="type" name="type" id="sendemail-type">
                    <div class="row">
                        <div class="col-xs-12 col-md-5">
                            <div class="form-group">
                                <label><?php echo lang("First_Name") ?></label>
                                <input type="text" name="firstname" id="firstname" placeholder="<?php echo lang("First_Name") ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label><?php echo lang("Last_Name") ?></label>
                                <input type="text" name="lastname" id="lastname" placeholder="<?php echo lang("Last_Name") ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label><?php echo lang("Email_Address") ?></label>
                                <input type="text" name="email" id="email-se" placeholder="<?php echo lang("Email_Address") ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label><?php echo lang("Subject") ?></label>
                                <select name="subject" id="subject-se" class="form-control">
                                    <option value="General Informations"><?php echo lang("General_Informations") ?></option>
                                    <option value="Reservations"><?php echo lang("Reservation") ?></option>
                                    <option value="Others"><?php echo lang("Other") ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <div class="form-group">
                                <label><?php echo lang("Message"); ?></label>
                                <textarea name="message" id="message" placeholder="<?php echo lang("Message"); ?>" style="min-height: 182px; max-height: 182px; max-width: 100%" class="form-control" style="height: 120px;"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="g-recaptcha" id="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_SITE_KEY; ?>"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-purity" id="btn-sendemail"><?php echo lang("Send") ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
