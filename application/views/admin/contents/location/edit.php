<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-location-arrow"></i> Modifica località</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php echo form_open(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $location->title_en; ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="menu2" class="tab-pane fade in active">
                            <br>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">Titolo [it]</label>
                                    <input type="text" class="form-control" id="title" name="title_it" value="<?php echo isset($location->title_it) ? $location->title_it : ''; ?>">
                                </div>
                            </div>
                             <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">Sottotitolo [it]</label>
                                    <input type="text" class="form-control" id="sottotitolo_it" name="sottotitolo_it" value="<?php echo isset($location->sottIt) ? $location->sottIt : ''; ?>">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="it_editor">Descrizione [it]</label>
                                    <textarea id="it_editor" name="description_it" rows="10" cols="80"><?php echo isset($location->description_it) ? base64_decode($location->description_it) : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_it">Meta Keywords[it]</label>
                                    <input type="text" class="form-control" id="meta_keywords_it" name="meta_keywords_it" value="<?php echo $location->meta_keywords_it ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_it">Meta Title [it]</label>
                                    <input type="text" class="form-control" id="meta_it" name="meta_it" value="<?php echo $location->meta_it; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_it">URL rewrite [it] <small style="color:gray"><?php echo base_url().$this->lang->lang().'/what_to_visit/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_it" name="rewrite_url_it" value="<?php echo $location->rewrite_url_it; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_it">Meta Description [it]</label>
                                    <textarea class="form-control" id="meta_desc_it" style="height:180px" name="meta_desc_it"><?php echo $location->meta_desc_it; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">Titolo [en]</label>
                                    <input type="text" class="form-control" id="title" name="title_en" value="<?php echo $location->title_en; ?>">
                                </div>
                            </div>
                             <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">Sottotitolo [en]</label>
                                    <input type="text" class="form-control" id="sottotitolo_en" name="sottotitolo_en" value="<?php echo isset($location->sottEn) ? $location->sottEn : ''; ?>">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="en_editor">Descrizione [en]</label>
                                    <textarea id="en_editor" name="description_en" rows="10" cols="80"><?php echo isset($location->description_en) ? base64_decode($location->description_en) : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_en">Meta Keywords [en]</label>
                                    <input type="text" class="form-control" id="meta_keywords_en" name="meta_keywords_en" value="<?php echo $location->meta_keywords_en; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_en">Meta Title [en]</label>
                                    <input type="text" class="form-control" id="meta_en" name="meta_en" value="<?php echo $location->meta_en; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_en">URL rewrite [en] <small style="color:gray"><?php echo base_url().$this->lang->lang().'/what_to_visit/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_en" name="rewrite_url_en" value="<?php echo $location->rewrite_url_en ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_en">Meta Description [en]</label>
                                    <textarea class="form-control" id="meta_desc_en" style="height:180px" name="meta_desc_en"><?php echo $location->meta_desc_en; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">Titolo [de]</label>
                                    <input type="text" class="form-control" id="title" name="title_du" value="<?php echo isset($location->title_du) ? $location->title_du : ''; ?>">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="du_editor">Descrizione [de]</label>
                                    <textarea id="du_editor" name="description_du" rows="10" cols="80"><?php echo isset($location->description_du) ? base64_decode($location->description_du) : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_du">Meta Keywords [de]</label>
                                    <input type="text" class="form-control" id="meta_keywords_du" name="meta_keywords_du" value="<?php echo $location->meta_keywords_du; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_du">Meta Title [de]</label>
                                    <input type="text" class="form-control" id="meta_du" name="meta_du" value="<?php echo $location->meta_du; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_du">URL rewrite [de] <small style="color:gray"><?php echo base_url().$this->lang->lang().'/what_to_visit/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_du" name="rewrite_url_du" value="<?php echo $location->rewrite_url_du ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_du">Meta Description [de]</label>
                                    <textarea class="form-control" id="meta_desc_du" style="height:180px" name="meta_desc_du"><?php echo $location->meta_desc_du; ?></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Latitudine</label>
                            <input type="text" class="form-control" id="lat" name="lat" value="<?php echo $location->lat; ?>">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Longitudine</label>
                            <input type="text" class="form-control" id="lng" name="lng" value="<?php echo $location->lng; ?>">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group" style="padding-top:30px;">
                            <a class="btn btn-primary btn-xs" href="http://www.gps-coordinates.net/" target="_blank" >Trova Lat/Lng</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer" style="text-align: right">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                    <a href ="#" class="btn btn-danger"><i class="fa fa-times"></i> Annulla</a>
                </div>
            </div>
            <!-- /.panel -->
            <?php echo form_close(); ?>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-4">
            <?php echo form_open_multipart('admin/location/updatethumbnail'); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Carica la Thumbnail
                </div>
                <div class="panel-body">
                    <input type='hidden' name="id" value="<?php echo $location->id ?>">
                    <div class="form-group">
                        <label class="control-label" for="image"> Thumbnail</label>
                        <input type="file" id="image" name="p_thumbnail">
                    </div>
                    <?php if (strlen($location->p_image) > 0): ?>
                        <img class="img-thumbnail" width="180" src="<?php echo base_url() . 'uploads/locations/' . $location->p_image; ?>">
                    <?php else: ?>
                        <br>
                         <small>
                            <strong><i class="fa fa-info-circle"></i> Istruzioni per l'upload</strong><br>
                            Deve essere un file di immagine valido con estensione di jpg, jpeg, png, gif con dimensioni appropriate e dimensioni inferiori a 3 MB.
                        </small>
                    <?php endif; ?>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="col-lg-8">
            <?php echo form_open_multipart('admin/location/updatevideourl'); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    URL del video
                </div>
                <div class="panel-body">
                    <input type='hidden' name="id" value="<?php echo $location->id ?>">
                    <div class="col-xs-12 col-lg-6">
                        <div class="form-group">
                            <label class="control-label" for="image">URL di Youtube del video incorporato</label>
                            <input type="url" id="image" class="form-control" name="v_url" value="<?php echo $location->v_url ?>">
                        </div>
                        <small>
                            <strong><i class="fa fa-info-circle"></i> Istruzioni per l'URL del video</strong><br>
                            La URL del video deve essere una URL valida di un video incorporato di Youtube. (es. https://www.youtube.com/embed/Mw6EHvXpUOI)
                        </small>
                    </div>
                    <div class="col-xs-12 col-lg-6">
                        <iframe height="163" width="100%" src="<?php echo $location->v_url ?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Immagini per lo slider
                </div>
                <div class="panel-body">
                    <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                        <!-- Redirect browsers with JavaScript disabled to the origin page -->
                        <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                        <div class="row fileupload-buttonbar">
                            <div class="col-lg-7">
                                <!-- The fileinput-button span is used to style the file input field as button -->
                                <span class="btn btn-success fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Aggiungi file...</span>
                                    <input type="file" name="files[]" multiple>
                                </span>
                                <button type="submit" class="btn btn-primary start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Inizia upload</span>
                                </button>
                                <button type="reset" class="btn btn-warning cancel">
                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                    <span>Annulla upload</span>
                                </button>
                                <button type="button" class="btn btn-danger delete">
                                    <i class="glyphicon glyphicon-trash"></i>
                                    <span>Elimina</span>
                                </button>
                                <input type="checkbox" class="toggle">
                                <!-- The global file processing state -->
                                <span class="fileupload-process"></span>
                            </div>
                            <!-- The global progress state -->
                            <div class="col-lg-5 fileupload-progress fade">
                                <!-- The global progress bar -->
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                </div>
                                <!-- The extended global progress state -->
                                <div class="progress-extended">&nbsp;</div>
                            </div>
                        </div>
                        <!-- The table listing the files available for upload/download -->
                        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                    </form>
                    <!-- The blueimp Gallery widget -->
                    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                        <div class="slides"></div>
                        <h3 class="title"></h3>
                        <a class="prev">‹</a>
                        <a class="next">›</a>
                        <a class="close">×</a>
                        <a class="play-pause"></a>
                        <ol class="indicator"></ol>
                    </div>
                    <!-- The template to display files available for upload -->
                    <script id="template-upload" type="text/x-tmpl">
                        {% for (var i=0, file; file=o.files[i]; i++) { %}
                        <tr class="template-upload fade">
                        <td>
                        <span class="preview"></span>
                        </td>
                        <td>
                        <p class="name">{%=file.name%}</p>
                        <strong class="error text-danger"></strong>
                        </td>
                        <td>
                        <p class="size">In elaborazione...</p>
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                        </td>
                        <td>
                        {% if (!i && !o.options.autoUpload) { %}
                        <button class="btn btn-primary start" disabled>
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Inizia</span>
                        </button>
                        {% } %}
                        {% if (!i) { %}
                        <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Annulla</span>
                        </button>
                        {% } %}
                        </td>
                        </tr>
                        {% } %}
                    </script>
                    <!-- The template to display files available for download -->
                    <script id="template-download" type="text/x-tmpl">
                        {% for (var i=0, file; file=o.files[i]; i++) { %}
                        <tr class="template-download fade">
                        <td>
                        <span class="preview">
                        {% if (file.thumbnailUrl) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                        {% } %}
                        </span>
                        </td>
                        <td>
                        <p class="name">
                        {% if (file.url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                        {% } else { %}
                        <span>{%=file.name%}</span>
                        {% } %}
                        </p>
                        {% if (file.error) { %}
                        <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                        {% } %}
                        </td>
                        <td>
                        <span class="size">{%=o.formatFileSize(file.size)%}</span>
                        </td>
                        <td>
                        {% if (file.deleteUrl) { %}
                        <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Elimina</span>
                        </button>
                        <input type="checkbox" name="delete" value="1" class="toggle">
                        {% } else { %}
                        <button class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Annulla</span>
                        </button>
                        {% } %}
                        </td>
                        </tr>
                        {% } %}
                    </script>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var BASE_URL = '<?php echo base_url(); ?>';
        var UPLOAD_URL = BASE_URL + 'admin/location/fileupload/<?php echo $location->id ?>';
    </script>
