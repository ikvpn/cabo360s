<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Nuova Località</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dettagli
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php echo form_open_multipart(); ?>
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="menu2" class="tab-pane fade in active">
                            <br>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="title_it">Titolo [it]</label>
                                    <input type="text" class="form-control" id="title_it" name="title_it" value="<?php echo set_value('title_it'); ?>">
                                </div> <div class="form-group">
                                    <label class="control-label" for="title_it">Sottotitolo [it]</label>
                                    <input type="text" class="form-control" id="sottotitolo_it" name="sottotitolo_it" value="<?php echo set_value('sottotitolo_it'); ?>">
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="control-label">Descrizione [it]</label>
                                    <textarea id="it_editor" name="description_it" rows="10" cols="80" placeholder="Enter Details in Italian"><?php echo set_value('description_it') ?></textarea>
                                </div>
                                <div class="clearfix"></div><br>
                                 <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="control-label" for="meta_keywords_it">Meta Keywords[it]</label>
                                        <input type="text" class="form-control" id="meta_keywords_it" name="meta_keywords_it" value="<?php echo set_value('meta_keywords_it'); ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="meta_it">Meta Title [it]</label>
                                        <input type="text" class="form-control" id="meta_it" name="meta_it" value="<?php echo set_value('meta_it'); ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="rewrite_url_it">URL rewrite [it] <small style="color:gray"><?php echo base_url().'it/what_to_visit/[rewrite_url].html' ?></small></label>
                                        <input type="text" class="form-control" id="rewrite_url_it" placeholder="[rewrite_url]" name="rewrite_url_it" value="<?php echo set_value('rewrite_url_it'); ?>">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="control-label" for="meta_desc_it">Meta Description [it]</label>
                                        <textarea class="form-control" id="meta_desc_it" style="height:180px" name="meta_desc_it"><?php echo set_value('meta_desc_it'); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="title_en">Titolo [en]</label>
                                    <input type="text" class="form-control" id="title_en" name="title_en" value="<?php echo set_value('title_en') ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="title_it">Sottotitolo [En]</label>
                                    <input type="text" class="form-control" id="sottotitolo_en" name="sottotitolo_en" value="<?php echo set_value('sottotitolo_en'); ?>">
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="control-label">Descrizione [en]</label>
                                    <textarea id="en_editor" name="description_en" rows="10" cols="80" placeholder="Enter Details in English"><?php echo set_value('description_en') ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>

                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_en">Meta Keywords [en]</label>
                                    <input type="text" class="form-control" id="meta_keywords_en" name="meta_keywords_en" value="<?php echo set_value('meta_keywords_en'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_en">Meta Title [en]</label>
                                    <input type="text" class="form-control" id="meta_en" name="meta_en" value="<?php echo set_value('meta_en'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_en">URL rewrite [en] <small style="color:gray"><?php echo base_url().'en/what_to_visit/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_en" name="rewrite_url_en" placeholder="[rewrite_url]" value="<?php echo set_value('rewrite_url_en'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_en">Meta Description [en]</label>
                                    <textarea class="form-control" id="meta_desc_en" style="height:180px" name="meta_desc_en"><?php echo set_value('meta_desc_en'); ?></textarea>
                                </div>
                            </div>

                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="title_du">Titolo [de]</label>
                                    <input type="text" class="form-control" id="title_du" name="title_du" value="<?php echo set_value('title_du') ?>">
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="control-label" for="title_du">Descrizione [de]</label>
                                    <textarea id="du_editor" name="description_du" rows="10" cols="80" placeholder="Enter Details in Duetch"><?php echo set_value('description_du') ?></textarea>
                                </div>
                                <div class="clearfix"></div><br>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="control-label" for="meta_keywords_du">Meta Keywords [du]</label>
                                        <input type="text" class="form-control" id="meta_keywords_du" name="meta_keywords_du" value="<?php echo set_value('meta_keywords_du'); ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="meta_du">Meta Title [de]</label>
                                        <input type="text" class="form-control" id="meta_du" name="meta_du" value="<?php echo set_value('meta_du'); ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="rewrite_url_du">URL rewrite [de] <small style="color:gray"><?php echo base_url().'du/what_to_visit/[rewrite_url].html' ?></small></label>
                                        <input type="text" class="form-control" id="rewrite_url_du" placeholder="[rewrite_url]" name="rewrite_url_du" value="<?php echo set_value('rewrite_url_du'); ?>">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="control-label" for="meta_desc_du">Meta Description [de]</label>
                                        <textarea class="form-control" id="meta_desc_du" style="height:180px" name="meta_desc_du"><?php echo set_value('meta_desc_du'); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <br><br>
                    <div class="col-lg-5 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Latitudine</label>
                            <input type="text" class="form-control" id="lat" name="lat" value="<?php echo set_value('lat'); ?>">
                        </div>
                    </div>
                    <div class="col-lg-5 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Longitudine</label>
                            <input type="text" class="form-control" id="lng" name="lng" value="<?php echo set_value('lng'); ?>">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6 col-xs-12">
                        <div class="form-group" style="padding-top:30px;">
                            <a class="btn btn-primary btn-xs" href="http://www.gps-coordinates.net/" target="_blank" >Trova Lat/Lng</a>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <hr>
                    <div style="text-align: right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Aggiungi Località</button>
                        <a href ="#" class="btn btn-danger"><i class="fa fa-times"></i> Annulla</a>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
