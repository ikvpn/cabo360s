<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-flag"></i> Spiagge</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista Spiagge
                    <span class="pull-right">
                        <a href="<?php echo base_url()."admin/beach/add" ?>" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Nuova</a>&nbsp;
                    </span>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="beaches-dtable">
                            <thead>
                                <tr>
                                    <th style="width:50px">S.No</th>
                                    <th>Immagine</th>
                                    <th>Titolo</th>
                                    <th>Descrizione</th>
                                    <th style="width:80px;">Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($beaches)): ?>
                                    <?php if (is_array($beaches)): ?>
                                        <?php $counter = 1; ?>
                                        <?php foreach ($beaches as $beach): ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $counter ?></td>
                                                <td><img src='<?php echo base_url().'uploads/beaches/'.$beach->p_thumbnail; ?>' width="30" height="30"></td>
                                                <td><?php echo $beach->title ?></td>
                                                <td><?php
                                         $str = base64_decode($beach->description);
                                         if(strlen($str)>100){
                                             echo substr($str, 0,98).' ...';
                                         }else{
                                             echo $str;
                                         }
                                      ?></td>
                                                <td>
                                                    <a href="<?php echo base_url() ?>admin/beach/edit/<?php echo $beach->id ?>"  class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                    <a onclick="displayDeleteForm('<?php echo $beach->id ?>')" data-toggle="modal" data-target="#delete-beach" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="modal fade" id="delete-beach" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background-color: #fcf8e3;color: #8a6d3b;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i> Attenzione</h4>
                </div>
                <?php echo form_open('admin/beach/delete') ?>
                <input type="hidden" name="id" id="del_id" value="0"/>
                <div class="modal-body">
                    Sei sicuro di voler eliminare la spiaggia?
                    <br />
                    <b>Nota:</b> Questa azione eliminerà tutti i dati associati.
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-check"></i> OK
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<script type="text/javascript">
   function displayDeleteForm(id){
       document.getElementById("del_id").value = id;
   }
</script>
