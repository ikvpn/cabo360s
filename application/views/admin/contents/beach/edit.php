<style>
.form-group {
    margin-bottom: 14px !important;
}
</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-flag"></i> Modifica Spiaggia</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php echo form_open(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $beach->title_en; ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>
                    </ul>
                    <div class="tab-content">
                      <div id="menu2" class="tab-pane fade in active">
                          <br>
                          <div class="col-lg-12">
                              <div class="form-group">
                                  <label class="control-label" for="title">Titolo [it]</label>
                                  <input type="text" class="form-control" id="title" name="title_it" value="<?php echo isset($beach->title_it) ? $beach->title_it : ''; ?>">
                              </div>
                          </div>
                          <div class="col-lg-12">
                              <div class="form-group">
                                  <label class="control-label" for="it_editor">Descrizione [it]</label>
                                  <textarea id="it_editor" name="description_it" rows="10" cols="80"><?php echo isset($beach->description_it) ? base64_decode($beach->description_it) : ''; ?></textarea>
                              </div>
                          </div>
                          <div class="clearfix"></div><br>
                           <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="meta_keywords_it">Meta Keywords[it]</label>
                                  <input type="text" class="form-control" id="meta_keywords_it" name="meta_keywords_it" value="<?php echo $beach->meta_keywords_it ?>">
                              </div>
                              <div class="form-group">
                                  <label class="control-label" for="meta_it">Meta Title [it]</label>
                                  <input type="text" class="form-control" id="meta_it" name="meta_it" value="<?php echo $beach->meta_it; ?>">
                              </div>
                              <div class="form-group">
                                  <label class="control-label" for="rewrite_url_it">URL rewrite [it] <small style="color:gray"><?php echo base_url().$this->lang->lang().'/beach/[rewrite_url].html' ?></small></label>
                                  <input type="text" class="form-control" id="rewrite_url_it" name="rewrite_url_it" value="<?php echo $beach->rewrite_url_it; ?>">
                              </div>
                          </div>
                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="meta_desc_it">Meta Description [it]</label>
                                  <textarea class="form-control" id="meta_desc_it" style="height:180px" name="meta_desc_it"><?php echo $beach->meta_desc_it; ?></textarea>
                              </div>
                          </div>
                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="address_it">Indirizzo [it]</label>
                                  <input type="text" class="form-control" id="address_it" name="address_it" value="<?php echo $beach->address_it; ?>">
                              </div>
                          </div>
                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="bus_line_it">Linee del BUS [it]</label>
                                  <input type="text" class="form-control" id="bus_line_it" name="bus_line_it" value="<?php echo $beach->bus_line_it; ?>">
                              </div>
                          </div>
                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="accessibility_it">Accessibilità [it]</label>
                                  <input type="text" class="form-control" id="accessibility_it" name="accessibility_it" value="<?php echo $beach->accessibility_it; ?>">
                              </div>
                          </div>
                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="private_it">Privata/Non Privata [it]</label>
                                  <input type="text" class="form-control" id="private_it" name="private_it" value="<?php echo $beach->private_it; ?>">
                              </div>
                          </div>
                      </div>
                      <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">Titolo [en]</label>
                                    <input type="text" class="form-control" id="title" name="title_en" value="<?php echo $beach->title_en; ?>">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="en_editor">Descrizione [en]</label>
                                    <textarea id="en_editor" name="description_en" rows="10" cols="80"><?php echo isset($beach->description_en) ? base64_decode($beach->description_en) : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>

                             <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_en">Meta Keywords [en]</label>
                                    <input type="text" class="form-control" id="meta_keywords_en" name="meta_keywords_en" value="<?php echo $beach->meta_keywords_en; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_en">Meta Title [en]</label>
                                    <input type="text" class="form-control" id="meta_en" name="meta_en" value="<?php echo $beach->meta_en; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_en">URL rewrite [en] <small style="color:gray"><?php echo base_url().$this->lang->lang().'/beach/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_en" name="rewrite_url_en" value="<?php echo $beach->rewrite_url_en ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_en">Meta Description [en]</label>
                                    <textarea class="form-control" id="meta_desc_en" style="height:180px" name="meta_desc_en"><?php echo $beach->meta_desc_en; ?></textarea>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="address_en">Indirizzo [en]</label>
                                    <input type="text" class="form-control" id="address_en" name="address_en" value="<?php echo $beach->address_en; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="bus_line_en">Linee del BUS [en]</label>
                                    <input type="text" class="form-control" id="bus_line_en" name="bus_line_en" value="<?php echo $beach->bus_line_en; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="accessibility_en">Accessibilità [en]</label>
                                    <input type="text" class="form-control" id="accessibility_en" name="accessibility_en" value="<?php echo $beach->accessibility_en; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="private_en">Privata/Non Privata [en]</label>
                                    <input type="text" class="form-control" id="private_en" name="private_en" value="<?php echo $beach->private_en; ?>">
                                </div>
                            </div>
                        </div>
                      <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">Titolo [de]</label>
                                    <input type="text" class="form-control" id="title" name="title_du" value="<?php echo isset($beach->title_du) ? $beach->title_du : ''; ?>">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="du_editor">Descrizione [de]</label>
                                    <textarea id="du_editor" name="description_du" rows="10" cols="80"><?php echo isset($beach->description_du) ? base64_decode($beach->description_du) : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                           <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_du">Meta Keywords [de]</label>
                                    <input type="text" class="form-control" id="meta_keywords_du" name="meta_keywords_du" value="<?php echo $beach->meta_keywords_du; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_du">Meta Title [de]</label>
                                    <input type="text" class="form-control" id="meta_du" name="meta_du" value="<?php echo $beach->meta_du; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_du">URL rewrite [de] <small style="color:gray"><?php echo base_url().$this->lang->lang().'/beach/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_du" name="rewrite_url_du" value="<?php echo $beach->rewrite_url_du ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_du">Meta Description [de]</label>
                                    <textarea class="form-control" id="meta_desc_du" style="height:180px" name="meta_desc_du"><?php echo $beach->meta_desc_du; ?></textarea>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="address_du">Indirizzo [de]</label>
                                    <input type="text" class="form-control" id="address_du" name="address_du" value="<?php echo $beach->address_du; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="bus_line_du">Linee del BUS [de]</label>
                                    <input type="text" class="form-control" id="bus_line_du" name="bus_line_du" value="<?php echo $beach->bus_line_du; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="accessibility_du">Accessibilità [de]</label>
                                    <input type="text" class="form-control" id="accessibility_du" name="accessibility_du" value="<?php echo $beach->accessibility_du; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="private_du">Privata/Non Privata [de]</label>
                                    <input type="text" class="form-control" id="private_du" name="private_du" value="<?php echo $beach->private_du; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Latitudine</label>
                            <input type="text" class="form-control" id="lat" name="lat" value="<?php echo $beach->lat; ?>">
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Longitudine</label>
                            <input type="text" class="form-control" id="lng" name="lng" value="<?php echo $beach->lng; ?>">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="control-label" for="location">Località</label>
                            <select name="location_id" class="form-control">
                                <?php foreach ($locations as $location): ?>
                                    <option value="<?php echo $location->id ?>" <?php echo $location->id == $beach->location_id ? "selected='selected'" : ''; ?>><?php echo $location->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer" style="text-align: right">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva Spiaggia</button>
                    <a href ="#" class="btn btn-danger"><i class="fa fa-times"></i> Annulla</a>
                </div>
            </div>
            <!-- /.panel -->
            <?php echo form_close(); ?>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <?php echo form_open_multipart('admin/beach/updatethumbnail'); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Carica la Thumbnail
                </div>
                <div class="panel-body">
                    <input type='hidden' name="id" value="<?php echo $beach->id ?>">
                    <div class="form-group">
                        <label class="control-label" for="image"> Thumbnail</label>
                        <input type="file" id="image" name="p_thumbnail">
                    </div>
                    <?php if (strlen($beach->p_thumbnail) > 0): ?>
                        <img class="img-thumbnail" width="180" src="<?php echo base_url() . 'uploads/beaches/' . $beach->p_thumbnail; ?>">
                    <?php endif; ?>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="col-lg-6">
            <?php echo form_open_multipart('admin/beach/updatecover'); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Carica l'immagine di copertina
                </div>
                <div class="panel-body">
                    <input type='hidden' name="id" value="<?php echo $beach->id ?>">
                    <div class="form-group">
                        <label class="control-label" for="cover"> Immagine di copertina</label>
                        <input type="file" class="form-control" id="cover" name="p_cover">
                    </div>
                    <?php if (strlen($beach->p_cover) > 0): ?>
                        <img class="img-thumbnail" width="200"  src="<?php echo base_url() . 'uploads/beaches/' . $beach->p_cover; ?>">
                    <?php endif; ?>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
