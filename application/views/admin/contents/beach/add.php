<style>
.form-group {
    margin-bottom: 14px !important;
}
</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-flag"></i> Nuova Spiaggia</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <?php echo form_open(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dettagli
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>
                    </ul>
                    <div class="tab-content">
                      <div id="menu2" class="tab-pane fade in active">
                          <br>
                          <div class="col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="title_it">Titolo [it]</label>
                                  <input type="text" class="form-control" id="title_it" name="title_it" value="<?php echo set_value('title_it') ?>">
                              </div>
                          </div>
                          <div class="col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="description_it">Descrizione [it]</label>
                                  <textarea id="it_editor" name="description_it" rows="10" cols="80" placeholder="Enter Details in Italian"><?php echo set_value('description_it') ?></textarea>
                              </div>
                          </div>
                          <div class="clearfix"></div><br>

                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="meta_keywords_it">Meta Keywords[it]</label>
                                  <input type="text" class="form-control" id="meta_keywords_it" name="meta_keywords_it" value="<?php echo set_value('meta_keywords_it'); ?>">
                              </div>
                              <div class="form-group">
                                  <label class="control-label" for="meta_it">Meta Title [it]</label>
                                  <input type="text" class="form-control" id="meta_it" name="meta_it" value="<?php echo set_value('meta_it'); ?>">
                              </div>
                              <div class="form-group">
                                  <label class="control-label" for="rewrite_url_it">URL rewrite [it] <small style="color:gray"><?php echo base_url() . 'it/what_to_visit/[rewrite_url]' ?></small></label>
                                  <input type="text" class="form-control" id="rewrite_url_it" placeholder="[rewrite_url]" name="rewrite_url_it" value="<?php echo set_value('rewrite_url_it'); ?>">
                              </div>
                          </div>
                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="meta_desc_it">Meta Description [it]</label>
                                  <textarea class="form-control" id="meta_desc_it" style="height:180px" name="meta_desc_it"><?php echo set_value('meta_desc_it'); ?></textarea>
                              </div>
                          </div>
                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="address_it">Indirizzo [it]</label>
                                  <input type="text" class="form-control" id="address_it" name="address_it" value="<?php echo set_value('address_it'); ?>">
                              </div>
                          </div>
                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="bus_line_it">Linee del BUS [it]</label>
                                  <input type="text" class="form-control" id="bus_line_it" name="bus_line_it" value="<?php echo set_value('bus_line_it'); ?>">
                              </div>
                          </div>
                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="accessibility_it">Accessibilità [it]</label>
                                  <input type="text" class="form-control" id="accessibility_it" name="accessibility_it" value="<?php echo set_value('accessibility_it'); ?>">
                              </div>
                          </div>
                          <div class="col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="private_it">Privata/Non Privata [it]</label>
                                  <input type="text" class="form-control" id="private_it" name="private_it" value="<?php echo set_value('private_it'); ?>">
                              </div>
                          </div>
                      </div>
                        <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="title_en">Titolo [en]</label>
                                    <input type="text" class="form-control" id="title_en" name="title_en" value="<?php echo set_value('title_en') ?>">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="description_en">Descrizione [en]</label>
                                    <textarea id="en_editor" name="description_en" rows="10" cols="80" placeholder="Enter Details in English"><?php echo set_value('description_en') ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>

                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_en">Meta Keywords [en]</label>
                                    <input type="text" class="form-control" id="meta_keywords_en" name="meta_keywords_en" value="<?php echo set_value('meta_keywords_en'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_en">Meta Title [en]</label>
                                    <input type="text" class="form-control" id="meta_en" name="meta_en" value="<?php echo set_value('meta_en'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_en">URL rewrite [en] <small style="color:gray"><?php echo base_url() . 'en/beach/[rewrite_url]' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_en" name="rewrite_url_en" placeholder="[rewrite_url]" value="<?php echo set_value('rewrite_url_en'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_en">Meta Description [en]</label>
                                    <textarea class="form-control" id="meta_desc_en" style="height:180px" name="meta_desc_en"><?php echo set_value('meta_desc_en'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="address_en">Indirizzo [en]</label>
                                    <input type="text" class="form-control" id="address_en" name="address_en" value="<?php echo set_value('address_en'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="bus_line_en">Linee del BUS [en]</label>
                                    <input type="text" class="form-control" id="bus_line_en" name="bus_line_en" value="<?php echo set_value('bus_line_en'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="accessibility_en">Accessibilità [en]</label>
                                    <input type="text" class="form-control" id="accessibility_en" name="accessibility_en" value="<?php echo set_value('accessibility_en'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="private_en">Priviata/Non Privata [en]</label>
                                    <input type="text" class="form-control" id="private_en" name="private_en" value="<?php echo set_value('private_en'); ?>">
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="title_du">Titolo [de]</label>
                                    <input type="text" class="form-control" id="title_du" name="title_du" value="<?php echo set_value('title_du'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="description_du">Descrizione [de]</label>
                                    <textarea id="du_editor" name="description_du" rows="10" cols="80" placeholder="Enter Details in Duetch"><?php echo set_value('description_di') ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_du">Meta Keywords [de]</label>
                                    <input type="text" class="form-control" id="meta_keywords_du" name="meta_keywords_du" value="<?php echo set_value('meta_keywords_du'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_du">Meta Title [de]</label>
                                    <input type="text" class="form-control" id="meta_du" name="meta_du" value="<?php echo set_value('meta_du'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_du">URL rewrite [de] <small style="color:gray"><?php echo base_url() . 'du/what_to_visit/[rewrite_url]' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_du" placeholder="[rewrite_url]" name="rewrite_url_du" value="<?php echo set_value('rewrite_url_du'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_du">Meta Description [de]</label>
                                    <textarea class="form-control" id="meta_desc_du" style="height:180px" name="meta_desc_du"><?php echo set_value('meta_desc_du'); ?></textarea>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="address_du">Indirizzo [de]</label>
                                    <input type="text" class="form-control" id="address_du" name="address_du" value="<?php echo set_value('address_du'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="bus_line_du">Linee del BUS [de]</label>
                                    <input type="text" class="form-control" id="bus_line_du" name="bus_line_du" value="<?php echo set_value('bus_line_du'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="accessibility_du">Accessibilità [de]</label>
                                    <input type="text" class="form-control" id="accessibility_du" name="accessibility_du" value="<?php echo set_value('accessibility_du'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="private_du">Privata/Non Privata [de]</label>
                                    <input type="text" class="form-control" id="private_du" name="private_du" value="<?php echo set_value('private_du'); ?>">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Latitudine</label>
                            <input type="text" class="form-control" id="lat" name="lat" value="<?php echo set_value('lat') ?>">
                        </div>
                    </div>
                    <div class="col-lg-5 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Longitudine</label>
                            <input type="text" class="form-control" id="lng" name="lng" value="<?php echo set_value('lng') ?>">
                        </div>
                    </div>
                    <div class="col-lg-1 col-sm-6 col-xs-12">
                        <div class="form-group" style="padding-top:30px;">
                            <a class="btn btn-primary btn-xs" href="http://www.gps-coordinates.net/" target="_blank" >Trova Lat/Lng</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="control-label" for="location">Località</label>
                            <select name="location_id" class="form-control">
                                <?php foreach ($locations as $location): ?>
                                    <option value="<?php echo $location->id ?>"><?php echo $location->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer" style="text-align: right">
                    <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Aggiungi Spiaggia</button>
                    <a href ="#" class="btn btn-danger"><i class="fa fa-times"></i> Annulla</a>
                </div>
            </div>
            <!-- /.panel -->
            <?php echo form_close(); ?>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
