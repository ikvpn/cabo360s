<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-image"></i> Modifica Luogo di interesse</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php echo form_open(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $place->enTitle; ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="menu2" class="tab-pane fade in active">
                            <br>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">Titolo [it]</label>
                                    <input type="text" class="form-control" id="title" name="itTitle" value="<?php echo set_value('itTitle') ? set_value('itTitle') : $place->itTitle; ?>">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="it_editor">Descrizione [it]</label>
                                    <textarea id="it_editor" name="itDescription" rows="10" cols="80"><?php echo set_value('itDescription') ? set_value('itDescription') : base64_decode($place->itDescription); ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_it">Meta Keywords [it]</label>
                                    <input type="text" class="form-control" id="meta_keywords_du" name="meta_keywords_it" value="<?php echo $place->meta_keywords_it; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_it">Meta Title [it]</label>
                                    <input type="text" class="form-control" id="meta_du" name="meta_tag_it" value="<?php echo $place->meta_tag_it; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_du">URL rewrite [it] <small style="color:gray"><?php echo base_url().'it/places-of-interest/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_it" name="rewrite_url_it" value="<?php echo $place->rewrite_url_it ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_it">Meta Description [it]</label>
                                    <textarea class="form-control" id="meta_desc_it" style="height:180px" name="meta_desc_it"><?php echo $place->meta_desc_it; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">Titolo [en]</label>
                                    <input type="text" class="form-control" id="title" name="enTitle" value="<?php echo set_value('enTitle') ? set_value('enTitle') : $place->enTitle; ?>">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="en_editor">Descrizione [en]</label>
                                    <textarea id="en_editor" name="enDescription" rows="10" cols="80"><?php echo set_value('enDescription') ? set_value('enDescription') : base64_decode($place->enDescription); ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                             <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_en">Meta Keywords [en]</label>
                                    <input type="text" class="form-control" id="meta_keywords_en" name="meta_keywords_en" value="<?php echo $place->meta_keywords_en; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_en">Meta Title [en]</label>
                                    <input type="text" class="form-control" id="meta_en" name="meta_tag_en" value="<?php echo $place->meta_tag_en; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_en">URL rewrite [en] <small style="color:gray"><?php echo base_url().'en/places-of-interest/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_en" name="rewrite_url_en" value="<?php echo $place->rewrite_url_en ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_en">Meta Description [en]</label>
                                    <textarea class="form-control" id="meta_desc_en" style="height:180px" name="meta_desc_en"><?php echo $place->meta_desc_en; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="title">Titolo [de]</label>
                                    <input type="text" class="form-control" id="title" name="duTitle" value="<?php echo set_value('duTitle') ? set_value('duTitle') : $place->duTitle; ?>">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="du_editor">Descrizione [de]</label>
                                    <textarea id="du_editor" name="duDescription" rows="10" cols="80"><?php echo set_value('duDescription') ? set_value('duDescription') : base64_decode($place->duDescription); ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>

                             <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_du">Meta Keywords [de]</label>
                                    <input type="text" class="form-control" id="meta_keywords_du" name="meta_keywords_du" value="<?php echo $place->meta_keywords_du; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_du">Meta Title [de]</label>
                                    <input type="text" class="form-control" id="meta_du" name="meta_tag_du" value="<?php echo $place->meta_tag_du; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_du">URL rewrite [de] <small style="color:gray"><?php echo base_url().'du/places-of-interest/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_du" name="rewrite_url_du" value="<?php echo $place->rewrite_url_du ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_du">Meta Description [de]</label>
                                    <textarea class="form-control" id="meta_desc_du" style="height:180px" name="meta_desc_du"><?php echo $place->meta_desc_du; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="control-label" for="location">Località</label>
                            <select name="location_id" class="form-control">
                                <?php foreach ($locations as $location): ?>
                                    <option value="<?php echo $location->id ?>" <?php echo $location->id == $place->location_id ? "selected='selected'" : ''; ?>><?php echo $location->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Latitudine</label>
                            <input type="text" class="form-control" id="lat" name="lat" value="<?php echo $place->lat; ?>">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Longitudine</label>
                            <input type="text" class="form-control" id="lng" name="lng" value="<?php echo $place->lng; ?>">
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-xs-12">
                        <h4>Altri Dettagli</h4>
                        <hr>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="address">Indirizzo</label>
                            <input type="text" class="form-control" id="address" name="address" value="<?php echo set_value('address') ? set_value('address') : $place->address; ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="web">Sito Web</label>
                            <input type="text" class="form-control" id="web" name="web" value="<?php echo set_value('web') ? set_value('web') : $place->web; ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?php echo set_value('email') ? set_value('email') : $place->email; ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="phone">Numero di telefono</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo set_value('phone') ? set_value('phone') : $place->phone; ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="mobile">Numero di cellulare</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo set_value('mobile') ? set_value('mobile') : $place->mobile; ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="timing">Orari</label>
                            <input type="text" class="form-control" id="timing" name="timing" placeholder="00:00 - 00:00" value="<?php echo set_value('timing') ? set_value('timing') : $place->timing; ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="openingDays">Giorni di apertura</label>
                            <input type="text" class="form-control" id="openingDays" name="openingDays" value="<?php echo set_value('openingDays') ? set_value('openingDays') : $place->openingDays; ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="closingDays">Gorni di chiusura</label>
                            <input type="text" class="form-control" id="closingDays" name="closingDays" value="<?php echo set_value('closingDays') ? set_value('closingDays') : $place->closingDays; ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <br><br>
                            <label class="control-label" for="reservation">Richiede la prenotazione?</label>
                            <?php if (set_value('reservation')): ?>
                                <label><input type="radio" id="yesR" name="reservation" value="1" <?php echo set_radio('reservation', '1') ?>> Yes</label> &nbsp;&nbsp;&nbsp;
                                <label><input type="radio" id="NoR" name="reservation" value="0" <?php echo set_radio('reservation', '0') ?>> No</label>
                            <?php else: ?>
                                <label><input type="radio" id="yesR" name="reservation" value="1" <?php echo $place->reservation == 1 ? "checked='checked'" : '' ?>> Yes</label> &nbsp;&nbsp;&nbsp;
                                <label><input type="radio" id="NoR" name="reservation" value="0" <?php echo $place->reservation == 0 ? "checked='checked'" : '' ?>> No</label>
                            <?php endif; ?>
                            <br>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="pAdults">Prezzo per gli adulti</label>
                            <input type="text" class="form-control" id="pAdults" name="pAdults" value="<?php echo set_value('pAdults') ? set_value('pAdults') : $place->pAdults; ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="pChilds">Prezzo per i bambini</label>
                            <input type="text" class="form-control" id="pChilds" name="pChilds" value="<?php echo set_value('pChilds') ? set_value('pChilds') : $place->pChilds; ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="pGuests">Prezzo per gli ospiti</label>
                            <input type="text" class="form-control" id="pGuests" name="pGuests" value="<?php echo set_value('pGuests') ? set_value('pGuests') : $place->pGuests; ?>">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <small><i class="fa fa-info-circle"></i> NOTA: Inserisci 0 se il biglietto per una particolare categoria è gratis.</small>
                    </div>
                </div>

                <!-- /.panel-body -->
                <div class="panel-footer" style="text-align: right">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva Luogo</button>
                    <a href ="#" class="btn btn-danger"><i class="fa fa-times"></i> Annulla</a>
                </div>
            </div>
            <!-- /.panel -->
            <?php echo form_close(); ?>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <?php echo form_open_multipart('admin/places_of_interest/updatethumbnail'); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Carica l'immagine primaria
                </div>
                <div class="panel-body">
                    <input type='hidden' name="id" value="<?php echo $place->id ?>">
                    <div class="form-group">
                        <label class="control-label" for="image"> Immagine primaria</label>
                        <input type="file" id="image" name="p_thumbnail">
                    </div>
                    <?php if (strlen($place->photo) > 3): ?>
                        <img class="img-thumbnail" width="180" src="<?php echo base_url() . 'uploads/places_of_interest/' . $place->photo; ?>">
                    <?php endif; ?>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
