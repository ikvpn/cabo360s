<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-image"></i> Nuovo Luogo di Interesse</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <?php echo form_open(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dettagli
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="menu2" class="tab-pane fade in active">
                            <br>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="title_it">Titolo [it]</label>
                                    <input type="text" class="form-control" id="title_it" name="itTitle" value="<?php echo set_value('itTitle') ?>">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="description_it">Descrizione [it]</label>
                                    <textarea id="it_editor" name="itDescription" rows="10" cols="80" placeholder="Enter Details in Italian"><?php echo set_value('itDescription') ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_it">Meta Keywords[it]</label>
                                    <input type="text" class="form-control" id="meta_keywords_it" name="meta_keywords_it" value="<?php echo set_value('meta_keywords_it'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_it">Meta Title [it]</label>
                                    <input type="text" class="form-control" id="meta_it" name="meta_it" value="<?php echo set_value('meta_it'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_it">URL rewrite [it] <small style="color:gray"><?php echo base_url() . 'it/places-of-interest/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_it" placeholder="[rewrite_url]" name="rewrite_url_it" value="<?php echo set_value('rewrite_url_it'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_it">Meta Description [it]</label>
                                    <textarea class="form-control" id="meta_desc_it" style="height:180px" name="meta_desc_it"><?php echo set_value('meta_desc_it'); ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="title_en">Titolo [en]</label>
                                    <input type="text" class="form-control" id="title_en" name="enTitle" value="<?php echo set_value('enTitle') ?>">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="description_en">Descrizione [en]</label>
                                    <textarea id="en_editor" name="enDescription" rows="10" cols="80" placeholder="Enter Details in English"><?php echo set_value('enDescription'); ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_en">Meta Keywords [en]</label>
                                    <input type="text" class="form-control" id="meta_keywords_en" name="meta_keywords_en" value="<?php echo set_value('meta_keywords_en'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_en">Meta Title [en]</label>
                                    <input type="text" class="form-control" id="meta_en" name="meta_en" value="<?php echo set_value('meta_en'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_en">URL rewrite [en] <small style="color:gray"><?php echo base_url() .'en/places-of-interest/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_en" name="rewrite_url_en" placeholder="[rewrite_url]" value="<?php echo set_value('rewrite_url_en'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_en">Meta Description [en]</label>
                                    <textarea class="form-control" id="meta_desc_en" style="height:180px" name="meta_desc_en"><?php echo set_value('meta_desc_en'); ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="title_du">Titolo [de]</label>
                                    <input type="text" class="form-control" id="title_du" name="duTitle" value="<?php echo set_value('duTitle') ?>">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="description_du">Descrizione [de]</label>
                                    <textarea id="du_editor" name="duDescription" rows="10" cols="80" placeholder="Enter Details in Duetch"><?php echo set_value('duDescription'); ?></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_du">Meta Keywords [de]</label>
                                    <input type="text" class="form-control" id="meta_keywords_du" name="meta_keywords_du" value="<?php echo set_value('meta_keywords_du'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_du">Meta Title [de]</label>
                                    <input type="text" class="form-control" id="meta_du" name="meta_du" value="<?php echo set_value('meta_du'); ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_du">URL rewrite [de] <small style="color:gray"><?php echo base_url() . 'du/places-of-interest/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_du" placeholder="[rewrite_url]" name="rewrite_url_du" value="<?php echo set_value('rewrite_url_du'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_du">Meta Description [de]</label>
                                    <textarea class="form-control" id="meta_desc_du" style="height:180px" name="meta_desc_du"><?php echo set_value('meta_desc_du'); ?></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="location">Località</label>
                            <select name="location_id" class="form-control">
                                <?php foreach ($locations as $location): ?>
                                    <option value="<?php echo $location->id ?>"><?php echo $location->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Latitudine</label>
                            <input type="text" class="form-control" id="lat" name="lat" value="<?php echo set_value('lat'); ?>">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Longitudine</label>
                            <input type="text" class="form-control" id="lng" name="lng" value="<?php echo set_value('lng'); ?>">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                        <h4>Altri Dettagli</h4>
                        <hr>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="address">Indirizzo</label>
                            <input type="text" class="form-control" id="address" name="address" value="<?php echo set_value('address'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="web">Sito Web</label>
                            <input type="text" class="form-control" id="web" name="web" value="<?php echo set_value('web'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?php echo set_value('email'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="phone">Numero di telefono</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo set_value('phone'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="mobile">Numero di cellulare</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo set_value('mobile'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="timing">Orari</label>
                            <input type="text" class="form-control" id="timing" name="timing" placeholder="00:00 - 00:00" value="<?php echo set_value('timing'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="openingDays">Giorni di apertura</label>
                            <input type="text" class="form-control" id="openingDays" name="openingDays" value="<?php echo set_value('openingDays'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="closingDays">Giorni di chiusura</label>
                            <input type="text" class="form-control" id="closingDays" name="closingDays" value="<?php echo set_value('closingDays'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <br><br>
                            <label class="control-label" for="reservation">Richiede la prenotazione?</label>
                            <label><input type="radio" id="yesR" name="reservation" value="1" <?php echo set_radio('reservation', '1') ?>> Si</label> &nbsp;&nbsp;&nbsp;
                            <label><input type="radio" id="yesR" name="reservation" value="0" <?php echo set_radio('reservation', '0') ?>> No</label>
                            <br>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="pAdults">Prezzo per gli adulti</label>
                            <input type="text" class="form-control" id="pAdults" name="pAdults" value="<?php echo set_value('pAdults'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="pChilds">Prezzo per i bambini</label>
                            <input type="text" class="form-control" id="pChilds" name="pChilds" value="<?php echo set_value('pChilds'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label class="control-label" for="pGuests">Prezzo per gli ospiti</label>
                            <input type="text" class="form-control" id="pGuests" name="pGuests" value="<?php echo set_value('pGuests'); ?>">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <small><i class="fa fa-info-circle"></i> NOTA: Inserisci 0 se il biglietto per una particolare categoria è gratis.</small>
                    </div>
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer" style="text-align: right">
                    <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Aggiungi Luogo</button>
                    <a href ="#" class="btn btn-danger"><i class="fa fa-times"></i> Annulla</a>
                </div>
            </div>
            <!-- /.panel -->
            <?php echo form_close(); ?>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
