<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-money"></i> Gestione prezzi</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Categorie
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="pricing-dtable">
                            <thead>
                                <tr>
                                    <th style="width:50px">S.No</th>
                                    <th>Nome</th>
                                    <th>Costo annuale</th>
                                    <th>Costo mensile</th>
                                    <th style="width:80px;">Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($categories)): ?>
                                    <?php if (is_array($categories)): ?>
                                        <?php $counter = 1; ?>
                                        <?php foreach ($categories as $category): ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $counter ?></td>
                                                <td><?php echo $category->name; ?></td>
                                                <td><?php echo $category->y_charges ?></td>
                                                <td><?php echo $category->m_charges ?></td>
                                                <td>
                                                    <a onclick="displayEditForm('<?php echo $category->id ?>','<?php echo $category->name ?>','<?php echo $category->y_charges ?>','<?php echo $category->m_charges ?>')" data-toggle="modal" data-target="#edit-category" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="modal fade" id="edit-category" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> <span id='edit_name'></span></h4>
                </div>
                <?php echo form_open('admin/pricing/update') ?>
                <input type="hidden" name="id" id="edit_id" value="0"/>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="lat">Costo annuale</label>
                        <input type="number" class="form-control" id="edit_y_charges" name="y_charges">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="lat">Costo mensile</label>
                        <input type="number" class="form-control" id="edit_m_charges" name="m_charges">
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i> Salva
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script type="text/javascript">
        function displayEditForm(id, name, y_charges, m_charges) {
            document.getElementById("edit_id").value = id;
            document.getElementById("edit_name").innerHTML = name;
            document.getElementById("edit_y_charges").value = y_charges;
            document.getElementById("edit_m_charges").value = m_charges;
        }
    </script>
