<style>
    fieldset{
        border: 1px groove #ddd !important;
        padding: 0 0.4em 0.4em 0.4em !important;
        margin: 0 0 0.6em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }
    legend{
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;
        font-size: 11px;
        margin-bottom: 0;
    }
</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-calendar"></i> Eventi</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <?php if (validation_errors()): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo validation_errors(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12">
            <?php echo form_open(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->Event_model->enTitle?$this->Event_model->enTitle:"New Event"; ?>
                </div>
                <div class="panel-body">
                    <input type="hidden" name="id" value="<?php echo isset($this->Event_model->id) ? $this->Event_model->id : '0'; ?>">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="menu2" class="tab-pane fade in active">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="itTitle">Nome visualizzato [it]</label>
                                    <input type="text" name="itTitle" id="itTitle" placeholder="Enter Name in Italian" value="<?php echo set_value('itTitle') ? set_value('itTitle') : (isset($this->Event_model->itTitle) ? $this->Event_model->itTitle : ''); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12" style="margin-top:10px">
                                <label class="control-label" for="it_editor">Descrizione [it]</label>
                                <textarea id="it_editor" name="itDescription" rows="5" cols="80" placeholder="Enter page description for Italian">
                                    <?php echo set_value('itDescription') ? set_value('itDescription') : (isset($this->Event_model->itDescription) ? $this->Event_model->itDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                        <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="enTitle">Nome visualizzato [en]</label>
                                    <input type="text" name="enTitle" id="entitle" placeholder="Enter Title in English" value="<?php echo (set_value('enTitle') ? set_value('enTitle') : (isset($this->Event_model->enTitle) ? $this->Event_model->enTitle : '')); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <label class="control-label" for="en_editor">Descrizione [en]</label>
                                <textarea id="en_editor" name="enDescription" rows="5" cols="80" placeholder="Enter page description for English">
                                    <?php echo set_value('enDescription') ? set_value('enDescription') : (isset($this->Event_model->enDescription) ? $this->Event_model->enDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="duTitle">nome visualizzato [de]</label>
                                    <input type="text" name="duTitle" id="duTitle" placeholder="Enter Title in Deutch" value="<?php echo set_value('duTitle') ? set_value('duTitle') : (isset($this->Event_model->duTitle) ? $this->Event_model->duTitle : ''); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12" style="margin-top:10px">
                                <label class="control-label" for="du_editor">Descrizione [de]</label>
                                <textarea id="du_editor" name="duDescription" rows="5" cols="80" placeholder="Enter page description for Deutch">
                                    <?php echo set_value('duDescription') ? set_value('duDescription') : (isset($this->Event_model->duDescription) ? $this->Event_model->duDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12" style="margin-top: 15px;"></div>
                    <fieldset class="my_fileds">
                        <legend class="my_fileds">Date e orario</legend>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="startDate">Data di inizio</label>
                                <input type="text" name="startDate" placeholder="MM/DD/YYYY" value="<?php echo set_value('startDate') ? set_value('startDate') : (isset($this->Event_model->startDate) ? substr($this->Event_model->startDate, 5, 2) . '/' . substr($this->Event_model->startDate, 8, 2) . '/' . substr($this->Event_model->startDate, 0, 4) : ''); ?>" class="form-control datepicker">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label class="control-label" for="startTime">Orario di inizio</label>
                                    <input type="text" name="startTime" placeholder="HH:MM" value="<?php echo set_value('startTime') ? set_value('startTime') : (isset($this->Event_model->startTime) ? $this->Event_model->startTime : ''); ?>" class="form-control timepicker">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="endDate">Data di fine</label>
                                <input type="text" name="endDate" placeholder="MM/DD/YYYY" value="<?php echo set_value('endDate') ? set_value('endDate') : (isset($this->Event_model->endDate) ? substr($this->Event_model->endDate, 5, 2) . '/' . substr($this->Event_model->endDate, 8, 2) . '/' . substr($this->Event_model->endDate, 0, 4) : ''); ?>" class="form-control datepicker">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label class="control-label" for="endTime">Orario di fine</label>
                                    <input type="text" name="endTime" placeholder="HH:MM" value="<?php echo set_value('endTime') ? set_value('endTime') : (isset($this->Event_model->endTime) ? $this->Event_model->endTime : ''); ?>" class="form-control timepicker">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="my_fileds">
                        <legend class="my_fileds">Informazioni per la mappa</legend>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="lat">Latitudine</label>
                                <input type="text" name="lat" value="<?php echo set_value('lat') ? set_value('lat') : (isset($this->Event_model->lat) ? $this->Event_model->lat : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="lng">Longitudine</label>
                                <input type="text" name="lng" value="<?php echo set_value('lng') ? set_value('lng') : (isset($this->Event_model->lng) ? $this->Event_model->lng : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="location">Località</label>
                                <select name="location_id" class="form-control">
                                    <?php foreach ($locations as $location): ?>
                                        <option value="<?php echo $location->id ?>" <?php echo $location->id == $this->Event_model->location_id ? "selected='selected'" : ''; ?>><?php echo $location->title ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Prezzi</legend>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="pAdult">Adulti</label>
                                <input type="text" name="pAdult" value="<?php echo set_value('pAdult') ? set_value('pAdult') : (isset($this->Event_model->pAdult) ? $this->Event_model->pAdult : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="pChild">Bambini</label>
                                <input type="text" name="pChild" value="<?php echo set_value('pChild') ? set_value('pChild') : (isset($this->Event_model->pChild) ? $this->Event_model->pChild : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="pGuest">Ospiti</label>
                                <input type="text" name="pGuest" value="<?php echo set_value('pGuest') ? set_value('pGuest') : (isset($this->Event_model->pGuest) ? $this->Event_model->pGuest : ''); ?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Informazioni</legend>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="type">Tipo di evento</label>
                                <select name="type" id="type" class="form-control">
                                    <option <?php echo isset($this->Event_model->type) ? ($this->Event_model->type == 'General' ? 'selected=""' : '') : ''; ?>>Generale</option>
                                    <option <?php echo isset($this->Event_model->type) ? ($this->Event_model->type == 'Traditional' ? 'selected=""' : '') : ''; ?>>Traditional</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="category">Categoria</label>
                                <select name="category" id="category" class="form-control">
                                    <option <?php echo isset($this->Event_model->category) ? ($this->Event_model->category == 'Music' ? 'selected=""' : '') : ''; ?>>Musica</option>
                                    <option <?php echo isset($this->Event_model->category) ? ($this->Event_model->category == 'Prccession' ? 'selected=""' : '') : ''; ?>>Proccessione</option>
                                    <option <?php echo isset($this->Event_model->category) ? ($this->Event_model->category == 'Festivals' ? 'selected=""' : '') : ''; ?>>Festival</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <br>
                                <label class="control-label">Richiede la prenotazione?</label>
                                <label class="radio-inline">
                                    <input type="radio" name="reservation" id="reservation1" value="yes" <?php echo isset($this->Event_model->reservation) ? ($this->Event_model->reservation == 'yes' ? 'checked=""' : '') : 'checked=""'; ?>> Si
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="reservation" id="reservation2" value="no" <?php echo isset($this->Event_model->reservation) ? ($this->Event_model->reservation == 'no' ? 'checked=""' : '') : ''; ?>> No
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="web">Sito Web</label>
                                <input type="text" name="web" value="<?php echo set_value('web') ? set_value('web') : (isset($this->Event_model->web) ? $this->Event_model->web : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="email">Email</label>
                                <input type="text" name="email" value="<?php echo set_value('email') ? set_value('email') : (isset($this->Event_model->email) ? $this->Event_model->email : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="phone">Numero di telefono</label>
                                <input type="text" name="phone" value="<?php echo set_value('phone') ? set_value('phone') : (isset($this->Event_model->phone) ? $this->Event_model->phone : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="mob">Numero di cellulare</label>
                                <input type="text" name="mob" value="<?php echo set_value('mob') ? set_value('mob') : (isset($this->Event_model->mob) ? $this->Event_model->mob : ''); ?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Indirizzo</legend>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <textarea class="form-control" name="address"><?php echo set_value('address') ? set_value('address') : (isset($this->Event_model->address) ? $this->Event_model->address : ''); ?></textarea>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="panel-footer" style="text-align: right;">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <?php if (isset($this->Event_model->id)): ?>
        <div class="row">
            <div class="col-xs-6">
                <?php echo form_open_multipart('admin/events/upload_image/' . $this->Event_model->id); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Carica l'immagine primaria
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <input  type="hidden" name="imageType" value="thumbnail">
                            <div class="form-group">
                                <label class="control-label" for="p_image">Immagine primaria</label>
                                <input type="file" required="required" name="p_image" class="form-control">
                            </div>
                            <?php if (file_exists('./uploads/events/' . $this->Event_model->thumbnail) && $this->Event_model->thumbnail != ''): ?>
                                <img class="img-thumbnail" src="<?php echo base_url() . 'uploads/events/' . $this->Event_model->thumbnail ?>">
                            <?php else: ?>
                                Non ci sono foto
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right;">
                        <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Carica</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <?php if ($this->Event_model->type == "Traditional"): ?>
                <div class="col-xs-6">
                    <?php echo form_open_multipart('admin/events/upload_image/' . $this->Event_model->id); ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Carica l'immagine di copertina
                        </div>
                        <div class="panel-body">
                            <input type="hidden" name="imageType" value="cover">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="p_image">Immagine di copertina</label>
                                    <input type="file" required="required" name="p_image" class="form-control">
                                </div>
                                <?php if (file_exists('./uploads/events/' . $this->Event_model->cover) && $this->Event_model->cover != ''): ?>
                                    <img class="img-thumbnail" src="<?php echo base_url() . 'uploads/events/' . $this->Event_model->cover ?>">
                                <?php else: ?>
                                    Non ci sono foto
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right;">
                            <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Carica</button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
