<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-tags"></i> Gestione Tag</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tag Disponibili
                    <span class="pull-right">
                        <a onclick="displayAddForm()"  class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Nuovo</a>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="ads-dtable">
                            <thead>
                                <tr>
                                    <th style="width:50px">S.No</th>
                                    <th>Nome IT</th>
                                    <th>Nome EN</th>
                                    <th style="width:80px;">Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($tags)): ?>
                                    <?php if (is_array($tags)): ?>
                                        <?php $counter = 1; ?>
                                        <?php foreach ($tags as $tag): ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $counter ?></td>
                                                <td><?php echo $tag->nameIT ?></td>
                                                <td><?php echo $tag->nameEN ?></td>
                                                <td>
                                                    <a onclick="displayEditForm('<?php echo $tag->id ?>', '<?php echo $tag->nameIT ?>','<?php echo $tag->nameEN ?>')"  class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                    <a onclick="displayDeleteForm('<?php echo $tag->id ?>')" data-toggle="modal" data-target="#delete-tag" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->

    <div class="modal fade" id="edit-add" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> <span id='add_title'></span></h4>
                </div>
                <?php echo form_open(); ?>
                <input type="hidden" name="id" id="edit_id" value="0"/>
                <div class="modal-body">
                  <div class="panel-body">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="tag_name">Nome [It]</label>
                            <input type="text" class="form-control" name="tag_name_it" id="tag_name_it" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="tag_name_en">Nome [En]</label>
                            <input type="text" class="form-control" name="tag_name_en" id="tag_name_en" value="">
                        </div>
                    </div>
                  </div>
                    <div class='clearfix'></div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i> Salva
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="add-tag" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> <span id='add_title'>Aggiungi tag</span></h4>
                </div>
                <?php echo form_open(); ?>
                <input type="hidden" name="id" id="edit_id" value="0"/>
                <div class="modal-body">
                    <div class="col-xs-12">
                      <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label" for="tag_name_it">Nome [It]</label>
                            <input type="text" class="form-control" name="tag_name_it" id="tag_name_it" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="tag_name_en">Nome [En]</label>
                            <input type="text" class="form-control" name="tag_name_en" id="tag_name_en" value="">
                        </div>
                      </div>
                    </div>
                    <div class='clearfix'></div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i> Salva
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="delete-tag" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background-color: #fcf8e3;color: #8a6d3b;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i> Attenzione</h4>
                </div>
                <?php echo form_open('admin/tags/delete') ?>
                <input type="hidden" name="id" id="del_id" value="0"/>
                <div class="modal-body">
                    Sei sicuro di voler eliminare il tag?
                    <br />
                    <b>Nota:</b> Questa azione eliminerà tutti i dati associati.
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-check"></i> OK
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <script type="text/javascript">
      function displayEditForm(id, nameIT, nameEN) {
            document.getElementById("edit_id").value = id;
            document.getElementById("add_title").innerHTML = nameIT;
            document.getElementById("tag_name_it").value = nameIT;
            document.getElementById("tag_name_en").value = nameEN;
            $('#edit-add').modal("show");
        }
        function displayAddForm(){
          $('#add-tag').modal("show");
        }
        function displayDeleteForm(id){
            document.getElementById("del_id").value = id;
        }
    </script>
