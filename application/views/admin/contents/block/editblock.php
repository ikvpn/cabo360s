<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Modifica Blocco</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <?php echo form_open_multipart(); ?>
            <input type="hidden" name="id" value="<?php echo $block->id; ?>">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $block->title_en; ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="menu2" class="tab-pane fade in active">
                            <br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="title">Titolo [it]</label>
                                    <input type="text" class="form-control" id="title_it" name="title_it" value="<?php echo isset($block->title_it) ? $block->title_it : ''; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="link_it"> Link [it]</label>
                                    <input type="text" class="form-control" id="link_it" name="link_it" value="<?php echo $block->link_it; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12">
                              <textarea id="it_editor" name="description_it" rows="10" cols="80"><?php echo isset($block->description_it) ? base64_decode($block->description_it) : ''; ?></textarea>
                            </div>
                            <div class="clearfix"></div><br>
                            <!--<div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_it">Meta Keywords[it]</label>
                                    <input type="text" class="form-control" id="meta_keywords_it" name="meta_keywords_it" value="<?php echo $block->meta_keywords_it ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_it">Meta Title [it]</label>
                                    <input type="text" class="form-control" id="meta_it" name="meta_it" value="<?php echo $block->meta_tag_it; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_it">URL rewrite [it] <small style="color:gray"><?php echo base_url() . $this->lang->lang() . '/blog/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_it" name="rewrite_url_it" value="<?php echo $block->rewrite_url_it; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_it">Meta Description [it]</label>
                                    <textarea class="form-control" id="meta_desc_it" style="height:180px" name="meta_desc_it"><?php echo $block->meta_desc_it; ?></textarea>
                                </div>
                            </div>-->
                        </div>
                        <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="title_en">Titolo [en]</label>
                                    <input type="text" class="form-control" id="title_en" name="title_en" value="<?php echo $block->title_en; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="link_en "> Link [en]</label>
                                    <input type="text" class="form-control" id="link_en" name="link_en" value="<?php echo $block->link_en; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12">
                            	<textarea id="en_editor" name="description_en" rows="10" cols="80"><?php echo isset($block->description_en) ? base64_decode($block->description_en) : ''; ?></textarea>
                            </div>
                            <div class="clearfix"></div><br>

                            <!--<div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_en">Meta Keywords [en]</label>
                                    <input type="text" class="form-control" id="meta_keywords_en" name="meta_keywords_en" value="<?php echo $block->meta_keywords_en; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_en">Meta Title [en]</label>
                                    <input type="text" class="form-control" id="meta_en" name="meta_en" value="<?php echo $block->meta_tag_en; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_en">URL rewrite [en] <small style="color:gray"><?php echo base_url() . $this->lang->lang() . '/blog/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_en" name="rewrite_url_en" value="<?php echo $block->rewrite_url_en ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_en">Meta Description [en]</label>
                                    <textarea class="form-control" id="meta_desc_en" style="height:180px" name="meta_desc_en"><?php echo $block->meta_desc_en; ?></textarea>
                                </div>
                            </div>-->
                        </div>
                        <div id='menu1' class="tab-pane fade">
                            <br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="title_du">Titolo [de]</label>
                                    <input type="text" class="form-control" id="title_du" name="title_du" value="<?php echo isset($block->title_du) ? $block->title_du : ''; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="link_du"> Link [de]</label>
                                    <input type="text" class="form-control" id="link_du" name="link_du" value="<?php echo $block->link_du; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12">
                            	<textarea id="du_editor" name="description_du" rows="10" cols="80"><?php echo isset($block->description_du) ? base64_decode($block->description_du) : ''; ?></textarea>
                            </div>
                            <div class="clearfix"></div><br>
                            <!--<div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_du">Meta Keywords [de]</label>
                                    <input type="text" class="form-control" id="meta_keywords_du" name="meta_keywords_du" value="<?php echo $block->meta_keywords_du; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_du">Meta Title [de]</label>
                                    <input type="text" class="form-control" id="meta_du" name="meta_du" value="<?php echo $block->meta_tag_du; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="rewrite_url_du">URL rewrite [de] <small style="color:gray"><?php echo base_url() . $this->lang->lang() . '/blog/[rewrite_url].html' ?></small></label>
                                    <input type="text" class="form-control" id="rewrite_url_du" name="rewrite_url_du" value="<?php echo $block->rewrite_url_du ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_du">Meta Description [de]</label>
                                    <textarea class="form-control" id="meta_desc_du" style="height:180px" name="meta_desc_du"><?php echo $block->meta_desc_du; ?></textarea>
                                </div>
                            </div>-->
                        </div>

                    </div>
                     <div class="clearfix"></div><br>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="image"> Immagine Primaria</label>
                            <input type="file" class="form-control" id="image" name="p_image">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <?php if (strlen($block->p_image) > 3): ?>
                            <img class="img-thumbnail" src="<?php echo base_url() . 'uploads/block/' . $block->p_image; ?>" height="200" width="200">
                        <?php endif; ?>
                    </div>
				        </div>
                <!-- /.panel-body -->
                <div class="panel-footer">
                    <div style="text-align: right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva Blocco</button>
                        <a href ="#" class="btn btn-danger"><i class="fa fa-times"></i> Annulla</a>
                    </div>
                </div>
            </div>
            <!-- /.panel -->
            <?php echo form_close(); ?>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
