<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Impostazioni</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if($this->session->flashdata('msg') && $this->session->flashdata('type')): ?>
      <?php
        $msg = $this->session->flashdata('msg'); $type = $this->session->flashdata('type');
      if ($msg != '' && $type != ''): ?>
          <div class="alert alert-<?php echo $type ?> alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <?php echo $msg; ?>
          </div>
      <?php endif; ?>
    <?php endif; ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    PayPal
                </div>
                <!-- /.panel-heading -->
                <?php echo form_open('admin/dashboard/savepaypal'); ?>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label" for="marchant_id">Nome utente</label>
                        <input type="text" class="form-control" id="marchant_id" name="api_username" value="<?php echo $settings->api_username ?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="m_email">Password</label>
                        <input type="text" class="form-control" id="m_email" name="api_password" value="<?php echo $settings->api_password ?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="r_url">Key</label>
                        <input type="text" class="form-control" id="r_url" name="api_key" value="<?php echo $settings->api_key ?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="r_url">Abilita Pagamenti Offline</label>
                        <label class="radio-inline">
                            <input type="radio" name="offline_payment" value="1" <?php echo $settings->offline_payment=='1'?'checked=""':''; ?>> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="offline_payment" value="0" <?php echo $settings->offline_payment=='0'?'checked=""':''; ?>> No
                        </label>
                    </div>
                </div>
                <div class="panel-footer">
                    <div style="text-align:right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cambio password
                </div>
                <!-- /.panel-heading -->
                <?php echo form_open('dashboard/resetpassword'); ?>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label" for="password">Password attuale</label>
                        <input type="text" class="form-control" id="password" name="password">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="n_password">Nuova Password</label>
                        <input type="text" class="form-control" id="password" name="n_password">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password">Conferma Password</label>
                        <input type="text" class="form-control" id="c_password" name="c_password">
                    </div>
                </div>
                <div class="panel-footer">
                    <div style="text-align: right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-retweet"></i> Cambia</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="clearfix"></div>
         <div class="col-lg-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Generali
                </div>
                <!-- /.panel-heading -->
                <?php echo form_open('admin/dashboard/savegeneralsettings'); ?>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label" for="radius">Near By Areas Radius(KM)</label>
                        <input type="text" class="form-control" id="radius" name="radius" value="<?php echo $settings->radius ?>">
                    </div>
                </div>
                <div class="panel-footer">
                    <div style="text-align:right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Impostazioni Meta Tag dell'Homepage
                </div>
                <!-- /.panel-heading -->
                <?php echo form_open('admin/dashboard/savehomemeta'); ?>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">Du</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="menu2" class="tab-pane fade in active">
                             <br>
                             <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_it">Meta Keywords[it]</label>
                                    <input type="text" class="form-control" id="meta_keywords_it" name="meta_keywords_it" value="<?php echo $settings->meta_keywords_it ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_it">Meta Title [it]</label>
                                    <input type="text" class="form-control" id="meta_it" name="meta_tag_it" value="<?php echo $settings->meta_tag_it; ?>">
                                </div>
                            </div>
                             <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_it">Meta Description [it]</label>
                                    <textarea class="form-control" id="meta_desc_it" style="height:108px" name="meta_desc_it"><?php echo $settings->meta_desc_it; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="home" class="tab-pane fade">
                            <br>
                             <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_en">Meta Keywords [en]</label>
                                    <input type="text" class="form-control" id="meta_keywords_en" name="meta_keywords_en" value="<?php echo $settings->meta_keywords_en; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_en">Meta Title [en]</label>
                                    <input type="text" class="form-control" id="meta_en" name="meta_tag_en" value="<?php echo $settings->meta_tag_en; ?>">
                                </div>
                            </div>
                             <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_en">Meta Description [en]</label>
                                    <textarea class="form-control" id="meta_desc_en" style="height:108px" name="meta_desc_en"><?php echo $settings->meta_desc_en; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_keywords_du">Meta Keywords [de]</label>
                                    <input type="text" class="form-control" id="meta_keywords_du" name="meta_keywords_du" value="<?php echo $settings->meta_keywords_du; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="meta_du">Meta Title [de]</label>
                                    <input type="text" class="form-control" id="meta_du" name="meta_tag_du" value="<?php echo $settings->meta_tag_du; ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label" for="meta_desc_du">Meta Description [de]</label>
                                    <textarea class="form-control" id="meta_desc_du" style="height:108px" name="meta_desc_du"><?php echo $settings->meta_desc_du; ?></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel-footer">
                    <div style="text-align:right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!-- /.row -->
