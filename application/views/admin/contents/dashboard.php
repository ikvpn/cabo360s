<?php
/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */
?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-dashboard"></i> Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <a href="location">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-location-arrow fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">
                                    <?php echo $this->Dashboard_model->locations; ?>
                                </div>
                                <div>Località</div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-left">Dettaglio</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="businesses">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-file fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo $this->Dashboard_model->businesses; //var_dump($this->Dashboard_model->businesses); ?>
                                </div>
                                <div>Attività</div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <span class="pull-left">Dettaglio</span>
                        <span class="pull-right">
                            <?php if ($this->Dashboard_model->registeredBusinesses): ?>
                                <button class="btn btn-danger btn-xs pull-right" style="border-radius: 10px;"><?php echo $this->Dashboard_model->registeredBusinesses; ?> new</button>
                            <?php else: ?>
                                <i class="fa fa-arrow-circle-right"></i>

                            <?php endif; ?>
                        </span>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="blog">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-thumb-tack fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo $this->Dashboard_model->posts; ?></div>
                                <div>Post</div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-left">Dettaglio</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="customers">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo $this->Dashboard_model->customers; ?>
                                </div>
                                <div>Clienti</div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-left">Dettaglio</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-lg-3 col-md-6">

          <a href="<?php  echo base_url() . 'HotelBB/getHotels' ?>"> 
              <div class="panel panel-gray">
                  <div class="panel-heading">
                      <div class="row">
                          <div class="col-xs-3">
                              <i class="fa fa-building-o fa-5x"></i>
                          </div>
                          <div class="col-xs-9 text-right">
                              <div class="huge"><?php echo $hotel_bb; ?></div>
                              <div>Sincronizzati da HBB</div>
                          </div>
                      </div>
                  </div>
                  <div class="panel-footer">
                      <span class="pull-left">Aggiorna il database per HotelBB </span>
                      <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                      <div class="clearfix"></div>
                  </div>
              </div>
          </a>
      </div>
    </div>
