<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-file"></i> Attività</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $title; ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="new_pages">
                            <thead>
                                <tr>
                                    <th style="width:80px">S.No</th>
                                    <th>Nome Attività</th>
                                    <th>Categoria</th>
                                    <th>Tipo di pagamento</th>
                                    <th>Costo</th>
                                    <th>Stato</th>
                                    <th>Gestione Metadati</th>
                                    <th style="width:150px;">Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($pages)): ?>
                                    <?php if (is_array($pages)): ?>
                                        <?php $counter = 1; ?>
                                        <?php foreach ($pages as $page): ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $counter ?></td>
                                                <td><?php echo $page->title ?></td>
                                                <td><?php echo $this->Orders_model->getCategoryName($page->category_id); ?></td>
                                                <td><?php echo $page->payment_type ?></td>
                                                <td><?php echo $page->agreement_amount ?></td>
                                                <td><?php echo $page->status ?></td>
                                                <?php
                                                $titleIT = $keyIT = $descIT = $titleEN = $keyEN = $descEN = $titleDE = $keyDE = $descDE = '';
                                                if($page->metadata != NULL) {
                                                  $titleIT = $page->metadata->meta_title_it ? $page->metadata->meta_title_it : '';
                                                  $keyIT = $page->metadata->meta_keywords_it ? $page->metadata->meta_keywords_it : '';
                                                  $descIT = $page->metadata->meta_desc_it ? $page->metadata->meta_desc_it : '';

                                                  $titleEN = $page->metadata->meta_title_en ? $page->metadata->meta_title_en : '';
                                                  $keyEN = $page->metadata->meta_keywords_en ? $page->metadata->meta_keywords_en : '';
                                                  $descEN = $page->metadata->meta_desc_en ? $page->metadata->meta_desc_en : '';

                                                  $titleDE = $page->metadata->meta_title_du ? $page->metadata->meta_title_du : '';
                                                  $keyDE = $page->metadata->meta_keywords_du ? $page->metadata->meta_keywords_du : '';
                                                  $descDE = $page->metadata->meta_desc_du ? $page->metadata->meta_desc_du : '';
                                                }
                                                
                                                $titleIT = str_replace("'", "\\'", $titleIT);
                                                $keyIT = str_replace("'", "\\'", $keyIT);
                                                $descIT = str_replace("'", "\\'", $descIT);
                                                  
                                                $titleEN = str_replace("'", "\\'", $titleEN);
                                                $keyEN = str_replace("'", "\\'", $keyEN);
                                                $descEN = str_replace("'", "\\'", $descEN);
                                                
                                                $titleDE = str_replace("'", "\\'", $titleDE);
                                                $keyDE = str_replace("'", "\\'", $keyDE);
                                                $descDE = str_replace("'", "\\'", $descDE);
		
                                            ?>
                                                <td><a onclick="metadata('<?php echo $page->id ?>', '<?php echo $titleIT ?>', '<?php echo $keyIT ?>', '<?php echo $descIT ?>', '<?php echo $titleEN ?>', '<?php echo $keyEN ?>', '<?php echo $descEN ?>', '<?php echo $titleDE ?>', '<?php echo $keyDE ?>', '<?php echo $descDE ?>')" data-toggle="modal" data-target="#edit_metadata" tooltip="display" data-placement="top" title="Metadati" class="btn btn-primary btn-xs"><i class="fa fa-file-code-o"></i></a></td>
                                                <td>
                                                    <a href="<?php echo base_url() . 'admin/businesses/details/' . $page->id ?>" tooltip="display" data-placement="top" title="Dettaglio Ordine" class="btn btn-primary btn-xs"><i class="fa fa-folder-open"></i></a>
                                                    <a href="<?php echo base_url() . 'customer/'.$this->Orders_model->getCategoryTableName($page->category_id).'/edit/' . $page->id ?>" tooltip="display" data-placement="top" title="Modifica BusinessPage" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                    <?php if($page->category_id == 1){ ?>
                                                      <a onclick="associateHotel('<?php echo $page->id ?>')" data-toggle="modal" data-target="#associateHotel" tooltip="display" data-placement="top" title="HotelBB Association" class="btn btn-warning btn-xs"><i class="fa fa-calendar-o"></i></a>
                                                    <?php } ?>
                                                    <a onclick="editPage('<?php echo $page->id ?>')" data-toggle="modal" data-target="#edit_page" tooltip="display" data-placement="top" title="Edit Status" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>
                                                    <a onclick="deletePage('<?php echo $page->id ?>')" data-toggle="modal" data-target="#delete_page" tooltip="display" data-placement="top" title="Delete Page" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="modal fade" id="edit_metadata" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Modifica Metadati</h4>                    
                </div>
                <?php echo form_open('admin/businesses/editmetadata') ?>
                <input type="hidden" name="id" id="business_page_id" value="0"/>
                <div class="modal-body">
                  <div class="row">
                    <div class="form-group col-xs-12">
                        <div class="col-xs-12">
                          <label class="control-label" for="status">Meta description IT</label>
                          <input class="form-control" type="text" name="meta_desc_it" id="meta_desc_it">
                        </div>
                        <div class="col-xs-12">
                          <label class="control-label" for="status">Meta keywords IT</label>
                          <input class="form-control" type="text" name="meta_keywords_it"  id="meta_keywords_it">
                        </div>
                        <div class="col-xs-12">
                          <label class="control-label" for="status">Meta title IT</label>
                          <input class="form-control" type="text" name="meta_title_it" id="meta_title_it">
                        </div>
                    </div>
                    <div class="form-group col-xs-12">
                        <div class="col-xs-12">
                          <label class="control-label" for="status">Meta description EN</label>
                          <input class="form-control" type="text" name="meta_desc_en" id="meta_desc_en">
                        </div>
                        <div class="col-xs-12">
                          <label class="control-label" for="status">Meta keywords EN</label>
                          <input class="form-control" type="text" name="meta_keywords_en" id="meta_keywords_en">
                        </div>
                        <div class="col-xs-12">
                          <label class="control-label" for="status">Meta title EN</label>
                          <input class="form-control" type="text" name="meta_title_en" id="meta_title_en">
                        </div>
                    </div>
                    <div class="form-group col-xs-12">
                        <div class="col-xs-12">
                          <label class="control-label" for="status">Meta description DE</label>
                          <input class="form-control" type="text" name="meta_desc_du" id="meta_desc_du">
                        </div>
                        <div class="col-xs-12">
                          <label class="control-label" for="status">Meta keywords DE</label>
                          <input class="form-control" type="text" name="meta_keywords_du" id="meta_keywords_du">
                        </div>
                        <div class="col-xs-12">
                          <label class="control-label" for="status">Meta title DE</label>
                          <input class="form-control" type="text" name="meta_title_du"  id="meta_title_du">
                        </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i> Salva
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="edit_page" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Modifica attività</h4>
                </div>
                <?php echo form_open('admin/businesses/updatestatus') ?>
                <input type="hidden" name="id" id="business_edit_id" value="0"/>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="status">Cambia status</label>
                        <select name="status" class="form-control">
                            <option value="unpaid">Non pagato</option>
                            <option value="paid">Pagato</option>
                            <option value="expire">Scaduto</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i> Salva
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="delete_page" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background-color: #fcf8e3;color: #8a6d3b;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i> Attenzione</h4>
                </div>
                <?php echo form_open('admin/businesses/delete') ?>
                <input type="hidden" name="id" id="business_del_id" value="0"/>
                <div class="modal-body">
                    Sei sicuro di voler eliminare questa Attività?
                    <br />
                    <b>Nota:</b> Questa azione cancellerà tutti i dati associati.
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-check"></i> OK
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="associateHotel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Associazione di un Hotel da Procida a HotelBB</h4>
                </div>
                <?php echo form_open('admin/businesses/associateHotel') ?>
                <input type="hidden" name="associateHotel_id" id="associateHotel_id" value="0"/>
                <div class="modal-body">
                  <div class="text-center">
                    <select name="hotelBB_id" class="form-control">
                      <option>Seleziona un hotel</option>
                      <?php foreach ($hotel_bb as $hotel) { ?>
                        <option value="<?php echo $hotel->partner_id; ?>"><?php echo $hotel->name; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-check"></i> OK
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script type="text/javascript">
        function editPage(id) {
           $('#business_edit_id').val(id);
        }

        function deletePage(id) {
           $('#business_del_id').val(id);
        }
        function associateHotel(id) {
           $('#associateHotel_id').val(id);
        }

        function metadata(id, titleIT, keywordIT, descriptionIT, titleEN, keywordEN, descriptionEN, titleDU, keywordDU, descriptionDU) {
          $('#business_page_id').val(id);

          $('#meta_desc_it').val(descriptionIT);
          $('#meta_desc_en').val(descriptionEN);
          $('#meta_desc_du').val(descriptionDU);

          $('#meta_keywords_it').val(keywordIT);
          $('#meta_keywords_en').val(keywordEN);
          $('#meta_keywords_du').val(keywordDU);

          $('#meta_title_it').val(titleIT);
          $('#meta_title_en').val(titleEN);
          $('#meta_title_du').val(titleDU);
       }
    </script>
