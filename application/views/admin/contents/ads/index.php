<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-bookmark"></i> Gestione Ad's</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ad's Disponibili
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="ads-dtable">
                            <thead>
                                <tr>
                                    <th style="width:50px">S.No</th>
                                    <th>Nome</th>
                                    <th>Tipo</th>
                                    <th>Contenuto</th>
                                    <th style="width:80px;">Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($ads)): ?>
                                    <?php if (is_array($ads)): ?>
                                        <?php $counter = 1; ?>
                                        <?php foreach ($ads as $add): ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $counter ?></td>
                                                <td><?php echo $add->name ?></td>
                                                <td><?php echo $add->type ?></td>
                                                <td><code><?php echo $add->type =='image'?base64_decode($add->src):'JAVASCRIPT CODE'; ?></code></td>
                                                <td>
                                                    <a onclick="displayEditForm('<?php echo $add->id ?>', '<?php echo $add->name ?>', '<?php echo $add->type ?>', '<?php echo $add->src; ?>')"  class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->

    <div class="modal fade" id="edit-add" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> <span id='add_title'></span></h4>
                </div>
                <?php echo form_open(); ?>
                <input type="hidden" name="id" id="edit_id" value="0"/>
                <div class="modal-body">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="">Tipo di contenuto</label>
                            <label class="radio-inline">
                                <input type="radio" name="type" id="type1" value="image"> Immagine
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="type" id="type2" value="javascript"> Javascript
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Aggiungi Contenuto</label>
                            <textarea class="form-control" id='content' name='src'></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <small><i class="fa fa-info-circle"></i> Nota: Inserisci una URL se il tipo di contenuto è un'immagine.</small>
                    </div>
                    <div class='clearfix'></div>

                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i> Salva
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script type="text/javascript">
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
        function displayEditForm(id, title, type, content) {
            document.getElementById("edit_id").value = id;
            document.getElementById("add_title").innerHTML = title;
            document.getElementById("content").value = Base64.decode(content);
            if(type == 'image'){
                document.getElementById("type1").checked = true;
                document.getElementById("type2").checked = false;
            }else{
                document.getElementById("type1").checked = false;
                document.getElementById("type2").checked = true;
            }
            $('#edit-add').modal("show");
        }
    </script>
