<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-users"></i> Clienti</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   Clienti registrati
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="customers">
                            <thead>
                                <tr>
                                    <th style="width:80px">S.No</th>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Società</th>
                                    <th>Attivo</th>
                                    <th style="width:110px;">Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($customers)): ?>
                                    <?php if (is_array($customers)): ?>
                                        <?php $counter = 1; ?>
                                        <?php foreach ($customers as $customer): ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $counter ?></td>
                                                <td><?php echo $customer->firstName ?> <?php echo $customer->lastName ?></td>
                                                <td><?php echo $customer->email ?></td>
                                                <td><?php echo $customer->company ?></td>
                                                <td>
																									<?php if($customer->active == 1){?> 
																										<div style="cursor:default;" class="btn btn-success btn-xs"><i class="fa fa-check"></i></div>                                                  
																									<?php }else{?> 
																										<div style="cursor:default;" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></div>                                                  
																									<?php }?>
                                                </td>
                                                <td>
                                                    <!-- <a href="<?php echo base_url() . 'admin/customers/details/' . $customer->id ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder-open"></i></a> -->
                                                    <a onclick="deleteCustomer('<?php echo $customer->id ?>')" data-toggle="modal" data-target="#delete_customer" tooltip="display" data-placement="top" title="Delete Page" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="modal fade" id="delete_customer" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background-color: #fcf8e3;color: #8a6d3b;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i> Attenzione</h4>
                </div>
                <?php echo form_open('admin/customers/delete') ?>
                <input type="hidden" name="id" id="customer_del_id" value="0"/>
                <div class="modal-body">
                    Sei sicuro di voler eliminare questo cliente?
                    <br />
                    <b>Nota:</b> Questa azione cancellerà tutti i dati associati.
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-check"></i> OK
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script type="text/javascript">

        function deleteCustomer(id) {
           $('#customer_del_id').val(id);
        }
    </script>
