<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">

            <li>
                <a class="<?php if ($cpage == 'dashboard') echo 'active' ?>" href="<?php echo base_url() . 'admin/dashboard' ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li class='<?php echo isset($sub_cpage)&&($sub_cpage=='all-posts' || $sub_cpage=='new-post')?'active':'' ?>'>
                <a href="#" class="<?php if ($cpage == 'blog') echo 'active' ?>">
                    <i class="fa fa-thumb-tack fa-fw"></i> Blog<span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url() ?>admin/blog/addpost" class="<?php echo (isset($sub_cpage) && $sub_cpage=='new-post')?'active':'' ?>">Nuovo Post</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>admin/blog" class="<?php echo (isset($sub_cpage) && $sub_cpage=='all-posts')?'active':'' ?>">Tutti i Posts</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a class="<?php if ($cpage == 'ads') echo 'active' ?>" href="<?php echo base_url() . 'admin/ads' ?>"><i class="fa fa-bookmark fa-fw"></i> Gestione Ad's</a>
            </li>
            <li>
                <a class="<?php if ($cpage == 'tag') echo 'active' ?>" href="<?php echo base_url() . 'admin/tags' ?>"><i class="fa fa-tags fa-fw"></i> Gestione Tag</a>
            </li>
            <li>
                <a class="<?php if ($cpage == 'activityservices') echo 'active' ?>" href="<?php echo base_url() . 'admin/ActivityServices' ?>"><i class="fa fa-ticket fa-fw"></i> Gestione Servizi</a>
            </li>
             <li class='<?php echo isset($sub_cpage)&&($sub_cpage=='all-locations' || $sub_cpage=='new-location')?'active':'' ?>'>
                <a href="#" class="<?php if ($cpage == 'locations') echo 'active' ?>">
                    <i class="fa fa-location-arrow fa-fw"></i> Località<span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url() ?>admin/location/add" class="<?php echo (isset($sub_cpage) && $sub_cpage=='new-location')?'active':'' ?>">Nuova Località</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>admin/location" class="<?php echo (isset($sub_cpage) && $sub_cpage=='all-locations')?'active':'' ?>">Tutte</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
           <li class='<?php echo isset($sub_cpage)&&($sub_cpage=='all-beaches' || $sub_cpage=='new-beach')?'active':'' ?>'>
                <a href="#" class="<?php if ($cpage == 'beaches') echo 'active' ?>">
                    <i class="fa fa-flag fa-fw"></i> Spiagge<span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url() ?>admin/beach/add" class="<?php echo (isset($sub_cpage) && $sub_cpage=='new-beach')?'active':'' ?>">Nuova Spiaggia</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>admin/beach" class="<?php echo (isset($sub_cpage) && $sub_cpage=='all-beaches')?'active':'' ?>">Tutte</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
             <li class='<?php echo isset($sub_cpage)&&($sub_cpage=='all-blocks' || $sub_cpage=='new-block')?'active':'' ?>'>
                <a href="#" class="<?php if ($cpage == 'block') echo 'active' ?>">
                    <i class="fa fa-flag fa-info-circle"></i> Blocchi di informazioni<span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url() ?>admin/block/addblock" class="<?php echo (isset($sub_cpage) && $sub_cpage=='new-block')?'active':'' ?>">Nuovo Blocco</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>admin/block" class="<?php echo (isset($sub_cpage) && $sub_cpage=='all-blocks')?'active':'' ?>">Tutti</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li class='<?php echo isset($sub_cpage)&&($sub_cpage=='all-places' || $sub_cpage=='new-place')?'active':'' ?>'>
                <a href="#" class="<?php if ($cpage == 'places') echo 'active' ?>">
                    <i class="fa fa-image fa-fw"></i> Luoghi di interesse<span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url() ?>admin/places_of_interest/add" class="<?php echo (isset($sub_cpage) && $sub_cpage=='new-place')?'active':'' ?>">Nuovo luogo</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>admin/places_of_interest" class="<?php echo (isset($sub_cpage) && $sub_cpage=='all-places')?'active':'' ?>">Tutti</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li class='<?php echo isset($sub_cpage)&&($sub_cpage=='all-events' || $sub_cpage=='new-event')?'active':'' ?>'>
                <a href="#" class="<?php if ($cpage == 'events') echo 'active' ?>">
                    <i class="fa fa-calendar fa-fw"></i> Eventi<span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url() ?>admin/events/add" class="<?php echo (isset($sub_cpage) && $sub_cpage=='new-event')?'active':'' ?>">Nuovo evento</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>admin/events" class="<?php echo (isset($sub_cpage) && $sub_cpage=='all-events')?'active':'' ?>">Tutti</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <?php $class = ''; $submenuClass =''; $allCatTables = ['hotels','resturant','shopes','apartments','bars_cafe', 'boat_rentals', 'weddings', 'beach_clubs','nightlife', 'services'];
              if(isset($sub_cpage) && in_array($sub_cpage, $allCatTables)){
                $submenuClass = 'active';
              }
              else if($cpage == 'businesses'){
                $class = 'active';
              }
             ?>

            <li class='<?php echo ($class!='' ?  $class :  $submenuClass); ?>'>
                <a href="#" class="<?php echo ($class!='' ?  $class :  $submenuClass); ?>">
                    <i class="fa fa-file fa-fw"></i> Attività<span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                  <li>
                      <a class="<?php  echo $class; ?>" href="<?php echo base_url() . 'admin/businesses' ?>"><i class="fa fa-file fa-fw"></i> Tutte le Attività</a>
                  </li>
                  <?php foreach ($all_categories as $key => $category): ?>

                    <?php  $classActiveSub = ''; if(isset($sub_cpage) && $sub_cpage == $category->table) {$classActiveSub = 'active'; } ?>
                    <li>
                        <a href="<?php echo base_url() ?>admin/businesses/activity/<?php echo $category->table; ?>" class="<?php echo $classActiveSub; ?>"><i class="fa fa-fw <?php echo $category->icon; ?>"></i> <?php echo $category->name; ?></a>
                    </li>
                  <?php endforeach; ?>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a class="<?php if ($cpage == 'pricing') echo 'active' ?>" href="<?php echo base_url() . 'admin/pricing' ?>"><i class="fa fa-money fa-fw"></i> Gestione prezzi</a>
            </li>
            <li>
                <a class="<?php if ($cpage == 'customers') echo 'active' ?>" href="<?php echo base_url() . 'admin/customers' ?>"><i class="fa fa-users fa-fw"></i> Clienti</a>
            </li>
            <li>
                <a class="<?php if ($cpage == 'settings') echo 'active' ?>" href="<?php echo base_url() . 'admin/dashboard/settings' ?>"><i class="fa fa-wrench fa-fw"></i> Impostazioni</a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>
