<br>
<footer>
    <center> <small style="color:gray;">Tutti i diritti riservati &copy; <?php echo date('Y'); ?> <a href="">VisitProcida</a></small></center>
</footer>
</div>
</div>
<!-- /#wrapper -->

<!-- jQuery Version 1.11.0 -->
<!-- <script src="<?php echo base_url() ?>assets/admin/js/jquery-1.11.0.js"></script> -->
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>

  <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>

  <script type="text/javascript">
    var count = $('#home_heighlight_count').val();
    var zoneCount = $('#zone_count').val();
    $('#btn-add').click(function(){
        var html  = '';
        count = count + 1;

        html += '<div class="form-group housining-div highlight-div-'+count+' mt-2"><input type="text" name="amenities_name[]" placeholder="Enter Amenities Name" class="form-control"><button type="button" class="btn btn-danger" onclick="removeValues(this)" data-removevalue='+count+' id="btn-remove"><i class="fa fa-minus"></i></button></div>';
        $('.main-highlight-div').append(html);
    });

    $('#zone-btn-add').click(function(){
      var html  = '';
      zoneCount = zoneCount + 1;

      html += '<div class="form-group zone-div highlight-div-'+zoneCount+' mb-2"><label class="control-label" for="categories">Zone Name</label><input type="text" name="zone_name[]" placeholder="Enter Zone Name" class="form-control" required="required"></div><div class="form-group zone-div highlight-div-'+zoneCount+' mb-2"><label class="control-label" for="categories">(SJD) San José del Cabo Price Price</label><input type="text" name="sjd_price[]" placeholder="Enter SJD Price" class="form-control" required="required"></div><div class="form-group zone-div highlight-div-'+zoneCount+' mb-2"><label class="control-label" for="categories">(CSL) Cabo San Lucas Price Price</label><input type="text" name="csl_price[]" placeholder="Enter CSL Price" class="form-control" required="required"></div><button type="button" class="btn btn-danger highlight-div-'+zoneCount+'" onclick="removeZoneValues(this)" data-removezonevalue='+zoneCount+' id="btn-remove"><i class="fa fa-minus"></i></button>';
      $('.main-zone-div').append(html);
  });

    function removeValues(_this){
      var removeSection = $(_this).data('removevalue');
      $('.highlight-div-' + removeSection).remove();
    }

    function removeZoneValues(_this){
      var removeSection = $(_this).data('removezonevalue');
      $('.highlight-div-' + removeSection).remove();
    }
</script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url() ?>assets/admin/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url() ?>assets/admin/js/plugins/metisMenu/metisMenu.min.js"></script>

<!-- add addtional js here  -->
<?php if (isset($jsfiles)): ?>
    <?php foreach ($jsfiles as $js): ?>
        <?php if ($js['type'] == 'file'): ?>
            <script src="<?php echo $js['src'] ?>" type="text/javascript"></script>
        <?php else: ?>
            <script type="text/javascript">
            <?php echo $js['src'] ?>
            </script>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url() ?>assets/admin/js/sb-admin-2.js"></script>

<?php if(isset($stats) || isset($contactStats)){ ?>
<!-- Resources -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<?php } ?>
<!-- Chart code -->
<?php if(isset($stats)){ ?>
<script>
var chart = AmCharts.makeChart("chartdiv", {
  "type": "serial",
  "theme": "light",
  "dataProvider": [ {
    "month": "Gen",
    "views": <?php echo $stats[0]; ?>
  }, {
    "month": "Feb",
    "views": <?php echo $stats[1]; ?>
  }, {
    "month": "Mar",
    "views": <?php echo $stats[2]; ?>
  }, {
    "month": "Apr",
    "views": <?php echo $stats[3]; ?>
  }, {
    "month": "Mag",
    "views": <?php echo $stats[4]; ?>
  }, {
    "month": "Giu",
    "views": <?php echo $stats[5]; ?>
  }, {
    "month": "Lug",
    "views": <?php echo $stats[6]; ?>
  }, {
    "month": "Ago",
    "views": <?php echo $stats[7]; ?>
  }, {
    "month": "Set",
    "views": <?php echo $stats[8]; ?>
  }, {
    "month": "Ott",
    "views": <?php echo $stats[9]; ?>
  }, {
    "month": "Nov",
    "views": <?php echo $stats[10]; ?>
  }, {
    "month": "Dic",
    "views": <?php echo $stats[11]; ?>
  }],
  "valueAxes": [ {
    "gridColor": "#FFFFFF",
    "gridAlpha": 0.2,
    "dashLength": 0
  } ],
  "gridAboveGraphs": true,
  "startDuration": 1,
  "graphs": [ {
    "balloonText": "[[category]]: <b>[[value]]</b>",
    "fillAlphas": 0.8,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "views"
  } ],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "month",
  "categoryAxis": {
    "gridPosition": "start",
    "gridAlpha": 0,
    "tickPosition": "start",
    "tickLength": 20
  },
  "export": {
    "enabled": true
  }

});
</script>
<?php } ?>

<?php if(isset($contactStats)){ ?>
<script>
  var chart = AmCharts.makeChart("contactChart", {
    "type": "serial",
    "theme": "light",
    "dataProvider": [ {
      "month": "Gen",
      "views": <?php echo $contactStats[0]; ?>
    }, {
      "month": "Feb",
      "views": <?php echo $contactStats[1]; ?>
    }, {
      "month": "Mar",
      "views": <?php echo $contactStats[2]; ?>
    }, {
      "month": "Apr",
      "views": <?php echo $contactStats[3]; ?>
    }, {
      "month": "Mag",
      "views": <?php echo $contactStats[4]; ?>
    }, {
      "month": "Giu",
      "views": <?php echo $contactStats[5]; ?>
    }, {
      "month": "Lug",
      "views": <?php echo $contactStats[6]; ?>
    }, {
      "month": "Ago",
      "views": <?php echo $contactStats[7]; ?>
    }, {
      "month": "Set",
      "views": <?php echo $contactStats[8]; ?>
    }, {
      "month": "Ott",
      "views": <?php echo $contactStats[9]; ?>
    }, {
      "month": "Nov",
      "views": <?php echo $contactStats[10]; ?>
    }, {
      "month": "Dic",
      "views": <?php echo $contactStats[11]; ?>
    }],
    "valueAxes": [ {
      "gridColor": "#FFFFFF",
      "gridAlpha": 0.2,
      "dashLength": 0
    } ],
    "gridAboveGraphs": true,
    "startDuration": 1,
    "graphs": [ {
      "balloonText": "[[category]]: <b>[[value]]</b>",
      "fillAlphas": 0.8,
      "lineAlpha": 0.2,
      "type": "column",
      "valueField": "views"
    } ],
    "chartCursor": {
      "categoryBalloonEnabled": false,
      "cursorAlpha": 0,
      "zoomable": false
    },
    "categoryField": "month",
    "categoryAxis": {
      "gridPosition": "start",
      "gridAlpha": 0,
      "tickPosition": "start",
      "tickLength": 20
    },
    "export": {
      "enabled": true
    }

  });
</script>
<?php } ?>

</body>

</html>
