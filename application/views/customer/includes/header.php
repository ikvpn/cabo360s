<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Easy Island | Pannello clienti</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url() ?>assets/admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url() ?>assets/admin/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <?php if(isset($cssfiles)):?>
             <?php foreach($cssfiles as $css): ?>
                  <link href="<?php echo $css['src'] ?>" rel="stylesheet" type="text/css" />
             <?php endforeach; ?>
        <?php endif; ?>

    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>assets/admin/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url() ?>assets/admin/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        #nostyle{
            padding-left: 0;
            padding-right: 0;
        }
        nav .fa-ship:before {
            content: "";
            background-image: url(<?php echo base_url() ?>/assets/images/ship.png);
            width: 14px;
            height: 14px;
            position: absolute;
            background-size: contain;
            left: 16px;
            margin: 0 auto;
            top: 13px;
        }
    </style>
    <!-- DYNAMICALLY ADDED FILES -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url().'index.php/customer/dashboard'?>">Visit Procida - Pannello clienti</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                 <?php if ($this->session->userdata('usertype') == 'admin'): ?>
                <li style="color:#8a6d3b;">
                    You are logged in as admin. <a id="nostyle" href="<?php echo base_url(); ?>admin/dashboard" style="display:inline;">Click Here</a> to go to admin dashboard.
                </li>
                <?php endif; ?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo base_url().'customer/dashboard/logout' ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
