<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a class="<?php if ($cpage == 'dashboard') echo 'active' ?>" href="<?php echo base_url() . $this->lang->lang() . '/customer/dashboard' ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a class="<?php if ($cpage == 'register') echo 'active' ?>" href="<?php echo base_url() . $this->lang->lang() . '/customer/register_business' ?>"><i class="fa fa-bookmark fa-fw"></i> Registra Attività</a>
            </li>
            <?php $cats = $this->Customer_model->getCategories(); ?>

            <?php if ($cats && is_array($cats)): ?>
                <?php foreach ($cats as $cat): ?>
                    <?php if ($cat->table == 'shops'): ?>
                        <li>
                            <a class="<?php if ($cpage == "shopes") echo 'active' ?>" href="<?php echo base_url() . $this->lang->lang() . '/customer/shopes' ?>"><i class="fa <?php echo $cat->icon ?> fa-fw"></i> <?php echo lang($cat->table) ?></a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a class="<?php if ($cpage == $cat->table) echo 'active' ?>" href="<?php echo base_url() . $this->lang->lang() . '/customer/' . $cat->table ?>"><i class="fa <?php echo $cat->icon ?> fa-fw"></i> <?php echo lang($cat->table) ?></a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <li class='<?php echo isset($sub_cpage)&&($sub_cpage=='tours' || $sub_cpage=='new-tour')?'active':'' ?>'>
               <a href="#" class="<?php if ($cpage == 'tours') echo 'active' ?>">
                   <i class="fa fa-location-arrow fa-fw"></i> Tours<span class="fa arrow"></span>
               </a>
               <ul class="nav nav-second-level">
                   <li>
                       <a href="<?php echo base_url() ?>customer/tours/add" class="<?php echo (isset($sub_cpage) && $sub_cpage=='new-location')?'active':'' ?>">Nuovo Tour</a>
                   </li>
                   <li>
                       <a href="<?php echo base_url() ?>customer/tours" class="<?php echo (isset($sub_cpage) && $sub_cpage=='all-locations')?'active':'' ?>">Tutti</a>
                   </li>
               </ul>
               <!-- /.nav-second-level -->
           </li>
           <li class='<?php echo isset($sub_cpage)&&($sub_cpage=='transfer' || $sub_cpage=='new-transfer')?'active':'' ?>'>
               <a href="#" class="<?php if ($cpage == 'transfer') echo 'active' ?>">
                   <i class="fa fa-exchange fa-fw"></i> Transfer<span class="fa arrow"></span>
               </a>
               <ul class="nav nav-second-level">
                   <li>
                       <a href="<?php echo base_url() ?>customer/transfer/add" class="<?php echo (isset($sub_cpage) && $sub_cpage=='new-location')?'active':'' ?>">Nuovo Transfer</a>
                   </li>
                   <li>
                       <a href="<?php echo base_url() ?>customer/transfer" class="<?php echo (isset($sub_cpage) && $sub_cpage=='all-locations')?'active':'' ?>">Tutti</a>
                   </li>
               </ul>
               <!-- /.nav-second-level -->
           </li>
            <?php if($this->session->userdata('usertype') != 'admin'): ?>
            <li>
                <a class="<?php if ($cpage == 'settings') echo 'active' ?>" href="<?php echo base_url() . $this->lang->lang() . '/customer/settings' ?>"><i class="fa fa-wrench fa-fw"></i> <?php echo lang('settings'); ?></a>
            </li>
            <?php endif; ?>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>
<div id="page-wrapper">
