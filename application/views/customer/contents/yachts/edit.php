<?php

$divID = ($usertype == 'admin') ? 'page-wrapper' : '';

?>
<style type="text/css">
    .housining-div .form-control,
    .input-file {
        width: calc(100% - 50px);
    }
    .housining-div {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        width: 100%;
        align-items: self-start;
    }
    .input-file .form-control{
        width: 100%;
    }
    .input-file img{
        width: 60px;
        height: 60px;
        padding: 2px;
        object-fit: cover;
        border: 1px solid #ccc;
        margin: 10px 0 0 0;
        border-radius: 2px;
    }
</style>

<div id=<?php echo $divID ?>>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-home"></i> Ville</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <?php if (validation_errors()): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo validation_errors(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12">
            <?php echo form_open_multipart(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->Orders_model->title; ?>
                </div>
                <div class="panel-body">
                    <input type="hidden" name="id" value="<?php echo isset($this->Yacht_model->id) ? $this->Yacht_model->id : '0'; ?>">
                    <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>

                    </ul>
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="enName">Nome visualizzato [en]</label>
                                    <input type="text" name="enName" id="enName" placeholder="Enter Name in English" value="<?php echo (set_value('enName') ? set_value('enName') : (isset($this->Yacht_model->enName) ? $this->Yacht_model->enName : '')); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <label class="control-label" for="en_editor">Descrizione [en]</label>
                                <textarea id="en_editor" name="enDescription" rows="5" cols="80" placeholder="Enter page description for English">
                                    <?php echo set_value('enDescription') ? set_value('enDescription') : (isset($this->Yacht_model->enDescription) ? $this->Yacht_model->enDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="duName">Nome visualizzato [de]</label>
                                    <input type="text" name="duName" id="duName" placeholder="Enter Name in Duetch" value="<?php echo set_value('duName') ? set_value('duName') : (isset($this->Yacht_model->duName) ? $this->Yacht_model->duName : ''); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12" style="margin-top:10px">
                                <label class="control-label" for="du_editor">Descrizione [de]</label>
                                <textarea id="du_editor" name="duDescription" rows="5" cols="80" placeholder="Enter page description for Deutch">
                                    <?php echo set_value('duDescription') ? set_value('duDescription') : (isset($this->Yacht_model->duDescription) ? $this->Yacht_model->duDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade in active">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="itName">Nome visualizzato [it]</label>
                                    <input type="text" name="itName" id="itName" placeholder="Enter Name in Italian" value="<?php echo set_value('itName') ? set_value('itName') : (isset($this->Yacht_model->itName) ? $this->Yacht_model->itName : ''); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12" style="margin-top:10px">
                                <label class="control-label" for="it_editor">Descrizione [it]</label>
                                <textarea id="it_editor" name="itDescription" rows="5" cols="80" placeholder="Enter page description for Italian">
                                    <?php echo set_value('itDescription') ? set_value('itDescription') : (isset($this->Yacht_model->itDescription) ? $this->Yacht_model->itDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 15px;"></div>

                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Latitudine</label>
                            <input type="text" name="lat" value="<?php echo set_value('lat') ? set_value('lat') : (isset($this->Yacht_model->lat) ? $this->Yacht_model->lat : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lng">Longitudine</label>
                            <input type="text" name="lng" value="<?php echo set_value('lng') ? set_value('lng') : (isset($this->Yacht_model->lng) ? $this->Yacht_model->lng : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group" style="padding-top:30px;">
                            <a  class="btn btn-primary btn-xs" href="http://www.gps-coordinates.net/" target="_blank">Trova Lat/Lng</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="dailyPrice">Prezzo giornaliero</label>
                            <input type="text" name="dailyPrice" value="<?php echo set_value('dailyPrice') ? set_value('dailyPrice') : (isset($this->Yacht_model->dailyPrice) ? $this->Yacht_model->dailyPrice : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="weeklyPrice">Prezzo settimanale</label>
                            <input type="text" name="weeklyPrice" value="<?php echo set_value('weeklyPrice') ? set_value('weeklyPrice') : (isset($this->Yacht_model->weeklyPrice) ? $this->Yacht_model->weeklyPrice : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="unit">Prezzo</label>
                            <select class="form-control" name="unit">
                                <option value="N" <?php echo set_select('unit', 'N') ? set_select('unit', 'N') : (isset($this->Yacht_model->unit) && $this->Yacht_model->unit == 'N' ? 'selected="selected"' : '') ?>><?php echo lang('per_night'); ?></option>
                                <option value="D" <?php echo set_select('unit', 'D') ? set_select('unit', 'D') : (isset($this->Yacht_model->unit) && $this->Yacht_model->unit == 'D' ? 'selected="selected"' : '') ?>><?php echo lang('per_day'); ?></option>
                                <option value="W" <?php echo set_select('unit', 'W') ? set_select('unit', 'W') : (isset($this->Yacht_model->unit) && $this->Yacht_model->unit == 'W' ? 'selected="selected"' : '') ?>><?php echo lang('per_week'); ?></option>
                                <option value="M" <?php echo set_select('unit', 'M') ? set_select('unit', 'M') : (isset($this->Yacht_model->unit) && $this->Yacht_model->unit == 'M' ? 'selected="selected"' : '') ?>><?php echo lang('per_month'); ?></option>
                                <option value="Y" <?php echo set_select('unit', 'Y') ? set_select('unit', 'Y') : (isset($this->Yacht_model->unit) && $this->Yacht_model->unit == 'Y' ? 'selected="selected"' : '') ?>><?php echo lang('per_year'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="bedrooms">Cabine</label>
                            <input type="text" name="bedrooms" value="<?php echo set_value('bedrooms') ? set_value('bedrooms') : (isset($this->Yacht_model->bedrooms) ? $this->Yacht_model->bedrooms : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="beds">Letti</label>
                            <input type="text" name="beds" value="<?php echo set_value('beds') ? set_value('beds') : (isset($this->Yacht_model->beds) ? $this->Yacht_model->beds : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="guests">Guests</label>
                            <input type="text" name="guests" value="<?php echo set_value('guests') ? set_value('guests') : (isset($this->Yacht_model->guests) ? $this->Yacht_model->guests : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="trypology">Piedi</label>
                            <input type="text" name="trypology" value="<?php echo set_value('trypology') ? set_value('trypology') : (isset($this->Yacht_model->trypology) ? $this->Yacht_model->trypology : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <!-- <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="bathrooms">Bagni</label>
                            <input type="text" name="bathrooms" value="<?php echo set_value('bathrooms') ? set_value('bathrooms') : (isset($this->Yacht_model->bathrooms) ? $this->Yacht_model->bathrooms : ''); ?>" class="form-control">
                        </div>
                    </div> -->
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="manager">Nome del manager</label>
                            <input type="text" name="manager" value="<?php echo set_value('manager') ? set_value('manager') : (isset($this->Yacht_model->manager) ? $this->Yacht_model->manager : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="m_image">Foto del manager</label>
                            <input type="file"  name="m_image" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="phone">Numero di telefono</label>
                            <input type="text" name="phone" value="<?php echo set_value('phone') ? set_value('phone') : (isset($this->Yacht_model->phone) ? $this->Yacht_model->phone : ''); ?>" class="form-control">
                        </div>
                    </div>

                  <!-- Housining Highlights Section Start -->
                  <div class="col-lg-12">
                      <div class="panel panel-default">
                          <div class="panel-heading">Yachts Highlights</div>
                          <div class="panel-body">
                              <div class="row">
                                  <div class="col-lg-12 col-md-12 col-xs-12 main-highlight-div">
                                    <?php 
                                        if(isset($this->Yacht_model->yachtHomeHeighlights) && $this->Yacht_model->yachtHomeHeighlights != ""){ 
                                        $unSerializeHeighlights = unserialize($this->Yacht_model->yachtHomeHeighlights);
                                        echo '<input type="hidden" name="home_heighlight_count" id="home_heighlight_count" value="'.count($unSerializeHeighlights).'">';
                                        foreach ($unSerializeHeighlights as $key => $unSerializeHeighlight) {
                                    ?>
                                        <div class="form-group housining-div highlight-div-<?= $key + 1 ?> mb-2">
                                          <input type="text" name="yachtHomeHeighlights[]" value="<?php echo $unSerializeHeighlight['name']; ?>" placeholder="Enter Amenities Name" class="form-control">
                                          <?php if($key == 0){ ?>
                                            <button type="button" class="btn btn-info" id="btn-add"><i class="fa fa-plus"></i></button>
                                          <?php }else{ ?>
                                            <button type="button" class="btn btn-danger" onclick="removeValues(this)" data-removevalue="<?= $key + 1 ?>" id="btn-remove"><i class="fa fa-minus"></i></button>
                                          <?php } ?>
                                        </div>
                                    <?php }}else{ ?>
                                        <input type="hidden" name="home_heighlight_count" id="home_heighlight_count" value="1">
                                        <div class="form-group housining-div highlight-div-1 mb-2">
                                          <input type="text" name="yachtHomeHeighlights[]" placeholder="Enter Amenities Name" class="form-control">
                                          <button type="button" class="btn btn-info" id="btn-add"><i class="fa fa-plus"></i></button>
                                        </div>
                                    <?php } ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- Housining Highlights Section End -->
                </div>
                <div class="panel-footer" style="text-align: right;">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>