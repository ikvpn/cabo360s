<?php


?>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> <i class="fa fa-location-arrow"></i> Tours</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo lang('registerd_events') ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="hotels-dtable">
                            <thead>
                                <tr>
                                    <th style="width:50px">S.No</th>
                                    <th>Name</th>
                                    <th>Location</th>
                                    <th style="width:80px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($tours)): ?>
                                    <?php if (is_array($tours)): ?>
                                        <?php $counter = 1; ?>
                                        <?php foreach ($tours as $tour): ?>

                                            <tr>
                                                <td style="text-align: center;"><?php echo $counter ?></td>
                                                <td><?php echo $tour->itTitle ?></td>
                                                <td><?php echo $this->Tour_model->getLocationName($tour->locationId); ?></td>
                                                <td>
                                                  <a href="<?php echo base_url().$this->lang->lang() ?>/customer/tours/edit/<?php echo $tour->id ?>"  class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                  <a onclick="displayDeleteForm('<?php echo $tour->id ?>')" data-toggle="modal" data-target="#delete-beach" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php $counter++; ?>

                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="modal fade" id="delete-beach" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background-color: #fcf8e3;color: #8a6d3b;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-warning"></i> Attenzione</h4>
                </div>
                <?php echo form_open('customer/tours/delete') ?>
                <input type="hidden" name="id" id="del_id" value="0"/>
                <div class="modal-body">
                    Sei sicuro di voler cancellare il tour?
                    <br />
                    <b>Nota:</b> Questa azione cancellerà tutti i dati associati.
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-check"></i> OK
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Annulla</button>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script type="text/javascript">
        function displayDeleteForm(id) {
            document.getElementById("del_id").value = id;
        }
    </script>
