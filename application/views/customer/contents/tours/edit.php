<?php
 $divID = ($usertype == 'admin') ? 'page-wrapper' : '';
?>
<style>
    fieldset{
        border: 1px groove #ddd !important;
        padding: 0 0.4em 0.4em 0.4em !important;
        margin: 0 0 0.6em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }
    legend{
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;
        font-size: 11px;
        margin-bottom: 0;
    }
    .bootstrap-tagsinput {
      display: block !important;
    }
</style>
<div id=<?php echo $divID ?>>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-calendar"></i> Tours</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <?php if (validation_errors()): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo validation_errors(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12">
            <?php echo form_open(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->Tour_model->itTitle; ?>
                </div>
                <div class="panel-body">
                    <input type="hidden" name="id" value="<?php echo isset($this->Tour_model->id) ? $this->Tour_model->id : '0'; ?>">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>

                    </ul>
                    <div class="tab-content">
                      <div id="menu2" class="tab-pane fade  in active">
                          <br>
                          <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="itTitle">Nome visualizzato [it]</label>
                                  <input type="text" name="itTitle" id="itTitle" placeholder="Enter Name in Italian" value="<?php echo set_value('itTitle') ? set_value('itTitle') : (isset($this->Tour_model->itTitle) ? $this->Tour_model->itTitle : ''); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-xs-12" style="margin-top:10px">
                              <label class="control-label" for="it_editor">Descrizione [it]</label>
                              <textarea id="it_editor" name="itDescription" rows="5" cols="80" placeholder="Enter page description for Italian">
                                  <?php echo set_value('itDescription') ? set_value('itDescription') : (isset($this->Tour_model->itDescription) ? $this->Tour_model->itDescription : ''); ?>
                              </textarea>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="itServicesIncluded">Servizi Inclusi [it]</label>
                                  <textarea class="form-control" id="itServicesIncluded" style="height:100px" name="itServicesIncluded"><?php echo set_value('itServicesIncluded') ? set_value('itServicesIncluded') : (isset($this->Tour_model->itServicesIncluded) ? $this->Tour_model->itServicesIncluded : ''); ?></textarea>
                              </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="itServicesNotIncluded">Servizi Non Inclusi [it]</label>
                                  <textarea class="form-control" id="itServicesNotIncluded" style="height:100px" name="itServicesNotIncluded"><?php echo set_value('itServicesNotIncluded') ? set_value('itServicesNotIncluded') : (isset($this->Tour_model->itServicesNotIncluded) ? $this->Tour_model->itServicesNotIncluded : ''); ?></textarea>
                              </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="itPolicy">Policy di cancellazione e prenotazione [it]</label>
                                  <textarea class="form-control" id="itPolicy" style="height:100px" name="itPolicy"><?php echo set_value('itPolicy') ? set_value('itPolicy') : (isset($this->Tour_model->itPolicy) ? $this->Tour_model->itPolicy : ''); ?></textarea>
                              </div>
                          </div>
                      </div>
                      <div id="home" class="tab-pane fade">
                          <br>
                          <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="enTitle">Display Name [en]</label>
                                  <input type="text" name="enTitle" id="entitle" placeholder="Enter Title in English" value="<?php echo (set_value('enTitle') ? set_value('enTitle') : (isset($this->Tour_model->enTitle) ? $this->Tour_model->enTitle : '')); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-xs-12">
                              <label class="control-label" for="en_editor">Description [en]</label>
                              <textarea id="en_editor" name="enDescription" rows="5" cols="80" placeholder="Enter page description for English">
                                  <?php echo set_value('enDescription') ? set_value('enDescription') : (isset($this->Tour_model->enDescription) ? $this->Tour_model->enDescription : ''); ?>
                              </textarea>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="enServicesIncluded">Servizi Inclusi [en]</label>
                                  <textarea class="form-control" id="enServicesIncluded" style="height:100px" name="enServicesIncluded"><?php echo set_value('enServicesIncluded') ? set_value('enServicesIncluded') : (isset($this->Tour_model->enServicesIncluded) ? $this->Tour_model->enServicesIncluded : ''); ?></textarea>
                              </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="enServicesNotIncluded">Servizi Non Inclusi [en]</label>
                                  <textarea class="form-control" id="enServicesNotIncluded" style="height:100px" name="enServicesNotIncluded"><?php echo set_value('enServicesNotIncluded') ? set_value('enServicesNotIncluded') : (isset($this->Tour_model->enServicesNotIncluded) ? $this->Tour_model->enServicesNotIncluded : ''); ?></textarea>
                              </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="enPolicy">Policy di cancellazione e prenotazione [en]</label>
                                  <textarea class="form-control" id="enPolicy" style="height:100px" name="enPolicy"><?php echo set_value('enPolicy') ? set_value('enPolicy') : (isset($this->Tour_model->enPolicy) ? $this->Tour_model->enPolicy : ''); ?></textarea>
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 15px;"></div>
                    <fieldset class="my_fileds">
                        <legend class="my_fileds">Periodo di validità</legend>
                        <div class="col-xs-12">
                          <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="startDate">Data di inizio</label>
                                  <input type="text" name="dateStart" placeholder="MM/DD/YYYY" value="<?php echo set_value('dateStart') ? set_value('dateStart') : (isset($this->Tour_model->dateStart) ? substr($this->Tour_model->dateStart, 5, 2) . '/' . substr($this->Tour_model->dateStart, 8, 2) . '/' . substr($this->Tour_model->dateStart, 0, 4) : ''); ?>" class="form-control datepicker">
                              </div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="endDate">Data di fine</label>
                                  <input type="text" name="dateEnd" placeholder="MM/DD/YYYY" value="<?php echo set_value('dateEnd') ? set_value('dateEnd') : (isset($this->Tour_model->dateEnd) ? substr($this->Tour_model->dateEnd, 5, 2) . '/' . substr($this->Tour_model->dateEnd, 8, 2) . '/' . substr($this->Tour_model->dateEnd, 0, 4) : ''); ?>" class="form-control datepicker">
                              </div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="bootstrap-timepicker">
                                  <div class="form-group">
                                      <label class="control-label" for="startTime">Giorni della settimana</label>
                                      <input type="hidden" name="daysAvailability" id="selected_days" value="<?php echo $this->Tour_model->daysAvailability?>"/>
                                      <?php $days = str_replace(' ', '', $this->Tour_model->daysAvailability);  $days = explode(',', $days); ?>
                                      <select class="form-control" name="days" id="daysAvailability" multiple>
                                        <?php for ($i=0; $i < 7; $i++):?>
                                          <option value="<?php echo $i?>" <?php if(in_array( $i, $days) != false) echo 'selected="selected"';?>><?php echo lang($i) ?></option>
                                        <?php endfor ?>
                                      </select>
                                  </div>
                              </div>
                          </div>
                        </div>

                        <div class="col-xs-12">
                          <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="sessionHour1">Orari della prima sessione</label>
                                  <input type="hidden" name="sessionHour1" id='sessionHourH1'   value="<?php echo set_value('sessionHour1') ? set_value('sessionHour1') : (isset($this->Tour_model->sessionHour1) ? $this->Tour_model->sessionHour1 : '00:00 - 00:00'); ?>" class="form-control">
                                  <input type="text" id='sessionHour1' disabled  value="<?php echo set_value('sessionHour1') ? set_value('sessionHour1') : (isset($this->Tour_model->sessionHour1) ? $this->Tour_model->sessionHour1 : '00:00 - 0:00'); ?>" class="form-control">
                                  <div class="time-range">
                                      <div class="sliders_step1">
                                          <div class="slider-range" id="1"></div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="sessionHour2">Orari della seconda sessione</label>
                                  <input type="hidden" name="sessionHour2" id='sessionHourH2'   value="<?php echo set_value('sessionHour2') ? set_value('sessionHour2') : (isset($this->Tour_model->sessionHour3) ? $this->Tour_model->sessionHour2 : '0:00 - 0:00'); ?>" class="form-control">
                                  <input type="text"  id='sessionHour2' disabled  value="<?php echo set_value('sessionHour2') ? set_value('sessionHour2') : (isset($this->Tour_model->sessionHour3) ? $this->Tour_model->sessionHour2 : '00:00 - 00:00'); ?>" class="form-control">
                                  <div class="time-range">
                                      <div class="sliders_step1">
                                          <div class="slider-range" id="2"></div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="sessionHour3">Orari della terza sessione</label>
                                  <input type="hidden" name="sessionHour3" id='sessionHourH3' value="<?php echo set_value('sessionHour3') ? set_value('sessionHour3') : (isset($this->Tour_model->sessionHour3) ? $this->Tour_model->sessionHour3 : '0:00 - 0:00'); ?>" class="form-control">
                                  <input type="text" id='sessionHour3' disabled  value="<?php echo set_value('sessionHour3') ? set_value('sessionHour3') : (isset($this->Tour_model->sessionHour3) ? $this->Tour_model->sessionHour3 : '00:00 - 00:00'); ?>" class="form-control">
                                  <div class="time-range">
                                      <div class="sliders_step1">
                                          <div class="slider-range" id="3"></div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <small>*Metti 0:00 - 0:00 per annullare gli orari delle sessioni. In questo modo l'utente potrà scegliere l'orario in modo automatico</small>
                        </div>
                        <div class="col-xs-12" style="margin-top:20px">
                          <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="durationDay">Durata in giorni</label>
                                  <input type="number" name="durationDay" value="<?php echo set_value('durationDay') ? set_value('durationDay') : (isset($this->Tour_model->durationDay) ? $this->Tour_model->durationDay : ''); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="durationHours">Durata in ore</label>
                                  <input type="number" step="any" name="durationHours" value="<?php echo set_value('durationHours') ? set_value('durationHours') : (isset($this->Tour_model->durationHours) ? $this->Tour_model->durationHours : ''); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="durationMin">Durata in minuti</label>
                                  <input type="number" name="durationMin" value="<?php echo set_value('durationMin') ? set_value('durationMin') : (isset($this->Tour_model->durationMin) ? $this->Tour_model->durationMin : ''); ?>" class="form-control">
                              </div>
                          </div>
                        </div>
                    </fieldset>
                    <fieldset class="my_fileds">
                        <legend class="my_fileds">Luogo</legend>
                        <div class="col-lg-4 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="location">Località</label>
                                <select name="locationId" class="form-control">
                                    <?php foreach ($locations as $location): ?>
                                        <option value="<?php echo $location->id ?>" <?php echo $location->id == $this->Tour_model->locationId ? "selected='selected'" : ''; ?>><?php echo $location->title ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="lat">Latitudine</label>
                                <input type="number" step="any" name="lat" value="<?php echo set_value('lat') ? set_value('lat') : (isset($this->Tour_model->lat) ? $this->Tour_model->lat : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="long">Longitudine</label>
                                <input type="number" step="any" name="long" value="<?php echo set_value('long') ? set_value('long') : (isset($this->Tour_model->long) ? $this->Tour_model->long : ''); ?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Prezzi</legend>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="priceUnit">Tipo di prezzo</label>
                                <select class="form-control" name="priceUnit">
                                    <option value="P" <?php echo set_select('priceUnit', 'P') ? set_select('priceUnit', 'P') : (isset($this->Tour_model->priceUnit) && $this->Tour_model->priceUnit == 'P' ? 'selected="selected"' : '') ?>>Per persona</option>
                                    <option value="T" <?php echo set_select('priceUnit', 'T') ? set_select('priceUnit', 'T') : (isset($this->Tour_model->priceUnit) && $this->Tour_model->priceUnit == 'T' ? 'selected="selected"' : '') ?>>Prezzo fisso</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="price">Prezzo per adulti ($)</label>
                                <input type="number" step="any" name="price" value="<?php echo set_value('price') ? set_value('price') : (isset($this->Tour_model->price) ? $this->Tour_model->price : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="childPrice">Prezzo bambino ($)</label>
                                <input type="number" step="any" name="childPrice" value="<?php echo set_value('childPrice') ? set_value('childPrice') : (isset($this->Tour_model->childPrice) ? $this->Tour_model->childPrice : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <!-- <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="discountAdult">Sconto Adulti (%)</label>
                                <input type="number" name="discountAdult" value="<?php echo set_value('discountAdult') ? set_value('discountAdult') : (isset($this->Tour_model->discountAdult) ? $this->Tour_model->discountAdult : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="discountChildren">Sconto bambini (%)</label>
                                <input type="number" name="discountChildren" value="<?php echo set_value('discountChildren') ? set_value('discountChildren') : (isset($this->Tour_model->discountChildren) ? $this->Tour_model->discountChildren : ''); ?>" class="form-control">
                            </div>
                        </div> -->
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="maxParticipants">Max Partecipanti</label>
                                <input type="number" name="maxParticipants" value="<?php echo set_value('maxParticipants') ? set_value('maxParticipants') : (isset($this->Tour_model->maxParticipants) ? $this->Tour_model->maxParticipants : ''); ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="fixedPrice">Prezzo Fisso ($)</label>
                                <input type="number" name="fixedPrice" value="<?php echo set_value('fixedPrice') ? set_value('fixedPrice') : (isset($this->Tour_model->fixedPrice) ? $this->Tour_model->fixedPrice : ''); ?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Informazioni Generali</legend>
                        <div class="col-lg-4 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="location">Associa il tour alla tua attività</label>
                                <select name="order_business" class="form-control">
                                    <?php foreach ($business_pages as $item): ?>
                                        <option value="<?php echo $item->id.','.$item->order_id ?>" <?php $string=$item->id.','.$item->order_id; $compare = $this->Tour_model->businessPageId.','.$this->Tour_model->orderId;  echo $compare == $string ? "selected='selected'" : ''; ?>><?php echo $item->itName ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="type">Tipo di tour</label>
                                <select name="type" id="type" class="form-control">
                                    <option value="1" <?php echo isset($this->Tour_model->type) ? ($this->Tour_model->type == '1' ? 'selected=""' : '') : ''; ?>>Tour</option>
                                    <option value="2" <?php echo isset($this->Tour_model->type) ? ($this->Tour_model->type == '2' ? 'selected=""' : '') : ''; ?>>Escursione</option>
                                    <option value="3" <?php echo isset($this->Tour_model->type) ? ($this->Tour_model->type == '3' ? 'selected=""' : '') : ''; ?>>Visita Guidata</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="categories">Categorie</label><small> (Premi invio dopo che hai scritto una categoria)</small>
                                <input type="text" data-role="tagsinput" value="<?php echo set_value('categories') ? set_value('categories') : (isset($this->Tour_model->categories) ? $this->Tour_model->categories : ''); ?>" class="form-control" name ="categories"/>
                            </div>
                        </div>

                    </fieldset>

                </div>
                <div class="panel-footer" style="text-align: right;">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <?php if (isset($this->Tour_model->id)): ?>
        <div class="row">
            <div class="col-xs-6">
                <?php echo form_open_multipart('customer/tours/upload_image/' . $this->Tour_model->id); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Carica la thumbnail
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <input  type="hidden" name="imageType" value="thumbnail">
                            <div class="form-group">
                                <label class="control-label" for="p_image">Thumbnail</label><small> (L'immagine non deve avere spazi o elementi di punteggiatura nel nome)</small>
                                <input type="file" required="required" name="p_image" class="form-control">
                            </div>
                            <?php if (  $this->Tour_model->thumbnail != '' && file_exists('./uploads/tours/' . $this->Tour_model->thumbnail)): ?>
                              <img class="img-thumbnail" src="<?php echo base_url() . 'uploads/tours/' . $this->Tour_model->thumbnail ?>">
                            <?php else: ?>
                                Non ci sono foto
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right;">
                        <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Carica</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>

            <div class="col-xs-6">
                <?php echo form_open_multipart('customer/tours/upload_image/' . $this->Tour_model->id); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Carica l'immagine di copertina
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="imageType" value="cover">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="p_image">Copertina</label><small> (L'immagine non deve avere spazi o elementi di punteggiatura nel nome)</small>
                                <input type="file" required="required" name="p_image" class="form-control">
                            </div>
                            <?php if ( $this->Tour_model->cover != '' && file_exists('./uploads/tours/' . $this->Tour_model->cover)): ?>
                                <img class="img-thumbnail" src="<?php echo base_url() . 'uploads/tours/' . $this->Tour_model->cover ?>">
                            <?php else: ?>
                              Non ci sono foto
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right;">
                        <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>

        </div>
    <?php endif; ?>
</div>

<style>
.time-range p {
    font-family:"Arial", sans-serif;
    font-size:14px;
    color:#333;
}
.ui-slider-horizontal {
    height: 8px;
    background: #D7D7D7;
    border: 1px solid #BABABA;
    box-shadow: 0 1px 0 #FFF, 0 1px 0 #CFCFCF inset;
    clear: both;
    margin: 8px 0;
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    -ms-border-radius: 6px;
    -o-border-radius: 6px;
    border-radius: 6px;
}
.ui-slider {
    position: relative;
    text-align: left;
}
.ui-slider-horizontal .ui-slider-range {
    top: -1px;
    height: 100%;
}
.ui-slider .ui-slider-range {
    position: absolute;
    z-index: 1;
    height: 8px;
    font-size: .7em;
    display: block;
    border: 1px solid #5BA8E1;
    box-shadow: 0 1px 0 #AAD6F6 inset;
    -moz-border-radius: 6px;
    -webkit-border-radius: 6px;
    -khtml-border-radius: 6px;
    border-radius: 6px;
    background: #81B8F3;
    background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…pZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
    background-size: 100%;
    background-image: -webkit-gradient(linear, 50% 0, 50% 100%, color-stop(0%, #A0D4F5), color-stop(100%, #81B8F3));
    background-image: -webkit-linear-gradient(top, #A0D4F5, #81B8F3);
    background-image: -moz-linear-gradient(top, #A0D4F5, #81B8F3);
    background-image: -o-linear-gradient(top, #A0D4F5, #81B8F3);
    background-image: linear-gradient(top, #A0D4F5, #81B8F3);
}
.ui-slider .ui-slider-handle {
    border-radius: 50%;
    background: #F9FBFA;
    background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…pZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
    background-size: 100%;
    background-image: -webkit-gradient(linear, 50% 0, 50% 100%, color-stop(0%, #C7CED6), color-stop(100%, #F9FBFA));
    background-image: -webkit-linear-gradient(top, #C7CED6, #F9FBFA);
    background-image: -moz-linear-gradient(top, #C7CED6, #F9FBFA);
    background-image: -o-linear-gradient(top, #C7CED6, #F9FBFA);
    background-image: linear-gradient(top, #C7CED6, #F9FBFA);
    width: 22px;
    height: 22px;
    -webkit-box-shadow: 0 2px 3px -1px rgba(0, 0, 0, 0.6), 0 -1px 0 1px rgba(0, 0, 0, 0.15) inset, 0 1px 0 1px rgba(255, 255, 255, 0.9) inset;
    -moz-box-shadow: 0 2px 3px -1px rgba(0, 0, 0, 0.6), 0 -1px 0 1px rgba(0, 0, 0, 0.15) inset, 0 1px 0 1px rgba(255, 255, 255, 0.9) inset;
    box-shadow: 0 2px 3px -1px rgba(0, 0, 0, 0.6), 0 -1px 0 1px rgba(0, 0, 0, 0.15) inset, 0 1px 0 1px rgba(255, 255, 255, 0.9) inset;
    -webkit-transition: box-shadow .3s;
    -moz-transition: box-shadow .3s;
    -o-transition: box-shadow .3s;
    transition: box-shadow .3s;
}
.ui-slider .ui-slider-handle {
    position: absolute;
    z-index: 2;
    width: 22px;
    height: 22px;
    cursor: default;
    border: none;
    cursor: pointer;
}
.ui-slider .ui-slider-handle:after {
    content:"";
    position: absolute;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    top: 50%;
    margin-top: -4px;
    left: 50%;
    margin-left: -4px;
    background: #30A2D2;
    -webkit-box-shadow: 0 1px 1px 1px rgba(22, 73, 163, 0.7) inset, 0 1px 0 0 #FFF;
    -moz-box-shadow: 0 1px 1px 1px rgba(22, 73, 163, 0.7) inset, 0 1px 0 0 white;
    box-shadow: 0 1px 1px 1px rgba(22, 73, 163, 0.7) inset, 0 1px 0 0 #FFF;
}
.ui-slider-horizontal .ui-slider-handle {
    top: -.5em;
    margin-left: -.6em;
}
.ui-slider a:focus {
    outline:none;
}
</style>
