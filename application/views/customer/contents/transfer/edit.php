<?php
 $divID = ($usertype == 'admin') ? 'page-wrapper' : '';
?>
<style>
    fieldset{
        border: 1px groove #ddd !important;
        padding: 0 0.4em 0.4em 0.4em !important;
        margin: 0 0 0.6em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }
    legend{
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;
        font-size: 11px;
        margin-bottom: 0;
    }
    .bootstrap-tagsinput {
      display: block !important;
    }
    .main-zone-div .zone-div {
        width: 30%;
    }
    .main-zone-div {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        width: 100%;
        align-items: center;
    }
    .main-zone-div button{
        margin-top: 10px;
    }
</style>
<div id=<?php echo $divID ?>>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-exchange"></i> Transfer</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <?php if (validation_errors()): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo validation_errors(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12">
            <?php echo form_open(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->Transfer_model->itTitle; ?>
                </div>
                <div class="panel-body">
                    <input type="hidden" name="id" value="<?php echo isset($this->Transfer_model->id) ? $this->Transfer_model->id : '0'; ?>">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>

                    </ul>
                    <div class="tab-content">
                      <div id="menu2" class="tab-pane fade  in active">
                          <br>
                          <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="itTitle">Nome visualizzato [it]</label>
                                  <input type="text" name="itTitle" id="itTitle" placeholder="Enter Name in Italian" value="<?php echo set_value('itTitle') ? set_value('itTitle') : (isset($this->Transfer_model->itTitle) ? $this->Transfer_model->itTitle : ''); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-xs-12" style="margin-top:10px">
                              <label class="control-label" for="it_editor">Descrizione [it]</label>
                              <textarea id="it_editor" name="itDescription" rows="5" cols="80" placeholder="Enter page description for Italian">
                                  <?php echo set_value('itDescription') ? set_value('itDescription') : (isset($this->Transfer_model->itDescription) ? $this->Transfer_model->itDescription : ''); ?>
                              </textarea>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="itServicesIncluded">Servizi Inclusi [it]</label>
                                  <textarea class="form-control" id="itServicesIncluded" style="height:100px" name="itServicesIncluded"><?php echo set_value('itServicesIncluded') ? set_value('itServicesIncluded') : (isset($this->Transfer_model->itServicesIncluded) ? $this->Transfer_model->itServicesIncluded : ''); ?></textarea>
                              </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="itServicesNotIncluded">Servizi Non Inclusi [it]</label>
                                  <textarea class="form-control" id="itServicesNotIncluded" style="height:100px" name="itServicesNotIncluded"><?php echo set_value('itServicesNotIncluded') ? set_value('itServicesNotIncluded') : (isset($this->Transfer_model->itServicesNotIncluded) ? $this->Transfer_model->itServicesNotIncluded : ''); ?></textarea>
                              </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="itPolicy">Policy di cancellazione e prenotazione [it]</label>
                                  <textarea class="form-control" id="itPolicy" style="height:100px" name="itPolicy"><?php echo set_value('itPolicy') ? set_value('itPolicy') : (isset($this->Transfer_model->itPolicy) ? $this->Transfer_model->itPolicy : ''); ?></textarea>
                              </div>
                          </div>
                      </div>
                      <div id="home" class="tab-pane fade">
                          <br>
                          <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="enTitle">Display Name [en]</label>
                                  <input type="text" name="enTitle" id="entitle" placeholder="Enter Title in English" value="<?php echo (set_value('enTitle') ? set_value('enTitle') : (isset($this->Transfer_model->enTitle) ? $this->Transfer_model->enTitle : '')); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-xs-12">
                              <label class="control-label" for="en_editor">Description [en]</label>
                              <textarea id="en_editor" name="enDescription" rows="5" cols="80" placeholder="Enter page description for English">
                                  <?php echo set_value('enDescription') ? set_value('enDescription') : (isset($this->Transfer_model->enDescription) ? $this->Transfer_model->enDescription : ''); ?>
                              </textarea>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="enServicesIncluded">Servizi Inclusi [en]</label>
                                  <textarea class="form-control" id="enServicesIncluded" style="height:100px" name="enServicesIncluded"><?php echo set_value('enServicesIncluded') ? set_value('enServicesIncluded') : (isset($this->Transfer_model->enServicesIncluded) ? $this->Transfer_model->enServicesIncluded : ''); ?></textarea>
                              </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="enServicesNotIncluded">Servizi Non Inclusi [en]</label>
                                  <textarea class="form-control" id="enServicesNotIncluded" style="height:100px" name="enServicesNotIncluded"><?php echo set_value('enServicesNotIncluded') ? set_value('enServicesNotIncluded') : (isset($this->Transfer_model->enServicesNotIncluded) ? $this->Transfer_model->enServicesNotIncluded : ''); ?></textarea>
                              </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:10px">
                              <div class="form-group">
                                  <label class="control-label" for="enPolicy">Policy di cancellazione e prenotazione [en]</label>
                                  <textarea class="form-control" id="enPolicy" style="height:100px" name="enPolicy"><?php echo set_value('enPolicy') ? set_value('enPolicy') : (isset($this->Transfer_model->enPolicy) ? $this->Transfer_model->enPolicy : ''); ?></textarea>
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 15px;"></div>
                    <fieldset class="my_fileds">
                        <legend class="my_fileds">Periodo di validità</legend>
                        <div class="col-xs-12">
                          <div class="col-lg-3 col-md-3 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="startDate">Data di inizio</label>
                                  <input type="text" name="dateStart" placeholder="MM/DD/YYYY" value="<?php echo set_value('dateStart') ? set_value('dateStart') : (isset($this->Transfer_model->dateStart) ? substr($this->Transfer_model->dateStart, 5, 2) . '/' . substr($this->Transfer_model->dateStart, 8, 2) . '/' . substr($this->Transfer_model->dateStart, 0, 4) : ''); ?>" class="form-control datepicker">
                              </div>
                          </div>
                          <div class="col-lg-3 col-md-3 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="endDate">Data di fine</label>
                                  <input type="text" name="dateEnd" placeholder="MM/DD/YYYY" value="<?php echo set_value('dateEnd') ? set_value('dateEnd') : (isset($this->Transfer_model->dateEnd) ? substr($this->Transfer_model->dateEnd, 5, 2) . '/' . substr($this->Transfer_model->dateEnd, 8, 2) . '/' . substr($this->Transfer_model->dateEnd, 0, 4) : ''); ?>" class="form-control datepicker">
                              </div>
                          </div>
                          <div class="col-lg-3 col-md-3 col-xs-6">
                              <div class="bootstrap-timepicker">
                                  <div class="form-group">
                                      <label class="control-label" for="startTime">Giorni della settimana</label>
                                      <input type="hidden" name="daysAvailability" id="selected_days" value="<?php echo $this->Transfer_model->daysAvailability?>"/>
                                      <?php $days = str_replace(' ', '', $this->Transfer_model->daysAvailability);  $days = explode(',', $days); ?>
                                      <select class="form-control" name="days" id="daysAvailability" multiple>
                                        <?php for ($i=0; $i < 7; $i++):?>
                                          <option value="<?php echo $i?>" <?php if(in_array( $i, $days) != false) echo 'selected="selected"';?>><?php echo lang($i) ?></option>
                                        <?php endfor ?>
                                      </select>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-3 col-md-3 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="startPrice">Prezzo di partenza</label>
                                 <input type="startPrice" name="startPrice" value="<?php echo set_value('startPrice') ? set_value('startPrice') : (isset($this->Transfer_model->startPrice) ? $this->Transfer_model->startPrice : ''); ?>" class="form-control">
                              </div>
                          </div>
                        </div>
                        <div class="col-xs-12">
                          <div class="col-lg-3 col-md-3 col-xs-6">
                              <div class="form-group">
                                <label>Punto di raccolta</label>
                                <input type="hidden" name="pickup" id="selected_pickup" value="<?php echo $this->Transfer_model->pickup?>"/>
                                      <?php $pickups = str_replace(' ', '', $this->Transfer_model->pickup);  $pickups = explode(',', $pickups); ?>
                                <select class="col-md-4" id="pickup" class="form-control" name="pick_up" multiple>
                                <?php 
                                
                                  foreach ($pick_up_points as $key => $pick_up) :?>
                                    <option value="<?= $key ?>" <?php echo isset($this->Transfer_model->pickup) && in_array($key, $pickups) ? 'selected="selected"' : ''; ?> ><?= $pick_up ?></option>;
                                  <?php endforeach; ?>
                                ?>
                                </select>
                              </div>
                          </div>
                          <!-- <div class="col-lg-3 col-md-3 col-xs-6">
                            <div class="form-group">
                              <label>Luogo di scarico</label>
                              <input type="hidden" name="dropoff" id="selected_dropoff" value="<?php echo $this->Transfer_model->dropoff?>"/>
                                      <?php $dropoffs = explode(',', $this->Transfer_model->dropoff);
                                      // print_r($hotels); exit;
                                       ?>
                              <select id="dropoff" class="form-control" name="drop_off" multiple>
                                <?php foreach($hotels as $hotel) { ?>
                                <option value="<?php echo $hotel->enName; ?>"  <?php echo isset($this->Transfer_model->dropoff) && in_array($hotel->enName, $dropoffs) ? 'selected="selected"' : ''; ?> ><?php echo $hotel->enName; ?></option>
                                  <?php } ?>
                              </select>
                            </div>
                          </div> -->
                          <div class="col-lg-3 col-md-3 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="duration">Durata</label>
                                  <input type="hidden" name="duration" id='duration' value="<?php echo set_value('duration') ? set_value('duration') : (isset($this->Transfer_model->duration) ? $this->Transfer_model->duration : ''); ?>" class="form-control">
                                  <input type="text" name="duration" id='duration' value="<?php echo set_value('duration') ? set_value('duration') : (isset($this->Transfer_model->duration) ? $this->Transfer_model->duration : ''); ?>" class="form-control">
                              </div>
                          </div>
                           <!-- <div class="col-lg-3 col-md-3 col-xs-6">
                             <div class="form-group">
                              <label class="control-label" for="transfer_type">Tipo di trasferimento</label>
                                <select name="transfer_type" id="transfer_type" class="form-control">
                                <?php foreach ($transfers_type as $key => $transfers_type) :?>
                                  <option value="<?= $key ?>" <?php echo $this->Transfer_model->transfer_type == $key ? 'selected="selected"' : ''; ?> ><?= $transfers_type ?></option>;
                                <?php endforeach; ?>
                                </select> 
                             </div>
                           </div> -->
                        </div> 
                        <div class="col-xs-12" style="margin-top:20px">
                          <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                  <label class="control-label" for="maxPassenger">Max passeggeri</label>
                                  <input type="maxPassenger" name="maxPassenger" value="<?php echo set_value('maxPassenger') ? set_value('maxPassenger') : (isset($this->Transfer_model->maxPassenger) ? $this->Transfer_model->maxPassenger : ''); ?>" class="form-control">
                              </div>
                          </div>
                          <!-- <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                <label class="control-label" for="car_type">Tipo di auto</label>
                                <select name="car_type" id="car_type" class="form-control">
                                <?php foreach ($cars_type as $key => $cars_type) :?>
                                  <option value="<?= $key ?>" <?php echo $this->Transfer_model->car_type == $key ? 'selected="selected"' : ''; ?> ><?= $cars_type ?></option>;
                                <?php endforeach; ?>
                                </select>
                              </div>
                          </div> -->
                          <!-- <div class="col-lg-4 col-md-4 col-xs-6">
                              <div class="form-group">
                                <label class="control-label" for="boat_type">Tipo di barca</label>
                                <select name="boat_type" id="boat_type" class="form-control">
                                    <?php foreach ($boats_type as $key => $boats_type) :?>
                                  <option value="<?= $key ?>" <?php echo $this->Transfer_model->boat_type == $key ? 'selected="selected"' : ''; ?> ><?= $boats_type ?></option>;
                                <?php endforeach; ?>
                                </select> 
                              </div>  
                          </div> -->
                        </div>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Informazioni Generali</legend>
                       <div class="col-lg-4 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="location">Associa il tour alla tua attività</label>
                                <select name="order_business" class="form-control">
                                    <?php foreach ($business_pages as $item): ?>
                                        <option value="<?php echo $item->id ?>" <?=  $this->Transfer_model->businessPageId == $item->id ? "selected='selected'" : ''; ?>><?php echo $item->title ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="trip_type">Tipo di viaggio</label>
                                <select name="trip_type" id="trip_type" class="form-control">
                                    <option value="Sola andata" <?php echo isset($this->Transfer_model->trip_type) ? ($this->Transfer_model->trip_type == 'Senso unico' ? 'selected=""' : '') : ''; ?>>Sola andata</option>
                                    <option value="Andata e ritorno" <?php echo $this->Transfer_model->trip_type ? ($this->Transfer_model->trip_type == 'Andata e ritorno' ? 'selected=""' : '') : ''; ?>>Andata e ritorno</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="categories">Categorie</label><small> (Premi invio dopo che hai scritto una categoria)</small>
                                <input type="text" data-role="tagsinput" value="<?php echo set_value('categories') ? set_value('categories') : (isset($this->Transfer_model->categories) ? $this->Transfer_model->categories : ''); ?>" class="form-control" name ="categories"/>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>Zones</legend>
                        <input type="hidden" name="zone_count" id="zone_count" value="3">
                          <div class="col-lg-12 col-md-12 col-xs-12 main-zone-div">
                            <?php 
                              if (!empty($zone)) {
                                echo '<input type="hidden" name="zone_count" id="zone_count" value="'.count($zone).'">';
                                foreach ($zone as $key => $value) 
                                { ?>
                                <div class="form-group zone-div highlight-div-<?= $key + 1 ?> mb-2">
                                  <label class="control-label" for="categories">Zone Name</label>
                                  <input type="text" name="zone_name[]" placeholder="Enter Zone Name" class="form-control" value="<?php echo $value->zone_name; ?>" required="required">
                                </div>
                                <div class="form-group zone-div highlight-div-<?= $key + 1 ?> mb-2">
                                  <label class="control-label" for="categories">(SJD) San José del Cabo Price</label>
                                  <input type="number" name="sjd_price[]" placeholder="Enter SJD Price" class="form-control" value="<?php echo $value->sjd_price; ?>" required="required">
                                </div>
                                <div class="form-group zone-div highlight-div-<?= $key + 1 ?> mb-2">
                                  <label class="control-label" for="categories">(CSL) Cabo San Lucas Price</label>
                                  <input type="number" name="csl_price[]" placeholder="Enter CSL Price" class="form-control" value="<?php echo $value->csl_price; ?>" required="required">
                                </div>
                                <?php if($key == 0){ ?>
                                  <button type="button" class="btn btn-info highlight-div-<?= $key + 1 ?>" id="zone-btn-add"><i class="fa fa-plus"></i></button>
                                <?php }else{ ?>
                                  <button type="button" class="btn btn-danger highlight-div-<?= $key + 1 ?>" onclick="removeValues(this)" data-removevalue="<?= $key + 1 ?>" id="btn-remove"><i class="fa fa-minus"></i></button>
                                <?php } ?>
                            <?php } } else { ?>
                              <input type="hidden" name="zone_count" id="zone_count" value="3">
                              <div class="form-group zone-div highlight-div-1 mb-2">
                                <label class="control-label" for="categories">Zone Name</label>
                                <input type="text" name="zone_name[]" placeholder="Enter Zone Name" class="form-control" required="required">
                              </div>
                              <div class="form-group zone-div highlight-div-2 mb-2">
                                <label class="control-label" for="categories">(SJD) San José del Cabo Price</label>
                                <input type="number" name="sjd_price[]" placeholder="Enter SJD Price" class="form-control" required="required">
                              </div>
                              <div class="form-group zone-div highlight-div-3 mb-2">
                                <label class="control-label" for="categories">(CSL) Cabo San Lucas Price</label>
                                <input type="number" name="csl_price[]" placeholder="Enter CSL Price" class="form-control" required="required">
                              </div>
                              <button type="button" class="btn btn-info highlight-div-1" id="zone-btn-add"><i class="fa fa-plus"></i></button>
                            <?php } ?>
                          </div>
                    </fieldset>
                </div>
                <div class="panel-footer" style="text-align: right;">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <?php if (isset($this->Transfer_model->id)): ?>
        <div class="row">
            <div class="col-xs-6">
                <?php echo form_open_multipart('customer/transfer/upload_image/' . $this->Transfer_model->id); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Carica la thumbnail
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <input  type="hidden" name="imageType" value="thumbnail">
                            <div class="form-group">
                                <label class="control-label" for="p_image">Thumbnail</label><small> (L'immagine non deve avere spazi o elementi di punteggiatura nel nome)</small>
                                <input type="file" required="required" name="p_image" class="form-control">
                            </div>
                            <?php if (  $this->Transfer_model->thumbnail != '' && file_exists('./uploads/transfer/' . $this->Transfer_model->thumbnail)): ?>
                              <img class="img-thumbnail" src="<?php echo base_url() . 'uploads/transfer/' . $this->Transfer_model->thumbnail ?>">
                            <?php else: ?>
                                Non ci sono foto
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right;">
                        <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Carica</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>

            <div class="col-xs-6">
                <?php echo form_open_multipart('customer/transfer/upload_image/' . $this->Transfer_model->id); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Carica l'immagine di copertina
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="imageType" value="cover">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="p_image">Copertina</label><small> (L'immagine non deve avere spazi o elementi di punteggiatura nel nome)</small>
                                <input type="file" required="required" name="p_image" class="form-control">
                            </div>
                            <?php if ( $this->Transfer_model->cover != '' && file_exists('./uploads/transfer/' . $this->Transfer_model->cover)): ?>
                                <img class="img-thumbnail" src="<?php echo base_url() . 'uploads/transfer/' . $this->Transfer_model->cover ?>">
                            <?php else: ?>
                              Non ci sono foto
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right;">
                        <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>

        </div>
    <?php endif; ?>
</div>

<style>
.time-range p {
    font-family:"Arial", sans-serif;
    font-size:14px;
    color:#333;
}
.ui-slider-horizontal {
    height: 8px;
    background: #D7D7D7;
    border: 1px solid #BABABA;
    box-shadow: 0 1px 0 #FFF, 0 1px 0 #CFCFCF inset;
    clear: both;
    margin: 8px 0;
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    -ms-border-radius: 6px;
    -o-border-radius: 6px;
    border-radius: 6px;
}
.ui-slider {
    position: relative;
    text-align: left;
}
.ui-slider-horizontal .ui-slider-range {
    top: -1px;
    height: 100%;
}
.ui-slider .ui-slider-range {
    position: absolute;
    z-index: 1;
    height: 8px;
    font-size: .7em;
    display: block;
    border: 1px solid #5BA8E1;
    box-shadow: 0 1px 0 #AAD6F6 inset;
    -moz-border-radius: 6px;
    -webkit-border-radius: 6px;
    -khtml-border-radius: 6px;
    border-radius: 6px;
    background: #81B8F3;
    background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…pZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
    background-size: 100%;
    background-image: -webkit-gradient(linear, 50% 0, 50% 100%, color-stop(0%, #A0D4F5), color-stop(100%, #81B8F3));
    background-image: -webkit-linear-gradient(top, #A0D4F5, #81B8F3);
    background-image: -moz-linear-gradient(top, #A0D4F5, #81B8F3);
    background-image: -o-linear-gradient(top, #A0D4F5, #81B8F3);
    background-image: linear-gradient(top, #A0D4F5, #81B8F3);
}
.ui-slider .ui-slider-handle {
    border-radius: 50%;
    background: #F9FBFA;
    background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…pZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
    background-size: 100%;
    background-image: -webkit-gradient(linear, 50% 0, 50% 100%, color-stop(0%, #C7CED6), color-stop(100%, #F9FBFA));
    background-image: -webkit-linear-gradient(top, #C7CED6, #F9FBFA);
    background-image: -moz-linear-gradient(top, #C7CED6, #F9FBFA);
    background-image: -o-linear-gradient(top, #C7CED6, #F9FBFA);
    background-image: linear-gradient(top, #C7CED6, #F9FBFA);
    width: 22px;
    height: 22px;
    -webkit-box-shadow: 0 2px 3px -1px rgba(0, 0, 0, 0.6), 0 -1px 0 1px rgba(0, 0, 0, 0.15) inset, 0 1px 0 1px rgba(255, 255, 255, 0.9) inset;
    -moz-box-shadow: 0 2px 3px -1px rgba(0, 0, 0, 0.6), 0 -1px 0 1px rgba(0, 0, 0, 0.15) inset, 0 1px 0 1px rgba(255, 255, 255, 0.9) inset;
    box-shadow: 0 2px 3px -1px rgba(0, 0, 0, 0.6), 0 -1px 0 1px rgba(0, 0, 0, 0.15) inset, 0 1px 0 1px rgba(255, 255, 255, 0.9) inset;
    -webkit-transition: box-shadow .3s;
    -moz-transition: box-shadow .3s;
    -o-transition: box-shadow .3s;
    transition: box-shadow .3s;
}
.ui-slider .ui-slider-handle {
    position: absolute;
    z-index: 2;
    width: 22px;
    height: 22px;
    cursor: default;
    border: none;
    cursor: pointer;
}
.ui-slider .ui-slider-handle:after {
    content:"";
    position: absolute;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    top: 50%;
    margin-top: -4px;
    left: 50%;
    margin-left: -4px;
    background: #30A2D2;
    -webkit-box-shadow: 0 1px 1px 1px rgba(22, 73, 163, 0.7) inset, 0 1px 0 0 #FFF;
    -moz-box-shadow: 0 1px 1px 1px rgba(22, 73, 163, 0.7) inset, 0 1px 0 0 white;
    box-shadow: 0 1px 1px 1px rgba(22, 73, 163, 0.7) inset, 0 1px 0 0 #FFF;
}
.ui-slider-horizontal .ui-slider-handle {
    top: -.5em;
    margin-left: -.6em;
}
.ui-slider a:focus {
    outline:none;
}
</style>
