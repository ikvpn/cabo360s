    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-wrench"></i> Impostazioni</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if($this->session->flashdata('msg') && $this->session->flashdata('type')): ?>
      <?php
        $msg = $this->session->flashdata('msg'); $type = $this->session->flashdata('type');
      if ($msg != '' && $type != ''): ?>
          <div class="alert alert-<?php echo $type ?> alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <?php echo $msg; ?>
          </div>
      <?php endif; ?>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12">
            <?php echo form_open(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dettagli del profilo - <strong><?php echo $this->Customer_model->email; ?></strong>
                </div>
                <div class="panel-body">
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("firstName")) > 0) ? "has-error" : ""; ?>">
                            <input class="form-control" placeholder="Inserisci il tuo nome" name="firstName" type="text" autofocus value="<?php echo set_value('firstName')?set_value('firstName')!='':$this->Customer_model->firstName; ?>">
                            <?php echo (strlen(form_error('firstName')) > 0) ? "<span class='text-danger'><small>" . form_error('firstName') . "</small></span>" : " "; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("lastName")) > 0) ? "has-error" : ""; ?>">
                            <input class="form-control" placeholder="Inserisci il tuo cognome" name="lastName" type="text" value="<?php  echo set_value('firstName')!=''?set_value('lastName'):$this->Customer_model->lastName;  ?>">
                            <?php echo (strlen(form_error('lastName')) > 0) ? "<span class='text-danger'><small>" . form_error('lastName') . "</small></span>" : " "; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("billingAddress1")) > 0) ? "has-error" : ""; ?>">
                            <input class="form-control" placeholder="Indirizzo riga 1" name="billingAddress1" type="text" value="<?php echo set_value('billingAddress1')!=''?set_value('billingAddress1'):$this->Customer_model->billingAddress1; ?>">
                            <?php echo (strlen(form_error('billingAddress1')) > 0) ? "<span class='text-danger'><small>" . form_error('billingAddress1') . "</small></span>" : " "; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("billingAddress2")) > 0) ? "has-error" : ""; ?>">
                            <input class="form-control" placeholder="Indirizzo riga 2" name="billingAddress2" type="text" value="<?php echo set_value('billingAddress2')!=''?set_value('billingAddress2'):$this->Customer_model->billingAddress2; ?>">
                            <?php echo (strlen(form_error('billingAddress2')) > 0) ? "<span class='text-danger'><small>" . form_error('billingAddress2') . "</small></span>" : " "; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("billingCity")) > 0) ? "has-error" : ""; ?>">
                            <input class="form-control" placeholder="Città" name="billingCity" type="text" value="<?php echo set_value('billingCity')?set_value('billingCity'):$this->Customer_model->billingCity; ?>">
                            <?php echo (strlen(form_error('billingCity')) > 0) ? "<span class='text-danger'><small>" . form_error('billingCity') . "</small></span>" : " "; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("billingPostcode")) > 0) ? "has-error" : ""; ?>">
                            <input class="form-control" placeholder="CAP" name="billingPostcode" type="text" value="<?php echo set_value('billingPostcode')?set_value('billingPostcode'):$this->Customer_model->billingPostcode; ?>">
                            <?php echo (strlen(form_error('billingPostcode')) > 0) ? "<span class='text-danger'><small>" . form_error('billingPostcode') . "</small></span>" : " "; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("billingState")) > 0) ? "has-error" : ""; ?>">
                            <input class="form-control" placeholder="Stato" name="billingState" type="text" value="<?php echo set_value('billingState')?set_value('billingState'):$this->Customer_model->billingState; ?>">
                            <?php echo (strlen(form_error('billingState')) > 0) ? "<span class='text-danger'><small>" . form_error('billingState') . "</small></span>" : " "; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("billingCountry")) > 0) ? "has-error" : ""; ?>">
                            <input class="form-control" placeholder="Paese di fatturazione" name="billingCountry" type="text" value="<?php echo set_value('billingCountry')?set_value('billingCountry'):$this->Customer_model->billingCountry; ?>">
                            <?php echo (strlen(form_error('billingCountry')) > 0) ? "<span class='text-danger'><small>" . form_error('billingCountry') . "</small></span>" : " "; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("billingPhone")) > 0) ? "has-error" : ""; ?>">
                            <input class="form-control" placeholder="Numero di telefono" name="billingPhone" type="text" value="<?php echo set_value('billingPhone')?set_value('billingPhone'):$this->Customer_model->billingPhone; ?>">
                            <?php echo (strlen(form_error('billingPhone')) > 0) ? "<span class='text-danger'><small>" . form_error('billingPhone') . "</small></span>" : " "; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("company")) > 0) ? "has-error" : ""; ?>">
                            <input class="form-control" placeholder="Società" name="company" type="text" value="<?php echo set_value('company')?set_value('company'):$this->Customer_model->company; ?>">
                            <?php echo (strlen(form_error('company')) > 0) ? "<span class='text-danger'><small>" . form_error('company') . "</small></span>" : " "; ?>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right;">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cambio password
                </div>
                <!-- /.panel-heading -->
                <?php echo form_open('customer/settings/resetpassword'); ?>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label" for="password">Password attuale</label>
                        <input type="text" class="form-control" id="password" name="password">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="n_password">Nuova Password</label>
                        <input type="text" class="form-control" id="password" name="n_password">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password">Conferma Password</label>
                        <input type="text" class="form-control" id="c_password" name="c_password">
                    </div>
                </div>
                <div class="panel-footer">
                    <div style="text-align: right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-retweet"></i> Cambia</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
