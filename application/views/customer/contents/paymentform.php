    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> <i class="fa <?php echo $this->Categories->icon; ?>"></i> <?php echo lang('register').' '.lang($this->Categories->table); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-xs-12">
            <?php echo form_open(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Please fill the details
                </div>
                <div class="panel-body">
                    <h4>Business Details</h4>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("title")) > 0) ? "has-error" : ""; ?>">
                            <label class="control-label" for="txtTitle"><?php echo lang('title') ?></label>
                            <input type="text" name="title" id="txtTitle" class="form-control" value="<?php echo set_value('title')?set_value('title'):$this->Customer_model->company; ?>">
                            <span class="text-danger"><small><?php echo (strlen(form_error('title')) > 0) ? form_error('title') : "<p>&nbsp;</p>"; ?></small></span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("phone")) > 0) ? "has-error" : ""; ?>">
                            <label class="control-label" for="phone"><?php echo lang('phone') ?></label>
                            <input type="text" name="phone" class="form-control" value="<?php echo set_value('phone')?set_value('phone'):$this->Customer_model->billingPhone; ?>">
                            <span class="text-danger"><small><?php echo (strlen(form_error('phone')) > 0) ? form_error('phone') : "<p>&nbsp;</p>"; ?></small></span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("location")) > 0) ? "has-error" : ""; ?>">
                            <label class="control-label" for="location"><?php echo lang('location'); ?></label>
                            <select name="location" class="form-control">
                                <option value="" disabled="disabled" <?php echo set_value('location') ? '' : 'selected="seleceted"'; ?>> --SELECT LOCATION-- </option>
                                <?php if (isset($locations) && is_array($locations)): ?>
                                    <?php foreach ($locations as $location): ?>
                                        <option value="<?php echo $location->id ?>" <?php echo set_checkbox('location', $location->id); ?>><?php echo $location->title ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                            <span class="text-danger"><small><?php echo (strlen(form_error('location')) > 0) ? form_error('location') : "<p>&nbsp;</p>"; ?></small></span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("address")) > 0) ? "has-error" : ""; ?>">
                            <label class="control-label" for="address"><?php echo lang('address'); ?></label>
                            <textarea name="address" class="form-control"><?php echo set_value("address")?set_value("address"):$this->Customer_model->billingAddress1.' '.$this->Customer_model->billingAddress2 ?></textarea>
                            <span class="text-danger"><small><?php echo (strlen(form_error('address')) > 0) ? form_error('address') : "<p>&nbsp;</p>"; ?></small></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <h4><?php echo lang('payment_details') ?></h4>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("payment_plan")) > 0) ? "has-error" : ""; ?>">
                            <label><?php echo lang('payment_plan') ?></label>
                            <?php $plans = explode(',', $this->Categories->payment_plans); ?>
                            <?php if (in_array('1', $plans)): ?>
                                <label class="radio-inline">
                                    <input type="radio" name="payment_plan" id="optionsRadiosInline1" value="M" <?php echo set_radio('payment_plan', 'M'); ?> <?php echo set_value('payment_plan')?'':'checked="checked"' ?>> <?php echo $this->Categories->m_charges ?>/Month
                                </label>
                            <?php endif; ?>
                            <?php if (in_array('2', $plans)): ?>
                                <label class="radio-inline">
                                    <input type="radio" name="payment_plan" id="optionsRadiosInline2" value="Y" <?php echo set_radio('payment_plan', 'Y'); ?>> <?php echo $this->Categories->y_charges ?>/Yearly
                                </label>
                            <?php endif; ?>
                            <span class="text-danger"><small><?php echo (strlen(form_error('payment_plan')) > 0) ? form_error('payment_plan') : "<p>&nbsp;</p>"; ?></small></span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group <?php echo (strlen(form_error("payment_type")) > 0) ? "has-error" : ""; ?>">
                            <label><?php echo lang('payment_option') ?></label>
                            <label class="radio-inline">
                                <input type="radio" name="payment_type" value="paypal" <?php echo set_radio('payment_type', 'paypal'); ?> <?php echo set_value('payment_type')?'':'checked="checked"'; ?>> <?php echo lang('paypal') ?>
                            </label>
                            <?php if($settings->offline_payment): ?>
                            <label class="radio-inline">
                                <input type="radio" name="payment_type" value="offline" <?php echo set_radio('payment_type', 'offline'); ?>> <?php echo lang('offline') ?>
                            </label>
                            <?php endif; ?>
                            <span class="text-danger"><small><?php echo (strlen(form_error('checkout_type')) > 0) ? form_error('checkout_type') : "<p>&nbsp;</p>"; ?></small></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <h4><?php echo lang('billing_details'); ?></h4>
                    <div class="col-lg-6 col-xs-12">
                        <table>
                            <tr>
                                <td style="width:150px"><b><?php echo lang('name'); ?></b></td>
                                <td><?php echo $this->Customer_model->firstName.' '.$this->Customer_model->lastName; ?></td>
                            </tr>
                            <tr>
                                <td><b><?php echo lang('address'); ?></b></td>
                                <td><?php echo $this->Customer_model->billingAddress1.' '.$this->Customer_model->billingAddress2 ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <table>
                            <tr>
                                <td style="width:150px"><b><?php echo lang('email'); ?></b></td>
                                <td><?php echo $this->Customer_model->email; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right;">
                    <button type="submit" class="btn btn-success btn-xs"><i class="fa fa-shopping-cart"></i> <?php echo lang('checkout') ?></button>
                    <button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i> <?php echo lang('cancel') ?></button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /.row -->
