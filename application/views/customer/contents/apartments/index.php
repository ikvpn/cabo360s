  <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> <i class="fa fa-home"></i> Case vacanza</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Case vacanza registrate
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="hotels-dtable">
                            <thead>
                                <tr>
                                    <th style="width:50px">S.No</th>
                                    <th>Nome</th>
                                    <th>Località</th>
                                    <th>Stato</th>
                                    <th style="width:80px;">Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($orders)): ?>
                                    <?php if (is_array($orders)): ?>
                                        <?php $counter = 1; ?>
                                        <?php foreach ($orders as $order): ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $counter ?></td>
                                                <td><?php echo $order->title ?></td>
                                                <td><?php echo $this->Orders_model->getLocationName($order->location); ?></td>
                                                <td><?php echo $order->status ?></td>
                                                <td>
                                                     <?php if($order->status=='new'||$order->status=='expired'): ?>
                                                       <a href="<?php echo base_url() ?>gateways/paypal_express/pay_invoice/<?php echo $order->id ?>"  class="btn btn-primary btn-xs"><i class="fa fa-money"></i></a>
                                                    <?php else: ?>
                                                       <a href="<?php echo base_url().$this->lang->lang(); ?>/customer/apartments/edit/<?php echo $order->id ?>"  class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <?php $counter++; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
