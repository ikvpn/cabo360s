<?php

$divID = ($usertype == 'admin') ? 'page-wrapper' : '';

?>
<style type="text/css">
    .housining-div .form-control,
    .input-file {
        width: calc(100% - 50px);
    }
    .housining-div {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        width: 100%;
        align-items: self-start;
    }
    .input-file .form-control{
        width: 100%;
    }
    .input-file img{
        width: 60px;
        height: 60px;
        padding: 2px;
        object-fit: cover;
        border: 1px solid #ccc;
        margin: 10px 0 0 0;
        border-radius: 2px;
    }
</style>

<div id=<?php echo $divID ?>>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-home"></i> Ville</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <?php if (validation_errors()): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo validation_errors(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12">
            <?php echo form_open_multipart(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->Orders_model->title; ?>
                </div>
                <div class="panel-body">
                    <input type="hidden" name="id" value="<?php echo isset($this->Apartment_model->id) ? $this->Apartment_model->id : '0'; ?>">
                    <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>

                    </ul>
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="enName">Nome visualizzato [en]</label>
                                    <input type="text" name="enName" id="enName" placeholder="Enter Name in English" value="<?php echo (set_value('enName') ? set_value('enName') : (isset($this->Apartment_model->enName) ? $this->Apartment_model->enName : '')); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <label class="control-label" for="en_editor">Descrizione [en]</label>
                                <textarea id="en_editor" name="enDescription" rows="5" cols="80" placeholder="Enter page description for English">
                                    <?php echo set_value('enDescription') ? set_value('enDescription') : (isset($this->Apartment_model->enDescription) ? $this->Apartment_model->enDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="duName">Nome visualizzato [de]</label>
                                    <input type="text" name="duName" id="duName" placeholder="Enter Name in Duetch" value="<?php echo set_value('duName') ? set_value('duName') : (isset($this->Apartment_model->duName) ? $this->Apartment_model->duName : ''); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12" style="margin-top:10px">
                                <label class="control-label" for="du_editor">Descrizione [de]</label>
                                <textarea id="du_editor" name="duDescription" rows="5" cols="80" placeholder="Enter page description for Deutch">
                                    <?php echo set_value('duDescription') ? set_value('duDescription') : (isset($this->Apartment_model->duDescription) ? $this->Apartment_model->duDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade in active">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="itName">Nome visualizzato [it]</label>
                                    <input type="text" name="itName" id="itName" placeholder="Enter Name in Italian" value="<?php echo set_value('itName') ? set_value('itName') : (isset($this->Apartment_model->itName) ? $this->Apartment_model->itName : ''); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12" style="margin-top:10px">
                                <label class="control-label" for="it_editor">Descrizione [it]</label>
                                <textarea id="it_editor" name="itDescription" rows="5" cols="80" placeholder="Enter page description for Italian">
                                    <?php echo set_value('itDescription') ? set_value('itDescription') : (isset($this->Apartment_model->itDescription) ? $this->Apartment_model->itDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 15px;"></div>

                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Latitudine</label>
                            <input type="text" name="lat" value="<?php echo set_value('lat') ? set_value('lat') : (isset($this->Apartment_model->lat) ? $this->Apartment_model->lat : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lng">Longitudine</label>
                            <input type="text" name="lng" value="<?php echo set_value('lng') ? set_value('lng') : (isset($this->Apartment_model->lng) ? $this->Apartment_model->lng : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group" style="padding-top:30px;">
                            <a  class="btn btn-primary btn-xs" href="http://www.gps-coordinates.net/" target="_blank">Trova Lat/Lng</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="minPrice">Prezzo minimo</label>
                            <input type="text" name="minPrice" value="<?php echo set_value('minPrice') ? set_value('minPrice') : (isset($this->Apartment_model->minPrice) ? $this->Apartment_model->minPrice : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="maxPrice">Prezzo massimo</label>
                            <input type="text" name="maxPrice" value="<?php echo set_value('maxPrice') ? set_value('maxPrice') : (isset($this->Apartment_model->maxPrice) ? $this->Apartment_model->maxPrice : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="unit">Prezzo</label>
                            <select class="form-control" name="unit">
                                <option value="N" <?php echo set_select('unit', 'N') ? set_select('unit', 'N') : (isset($this->Apartment_model->unit) && $this->Apartment_model->unit == 'N' ? 'selected="selected"' : '') ?>><?php echo lang('per_night'); ?></option>
                                <option value="D" <?php echo set_select('unit', 'D') ? set_select('unit', 'D') : (isset($this->Apartment_model->unit) && $this->Apartment_model->unit == 'D' ? 'selected="selected"' : '') ?>><?php echo lang('per_day'); ?></option>
                                <option value="W" <?php echo set_select('unit', 'W') ? set_select('unit', 'W') : (isset($this->Apartment_model->unit) && $this->Apartment_model->unit == 'W' ? 'selected="selected"' : '') ?>><?php echo lang('per_week'); ?></option>
                                <option value="M" <?php echo set_select('unit', 'M') ? set_select('unit', 'M') : (isset($this->Apartment_model->unit) && $this->Apartment_model->unit == 'M' ? 'selected="selected"' : '') ?>><?php echo lang('per_month'); ?></option>
                                <option value="Y" <?php echo set_select('unit', 'Y') ? set_select('unit', 'Y') : (isset($this->Apartment_model->unit) && $this->Apartment_model->unit == 'Y' ? 'selected="selected"' : '') ?>><?php echo lang('per_year'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="rooms">Camere</label>
                            <input type="text" name="rooms" value="<?php echo set_value('rooms') ? set_value('rooms') : (isset($this->Apartment_model->rooms) ? $this->Apartment_model->rooms : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="beds">Letti</label>
                            <input type="text" name="beds" value="<?php echo set_value('beds') ? set_value('beds') : (isset($this->Apartment_model->beds) ? $this->Apartment_model->beds : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="pax">Persone</label>
                            <input type="text" name="pax" value="<?php echo set_value('pax') ? set_value('pax') : (isset($this->Apartment_model->pax) ? $this->Apartment_model->pax : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="trypology">Piedi quadrati</label>
                            <input type="text" name="trypology" value="<?php echo set_value('trypology') ? set_value('trypology') : (isset($this->Apartment_model->trypology) ? $this->Apartment_model->trypology : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="bathrooms">Bagni</label>
                            <input type="text" name="bathrooms" value="<?php echo set_value('bathrooms') ? set_value('bathrooms') : (isset($this->Apartment_model->bathrooms) ? $this->Apartment_model->bathrooms : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="manager">Nome del manager</label>
                            <input type="text" name="manager" value="<?php echo set_value('manager') ? set_value('manager') : (isset($this->Apartment_model->manager) ? $this->Apartment_model->manager : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="m_image">Foto del manager</label>
                            <input type="file"  name="m_image" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="phone">Numero di telefono</label>
                            <input type="text" name="phone" value="<?php echo set_value('phone') ? set_value('phone') : (isset($this->Apartment_model->phone) ? $this->Apartment_model->phone : ''); ?>" class="form-control">
                        </div>
                    </div>

                  <!-- Housining Highlights Section Start -->
                  <div class="col-lg-12">
                      <div class="panel panel-default">
                          <div class="panel-heading">Villas Highlights</div>
                          <div class="panel-body">
                              <div class="row">
                                  <div class="col-lg-12 col-md-12 col-xs-12 main-highlight-div">
                                    <?php 
                                        if(isset($this->Apartment_model->home_heighlights) && $this->Apartment_model->home_heighlights != ""){ 
                                        $unSerializeHeighlights = unserialize($this->Apartment_model->home_heighlights);
                                        echo '<input type="hidden" name="home_heighlight_count" id="home_heighlight_count" value="'.count($unSerializeHeighlights).'">';
                                        foreach ($unSerializeHeighlights as $key => $unSerializeHeighlight) {
                                    ?>
                                        <div class="form-group housining-div highlight-div-<?= $key + 1 ?> mb-2">
                                          <input type="text" name="amenities_name[]" value="<?php echo $unSerializeHeighlight['name']; ?>" placeholder="Enter Amenities Name" class="form-control">
                                          <?php if($key == 0){ ?>
                                            <button type="button" class="btn btn-info" id="btn-add"><i class="fa fa-plus"></i></button>
                                          <?php }else{ ?>
                                            <button type="button" class="btn btn-danger" onclick="removeValues(this)" data-removevalue="<?= $key + 1 ?>" id="btn-remove"><i class="fa fa-minus"></i></button>
                                          <?php } ?>
                                        </div>
                                    <?php }}else{ ?>
                                        <input type="hidden" name="home_heighlight_count" id="home_heighlight_count" value="1">
                                        <div class="form-group housining-div highlight-div-1 mb-2">
                                          <input type="text" name="amenities_name[]" placeholder="Enter Amenities Name" class="form-control">
                                          <button type="button" class="btn btn-info" id="btn-add"><i class="fa fa-plus"></i></button>
                                        </div>
                                    <?php } ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- Housining Highlights Section End -->
                </div>
                <div class="panel-footer" style="text-align: right;">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <?php if (isset($this->Apartment_model->id)): ?>
        <div class="row">
            <div class="col-xs-12">
                <?php echo form_open_multipart('customer/apartments/upload_image/' . $this->Orders_model->id); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Carica l'immagine primaria
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-4 col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="p_image">Immagine Primaria</label>
                                <input type="file" required="required" name="p_image" class="form-control">
                            </div>
                        </div>
                      <div class="col-lg-4 col-md-6 col-xs-12">
                            <?php if (file_exists('./uploads/apartments/' . $this->Apartment_model->photo) && $this->Apartment_model->photo != ''): ?>
                                <img class="img-thumbnail" src="<?php echo base_url() . 'uploads/apartments/' . $this->Apartment_model->photo ?>">
                            <?php else: ?>
                                Non ci sono foto
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-4 col-md-6 col-xs-12 well">
                            <strong>Istruzioni per l'upload</strong>
                            <ol>
                                <li>Dimensione massima immagine 2MB</li>
                                <li>Risoluzione consigliata 1400x600</li>
                            </ol>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right;">
                        <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Carica</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Immagini per lo slider
                    </div>
                    <div class="panel-body">
                        <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                            <!-- Redirect browsers with JavaScript disabled to the origin page -->
                            <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                            <div class="row fileupload-buttonbar">
                                <div class="col-lg-7">
                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                    <span class="btn btn-success fileinput-button">
                                      <i class="glyphicon glyphicon-plus"></i>
                                      <span>Aggiungi file...</span>
                                      <input type="file" name="files[]" multiple>
                                  </span>
                                    <button type="submit" class="btn btn-primary start">
                                        <i class="glyphicon glyphicon-upload"></i>
                                        <span>Inizio upload</span>
                                    </button>
                                    <button type="reset" class="btn btn-warning cancel">
                                        <i class="glyphicon glyphicon-ban-circle"></i>
                                        <span>Annulla upload</span>
                                    </button>
                                    <button type="button" class="btn btn-danger delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Elimina</span>
                                    </button>
                                    <input type="checkbox" class="toggle">
                                    <!-- The global file processing state -->
                                    <span class="fileupload-process"></span>
                                </div>
                                <!-- The global progress state -->
                                <div class="col-lg-5 fileupload-progress fade">
                                    <!-- The global progress bar -->
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                    </div>
                                    <!-- The extended global progress state -->
                                    <div class="progress-extended">&nbsp;</div>
                                </div>
                            </div>
                            <!-- The table listing the files available for upload/download -->
                            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                        </form>
                        <!-- The blueimp Gallery widget -->
                        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                            <div class="slides"></div>
                            <h3 class="title"></h3>
                            <a class="prev">‹</a>
                            <a class="next">›</a>
                            <a class="close">×</a>
                            <a class="play-pause"></a>
                            <ol class="indicator"></ol>
                        </div>
                        <!-- The template to display files available for upload -->
                        <script id="template-upload" type="text/x-tmpl">
                          {% for (var i=0, file; file=o.files[i]; i++) { %}
                          <tr class="template-upload fade">
                          <td>
                          <span class="preview"></span>
                          </td>
                          <td>
                          <p class="name">{%=file.name%}</p>
                          <strong class="error text-danger"></strong>
                          </td>
                          <td>
                          <p class="size">In elaborazione...</p>
                          <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                          </td>
                          <td>
                          {% if (!i && !o.options.autoUpload) { %}
                          <button class="btn btn-primary start" disabled>
                          <i class="glyphicon glyphicon-upload"></i>
                          <span>Inizia</span>
                          </button>
                          {% } %}
                          {% if (!i) { %}
                          <button class="btn btn-warning cancel">
                          <i class="glyphicon glyphicon-ban-circle"></i>
                          <span>Annulla</span>
                          </button>
                          {% } %}
                          </td>
                          </tr>
                          {% } %}
                      </script>
                        <!-- The template to display files available for download -->
                        <script id="template-download" type="text/x-tmpl">
                          {% for (var i=0, file; file=o.files[i]; i++) { %}
                          <tr class="template-download fade">
                          <td>
                          <span class="preview">
                          {% if (file.thumbnailUrl) { %}
                          <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                          {% } %}
                          </span>
                          </td>
                          <td>
                          <p class="name">
                          {% if (file.url) { %}
                          <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                          {% } else { %}
                          <span>{%=file.name%}</span>
                          {% } %}
                          </p>
                          {% if (file.error) { %}
                          <div><span class="label label-danger">Errore</span> {%=file.error%}</div>
                          {% } %}
                          </td>
                          <td>
                          <span class="size">{%=o.formatFileSize(file.size)%}</span>
                          </td>
                          <td>
                          {% if (file.deleteUrl) { %}
                          <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                          <i class="glyphicon glyphicon-trash"></i>
                          <span>Elimina</span>
                          </button>
                          <input type="checkbox" name="delete" value="1" class="toggle">
                          {% } else { %}
                          <button class="btn btn-warning cancel">
                          <i class="glyphicon glyphicon-ban-circle"></i>
                          <span>Annulla</span>
                          </button>
                          {% } %}
                          </td>
                          </tr>
                          {% } %}
                      </script>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var BASE_URL = '<?php echo base_url(); ?>';
            var UPLOAD_URL = BASE_URL + 'customer/apartments/fileupload/<?php echo $this->Apartment_model->id ?>';
        </script>
    <?php endif; ?>
</div>
