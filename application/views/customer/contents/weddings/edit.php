<?php
$divID = ($usertype == 'admin') ? 'page-wrapper' : '';
?>
<div id=<?php echo $divID ?>>
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header"><i class="fa fa-heart"></i> Matrimoni</h1>
      </div>
      <!-- /.col-lg-12 -->
  </div>
  <?php if ($msg != '' && $type != ''): ?>
      <div class="alert alert-<?php echo $type ?> alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?php echo $msg; ?>
      </div>
  <?php endif; ?>
  <?php if (validation_errors()): ?>
      <div class="row">
          <div class="col-xs-12">
              <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo validation_errors(); ?>
              </div>
          </div>
      </div>
  <?php endif; ?>
  <div class="row">
      <div class="col-xs-12">
          <?php echo form_open_multipart(); ?>
          <div class="panel panel-default">
              <div class="panel-heading">
                  <?php echo $this->Orders_model->title; ?>
              </div>
              <div class="panel-body">
                  <input type="hidden" name="id" value="<?php echo isset($this->Weddings_model->id) ? $this->Weddings_model->id : '0'; ?>">
                  <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                      <li><a data-toggle="tab" href="#home">En</a></li>
                      <li><a data-toggle="tab" href="#menu1">De</a></li>
                  </ul>
                  <div class="tab-content">
                      <div id="menu2" class="tab-pane fade  in active">
                          <br>
                          <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="itName">Nome visualizzato [it]</label>
                                  <input type="text" name="itName" id="itName" placeholder="Enter Name in Italian" value="<?php echo set_value('itName') ? set_value('itName') : (isset($this->Weddings_model->itName) ? $this->Weddings_model->itName : ''); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="itSlogan">Slogan [it]</label>
                                  <input type="text" name="itSlogan" id="itSlogan" placeholder="Enter Slogan in Italian" value="<?php echo set_value('itSlogan') ? set_value('itSlogan') : (isset($this->Weddings_model->itSlogan) ? $this->Weddings_model->itSlogan : ''); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-xs-12" style="margin-top:10px">
                              <label class="control-label" for="it_editor">Descrizione [it]</label>
                              <textarea id="it_editor" name="itDescription" rows="5" cols="80" placeholder="Enter page description for Italian">
                                  <?php echo set_value('itDescription') ? set_value('itDescription') : (isset($this->Weddings_model->itDescription) ? $this->Weddings_model->itDescription : ''); ?>
                              </textarea>
                          </div>
                      </div>
                      <div id="home" class="tab-pane fade">
                          <br>
                          <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="enName">Nome visualizzato [en]</label>
                                  <input type="text" name="enName" id="enName" placeholder="Enter Name in English" value="<?php echo (set_value('enName') ? set_value('enName') : (isset($this->Weddings_model->enName) ? $this->Weddings_model->enName : '')); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="enSlogan">Slogan [en]</label>
                                  <input type="text" name="enSlogan" id="enSlogan" placeholder="Enter Slogan in English" value="<?php echo (set_value('enSlogan') ? set_value('enSlogan') : (isset($this->Weddings_model->enSlogan) ? $this->Weddings_model->enSlogan : '')); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-xs-12">
                              <label class="control-label" for="en_editor">Descrizione [en]</label>
                              <textarea id="en_editor" name="enDescription" rows="5" cols="80" placeholder="Enter page description for English">
                                  <?php echo set_value('enDescription') ? set_value('enDescription') : (isset($this->Weddings_model->enDescription) ? $this->Weddings_model->enDescription : ''); ?>
                              </textarea>
                          </div>
                      </div>
                      <div id="menu1" class="tab-pane fade">
                          <br>
                          <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="duName">Nome visualizzato [de]</label>
                                  <input type="text" name="duName" id="duName" placeholder="Enter Name in Duetch" value="<?php echo set_value('duName') ? set_value('duName') : (isset($this->Weddings_model->duName) ? $this->Weddings_model->duName : ''); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="form-group">
                                  <label class="control-label" for="duSlogan">Slogan [de]</label>
                                  <input type="text" name="duSlogan" id="duSlogan" placeholder="Enter Slogan in Duetch" value="<?php echo set_value('duSlogan') ? set_value('duSlogan') : (isset($this->Weddings_model->duSlogan) ? $this->Weddings_model->duSlogan : ''); ?>" class="form-control">
                              </div>
                          </div>
                          <div class="col-xs-12" style="margin-top:10px">
                              <label class="control-label" for="du_editor">Descrizione [de]</label>
                              <textarea id="du_editor" name="duDescription" rows="5" cols="80" placeholder="Enter page description for Deutch">
                                  <?php echo set_value('duDescription') ? set_value('duDescription') : (isset($this->Weddings_model->duDescription) ? $this->Weddings_model->duDescription : ''); ?>
                              </textarea>
                          </div>
                      </div>

                  </div>
                  <div class="col-xs-12" style="margin-top: 15px;"></div>
                  <div class="col-lg-4 col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="phone">Numero di telefono</label>
                          <input type="text" name="phone" value="<?php echo set_value('phone') ? set_value('phone') : (isset($this->Weddings_model->phone) ? $this->Weddings_model->phone : ''); ?>" class="form-control">
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="website">Sito Web</label>
                          <input type="text" name="website" value="<?php echo set_value('website') ? set_value('website') : (isset($this->Weddings_model->website) ? $this->Weddings_model->website : ''); ?>" class="form-control">
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="info">Informazioni</label>
                          <input type="text" name="Info" value="<?php echo set_value('Info') ? set_value('Info') : (isset($this->Weddings_model->Info) ? $this->Weddings_model->Info : ''); ?>" class="form-control">
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="menu_price">Prezzi medi del menù</label>
                          <input type="text" name="menu_price" value="<?php echo set_value('menu_price') ? set_value('menu_price') : (isset($this->Weddings_model->menu_price) ? $this->Weddings_model->menu_price : ''); ?>" class="form-control">
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="invited_people">Numero di persone</label>
                          <input type="text" name="invited_people" value="<?php echo set_value('invited_people') ? set_value('invited_people') : (isset($this->Weddings_model->invited_people) ? $this->Weddings_model->invited_people : ''); ?>" class="form-control">
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="address">Indirizzo</label>
                          <input type="text" name="address" value="<?php echo set_value('address') ? set_value('address') : (isset($this->Weddings_model->address) ? $this->Weddings_model->address : ''); ?>" class="form-control">
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="lat">Latitudine</label>
                          <input type="text" name="lat" value="<?php echo set_value('lat') ? set_value('lat') : (isset($this->Weddings_model->lat) ? $this->Weddings_model->lat : ''); ?>" class="form-control">
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="lng">Longitudine</label>
                          <input type="text" name="lng" value="<?php echo set_value('lng') ? set_value('lng') : (isset($this->Weddings_model->lng) ? $this->Weddings_model->lng : ''); ?>" class="form-control">
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="manager">Nome del manager</label>
                          <input type="text" name="manager" value="<?php echo set_value('manager') ? set_value('manager') : (isset($this->Weddings_model->manager) ? $this->Weddings_model->manager : ''); ?>" class="form-control">
                      </div>
                  </div>

                  <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="m_image">Foto del manager</label>
                        <input type="file"  name="m_image" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="closingDay">Giorni di chiusura</label>
                          <input type="text" name="closingDay" value="<?php echo set_value('closingDay') ? set_value('closingDay') : (isset($this->Weddings_model->closingDay) ? $this->Weddings_model->closingDay : ''); ?>" class="form-control">
                      </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <input type="hidden" name="hours" id="hours_" value="<?php echo set_value('hours') ? set_value('hours') : (isset($this->Weddings_model->hours) ? $this->Weddings_model->hours : ''); ?>">
                        <?php $hours = explode(" - ", $this->Weddings_model->hours);
                              $open = '';
                              $closed = '';
                              if(count($hours) >= 2){
                                $open = $hours[0];
                                $closed = $hours[1];
                              }
                        ?>
                        <input type="hidden" name="_openHours" id="_openHours" value="<?php echo $open ?>">
                        <input type="hidden" name="_closeHours" id="_closeHours" value="<?php echo $closed ?>">

                        <div class="form-group col-lg-6 col-xs-12">
                            <label class="control-label" for="hours">Orario di apertura</label>
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input id="openHour" type="text" class="form-control input-small">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-xs-12">
                            <label class="control-label" for="hours">Orario di chiusura</label>
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input id="closeHour" type="text" class="form-control input-small">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <input type="hidden" name="hours2" id="hours2_" value="<?php echo set_value('hours2') ? set_value('hours2') : (isset($this->Weddings_model->hours2) ? $this->Weddings_model->hours2 : ''); ?>">
                        <?php $hours2 = explode(" - ", $this->Weddings_model->hours2);
                              $open2 = '';
                              $closed2 = '';
                              if(count($hours2) >= 2){
                                $open2 = $hours2[0];
                                $closed2 = $hours2[1];
                              }
                        ?>
                        <input type="hidden" name="_openHours2" id="_openHours2" value="<?php echo $open2 ?>">
                        <input type="hidden" name="_closeHours2" id="_closeHours2" value="<?php echo $closed2 ?>">

                        <div class="form-group col-lg-6 col-xs-12">
                            <label class="control-label" for="hours2">Orario di apertura</label>
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input id="openHour2" type="text" class="form-control input-small">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-xs-12">
                            <label class="control-label" for="hours2">Orario di chiusura</label>
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input id="closeHour2" type="text" class="form-control input-small">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                    </div>
                  </div>


                  <div class="col-xs-12">
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="categories">Categorie</label>
                            <input type="hidden" name="main_category" id="main_category" value="<?php echo $main_category ?>"/>
                            <input type="hidden" name="selected_categories" id="selected_categories" value="<?php echo $this->Weddings_model->categories?>"/>
                            <select class="form-control" name="categories" id="categories" multiple>
                              <?php $activityCategories = explode(',', $this->Weddings_model->categories); ?>
                              <?php foreach ($categories as $key => $value): ?>
                                <option value="<?php echo $value->id ?>" <?php if($value->id == $main_category) echo 'selected="selected"'; else if(in_array( $value->id, $activityCategories) !== false) echo 'selected="selected"';?>><?php echo $value->name ?></option>
                              <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="creditCards">Carte di credito accettate</label>
                            <input type="hidden" name="creditCards" id="creditCards_selected" value="<?php echo $this->Weddings_model->creditCards?>"/>
                            <select class="form-control" name="credit_card" id="creditCards" multiple>
                              <?php $creditCard = explode(', ', isset($this->Weddings_model->creditCards) ? strtolower($this->Weddings_model->creditCards) : ''); ?>
                              <option value="americanexpress" <?php if(in_array( "americanexpress", $creditCard) !== false) echo 'selected="selected"';?>>American Express</option>
                              <option value="master" <?php if(in_array( "master", $creditCard) !== false) echo 'selected="selected"';?>>Master Card</option>
                              <option value="paypal" <?php if(in_array( "paypal", $creditCard) !== false) echo 'selected="selected"';?>>PayPal</option>
                              <option value="visa" <?php if(in_array( "visa", $creditCard) !== false) echo 'selected="selected"';?>>Visa</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="tags">Tag</label>
                            <input type="hidden" name="tags" id="selected_tags" value="<?php echo $this->Weddings_model->tags?>"/>
                            <select class="form-control" name="activity_tags" id="tags" multiple>
                                <?php $activityTags = explode(',', $this->Weddings_model->tags); ?>
                              <?php foreach ($tags as $key => $value): ?>
                                <option value="<?php echo $value->id ?>" <?php if(in_array( $value->id, $activityTags) !== false) echo 'selected="selected"';?>><?php echo $value->nameIT ?></option>
                              <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="activity_services">Servizi</label>
                            <input type="hidden" name="activity_services" id="selected_services" value="<?php echo $this->Weddings_model->activity_services?>"/>
                            <select class="form-control" name="services" id="activity_services" multiple>
                              <?php $activityServices = explode(',', $this->Weddings_model->activity_services); ?>
                              <?php foreach ($services as $key => $value): ?>
                                <option value="<?php echo $value->id ?>" <?php if(in_array( $value->id, $activityServices) !== false) echo 'selected="selected"';?>><?php echo $value->nameIT ?></option>
                              <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                  </div>
                  <?php   if($this->session->userdata('usertype') == 'admin'){ ?>
                      <div class="col-lg-12 col-md-12 col-xs-12">
                          <div class="form-group">
                              <label class="control-label" for="enInsta">Aggiungi il tuo codice widget di instagram qui</label>
                              <textarea class="form-control" id="enInsta" style="height:100px" name="enInsta"><?php echo htmlspecialchars_decode((set_value('enInsta') ? set_value('enInsta') : (isset($this->Weddings_model->enInsta) ? $this->Weddings_model->enInsta : ''))); ?></textarea>
                          </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="fbWidget">Aggiungi il tuo codice widget di facebook qui</label>
                            <textarea class="form-control" id="fbWidget" style="height:100px" name="fbWidget"><?php echo htmlspecialchars_decode((set_value('fbWidget') ? set_value('fbWidget') : (isset($this->Weddings_model->fbWidget) ? $this->Weddings_model->fbWidget : ''))); ?></textarea>
                        </div>
                      </div>
                <?php } ?>
                  <!-- <div class="col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="services">Servizi</label>
                          <br/>
                          <?php $services = explode(',', $this->Weddings_model->services); ?>
                          <input type="checkbox" name="services[]" value="Catering" id="services_catering" <?php echo in_array("Catering", $services) ? "checked='checked'" : "" ?>> <label for="services_catering"><?php echo lang("Catering"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="services[]" value="Hiring of kitchenware" id="services_hiringofkitchen" <?php echo in_array("Hiring of kitchenware", $services) ? "checked='checked'" : "" ?>> <label for="services_hiringofkitchen"><?php echo lang("Hiring of kitchenware"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="services[]" value="Linens and materials" id="services_linesandmaterials" <?php echo in_array("Linens and materials", $services) ? "checked='checked'" : "" ?>> <label for="services_linesandmaterials"><?php echo lang("Linens and materials"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="services[]" value="Wedding plan" id="services_weddingplan" <?php echo in_array("Wedding plan", $services) ? "checked='checked'" : "" ?>> <label for="services_weddingplan"><?php echo lang("Wedding plan"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="services[]" value="waiter service" id="services_waiter" <?php echo in_array("waiter service", $services) ? "checked='checked'" : "" ?>> <label for="services_waiter"><?php echo lang("Waiter service"); ?> </label> &nbsp;&nbsp;
                          <input type="checkbox" name="services[]" value="Beverage" id="services_beverages" <?php echo in_array("Beverage", $services) ? "checked='checked'" : "" ?>> <label for="services_beverages"><?php echo lang("Beverage"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="services[]" value="Music" id="services_Music" <?php echo in_array("Music", $services) ? "checked='checked'" : "" ?>> <label for="services_Music"><?php echo lang("Music"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="services[]" value="Venue provided" id="services_Venue_provided" <?php echo in_array("Venue provided", $services) ? "checked='checked'" : "" ?>> <label for="services_Venue_provided"><?php echo lang("Venue provided"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="services[]" value="preparation" id="services_preparation" <?php echo in_array("preparation", $services) ? "checked='checked'" : "" ?>> <label for="services_preparation"><?php echo lang("preparation"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="services[]" value="wedding cake" id="services_wedding_cake" <?php echo in_array("wedding cake", $services) ? "checked='checked'" : "" ?>> <label for="services_wedding_cake"><?php echo lang("wedding cake"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="services[]" value="candy" id="services_candy" <?php echo in_array("candy", $services) ? "checked='checked'" : "" ?>> <label for="services_candy"><?php echo lang("candy"); ?></label> &nbsp;&nbsp;
                      </div>
                  </div>
                  <div class="col-xs-12">
                      <div class="form-group">
                          <label class="control-label" for="cuisine">Tipo di cucina</label>
                          <br>
                          <?php $cuisines = explode(',', $this->Weddings_model->cuisine); ?>
                          <input type="checkbox" name="cuisine[]" value="KM0" id="cuisine_KMO" <?php echo in_array("KM0", $cuisines) ? "checked='checked'" : "" ?>> <label for="cuisine_KM0"><?php echo lang("KM0") ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="cuisine[]" value="Traditional" id="cuisine_Traditional" <?php echo in_array("Traditional", $cuisines) ? "checked='checked'" : "" ?>> <label for="cuisine_Traditional"><?php echo lang("Traditional") ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="cuisine[]" value="Regional" id="cuisine_Regional" <?php echo in_array("Regional", $cuisines) ? "checked='checked'" : "" ?>> <label for="cuisine_Regional"><?php echo lang("Regional"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="cuisine[]" value="Vegetarian" id="cuisine_Vegetarian" <?php echo in_array("Vegetarian", $cuisines) ? "checked='checked'" : "" ?>> <label for="cuisine_Vegetarian"><?php echo lang("Vegetarian"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="cuisine[]" value="International" id="cuisine_International" <?php echo in_array("International", $cuisines) ? "checked='checked'" : "" ?>> <label for="cuisine_International"><?php echo lang("International"); ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="cuisine[]" value="Vegan" id="cuisine_Vegan" <?php echo in_array("Vegan", $cuisines) ? "checked='checked'" : "" ?>> <label for="cuisine_Vegan"><?php echo lang("Vegan") ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="cuisine[]" value="Fusion" id="cuisine_Fusion" <?php echo in_array("Fusion", $cuisines) ? "checked='checked'" : "" ?>> <label for="cuisine_Fusion"><?php echo lang("Fusion") ?></label> &nbsp;&nbsp;
                          <input type="checkbox" name="cuisine[]" value="Mediterranean" id="cuisine_Mediterranean" <?php echo in_array("Mediterranean", $cuisines) ? "checked='checked'" : "" ?>> <label for="cuisine_Mediterranean"><?php echo lang("Mediterranean"); ?></label> &nbsp;&nbsp;
                      </div>
                  </div> -->
              </div>
              <div class="panel-footer" style="text-align: right;">
                  <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
              </div>
          </div>
          <?php echo form_close(); ?>
      </div>
  </div>
  <?php if (isset($this->Weddings_model->id)): ?>
      <div class="row">
          <div class="col-xs-12">
              <?php echo form_open_multipart('customer/weddings/upload_image/' . $this->Orders_model->id); ?>
              <div class="panel panel-default">
                  <div class="panel-heading">
                      Carica l'immagine primaria
                  </div>
                  <div class="panel-body">
                      <div class="col-lg-4 col-md-6 col-xs-12">
                          <div class="form-group">
                              <label class="control-label" for="p_image">Immagine primaria</label>
                              <input type="file" required="required" name="p_image" class="form-control">
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-6 col-xs-12">
                          <?php if (file_exists('./uploads/weddings/' . $this->Weddings_model->photo) && $this->Weddings_model->photo != ''): ?>
                              <img class="img-thumbnail" src="<?php echo base_url() . 'uploads/weddings/' . $this->Weddings_model->photo ?>">
                          <?php else: ?>
                              Non ci sono foto
                          <?php endif; ?>
                      </div>
                      <div class="col-lg-4 col-md-6 col-xs-12 well">
                        <strong>Istruzioni per l'upload</strong>
                        <ol>
                            <li>Dimensione massima immagine 2MB</li>
                            <li>Risoluzione consigliata 1400x600</li>
                        </ol>
                      </div>
                  </div>
                  <div class="panel-footer" style="text-align: right;">
                      <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Carica</button>
                  </div>
              </div>
              <?php echo form_close(); ?>
          </div>
    <div class="col-lg-12">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      Immagini per lo slider
                  </div>
                  <div class="panel-body">
                      <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                          <!-- Redirect browsers with JavaScript disabled to the origin page -->
                          <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                          <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                          <div class="row fileupload-buttonbar">
                              <div class="col-lg-7">
                                  <!-- The fileinput-button span is used to style the file input field as button -->
                                  <span class="btn btn-success fileinput-button">
                                      <i class="glyphicon glyphicon-plus"></i>
                                      <span>Aggiungi file...</span>
                                      <input type="file" name="files[]" multiple>
                                  </span>
                                  <button type="submit" class="btn btn-primary start">
                                      <i class="glyphicon glyphicon-upload"></i>
                                      <span>Inizia upload</span>
                                  </button>
                                  <button type="reset" class="btn btn-warning cancel">
                                      <i class="glyphicon glyphicon-ban-circle"></i>
                                      <span>Annulla upload</span>
                                  </button>
                                  <button type="button" class="btn btn-danger delete">
                                      <i class="glyphicon glyphicon-trash"></i>
                                      <span>Elimina</span>
                                  </button>
                                  <input type="checkbox" class="toggle">
                                  <!-- The global file processing state -->
                                  <span class="fileupload-process"></span>
                              </div>
                              <!-- The global progress state -->
                              <div class="col-lg-5 fileupload-progress fade">
                                  <!-- The global progress bar -->
                                  <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                      <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                  </div>
                                  <!-- The extended global progress state -->
                                  <div class="progress-extended">&nbsp;</div>
                              </div>
                          </div>
                          <!-- The table listing the files available for upload/download -->
                          <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                      </form>
                      <!-- The blueimp Gallery widget -->
                      <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                          <div class="slides"></div>
                          <h3 class="title"></h3>
                          <a class="prev">‹</a>
                          <a class="next">›</a>
                          <a class="close">×</a>
                          <a class="play-pause"></a>
                          <ol class="indicator"></ol>
                      </div>
                      <!-- The template to display files available for upload -->
                      <script id="template-upload" type="text/x-tmpl">
                          {% for (var i=0, file; file=o.files[i]; i++) { %}
                          <tr class="template-upload fade">
                          <td>
                          <span class="preview"></span>
                          </td>
                          <td>
                          <p class="name">{%=file.name%}</p>
                          <strong class="error text-danger"></strong>
                          </td>
                          <td>
                          <p class="size">In elaborazione...</p>
                          <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                          </td>
                          <td>
                          {% if (!i && !o.options.autoUpload) { %}
                          <button class="btn btn-primary start" disabled>
                          <i class="glyphicon glyphicon-upload"></i>
                          <span>Inizio</span>
                          </button>
                          {% } %}
                          {% if (!i) { %}
                          <button class="btn btn-warning cancel">
                          <i class="glyphicon glyphicon-ban-circle"></i>
                          <span>Annulla</span>
                          </button>
                          {% } %}
                          </td>
                          </tr>
                          {% } %}
                      </script>
                      <!-- The template to display files available for download -->
                      <script id="template-download" type="text/x-tmpl">
                          {% for (var i=0, file; file=o.files[i]; i++) { %}
                          <tr class="template-download fade">
                          <td>
                          <span class="preview">
                          {% if (file.thumbnailUrl) { %}
                          <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                          {% } %}
                          </span>
                          </td>
                          <td>
                          <p class="name">
                          {% if (file.url) { %}
                          <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                          {% } else { %}
                          <span>{%=file.name%}</span>
                          {% } %}
                          </p>
                          {% if (file.error) { %}
                          <div><span class="label label-danger">Errore</span> {%=file.error%}</div>
                          {% } %}
                          </td>
                          <td>
                          <span class="size">{%=o.formatFileSize(file.size)%}</span>
                          </td>
                          <td>
                          {% if (file.deleteUrl) { %}
                          <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                          <i class="glyphicon glyphicon-trash"></i>
                          <span>Elimina</span>
                          </button>
                          <input type="checkbox" name="delete" value="1" class="toggle">
                          {% } else { %}
                          <button class="btn btn-warning cancel">
                          <i class="glyphicon glyphicon-ban-circle"></i>
                          <span>Annulla</span>
                          </button>
                          {% } %}
                          </td>
                          </tr>
                          {% } %}
                      </script>
                  </div>
              </div>
          </div>
      </div>
      <script type="text/javascript">
          var BASE_URL = '<?php echo base_url(); ?>';
          var UPLOAD_URL = BASE_URL + 'customer/weddings/fileupload/<?php echo $this->Weddings_model->id ?>';
      </script>
  <?php endif; ?>
</div>
