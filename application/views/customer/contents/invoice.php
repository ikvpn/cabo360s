    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-file"></i> Fattura</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dettagli di fatturazione
            </div>
            <div class="panel-body">
                <h4>Informazioni attività</h4>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Titolo</th>
                                    <th>Categoria</th>
                                    <th>Località</th>
                                    <th>Data di iscrizione</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $this->Orders_model->title; ?></td>
                                    <td><?php echo $this->Orders_model->getLoadedCategoryName(); ?></td>
                                    <td><?php echo $this->Orders_model->getLoadedLocationName(); ?></td>
                                    <td><?php echo $this->Orders_model->cDate; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <h4>Informazioni cliente</h4>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Numero di telefono</th>
                                    <th>Società</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $this->Customer_model->firstName.' '.$this->Customer_model->lastName; ?></td>
                                    <td><?php echo $this->Customer_model->email; ?></td>
                                    <td><?php echo $this->Customer_model->billingPhone; ?></td>
                                    <td><?php echo $this->Customer_model->company; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <h4>Dettagli di pagamento</h4>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Tipo di pagamento</th>
                                    <th>Piano</th>
                                    <th>Costo</th>
                                    <th>Stato</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo strtoupper($this->Orders_model->payment_type); ?></td>
                                    <td><?php echo $this->Orders_model->payment_plan == 'M' ? lang('monthly') : lang('yearly'); ?></td>
                                    <td><?php echo $this->Orders_model->agreement_amount; ?> €</td>
                                    <td><?php echo strtoupper($this->Orders_model->status); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <h4>Dettagli di fatturazione</h4>
                    <strong>Indirizzo di fatturazione</strong>
                    <p><?php echo $this->Customer_model->billingAddress1.', '.$this->Customer_model->billingAddress2.',' ?></p>
                    <p><?php echo $this->Customer_model->billingCity.', '; echo $this->Customer_model->billingState.', ' ?><?php echo $this->Customer_model->billingPostcode ?></p>
                    <p><?php echo $this->Customer_model->billingCountry ?></p>
                    <strong>Numero di telefono</strong>
                    <p><?php echo $this->Customer_model->billingPhone ?></p>
            </div>
            <div class="panel-footer">

            </div>
        </div>
    </div>
    <!-- /.row -->
