<?php

$divID = ($usertype == 'admin') ? 'page-wrapper' : '';

?>

<div id=<?php echo $divID ?>>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-home"></i> Bed and breakfast</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != '' && $type != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <?php if (validation_errors()): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo validation_errors(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12">
            <?php echo form_open_multipart(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->Orders_model->title; ?>
                </div>
                <div class="panel-body">
                    <input type="hidden" name="id" value="<?php echo isset($this->Bed_and_breakfast_model->id) ? $this->Bed_and_breakfast_model->id : '0'; ?>">
                    <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#menu2">It</a></li>
                        <li><a data-toggle="tab" href="#home">En</a></li>
                        <li><a data-toggle="tab" href="#menu1">De</a></li>

                    </ul>
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="enName">Nome visualizzato [en]</label>
                                    <input type="text" name="enName" id="enName" placeholder="Enter Name in English" value="<?php echo (set_value('enName') ? set_value('enName') : (isset($this->Bed_and_breakfast_model->enName) ? $this->Bed_and_breakfast_model->enName : '')); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <label class="control-label" for="en_editor">Descrizione [en]</label>
                                <textarea id="en_editor" name="enDescription" rows="5" cols="80" placeholder="Enter page description for English">
                                    <?php echo set_value('enDescription') ? set_value('enDescription') : (isset($this->Bed_and_breakfast_model->enDescription) ? $this->Bed_and_breakfast_model->enDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="duName">Nome visualizzato [de]</label>
                                    <input type="text" name="duName" id="duName" placeholder="Enter Name in Duetch" value="<?php echo set_value('duName') ? set_value('duName') : (isset($this->Bed_and_breakfast_model->duName) ? $this->Bed_and_breakfast_model->duName : ''); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12" style="margin-top:10px">
                                <label class="control-label" for="du_editor">Descrizione [de]</label>
                                <textarea id="du_editor" name="duDescription" rows="5" cols="80" placeholder="Enter page description for Deutch">
                                    <?php echo set_value('duDescription') ? set_value('duDescription') : (isset($this->Bed_and_breakfast_model->duDescription) ? $this->Bed_and_breakfast_model->duDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade in active">
                            <br>
                            <div class="col-lg-4 col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" for="itName">Nome visualizzato [it]</label>
                                    <input type="text" name="itName" id="itName" placeholder="Enter Name in Italian" value="<?php echo set_value('itName') ? set_value('itName') : (isset($this->Bed_and_breakfast_model->itName) ? $this->Bed_and_breakfast_model->itName : ''); ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12" style="margin-top:10px">
                                <label class="control-label" for="it_editor">Descrizione [it]</label>
                                <textarea id="it_editor" name="itDescription" rows="5" cols="80" placeholder="Enter page description for Italian">
                                    <?php echo set_value('itDescription') ? set_value('itDescription') : (isset($this->Bed_and_breakfast_model->itDescription) ? $this->Bed_and_breakfast_model->itDescription : ''); ?>
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 15px;"></div>

                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lat">Latitudine</label>
                            <input type="text" name="lat" value="<?php echo set_value('lat') ? set_value('lat') : (isset($this->Bed_and_breakfast_model->lat) ? $this->Bed_and_breakfast_model->lat : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="lng">Longitudine</label>
                            <input type="text" name="lng" value="<?php echo set_value('lng') ? set_value('lng') : (isset($this->Bed_and_breakfast_model->lng) ? $this->Bed_and_breakfast_model->lng : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group" style="padding-top:30px;">
                            <a  class="btn btn-primary btn-xs" href="http://www.gps-coordinates.net/" target="_blank">Trova Lat/Lng</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="minPrice">Prezzo minimo</label>
                            <input type="text" name="minPrice" value="<?php echo set_value('minPrice') ? set_value('minPrice') : (isset($this->Bed_and_breakfast_model->minPrice) ? $this->Bed_and_breakfast_model->minPrice : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="maxPrice">Prezzo massimo</label>
                            <input type="text" name="maxPrice" value="<?php echo set_value('maxPrice') ? set_value('maxPrice') : (isset($this->Bed_and_breakfast_model->maxPrice) ? $this->Bed_and_breakfast_model->maxPrice : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="unit">Prezzo</label>
                            <select class="form-control" name="unit">
                                <option value="N" <?php echo set_select('unit', 'N') ? set_select('unit', 'N') : (isset($this->Bed_and_breakfast_model->unit) && $this->Bed_and_breakfast_model->unit == 'N' ? 'selected="selected"' : '') ?>><?php echo lang('per_night'); ?></option>
                                <option value="D" <?php echo set_select('unit', 'D') ? set_select('unit', 'D') : (isset($this->Bed_and_breakfast_model->unit) && $this->Bed_and_breakfast_model->unit == 'D' ? 'selected="selected"' : '') ?>><?php echo lang('per_day'); ?></option>
                                <option value="W" <?php echo set_select('unit', 'W') ? set_select('unit', 'W') : (isset($this->Bed_and_breakfast_model->unit) && $this->Bed_and_breakfast_model->unit == 'W' ? 'selected="selected"' : '') ?>><?php echo lang('per_week'); ?></option>
                                <option value="M" <?php echo set_select('unit', 'M') ? set_select('unit', 'M') : (isset($this->Bed_and_breakfast_model->unit) && $this->Bed_and_breakfast_model->unit == 'M' ? 'selected="selected"' : '') ?>><?php echo lang('per_month'); ?></option>
                                <option value="Y" <?php echo set_select('unit', 'Y') ? set_select('unit', 'Y') : (isset($this->Bed_and_breakfast_model->unit) && $this->Bed_and_breakfast_model->unit == 'Y' ? 'selected="selected"' : '') ?>><?php echo lang('per_year'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="rooms">Camere</label>
                            <input type="text" name="rooms" value="<?php echo set_value('rooms') ? set_value('rooms') : (isset($this->Bed_and_breakfast_model->rooms) ? $this->Bed_and_breakfast_model->rooms : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="beds">Letti</label>
                            <input type="text" name="beds" value="<?php echo set_value('beds') ? set_value('beds') : (isset($this->Bed_and_breakfast_model->beds) ? $this->Bed_and_breakfast_model->beds : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="pax">Persone</label>
                            <input type="text" name="pax" value="<?php echo set_value('pax') ? set_value('pax') : (isset($this->Bed_and_breakfast_model->pax) ? $this->Bed_and_breakfast_model->pax : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="trypology">Tipologia</label>
                            <input type="text" name="trypology" value="<?php echo set_value('trypology') ? set_value('trypology') : (isset($this->Bed_and_breakfast_model->trypology) ? $this->Bed_and_breakfast_model->trypology : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="bathrooms">Bagni</label>
                            <input type="text" name="bathrooms" value="<?php echo set_value('bathrooms') ? set_value('bathrooms') : (isset($this->Bed_and_breakfast_model->bathrooms) ? $this->Bed_and_breakfast_model->bathrooms : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="terrace">Terrazzo</label>
                            <input type="text" name="terrace" value="<?php echo set_value('terrace') ? set_value('terrace') : (isset($this->Bed_and_breakfast_model->terrace) ? $this->Bed_and_breakfast_model->terrace : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="manager">Nome del manager</label>
                            <input type="text" name="manager" value="<?php echo set_value('manager') ? set_value('manager') : (isset($this->Bed_and_breakfast_model->manager) ? $this->Bed_and_breakfast_model->manager : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="m_image">Foto del manager</label>
                            <input type="file"  name="m_image" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="phone">Numero di telefono</label>
                            <input type="text" name="phone" value="<?php echo set_value('phone') ? set_value('phone') : (isset($this->Bed_and_breakfast_model->phone) ? $this->Bed_and_breakfast_model->phone : ''); ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="col-lg-4 col-md-6 col-xs-12">
                          <div class="form-group">
                              <label class="control-label" for="categories">Categorie</label>
                              <input type="hidden" name="main_category" id="main_category" value="<?php echo $main_category ?>"/>
                              <input type="hidden" name="selected_categories" id="selected_categories" value="<?php echo $this->Bed_and_breakfast_model->categories?>"/>
                              <select class="form-control" name="categories" id="categories" multiple>
                                  <?php $activityCategories = explode(',', $this->Bed_and_breakfast_model->categories); ?>
                                <?php foreach ($categories as $key => $value): ?>
                                  <option value="<?php echo $value->id ?>" <?php if($value->id == $main_category) echo 'selected="selected"'; else if(in_array( $value->id, $activityCategories) !== false) echo 'selected="selected"';?>><?php echo $value->name ?></option>
                                <?php endforeach; ?>
                              </select>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-6 col-xs-12">
                          <div class="form-group">
                              <label class="control-label" for="tags">Tag</label>
                              <input type="hidden" name="tags" id="selected_tags" value="<?php echo $this->Bed_and_breakfast_model->tags?>"/>
                              <select class="form-control" name="activity_tags" id="tags" multiple>
                                  <?php $activityTags = explode(',', $this->Bed_and_breakfast_model->tags); ?>
                                <?php foreach ($tags as $key => $value): ?>
                                  <option value="<?php echo $value->id ?>" <?php if(in_array( $value->id, $activityTags) !== false) echo 'selected="selected"';?>><?php echo $value->nameIT ?></option>
                                <?php endforeach; ?>
                              </select>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-6 col-xs-12">
                          <div class="form-group">
                              <label class="control-label" for="activity_services">Servizi</label>
                              <input type="hidden" name="activity_services" id="selected_services" value="<?php echo $this->Bed_and_breakfast_model->activity_services?>"/>
                              <select class="form-control" name="services" id="activity_services" multiple>
                                  <?php $activityServices = explode(',', $this->Bed_and_breakfast_model->activity_services); ?>
                                <?php foreach ($services as $key => $value): ?>
                                  <option value="<?php echo $value->id ?>" <?php if(in_array( $value->id, $activityServices) !== false) echo 'selected="selected"';?>><?php echo $value->nameIT ?></option>
                                <?php endforeach; ?>
                              </select>
                          </div>
                      </div>
                    </div>

                    <?php   if($this->session->userdata('usertype') == 'admin'){ ?>
      					        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="enInsta">Aggiungi il tuo codice widget di instagram qui</label>
                                <textarea class="form-control" id="enInsta" style="height:100px" name="enInsta"><?php echo htmlspecialchars_decode((set_value('enInsta') ? set_value('enInsta') : (isset($this->Bed_and_breakfast_model->enInsta) ? $this->Bed_and_breakfast_model->enInsta : ''))); ?></textarea>
                            </div>
                      	</div>
                        <div class="col-lg-12 col-md-12 col-xs-12">
                          <div class="form-group">
                              <label class="control-label" for="fbWidget">Aggiungi il tuo codice widget di facebook qui</label>
                              <textarea class="form-control" id="fbWidget" style="height:100px" name="fbWidget"><?php echo htmlspecialchars_decode((set_value('fbWidget') ? set_value('fbWidget') : (isset($this->Bed_and_breakfast_model->fbWidget) ? $this->Bed_and_breakfast_model->fbWidget : ''))); ?></textarea>
                          </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="tripWidget">Aggiungi il tuo codice widget di facebook qui</label>
                                <textarea class="form-control" id="tripWidget" style="height:100px" name="tripWidget"><?php echo htmlspecialchars_decode((set_value('tripWidget') ? set_value('tripWidget') : (isset($this->Bed_and_breakfast_model->tripWidget) ? $this->Bed_and_breakfast_model->tripWidget : ''))); ?></textarea>
                            </div>
                        </div>
                  <?php } ?>
                </div>
                <div class="panel-footer" style="text-align: right;">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salva</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <?php if (isset($this->Bed_and_breakfast_model->id)): ?>
        <div class="row">
            <div class="col-xs-12">
                <?php echo form_open_multipart('customer/bed_and_breakfast/upload_image/' . $this->Orders_model->id); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Carica l'immagine primaria
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-4 col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label" for="p_image">Immagine Primaria</label>
                                <input type="file" required="required" name="p_image" class="form-control">
                            </div>
                        </div>
                      <div class="col-lg-4 col-md-6 col-xs-12">
                            <?php if (file_exists('./uploads/bed_and_breakfast/' . $this->Bed_and_breakfast_model->photo) && $this->Bed_and_breakfast_model->photo != ''): ?>
                                <img class="img-thumbnail" src="<?php echo base_url() . 'uploads/bed_and_breakfast/' . $this->Bed_and_breakfast_model->photo ?>">
                            <?php else: ?>
                                Non ci sono foto
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-4 col-md-6 col-xs-12 well">
                            <strong>Istruzioni per l'upload</strong>
                            <ol>
                                <li>Dimensione massima immagine 2MB</li>
                                <li>Risoluzione consigliata 1400x600</li>
                            </ol>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right;">
                        <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Carica</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Immagini per lo slider
                    </div>
                    <div class="panel-body">
                        <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                            <!-- Redirect browsers with JavaScript disabled to the origin page -->
                            <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                            <div class="row fileupload-buttonbar">
                                <div class="col-lg-7">
                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                    <span class="btn btn-success fileinput-button">
                                      <i class="glyphicon glyphicon-plus"></i>
                                      <span>Aggiungi file...</span>
                                      <input type="file" name="files[]" multiple>
                                  </span>
                                    <button type="submit" class="btn btn-primary start">
                                        <i class="glyphicon glyphicon-upload"></i>
                                        <span>Inizio upload</span>
                                    </button>
                                    <button type="reset" class="btn btn-warning cancel">
                                        <i class="glyphicon glyphicon-ban-circle"></i>
                                        <span>Annulla upload</span>
                                    </button>
                                    <button type="button" class="btn btn-danger delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Elimina</span>
                                    </button>
                                    <input type="checkbox" class="toggle">
                                    <!-- The global file processing state -->
                                    <span class="fileupload-process"></span>
                                </div>
                                <!-- The global progress state -->
                                <div class="col-lg-5 fileupload-progress fade">
                                    <!-- The global progress bar -->
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                    </div>
                                    <!-- The extended global progress state -->
                                    <div class="progress-extended">&nbsp;</div>
                                </div>
                            </div>
                            <!-- The table listing the files available for upload/download -->
                            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                        </form>
                        <!-- The blueimp Gallery widget -->
                        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                            <div class="slides"></div>
                            <h3 class="title"></h3>
                            <a class="prev">‹</a>
                            <a class="next">›</a>
                            <a class="close">×</a>
                            <a class="play-pause"></a>
                            <ol class="indicator"></ol>
                        </div>
                        <!-- The template to display files available for upload -->
                        <script id="template-upload" type="text/x-tmpl">
                          {% for (var i=0, file; file=o.files[i]; i++) { %}
                          <tr class="template-upload fade">
                          <td>
                          <span class="preview"></span>
                          </td>
                          <td>
                          <p class="name">{%=file.name%}</p>
                          <strong class="error text-danger"></strong>
                          </td>
                          <td>
                          <p class="size">In elaborazione...</p>
                          <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                          </td>
                          <td>
                          {% if (!i && !o.options.autoUpload) { %}
                          <button class="btn btn-primary start" disabled>
                          <i class="glyphicon glyphicon-upload"></i>
                          <span>Inizia</span>
                          </button>
                          {% } %}
                          {% if (!i) { %}
                          <button class="btn btn-warning cancel">
                          <i class="glyphicon glyphicon-ban-circle"></i>
                          <span>Annulla</span>
                          </button>
                          {% } %}
                          </td>
                          </tr>
                          {% } %}
                      </script>
                        <!-- The template to display files available for download -->
                        <script id="template-download" type="text/x-tmpl">
                          {% for (var i=0, file; file=o.files[i]; i++) { %}
                          <tr class="template-download fade">
                          <td>
                          <span class="preview">
                          {% if (file.thumbnailUrl) { %}
                          <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                          {% } %}
                          </span>
                          </td>
                          <td>
                          <p class="name">
                          {% if (file.url) { %}
                          <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                          {% } else { %}
                          <span>{%=file.name%}</span>
                          {% } %}
                          </p>
                          {% if (file.error) { %}
                          <div><span class="label label-danger">Errore</span> {%=file.error%}</div>
                          {% } %}
                          </td>
                          <td>
                          <span class="size">{%=o.formatFileSize(file.size)%}</span>
                          </td>
                          <td>
                          {% if (file.deleteUrl) { %}
                          <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                          <i class="glyphicon glyphicon-trash"></i>
                          <span>Elimina</span>
                          </button>
                          <input type="checkbox" name="delete" value="1" class="toggle">
                          {% } else { %}
                          <button class="btn btn-warning cancel">
                          <i class="glyphicon glyphicon-ban-circle"></i>
                          <span>Annulla</span>
                          </button>
                          {% } %}
                          </td>
                          </tr>
                          {% } %}
                      </script>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var BASE_URL = '<?php echo base_url(); ?>';
            var UPLOAD_URL = BASE_URL + 'customer/bed_and_breakfast/fileupload/<?php echo $this->Bed_and_breakfast_model->id ?>';
        </script>
    <?php endif; ?>
</div>
