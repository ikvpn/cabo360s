<?php
/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */
?>
<style>
    .cat_icon{
        font-size: 48px;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    #page-wrapper .fa-ship{
        padding-top: 40px;
    }
    #page-wrapper .fa-ship:before {
        content: "";
        background-image: url(<?php echo base_url() ?>/assets/images/ship.png);
        width: 48px;
        height: 48px;
        position: absolute;
        background-size: contain;
        left: 0;
        right: 0;
        margin: 0 auto;
        top: 10px;
    }
</style>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-bookmark"></i> Registra attività</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if ($msg != ''): ?>
        <div class="alert alert-<?php echo $type ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-xs-12">
            <strong>Seleziona la categoria</strong>
        </div>
    </div>
    <br>
    <div class="row">
        <?php if (isset($categories) && is_array($categories)): ?>
            <?php foreach ($categories as $category): ?>
                <div class="col-lg-2 col-md-4 col-xs-6">
                    <a href='<?php echo base_url() .$this->lang->lang(). '/customer/register_business/newbusiness/'. $category->id?>' style="margin-bottom: 30px;" class="col-xs-12 btn btn-default">
                        <center class="text-info"><i class="fa <?php echo $category->icon; ?> cat_icon"></i><br>
                            <?php echo lang($category->table); ?>
                        </center>
                    </a>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <!-- /.row -->
