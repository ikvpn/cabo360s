<!-- Styles -->
<style>
#chartdiv {
	width		: 100%;
	height: 300px;
	font-size	: 11px;
}
#chartdiv a[title] {
  display: none !important;
}
#contactChart {
	width		: 100%;
	height: 300px;
	font-size	: 11px;
}
#contactChart a[title] {
  display: none !important;
}
</style>

<!-- /.row -->
<div class="row">
  <div class="col-xs-12">
    <div class="col-md-6">
      <h2 class="page-header">Visite alle pagine</h2>
      <div id="chartdiv"></div>
    </div>
    <div class="col-md-6">
      <h2 class="page-header">Contatti ricevuti</h2>
      <div id="contactChart"></div>
    </div>
  </div>
</div>
<hr>
<div class="col-lg-12">
    <h1 class="page-header"><i class="fa fa-files-o"> </i> Riepilogo prenotazioni</h1>
</div>
<div class="col-md-12">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo $title; ?>
        </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="new_pages">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Cliente</th>
                    <th>Pickup</th>
                    <th>Dropoff</th>
                    <th>Serv.</th>
                    <th>Arrivo</th>
                    <th>Ritorno</th>
                    <th>Azienda</th>
                    <th>Prezzo</th>
                    <th>Stato</th>
                  </tr>
                </thead>
                <tbody>
                <?php $i = 1;
                foreach ($order_details as $value) { ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $value->firstname.' '.$value->lastname; ?></td>
                    <td><?php echo $value->pickupAddress; ?></td>
                    <td><?php echo $value->dropoffAddress; ?></td>
                    <td><?php echo $value->serviceType; ?></td>
                    <td><?php echo $value->checkIn; ?></td>
                    <td><?php echo $value->checkOut; ?></td>
                    <td><?php echo $value->company; ?></td>
                    <td><?php echo $value->total; ?></td>
                    <td><?php echo $value->status; ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.row -->
