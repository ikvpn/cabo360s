<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="sub-title">Terms and Conditions</h2>
                </div>

                <div class="col-xs-12">
                  <p>The following terms and conditions - and subsequent changes - apply to all our services made available online, directly or indirectly (via distributors), via any mobile device, via email or by phone. By accessing our website (also from mobile devices) by visiting it, using it and / or using any of the applications available through any platform (hereinafter referred to as &quot;website&quot;), and / or making a reservation, declare to have read , Including and accepting the terms and conditions set forth below (including the Privacy Policy). These pages, their content, their structure and the online booking service provided on the same and through this website are property, managed and offered by Visitprocida.com and are provided for exclusive personal use for non-commercial purposes, governed by the terms And the conditions outlined below.</p>

                  <h3>0. Definitions</h3>
                  <p>  The words &quot;Visitprocida.com&quot;, &quot;us&quot; and &quot;ours&quot; refer to Nicola Muro, an individual company subject to Italian law and with legal headquarters at via flavio gioia 13, 80079 Procida, Naples.&quot;Platform&quot; means a website, mobile site and app under the ownership and control of Visitprocida.com and managed, maintained and / or hosted by the Service on which the Service is made available. &quot;Service&quot; means the online booking service (including payment management) of various products and services made available by the Partners on the Platform. &quot;Partner&quot; means the provider of accommodation (eg a hotel, motel, apartment or bed and breakfast), attractions, museums, tours, cruises, train and coach tours, transfers, tour operators and any other related product or service Or similar to the travel theme, made available for reservation on the Platform.
                  <h3>1. Scopo del servizio</h3>
                    <p>Through the Platform, we (Visitprocdida.com) provide an online platform that allows Partners to advertise their products and services and visitors to the Platform to make reservations (ie the booking service). By making a reservation through Visitprocida.com, you will establish a direct contractual relationship (legally binding) with the Partner at which you made the reservation of the product or service (where applicable). From the moment of the above mentioned reservation, we will only become intermediaries between you and the Partner, sending to the Affiliates the details of the reservation and sending you a confirmation e-mail for and on behalf of the Partner.<br />
                    Clear errors are not binding, including printing.
                   <h3>2. Tariffe e disponibilità</h3>
                   <p> Hotel rates and availability are managed directly by partner structures, through Hotel.BB (a management software provided by NETSKIN.NET Piazza Guglielmo Marconi, 3 95030 Sant'Agata Li Battiati, Catania, Italy). All Partners through access to hotel.bb are fully responsible for updating rates, availability and other information displayed on our Platform. Although we provide our Service with diligence and attention, we are unable to verify and guarantee the accuracy, completeness and correctness of the information, nor can we be held responsible for any errors (including posters and typing), service interruption (Which is due to (temporary and / or partial) failure, Repair, Upgrade or Maintenance of the Platform or otherwise) for inaccurate, misleading or false information or for failure to deliver them. Each Partner is responsible at any time for the accuracy, completeness and correctness of the information (descriptive), including the rates and availability displayed on our Platform. Our Platform has no purpose, and should not be seen in this regard, to recommend or support any Affiliate Marketed on it, in terms of quality, level of service, classification and rating of the category through stars or relatively To its locations and facilities or services and products offered.</p>

                  <h3>3. Privacy e cookies</h3>
                  <p>Visitprocida.com respects your privacy. Check out our privacy policy and cookies for more information.</p>

                  <h3>4. Free service</h3>
                  <p>Our service is free as, Partners pay a commission, that is, a small percentage of the product's price (for example, the rate of the accommodation) at Visitprocida.com after the guest has benefited from the Partner's product or service After staying at the property (and paying the amount due to it).</p>

                  <h3>5. Credit Card Payments</h3>
                  <p>For hotel bookings, your credit card details will be used directly by Partners to guarantee your reservation. In some cases, this may be pre-authorized or charged (sometimes without refund) at the time of booking. Before making a reservation, please be aware of the presence of these conditions in the details regarding the product or service you have chosen. visitprocida.com is not responsible for any charges (whether authorized or (presumed) unauthorized or incorrect) by the Partner and you will not be able to claim the refund of the amount properly charged to your credit card by the Partner (including pre- Paid, missed presentation or cancellations with penalties). In the event of fraud or unauthorized use of your credit card by third parties, most banks and credit card providers are liable for the risk and cover all charges resulting from such fraud or unauthorized use. </p>

                  <h3>6. Cancellation, Missing Presentation and Information To know</h3>
                  <p>By making a reservation with a Partner, you agree to and approve the respective cancellation and non-submission policies of that Partner and any other applicable Partner's terms and conditions applicable to your reservation or during your stay (including the &quot;Know&quot; section of the Partner , Present on our Platform and the rules of the Partner's structure), including the services and / or products offered by the structure (the rules and conditions of a facility are obtained directly from the property itself). The general cancellation and non-disclosure rules of each Partner are mentioned on the Platform in the Partner information pages, during the booking process and on the confirmation email or ticket. Cancellations or modifications are not permitted for certain rates or special offers. If you want to check, modify or cancel your booking, log in to your confirmation e-mail and follow the instructions contained therein. The property may charge a cancellation fee in accordance with the cancellation, prepayment and non-delivery rules of the hotel, and you may not be eligible for any sums already paid (in advance). Before booking, we recommend that you read carefully the cancellation, prepayment and non-delivery instructions provided by the hotel, and remember to make subsequent payments as specified in the specific reservation.</p>

                  <h3>7. Correspondence and further communications</h3>

                  <p>Visitprocida.com is not responsible for any communication with the Partner that takes place on or through its Platform. You can not advance rights related to requests or communications directed to the Partner, or related to any form of notification of receiving requests or communications. visitprocida.com can not guarantee that any request or communication will be received, read, billed, executed or accepted by the Partner in a precise and timely manner.<br />
                  In order to complete and complete the booking successfully you must provide a valid email address. We are not responsible for any non-existent or incorrect e-mail addresses, phone numbers (cellular phone numbers) or credit card numbers, nor are we required to verify their existence or correctness. Any claims or claims against visitprocida.com or related to the Service must be filed as soon as possible and in any event within 30 days of the date on which the use of the product or service (eg the date Check-out for a stay). Claims and claims filed after such a 30-day period may be rejected and the applicant will have to waive any right to compensation (damages or costs).</p>

                  <h3>9. Disclaimer of Liability</h3>
                  <p>Without prejudice to the limitations and the particulars indicated in the hypotheses provided by these terms and conditions and as far as permitted by law, we will be held responsible only for the direct and immediate damage actually suffered, paid or incurred due to non-compliance Our obligations in respect of the services we would have to fulfill. Compensation may reach up to the total amount of the cost of your reservation, as stated in the confirmation email (or for a single circumstance or for a number of related circumstances). Whether the Partner charges (or has already charged) the cost of the room, product or service, whether the payment (of the room or the booking) is handled by us, in both cases accept and declare to be aware that It is always the responsibility of the Partner to collect, retain, send and pay to the competent authorities the applicable fees, calculated on the basis of the total price of the room or reservation. Visitprocida.com can not be held liable (nor can be called to answer) for the collection, retention, sending or payment to the competent authorities of applicable fees at the cost of the room or reservation. </p>

                  <h3>10. Intellectual Property Rights</h3>
                  <p>Visitprocida.com does not act or operate under any circumstances as an official seller of any product or service made available on the Platform. Visitprocida.com exclusively holds all rights, titles and interests in and against all intellectual property rights relating to the graphic aspect and the general interface (including the infrastructure) of the Platform through which the service Is made available (including guest reviews and translated content). No one is authorized to copy, extract, link, publish, promote, integrate, use, combine, or otherwise use the content (translations and guest ratings included) or our trademark in the absence of our express written consent . If a subject uses or combines (in whole or in part) our content (including translations) or otherwise possesses any intellectual property rights of the Platform or any content (or translation), it will have to attribute, Transfer and transfer all such intellectual property rights to Visitprocida.com. Any use that is not in accordance with the law or any of the above actions or behaviors constitutes a material breach of our intellectual property rights (including copyright and data protection rights).</p>
                </div>

            </div>
    </section>
    <section class="bottom-section gray">

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>

            </div>

        </div>

    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
