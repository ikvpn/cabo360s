<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="sub-title">Terms and Conditions</h2>
                </div>

                <div class="col-xs-12">
                  <p>
                    <strong>Information ex D. Lgs. 196/03 - Protection of privacy.</strong> The personal data collected with this card is processed for the site registration, statistical processing, and sending, if desired, information about VisitProcida.it activities and services, including automated, strictly necessary for such purposes. We also inform you that communication data is compulsory and any refusal may result in failure to meet the obligations proposed by our website for the provision of the service you requested.</p>
                     <p> The holder of the treatment is Nicola Muro, Via Flavio Gioia 13, 80079, Procida, (Naples). You may exercise the rights as referred in Article 7 of the Legislative Decree no. 196/03 (access, integration, correction, opposition, cancellation) by writing to
                       <a href="mailto:visitprocida@gmail.com" target="_top">visitprocida@gmail.com</a>. </p>
                       <p> Data will be processed by VisitProcida, the project managers and administrators. Consent - Read the information, * By filling out this brochure, I consent to the processing of my personal data in the manner and for the purposes indicated in the given information * by the delivery of the e-mail address, facsimile number and /or phone number for the purpose of sending useful information.
                  </p>
                </div>

            </div>
    </section>
    <section class="bottom-section gray">

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>

            </div>

        </div>

    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
