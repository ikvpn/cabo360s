<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="sub-title">Privacy Policy</h2>
                </div>

                <div class="col-xs-12">
                    <p>
                      <strong>Informativa ex D. Lgs. n. 196/03 - Tutela della privacy.</strong> I dati personali raccolti con questa scheda vengono trattati per la registrazione al sito, per elaborazioni di tipo statistico, e per l'invio, se lo desidera, di informazioni su attività e servizi di VisitProcida.it con modalità, anche automatizzate, strettamente necessarie a tali scopi. Informiamo ancora che la comunicazione dei dati è obbligatoria e l'eventuale rifiuto potrebbe comportare il mancato puntuale adempimento delle obbligazioni assunte dal nostro sito per la fornitura del servizio da Lei richiesto. </p>
                      <p>Titolare del trattamento è Nicola Muro, Via Flavio Gioia13, 13, 80079, Procida, (Napoli). Potrà esercitare i diritti di cui all'articolo 7 del D. Lgs. n. 196/03 (accesso, integrazione, correzione, opposizione, cancellazione) scrivendo a <a href="mailto:visitprocida@gmail.com" target="_top">visitprocida@gmail.com</a>.</p>
                        <p>I dati saranno trattati, per Visit Procida, da addetti preposti alla gestione dell'iniziativa e dall'amministrazione. Consenso - Letta l'informativa, *con la compilazione della presente scheda consento al trattamento dei miei dati personali con le modalità e per le finalità indicate nella stessa informativa * attraverso il conferimento dell'indirizzo e-mail, del numero di telefax e/o del numero di telefono consento all'utilizzo di questi strumenti per l'invio di informazioni utili.
                    </p>
                </div>

            </div>
    </section>
    <section class="bottom-section gray">

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>

            </div>

        </div>

    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
