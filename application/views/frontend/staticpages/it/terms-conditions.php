<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="sub-title">Termini e condizioni generali</h2>
                </div>

                <div class="col-xs-12">
                    <p>I seguenti termini e condizioni - e le successive modifiche apportate - si applicano a tutti i nostri servizi resi disponibili online, in modo diretto o indiretto (attraverso i distributori), tramite qualsiasi dispositivo mobile, via e-mail o per telefono. Accedendo al nostro sito web (anche da dispositivi mobili), visitandolo, utilizzandolo e/o utilizzando una qualsiasi delle applicazioni disponibili attraverso una qualsiasi piattaforma (di seguito definiti come &quot;sito web&quot;), e/o effettuando una prenotazione, dichiari di aver letto, compreso e accettato i termini e le condizioni di seguito riportati (inclusa l'Informativa sulla privacy).<br /> Queste pagine, il loro contenuto, la loro struttura e il servizio di prenotazione online fornito sulle stesse e attraverso questo sito web sono di proprietà, gestiti e offerti da Visitprocida.com e vengono forniti per esclusivo uso personale con scopi non commerciali, regolato dai termini e dalle condizioni di seguito descritti.
                    </p>

                    <h3>0. Definizioni</h3>
                    <p>  Le parole &quot;Visitprocida.com&quot;, &quot;noi&quot; e &quot;nostro/a/i/e&quot; si riferiscono a Nicola Muro, ditta individuale soggetta alle leggi Italiane e con sede legale presso via flavio gioia 13, 80079 Procida, Napoli.  &quot;Piattaforma&quot;: sito web, sito mobile e app sotto la proprietà e il controllo di Visitprocida.com e da esso gestiti, manutenuti e/o ospitati, su cui il Servizio viene reso disponibile.  &quot;Servizio&quot;: il servizio di prenotazione online (inclusa la gestione del relativo pagamento) di vari prodotti e servizi resi disponibili dai Partner sulla Piattaforma. &quot;Partner&quot;: il fornitore di alloggi (per esempio un hotel, motel, appartamento o bed and breakfast), attrazioni, musei, giri turistici, crociere, tour in treno e in pullman, trasferimenti, tour operator e qualsiasi altro prodotto o servizio relativo o affine al tema dei viaggi, reso disponibile per la prenotazione sulla Piattaforma.</p>
                    <h3>1. Scopo del servizio</h3>
                    <p>Attraverso la Piattaforma, noi (Visitprocdida.it) forniamo una piattaforma online che consente ai Partner di pubblicizzare i propri prodotti e servizi e ai visitatori della Piattaforma di effettuare le prenotazioni (cioè il servizio di prenotazione). Effettuando la prenotazione tramite Visitprocida.com, instaurerai un rapporto contrattuale diretto (vincolante in termini legali) con il Partner presso il quale hai effettuato la prenotazione del prodotto o del servizio (dove applicabile). Dal momento della suddetta prenotazione, noi faremo esclusivamente da intermediari tra te e il Partner, trasmettendo ai Partner di pertinenza i dettagli della prenotazione e inviando a te una e-mail di conferma per e a nome del Partner.<br /> Gli errori evidenti non sono vincolanti, compresi quelli di stampa.</p>

                    <h3>2. Tariffe e disponibilità</h3>
                    <p> Le tariffe e disponibilità alberghiere sono gestite direttamente delle strutture partner, attraverso Hotel.BB (un software gestionale fornito da  NETSKIN.NET Piazza Guglielmo Marconi, 3 95030 Sant'Agata Li Battiati, Catania, Italy). Tutti i Partner tramite accesso ad hotel.bb, sono totalmente responsabili per ciò che riguarda l'aggiornamento delle tariffe, della disponibilità e delle altre informazioni visualizzate sulla nostra Piattaforma. Sebbene prestiamo il nostro Servizio con diligenza e attenzione, non siamo in grado di verificare e garantire l'accuratezza, la completezza e la correttezza delle informazioni, né possiamo essere ritenuti responsabili per qualsiasi errore (inclusi quelli manifesti e di battitura), interruzione del servizio (che sia dovuta a guasto (temporaneo e/o parziale), alla riparazione, all'upgrade o alla manutenzione della Piattaforma o altro), per l'informazione imprecisa, fuorviante o falsa o per il loro mancato recapito. Ogni Partner è responsabile in qualsiasi momento dell'accuratezza, della completezza e della correttezza delle informazioni (descrittive), incluse le tariffe e la disponibilità visualizzate sulla nostra Piattaforma. La nostra Piattaforma non ha la finalità, e non dovrà esser vista sotto quest'ottica, di raccomandare o di sostenere nessun Partner commercializzato sulla stessa, in termini di qualità, di livello di servizio, di classificazione e di assegnazione della categoria tramite stelle o relativamente alle sue sedi e strutture o servizi e prodotti offerti.
                    </p>

                    <h3>3. Privacy e cookie</h3>
                    <p>Visitprocida.com rispetta la tua privacy. Consulta le nostre norme sulla privacy e sui cookie per ulteriori informazioni.</p>

                    <h3>4. Servizio gratuito</h3>
                    <p>Il nostro servizio è gratuito poiché,  I Partner pagano una commissione, ovvero una piccola percentuale sul prezzo del prodotto (ad esempio, sulla tariffa dell'alloggio) a Visitprocida.com dopo che l'ospite ha usufruito del prodotto o del servizio del Partner o dopo che ha soggiornato presso la struttura (e pagato l'importo dovuto alla stessa).</p>

                    <h3>5. Pagamenti con Carta di credito</h3>
                    <p>Per le prenotazioni alberghiere i dati della carta di credito verranno utilizzati direttamente dai Partner a garanzia della prenotazione. In alcuni casi, questa potrebbe essere pre-autorizzata o addebitata (talvolta senza possibilità di rimborso) all'atto della prenotazione. Prima di effettuare la prenotazione, verifica con attenzione la presenza di tali condizioni nei dettagli riguardanti il prodotto o il servizio che hai scelto. Visitprocida.com non è da considerarsi responsabile per qualsiasi addebito (autorizzato o (presunto) non autorizzato o errato) da parte del Partner e non potrai chiedere la restituzione dell'importo correttamente addebitato sulla tua carta di credito dal Partner (inclusi: tariffe pre-pagate, mancata presentazione o cancellazioni con penale).<br />
                    In caso di frode o di uso non autorizzato della tua carta di credito da parti terze, la maggior parte delle banche e delle società fornitrici delle carte di credito si fanno carico del rischio e coprono tutti gli addebiti risultanti da tali frodi o usi non autorizzati. Questo potrebbe però comportare l'addebito sulla tua carta di un importo deducibile (solitamente corrispondente a 50 EUR, o all'equivalente nella tua valuta). Nel caso in cui la società che rilascia la tua carta di credito o la banca ti addebitino tale importo a causa delle transazioni non autorizzate derivanti da una prenotazione effettuata sulla nostra Piattaforma, ti risarciremo tale importo, fino a un ammontare pari a 50 EUR (o all'equivalente nella tua valuta locale). </p>

                    <h3>6. Cancellazione, mancata presentazione e informazioni &quot;Da sapere&quot;</h3>
                    <p>Effettuando una prenotazione presso un Partner, accetti e approvi le rispettive norme di cancellazione e di mancata presentazione di quel Partner e qualsiasi altra norma o condizione aggiuntiva del Partner applicabile alla tua prenotazione o durante il tuo soggiorno (inclusa la sezione &quot;Da sapere&quot; del Partner, presente sulla nostra Piattaforma e le relative regole della struttura del Partner), inclusi i servizi e/o i prodotti offerti dalla struttura (le norme e le condizioni di una struttura si ottengono direttamente dalla struttura stessa di pertinenza). Le norme generali di cancellazione e di mancata presentazione di ciascun Partner sono menzionate sulla Piattaforma nelle pagine informative riguardanti il Partner, durante la procedura di prenotazione e sull'e-mail di conferma o biglietto. Per alcune tariffe o offerte speciali non sono consentite cancellazioni né modifiche. Se desideri controllare, modificare o cancellare la tua prenotazione, accedi alla tua e-mail di conferma e segui le istruzioni in essa contenute. La struttura potrebbe addebitare una penale di cancellazione in conformità con quanto previsto dalle norme di cancellazione, di pagamento anticipato e di mancata presentazione della struttura stessa, e potresti non aver diritto al risarcimento di eventuali somme già pagate (in anticipo). Prima di effettuare la prenotazione, ti raccomandiamo di leggere con attenzione le norme relative alla cancellazione, al pagamento anticipato e alla mancata presentazione previste dalla struttura, e di ricordate di effettuare puntualmente i pagamenti successivi, così come previsto dalla prenotazione specifica.
                    </p>

                    <h3>7. (Corrispondenza e comunicazioni ulteriori)</h3>
                    <p>Visitprocida.com non è responsabile nei confronti delle comunicazioni con il Partner che avvengono su o tramite la sua Piattaforma. Non è possibile avanzare diritti legati a richieste o comunicazioni dirette al Partner, o legati a qualsiasi forma di notifica di ricezione di richieste o comunicazioni. Visitprocida.com non può garantire che ogni richiesta o comunicazione sarà ricevuta, letta, presa in carico, eseguita o accettata dal Partner in maniera precisa e puntuale.<br />
                    Al fine di completare e portare a termine la prenotazione con successo è necessario fornire un indirizzo e-mail valido. Non siamo responsabili in caso di indirizzi e-mail, numeri di telefono (cellulare) o numeri di carta di credito inesistenti o errati, né abbiamo l'obbligo di verificarne l'esistenza o la correttezza.<br />
                    Eventuali reclami o richieste di risarcimento nei confronti di Visitprocida.com o relativi al Servizio dovranno essere presentati il prima possibile, e in ogni caso entro 30 giorni dopo la data in cui si completerebbe l'uso del prodotto o del servizio (per esempio la data di check-out di un soggiorno). I reclami e le richieste che pervenissero dopo il detto periodo di 30 giorni potranno essere respinti, e il richiedente dovrà rinunciare a ogni suo diritto al risarcimento (di danni o costi).</p>

                    <h3>9. Clausola di esonero da responsabilità</h3>
                    <p>Ferme restando le limitazioni e le precisazioni indicate nelle ipotesi previste dai presenti termini e condizioni e per quanto consentito dalla legge, saremo ritenuti responsabili solo per i danni diretti e immediati effettivamente subiti, pagati o a cui si è andati incontro, dovuti a una mancata osservanza dei nostri obblighi nel rispetto dei servizi che avremmo dovuto assolvere. Il risarcimento potrà arrivare fino all'ammontare complessivo del costo della tua prenotazione, come stabilito nella e-mail di conferma (o per una circostanza singola o per una serie di circostanze collegate). Sia che il Partner addebiti (o abbia già addebitato) il costo della camera, del prodotto o del servizio, sia che il pagamento (della camera o della prenotazione) venga gestito da noi, in entrambi i casi accetti e dichiari di essere a conoscenza che è sempre responsabilità del Partner riscuotere, trattenere, inviare e pagare alle autorità competenti le tasse applicabili, calcolate in base al prezzo totale della camera o della prenotazione. Visitprocida.com non può essere ritenuta responsabile (né può essere chiamata a rispondere) per la riscossione, la trattenuta, l'invio o il pagamento alle autorità competenti delle tasse applicabili al costo della camera o della prenotazione. Visitprocida.com non agisce né opera in nessun caso in qualità di venditore ufficiale di qualsiasi prodotto o servizio reso disponibile sulla Piattaforma.
                    </p>

                    <h3>10. Diritti di proprietà intellettuale</h3>
                    <p>Visitprocida.com detiene in via esclusiva tutti i diritti, titoli e interessi in materia di e nei confronti di tutti i diritti di proprietà intellettuale riguardanti l'aspetto grafico e l'interfaccia generale (inclusa l'infrastruttura) della Piattaforma tramite il quale il servizio viene reso disponibile (inclusi i giudizi degli ospiti e i contenuti tradotti). Nessuno è autorizzato a copiare, ricavare, riportare tramite link, pubblicare, promuovere, integrare, utilizzare, combinare, né in nessun altro modo utilizzare i contenuti (traduzioni e giudizi degli ospiti inclusi) o il nostro marchio in assenza di una nostra espressa approvazione scritta. Nel caso in cui un soggetto utilizzi o combini (in parte o totalmente) i nostri contenuti (incluse le traduzioni) o in altro modo prenda possesso di qualsiasi diritto di proprietà intellettuale della Piattaforma o di qualsiasi contenuto (o traduzione), esso dovrà attribuire, trasferire e cedere interamente tali diritti di proprietà intellettuale a Visitprocida.com. Qualsiasi utilizzo non conforme alla legge o una qualsiasi delle azioni o comportamenti sopra menzionati costituiscono una materiale violazione dei nostri diritti di proprietà intellettuale (inclusi i diritti di copyright e di tutela delle banche dati).
                    </p>
                </div>

            </div>
    </section>
    <section class="bottom-section gray">

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>

            </div>

        </div>

    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
