<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?> 
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2 class="sub-title">Orari traghetti da Procida a Ischia</h2>
                </div>
                <div class="col-md-8">
                    <!-- Nav tabs -->
                    <ul class="list-inline list-timetable paddingtop20" role="tablist">
                        <li role="presentation" class="active"><a href="#home" role="tab" data-toggle="tab">Procida - Ischia</a></li>
                        <li role="presentation"><a href="#profile" role="tab" data-toggle="tab">Ischia - Procida</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content paddingtop10">
                        <div>
                            <iframe id="tlines" src="http://www.traghettilines.it/affiliati/Motore.aspx?AID=231290&cat0_id=42&language=it&iframe_booking=true&bkcolor=EDEDED&txtcolor=2B2B2B&bkbtncolor=FF9900&txtbtncolor=FFFFFF&bktitolo=FF9900&txttitolo=FFFFFF&titolo=false&v2=true" style="border:0px; width: 100%;" class="tlines_frame" scrolling="no"></iframe>
                            <span id="span_tlines"></span> </div>
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="table-responsive">
                              <table class="table table-hover time-table">
                                <thead>
                                  <tr>
                                    <th>Partenza&nbsp;<i class="fa fa-unsorted"></i></th>
                                    <th>Durata&nbsp;<i class="fa fa-unsorted"></i></th>
                                    <th>Destinazione&nbsp;<i class="fa fa-unsorted"></i></th>
                                    <th class="text-center">Nave&nbsp;<i class="fa fa-unsorted"></i></th>
                                    <th>Compagnia&nbsp;<i class="fa fa-unsorted"></i></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td class="time">07.25</td>
                                    <td>30m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"><span class="fa fa-car"></span></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">09.10</td>
                                    <td>20m</td>
                                    <td>Casamicciola</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">09.30</td>
                                    <td>15m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">09.40</td>
                                    <td>30m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">10.55</td>
                                    <td>30m</td>
                                    <td>Casamicciola</td>
                                    <td class="text-center"><span class="fa fa-car"></span></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">11.50</td>
                                    <td>30m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"><span class="fa fa-car"></span></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">12.30</td>
                                    <td>20m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">13.15</td>
                                    <td>20m</td>
                                    <td>Casamicciola</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">13.55</td>
                                    <td>20m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">14.30</td>
                                    <td>25m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"><span class="fa fa-car"></span></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable2.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">13.55</td>
                                    <td>15m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">14.30</td>
                                    <td>30m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"><span class="fa fa-car"></span></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">15.30</td>
                                    <td>15m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">16.20</td>
                                    <td>30m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"><span class="fa fa-car"></span></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">17.05</td>
                                    <td>20m</td>
                                    <td>Casamicciola</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">18.35</td>
                                    <td>30m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"><span class="fa fa-car"></span></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">19.00</td>
                                    <td>15m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">19.35</td>
                                    <td>15mm</td>
                                    <td>Ischia</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">19.45</td>
                                    <td>20m</td>
                                    <td>Casamicciola</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">20.30</td>
                                    <td>30m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"><span class="fa fa-car"></span></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">23.00</td>
                                    <td>30m</td>
                                    <td>Ischia</td>
                                    <td class="text-center"><span class="fa fa-car"></span></td>
                                    <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="table-responsive">
                              <table class="table table-hover time-table">
                                <thead>
                                  <tr>
                                    <th>Partenza&nbsp;<i class="fa fa-unsorted"></i></th>
                                    <th>Durata&nbsp;<i class="fa fa-unsorted"></i></th>
                                    <th>Porto di Partenza<i class="fa fa-unsorted"></i></th>
                                    <th class="text-center">Nave&nbsp;<i class="fa fa-unsorted"></i></th>
                                    <th>Compagnia&nbsp;<i class="fa fa-unsorted"></i></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td class="time">06,25</td>
                                    <td>30m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"><span class="fa fa-car"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable2.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">06,45</td>
                                    <td>30m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"><span class="fa fa-car"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">07,10</td>
                                    <td>15m</td>
                                    <td>Casamicciola</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable3.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">07,20</td>
                                    <td>40m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"><span class="fa fa-car"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">08,20</td>
                                    <td>40m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"><span class="fa fa-car"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">09,45</td>
                                    <td>15m</td>
                                    <td>Casamicciola</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable3.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">10.15</td>
                                    <td>25m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">10.35</td>
                                    <td>30m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"><span class="fa fa-car"></span></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable2.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">11.30</td>
                                    <td>1hr</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"><span class="fa fa-car"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">12,55</td>
                                    <td>35m</td>
                                    <td>Casamicciola</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">13.00</td>
                                    <td>20m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">14.30</td>
                                    <td>25m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"><span class="fa fa-car"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">15.20</td>
                                    <td>40m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">16.25</td>
                                    <td>25m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">17.20</td>
                                    <td>40m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">17.30</td>
                                    <td>40m</td>
                                    <td>Casamicciola</td>
                                    <td class="text-center"><span class="fa fa-car"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">17.40</td>
                                    <td>20m</td>
                                    <td>Casamicciola</td>
                                    <td class="text-center"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable3.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                  <tr>
                                    <td class="time">19.25</td>
                                    <td>40m</td>
                                    <td>Ischia Porto</td>
                                    <td class="text-center"><span class="fa fa-car"></td>
                                    <td><img src="<?php echo base_url() . 'assets/images' ?>/ttable1.png" alt="..." class="img-responsive" /></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="messages">
                            <div class="table-responsive">
                                <table class="table table-hover time-table">
                                    <thead>
                                        <tr>
                                            <th>Partenza&nbsp;<i class="fa fa-unsorted"></i></th>
                                            <th>Durata&nbsp;<i class="fa fa-unsorted"></i></th>
                                            <th>Destinazione&nbsp;<i class="fa fa-unsorted"></i></th>
                                            <th class="text-center">Nave&nbsp;<i class="fa fa-unsorted"></i></th>
                                            <th>Compagnia&nbsp;<i class="fa fa-unsorted"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="time">05.00</td>
                                            <td>30m</td>
                                            <td>Ischia</td>
                                            <td class="text-center"><span class="fa fa-car"></span></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable2.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">07.25</td>
                                            <td>30m</td>
                                            <td>Ischia</td>
                                            <td class="text-center"><span class="fa fa-car"></span></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">08.25</td>
                                            <td>15m</td>
                                            <td>Ischia</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">09.10</td>
                                            <td>15m</td>
                                            <td>Casamicciola</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">9.40</td>
                                            <td>30m</td>
                                            <td>Casamicciola</td>
                                            <td class="text-center"><span class="fa fa-car"></span></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">10.55</td>
                                            <td>30m</td>
                                            <td>Casamicciola</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">13.15</td>
                                            <td>15m</td>
                                            <td>Casamicciola</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">13.55</td>
                                            <td>15m</td>
                                            <td>Ischia</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">14.30</td>
                                            <td>30m</td>
                                            <td>Ischia</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">16.20</td>
                                            <td>30m</td>
                                            <td>Ischia</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">17.05</td>
                                            <td>15m</td>
                                            <td>Casamicciola</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">18.35</td>
                                            <td>30m</td>
                                            <td>Ischia</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">19.00</td>
                                            <td>15m</td>
                                            <td>Ischia</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">19.45</td>
                                            <td>15mm</td>
                                            <td>Casamicciola</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                        <tr>
                                            <td class="time">20.30</td>
                                            <td>30m</td>
                                            <td>Ischia</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="settings">
                            <div class="table-responsive">
                                <table class="table table-hover time-table">
                                    <thead>
                                        <tr>
                                            <th>Departure&nbsp;<i class="fa fa-unsorted"></i> </th>
                                            <th>Time&nbsp;<i class="fa fa-unsorted"></i> </th>
                                            <th>Destination&nbsp;<i class="fa fa-unsorted"></i> </th>
                                            <th class="text-center">Ship&nbsp;<i class="fa fa-unsorted"></i> </th>
                                            <th>Company&nbsp;<i class="fa fa-unsorted"></i> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="time">15.12</td>
                                            <td>1hr 10m</td>
                                            <td>Napoli</td>
                                            <td class="text-center"><span class="fa fa-car"></span></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive"></td>
                                        </tr>
                                        <tr>
                                            <td class="time">17.30</td>
                                            <td>2hr 10m</td>
                                            <td>Napoli Beverello</td>
                                            <td class="text-center"><span class="fa fa-car"></span></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable2.png" alt="..." class="img-responsive"></td>
                                        </tr>
                                        <tr>
                                            <td class="time">19.25</td>
                                            <td>1hr 30m</td>
                                            <td>Ischia Porto</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable3.png" alt="..." class="img-responsive"></td>
                                        </tr>
                                        <tr>
                                            <td class="time">15.10</td>
                                            <td>1hr 10m</td>
                                            <td>Napoli</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive"></td>
                                        </tr>
                                        <tr>
                                            <td class="time">17.30</td>
                                            <td>2hr 10m</td>
                                            <td>Napoli Beverello</td>
                                            <td class="text-center"><span class="fa fa-car"></span></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive"></td>
                                        </tr>
                                        <tr>
                                            <td class="time">19.25</td>
                                            <td>1hr 30m</td>
                                            <td>Ischia Porto</td>
                                            <td class="text-center"></td>
                                            <td><img src="<?php echo base_url(); ?>assets/images/ttable1.png" alt="..." class="img-responsive"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--  <div class="table-responsive">
                          <table class="table table-hover time-table">
                              <thead>
                                  <tr>
                                      <th>Departure&nbsp;<i class="fa fa-unsorted"></i>
                                      </th>
                                      <th>Time&nbsp;<i class="fa fa-unsorted"></i>
                                      </th>
                                      <th>Destination&nbsp;<i class="fa fa-unsorted"></i>
                                      </th>
                                      <th class="text-center">Ship&nbsp;<i class="fa fa-unsorted"></i>
                                      </th>
                                      <th>Company&nbsp;<i class="fa fa-unsorted"></i>
                                      </th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td class="time">15.10</td>
                                      <td>1hr 10m</td>
                                      <td>Napoli</td>
                                      <td class="text-center">
                                          <span class="fa fa-car"></span>
                                      </td>
                                      <td>
                                          <img src="images/ttable1.png" alt="..." class="img-responsive">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="time">17.30</td>
                                      <td>2hr 10m</td>
                                      <td>Napoli Beverello</td>
                                      <td class="text-center">
                                          <span class="fa fa-car"></span>
                                      </td>

                                      <td>
                                          <img src="images/ttable2.png" alt="..." class="img-responsive">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="time">19.25</td>
                                      <td>1hr 30m</td>
                                      <td>Ischia Porto</td>
                                      <td class="text-center"></td>

                                      <td>
                                          <img src="images/ttable3.png" alt="..." class="img-responsive">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="time">15.10</td>
                                      <td>1hr 10m</td>
                                      <td>Napoli</td>
                                      <td class="text-center"></td>

                                      <td>
                                          <img src="images/ttable1.png" alt="..." class="img-responsive">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="time">17.30</td>
                                      <td>2hr 10m</td>
                                      <td>Napoli Beverello</td>
                                      <td class="text-center">
                                          <span class="fa fa-car"></span>
                                      </td>

                                      <td>
                                          <img src="images/ttable1.png" alt="..." class="img-responsive">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class="time">19.25</td>
                                      <td>1hr 30m</td>
                                      <td>Ischia Porto</td>
                                      <td class="text-center"></td>

                                      <td>
                                          <img src="images/ttable1.png" alt="..." class="img-responsive">
                                      </td>
                                  </tr>

                              </tbody>
                          </table>
                      </div> -->
                </div>
                <div class="col-md-4">
                    <div class="row">

                        <div class="col-md-12">

                            <div class="sidebar-element">
                                <?php $add = $this->Ads_model->get('300X250'); ?>
                                <?php if ($add): ?>
                                    <?php if ($add->src != ''): ?>
                                        <?php if ($add->type == 'image'): ?>
                                            <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                        <?php else: ?>
                                            <?php echo base64_decode($add->src); ?>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                                <?php endif; ?>
                            </div>

                        </div>
                        <div class="col-md-12">
                           <div class="sidebar-element">
                                <h4 class="sidebar-title">Tutte le tratte</h4>
                                <ul class="list-unstyled sidebar-website">
                                    <li><a href="traghetti-procida-napoli">Procida - Napoli</a><i class="fa fa-angle-right"></i></li>
                                    <li><a href="traghetti-procida-pozzuoli">Procida - Pozzuoli</a><i class="fa fa-angle-right"></i></li>
                                    <li><a href="traghetti-procida-ischia">Procida - Ischia</a><i class="fa fa-angle-right"></i></li>
                                    <li><a href="traghetti-napoli-procida">Napoli - Procida</a><i class="fa fa-angle-right"></i></li>
                                    <li><a href="traghetti-pozzuoli-procida">Pozzuoli - Procida</a><i class="fa fa-angle-right"></i></li>
                                    <li><a href="traghetti-ischia-procida">Ischia - Procida</a><i class="fa fa-angle-right"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    </section>
    <section class="bottom-section gray">

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>

            </div>

        </div>

    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>