<?php /*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */ ?>

<div class="col-md-12 visible-lg">
    <div class="page-title-search">
    
        <form action="<?php echo base_url(); ?><?php echo $this->lang->lang(); ?>/HotelBB/search" method="post" name="formhotel">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                    <div class="input-group">
                        <input id="bs_checkin_date" type="text" required class="form-control input-sm datepicker" name="datastart" placeholder="Check In">
                        <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" id="bs_checkout_date" class="form-control input-sm datepicker" name="dataend" placeholder="Check Out">
                            <div class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="col-md-3 col-xs-6">
                    <div class="form-group">
                        <input type="hidden" id="jpmob" name="jsonparty" value='' />
                        <select class="form-control input-sm" id="bs_mobile_rooms" required="">
                            <option value="0" disabled="disabled" selected="selected"><?php echo lang("Number_of_People"); ?></option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 noPaddingLeft col-xs-6">
                    <button type="submit" class="btn btn-purity btn-sm btn-block btn-search"><?php echo lang('Search') ?></a>
                </div>
            </div>
            
        </form>
    </div>
</div>
<script>
$("#bs_mobile_rooms").change(function(){
  var r = document.getElementById("bs_mobile_rooms");
  var people = r.options[r.selectedIndex].value;
  var jparty = '[{"adults": '+people+'}]';
  $('#jpmob').val(jparty);
});
</script>
