<!DOCTYPE html>
<html lang="<?php echo $this->lang->lang(); ?>">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title><?php echo isset($meta_title) ? $meta_title : ''; ?></title>
    <meta name="keywords" content="<?php echo isset($meta_keywords) ? $meta_keywords : ''; ?>">
    <meta name="description" content="<?php echo isset($meta_desc) ? $meta_desc : ''; ?>">
    <meta name="author" content="cabo360s">
    <meta name="geo.region" content="MX-BCS" />
    <meta name="geo.placename" content="Cabo San Lucas" />
    <meta name="geo.position" content="22.893888;-109.92006" />
    <meta name="ICBM" content="22.893888, -109.92006" />

    <meta name="google-site-verification" content="K0EA3MP6cABZhjYJaStH6gtID90wmg8fyCK2GJ-8vqA" />
    <link rel="alternate" href="https://www.cabo360s.com/booking/en" hreflang="en-gb" />
    <link rel="alternate" href="https://www.cabo360s.com/booking/it" hreflang="it-it" />

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!--Le HTML5 shim, for IE6-8 support of HTML5 elements-->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/subset.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/slidebox.css" rel="stylesheet" type="text/css" />
    

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" type="text/css" rel="stylesheet">
    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png">
    <style>
      .datepicker-dropdown{
        background-color: white;
      }
    </style>
      <!-- Begin Inspectlet Embed Code -->
      <script type="text/javascript" id="inspectletjs"> window.__insp = window.__insp || []; __insp.push(['wid', 407163576]); (function() { function ldinsp(){if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); }; setTimeout(ldinsp, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp(); })(); </script> <!-- End Inspectlet Embed Code -->

       <!--Start of Zopim Live Chat Script-->
      <!--<script type="text/javascript">
      window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
      d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
      _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
      $.src="//v2.zopim.com/?4A3BAVoW59bWrT7sThUIUqecEvQNmC8g";z.t=+new Date;$.
      type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
      </script>-->
      <!--End of Zopim Live Chat Script-->

	<!-- Facebook Pixel Code -->
	<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1813078395685172'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=1813078395685172&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

      <?php if (isset($cssfiles)): ?>
          <?php foreach ($cssfiles as $css): ?>
              <link href="<?php echo $css['src'] ?>" rel="stylesheet" type="text/css" />
          <?php endforeach; ?>
      <?php endif; ?>
      <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
      <script>
        $('#ferryFrame').height( $('#ferryFrame').contents().outerHeight() );
      </script>
      <!-- michele <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBh_xpi9WjiKkyQgx_OYW_Hbijfhj-WBSs&v=3"></script> -->
      
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvNvorlsXyt0Q-1bhMGiEsWlj7i75sfQU&callback=initMap" type="text/javascript"></script>
      <script type="text/javascript">
        var base_url = '<?= base_url() ?>'
      </script>


      
		<?php if(isset($map)){ echo $map['js']; } ?>
    </head>
    <body onload="document.body.style.opacity='1'">
        <?php if (isset($facebook_sdk)): ?>
            <?php echo $facebook_sdk; ?>
        <?php endif; ?>
        <div class="navbar-wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top navbar-purity  megamenu" role="navigation" id="navigation">
                <div class="container">
                    <div class="navbar-header">
                    	<div class="col-xs-12 mobileMenu">
	                    	<div class="col-15 visible-xs">
	                    		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar">
	                    		<span class="sr-only">Toggle navigation</span>
	                    		    <span class="icon-bar"></span>
	                    		    <span class="icon-bar"></span>
	                    		    <span class="icon-bar"></span>
	                    		</button>
	                    	</div>
	                    	<div class="col-70 border-lr-white">
	                    		<a class="navbar-brand" href="<?php echo base_url() . $this->lang->lang(); ?>">
	                    		    <img src="<?php echo base_url(); ?>assets/images/logo.png" id="logokhan" alt="...">
	                    		</a>
	                    	</div>
	                    	<div class="col-15">
	                    		<div class="hidden-xs visible-sm visible-md">
		                    		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar">
		                    		<span class="sr-only">Toggle navigation</span>
		                    		    <span class="icon-bar"></span>
		                    		    <span class="icon-bar"></span>
		                    		    <span class="icon-bar"></span>
		                    		</button>
	                    		</div>
	                    		<li class="drop-down dropdown">
	                    		    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><p><?php echo $this->lang->lang(); ?></p><b class="caret"></b></a>
	                    		    <ul class="dropdown-menu">
	                    		        <li><a href="<?= base_url() . 'it' ?>">&nbsp;&nbsp;Italiano</a></li>
	                    		        <li><a href="<?= base_url() . 'en' ?>">&nbsp;&nbsp;English</a></li>
	                    		    </ul>
	                    		</li>
	                    		<li class="drop-down dropdown hidden-xs visible-sm visible-md">
	                    		    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url(); ?>assets/images/<?php echo $this->lang->lang() == 'it' ? "flag-1.png" : ($this->lang->lang() == 'en' ? "flag-3.png" : ($this->lang->lang() == 'du' ? "flag-2.png" : "")); ?>" class="menu-flag marginTopMenu" alt=""><b class="caret"></b></a>
	                    		    <ul class="dropdown-menu">
	                    		        <li><a href="<?= base_url() . 'it/transfer' ?>"><img src="<?php echo base_url(); ?>assets/images/flag-1.png" class="menu-flag" alt="">&nbsp;&nbsp;Italiano</a></li>
	                    		        <li><a href="<?= base_url() . 'en/transfer' ?>"><img src="<?php echo base_url(); ?>assets/images/flag-3.png" class="menu-flag" alt="">&nbsp;&nbsp;English</a></li>
	                    		    </ul>
	                    		</li>
	                    	</div>
                    	</div>
                    </div>
                    <div class="collapse navbar-collapse" id="main-navbar">

                        <!--MENU PRINCIPALE-->
                        <ul class="nav navbar-nav page navbar-right " id="menu">

                           <!--PRIMA VOCE-->
     <!-- For hide menu -->
    <?php if('hide' != 'hide') : ?>
                            <li class="drop-down dropdown ">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/pointerico.png"><?php echo lang("What_to_visit"); ?> <b class="caret"></b><b class="arrow"><i class="fa fa-angle-right fa-3" aria-hidden="true"></i></b></a>
                                <ul class="dropdown-menu">
                                    <li class="megamenu-content">
                                        <div class="row">
                                            <ul class="col-sm-4 list-unstyled nopadding ">
                                                
                                                <li class="dropdown-header "><a href="<?php echo base_url().$this->lang->lang().'/'.lang('locations') ?>"><?php echo lang("Locations"); ?> </a></li>
                                                <?php if (isset($locations) && !empty($locations) && is_array($locations)): ?>
                                                    <?php foreach ($locations as $location): ?>
                                                        <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('what-to-visit') . '/' . urlencode(str_replace(' ', '-', strtolower($location->title))); ?>"><?php echo $location->title; ?></a></li>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                            <ul class="col-sm-4 list-unstyled nopadding">
                                                <!-- <a href="<?php // echo base_url().$this->lang->lang().'/'.lang('places-of-interest').'.html' ?>"> -->
                                                <li class="dropdown-header"><a href="<?php echo base_url().$this->lang->lang().'/'.lang('places-of-interest') ?>"><?php echo lang("Places_of_Interest"); ?> </a></li>
                                                <?php if (isset($m_poi) && !empty($m_poi)): ?>
                                                    <?php foreach ($m_poi as $poi): ?>
                                                        <?php $poi_array = (array) $poi; ?>
                                                        <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . str_replace(' ', '-', strtolower(lang('places-of-interest'))) . '/' . str_replace(' ', '-', strtolower($poi_array["rewrite_url_" . $this->lang->lang()])); ?>"><?php echo$poi_array[$this->lang->lang() . "Title"]; ?></a></li>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                            <ul class="col-sm-4 list-unstyled nopadding">
                                                <li class="dropdown-header"><a href="<?php echo base_url().$this->lang->lang().'/'.lang('beaches') ?>"><?php echo lang('Beaches'); ?>  </a></li>
                                                <?php if (isset($m_beaches) && !empty($m_beaches)): ?>
                                                    <?php foreach ($m_beaches as $beach): ?>
                                                        <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('beach') . '/' . str_replace(' ', '-', strtolower($beach->title)); ?>"><?php echo $beach->title; ?></a></li>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                              <!--SECONDA VOCE-->

                            <?php if($this->lang->lang()=="it"){ ?>
                            <li class="drop-down dropdown">
                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/restaurantsico.png">Cosa Fare<b class="caret"></b> <b class="arrow"><i class="fa fa-angle-right fa-3" aria-hidden="true"></i></b></a>
                                <ul class="dropdown-menu">                                                          
                                            <li><a href="<?php echo base_url(); ?>/it/mangiare-e-bere/restaurants">Ristoranti</a></li>
                                            <li><a href="<?php echo base_url(); ?>/it/mangiare-e-bere/bars-and-cafe">Bar e caffè</a></li>
                                             <li><a href="<?php echo base_url(); ?>/it/mangiare-e-bere/nightlife">Vita notturna</a></li>                                           
                                               <li><a href="<?php echo base_url(); ?>/it/eventi/traditional-events">Manifestazioni tradizionali</a></li>
                                                 <li><a href="<?php echo base_url(); ?>/it/cosa-fare/shopping">Shopping</a></li>
                                            <li><a href="<?php echo base_url(); ?>/it/cosa-fare/rentals">Noleggio</a></li>
                                             <li><a href="<?php echo base_url(); ?>/it/cosa-fare/beach-clubs">Stabilimenti balneari</a></li>
                                               <li><a href="<?php echo base_url(); ?>/it/cosa-fare/wedding">Matrimonio</a></li>
                                            <li><a href="<?php echo base_url(); ?>/it/cosa-fare/escursions-and-visit">Escursioni e visite</a></li>
                                             <li><a href="<?php echo base_url(); ?>/it/cosa-fare/agencies">Agenzie</a></li>
                                         </ul>


                            </li>
                            <?php }elseif($this->lang->lang()=="en"){?>

                               <li class="drop-down dropdown">
                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/restaurantsico.png">What to do<b class="caret"></b> <b class="arrow"><i class="fa fa-angle-right fa-3" aria-hidden="true"></i></b></a>
                                <ul class="dropdown-menu">                                                          
                                            <li><a href="<?php echo base_url(); ?>/en/eat-and-drink/restaurants">Restaurants</a></li> 
                                            <li><a href="<?php echo base_url(); ?>/en/eat-and-drink/bars-and-cafe">Bars and Cafe</a></li>
                                             <li><a href="<?php echo base_url(); ?>/en/eat-and-drink/nightlife">Nightlife</a></li>
                                             <li><a href="<?php echo base_url(); ?>/en/events/traditional-events">Traditional Events</a></li>
                                             <li><a href="<?php echo base_url(); ?>/en/activities/shopping">Shopping</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/activities/rentals">Rentals</a></li>
                                             <li><a href="<?php echo base_url(); ?>/en/activities/beach-clubs">Beach Clubs</a></li>
                                               <li><a href="<?php echo base_url(); ?>/en/activities/wedding">Wedding</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/activities/escursions-and-visit">Escursions And Visit</a></li>
                                             <li><a href="<?php echo base_url(); ?>/en/activities/agencies">Agencies</a></li>
                                         </ul>
                            </li>



                           <?php } ?>



                            <!--TERZA VOCE-->

                            <?php if($this->lang->lang()=="it"){ ?>
                            <li class="drop-down dropdown">
                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/hotelsico.png">Dove dormire <b class="caret"></b><b class="arrow"><i class="fa fa-angle-down fa-3" aria-hidden="true"></i></b></a>
                                <ul class="dropdown-menu">                                                          
                                            <li><a href="<?php echo base_url(); ?>/it/hotels" alt="hotels" id="hotels">Hotels</a></li>
                                            <li><a href="<?php echo base_url(); ?>/it/dove-dormire/case-vacanza" alt="case-vacanza" id="holiday-houses">Ville</a></li>
                                             <li><a href="<?php echo base_url(); ?>/it/dove-dormire/bed-and-breakfast" alt="bed-and-breakfast" id="bed-and-breakfast">Bed and breakfast</a></li>
                                         </ul>
                            </li>
                            <?php }elseif($this->lang->lang()=="en"){?>

                               <li class="drop-down dropdown">
                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/hotelsico.png">Where to stay <b class="caret"></b><b class="arrow"><i class="fa fa-angle-down fa-3" aria-hidden="true"></i></b></a>
                                <ul class="dropdown-menu">                                                          
                                            <li><a href="<?php echo base_url(); ?>/en/hotels" alt="hotels" id="hotels">Hotels</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/where-to-stay/holiday-houses" alt="holiday-houses" id="holiday-houses">Villas</a></li>
                                             <li><a href="<?php echo base_url(); ?>/en/where-to-stay/bed-and-breakfast" alt="bed-and-breakfast" id="bed-and-breakfast">Bed and breakfast</a></li>
                                         </ul>
                            </li>



                           <?php } ?>


                          
                  
                            <?php if($this->lang->lang()=="it"){ ?>
                            <li class="drop-down dropdown">
                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/ferriesico.png">Come arrivare <b class="caret"></b><b class="arrow"><i class="fa fa-angle-down fa-3" aria-hidden="true"></i></b></a>
                                <ul class="dropdown-menu">                                                          
                                            <li><a href="<?php echo base_url(); ?>/it/info-utili/how-to-get-there">Info Utili</a></li>
                                            <li><a href="<?php echo base_url(); ?>/it/orari-traghetti">Orari traghetti</a></li>
                                             <li><a href="<?php echo base_url(); ?>/it/info-utili/transfer">Trasferimenti</a></li>
                                         </ul>
                            </li>
                            <?php }elseif($this->lang->lang()=="en"){?>

                               <li class="drop-down dropdown">
                                
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/ferriesico.png">How To Get There<b class="caret"></b><b class="arrow"><i class="fa fa-angle-down fa-3" aria-hidden="true"></i></b></a>
                                <ul class="dropdown-menu">                                                          
                                            <li><a href="<?php echo base_url(); ?>en/useful-info/how-to-get-there">Useful Info</a></li>
                                            <li><a href="<?php echo base_url(); ?>en/useful-info/ferry-time-table">Ferry time table</a></li>
                                             <li><a href="<?php echo base_url(); ?>en/useful-info/transfer">Transfer</a></li>
                                         </ul>
                            </li>



                           <?php } ?>

 <!-- For hide menu -->			                  
<?php endif; ?>
          
                 
                
                			 <!--TOURS-->
                            <li class=""> <a href="<?php echo base_url() . $this->lang->lang() . '/tours' ?>"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/toursico.png">
                                <?php if($this->lang->lang()=="it"){echo "Esperienze";}elseif($this->lang->lang()=="en"){echo "Experiences";} ?>
                            </a></li>
                       		<!-- Transfer -->
                          <li class=""> <a href="<?php echo base_url() . $this->lang->lang() . '/transfer' ?>"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/toursico.png">
                                <?php if($this->lang->lang()=="it"){echo "Transfer Privati";}elseif($this->lang->lang()=="en"){echo "Private Transfers";} ?>
                            </a></li>
 <!-- For hide menu -->
  <?php if('hide' != 'hide') : ?>
                   			<li class=""> <a href="<?php echo base_url() . $this->lang->lang() . '/useful-info/transfer' ?>"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/toursico.png">
                                <?php if($this->lang->lang()=="it"){echo "Accommodations";}elseif($this->lang->lang()=="en"){echo "Book Transfers";} ?>
                            </a></li>
 <!-- For hide menu -->                       
<?php endif; ?>
                            <!-- ferry -->
                            <li class=""> <a href="<?php echo base_url() . $this->lang->lang() . '/villas' ?>"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/toursico.png">
                                <?php if($this->lang->lang()=="it"){echo "Ville esclusive";}elseif($this->lang->lang()=="en"){echo "Exclusive Villas";} ?>
                            </a></li>
                    
                            <!--<li class="hidden-xs hidden-sm hidden-md"> <a href="<?php echo base_url() . $this->lang->lang() . '/blog' ?>.html"><?php echo lang('Blog'); ?></a></li>-->

                             

                            <li class="drop-down">
                                <a href="#" class="hidden-xs hidden-sm hidden-md dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i></a>
                                <ul class="dropdown-menu">
								     <?php if ($this->session->userdata('usertype') == 'admin'): ?>
									 <li>
                                        <a href="<?php echo base_url()."admin/logout" ?>">Logout</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url()."admin/dashboard" ?>"><?php echo lang('settings'); ?></a>
                                    </li>
									 <?php elseif($this->session->userdata('id')): ?>
                                      <li>
                                        <a href="<?php echo base_url().$this->lang->lang()."/customer/login" ?>">Logout</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url().$this->lang->lang()."/customer/dashboard" ?>"><?php echo lang('settings'); ?></a>
                                    </li>
									<?php else: ?>
									<li>
                                        <a href="<?php echo base_url().$this->lang->lang()."/customer/login" ?>">Login</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url().$this->lang->lang()."/customer/signup" ?>"><?php echo lang('signup'); ?></a>
                                    </li>
									<?php endif; ?>
                                </ul>
                            </li>
                            <li class="drop-down dropdown hidden-xs hidden-sm hidden-md">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url(); ?>assets/images/<?php echo $this->lang->lang() == 'it' ? "flag-1.png" : ($this->lang->lang() == 'en' ? "flag-3.png" : ($this->lang->lang() == 'du' ? "flag-2.png" : "")); ?>" class="menu-flag" alt=""></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= base_url() . 'it/transfer' ?>"><img src="<?php echo base_url(); ?>assets/images/flag-1.png" class="menu-flag" alt="">&nbsp;&nbsp;Italian</a></li>
                                    <li><a href="<?= base_url() . 'en/transfer' ?>"><img src="<?php echo base_url(); ?>assets/images/flag-3.png" class="menu-flag" alt="">&nbsp;&nbsp;English</a></li>
                                </ul>
                            </li>





                            <li class="drop-down dropdown hidden-lg">
                            	<a href="<?php echo base_url('Cart/index'); ?>" class="dropdown-toggle"><img class="icomenu" src="<?php echo base_url(); ?>assets/images/carrelloico.png"><?php echo lang('cart'); ?> <b class="arrow"><i class="fa fa-angle-right fa-3" aria-hidden="true"></i></b></a>
                            </li>
                            <li class="drop-down dropdown hidden-xs hidden-sm hidden-md">
	                          <div class="dropdown dropdown-cart">
	                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-basket-1"></i><?php echo lang('cart'); ?> <span id="cart_count">(<?php echo $cart['items']; ?>)</span> </a>
	                            <ul class="dropdown-menu" id="cart_items">
	                              <?php foreach($cart['hotels'] as $hotels) { ?>
	                                <li id="<?php echo $hotels->id; ?>">
	                                    <div class="image"><img src="<?php echo $hotels->roomImage; ?>" alt="image"></div>
										                   <strong><a><?php echo $hotels->hotelName; ?></a> <?php echo $hotels->serviceQuantity; ?> x € <?php echo $hotels->servicePrice; ?></strong>
	                                    <a onclick="removeFromCart('<?php echo $hotels->id; ?>')" class="action"><i class="fa fa-trash"></i></a>
	                                </li>
	                              <?php } ?>
	                              <?php foreach($cart['transfer'] as $transfer) { ?>
	                                <li id="<?php echo $transfer->id; ?>">
	                                    <div class="image"><img src="<?php echo base_url('assets/images/1468417512transfer.jpg'); ?>" alt="image"></div>
	                                    <strong><a><?php echo $transfer->serviceQuantity; ?> <?php echo $transfer->serviceType; ?> to <?php echo $transfer->dropoffAddress; ?></a></strong>
	                                    <a onclick="removeFromCart('<?php echo $transfer->id; ?>')" class="action"><i class="fa fa-trash"></i></a>
	                                </li>
	                               <?php } ?>
	                               <?php foreach($cart['tours'] as $tour) { ?>
	                                <li id="<?php echo $tour->info->id; ?>">
	                                    <div class="image"><img src="<?php echo base_url('uploads/tours/').$tour->info->thumbnail; ?>" alt="image"></div>
	                                    <strong><a><?php if($this->lang->lang() == 'it'){ echo $tour->info->itTitle; } else { echo $tour->info->enTitle; } ?></a> <?php echo $tour->serviceQuantity; ?> x € <?php echo $tour->servicePrice; ?> </strong>
	                                    <a onclick="removeFromCart('<?php echo $tour->id; ?>')" class="action"><i class="fa fa-trash"></i></a>
	                                </li>
	                                <?php } ?>
	                                <li>
                                   
	                                    <a href="<?php echo base_url(); ?>Cart/index" class="button_drop"><?php echo lang('go_cart'); ?></a>
	                                </li>
                             
	                            </ul>
	                          </div><!-- End dropdown-cart-->
	                        </li>



                        
                        </ul>

                    </div>
                    <div class="collapse navbar-collapse hidden-lg" id="search-navbar">
                        <form class="form" role="form">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6 col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input id="bs_checkin_date_menu" type="text" class="form-control input-sm datepicker" name="check_in" placeholder="Check In">
                                            <div class="input-group-addon">
                                                <span class="oi oi-calendar"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="bs_checkout_date_menu" class="form-control input-sm datepicker" name="check_out" placeholder="Check Out">
                                            <div class="input-group-addon">
                                                <span class="oi oi-calendar"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-6 col-md-6">
                                    <div class="form-group">
                                        <select class="form-control input-sm" id="bs_rooms_menu" required="">
                                            <option value="0" disabled="disabled" selected="selected"><?php echo lang('num_room'); ?></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 noPaddingLeft col-xs-6 col-sm-4">
                                    <a href="#" onclick="booking_search_menu()" class="btn btn-purity btn-sm btn-block btn-search"><?php echo lang('Search') ?></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
        </div>
