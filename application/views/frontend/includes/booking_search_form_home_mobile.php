<section id="mobilesearch" class="hidden-lg">
          <!-- mobile search bar-->
		  <div class="search-bar-home-mobile no-padding hiddenAfterTablet fullWidth">
		  	<div class="container-fluid no-padding ">
		  		<div class="tab-content">
		  		  <div id="home" class="tab-pane fade in active">
								<form action="<?php echo base_url(); ?><?php echo $this->lang->lang(); ?>/HotelBB/search" method="post" name="formhotel">
		  		          <div class="row">
		  		          	  <!-- <div class="col-sm-12 hidden-xs hidden-lg">
		  		          	    	<div class="pad15">
		  		          	    		<select class="form-control " id="bs_accomodation" required="">
		  		              		      <option value="0" disabled="disabled" selected="selected"><?php echo lang('accomodation_types'); ?></option>
		  		          	    		  	<option value="3D216">Affitta camere</option>
																	<option value="3D220">Case vacanze</option>
													        <option value="3D208">B&amp;B</option>
		  		          	    		    <option value="3D204">Hotel</option>
		  		          	    		  	<option value="3D201">Appartamenti</option>
		  		          	    		</select>
		  		          	    	</div>
		  		          	    </div> -->
		  		              <div class="col-xs-12 col-sm-6">
		  		                  <div class="input-group pad15">
		  		                      <input id="bs_mobile_checkin_date" type="text" required class="form-control datepicker" name="datastart" placeholder=" <?php echo lang('Check-In'); ?>">
		  		                      <div id="mobDataStart" class="input-group-addon">
		  		                          <span class="fa fa-calendar"></span>
		  		                      </div>
		  		                  </div>
		  		              </div>
		  		              <div class="col-xs-12 col-sm-6">
		  		                      <div class="input-group pad15">
		  		                          <input type="text" id="bs_mobile_checkout_date" required class="form-control  datepicker" name="dataend" placeholder=" <?php echo lang('Check-Out'); ?>">
		  		                          <div id="mobDataEnd" class="input-group-addon">
		  		                              <span class="fa fa-calendar"></span>
		  		                          </div>
		  		                      </div>
		  		              </div>
		  		              <div class="col-xs-6 col-sm-6">
		  		              	<div class="pad15">
														<input type="hidden" id="jpmob" name="jsonparty" value='' />
		  		              		<select class="form-control" name="n_people" id="bs_mobile_rooms" required="">
																	<option value="0" disabled="disabled" selected="selected"><?php echo lang("Number_of_People"); ?></option>
		  		              		      <option value="1">1</option>
		  		              		      <option value="2">2</option>
		  		              		      <option value="3">3</option>
		  		              		      <option value="4">4</option>
		  		              		      <option value="5">5</option>
		  		              		  </select>
		  		              	</div>
		  		              </div>
		  		              <div class="col-xs-6 col-sm-6">
		  		              	<div class="pad15">
		  		              		<button type="submit" class="btn btn-purity btn-block "><?php echo lang('Search');  ?></button>
		  		              	</div>
		  		              </div>
		  		          </div>
		  		      </form>
		  		  </div>

		  		</div>
		  		
		  	</div>
		  </div>
	</section>
<script>

$('#ferryBtn').on('click', function() {
	$('.tab-content').css('background-color', '#f9f9f9');
});

$('#bookBtn').on('click', function() {
	$('.tab-content').css('background-color', '#f9f9f9');
});


$('#bookingFerry').on('click',function() {
	var lang = '<?php echo $this->lang->lang(); ?>';
	var date = $('#bookingDate').val();
	var pars = date.split("/");
	var check_day = parseInt(pars[1], 10);
	var check_month = parseInt(pars[0], 10);
			if(check_month < 10)
				check_month = '0'+check_month;
	var check_year = parseInt(pars[2], 10);
	date = check_year+'-'+check_month+'-'+check_day;
	var route = $('#traghettilineSearch option:selected').val();
	var url = '<?php echo base_url().$this->lang->lang(); ?>/info-utili/ferry-time-table?route='+route+'&date='+date;
	window.location = url;
});

var fsf = document.getElementById('ferrySearch');
var hsf= document.getElementById('hotelSearch');
fsf.style.display = "none";


function showHideHotel(el){
  fsf.style.display = "none";
  hsf.style.display = "block";
  el.classList.add('active');
  var el2 = el.nextSibling.nextElementSibling;
  el2.classList.remove('active');
}

function showHideFerry(el){
  hsf.style.display = "none";
  fsf.style.display = "block";
  el.classList.add('active');
  var el3 = el.previousSibling.previousElementSibling;
  el3.classList.remove('active');
}

function isValidDate(dateString){
    // First check for the pattern
    if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[1], 10);
    var month = parseInt(parts[0], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month == 0 || month > 12)
        return false;

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
  }

	$("#bs_mobile_rooms").change(function(){
		var r = document.getElementById("bs_mobile_rooms");
		var people = r.options[r.selectedIndex].value;
		var jparty = '[{"adults": '+people+'}]';
		$('#jpmob').val(jparty);
	});
	
	 
	$('#mobDataStart').on('click', function() {
		$('#mobilesearch input[name=datastart]').datepicker('show');
	});
	
	$('#mobDataEnd').on('click', function() {
		$('#mobilesearch input[name=dataend]').datepicker('show');
	});
</script>
