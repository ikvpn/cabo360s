<!--################ FOOTER START ################-->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <ul class="footer-list list-unstyled">
                    <li class="heading"><?php echo lang("What_to_visit"); ?></li>
                    <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('locations') . '' ?>"><?php echo lang('Locations') ?></a></li>
                    <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('places-of-interest') . '' ?>"><?php echo lang("Places_of_Interest") ?></a></li>
                    <li><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('beach') . '' ?>"><?php echo lang("Beaches"); ?></a></li>
                </ul>
                <ul class="footer-list list-unstyled paddingtop10">
                    <li class="heading"><?php echo lang('Where_to_stay'); ?></li>
                    <li><a href="<?php echo base_url(); ?>/en/villa/Casacorbezzolo">Villa Penasco</a></li>
                     <li><a href="<?php echo base_url(); ?>/en/villa/LaDatcha">La Datcha</a></li>
                </ul>
            </div>
            <div class="col-md-2">        
                <ul class="footer-list list-unstyled ">
                      <?php if($this->lang->lang()=="it"){ ?>
                    <li class="heading">Cosa Fare</li>
                                          <li><a href="<?php echo base_url(); ?>/it/mangiare-e-bere/restaurants">Ristoranti</a></li>
                                          <li><a href="<?php echo base_url(); ?>/it/mangiare-e-bere/bars-and-cafe">Bar e caffè</a></li>
                                          <li><a href="<?php echo base_url(); ?>/it/mangiare-e-bere/nightlife">Vita notturna</a></li>                                           
                                          <li><a href="<?php echo base_url(); ?>/it/eventi/traditional-events">Manifestazioni tradizionali</a></li>
                                          <li><a href="<?php echo base_url(); ?>/it/cosa-fare/shopping">Shopping</a></li>
                                          <li><a href="<?php echo base_url(); ?>/it/cosa-fare/rentals">Noleggio</a></li>
                                          <li><a href="<?php echo base_url(); ?>/it/cosa-fare/beach-clubs">Stabilimenti balneari</a></li>
                                          <li><a href="<?php echo base_url(); ?>/it/cosa-fare/wedding">Matrimonio</a></li>
                                          <li><a href="<?php echo base_url(); ?>/it/cosa-fare/escursions-and-visit">Escursioni e visite</a></li>
                                          <li><a href="<?php echo base_url(); ?>/it/cosa-fare/agencies">Agenzie</a></li>
                    <?php   }elseif ( $this->lang->lang()=="en"  ) { ?>
                     <li class="heading">What to do</li>
                                            <li><a href="<?php echo base_url(); ?>/en/eat-and-drink/restaurants">Restaurants</a></li> 
                                            <li><a href="<?php echo base_url(); ?>/en/eat-and-drink/bars-and-cafe">Bars and Cafe</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/eat-and-drink/nightlife">Nightlife</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/events/traditional-events">Traditional Events</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/activities/shopping">Shopping</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/activities/rentals">Rentals</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/activities/beach-clubs">Beach Clubs</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/activities/wedding">Wedding</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/activities/escursions-and-visit">Escursions And Visit</a></li>
                                            <li><a href="<?php echo base_url(); ?>/en/activities/agencies">Agencies</a></li>
                  <?php  } ?>
                </ul>

            </div>
            <div class="col-md-2">
             <ul class="footer-list list-unstyled">
                    <li class="heading"><?php echo lang("About-us");?></li>
                    <!-- <li><a href="#"><?php echo lang('Our-Team');?></a></li> -->
                    <li><a href="<?php echo base_url() . "customer/login" ?>"><?php echo lang("Add-your-business");?></a></li>
                    <!-- <li><a href="#"><?php echo lang("Prices");?></a></li> -->
                </ul>
                <ul class="footer-list list-unstyled paddingtop10">
                    <li class="heading"><?php echo lang('Articles'); ?></li>
                    <?php if(isset($articles) && count($articles)>0): ?>
                    <?php foreach($articles as $post): ?>
                      <!--<li><a href="<?php echo base_url() . 'blog/post/' . $post->id.'/'.date('Y/m/d').'/'.  urlencode(str_replace(' ','-',$post->title)).''; ?>"><?php echo $post->title; ?></a></li>-->
                    <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="col-md-3">
            </div>
            <div class="col-md-3">

                <img src="<?php echo base_url() ?>assets/images/logo.png">
                <p class="copyright">© <script>document.write(new Date().getFullYear())</script> Joy Holidays Group
                    <br>Via Flavio Gioia, 13 - Procida, Naples, Italy
                    <br>p.iva 07207821211</p>
                <br>
                <ul class="list-inline">
                    <li><a href="#" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
                    </li>
                    <li><a href="https://www.instagram.com/cabo360s" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
                    </li>
                    <li><a href="https://www.facebook.com/cabo360s" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
                    </li>
                </ul>
                <br>

                <div class="input-group col-md-">
                    <input type="text" class="form-control" placeholder="Newsletter">
                    <span class="input-group-btn">
                        <button class="btn  btn-newsletter" type="button"><i class="fa fa-angle-right"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">

                    <ul class="list-inline footer-bottom-menu">
                        <li><a href="<?php echo base_url() . $this->lang->lang(); ?>"><?php echo lang("about-us");?></a>
                        </li>
                        <li>|</li>
                        <li><a href="<?php echo base_url() . 'registrati' ?>"><?php echo lang("how-it-works");?></a>
                        </li>
                        <li>|</li>
                        <li><a href="<?php echo base_url() . $this->lang->lang() . '/privacy-policy' ?>"><?php echo lang("privacy-policy");?></a>
                        </li>
                        <li>|</li>
                        <li><a href="<?php echo base_url() . $this->lang->lang() . '/terms-conditions' ?>"><?php echo lang("terms-conditions");?></a>
                        </li>
                        <li>|</li>
                        <li><a href="mailto:visitprocida@gmail.com"><?php echo lang("contact-us");?></a>
<li>|</li>
                        <li><a href="http://blog.visitprocida.com/">Blog</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--################ FOOTER END ################-->
<!--################ JAVASCRIPTS ################-->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.fitvids.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.1.3.js"></script>

<script type="text/javascript" charset="utf-8" src="<?php echo base_url(); ?>assets/assets/twitter/jquery.tweet.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/slidebox-min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox-thumbs.js?v=1.0.2"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox-media.js?v=1.0.0"></script>
<script src="<?php echo base_url(); ?>assets/js/stellar.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.isotope.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.lazyimage.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom-home.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portfolio.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>
<!-- ajax contact form script -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mail.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/js-webshim/minified/polyfiller.js"></script>
<div id='invtrflfloatbtn'></div>
<script>    
var invite_referrals = window.invite_referrals || {}; (function() { 
    invite_referrals.auth = { bid_e :'388A4CF3EF142DD6BF8B3186D5287115',
                                bid : '19686', 
                                t : '420', 
                                email : '', userParams : {'fname': ''}};    
                                invite_referrals.async = false;
var script = document.createElement('script');
script.src = "//cdn.invitereferrals.com/js/invite-referrals-1.0.js";
var entry = document.getElementsByTagName('script')[0];entry.parentNode.insertBefore(script, entry); })();
</script>


<?php if (isset($jsfiles)): ?>
    <?php foreach ($jsfiles as $js): ?>
        <?php if ($js['type'] == 'file'): ?>
            <script src="<?php echo $js['src'] ?>" type="text/javascript"></script>
        <?php else: ?>
            <script type="text/javascript">
            <?php echo $js['src'] ?>
            </script>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
<script type="text/javascript">

    webshims.polyfill();

    function booking_search_menu() {
        checkin = document.getElementById('bs_checkin_date_menu').value;
        if (checkin == '') {
            toastr.error("Checkin Date is required field");
            return false;
        } else if (!isValidDate(checkin)) {
            toastr.error("Please enter check in date in MM/DD/YYYY format");
            return false;
        }
        checkout = document.getElementById('bs_checkout_date_menu').value;
        if (checkout == '') {
            toastr.error('Checkout Date is required field');
            return false;
        } else if (!isValidDate(checkout)) {
            toastr.error("Please enter check out date in MM/DD/YYYY format");
            return false;
        }
        var r = document.getElementById("bs_rooms_menu");
        rooms = r.options[r.selectedIndex].value;
        if (rooms == '' || rooms == '0') {
            toastr.error('Rooms is a required field');
            return false;
        }

        var parts = checkin.split("/");
        var checkin_day = parseInt(parts[1], 10);
        var checkin_month = parseInt(parts[0], 10);
        var checkin_year = parseInt(parts[2], 10);

        var parts = checkout.split("/");
        var checkout_day = parseInt(parts[1], 10);
        var checkout_month = parseInt(parts[0], 10);
        var checkout_year = parseInt(parts[2], 10);

        window.open('http://www.booking.com/searchresults.html?sid=5b818543a2ad751a35cf1d134baee464&dest_id=2219&dest_type=region&checkin_monthday=' + checkin_day + '&checkin_year_month=' + checkin_month + '-' + checkin_year + '&checkout_monthday=' + checkout_day + '&checkout_year_month=' + checkout_month + '-' + checkout_year + '&aid=363714', '_target');
    }

    function isValidDate(dateString) {
        // First check for the pattern
        if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
            return false;

        // Parse the date parts to integers
        var parts = dateString.split("/");
        var day = parseInt(parts[1], 10);
        var month = parseInt(parts[0], 10);
        var year = parseInt(parts[2], 10);

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12)
            return false;

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        return day > 0 && day <= monthLength[month - 1];
    }
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-30394795-7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-30394795-7');
</script>


<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"<?php echo lang('cookie-policy')?>","dismiss":"<?php echo lang('got-it')?>","learnMore":"<?php echo lang('more-info')?>","link":"<?php echo base_url() . $this->lang->lang() . '/privacy-policy' ?>","theme":"dark-floating"};
</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Richiesta informazioni</h4>
            </div>
            <div class="modal-body">
                <form action="#" method="post"  id="form">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nome*</label>
                                <input type="text" name="nome" id="nome" placeholder="Nome" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Cognome*</label>
                                <input type="text" name="cognome" id="cognome" placeholder="Cognome" class="form-control" />
                            </div>
                            <div class="form-group"><label>Indirizzo Email*</label>
                                <input type="text" name="email" id="email" placeholder="Indirizzo Email" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Indirizzo</label>
                                <input type="text" name="indirizzo" id="indirizzo" placeholder="Via, Piazza, Civico" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Città/CAP*</label>
                                <input type="text" name="cap" id="cap" placeholder="Città, CAP" class="form-control" /></div><div class="form-group">
                                <label>Tipologia richiesta
                                    <select name="tipoScelto0" id="tipoScelto0" class="form-control">
                                        <option value="scegli">scegli</option>
                                        <option value="informazioni generiche">informazioni generiche</option>
                                        <option value="disponibilità alloggi">disponibilità alloggi</option>
                                        <option value="come raggiungerci">come raggiungerci</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Messaggio</label>
                                <textarea name="richiesta" id="richiesta" placeholder="Message" class="form-control" style="height: 120px;"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-purity">Invia richiesta</button>
            </div>
        </div>
    </div>
</div>
<!--<script src='http://www.google.com/recaptcha/api.js'></script>-->
<script>
    var LANG = '<?php echo $this->lang->lang(); ?>';
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    function validate(data) {
        valid = true;
        if (data.firstname.length < 1) {
            decorateElement("#firstname", true, LANG == "en" ? "First name is required field." : "Nome è richiesto Campi.");
            valid = false;
        } else {
            decorateElement("#firstname", false, "");
        }

        if (data.lastname.length < 1) {
            decorateElement("#lastname", true, LANG == "en" ? "Last name is required field." : "Cognome è richiesto Campi.");
            valid = false;
        } else {
            decorateElement("#lastname", false, "");
        }

        if (data.email.length < 1) {
            decorateElement("#email-se", true, LANG == "en" ? "Email is required field." : "Email è richiesta campo.");
            valid = false;
        } else if (!validateEmail(data.email)) {
            decorateElement("#email-se", true, LANG == "en" ? "Please enter valid email address." : "Inserisci indirizzo email valido.");
            valid = false;
        } else {
            decorateElement("#email-se", false, "");
        }

        if (data.message.length < 1) {
            decorateElement("#message", true, LANG == "en" ? "Message is required field." : "Messaggio è richiesto Campi.");
            valid = false;
        } else {
            decorateElement("#message", false, "");
        }

        if (data.subject.length < 1) {
            decorateElement("#subject-se", true, LANG == "en" ? "Subject is required field." : "Soggetto è richiesto Campi.");
            valid = false;
        } else {
            decorateElement("#subject-se", false, "");
        }
        /*  MICHELE
        if (data.g_recaptcha_response.length < 1) {
            decorateElement("#g-recaptcha", true, LANG == "en" ? "Please verify you are not robot." : "Si prega di verificare che non sono robot.");
            valid = false;
        } else {
            decorateElement("#g-recaptcha", false, "");
        }*/
        return valid;
    }

    function validatetransfer(data) {
        valid = true;
        if (data.firstname.length < 1) {
            decorateElement("#firstname_transfer", true, LANG == "en" ? "First name is required field." : "Nome è richiesto Campi.");
            valid = false;
        } else {
            decorateElement("#firstname_transfer", false, "");
        }

        if (data.lastname.length < 1) {
            decorateElement("#lastname_transfer", true, LANG == "en" ? "Last name is required field." : "Cognome è richiesto Campi.");
            valid = false;
        } else {
            decorateElement("#lastname_transfer", false, "");
        }

        if (data.email.length < 1) {
            decorateElement("#email_add", true, LANG == "en" ? "Email is required field." : "Email è richiesta campo.");
            valid = false;
        } else if (!validateEmail(data.email)) {
            decorateElement("#email_add", true, LANG == "en" ? "Please enter valid email address." : "Inserisci indirizzo email valido.");
            valid = false;
        } else {
            decorateElement("#email_add", false, "");
        }

        if (data.cnfemail.length < 1) {
            decorateElement("#email-cnf", true, LANG == "en" ? "Email is required field." : "Email è richiesta campo.");
            valid = false;
        } else if (!validateEmail(data.cnfemail)) {
            decorateElement("#email-cnf", true, LANG == "en" ? "Please enter valid email address." : "Inserisci indirizzo email valido.");
            valid = false;
        } else if (data.cnfemail != data.email) {
            decorateElement("#email-cnf", true, LANG == "en" ? "Please enter identical email address." : "Inserisci indirizzo email identico.");
            valid = false;
        }else {
            decorateElement("#email-cnf", false, "");
        }

        if (data.phone.length < 1) {
            decorateElement("#phone", true, LANG == "en" ? "Telephone is required field." : "Telefono è richiesto Campi.");
            valid = false;
        } else if (data.phone.length > 12) {
            decorateElement("#phone", true, LANG == "en" ? "Please enter valid phone number." : "Si prega di inserire un numero di telefono valido.");
            valid = false;
        } else if (data.phone.length < 10) {
            decorateElement("#phone", true, LANG == "en" ? "Please enter valid phone number." : "Si prega di inserire un numero di telefono valido.");
            valid = false;
        }else {
            decorateElement("#phone", false, "");
        }
        
        return valid;
    }

    function resetSendMailForm() {
        $('#sendmail-form')[0].reset();
        decorateElement("#g-recaptcha", false, "");
        decorateElement("#subject-se", false, "");
        decorateElement("#message", false, "");
        decorateElement("#email-se", false, "");
        decorateElement("#lastname", false, "");
        decorateElement("#firstname", false, "");
    }
    function decorateElement(id, error, message) {
        if (error) {
            if (!$(id).next().is("span")) {
                $(id).closest('div[class^="form-group"]').addClass("has-error");
                $(id).after(function () {
                    return "<span style='color:red'><small>" + message + "</small></span>";
                });
            }
        } else if ($(id).next().is("span")) {
            $(id).closest('div[class^="form-group"]').removeClass("has-error");
            $(id).next("span").remove();
        }
    }

    $(document).ready(function () {
        $(".btn-sendemail").click(function () {
            $("#sendemail-orderid").val($(this).attr("data-orderid"));
            $("#sendemail-type").val($(this).attr("data-type"));
            $("#sendMail").modal("show");
        });

        var booking = [];

        $(".btn-sendTransferEmail").on('click',function () {
            if ($('#address_2').val() == '') {
                toastr.warning($('#address_2 option:selected').text());
                return false;
            }
    
            information.pickupAddress = $('#address option:selected').text();
            information.dropoffAddress = $('#address_2 option:selected').text();
            information.transferType = true;
            information.date = $('#date').val();
            information.time = $('#time').val();
            information.returnDate = $('#returnDate').val();
            information.returnTime = $('#returnTime').val();
            information.price = $('#price-n').val();
            information.people = $('#adults').val();
            information.children = $('#children').val();
            information.serviceQuantity = 1;
            information.transfer_type =  $("input[name='transfer_type']:checked").val();
            information.flight_train_no =  $("#flight_train_no").val();
            information.special_request =  $("#special_request").val();
            information.infants =  $("#infants").val();
            information.pets =  $("#pets").val();

            var returnDate = $('#returnDate').val();

            if (returnDate != " " && returnDate != "" && returnDate != undefined){
                if ($('#address_return').val() == '') {
                    toastr.warning($('#address_return option:selected').text());
                    return false;
                }
            information.pickupReturnAddress = $('#address_return option:selected').text();
            information.dropoffReturnAddress = $('#address_return_2 option:selected').text();
                information.serviceQuantity = 2;
                information.return_flight_train_no =  $("#return_flight_train_no").val();
            }
            booking.push(information);
            console.log(booking);
            $("#sendTransferEmail").modal("show");
        });

        $('#btn-sendtransferemail').on('click', function () {
            // console.log(booking);
            // return false:
            var formdata = {
                firstname: $.trim($("#sendtransfermail-form input[name=firstname]").val()),
                lastname: $.trim($("#sendtransfermail-form input[name=lastname]").val()),
                email: $.trim($("#sendtransfermail-form input[name=email]").val()),
                cnfemail: $.trim($("#sendtransfermail-form input[name=confirm_email]").val()),
                phone: $.trim($("#sendtransfermail-form input[name=phone]").val()),
                page_url: window.location.href
            };
            if (validatetransfer(formdata)) {
                $(".preloading").show();
                $.post('<?php echo base_url(); ?>cart/sendtransfermail',
                        {firstname: formdata.firstname,
                            lastname: formdata.lastname,
                            email: formdata.email,
                            cnfemail: formdata.cnfemail,
                            phone: formdata.phone,
                            page_url: formdata.page_url,
                            bookingInfo : booking
                        },
                function (response) {

                    $('#sendTransferEmail').modal({backdrop: 'static',keyboard: false});

                    $('.modal-body-2').fadeOut('slow','linear');
                    $('.modal-body-2').empty();

                    $('.modal-footer-2').fadeOut('slow','linear');
                    $('.modal-footer-2').empty();

                    $('.modal-footer-2').append('<a onClick="closeMail();" class="btn btn-purity" id="btn-sendemail"><?php echo lang("close"); ?></a>');

                    if (response.status == 200) {

                        if(LANG == "it") {
                            $('.modal-body-2').append('<div id="message"> Il messaggio è stato inviato con successo al proprietario bussiness.</div>');
                        } else {
                            $('.modal-body-2').append('<div id="message"> Success, your message is successfully sent to the bussiness owner.</div>');
                        }

                        $('.modal-body-2').fadeIn('slow','linear');
                        $('.modal-footer-2').fadeIn('slow','linear');

                    } else {
                        //$(".preloading").hide();
                        $('.modal-body-2').append('<div id="message">Error: '+ response.content +'</div>');
                    }
                    //$(".preloading").hide();
                }
                );
            }
        });

        $('#btn-sendemail').on('click', function () {
            var formdata = {
                firstname: $.trim($("#sendmail-form input[name=firstname]").val()),
                lastname: $.trim($("#sendmail-form input[name=lastname]").val()),
                email: $.trim($("#sendmail-form input[name=email]").val()),
                subject: $.trim($("#sendmail-form select[name=subject]").val()),
                message: $.trim($("#sendmail-form textarea[name=message]").val()),
                orderid: $.trim($("#sendmail-form input[name=orderid]").val()),
                type: $.trim($("#sendmail-form input[name=type]").val()),
                // MICHELE  g_recaptcha_response: $("#sendmail-form textarea[name=g-recaptcha-response]").val(),
                page_url: window.location.href
            };
            if (validate(formdata)) {
              
                $(".preloading").show();
                $.post('<?php echo base_url(); ?>ajax/sendmail',
                        {firstname: formdata.firstname,
                            lastname: formdata.lastname,
                            email: formdata.email,
                            subject: formdata.subject,
                            message: formdata.message,
                            orderid: formdata.orderid,
                            type: formdata.type,
                            // MICHELE  g_recaptcha_response: formdata.g_recaptcha_response,
                            page_url: formdata.page_url
                        },
                function (response) {
                     // MICHELE grecaptcha.reset();

                    $('#sendMail').modal({backdrop: 'static',keyboard: false});

                    $('.modal-body').fadeOut('slow','linear');
                    $('.modal-body').empty();

                    $('.modal-footer').fadeOut('slow','linear');
                    $('.modal-footer').empty();

                    $('.modal-footer').append('<a onClick="closeMail();" class="btn btn-purity" id="btn-sendemail"><?php echo lang("close"); ?></a>');

                    if (response.status == 200) {
                        resetSendMailForm();

						if(LANG == "it") {
							$('.modal-body').append('<div id="message"> Il messaggio è stato inviato con successo al proprietario bussiness.</div>');
						} else {
							$('.modal-body').append('<div id="message"> Success, your message is successfully sent to the bussiness owner.</div>');
						}

						$('.modal-body').fadeIn('slow','linear');
						$('.modal-footer').fadeIn('slow','linear');

                    } else {
                        //$(".preloading").hide();
                        $('.modal-body').append('<div id="message">Error: '+ response.content +'</div>');
                    }
                    //$(".preloading").hide();
                }
                );
            }
        });
    });

$("#myModal").on("hidden.bs.modal", function () {
	location.reload();
});

function closeMail() {
    location.reload();
}
function removeFromCart(itemId){
	var data = { "serviceId": itemId };

	$.ajax({
	  type: "POST",
	  url: '<?php echo base_url(); ?>Cart/delete',
	  data: data,
	  success: function(data, textStatus, jqXHR){
		if(data){
      var cartCount = $('#cart_count').html();
			cartCount = cartCount.replace('(', '');
			cartCount = cartCount.replace(')', '');
			var newCartCount = parseInt(cartCount) -1;
			$('#cart_count').html('('+newCartCount+')');
			$('#'+itemId).remove();
			var location = '<?php echo $this->uri->segment(1); ?>';
			if(location === 'Cart'){
				window.location.reload()
			}
		}
	  },
	  error: function (jqXHR, textStatus, errorThrown){
		toastr.warning('Ops something went wrong');
	  }
	})
}

</script>
<!-- Send Modal -->
<div class="modal fade" id="sendMail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" method="post" id="sendmail-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo lang("Request_Information"); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" value="0" name="orderid" id="sendemail-orderid">
                    <input type="hidden" value="type" name="type" id="sendemail-type">
                    <div class="row">
                        <div class="col-xs-12 col-md-5">
                            <div class="form-group">
                                <label><?php echo lang("First_Name") ?></label>
                                <input type="text" name="firstname" id="firstname" placeholder="<?php echo lang("First_Name") ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label><?php echo lang("Last_Name") ?></label>
                                <input type="text" name="lastname" id="lastname" placeholder="<?php echo lang("Last_Name") ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label><?php echo lang("Email_Address") ?></label>
                                <input type="text" name="email" id="email-se" placeholder="<?php echo lang("Email_Address") ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label><?php echo lang("Subject") ?></label>
                                <select name="subject" id="subject-se" class="form-control">
                                    <option value="General Informations"><?php echo lang("General_Informations") ?></option>
                                    <option value="Reservations"><?php echo lang("Reservation") ?></option>
                                    <option value="Others"><?php echo lang("Other") ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <div class="form-group">
                                <label><?php echo lang("Message"); ?></label>
                                <textarea name="message" id="message" placeholder="<?php echo lang("Message"); ?>" style="min-height: 182px; max-height: 182px; max-width: 100%" class="form-control" style="height: 120px;"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="g-recaptcha" id="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_SITE_KEY; ?>"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-purity" id="btn-sendemail"><?php echo lang("Send") ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<div    class="modal fade" id="sendTransferEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" method="post" id="sendtransfermail-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo lang("User_information"); ?></h4>
                </div>
                <div class="modal-body modal-body-2">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <label><?php echo lang("First_Name") ?></label>
                                <input type="text" name="firstname" id="firstname_transfer" placeholder="<?php echo lang("First_Name") ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label><?php echo lang("Last_Name") ?></label>
                                <input type="text" name="lastname" id="lastname_transfer" placeholder="<?php echo lang("Last_Name") ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label><?php echo lang("Email_Address") ?></label>
                                <input type="text" name="email" id="email_add" placeholder="<?php echo lang("Email_Address") ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label><?php echo lang("Confirm_Email_Address") ?></label>
                                <input type="text" name="confirm_email" id="email-cnf" placeholder="<?php echo lang("Confirm_Email_Address") ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label><?php echo lang("Phone") ?></label>
                                <input type="number" name="phone" id="phone" placeholder="<?php echo lang("Phone") ?>" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-2">
                    <button type="button" class="btn btn-purity" id="btn-sendtransferemail"><?php echo lang("Send") ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--<div class="bottom_ads visible-xs">
	<div class="col-xs-12 text-center no-padding">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- VSP_468_mobile --*>
		<ins class="adsbygoogle"
		style="display:inline-block;width:320px;height:100px"
		data-ad-client="ca-pub-0169409437886826"
		data-ad-slot="8063397908"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>
</div>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCZSbQ1a4lxi2TRfqqtS2RIyOUmALhCDo&callback=initMap"></script>
</body>
</html>
