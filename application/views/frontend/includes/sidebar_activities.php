﻿<?php
/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */
?>
<div class="row">
	 <?php if($table != "beaches"): ?>
    <div class="col-md-12">
        <div class="sidebar-element">
        	<?php if($table != "restaurants" ){?>
            	<?php if($table != "barsandcafe" ){?>
           			<?php if($table != "boatrentals" ){?>
                    	<?php if($table != "beach_clubs" ){?>
                        	<?php if($table != "shops" ){?>
                                <?php if($table != "weddings" ){?>
                                    <div id="map-canvas-sidebar" class="img-rounded"></div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
           		<?php } ?>
           	<?php } ?>
		</div>
    </div>
    <?php endif; ?>
    <div class="col-md-12">
        <h4 class="sidebar-title"><?= lang("In the same area");?> </h4>
    </div>
    <div class="col-md-12">
        <div class="restaurant-sidebar-tabs">
            <ul id="tab" class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#restaurants" role="tab" aria-controls="restaurants" data-toggle="tab"><i class="fa fa-2x color2 fa-cutlery"></i></a></li>
                <li class=""><a href="#hotelssd" role="tab" aria-controls="hotelssd" data-toggle="tab"><i class="fa fa-2x color3 fa-hotel"></i></a></li>
                <li class=""><a href="#sidebar_cafes" role="tab" aria-controls="sidebarcafes" data-toggle="tab"><i class="fa fa-2x color4 fa-coffee"></i></a></li>
                <li class=""><a href="#sidebar_shopes" role="tab" aria-controls="sidebarshopes" data-toggle="tab"><i class="fa fa-2x color5 fa-shopping-cart"></i></a></li>
				<li class=""><a href="#sidebar_beachclubs" role="tab" aria-controls="sidebarbeachclubs" data-toggle="tab"><i class="fa fa-2x color4 fa-life-saver"></i></a></li>
            </ul>
            <div id="myTabContent" class="tab-content">

                <div class="tab-pane fade in active" id="restaurants">
                    <?php //var_dump($resturants[0]);  ?>
                    <!-- RESTAURANT LOOP -->

                    <?php if (isset($resturants) && is_array($resturants)): ?>
                        <?php foreach ($resturants as $resturant): ?>
                            <?php if ($resturant->id != $this->Resturant_model->id): ?>
                                <?php $resturant_array = (array) $resturant; ?>
                                <div class="listing-item sidebar-item">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="listing-image-sidebar">
                                                <img src="<?php echo base_url(); ?>uploads/resturants/<?php echo $resturant->photo ?>" class="img-responsive img-rounded" alt="...">
                                            </div>
                                        </div>
                                        <div class="col-xs-8">

                                            <a href="<?php echo base_url() . $this->lang->lang() . "/" . lang('eat-and-drink') . "/restaurant/" . $resturant->pageTitle; ?>"><h3 class="listing-title-sidebar noMarginBottom"><?php echo $resturant_array[$this->lang->lang() . 'Name']; ?></h3></a>
                                            <p class="listing-location noMarginBottom">
                                                <span class="color2 fa fa-map-marker"></span>&nbsp; <?php echo round($resturant->distance_in_km, 2) ?> Km</p>
                                            <span class="listing-price-sidebar">€ <?php echo $resturant->min_price; ?>/<?php echo $resturant->max_price; ?></span>
                                            <p>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <!-- RESTURANT LOOP ENDS -->
                </div>
                <div class="tab-pane fade" id="hotelssd">
                    <!-- HOTEL LOOP -->

                    <?php if (isset($hotels) && is_array($hotels)): ?>
                        <?php foreach ($hotels as $hotel): ?>
                            <?php $hotel_array = (array) $hotel; ?>
                            <div class="listing-item sidebar-item">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="listing-image-sidebar">
                                            <img src="<?php echo base_url(); ?>uploads/hotels/<?php echo $hotel->photo ?>" class="img-responsive img-rounded" alt="...">
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <h3 class="listing-title-sidebar noMarginBottom"><a class="hotel-list" href='<?= isset($hotel->url)?$hotel->url:'#'; ?>' alt='<?php echo $hotel->enName ?>' rel="nofollow"><h3 class="listing-title-sidebar noMarginBottom"><?php echo $hotel_array[$this->lang->lang() . 'Name'] ?></h3></a></h3>
                                        <p class="listing-location noMarginBottom">
                                            <span class="color2 fa fa-map-marker"></span>&nbsp; <?php echo round($hotel->distance_in_km,2) ?> Km</p>
                                        <span class="listing-price-sidebar">€ <?php echo $hotel->price . "/" . $hotel->unit ?></span>
                                        <p>
                                            <span class="fa fa-star color4"></span>
                                            <span class="fa fa-star color4"></span>
                                            <span class="fa fa-star color4"></span>
                                            <span class="fa fa-star color4"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <!-- HOTEL LOOP ENDS-->
                </div>
                <div class="tab-pane fade" id="sidebar_cafes">
                    <!-- HOTEL LOOP -->

                    <?php if (isset($sb_bars_cafe) && is_array($sb_bars_cafe)): ?>
                        <?php foreach ($sb_bars_cafe as $item): ?>
                            <?php if ($item->id != $this->Barsandcafe_model->id): ?>
                                <?php $bc_array = (array) $item; ?>
                                <div class="listing-item sidebar-item">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="listing-image-sidebar">
                                                <img src="<?php echo base_url(); ?>uploads/bars_cafe/<?php echo $item->photo ?>" class="img-responsive img-rounded" alt="...">
                                            </div>
                                        </div>
                                        <div class="col-xs-8">
                                            <h3 class="listing-title-sidebar noMarginBottom"><a class="hotel-list" href='<?php echo base_url() . $this->lang->lang() . '/' . lang('eat-and-drink') . '/bars-and-cafe/' . $item->pageTitle . '' ?>' rel="nofollow"><h3 class="listing-title-sidebar noMarginBottom"><?php echo $bc_array[$this->lang->lang() . 'Name'] ?></h3></a></h3>
                                            <p class="listing-location noMarginBottom">
                                                <span class="color2 fa fa-map-marker"></span>&nbsp; <?php echo round($item->distance_in_km, 2); ?> Km</p>
                                            <span class="listing-price-sidebar">€ <?php echo $item->min_price . "/" . $item->max_price; ?></span>
                                            <p>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <!-- HOTEL LOOP ENDS-->
                </div>
                <div class="tab-pane fade" id="sidebar_shopes">
                    <!-- HOTEL LOOP -->

                    <?php if (isset($sb_shops) && is_array($sb_shops)): ?>
                        <?php foreach ($sb_shops as $item): ?>
                            <?php if ($item->id != $this->Shope_model->id): ?>
                                <?php $bc_array = (array) $item; ?>
                                <div class="listing-item sidebar-item">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="listing-image-sidebar">
                                                <img src="<?php echo base_url(); ?>uploads/shopes/<?php echo $item->photo ?>" class="img-responsive img-rounded" alt="...">
                                            </div>
                                        </div>
                                        <div class="col-xs-8">
                                            <h3 class="listing-title-sidebar noMarginBottom"><a class="hotel-list" href='<?php echo base_url() . $this->lang->lang() . '/' . lang('what-to-do') . '/shopping/' . $item->pageTitle . '' ?>' rel="nofollow"><h3 class="listing-title-sidebar noMarginBottom"><?php echo $bc_array[$this->lang->lang() . 'Name'] ?></h3></a></h3>
                                            <p class="listing-location noMarginBottom">
                                                <span class="color2 fa fa-map-marker"></span>&nbsp; <?php echo round($item->distance_in_km, 2); ?> Km</p>
                                            <span class="listing-price-sidebar">€ <?php echo $item->minPrice . "/" . $item->maxPrice; ?></span>
                                            <p>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <!-- HOTEL LOOP ENDS-->
                </div>
				<div class="tab-pane fade" id="sidebar_beachclubs">
                    <!-- HOTEL LOOP -->

                    <?php if (isset($sb_beach_clubs) && is_array($sb_beach_clubs)): ?>
                        <?php foreach ($sb_beach_clubs as $item): ?>
                            <?php if ($item->id != $this->Beachclub_model->id): ?>
                                <?php $bc_array = (array) $item; ?>
                                <div class="listing-item sidebar-item">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="listing-image-sidebar">
                                                <img src="<?php echo base_url(); ?>uploads/beach_clubs/<?php echo $item->photo ?>" class="img-responsive img-rounded" alt="...">
                                            </div>
                                        </div>
                                        <div class="col-xs-8">
                                            <h3 class="listing-title-sidebar noMarginBottom"><a class="hotel-list" href='<?php echo base_url() . $this->lang->lang() . '/' . lang('what-to-do') . '/'.lang('beach-clubs').'/' . $item->pageTitle . '' ?>' rel="nofollow"><h3 class="listing-title-sidebar noMarginBottom"><?php echo $bc_array[$this->lang->lang() . 'Name'] ?></h3></a></h3>
                                            <p class="listing-location noMarginBottom">
                                                <span class="color2 fa fa-map-marker"></span>&nbsp; <?php echo round($item->distance_in_km, 2); ?> Km</p>
                                            <span class="listing-price-sidebar">€ <?php echo $item->minPrice . "/" . $item->maxPrice; ?></span>
                                            <p>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                                <span class="fa fa-star color4"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <!-- HOTEL LOOP ENDS-->
                </div>

            </div>
        </div>
    </div>
</div>
