<?php
/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */
?>

<div class="sidebar-element">
    <h4 class="sidebar-title"><?php echo lang('Upcoming_Events') ?></h4>
    <?php if (count($events) > 0): ?>
        <?php foreach ($events as $event): ?>
            <?php $event_array = (array) $event; ?>
            <div class="sidebar-event">
                <div class="row">
                    <div class="col-md-4">
                        <div class="dateBox">
                            <span class="date"><?php echo date('d', strtotime($event->startDate)); ?></span>
                            <span class="month"><?php echo date('M', strtotime($event->startDate)); ?></span>
                        </div>
                    </div>
                    <div class="col-md-8 noPaddingLeft">
                        <a href="<?php echo base_url().$this->lang->lang()."/".lang('events').'/traditional-event/'.$event->order_id ?>"><h3 class="sidebar-event-heading"><?php echo $event_array[$this->lang->lang() . 'Title']; ?></h3></a>
                        <p class="sidebar-event-date"><?php echo date('M d, Y', strtotime($event->startDate)); ?></p>
                        <p class="sidebar-event-location"><?php echo $this->procida_model->getLocationName($event->location,$this->lang->lang()); ?></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <?php echo lang('events_not_found_mesg') ?>
    <?php endif; ?>
</div>