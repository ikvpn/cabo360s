<div id="search_bar_container" style="z-index:999">
  <div class="container">
    <div class="search_bar">
      <div class="search-bar-home visible-lg" >
        <div class="container">
          <div class="row">
            <div class="col-md-12" style="padding-left:   0;">
              <div class="page-title-search page-title-search-inline">
                <script src="https://hbb.bz/thirdmask/js/masck.js"></script>
                <script src="https://hbb.bz/thirdmask/js/library/jquery-ui.min.js"></script>
                <link href="https://hbb.bz/thirdmask/config_reseller/16/custom.css" rel="stylesheet">
                <link href="https://hbb.bz/thirdmask/css/library/jquery-ui.min.css" rel="stylesheet">
                <div id="hotelSearch">
                  <div class="form-cn form-hotel " id="form-hotel">
                    <form action="<?php echo base_url(); ?><?php echo $this->lang->lang(); ?>/HotelBB/search" method="post" name="formhotel">
                      <input type="hidden" value="<?php echo $this->lang->lang(); ?>" name="lang">
                      <div class="form-search clearfix">
                        <div class="col-sm-3 col-xs-6" style="padding-left:   0; ">
                          <div class="input-group">
                            <input value="" type="text" style="padding-left:10px;" class="form-control datepicker" required autocomplete="off" name="datastart" placeholder="<?php echo lang('Check-In'); ?>">
                            <div id="startIcon" class="input-group-addon">
                              <span class="fa fa-calendar"></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                          <div class="input-group">
                            <input value="" type="text" style="padding-left:10px; " class="form-control datepicker" required autocomplete="off" name="dataend" placeholder="<?php echo lang('Check-Out'); ?>">
                            <div id="endIcon" class="input-group-addon">
                              <span class="fa fa-calendar"></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                          <div class="form-group" id="divospitiroom">
                            <input type="text" id="ospiti"  sty name="txtospiti" class="form-control"  autocomplete="off" placeholder="<?php if ($this->lang->lang() == 'en') { echo "Peoples"; } else { echo "Persone"; }  ?>" value="">
                            <input type="hidden" id="jsonparty" name="jsonparty" value='[{"adults":2}]' />
                            <div class="popupbambini" id="boxospiti">
                              <div id="boxcamere"></div>
                              <div class="ospitibottom">
                                <a style="float:left; font-size:12px;" id="addcamera" href="#"><?php echo '+ '.lang('add_camera'); ?></a>

                                <a class="btn btn-sm btn-purity awe-btn awe-btn-1 awe-btn-small settaospiti" id="settaospiti" href="#">OK</a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3 col-xs-6" style="  padding-right: 40px;">
                          <button type="submit" class="btn btn-purity btn-block "><?php echo lang('Search'); ?></button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <div id="ferrySearch">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="col-lg-4">
                        <div class="input-group pad15">
                          <input id="desktopBookingDate" type="text" class="form-control  datepicker" name="check_in" placeholder=" <?php echo lang('Departure'); ?>">
                          <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group pad15">
                          <select id="traghettilineDesktopSearch" class="form-control tratte">
                            <?php if ($this->lang->lang() == 'en') { ?>
                              <option value=""> Route </option>
                              <optgroup label="to Ischia Island" per="True">
                                <option value="Napoli-Procida" per="True">Naples - Procida</option>
                                <option value="Napoli Beverello-Procida" per="True">Naples Beverello - Procida</option>
                                <option value="Pozzuoli-Procida" per="True">Pozzuoli - Procida</option>
                                <option value="Procida-Casamicciola" per="True">Procida - Casamicciola</option>
                                <option value="Procida-Ischia" per="True">Procida - Ischia</option>
                              </optgroup>
                              <optgroup label="from Ischia Island" per="True">
                                <option value="Casamicciola-Procida" per="False">Casamicciola - Procida</option>
                                <option value="Ischia-Procida" per="False">Ischia - Procida</option>
                                <option value="Procida-Napoli" per="False">Procida - Naples</option>
                                <option value="Procida-Napoli Beverello" per="False">Procida - Naples Beverello</option>
                                <option value="Procida-Pozzuoli" per="False">Procida - Pozzuoli</option>
                              </optgroup>
                              <?php }else{ ?>
                                <option value=""> Tratta </option>
                                <optgroup label="per l'Isola di Ischia" per="True">
                                  <option value="Napoli-Procida" per="True">Napoli - Procida</option>
                                  <option value="Napoli Beverello-Procida" per="True">Napoli Beverello - Procida</option>
                                  <option value="Pozzuoli-Procida" per="True">Pozzuoli - Procida</option>
                                  <option value="Procida-Casamicciola" per="True">Procida - Casamicciola</option>
                                  <option value="Procida-Ischia" per="True">Procida - Ischia</option>
                                </optgroup>
                                <optgroup label="dall'Isola di Ischia" per="True">
                                  <option value="Casamicciola-Procida" per="False">Casamicciola - Procida</option>
                                  <option value="Ischia-Procida" per="False">Ischia - Procida</option>
                                  <option value="Procida-Napoli" per="False">Procida - Napoli</option>
                                  <option value="Procida-Napoli Beverello" per="False">Procida - Napoli Beverello</option>
                                  <option value="Procida-Pozzuoli" per="False">Procida - Pozzuoli</option>
                                </optgroup>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-4 ferryDiv">
                            <a class="btn btn-block btn-ferry-desktop" id="bookingFerryDesktop">
                              <?php echo lang('Search'); ?>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- End search bar-->
      </div><!-- End container bar-->
    </div><!-- /search_bar-->
<script>
	$('#bookingFerryDesktop').on('click',function() {
		var lang = '<?php echo $this->lang->lang(); ?>';
		var date = $('#desktopBookingDate').val();
		var pars = date.split("/");
		var check_day = parseInt(pars[1], 10);
		var check_month = parseInt(pars[0], 10);
        if(check_month < 10)
          check_month = '0'+check_month;
		var check_year = parseInt(pars[2], 10);
		date = check_year+'-'+check_month+'-'+check_day;
		var route = $('#traghettilineDesktopSearch option:selected').val();
    var url = '<?php echo base_url().$this->lang->lang(); ?>/info-utili/ferry-time-table?route='+route+'&date='+date;
		window.location = url;
	});
	
	$('#startIcon').on('click', function() {
		$('#hotelSearch input[name=datastart]').datepicker('show');
	});
	
	$('#endIcon').on('click', function() {
		$('#hotelSearch input[name=dataend]').datepicker('show');
	});
</script>
