<?php if('hide' != 'hide') : ?>
<section class="page-title" id="fixed-booking-form">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-search page-title-search-inline">
                  <script src="https://hbb.bz/thirdmask/js/masck.js"></script>
		              <script src="https://hbb.bz/thirdmask/js/library/jquery-ui.min.js"></script>
		              <link href="https://hbb.bz/thirdmask/config_reseller/16/custom.css" rel="stylesheet">
                  <link href="https://hbb.bz/thirdmask/css/library/jquery-ui.min.css" rel="stylesheet">
                  <div class="form-cn form-hotel " id="form-hotel">
                    <form action="<?php echo base_url(); ?><?php echo $this->lang->lang(); ?>/HotelBB/search" method="post" name="formhotel">
                      <input type="hidden" value="<?php echo $this->lang->lang(); ?>" name="lang">
	                    <div class="form-search clearfix">
	                    	<div class="col-sm-3">
	                    	    <h5><?php echo lang("Search Hotel/Apartment in Procida"); ?></h5>
	                    	</div>
	                    	<div class="col-sm-2 col-xs-6">
	                            <div class="input-group">
	                                <input value="" type="text" class="form-control datepicker" required autocomplete="off" name="datastart" placeholder="<?php echo lang('Check-In'); ?>">
	                                <div id="hotelDataStart" class="input-group-addon">
	                                      <span class="fa fa-calendar"></span>
	                                  </div>
	                            </div>
	                        </div>
	                        <div class="col-sm-2 col-xs-6">
	                            <div class="input-group">
	                                <input value="" type="text" class="form-control datepicker" required autocomplete="off" name="dataend" placeholder="<?php echo lang('Check-Out'); ?>">
	                                <div id="hotelDataEnd" class="input-group-addon">
	                                      <span class="fa fa-calendar"></span>
	                                  </div>
	                            </div>
	                        </div>
	                        <div class="col-sm-3 col-xs-6">
								<div class="form-group" id="divospitiroom">
	                  <input type="text" id="ospiti" name="txtospiti" class="form-control"  autocomplete="off" placeholder="Ospiti" value='<?php echo lang('placeholder_form'); ?>'>
								   <input type="hidden" id="jsonparty" name="jsonparty" value='[{"adults":2}]' />
								   <div class="popupbambini" id="boxospiti">
										<div id="boxcamere"></div>
								        <div class="ospitibottom">
											<a style="float:left; font-size:12px;" id="addcamera" href="#"><?php echo '+ '.lang('add_camera'); ?></a>
										    <a class="btn btn-sm btn-purity awe-btn awe-btn-1 awe-btn-small settaospiti" id="settaospiti" href="#">OK</a>
									    </div>
								   </div>
	                            </div>
	                        </div>
	                        <div class="col-sm-2 col-xs-6">
	                            <button type="submit" class="btn btn-purity btn-block "><?php echo lang('Search'); ?></button>
	                        </div>
	                    </div>
					</form>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
#boxospiti {
    top: 40px !important;
}
</style>
<script type="text/javascript">

    function isValidDate(dateString)
    {
        // First check for the pattern
        if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
            return false;

        // Parse the date parts to integers
        var parts = dateString.split("/");
        var day = parseInt(parts[1], 10);
        var month = parseInt(parts[0], 10);
        var year = parseInt(parts[2], 10);

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12)
            return false;

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        return day > 0 && day <= monthLength[month - 1];
    }
    
     
    $('#hotelDataStart').on('click', function() {
    	$('#form-hotel input[name=datastart]').datepicker('show');
    });
    
    $('#hotelDataEnd').on('click', function() {
    	$('#form-hotel input[name=dataend]').datepicker('show');
    });
</script>

<?php endif; ?>