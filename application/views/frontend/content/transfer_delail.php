<!--################ WRAP START ################-->
<style>
.tourHeader {
    margin-top: 5px !important;
}
.left-wrapper {
    display: inline-block;
    width: 100%;
}
.right-wrapper {
    display: inline-block;
}
.left-wrapper h2.color1 {
  color: #5cbca1 !important;
}
p.color1,
h1.color1{
  color: #f10400 !important;
}

.pr-0 {
   padding-right: 0px;
}
.main-title {
    font-size: 40px !important;
}
.title-with-price {
    width: 100%;
    display: grid;
    grid-template-columns: 50% 50%;
    align-items: center;
}
.title-with-price h2 {
    margin-bottom: 0;
}
.title-with-price p:last-child {
    margin: 0;
    text-align: right;
    font-size: 30px;
    color: #b17e2b;
    font-weight: bold;
    line-height: 30px;
}
.title-with-price p{
    margin: 0;
}
.title-with-price p:last-child span{
    width: 100%;
    display: inline-block;
    font-size: 14px;
    line-height: 20px;
}
.img-info-both{
    float: none;
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
}
.img-div{
    padding: 0px;
    width: 60px;
    display: inline-block;
}
.place-info{
    width: calc(100% - 70px);
    display: inline-block;
}
@media (min-width: 1024px){
  .tourHeader {
    max-height: 300px;
    height:300px;
    margin:0 !important;
  }
  .tourHeader img {
      max-height: 400px !important;
  }
  .menu-bg {
    height: 76px!important;
  }
}
@media (max-width: 768px){
  #detail_slider .banner-title h1 {
    font-size: 36px;
  }
  .tp-banner-container  {
    position: relative;
  }
  .main-page-title {
    bottom: 20px;
  }
  .detail-page-wrapper .tourHeader {
    margin-top: 0px !important;
  }
  .tourHeader {
    height: auto;
  }
  .title-with-price {
      display: inline-block;
      grid-template-columns: 100% 100%;
  }
  .title-with-price p:last-child{
    text-align: left;
  }
  .img-info-both{
    align-items: flex-start;
  }
}
@media (max-width: 600px){
  #detail_slider .banner-title h1 {
    font-size: 20px;
  }
  .left-wrapper h2.color1 {
    font-size: 21px;
    margin-bottom: 0PX;
  }
  .left-wrapper p {
    margin-bottom: 0PX;
  }
  .right-wrapper h1 {
    margin-top: 0px !important;
    font-size: 15px;
    display: inline-block;
  }
  .right-wrapper P {
    display: inline-block;
    font-size: 17PX;
  }
}
@media (max-width: 425px) {
  .left-wrapper p {
    margin-bottom: 3px;
    font-size: 12px;
  }
  .left-wrapper {
    display: inline-block;
    width: 100%;
  }
  .left-wrapper h2.color1 {
    font-size: 18px;
    margin-bottom: 3px;
  }
.right-wrapper h1 {
    margin-top: 0px !important;
    display: inline-block;
    font-size: 13px;
  }
.right-wrapper p {
    display: inline-block;
    vertical-align: top;
    font-size: 13px;
  }

}
</style>
<div id="wrap" class="detail-page-wrapper" style="margin: -20px auto 0!important">
    <div class="hiddex-xs" style="min-height:70px"></div>
    <div class="hidden-xs no-padding">
    <input type="hidden" value= "<?php echo $transfer->maxPassenger ?>" id="maxPass"/>
    <input type="hidden" value= "<?php echo $transfer->transfer_type ?>" id="transType"/>
    <input type="hidden" value= "<?php echo $transfer->id ?>" id="service_id"/>
      <input type="hidden" value= "<?php echo $this->lang->lang() ?>" id="lang"/>
    <input type="hidden" value= "<?php echo $transfer->daysAvailability ?>" id="daysAvailability"/>
    <input type="hidden" value= "<?php echo $transfer->dateStart ?>" id="dateStart"/>
    <input type="hidden" value= "<?php echo $transfer->dateEnd ?>" id="dateEnd"/>
    	<?php $this->view('frontend/includes/booking_search_form'); ?>
    </div>
    <header class="tourHeader">
        <!-- ############## SLIDER START #################### -->
        <div class="tp-banner-container clearfix">
            <div class="tp-banner">
                <ul style="margin-bottom:0px !important;">
                    <?php if($transfer->cover != NULL && $transfer->cover != '' && $transfer->cover != ' '){
                      $image = 'uploads/transfer/'.$transfer->cover;
                    ?>
                      <li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
                          <img src="<?php echo base_url() . $image; ?>" alt="<?php echo  $transfer->itTitle; ?>" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazyload="<?php echo base_url() . $image; ?>">
                      </li>
                    <?php

                    }
                    else {
                      $image = 'assets/images/slider.jpg'; ?>
                      <li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
                          <img src="<?php echo base_url() . $image; ?>" alt="<?php echo  $transfer->itTitle; ?>" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazyload="<?php echo base_url() . $image; ?>">
                      </li>
                        <?php
                    }

                    ?>
                </ul>
            </div>
            <div id="detail_slider" class="main-page-title">
                <div class="container">
                    <div class="row paddingtop50">
                        <div class="col-md-8 banner-title">
                            <h1 class="main-title"><?php echo  $transfer->itTitle; ?></h1>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- ############## SLIDER END #################### -->
    </header>

    <div class="container">

        <div class="row paddingtop10 paddingbot40">
          <div class="col-md-8 col-xs-12">
            <div class="well well-small" style="background-color: #fff !important;border-radius: 0px !important;">
              <div class="col-xs-12 no-padding" style="margin-top:20px;">
                <div class="img-info-both col-xs-12 no-padding">
                  <div class="img-div text-center" style="padding:0px">
                    <?php $imagePath = 'uploads/transfer/'.$transfer->thumbnail; ?>
                    <?php if ($transfer->thumbnail != null && $transfer->thumbnail != '' && $transfer->thumbnail != ' '): ?>
                      <img style="width: 60px !important;height: 60px;object-fit: cover; border-radius:30px" src="<?php echo base_url() . $imagePath; ?>" class="project-img " alt="<?php echo $transfer->itTitle;  ?>">
                    <?php else: ?>
                      <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                      <img style="width: 60px !important;height: 60px;object-fit: cover; border-radius:30px" class="project-img " src="<?php echo $scr; ?>" alt="<?php echo $transfer->itTitle; ?>">
                    <?php endif; ?>

                  </div>
                  <div class="place-info pr-0">
                      <div class="left-wrapper">
                        <div class="title-with-price">
                          <div>
                            <p>Transfer organised by</p>
                            <a href=""><h2 class="color1"> <strong><?php echo $transfer->business_title; ?></strong></h2></a>
                          </div>
                          <p><span>Starting from</span><?= $transfer->startPrice; ?>$ </p>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-12 no-padding-xs">  
                </div>
              </div> 
              <div class="no-padding-xs col-xs-12"  style="margin-top:50px;">


                <ul class="list-unstyled">
                    <li><i class="fa fa-clock-o color1"></i><strong><?php echo lang('duration'); ?></strong><p class="pull-right"><?php echo $transfer->duration; ?></p></li>
                    <li><i class="fa fa-users color1"></i><strong>Max Passeggeri</strong><p class="pull-right"><?php echo $transfer->maxPassenger; ?></p></li>
                    <li><i class="fa fa-car color1"></i><strong>Tido Auto</strong><p class="pull-right"><?php echo $cars_type[$transfer->car_type]?></p></li> 
                    <!-- <li><i class="fa fa-ship color1"></i><strong>Tipo Nave</strong><p class="pull-right"><?php echo $boats_type[$transfer->boat_type]?></p></li> -->
                    <li><i class="fa fa-suitcase color1"></i><strong>Tipo di trasferimento</strong><p class="pull-right"><?php echo $transfers_type[$transfer->transfer_type]?></p></li>
                    <!-- <li><i class="fa fa-exchange color1"></i><strong>Tipo di viaggio</strong><p class="pull-right"><?php echo $transfer->trip_type?></p></li> -->
                    <!-- <li><i class="fa fa-plane color1"></i><strong>Aeroporti</strong><p class="pull-right">SJD - San José del Cabo , CSL - Cabo San Lucas</p></li> -->
                    <li>
                      <i class="fa fa-plane color1"></i><strong>Aeroporti</strong>
                        <p class="pull-right">
                          <?php
                            $explodeData = explode(",", $transfer->pickup);
                            if(in_array("5", $explodeData) && in_array("6", $explodeData))
                              echo "SJD - San José del Cabo , CSL - Cabo San Lucas";
                            else if(in_array("5", $explodeData))
                              echo "SJD - San José del Cabo";
                            else if(in_array("6", $explodeData))
                              echo "CSL - Cabo San Lucas";
                          ?>
                        </p>
                    </li>
                </ul>
                </div>
                 <div class="well-heading">Informazioni</div>
                <div class="well-content">
                 <p><?php echo base64_decode($transfer->itDescription); ?></p>
                </div>

                <div class="well-heading"><?php echo lang('included'); ?></div>
                <div class="well-content">
                  <p><?php echo base64_decode($transfer->itServicesIncluded); ?></p>
                </div>

                <div class="well-heading"><?php echo lang('not_incuded'); ?></div>
                <div class="well-content">
                  <p><?php echo base64_decode($transfer->itServicesNotIncluded); ?></p>
                </div>

                <div class="well-heading"><?php echo lang('policy'); ?></div>
                <div class="well-content">
                  <p><?php echo base64_decode($transfer->itServicesNotIncluded); ?></p>
                </div>
            </div>

          </div>
          <aside class="col-md-4 col-xs-12">
        		<div class="box_style_1 expose">
        			<h3 class="inner">- <?php echo lang('booking'); ?> -</h3>
        			<div class="row">
        				<div class="col-md-12 col-sm-12">
        					<div class="form-group">
                    <label class="manage-label-line fa fa-calendar"> Data</label>
                    <input type="text" name="dateStart" placeholder="MM/DD/YYYY" value="" class="form-control datepicker" id="date" autocomplete="off">
                  </div>
        				</div>
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                   <label><i class="fa fa-clock-o"></i> <?php echo lang('time'); ?></label>
                    <input id="time" required class="time-pick form-control" value="12:00" type="text">
                  </div>
                </div>
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                  <label class="manage-label-line">Numero de volo/treno</label>
                      <input id="flight_train_no" class="form-control" name="flight_train_no" type="text">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <label><?php echo lang('adults'); ?></label>
                    <div class="adults numbers-row">
                      <input type="text" value="1" class="qty2 form-control" name="quantity" id="adults">
                      <div class="inc button_inc">+</div><div class="dec button_inc">-</div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <label><?php echo lang('child'); ?></label>
                    <div class="child numbers-row">
                      <input type="text" value="0" class="qty2 form-control" name="quantity" id="children">
                      <div class="inc button_inc">+</div><div class="dec button_inc">-</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                      <label>Indirizzo partenza</label>
                      <select id="address" class="form-control" name="address">
                      <option value="">Scegli la tua partenza</option>
                      <?php 
                        $pick = explode(",", $transfer->pickup);
                        foreach ($pick_up_points as $key => $pick_up) :
                          if (in_array($key, $pick)) {
                          echo "<option value='".$key."'>".$pick_up."</option>";
                          }
                        endforeach;
                      ?>
                      </select>
                    </div>
                  </div>
                <div class="col-md-12 col-sm-12 dropoff">
                  <div class="form-group">
                      <label>Zona di arrivo</label>
                          <select id="address_2" class="form-control" name="drop_off">
                            <option value=""><?= lang('choose_your_accommodation'); ?></option>
                          </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 sjd_price" style="display: none;">
                  <div class="form-group">
                      <label>Zona di arrivo</label>
                          <select id="address_3" class="form-control" name="sjd_price">
                            <option value=""><?= lang('choose_your_accommodation'); ?></option>
                            <?php
                            foreach ($zone as $key => $value) :
                            echo "<option value='".$value->sjd_price."'>".$value->zone_name."</option>";
                            endforeach;
                            ?>
                          </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 csl_price" style="display: none;">
                  <div class="form-group">
                      <label>Zona di arrivo</label>
                          <select id="address_4" class="form-control" name="csl_price">
                            <option value=""><?= lang('choose_your_accommodation'); ?></option>
                            <?php
                            foreach ($zone as $key => $value) :
                            echo "<option value='".$value->csl_price."'>".$value->zone_name."</option>";
                            endforeach;
                            ?>
                          </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                      <input id="text" class="form-control" name="text" type="text" placeholder="Enter The Address">
                  </div>
                </div>
        		</div>
              <a class="btn_collapse" data-toggle="collapse" href="#collapseForm" aria-expanded="false" aria-controls="collapseForm">
              <i class="fa fa-plus-circle"></i> Return</a>
              <small>(Optionally)</small>
        			 <div class="collapse" id="collapseForm">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <div class="form-group">
                        <label class="manage-label-line fa fa-calendar"> Data</label>
                        <input type="text" name="dateStart" placeholder="MM/DD/YYYY" value="" id="returnDate" class="form-control datepicker" autocomplete="off">
                      </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                      <div class="form-group">
                       <label><i class="fa fa-clock-o"></i> <?php echo lang('time'); ?></label>
                        <input id="returnTime" required class="time-pick form-control" value="12:00" type="text">
                      </div>
                    </div>
                     <div class="col-md-12 col-sm-12">
                      <div class="form-group">
                      <label class="manage-label-line">Numero de volo/treno</label>
                          <input id="return_flight_train_no" class="form-control" name="flight_train_no" type="text">
                      </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                      <div class="form-group">
                        <label>Indirizzo partenza</label>
                        <select id="address_return" class="form-control" name="drop_off" >
                          <option value=""><?= lang('choose_your_accommodation'); ?></option>
                            <?php
                            $drop = explode(",", $transfer->dropoff);
                            foreach ($drop as $key => $drop) :
                            echo "<option value='".$drop."'>".$drop."</option>";
                            endforeach;
                            ?>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                      <div class="form-group">
                        <label>Indirizzo arrivo</label>
                        <select id="address_return_2" class="form-control" name="address">
                      <?php 
                        $pick = explode(",", $transfer->pickup);
                        foreach ($pick_up_points as $key => $pick_up) :
                          if (in_array($key, $pick)) {
                          echo "<option value='".$key."'>".$pick_up."</option>";
                          }
                        endforeach;
                      ?>
                      </select>
                      </div>
                    </div>  
                  </div>
                </div>  
                <div class="form-group">
                  <label for="comment">Special Request</label>
                  <textarea class="form-control user-success" rows="5" id="special_request" autocomplete="off"></textarea>
                </div>
        			<br>
        			<table class="table table_summary">
        			<tbody>
        			<tr>
        				<td>
        					<?php echo lang('adults'); ?>
        				</td>
        				<td id="adults-n" class="text-right">
        					
        				</td>
        			</tr>
        			<tr>
        				<td>
        					<?php echo lang('child'); ?>
        				</td>
        				<td id="children-n" class="text-right">
        					
        				</td>
        			</tr>
        			<tr>
        				<td>
        					<?php echo lang('total'); ?>
        				</td>
        				<td id="total-n" class="text-right">
        					
        				</td>
        			</tr>
        			<tr class="total">
        				<td>
        					<?php echo lang('total'); ?>
        				</td>
        				<td id="price-n" class="text-right">
        					
        				</td>
        			</tr>
        			</tbody>
        			</table>
        			<a class="btn_full" id="addTo"  onClick="sendBooking()"><?php echo lang('book_now'); ?></a>
        		</div><!--/box_style_1 -->
            </aside>

          </div>
        </div>
    </div>
</section>

<section class="bottom-section gray">
    <div class="container">
        <div class="row">
            <?php if($blocks) {?>
              <section class="page-content paddingtop40 paddingbot40 home-page sec-4" style="background-color:#FBFBFB">
                  <div class="container">
                      <div class="row paddingtop10">
                          <?php foreach ($blocks as $block): ?>
                        <div class="col-md-3">
                              <a href="<?php echo $block->link; ?>">

                                  <div class="overview-item">
                                      <div class="overview-image">
                                          <img src="<?php echo base_url(); ?>uploads/block/<?php echo $block->p_image; ?>" alt="<?php echo $block->title; ?>" class="lazy2 img-responsive img-rounded">
                                      </div>
                                      <h3><?php echo $block->title; ?></h3>
                                      <p class="index-item-meta"><?php echo base64_decode($block->description); ?></p>
                                  </div>
                              </a>
                          </div>
                            <?php endforeach; ?>
                      </div>
                  </div>
              </section>
            <?php } ?>
        </div>
    </div>
</section>
</div>
<div id="push" style="height:0px !important"></div>
