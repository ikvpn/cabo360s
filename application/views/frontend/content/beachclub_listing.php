<!--################ WRAP START ################-->
<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <div class="hidden-xs no-padding">
    	<?php $this->view('frontend/includes/booking_search_form'); ?>
    </div>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 paddingbot20">
                    <ul class="breadcrumbs list-inline">
                        <li>
                            <span class="glyphicon glyphicon-home"></span>
                        </li>
                        <li><a href="<?php echo base_url().$this->lang->lang(); ?>">Procida</a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li><a href="#"><?php echo lang("Beach Clubs");?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                        $count = count($shops);
                        $count = $count + (isset($othersActivity) ? count($othersActivity) : 0);
                      ?>
                      <h1 ><?php echo $count; ?> <?php echo lang('Beach Clubs') ?> </h1>
                </div>
                <div class="col-md-8 paddingtop10">
                    <div class="row">
                       	<script type="text/javascript">
                          var markers = [];
                        </script>
                        <div class="col-md-12 paddingtop20">
                             <?php if (isset($shops) && is_array($shops)): ?>
                                <?php if ($count > 0): ?>
                                    <!-- listing item -->
                                    <?php foreach ($shops as $item): ?>
                                        <?php $item_array = (array) $item; ?>
                                    					<script type="text/javascript">
                                               markers.push(['<?php  echo addslashes($item_array[$this->lang->lang() . 'Name']); ?>', <?php echo $item_array['lat'] ?>, <?php echo $item_array['lng'] ?>]);
                                        </script>
                                        <div class="listing-item"  data-tag="<?php echo $item->tags ?>" data-services="<?php echo $item->activity_services ?>">
                                            <div class="row">
                                                <div class="col-md-5 col-xs-6">
                                                    <a href="<?php echo base_url().$this->lang->lang().'/'.lang('what-to-do').'/'.lang('beach-clubs').'/'.$item->pageTitle.''; ?>">
                                                      <div class="listing-image">
                                                          <?php if ($item->photo != null && $item->photo != ''): ?>
                                                            <img src="<?php echo base_url() . "uploads/beach_clubs/" . $item->photo; ?>" class="img-responsive img-rounded" alt="<?php echo $item_array[$this->lang->lang() . 'Name']; ?>">
                                                          <?php else: ?>
                                                            <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                                                            <img class="img-responsive img-rounded" src="<?php echo $scr; ?>" alt="<?php echo $item_array[$this->lang->lang() . 'Name']; ?>">
                                                          <?php endif; ?>
                                                      </div>
                                                    </a>
                                                </div>
                                                <div class="col-md-7 col-xs-6">
                                                    <h3 class="listing-title"><?php echo $item_array[$this->lang->lang() . 'Name']; ?></h3>
                                                    <h4 class="listing-sub-title" style="text-transform: capitalize;"><?php echo $item->cuisine;  ?></h4>
                                                    <p><?php echo strlen(base64_decode($item_array[$this->lang->lang() . 'Description'])) > 50 ? substr(base64_decode($item_array[$this->lang->lang() . 'Description']), 0, 50) . "..." : base64_decode($item_array[$this->lang->lang() . 'Description']); ?>..</p>
                                                    <div class="listing-details shopping">
                                                        <div class="row">
                                                            <div class="col-md-9">
                                                                <p class="listing-location">
                                                                    <span class="color6 picon picon-map-marker"></span>&nbsp; <?php echo $this->Beachclub_model->getLocationName($this->lang->lang(),$item->order_id); ?></p>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <a href="<?php echo base_url().$this->lang->lang().'/'.lang('what-to-do').'/'.lang('beach-clubs').'/'.$item->pageTitle.''; ?>" class="btn btn-block btn-danger btn-search btn-xs"><?php echo lang('details'); ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php elseif (count($othersActivity) <= 0 && count($shops) <= 0): ?>
                                    <div class="alert alert-warning">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <?php echo lang('Shops_Not_Found_Msg'); ?>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php if (isset($othersActivity) && is_array($othersActivity)): ?>
                                <?php if (count($othersActivity) > 0): ?>
                                    <?php foreach ($othersActivity as $item): ?>
                                        <?php $item_array = (array) $item; ?>
                                        <script type="text/javascript">
                                                markers.push(['<?php  echo addslashes($item_array[$this->lang->lang() . 'Name']); ?>', <?php echo $item_array['lat'] ?>, <?php echo $item_array['lng'] ?>]);
                                        </script>
                                        <!-- listing item -->
                                          <?php  $langURL = lang(str_replace('_', '-', $item->urlType));
                                          $type = '';
                                          switch ($item->categoryID) {
                                            case 5:
                                              $type = 'holiday-house';
                                              break;
                                            case 2:
                                              $type = 'restaurant';
                                              break;
                                            case 4:
                                              $type = 'shopping';
                                              break;
                                            case 7:
                                              $type = 'bars-and-cafe';
                                              break;
                                            case 8:
                                              $type = 'rentals';
                                              break;
                                            case 9:
                                              $type = 'wedding';
                                              break;
                                            case 10:
                                              $type = lang('beach-clubs');
                                              break;
                                            case 11:
                                              $type = 'nightlife';
                                              break;
                                            case 12:
                                              $type = lang('act_services');;
                                              break;
                                            default:
                                              break;
                                          }
                                          ?>
                                          <div class="listing-item"  data-tag="<?php echo $item->tags ?>" data-services="<?php echo $item->activity_services ?>">
                                              <div class="row">
                                                    <?php if ($item->categoryID == 1): ?>
                                                      <a href="<?php echo base_url().$this->lang->lang(); ?>/hotel-detail/<?php echo $item->id; ?>/<?php echo str_replace('&','', str_replace(' ', '-',$item->enName)); ?>">
                                                        <div class="col-md-5">
                                                          <div class="listing-image">
                                                              <?php if ($item->photo != null && $item->photo != ''): ?>
                                                                <img src="<?php echo base_url() . "uploads/hotels/" . $item->photo; ?>" class="img-responsive img-rounded" alt="<?php echo $item_array[$this->lang->lang() . 'Name']; ?>">
                                                              <?php else: ?>
                                                                <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                                                                <img class="img-responsive img-rounded" src="<?php echo $scr; ?>" alt="<?php echo $item_array[$this->lang->lang() . 'Name']; ?>">
                                                              <?php endif; ?>
                                                          </div>
                                                        </div>
                                                      </a>
                                                    <?php else: ?>
                                                        <a href="<?php echo base_url() . $this->lang->lang()."/". $langURL."/".$type."/" . $item->pageTitle ?>">
                                                          <div class="col-md-5">
                                                            <div class="listing-image">
                                                          <?php if ($item->photo != null && $item->photo != ''): ?>
                                                            <img src="<?php echo base_url() . "uploads/".$item->categoryName."/" . $item->photo; ?>" class="img-responsive img-rounded" alt="<?php echo $item_array[$this->lang->lang() . 'Name']; ?>">
                                                          <?php else: ?>
                                                            <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                                                            <img class="img-responsive img-rounded" src="<?php echo $scr; ?>" alt="<?php echo $item_array[$this->lang->lang() . 'Name']; ?>">
                                                          <?php endif; ?>
                                                        </div>
                                                      </div>
                                                        </a>
                                                    <?php endif; ?>
                                                  <div class="col-md-7 col-xs-6">
                                                      <h3 class="listing-title"><?php echo $item_array[$this->lang->lang() . 'Name']; ?></h3>
                                                      <?php if (isset($item_array[$this->lang->lang() . 'Slogan'])): ?>
                                                          <h4 class="listing-sub-title"><?php echo $item_array[$this->lang->lang() . 'Slogan']; ?></h4>
                                                      <?php endif; ?>
                                                      <div class="listing-details">
                                                          <div class="row">
                                                            <div class="col-md-9">
                                                              <p class="listing-location">
                                                                  <span class="color6 picon picon-map-marker"></span>&nbsp; <?php echo $item->locationName; ?></p>
                                                            </div>
                                                              <div class="col-md-3">
                                                                  <?php if ($item->categoryID == 1): ?>
                                                                    <a href="<?php echo base_url().$this->lang->lang(); ?>/hotel-detail/<?php echo $item->id; ?>/<?php echo str_replace('&','', str_replace(' ', '-',$item->enName)); ?>" class="btn btn-block btn-danger btn-search btn-xs"><?php echo lang("details"); ?></a>
                                                                  <?php else: ?>
                                                                    <a href="<?php echo base_url() . $this->lang->lang()."/".$langURL."/".$type."/" . $item->pageTitle ?>" class="btn btn-block btn-danger btn-search btn-xs"><?php echo lang("details"); ?></a>
                                                                  <?php endif; ?>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-12 paddingtop20">
                            <?php $add = $this->Ads_model->get('468X15'); ?>
                            <?php if ($add): ?>
                                <?php if ($add->src != ''): ?>
                                    <?php if ($add->type == 'image'): ?>
                                        <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                    <?php else: ?>
                                        <?php echo base64_decode($add->src); ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                         <div class="col-md-12">
                         </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="sidebar-element">
                            <div class="well well-small well-filter">
                              <div class="title nopadding"><?php echo lang('Filters'); ?></div>
                              <div class="form-group" style="margin-top:20px;">
                                  <select class="form-control input-sm" name="tags" id="tags">
                                    <option value="-1"><?php echo lang('Tags'); ?></option>
                                    <?php foreach ($all_tags as $key => $tag): ?>
                                      <option value=<?php echo $tag->id; ?>> <?php if ($this->lang->lang() == 'en') { echo $tag->nameEN; } else {echo $tag->nameIT;} ?></option>
                                    <?php endforeach; ?>
                                  </select>
                              </div>
                              <div class="form-group">
                                <select class="form-control input-sm" name="services" id="services">
                                  <option value="-1"><?php echo lang('Activity_services'); ?></option>
                                  <?php foreach ($all_services as $key => $service): ?>
                                    <option value=<?php echo $service->id; ?>> <?php if ($this->lang->lang() == 'en') { echo $service->nameEN; } else {echo $service->nameIT;} ?></option>
                                  <?php endforeach; ?>
                                </select>
                              </div>
                            </div>
                          </div>
                      </div>
                        <div class="col-md-12">
                            <div class="sidebar-element">
                                <div id="map-canvas-sidebar" class="img-rounded"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="well well-small text-center">
                                <?php $add = $this->Ads_model->get('300X250'); ?>
                                <?php if ($add): ?>
                                    <?php if ($add->src != ''): ?>
                                        <?php if ($add->type == 'image'): ?>
                                            <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                        <?php else: ?>
                                            <?php echo base64_decode($add->src); ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?php $this->view('frontend/includes/sidebar_widget_upcoming_events'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bottom-section gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>

                        <?php endif; ?>

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
