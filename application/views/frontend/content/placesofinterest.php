<!--################ WRAP START ################-->
<div id="wrap" class="detail-page-wrapper placeinterest" style="margin: -20px auto 0!important">
    <div class="menu-bg" ></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <?php $place_array = (array) $place; ?>
    <section class="map-section place-of-interest">
        <div id="map-canvas-header"></div>
        <div class="map-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?php echo $place_array[$this->lang->lang() . 'Title']; ?></h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- <div id="map-canvas-sidebar"></div> -->

    <div class="container">
        <div class="row paddingtop20">
            <div class="col-md-12 paddingbot20">
                <ul class="breadcrumbs list-inline">
                    <li>
                        <span class="glyphicon glyphicon-home"></span>
                    </li>
                    <li><a href="#">Procida</a>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </li>
                    <li><a href="<?php echo base_url().$this->lang->lang().'/'.lang('places-of-interest').'' ?>"><?php echo lang("Places_Of_Interest"); ?></a>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </li>
                    <li><a href="#"><?php echo $place_array['enTitle']; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row paddingtop10 paddingbot40">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="image-shadow">
                            <img src="<?php echo base_url(); ?>uploads/places_of_interest/<?php echo $place->photo; ?>" class="img-responsive img-rounded ">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <?php $add = $this->Ads_model->get('468X60'); ?>
                        <?php if ($add): ?>
                            <?php if ($add->src != ''): ?>
                                <?php if ($add->type == 'image'): ?>
                                    <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                <?php else: ?>
                                    <?php echo base64_decode($add->src); ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <img src="http://placehold.it/468x60&text=ads+/+468+x+60" alt="..." class="paddingtop20">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/468x60&text=ads+/+468+x+60" alt="..." class="paddingtop20">
                        <?php endif; ?>
                        <div class="paddingtop20">
                            <?php echo base64_decode($place_array[$this->lang->lang() . 'Description']); ?>
                        </div>

                    </div>
                    <div class="col-md-12 paddingtop20">
                        <hr class="procida">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="detail-summary">
                                    <p>
                                        <i class="picon picon-location color1"></i>
                                        <?php $location = $this->Location_model->getLocation($place->location_id, null,'', $this->lang->lang()); ?>

                                        <?php $loc = (array) $location; ?>
                                        <span><?php echo $loc['title']; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="detail-summary">
                                    <p>
                                        <i class="picon picon-map-marker color1"></i>
                                        <span><?php echo $place->address; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="detail-summary">
                                    <p>
                                        <i class="fa fa-share-square-o  color1"></i>

                                        <span><?php echo $place->web; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="detail-summary">
                                    <p>
                                        <i class="fa fa-envelope  color1"></i>
                                        <span><?php echo $place->email; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="detail-summary">
                                    <p>
                                        <i class="fa fa-phone  color1"></i>
                                        <span><?php echo $place->phone ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="detail-summary">
                                    <p>
                                        <i class="picon picon-mobile color1"></i>
                                        <span><?php echo $place->mobile; ?></span>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-3 nopadding">
                <div class="well well-small">
                    <div class="well-heading">Info</div>
                    <ul class="list-unstyled">
                        <li><i class="picon picon-clock color1"></i><?php echo $place->timing; ?></li>
                        <li><i class="picon picon-calendar color1"></i><?php echo $place->openingDays=='Temporary closed'?lang("Temporary_closed"):$place->openingDays; ?></li>
                        <li><i class="picon picon-calendar color1 strike"></i><?php echo $place->closingDays=='Temporary closed'?lang("Temporary_closed"):$place->closingDays; ?></li>
                        <li><i class="picon picon-info color1"></i><?php echo $place->reservation == 1 ? lang("Reservation_Required") : lang("Public_Space"); ?></li>
                    </ul>
                </div>
                <div class="well well-small paddingtop10">
                    <?php if($place->pAdults == 0 && $place->pChilds == 0 && $place->pGuests == 0): ?>
                    <div class="well-heading"><?php echo lang("Free_Entry");?></div>
                    <?php $add = $this->Ads_model->get('234X60'); ?>
                        <?php if ($add): ?>
                            <?php if ($add->src != ''): ?>
                                <?php if ($add->type == 'image'): ?>
                                    <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                <?php else: ?>
                                <div class="text-center">
                                    <?php echo base64_decode($add->src); ?>
                                    </div>
                                <?php endif; ?>
                            <?php else: ?>
                                <img src="http://placehold.it/234X60&text=234+x+60" class="img-responsive" alt="..">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/234X60&text=234+x+60" class="img-responsive" alt="..">
                        <?php endif; ?>

                <?php else: ?>
                    <div class="well-heading"><?php echo lang("Ticket_Prices") ?></div>
                     <ul class="list-unstyled">
                        <li><?php echo lang("Adults") ?><span class="text-right"><?php echo $place->pAdults == 0 ? lang('Free') : $place->pAdults.' €'; ?></span>
                        </li>
                        <li><?php echo lang("Children") ?><span class="text-right"><?php echo $place->pChilds == 0 ? lang('Free') : $place->pChilds.' €'; ?></span>
                        </li>
                        <li><?php echo lang("Guest")?><span class="text-right"><?php echo $place->pGuests == 0 ? lang('Free') : $place->pGuests.' €'; ?></span>
                        </li>
                    </ul>
                <?php endif; ?>

                </div>
                <div class="well well-small">

                    <a href="#" class="btn btn-warning btn-lg btn-block btn-request btn-sendemail" data-orderid="<?php echo $place->id ?>" data-type="placesofinterest"><?php echo lang('Send_a_request'); ?><span class="glyphicon glyphicon-chevron-right"></span></a>

                </div>

            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <?php $add = $this->Ads_model->get('300X250'); ?>
                        <?php if ($add): ?>
                            <?php if ($add->src != ''): ?>
                                <?php if ($add->type == 'image'): ?>
                                    <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                <?php else: ?>
                                    <?php echo base64_decode($add->src); ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                        <?php endif; ?>
                    </div>
                    <?php if (isset($other_places) && is_array($other_places) && count($other_places)>0): ?>
                        <div class="col-md-12">
                            <div class="sidebar-element">
                                <h4 class="sidebar-title">Other Sites</h4>
                                <ul class="list-unstyled sidebar-website sidebar-sight">
                                    <?php foreach ($other_places as $item): ?>
                                        <?php $item_array = (array) $item; ?>
                                        <li><a href="<?php echo base_url() . $this->lang->lang()  . '/' . lang('places-of-interest') . '/' . str_replace(' ', '-', strtolower($item_array['rewrite_url_'.$this->lang->lang()])); ?>"><?php echo $item_array[$this->lang->lang() . 'Title'] ?></a><i class="fa fa-angle-right"></i></li>

                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="nearby-areas margintop40">
    <div class="container">
        <div class=" paddingtop20">
            <div class="col-md-12">
                <h4 class="bold nomargin">Near By Areas</h4>
            </div>
        </div>
    </div>
    <?php if (is_array($near_locations) && count($near_locations)): ?>
            <div id="nearby-areas-carousel" class="carousel slide" data-ride="carousel">
                <div class="container">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php $starter = 0; ?>
                        <?php $active = TRUE; ?>
                        <?php foreach ($near_locations as $loc): ?>
                            <?php if ($loc->id == $location->id):continue;
                            endif;
                            ?>
                            <?php if ($starter == 0): ?>
                                <?php echo $active == TRUE ? "<div class='item  active'>" : "<div class='item'>"; ?>
                                <?php $active = false; ?>
        <?php endif; ?>
                            <div class="col-md-4">
                                <div class="overview-item">
                                    <div class="overview-image">
                                        <div class="project">
                                            <img src="<?php echo base_url(); ?>uploads/locations/<?php echo $loc->p_image; ?>" style="height:220px" alt="" class="img-responsive img-rounded">
                                            <span class="overlay"></span>
                                            <div class="cnt">
                                                <a href="<?php echo base_url() . $this->lang->lang() . '/'. lang('what-to-visit') .'/' . str_replace(' ', '-', strtolower($loc->title)); ?>" class="btn btn-purity"><?php echo lang('View_details'); ?></a>
                                            </div>
                                        </div>

                                    </div>
                                    <h3><?php echo $loc->title ?></h3>
                                    <p>
                                        <!--<a href="<?php /*echo base_url() . $this->lang->lang() .'/'.lang('events').'/upcoming-events'*/?>"><?php /*echo $this->Location_model->count_orders_on_location_id($loc->id, 6); */?> <?php /*echo lang('Events'); */?></a>,-->
                                        <a href="<?php echo base_url() . $this->lang->lang().'/'.lang('what-to-do').'/shopping' ?>"><?php echo $this->Location_model->count_orders_on_location_id($loc->id, 4); ?> <?php echo lang('Shops'); ?></a>,
                                        <a href="<?php echo base_url() . $this->lang->lang().'/'.lang('eat-and-drink'). '/resturants'?>"><?php echo $this->Location_model->count_orders_on_location_id($loc->id, 2); ?> <?php echo lang('Restaurants'); ?></a>,
                                        <a href="<?php echo base_url() . $this->lang->lang().'/' . lang('where-to-stay') . '/hotels'; ?>"><?php echo $this->Location_model->count_orders_on_location_id($loc->id, 1); ?> <?php echo lang('Hotels') ?></a>
                                    </p>
                                </div>
                            </div>
                            <?php $starter++; ?>
                            <?php if ($starter == 3): ?>
                                <?php echo "</div>";
                                $starter = 0;
                                ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <?php if ($starter > 0 && $starter < 3): ?>
        <?php echo"</div>"; ?>
    <?php endif; ?>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#nearby-areas-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#nearby-areas-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
<?php endif; ?>

</section>
<section class="bottom-section gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <?php $add = $this->Ads_model->get('970X90'); ?>
                <?php if ($add): ?>
                    <?php if ($add->src != ''): ?>
                        <?php if ($add->type == 'image'): ?>
                            <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                        <?php else: ?>
                            <?php echo base64_decode($add->src); ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                <?php else: ?>
                    <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
</div>
<div id="push"></div>
