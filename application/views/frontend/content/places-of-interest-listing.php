<script type="text/javascript">

    var markers = [];

</script>

<!--################ WRAP START ################-->

<div id="wrap" class="detail-page-wrapper index" style="margin: -20px auto 0!important">

    <div style="height:76px"></div>

    <?php $this->view('frontend/includes/booking_search_form'); ?>

    <section class="page-content">

        <div class="container">

            <div class="row">

                <div class="col-md-12 paddingbot20">

                    <ul class="breadcrumbs list-inline">

                        <li>

                            <span class="glyphicon glyphicon-home"></span>

                        </li>

                        <li><a href="<?php echo base_url() . $this->lang->lang(); ?>">Procida</a>

                        </li>

                        <li>

                            <span class="glyphicon glyphicon-chevron-right"></span>

                        </li>

                        <li><a href="#"><?php echo lang("Places_of_Interest"); ?></a>

                        </li>

                    </ul>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12">

                    <h2 class="sub-title"><?php echo lang('Places_of_Interest'); ?></h2>

                </div>

            </div>

            <div class="row paddingtop10"> 

                <?php if (isset($m_poi) && !empty($m_poi)): ?>

                    <?php foreach ($m_poi as $poi): ?>

                        <?php $poi_array = (array) $poi; ?> 

                        <script type="text/javascript">

                            markers.push(['<?php echo addslashes($poi_array[$this->lang->lang() . 'Title']); ?>', <?php echo $poi->lat ?>, <?php echo $poi->lng ?>]);

                        </script>

                        <div class="col-md-3">

                            <a href="<?php echo base_url() . $this->lang->lang() . '/' . str_replace(' ', '-', strtolower(lang('places-of-interest'))) . '/' . str_replace(' ', '-', strtolower($poi_array["rewrite_url_" . $this->lang->lang()])) . ''; ?>">

                                <div class="overview-item poi ">

                                    <div class="overview-image o-i-poi">

										<img class="img-responsive lazy2" alt="" src="<?php echo base_url() . "uploads/places_of_interest/" . $poi->photo; ?>" />

                                    </div>

                                    <h3><?php echo$poi_array[$this->lang->lang() . "Title"]; ?></h3> 

                                    <p>

                                        <?php $location = $this->Location_model->getLocation($poi->location_id, null, '', $this->lang->lang()); ?>

                                        <?php $loc_array = (array) $location; ?>

                                        <i class="fa fa-map-marker color1"></i>  <a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('what-to-visit') . '/' . $loc_array['rewrite_url_' . $this->lang->lang()] . '' ?>"><?php echo $location->title ?></a>

                                        <?php

                                        if ($poi->pAdults == 0 && $poi->pChilds == 0 && $poi->pGuests == 0) {



                                            if ($this->lang->lang() == 'en') {

                                                ?>

                                            <div class="poi-flag-free"> Free </div>



                                            <?





                                            }

                                            else{

                                            ?>

                                            <div class="poi-flag-free"> Gratis </div>

                                            <?php

                                        }

                                        ?>











                                        <?php

                                    }

                                    ?>



                                    </p>

                                </div>

                            </a>

                        </div>

    <?php endforeach; ?>

<?php else: ?>



                <?php endif; ?>

            </div>

        </div>

    </section>





    <section class="map-section">

        <div id="map-canvas"></div>

        <div class="container"> 

        </div>

    </section> 



    <section class="bottom-section hidden-xs hidden-sm">



        <div class="container">

            <div class="row">

                <div class="col-md-12 text-center">

<?php $add = $this->Ads_model->get('970X90'); ?>

<?php if ($add): ?>

    <?php if ($add->src != ''): ?>

                            <?php if ($add->type == 'image'): ?>

                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">

                            <?php else:

                                ?>

                                <?php echo base64_decode($add->src); ?>

                            <?php endif; ?>

                        <?php else:

                            ?>

                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">

                        <?php endif; ?>

                    <?php else:

                        ?>

                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">

                    <?php endif; ?>

                </div>

            </div>

        </div>

    </section>

</div>

<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->

<div id="push"></div>



