<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <header class="background-header traditional-events-header paddingtop100 paddingbot20 header-common-height" style="background-image:url('<?php echo base_url('assets/images/traghetti-procida.jpg'); ?>');">

      <div class="main-page-title">
              <div class="container">
                  <div class="row paddingtop50">
                      <div class="col-md-8 banner-title">
                          <h1 class="main-title ml9" style="bottom: 30px !important;position: absolute;left: 20px;">
                          <?php if ($this->lang->lang() == "en"): ?>Timetables <?php echo $route; ?>
                          <?php elseif ($this->lang->lang() == "it"): ?>Orari <?php echo $route; ?>
                          <?php endif; ?>
                          </h1>
                      </div>
                  </div>
              </div>
          </div>
    </header>
    <div class="hidden-xs">
	    <?php $this->view('frontend/includes/booking_search_form'); ?>
    </div>
    
    <section id="tableSection" class="page-content how-to-get-there"
    	<?php if(isset($iframe_url)){ echo 'style="margin-top:80px"'; } ?>>
        <div class="container no-padding-xs">
            <div class="row">
                <div class="col-md-12 text-center">
                  <?php if(isset($iframe_url)) { ?>
                    <iframe id="ferryFrame" src="https://www.traghettilines.it/steps.aspx?<?php echo $iframe_url; ?>&language=<?php echo $this->lang->lang() ?>&iframe_booking=true&AID=21290&UID=372732&UAID=372732" frameborder="0" scrolling="auto" height="1000"></iframe>
                  <?php } else { ?>
                    <form id="ferryroutes" method="get" action="<?php echo base_url().$this->lang->lang().'/info-utili/ferry-time-table'; ?>">
                      <div class="col-xs-12 no-padding">
                        <div class="col-md-4 col-xs-12 col-md-offset-1">
                          <div class="form-group">
                            <select name="route" class="form-control routeList">
                              <?php for($i=0; $i<count($routes); $i++) { 
                                 if (!in_array($routes[$i]->external_id, [319,318,1412,1413])) {
                                ?>
                                <option <?php if(isset($route)){ echo $route == $routes[$i]->descrizione ? 'selected="selected"' : ''; } ?> data-table="<?php echo $routes[$i]->id; ?>" value="<?php echo $routes[$i]->descrizione; ?>"><?php echo $routes[$i]->descrizione; ?></option>
                              <?php } ?>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4 col-xs-8">
                            <div class="form-group">
                              <div class="input-group">
                                <input type="text" class="form-control ferrypicker" value="<?php if(isset($date)){ echo $date; } ?>" name="date" placeholder="<?php echo lang('today'); ?>">
                                <div class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-4">
                          <button type="submit" class="btn btn-block btn-purity"><?php echo lang('search'); ?></button>
                        </div>
                      </form>
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content paddingtop10">
                      <?php for($i=0; $i<count($RoutesWithDailyDepartures); $i++){ ?>
                      
                      	<?php $active = '';
                      		
                      		if(isset($RoutesWithDailyDepartures[$i][0]->descrizione) && !empty($route)) {
                      			if($RoutesWithDailyDepartures[$i][0]->descrizione == $route){
                      				$active = 'active';
                      			}
                      		}
                      		else {
                      			if ($i == 0) {
                      				$active = 'active';
                      			}
                      		} 
                      	
                      	?>
                        <div role="tabpanel" class="tab-pane <?php echo $active; ?>" id="<?php if(isset($RoutesWithDailyDepartures[$i][0]->descrizione)) { echo $RoutesWithDailyDepartures[$i][0]->id; } ?>">
                            <div class="table-responsive">
                                <table class="table table-hover time-table">
                                    <thead>
                                      <?php if(empty($RoutesWithDailyDepartures[$i][0])){ ?>
                                        <tr>
                                            <th>Informazioni</th>
                                        </tr>
                                      <?php } else { ?>
                                        <tr>
                                            <th class="text-center"><?php echo lang('departure'); ?></th>
                                            <th class="text-center"><?php echo lang('duration'); ?></th>
                                            <th class="hidden-xs text-center"><?php echo lang('departure'); ?></th>
											                      <th class="hidden-xs text-center"><?php echo lang('destination'); ?></th>
                                            <th class="text-center"><?php echo lang('boat'); ?></th>
                                            <th class="text-center"><?php echo lang('company'); ?></th>
                                            <th class="text-center"><?php echo lang('price'); ?></th>
                                            <th class="text-center"><?php echo lang('book'); ?></th> 
                                        </tr>
                                      <?php } ?>
                                    </thead>
                                    <tbody>
                                      <?php foreach ($RoutesWithDailyDepartures[$i] as $departure) { ?>
                                        <?php if(empty($departure)) { ?>
                                          <tr>
                                              <td class="text-center"><?php echo lang('no_routes'); ?></td>
                                          </tr>
                                        <?php } else { ?>
                                          <tr class="font-14">
                                              <td class="time procidaGreen"><?php $time = explode(':', $departure->orapartenza); echo $time[0].':'.$time[1]; ?></td>
                                              <td><?php echo $departure->durata; ?> M</td>
                                              <td class="hidden-xs">
                                                <?php
                                                  // $portDeparture = $route ? explode('-', $route) : explode('-', $departure->descrizione);
                                                  $portDeparture = isset($departure->descrizione) ? explode('-', $departure->descrizione) : explode('-', $departure->route_name);

                                                  echo $portDeparture[0];
                                                ?>
                                              </td>
                                              <td class="hidden-xs">
                                                <?php
                                                  // $portDeparture = $route ? explode('-', $route) : explode('-', $departure->descrizione);
                                                  $portDeparture = isset($departure->descrizione) ? explode('-', $departure->descrizione) : explode('-', $departure->route_name);
                                                  echo $portDeparture[1];
                                                ?>
                                              </td>
                                              <td class="text-left ferry-align">
                                              <?php if($departure->autodisponibile) { ?>
                                              	<img src="<?php echo base_url('assets/images/noun_.svg'); ?>" style="height:40px" alt=""> <?php if ($this->lang->lang() == "en"): ?> Ferry <?php elseif ($this->lang->lang() == "it"): ?> Traghetto <?php endif; ?>
                                              <?php } else { ?>
	                                              <img src="<?php echo base_url('assets/images/noun.svg'); ?>" style="height:40px" alt=""> <?php if ($this->lang->lang() == "en"): ?> Hydrofoil <?php elseif ($this->lang->lang() == "it"): ?> Aliscafo <?php endif; ?>
                                              <?php } ?>
                                              </td>
                                              <td class="text-center"><img src="<?php echo $departure->urlLogoCompagnia; ?>" alt="..." class="img-responsive" /></td>
                                              <td>
                                                <?php echo number_format($departure->prezzo1pax,2); ?>&euro;
                                              </td>
                                              <td>
                                                <?php $disable = '';
                                                if (property_exists($departure, 'disabled') && $departure->disabled == true) {
                                                    $disable = 'disabled';
                                                }
                                                $dest_extId = $routeId['ferry_destination_external_id'];
                                                if (property_exists($departure, 'ferry_destination_external_id') ) {
                                                  $dest_extId = $departure->ferry_destination_external_id;
                                                }
                                                $andata = $departure->id;
                                                if (property_exists($departure, 'ext_id') ) {
                                                  $andata = $departure->ext_id;
                                                }

                                                ?>
                                              	<a href="<?php echo base_url().$this->lang->lang().'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'?url='; ?>andata=<?php echo $andata; ?>&ritorno=&direzione=<?php echo $dest_extId; ?>&language=<?php echo $this->lang->lang() ?>&iframe_booking=true&AID=21290&UID=372732&UAID=372732" class="btn btn-purity" <?php echo $disable; ?>><?php echo lang('book'); ?></a>
                                              	</td>
                                          </tr>
                                      <?php }} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                      <?php } ?>
                    </div>
                  <?php } ?>
                </div>
            </div>
    </section>
    <section class="bottom-section gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
<?php $add = $this->Ads_model->get('970X90'); ?>
<?php if ($add): ?>
    <?php if ($add->src != ''): ?>
        <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
            <?php else:
            ?>
            <?php echo base64_decode($add->src); ?>
        <?php endif; ?>
        <?php else:
        ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                        <?php else:
                        ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="push"></div>
<script>
var search = window.location.search;
// if(search.length <= 0){
//   $('.routeList').on('change', function(){
//     var tableToHide = $('#tableSection').find('.active').removeClass('active');
//     var tableToShow = $(this).find(':selected').attr('data-table');
//     $('#'+tableToShow).addClass('active');
//   });
// }

$('#ferryroutes').submit(function(event) {
	event.preventDefault();
	var date = $('.ferrypicker').val();
	console.log(date.length);

	if(date.length == 0){
		return toastr.warning('È necessario inserire la data per effettuare una ricerca');
	}

	return $('#ferryroutes')[0].submit();
})
</script>
