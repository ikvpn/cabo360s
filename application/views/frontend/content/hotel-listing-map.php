<style>
.hotelListing {
  max-height: 550px;
  overflow-y: scroll;
  min-height: 550px;
}

.searchHotels .hotel-listing-item {
    padding-bottom: 20px;
    min-height: 330px;
}

.hotelListing button {
  padding: 0 !important;
  background: none !important;
  border: 0 !important;
}
</style>
<div id="wrap" class="detail-page-wrapper">
  <div class="menu-bg"></div>
  <?php $this->view('frontend/includes/booking_search_form'); ?>
  <section class="page-content searchHotels paddingtop30">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="row">
                              <div class="col-md-6">
                                  <h2 class="sub-title"><?php echo count($fullHotels); ?> Hotel<?php if(count($fullHotels) > 1) { echo 's'; } ?> a Procida</h2>
                              </div>
                              <!--<div class="col-md-2">
                                  <div class="form-group">
                                      <select class="form-control input-sm">
                                          <option>Location</option>
                                          <option>2</option>
                                          <option>3</option>
                                          <option>4</option>
                                          <option>5</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-2">
                                  <div class="form-group">
                                      <select class="form-control input-sm">
                                          <option>Typology</option>
                                          <option>2</option>
                                          <option>3</option>
                                          <option>4</option>
                                          <option>5</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-2">

                                  <div class="form-group">
                                      <select class="form-control input-sm">
                                          <option>Rating</option>
                                          <option>2</option>
                                          <option>3</option>
                                          <option>4</option>
                                          <option>5</option>
                                      </select>
                                  </div>
                              </div>-->
                          </div>
                      </div>
                      <div class="col-sm-6 col-xs-12 paddingtop10 hotelListing">
                          <div class="row">
                              <?php for($i=0; $i<count($fullHotels); $i++) { ?>
                                <div class="col-md-6 col-xs-12">
                                    <div class="hotel-listing-item">
                                        <a href="<?php echo base_url().$this->lang->lang(); ?>/hotel-detail/<?php echo $fullHotels[$i]->id; ?>/<?php echo str_replace('&','', str_replace(' ', '-',$fullHotels[$i]->enName)); ?>">
                                          <div class="hotel-listing-image">
                                              <img src="<?php echo base_url('uploads/hotels'); ?>/<?php echo $fullHotels[$i]->photo; ?>" class="img-responsive img-rounded" alt="...">
                                              <div class="hotel-listing-price">
                                                  <strong>da € <?php echo intval($fullHotels[$i]->price); ?> / <?php echo $fullHotels[$i]->unit; ?></strong>
                                              </div>
                                          </div>
                                        </a>
                                        <div class="hotel-listing-details">
                                            <h3 class="hotel-listing-title"><?php echo $fullHotels[$i]->itName; ?></h3>
                                            <p>
                                              <?php
                                              if(isset($fullHotels[$i]->hbb_info->stars)  && intval($fullHotels[$i]->hbb_info->stars) < 6) {
                                                for($j=0; $j<intval($fullHotels[$i]->hbb_info->stars); $j++) { ?>
                                                <span class="fa fa-star color4"></span>
                                              <?php }} ?> &nbsp; <!--<span class="reviews">(118 Reviews)</span>-->
                                            </p>
                                        </div>
                                    </div>
                                </div>
                              <?php } ?>
                          </div>
                      </div>
                      <div class="col-sm-6 col-xs-12 paddingtop20 no-padding map-section">
                        <?php echo $map['html']; ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <?php $add = $this->Ads_model->get('970X90');?>
  <?php if ($add): ?>
    <?php if ($add->src != ''): ?>
      <section class="bottom-section gray hidden-xs hidden-sm">
          <div class="container">
              <div class="row">
                  <div class="col-md-12 text-center">
                      <?php if ($add->type == 'image'): ?>
                          <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                      <?php else:?>
                          <?php echo base64_decode($add->src); ?>
                      <?php endif; ?>
                  </div>
              </div>
          </div>
      </section>
    <?php endif; ?>
  <?php endif; ?>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
