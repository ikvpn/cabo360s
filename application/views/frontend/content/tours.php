<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <header class="background-header traditional-events-header paddingtop100 paddingbot20 header-common-height" style="background-image:url('<?php echo base_url('assets/images/tours-procida.jpg'); ?>');">

      <div class="main-page-title">
              <div class="container">
                  <div class="row paddingtop50">
                      <div class="col-md-8 banner-title">
                          <h1 class="main-title ml9" style="bottom: 30px !important;position: absolute;left: 20px;">
                            <?php echo lang('tours_title') ?>
                          </h1>
                      </div>
                  </div>
              </div>
          </div>
    </header>
    <div class="hidden-xs no-padding">
    	<?php $this->view('frontend/includes/booking_search_form'); ?>
    </div>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 paddingbot20">
                    <ul class="breadcrumbs list-inline">
                        <li>
                            <span class="glyphicon glyphicon-home"></span>
                        </li>
                        <li><a href="<?php echo base_url().$this->lang->lang(); ?>">Cabo San Lucas</a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li><a href="#"><?php echo lang('tours_title'); ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-9 col-xs-12">
                          <h2 class="sub-title"><?php echo lang('tours_title'); ?></h2>
                    </div>
                    <div class="col-md-3 col-xs-12 pull-right">

                    		<div class="col-xs-12">
                                <label for="selectedSort"><?php echo lang('filter'); ?></label>
                                <div class="input-group" id="adv-search">
                                    <input id="selectedSort" type="text" class="form-control" placeholder="<?php echo lang('all');?>" readonly/>
                                    <div class="input-group-btn">
                                        <div class="btn-group" role="group">
                                            <div class="dropdown dropdown-lg">
                                                <button type="button" class="fix-padding btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" >
                                                   <i class="fa fa-caret-down"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right no-margin dropdownFilter" >
                                                    <li><a href="javascript:void(0)" onclick="selectFilter(0, '<?php echo lang('all');?>')"><?php echo lang('all');?></a></li>
                                                    <li><a href="javascript:void(0)" onclick="selectFilter(1, '<?php echo lang('tours');?>')"><?php echo lang('tours');?></a></li>
                                                    <li><a href="javascript:void(0)" onclick="selectFilter(2, '<?php echo lang('escursion');?>')"><?php echo lang('escursion');?></a></li>
                                                    <li><a href="javascript:void(0)" onclick="selectFilter(3, '<?php echo lang('visit');?>')"><?php echo lang('visit');?></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                    	</div>
                </div>
            </div>
            <div class="row paddingtop10">
              <script type="text/javascript">
                  var markers = [];
              </script>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="row">
                            <?php foreach ($tours as $key => $tour): ?>
                              <script type="text/javascript">
                                  markers.push(['<?php  echo addslashes($tour->itTitle); ?>', <?php echo $tour->lat ?>, <?php echo $tour->long ?> ]);
                              </script>
                                  <div class="col-md-3 listing-tour" data-type="<?php echo $tour->type; ?>">
                                  	<a href="<?php echo base_url().$this->lang->lang()."/tour/".$tour->slug; ?>">
                                      <div class="project">
                                          <?php if ($tour->thumbnail != null && $tour->thumbnail != '' && $tour->thumbnail != ' '): ?>
                                            <img src="<?php echo base_url() . "uploads/tours/" . $tour->thumbnail; ?>" class="img-responsive project-img img-rounded" alt="<?php echo  $this->lang->lang() == 'it' ?  $tour->itTitle : $tour->enTitle; ?>">
                                          <?php else: ?>
                                            <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                                            <img class="img-responsive project-img img-rounded" src="<?php echo $scr; ?>" alt="<?php echo  $this->lang->lang() == 'it' ?  $tour->itTitle : $tour->enTitle; ?>">
                                          <?php endif; ?>
                                          <label class="price">$ <?php echo $tour->price; ?></label>
                                      </div>
                                      </a>
                                      <div class="tab-item-with-image">
                                             <h2><a href="<?php echo base_url().$this->lang->lang()."/tour/".$tour->slug; ?>"><?php echo  $this->lang->lang() == 'it' ?  $tour->itTitle : $tour->enTitle; ?></a></h2>
                                             <?php $duration = 0; if($tour->durationDay>0){$duration = $tour->durationDay.' d';}else if($tour->durationHours>0){$duration = $tour->durationHours.' h';}else{$duration = $tour->durationMin.' m';} ?>
                                              <p class="duration "><i class="fa colorTour fa-map-marker"></i> <?php echo $tour->locationName; ?></p>
                                          </div>
                                        </div>
                                    <?php endforeach; ?>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row paddingtop40"></div>
        </div>
    </section>
    <section class="map-section">
        <div id="map-canvas"></div>
        <div class="container">
        </div>
    </section>
    <section class="bottom-section gray">
        <div class="container">
            <div class="row">
                <?php if($blocks) {?>
                    <section class="page-content paddingtop40 paddingbot40 home-page sec-4" style="background-color:#FBFBFB">
                      <div class="container">
                        <div class="row paddingtop10">
                            <?php foreach ($blocks as $block): ?>
                          <div class="col-md-3">
                            <a href="<?php echo $block->link; ?>">

                              <div class="overview-item">
                                <div class="overview-image">
                                  <img src="<?php echo base_url(); ?>uploads/block/<?php echo $block->p_image; ?>" alt="<?php echo $block->title; ?>" class="lazy2 img-responsive img-rounded">
                                </div>
                                <h3><?php echo $block->title; ?></h3>
                                <p class="index-item-meta"><?php echo base64_decode($block->description); ?></p>
                              </div>
                            </a>
                          </div>

                            <?php endforeach; ?>
                        </div>
                      </div>
                    </section>
                <?php } ?>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
<style type="text/css">
section.page-content {
    background: #fff;
    padding-top: 20px !important;
}
</style>
