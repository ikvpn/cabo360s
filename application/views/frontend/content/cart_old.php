<script src="https://js.stripe.com/v2/"></script>
<div id="wrap" class="detail-page-wrapper">
  <div class="menu-bg"></div>
  <?php $this->view('frontend/includes/booking_search_form'); ?>
  <section class="page-content cartview">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div id="step1">
            <!-- list of hotels in cart -->
              <?php if(!empty($hotelCart)){ ?>
                <div class="alert alert-info" role="alert">
                  <strong><?php echo lang('room_available_on_selected_date'); ?></strong> <br> <?php echo lang('insert_qt'); ?>
                </div>
                <table class="table table-gray cart-list add_bottom_30">
                <thead>
                  <tr>
                    <th class=""><?php echo lang('rooms'); ?> </th>
                    <th></th>
                    <th></th>
                    <th class="text-center"><?php echo lang('per_night'); ?></th>
                    <th>TOT</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($hotelCart as $key => $item) { ?>
                    <input type="hidden" id="checkInHotel" value="<?php echo $item->checkIn; ?>">
                    <input type="hidden" id="checkOutHotel" value="<?php echo $item->checkOut; ?>">
                    <tr>
                      <td style="width:10%;">
                        <div class="thumb_cart">
                          <a href="#" data-toggle="modal" data-target="#modal_single_room"><img src="<?php echo $item->roomImage; ?>" width="100" alt="Image"></a>
                        </div>
                      </td>
                      <td style="width:40%;"><strong><?php echo $item->hotelName; ?> </strong></br> <?php echo $item->roomName; ?></td>
                      <td>
                      </td>
                      <td class="text-center"><strong class="singleprice">€ <?php echo $item->servicePrice / $item->nights; ?></strong></td>
                      <td><strong class="Hprice">€ <?php echo $item->servicePrice * $item->rooms;   ?></strong></td>
                    </tr>
                  <?php } ?> <!-- close foreach -->
                </tbody>
              </table>
            <?php } ?> <!-- close if not empty cart hotels -->
            <!-- list of transfer in cart -->
            <?php if (count($tranferCart) > 0): ?>
              <div class="scrollable">
                <table class="table table-gray options_cart">
                  <thead>
                    <tr>
                       <th><?php echo lang('transfer'); ?></th>
                       <th></th>
                       <th></th>
                       <th class="text-center"><?php echo lang('per_person'); ?></th>
                       <th>TOT</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($tranferCart as $key => $item) { ?>
                      <input type="hidden" id="checkInTransfer" value="<?php echo $item->checkIn; ?>">
                      <input type="hidden" id="checkOutTransfer" value="<?php echo $item->checkOut; ?>">
                      <tr>
                        <td style="width:10%;">
                          <div class="thumb_cart">
                            <img src="<?php echo base_url('assets/images/1468417512transfer.jpg'); ?>" width="100"  alt="image">
                          </div>
                        </td>
                        <td style="width:40%;">
                          <!-- <select id="address_return_2" class="form-control" name="address_return_2">
                              <option value="AirportNA"><?php echo lang('airport'); ?> &harr; Hotel</option>
                              <option value="CentralStation"><?php echo lang('station'); ?> &harr; Hotel</option>
                          </select> -->
                          <p id="round" >
                          <?php if($item->pickupAddress != null && $item->pickupAddress != '' && $item->pickupAddress != ' ') { ?>
                            <strong>Andata:</strong><br />
                              <?php echo $item->pickupAddress; ?>
                          <?php } ?>
                          <?php if($item->dropoffAddress != null && $item->dropoffAddress != '' && $item->dropoffAddress != ' ') {?>
                              <?php echo " - ".$item->dropoffAddress; ?>
                          <?php } ?>
                          </p>
                          <p id="return" >
                          <?php if($item->pickupReturnAddress != null && $item->pickupReturnAddress != '' && $item->pickupReturnAddress != ' ') {?>
                            <strong>Ritorno:</strong><br />
                              <?php  echo $item->pickupReturnAddress; ?>
                          <?php } ?>
                          <?php if($item->dropoffReturnAddress != null && $item->dropoffReturnAddress != '' && $item->dropoffReturnAddress != ' ') {?>
                              <?php echo " - ".$item->dropoffReturnAddress; ?>
                          <?php } ?>
                          </p>
                         </td>
                        <td>
                          <div class="numbers-row center-item text-center" data-price="53">
                            <input disabled type="text" value="<?php echo intval($item->people) + intval($item->children); ?>" class="qty2 transfer form-control" id='transfer-<?php echo $item->id; ?>' data-id='<?php echo $item->id; ?>' name="quantity_1" data-service-quantity='<?php echo $item->serviceQuantity; ?>' data-round-pickup="<?= $item->pickupAddress_id ?>" data-return-drop="<?= $item->returnDropAddress_id ?>" data-trasfer-type="<?= $item->extraType ?>">
                          </div>
                        </td>
                        <td class="text-center transferSP singleprice" id='transferSP-<?php echo $item->id; ?>' data-id='<?php echo $item->id; ?>' data-service-quantity='<?php echo $item->serviceQuantity; ?>'>
                          <strong>€ 53</strong>
                        </td>
                        <td class="text-center transferTP singleprice" id='transferTP-<?php echo $item->id; ?>' data-id='<?php echo $item->id; ?>'>
                          <strong>€ 53</strong>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            <?php endif; ?> <!-- close if not empty cart transfer -->
              <!-- list of tour in cart -->
            <?php if(!empty($tourCart)) { ?>
              <div class="scrollable">
                <table class="table table-gray options_cart">
                  <thead>
                    <tr>
                       <th class="">Tour</th>
                       <th></th>
                       <th class="text-center"><?php echo lang('adults'); ?></th>
                       <th class="text-center"><?php echo lang('children'); ?></th>
                       <th class="text-center"></th>
                       <th>TOT</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($tourCart as $tour) { ?>
                    <input type="hidden" id='tour-price-unit-<?php echo $tour->id; ?>' value='<?php echo $tour->info->priceUnit ?>'/>
                    <tr>
                      <td style="width:10%;">
                        <?php if($tour->info->thumbnail != NULL && $tour->info->thumbnail != '' && $tour->info->thumbnail != ' '){
                          $image = 'uploads/tours/'.$tour->info->thumbnail; } else {   $image = 'assets/images/slider.jpg'; }
                        ?>
                        <div class="thumb_cart">
                          <img src="<?php echo base_url() . $image; ?>" width="100" alt="Image">
                        </div>
                      </td>
                      <td style="width:20%;"><strong><?php if($this->lang->lang() == 'it'){ echo $tour->info->itTitle; } else { echo $tour->info->enTitle; } ?></strong></td>
                      <td>
                        <div class="tourp center-item numbers-row text-center" data-price="<?php echo $tour->servicePrice; ?>"  data-id='<?php echo $tour->id; ?>' data-discountAdults='<?php echo $tour->info->discountAdult; ?>' data-discountChildren='<?php echo $tour->info->discountChildren; ?>'>
                          <input disabled type="text" value="<?php echo $tour->people; ?>" class="qty2 form-control touradults" name="quantity_1"id='tour-adults-<?php echo $tour->id; ?>' data-id='<?php echo $tour->id; ?>' >
                        </div>
                      </td>
                      <td>
                        <div class="center-item numbers-row text-center tourpCh" data-price="<?php echo $tour->servicePrice; ?>" data-discountAdults='<?php echo $tour->info->discountAdult; ?>' data-discountChildren='<?php echo $tour->info->discountChildren; ?>'>
                          <input disabled type="text" value="<?php echo $tour->children; ?>" class="qty2 form-control tourchildren" name="quantity_1" id='tour-children-<?php echo $tour->id; ?>' data-id='<?php echo $tour->id; ?>'>
                        </div>
                      </td>
                      <td class="text-center center-item singleprice" id='tour-servicePriceSingle-<?php echo $tour->id; ?>'> <strong>€ <?php if($tour->info->priceUnit == 'P') { echo $tour->servicePrice; } ?></strong></td>
                      <td id='tour-servicePriceTotal-<?php echo $tour->id; ?>'><strong>€ <?php if($tour->info->priceUnit == 'P') { echo $tour->servicePrice*$tour->people; } else { echo $tour->servicePrice; } ?></strong></td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            <?php }  ?> <!-- close if not empty cart tour -->

<?php if('Hide Toor suggest' == '1') : ?>
            <!-- suggest -->
            <!-- suggest tour if not is in cart and if there are hotels  or tranfer -->
          <?php if((count($hotelCart) > 0 || count($tranferCart) > 0 ) && count($tourCart) == 0 ) { ?>
            <?php if(isset($toursSuggested) && count($toursSuggested) > 0) { ?>
            <div class="scrollable">
              <table class="table table-gray options_cart">
                <thead>
                  <tr>
                     <th class="">Tour Suggerito</th>
                     <th></th>
                     <th class="text-center"><?php echo lang('adults'); ?></th>
                     <th class="text-center"><?php echo lang('children'); ?></th>
                     <th class="text-center"></th>
                     <th>TOT</th>
                  </tr>
                </thead>
                <tbody>
                <?php $tour = $toursSuggested[0];  ?>
                  <input type="hidden" id='tour-price-unit-<?php echo $tour->id; ?>' value='<?php echo $tour->priceUnit ?>'/>
                  <input type="hidden" id='tour_id' value='<?php echo $tour->id ?>'/>
                  <tr>
                    <td style="width:10%;">
                      <?php if($tour->thumbnail != NULL && $tour->thumbnail != '' && $tour->thumbnail != ' '){
                        $image = 'uploads/tours/'.$tour->thumbnail; } else {   $image = 'assets/images/slider.jpg'; }
                      ?>
                      <div class="thumb_cart">
                        <img src="<?php echo base_url() . $image; ?>" width="100" alt="Image">
                      </div>
                    </td>
                    <td style="width:20%;">
                      <strong><a href="<?php echo base_url().$this->lang->lang().'/tour/'.$tour->slug ?>"><?php if($this->lang->lang() == 'it'){ echo $tour->itTitle; } else { echo $tour->enTitle; } ?></a></strong>
                      <p></p>
                      <input id="datepickertour" required class="date-pick form-control user-error" type="text" aria-invalid="true" placeholder="Date...">

                    </td>
                    <td>
                      <div class="tourp center-item numbers-row text-center" data-price="<?php echo $tour->price; ?>"  data-id='<?php echo $tour->id; ?>' data-discountAdults='<?php echo $tour->discountAdult; ?>' data-discountChildren='<?php echo $tour->discountChildren; ?>'>
                        <input disabled type="text" value="0" class="qty2 form-control touradults" name="quantity_1"id='tour-adults-<?php echo $tour->id; ?>' data-id='<?php echo $tour->id; ?>' >
                      </div>
                    </td>
                    <td>
                      <div class="center-item numbers-row text-center tourpCh" data-price="<?php echo $tour->price; ?>" data-discountAdults='<?php echo $tour->discountAdult; ?>' data-discountChildren='<?php echo $tour->discountChildren; ?>'>
                        <input disabled type="text" value="0" class="qty2 form-control tourchildren" name="quantity_1" id='tour-children-<?php echo $tour->id; ?>' data-id='<?php echo $tour->id; ?>'>
                      </div>
                    </td>
                    <td class="text-center center-item singleprice" id='tour-servicePriceSingle-<?php echo $tour->id; ?>'> <strong>€ <?php if($tour->priceUnit == 'P') { echo $tour->price; } ?></strong></td>
                    <td id='tour-servicePriceTotal-<?php echo $tour->id; ?>'><strong>€ <?php if($tour->priceUnit == 'P') { echo $tour->price*0; } else { echo $tour->price; } ?></strong></td>
                  </tr>

                </tbody>
              </table>
            </div>
            <?php } ?>
          <?php }  ?> <!-- close suggest tour -->
          <!-- suggest transfer if not is in cart and if there are hotels or tour -->
          <?php if((count($hotelCart) > 0 || count($tourCart) > 0 ) && count($tranferCart) == 0 ) { ?>
              <input hidden id="has_suggest_transfer" value="1"/>
              <div class="scrollable">
                <table class="table table-gray options_cart">
                  <thead>
                    <tr>
                       <th><?php echo lang('transfer'); ?></th>
                       <th>Andata e Ritorno</th>
                       <th></th>
                       <th class="text-center"><?php echo lang('per_person'); ?></th>
                       <th>TOT</th>
                    </tr>
                  </thead>
                  <tbody>

                      <tr>
                        <td style="width:10%;">
                          <div class="thumb_cart">
                            <img src="<?php echo base_url('assets/images/1468417512transfer.jpg'); ?>" width="100"  alt="image">
                          </div>
                        </td>
                        <td style="width:40%;">
                          <select id="address_return_2" class="form-control" name="address_return_2">
                              <option value="Airport-Hotel"><?php echo lang('airport'); ?> &harr; Hotel</option>
                              <option value="CentralStation-Hotel"><?php echo lang('station'); ?> &harr; Hotel</option>
                           </select>
                         </td>
                        <td>
                          <div class="numbers-row center-item text-center" data-price="53">
                            <input disabled type="text" value="0" class="qty2 transfer form-control" id='transfer-1' data-id='1' name="quantity_1" data-service-quantity='2'>
                          </div>
                        </td>
                        <td class="text-center transferSP singleprice" id='transferSP-1' data-id='1' data-service-quantity='2'>
                          <strong>€ 53</strong>
                        </td>
                        <td class="text-center transferTP singleprice" id='transferTP-1' data-id='1'>
                          <strong>€ 0</strong>
                        </td>
                      </tr>

                  </tbody>
                </table>
              </div>

          <?php }  ?> <!-- close suggest tour -->
<?php endif; ?>

          </div>
          <div id="step2" class="hidden">
            <form action="<?php echo base_url(); ?>Cart/pay" method="post" id="payment-form">
              <input hidden id="hotelP" name="hotelP" />
              <input hidden id="tourP" name="tourP" />
              <input hidden id="transferP" name="transferP" />
              <input hidden id="totalP" name="totalP" />
              <input hidden id="people-tour-cart" name="people-tour-cart"/>
              <input hidden id="children-tour-cart" name="children-tour-cart"/>
              <input hidden id="transfer-people-cart" name="transfer-people-cart"/>
              <input hidden id="tour_suggested_id" name="tour_suggested_id"/>
              <?php if(isset($toursSuggested) && count($toursSuggested) > 0){ ?>
                <input hidden id="suggest_tour" name="suggest_tour" value="1"/>
                <input hidden id="date_tour" name="date_tour"/>
              <?php } else { ?>
                <input hidden id="suggest_tour" name="suggest_tour" value="0"/>
              <?php } ?>
              <input hidden id="suggest_transfer" name="suggest_transfer"/>
              <input hidden id="destination_transfer" name="destination_transfer"/>
              <div class="form_title">
                <h3><strong>1</strong><?php echo lang('personal_info'); ?></h3>
              </div>
              <div class="step">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                      <label><?php echo lang('name'); ?></label>
                      <input type="text" class="form-control" name="firstname">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                      <label><?php echo lang('surname'); ?></label>
                      <input type="text" class="form-control" name="lastname">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                      <label><?php echo lang('email'); ?></label>
                      <input type="email" name="email" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                      <label><?php echo lang('confirm_email'); ?></label>
                      <input type="email" name="email_2" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                      <label><?php echo lang('phone'); ?></label>
                      <input type="text" name="phone" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                      <label><?php echo lang('flight_train'); ?></label>
                      <input type="text" name="trainOrFlight" class="form-control">
                    </div>
                  </div>
                </div>
              </div><!--End step -->
              <div class="form_title">
                <h3><strong>2</strong><?php echo lang('payment_info'); ?></h3>
              </div>
              <div class="step">
                <div class="form-group">
                  <label><?php echo lang('name_on_card'); ?></label>
                  <input type="text" class="form-control" name="name_card">
                </div>
                <div class="row">
        					<div class="col-md-6 col-sm-6">
        						<div class="form-group">
        							<label><?php echo lang('card_number'); ?></label>
        							<input type="text" id="card_number" name="card_number" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="4242424242424242">
        						</div>
        					</div>
        					<div class="col-md-6 col-sm-6">
        						<img src="<?php echo base_url(); ?>assets/img/cards.png" width="207" height="43" alt="Cards" class="cards">
        					</div>
        				</div>
        				<div class="row">
        					<div class="col-md-4">
        						<label><?php echo lang('expire_at'); ?></label>
        						<div class="row">
        							<div class="col-md-6">
        								<div class="form-group">
        									<input type="text" id="expire_month" name="expire_month" class="form-control" placeholder="MM" size="2" maxlength="2">
        								</div>
        							</div>
        							<div class="col-md-6">
        								<div class="form-group">
        									<input type="text" id="expire_year" name="expire_year" class="form-control" placeholder="YY" size="2" maxlength="2">
        								</div>
        							</div>
        						</div>
        					</div>
        					<div class="col-md-4">
        						<div class="form-group">
        							<label><?php echo lang('cvv_code'); ?></label>
        							<div class="row">
        								<div class="col-md-4">
        									<div class="form-group">
        										<input type="text" id="cvc" name="ccv" class="form-control" placeholder="CCV" size="3" maxlength="3">
        									</div>
        								</div>
        								<div class="col-md-8">
        									<img src="<?php echo base_url(); ?>assets/img/icon_ccv.gif" width="50" height="29" alt="ccv"><small><?php echo lang('last_digits'); ?></small>
        								</div>
        							</div>
        						</div>
        					</div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label><?php echo lang('location'); ?></label>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <input type="text" id="address_zip" name="address_zip" class="form-control" placeholder="ZIP CODE">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 payment-errors"></div>
        				</div>
                <hr>
              </div><!--End step -->
              <div class="form_title">
                <h3><strong>3</strong><?php echo lang('billing_address'); ?></h3>
              </div>
              <div class="step">
                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                      <label><?php echo lang('street_1'); ?></label>
                      <input type="text" name="street_1" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                      <label><?php echo lang('street_2'); ?></label>
                      <input type="text" name="street_2" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo lang('city'); ?></label>
                      <input type="text" name="city_booking" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label><?php echo lang('state'); ?></label>
                      <select class="form-control" name="state_booking" id="state_booking">
                        <option value="AF">Afghanistan</option><option value="">Alabama</option><option value="">Alaska</option><option value="AL">Albania</option><option value="DZ">Algeria</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antartide</option><option value="AG">Antigua e Barbuda</option><option value="AN">Antille Olandesi</option><option value="SA">Arabia Saudita</option><option value="AR">Argentina</option><option value="">Arizona</option><option value="">Arkansas</option><option value="AM">Armenia</option><option value="AW">Aruba</option><option value="AU">Australia</option><option value="AT">Austria</option><option value="AZ">Azerbaijan</option><option value="BS">Bahamas</option><option value="BH">Bahrein</option><option value="BD">Bangladesh</option><option value="BB">Barbados</option><option value="BE">Belgio</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BY">Bielorussia</option><option value="BO">Bolivia</option><option value="BA">Bosnia Erzegovina</option><option value="BW">Botswana</option><option value="BR">Brasile</option><option value="BN">Brunei Darussalam</option><option value="BG">Bulgaria</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="">California</option><option value="KH">Cambogia</option><option value="CM">Camerun</option><option value="CA">Canada</option><option value="CV">Capo Verde</option><option value="">Carolina del Nord</option><option value="">Carolina del Sud</option><option value="CX">Christmas Island</option><option value="TD">Ciad</option><option value="CL">Cile</option><option value="CN">Cina</option><option value="CY">Cipro</option><option value="CC">Cocos (Keeling) Islands</option><option value="CO">Colombia</option><option value="">Colorado</option><option value="KM">Comore</option><option value="CG">Congo</option><option value="">Congo,Rep. Democratica</option><option value="">Connecticut</option><option value="">Corea del Nord</option><option value="KR">Corea del Sud</option><option value="">Costa d'Avorio</option><option value="CR">Costa Rica</option><option value="">Croazia</option><option value="CU">Cuba</option><option value="">Dakota del Nord</option><option value="">Dakota del Sud</option><option value="DK">Danimarca</option><option value="">Delaware</option><option value="DM">Dominica</option><option value="EC">Ecuador</option><option value="EG">Egitto</option><option value="IE">EIRE</option><option value="SV">El Salvador</option><option value="AE">Emirati Arabi Uniti</option><option value="ER">Eritrea</option><option value="EE">Estonia</option><option value="ET">Etiopia</option><option value="FK">Falkland Islands (Malvinas)</option><option value="FJ">Figi</option><option value="PH">Filippine</option><option value="FI">Finlandia</option><option value="">Florida</option><option value="FR">Francia</option><option value="GA">Gabon</option><option value="GM">Gambia</option><option value="GE">Georgia</option><option value="GE">Georgia</option><option value="DE">Germania</option><option value="GH">Ghana</option><option value="JM">Giamaica</option><option value="JP">Giappone</option><option value="GI">Gibilterra</option><option value="DJ">Gibuti</option><option value="JO">Giordania</option><option value="GB">Gran Bretagna</option><option value="GR">Grecia</option><option value="GD">Grenada</option><option value="GL">Groenlandia</option><option value="GP">Guadalupa</option><option value="GU">Guam</option><option value="GT">Guatemala</option><option value="GF">Guiana Francese</option><option value="GN">Guinea</option><option value="GQ">Guinea Equatoriale</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="">Hawaii</option><option value="">Holy See (Vatican City State)</option><option value="HN">Honduras</option><option value="HK">Hong Kong</option><option value="">Idaho</option><option value="">Illinois</option><option value="IN">India</option><option value="">Indiana</option><option value="ID">Indonesia</option><option value="">Iowa</option><option value="">Iran</option><option value="IQ">Iraq</option><option value="IS">Islanda</option><option value="BV">Isola Bouvet</option><option value="">Isola di Norfolk</option><option value="KY">Isole Cayman</option><option value="CK">Isole di Cook</option><option value="FO">Isole Faroe</option><option value="">Isole Heard e McDonald</option><option value="MV">Isole Maldive</option><option value="MP">Isole Marianne del Nord</option><option value="MH">Isole Marshall</option><option value="UM">Isole Minor Outlying, USA</option><option value="SB">Isole Solomone</option><option value="TC">Isole Turks e Caicos</option><option value="">Isole Vergini, GB</option><option value="">Isole Vergini, USA</option><option value="IL">Israele</option><option selected="selected" value="IT">Italia</option><option value="">Kansas</option><option value="">Kazakistan</option><option value="">Kentucky</option><option value="KE">Kenya</option><option value="KG">Kirgizistan</option><option value="KI">Kiribati</option><option value="KW">Kuwait</option><option value="">Laos</option><option value="LS">Lesotho</option><option value="LV">Lettonia</option><option value="LB">Libano</option><option value="LR">Liberia</option><option value="LY">Libia</option><option value="LI">Liechtenstein</option><option value="LT">Lituania</option><option value="">Louisiana</option><option value="LU">Lussemburgo</option><option value="MO">Macao</option><option value="MK">Macedonia</option><option value="MG">Madagascar</option><option value="">Maine</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MA">Marocco</option><option value="MQ">Martinica</option><option value="">Maryland</option><option value="">Massachusetts</option><option value="MR">Mauritania</option><option value="MU">Mauritius</option><option value="TY">Mayotte</option><option value="MX">Mexico</option><option value="">Michigan</option><option value="FM">Micronesia</option><option value="">Minnesota</option><option value="">Mississippi</option><option value="">Missouri</option><option value="MD">Moldova, Republic of</option><option value="MC">Monaco</option><option value="MN">Mongolia</option><option value="">Montana</option><option value="MS">Montserrat</option><option value="MZ">Mozambique</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="">Nebraska</option><option value="NP">Nepal</option><option value="">Nevada</option><option value="">New Hampshire</option><option value="">New Jersey</option><option value="">New York</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="NO">Norvegia</option><option value="NC">Nuova Caledonia</option><option value="PG">Nuova Guinea</option><option value="NZ">Nuova Zelanda</option><option value="">Nuovo Messico</option><option value="">Oceano Indiano, territorio britannico</option><option value="">Ohio</option><option value="">Oklahoma</option><option value="OM">Oman</option><option value="">Oregon</option><option value="NL">Paesi Bassi</option><option value="PK">Pakistan</option><option value="PW">Palau</option><option value="PA">Panama</option><option value="PY">Paraguay</option><option value="">Pennsylvania</option><option value="PE">Peru</option><option value="PN">Pitcairn</option><option value="PF">Polinesia Francese</option><option value="PL">Polonia</option><option value="PR">Porto Rico</option><option value="PT">Portogallo</option><option value="QA">Qatar</option><option value="">R?union</option><option value="CZ">Repubblica Ceca</option><option value="CF">Repubblica Centrafricana</option><option value="DO">Repubblica Dominicana</option><option value="">Rhode Island</option><option value="RO">Romania</option><option value="RU">Russia</option><option value="RW">Rwanda</option><option value="EH">Sahara Occidentale</option><option value="KN">Saint Kitts e Nevis</option><option value="">Saint Pierre e Miquelon</option><option value="VC">Saint Vincent e le Grenadine</option><option value="WS">Samoa</option><option value="DS">Samoa Americane</option><option value="SM">San Marino</option><option value="">Sant'Elena</option><option value="LC">Santa Lucia</option><option value="ST">Sao Tome e Principe</option><option value="SN">Senegal</option><option value="YU">Serbia e Montenegro</option><option value="SC">Seychelles</option><option value="SL">Sierra Leone</option><option value="SG">Singapore</option><option value="SY">Siria</option><option value="SK">Slovacchia</option><option value="SI">Slovenia</option><option value="SO">Somalia</option><option value="ES">Spagna</option><option value="LK">Sri Lanka</option><option value="ZA">Sud Africa</option><option value="">Sud Georgia e Isole Sandwich del Sud</option><option value="SD">Sudan</option><option value="SR">Suriname</option><option value="">Svalbard e Jan Mayen</option><option value="SE">Svezia</option><option value="CH">Svizzera</option><option value="SZ">Swaziland</option><option value="TJ">Tagikistan</option><option value="">Taiwan</option><option value="TZ">Tanzania</option><option value="">Tennessee</option><option value="TF">Territori Francesi del Sud</option><option value="">Territori Palestinesi</option><option value="">Texas</option><option value="TH">Thailand</option><option value="TP">Timor Est</option><option value="TG">Togo</option><option value="TK">Tokelau</option><option value="TO">Tonga</option><option value="TT">Trinidad e Tobago</option><option value="TN">Tunisia</option><option value="TR">Turchia</option><option value="TM">Turkmenistan</option><option value="TV">Tuvalu</option><option value="UA">Ucraina</option><option value="UG">Uganda</option><option value="HU">Ungheria</option><option value="UY">Uruguay</option><option value="US">USA</option><option value="">Utah</option><option value="UZ">Uzbekistan</option><option value="VU">Vanuatu</option><option value="VE">Venezuela</option><option value="">Vermont</option><option value="VN">Vietnam</option><option value="">Virginia</option><option value="">Virginia Occidentale</option><option value="">Wallis e Futuna</option><option value="">Washington</option><option value="">Wisconsin</option><option value="">Wyoming</option><option value="YE">Yemen</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label><?php echo lang('p_code'); ?></label>
                      <input type="text" name="postal_code" class="form-control">
                    </div>
                  </div>
                </div><!--End row -->
              </div><!--End step -->
              <div id="policy">
                <div class="col-xs-12 col-md-7">
                  <h4><?php echo lang('payment_info'); ?></h4>
                  <div class="form-group">
                    <label>
                      <input style="margin-right: 10px;" type="checkbox" name="policy_terms">
                      <a href="<?php echo base_url() . $this->lang->lang() . '/terms-conditions' ?>" target="_blank"><?php echo lang('cancel_policy'); ?></a>
                    </label>
                  </div>
                </div>
                <div style="margin-top:30px" class="col-xs-12 col-md-5">
                  <a onclick="Cart.do_payment()" class="btn btn-purity btn-block pull-right"><?php echo lang('confirm_booking'); ?></a>
                </div>
              </div>
            </form>
          </div>

          <div class="box_style_4 col-xs-12 no-padding-xs">
              <i class="icon_set_1_icon-57"></i>
              <h4><?php echo lang('need_help'); ?> </h4>
              <a href="tel://+390818960454" class="phone">+39 081 8960454</a>
              <small><?php echo lang('hours_tel'); ?></small>
          </div>
        </div><!-- End col-md-8 -->
        <aside class="col-md-4">
          <div class="box_style_1">
            <h3 class="inner">- <?php echo lang('summary'); ?> -</h3>
            <table class="table table_summary">
              <tbody>
                <?php if (count($items) == 0): ?>
                  <tr>
                    <td>Check in</td>
                    <td class="text-right"></td>
                  </tr>
                  <tr>
                    <td>Check out</td>
                    <td class="text-right"></td>
                  </tr>
                  <tr>
                    <td><?php echo lang('rooms'); ?></td>
                    <td class="rooms text-right"></td>
                  </tr>
                  <tr>
                    <td><?php echo lang('nights'); ?></td>
                    <td class="text-right"></td>
                  </tr>
                  <tr>
                    <td><?php echo lang('adults'); ?></td>
                    <td class="text-right"></td>
                  </tr>
                  <tr>
                    <td><?php echo lang('children'); ?></td>
                    <td class="text-right"></td>
                  </tr>

                <?php endif; ?>
                <?php foreach ($items as $key => $value): ?>
                  <?php if ($value->serviceType == 'hotel'): ?>
                    <tr>
                      <td><strong>HOTEL</strong></td>
                      <td class="text-right"><?php echo $value->hotelName; ?></td>
                    </tr>
                    <tr>
                      <td>Check in</td>
                      <td class="text-right"><?php  echo $value->checkIn;  ?></td>
                    </tr>
                    <tr>
                      <td>Check out</td>
                      <td class="text-right"><?php echo $value->checkOut;  ?></td>
                    </tr>
                    <tr>
                      <td><?php echo lang('rooms'); ?></td>
                      <td class="rooms text-right" id='rooms-hotel-<?php echo $value->id; ?>'><?php echo $value->rooms; ?></td>
                    </tr>
                    <tr>
                      <td><?php echo lang('nights'); ?></td>
                      <td class="text-right"><?php echo $value->nights;  ?></td>
                    </tr>
                    <tr>
                      <td><?php echo lang('adults'); ?></td>
                      <td class="text-right"><?php echo $value->people;  ?></td>
                    </tr>
                    <tr>
                      <td><?php echo lang('children'); ?></td>
                      <td class="text-right"><?php echo $value->children;  ?></td>
                    </tr>
                  <?php elseif($value->serviceType == 'transfer'): ?>
                    <tr id='transferTable-<?php echo $value->id; ?>'>
                      <td><strong>TRANSFER</strong></td>
                      <td>
                    </tr>
                    <?php if ($value->checkIn != null): ?>
                      <tr>
                        <td><?php echo lang('forward_trip_date'); ?></td>
                        <td class="text-right"><?php  echo $value->checkIn;  ?></td>
                      </tr>
                    <?php endif; ?>
                    <?php if ($value->checkOut != null && $value->checkOut != '' && $value->checkOut != ' '): ?>
                      <tr>
                        <td><?php echo lang('return_trip_date'); ?></td>
                        <td class="text-right"><?php echo $value->checkOut;  ?></td>
                      </tr>
                    <?php endif; ?>
                    <?php if ($value->pickupAddress != null): ?>
                      <tr>
                        <td><?php echo lang('forward_trip_pickup'); ?></td>
                        <td class="text-right"><?php echo $value->pickupAddress;  ?></td>
                      </tr>
                    <?php endif; ?>
                    <?php if ($value->dropoffAddress != null): ?>
                      <tr>
                        <td><?php echo lang('forward_trip_dropoff'); ?></td>
                        <td class="text-right"><?php echo $value->dropoffAddress;  ?></td>
                      </tr>
                    <?php endif; ?>
                    <?php if ($value->pickupReturnAddress != null): ?>
                      <tr>
                        <td><?php echo lang('return_trip_pickup'); ?></td>
                        <td class="text-right"><?php echo $value->pickupReturnAddress;  ?></td>
                      </tr>
                    <?php endif; ?>
                    <?php if ($value->dropoffReturnAddress != null): ?>
                      <tr>
                        <td><?php echo lang('return_trip_dropoff'); ?></td>
                        <td class="text-right"><?php echo $value->dropoffReturnAddress;  ?></td>
                      </tr>
                    <?php endif; ?>
                    <tr>
                      <td><?php echo lang('people'); ?></td>
                      <td class="text-right" id='transferTablePeople-<?php echo $value->id; ?>'><?php echo intVal($value->people) + intVal($value->children);  ?></td>
                    </tr>

                  <?php elseif($value->serviceType == 'tour'): ?>
                    <tr id='tourTable-<?php echo $value->id; ?>'>
                      <td><strong>TOUR</strong></td>
                      <td>
                    </tr>
                    <?php if ($value->checkIn != null): ?>
                      <tr>
                        <td><?php echo lang('date'); ?></td>
                        <td class="text-right tourDate"><?php echo $value->checkIn;  ?></td>
                      </tr>
                    <?php endif; ?>
                    <?php if ($value->checkOut != null): ?>
                      <tr>
                        <td><?php echo lang('hour'); ?></td>
                        <td class="text-right"><?php echo $value->checkOut;  ?></td>
                      </tr>
                    <?php endif; ?>
                    <tr>
                      <td><?php echo lang('adults'); ?></td>
                      <td class="text-right" id="tourTableAdults-<?php echo $value->id; ?>"><?php echo $value->people;  ?></td>
                    </tr>
                    <tr>
                      <td><?php echo lang('children'); ?></td>
                      <td class="text-right" id="childTourTable-<?php echo $value->id; ?>"><?php echo $value->children;  ?></td>
                    </tr>

                  <?php endif; ?>
                <?php endforeach; ?>

                <tr class="total">
                  <td><?php echo lang('total'); ?></td>
                  <td class="totalcost text-right">€ <?php
                      $amount = 0;
                      if(isset($items[0])){
                        foreach ($items as $item) {
                          if($item->serviceType == 'hotel' && $item->rooms > 0){
                            $amount = $amount + ($item->servicePrice*$item->rooms);
                          }
                          else {
                            $amount = $amount + $item->servicePrice;
                          }
                        }
                      }
                      echo $amount;
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
            <a class="btn_full" id='book-now-button' onclick="Cart.paymentView()"><?php echo lang('book_now'); ?></a>
          </div>

        </aside><!-- End aside -->
      </div>
    </div>
  </section>
  <section class="bottom-section gray" style="margin-top:30px">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <?php $add = $this->Ads_model->get('970X90'); ?>
          <?php if ($add): ?>
            <?php if ($add->src != ''): ?>
              <?php if ($add->type == 'image'): ?>
                  <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
              <?php else: ?>
                  <?php echo base64_decode($add->src); ?>
              <?php endif; ?>
            <?php else: ?>
                <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
            <?php endif; ?>
          <?php else: ?>
              <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
<style type="text/css">
  section.page-content {
    padding-top: 20px !important;
}
</style>