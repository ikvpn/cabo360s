<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 paddingbot20">
                    <ul class="breadcrumbs list-inline">
                        <li>
                            <span class="glyphicon glyphicon-home"></span>
                        </li>
                        <li><a href="<?php echo base_url().$this->lang->lang(); ?>">Procida</a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li><a href="#"><?php echo lang('Upcoming_Events'); ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="sub-title"><?php echo lang('Upcoming_Events'); ?></h2>
                </div>
            </div>
            <div class="row paddingtop10">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover table-events">
                                <thead>
                                    <tr>
                                        <th colspan="2"><?php echo lang('Event'); ?>&nbsp;<i class="fa fa-unsorted"></i>
                                        </th>
                                        <th><?php echo lang('Location'); ?>&nbsp;<i class="fa fa-unsorted"></i>
                                        </th>
                                        <th><?php echo lang('Date') ?>&nbsp;<i class="fa fa-unsorted"></i>
                                        </th>
                                        <th colspan="2"><?php echo lang('Time')?>&nbsp;<i class="fa fa-unsorted"></i>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (isset($events) && is_array($events)): ?>
                                        <?php foreach ($events as $event): ?>
                                        <?php  $event_array = (array) $event; ?>
                                            <tr>
                                                <td>
                                                    <div class="dateBox">
                                                        <span class="date"><?php echo date('d',  strtotime($event->startDate)); ?></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h3 class="event-heading"><?php echo $event_array[$this->lang->lang().'Title']; ?></h3>
                                                    <p class="event-subheading">Jazz Quartet Trio</p>
                                                </td>
                                                <td>
                                                    <p class="event-location"><?php echo $this->Event_model->getLocationName($event->order_id,$this->lang->lang()); ?></p>
                                                </td>
                                                <td>
                                                    <p class="event-date"><?php echo date('M d, y',strtotime($event->startDate)) ?> to <?php echo date('M d, y',strtotime($event->endDate)) ?></p>
                                                </td>
                                                <td>
                                                    <p class="event-date"><?php echo substr($event->startTime,0,5).' to '.substr($event->endTime,0,5); ?></p>
                                                </td>
                                                <td><a href="<?php echo base_url().lang('events').'/upcoming-event/'.$event->id.'' ?>" class="btn btn-sm btn-danger"><?php echo strtolower(lang('more'));  ?> <i class="fa fa-angle-right"></i></a></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                            <tr>
                                                <td colspan="6"><?php echo lang('Event_Not_Found_Msg'); ?></td>
                                            </tr>        
                                    <?php endif; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6">
                                            <?php echo $this->pagination->create_links(); ?>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="well well-small text-center">
                            
                               <?php $add = $this->Ads_model->get('300X250'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                    <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row paddingtop40"></div>
        </div>
    </section>
    <section class="bottom-section gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>