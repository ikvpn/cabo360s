<!--################ WRAP START ################-->
<style>
@media (min-width: 1024px){
  .tourHeader {
    max-height: 300px;
    height:300px;
    margin:0 !important;
  }
  .tourHeader img {
      max-height: 400px !important;
  }
  .menu-bg {
    height: 76px!important;
  }
}
</style>
<div id="wrap" class="detail-page-wrapper" style="margin: -20px auto 0!important">
    <div class="hiddex-xs" style="min-height:70px"></div>
    <div class="hidden-xs no-padding">
    	<?php $this->view('frontend/includes/booking_search_form'); ?>
    </div>
    <?php $item_array = (array) $tour; ?>
    <input type="hidden" value= "<?php echo $tour->dateStart ?>" id="dateStart"/>
      <input type="hidden" value= "<?php echo $this->lang->lang() ?>" id="lang"/>
    <input type="hidden" value= "<?php echo $tour->dateEnd ?>" id="dateEnd"/>
    <input type="hidden" value= "<?php echo $tour->daysAvailability ?>" id="daysAvailability"/>
    <?php if($tour->sessionHour1 != NULL && $tour->sessionHour1 != '') {?>
      <input type="hidden" value="<?php echo $tour->sessionHour1 ?>" id="sessionHour1"/>
    <?php } ?>
    <?php if($tour->sessionHour2 != NULL && $tour->sessionHour2 != '') {?>
      <input type="hidden" value= "<?php echo $tour->sessionHour2 ?>" id="sessionHour2"/>
    <?php } ?>
    <?php if($tour->sessionHour3 != NULL && $tour->sessionHour3 != '') {?>
      <input type="hidden" value= "<?php echo $tour->sessionHour3 ?>" id="sessionHour3"/>
    <?php } ?>
    <input type="hidden" value= "<?php echo $tour->price ?>" id="price"/>
    <header class="tourHeader">
        <!-- ############## SLIDER START #################### -->
        <div class="tp-banner-container clearfix">
            <div class="tp-banner">
                <ul style="margin-bottom:0px !important;">
                    <?php if($tour->cover != NULL && $tour->cover != '' && $tour->cover != ' '){
                      $image = 'uploads/tours/'.$tour->cover;
                    ?>
                      <li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
                          <img src="<?php echo base_url() . $image; ?>" alt="<?php echo $item_array[$this->lang->lang() . "Title"]; ?>" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazyload="<?php echo base_url() . $image; ?>">
                      </li>
                    <?php

                    }
                    else {
                      $image = 'assets/images/slider.jpg'; ?>
                      <li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
                          <img src="<?php echo base_url() . $image; ?>" alt="<?php echo $item_array[$this->lang->lang() . "Title"]; ?>" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazyload="<?php echo base_url() . $image; ?>">
                      </li>
                        <?php
                    }

                    ?>
                </ul>
            </div>
            <div id="detail_slider" class="main-page-title">
                <div class="container">
                    <div class="row paddingtop50">
                        <div class="col-md-8 banner-title">

                            <h1 class="main-title"><?php echo $item_array[$this->lang->lang() . "Title"]; ?></h1>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- ############## SLIDER END #################### -->
    </header>

    <div class="container">

        <div class="row paddingtop10 paddingbot40 paddingtop40">
          <div class="col-md-8 col-xs-12">
            <div class="well well-small" style="background-color: #fff !important;border-radius: 0px !important;">
              <div class="col-xs-12 no-padding-xs" style="margin-top:20px;">
                <div class="col-xs-12 col-sm-9 no-padding-xs">
                  <div class="col-xs-3 text-center" style="padding:0px">
                    <?php $imagePath = 'uploads/'.$tour->businessPage->type.'/'; ?>
                    <?php if ($tour->businessPage->photo != null && $tour->businessPage->photo != '' && $tour->businessPage->photo != ' '): ?>
                      <img style="width: 60px !important;height: 60px;object-fit: cover; border-radius:30px" src="<?php echo base_url() . $imagePath . $tour->businessPage->photo; ?>" class="project-img " alt="<?php echo $tour->businessPage->itName; ?>">
                    <?php else: ?>
                      <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                      <img style="width: 60px !important;height: 60px;object-fit: cover; border-radius:30px" class="project-img " src="<?php echo $scr; ?>" alt="<?php echo $tour->businessPage->itName; ?>">
                    <?php endif; ?>

                  </div>
                  <div class="col-xs-9">
                    <p><?php echo lang('tour_org'); ?></p>

                    <?php
                      $langURL = lang(str_replace('_', '-', $tour->businessPage->urlType));
                      $type = '';
                      switch ($tour->businessPage->categoryID) {
                        case 5:
                          $type = 'holiday-house';
                          break;
                        case 2:
                          $type = 'restaurant';
                          break;
                        case 4:
                          $type = 'shopping';
                          break;
                        case 7:
                          $type = 'bars-and-cafe';
                          break;
                        case 8:
                          $type = 'rentals';
                          break;
                        case 9:
                          $type = 'wedding';
                          break;
                        case 10:
                          $type = lang('beach-clubs');
                          break;
                        case 11:
                          $type = 'nightlife';
                          break;
                        case 12:
                          $type = lang('act_services');;
                          break;
                      }

                      $url = $tour->businessPage->categoryID == 1 ? base_url().$this->lang->lang().'/hotel-detail/'.$tour->businessPage->id.'/'.str_replace(' ', '-', $tour->businessPage->enName).'' : base_url().$this->lang->lang().'/'.$langURL.'/'.$type.'/'.$tour->businessPage->pageTitle.'';
                    ?>
                    <a href="<?php echo $url;?>"><h2 class="color1"> <strong><?php echo $tour->businessPage->itName; ?></strong></h2></a>
                  </div>
                </div>
                <div class="col-sm-3 col-xs-12 no-padding-xs">
                  <p class="color1 pull-right"><strong><?php echo lang($tour->priceUnit); ?></strong></p>
                  <h1 class="color1 pull-right" style='margin-top:0px !important;'>  <strong>€ <?php echo $tour->price; ?></strong></h1>
                </div>
              </div>
              <div class="no-padding-xs col-xs-12"  style="margin-top:50px;">


                <ul class="list-unstyled">
                    <?php $duration = 0; if($tour->durationDay>0){$duration = $tour->durationDay.' d';}else if($tour->durationHours>0){$duration = $tour->durationHours.' h';}else{$duration = $tour->durationMin.' m';} ?>
                    <li><i class="picon picon-clock color1"></i><strong><?php echo lang('duration'); ?></strong><p class="pull-right"><?php echo $duration; ?></p></li>
                    <li><i class="picon picon-calendar color1"></i><strong><?php echo lang('validity'); ?></strong><p class="datesFix pull-right"><?php echo lang('from').' '.$tour->dateStart.' '.lang('to').' '.$tour->dateEnd ?></p></li>
                    <?php $daysAvailability ='' ;$days = $tour->daysAvailability; $days = explode(',', $days); foreach ($days as $day) {
                      $daysAvailability = $daysAvailability.' '.lang($day);
                    }?>
                    <li><i class="picon picon-calendar color1 "></i><strong><?php echo lang('days'); ?></strong><p class="pull-right"><?php echo $daysAvailability; ?></p></li>
                    <?php if ($tour->sessionHour1 && $tour->sessionHour1 != '' && $tour->sessionHour1 != '0:00 - 0:00'): ?>
                        <li><i class="picon picon-info color1"></i><strong><?php echo lang('session1'); ?></strong><p class="pull-right"><?php echo $tour->sessionHour1 ?></p></li>
                    <?php endif; ?>
                    <?php if ($tour->sessionHour2 && $tour->sessionHour2 != '' && $tour->sessionHour2 != '0:00 - 0:00'): ?>
                        <li><i class="picon picon-info color1"></i><strong><?php echo lang('session2'); ?></strong><p class="pull-right"><?php echo $tour->sessionHour2 ?></p></li>
                    <?php endif; ?>
                    <?php if ($tour->sessionHour3 && $tour->sessionHour3 != '' && $tour->sessionHour3 != '0:00 - 0:00'): ?>
                        <li><i class="picon picon-info color1"></i><strong><?php echo lang('session3'); ?></strong><p class="pull-right"><?php echo $tour->sessionHour3 ?></p></li>
                    <?php endif; ?>

                </ul>
                </div>
                <div class="well-heading"><?php echo lang('information'); ?></div>
                <div class="well-content">
                  <?php echo base64_decode($item_array[$this->lang->lang() . "Description"]); ?>
                </div>

                <div class="well-heading"><?php echo lang('included'); ?></div>
                <div class="well-content">
                  <p>
                  <?php echo base64_decode($item_array[$this->lang->lang() . "ServicesIncluded"]); ?></p>
                </div>

                <div class="well-heading"><?php echo lang('not_incuded'); ?></div>
                <div class="well-content">
                  <p><?php echo base64_decode($item_array[$this->lang->lang() . "ServicesNotIncluded"]); ?></p>
                </div>

                <div class="well-heading"><?php echo lang('policy'); ?></div>
                <div class="well-content">
                  <p><?php echo base64_decode($item_array[$this->lang->lang() . "Policy"]); ?></p>
                </div>

                <div class="well-heading"><?php echo lang('starting_point'); ?></div>
                <div class="well-content">
                  <div id="map-canvas-sidebar" class="img-rounded" style="height:450px; z-index: 999;"></div>
                  <div class="col-md-12 paddingtop20">
                      <hr class="procida">
                      <div class="row">
                          <div class="col-md-6">
                              <div class="detail-summary">
                                  <p>
                                      <i class="picon picon-location color1"></i>
                                      <span><?php echo $tour->locationName; ?></span>
                                  </p>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="detail-summary">
                                  <p>
                                      <i class="picon picon-map-marker color1"></i>
                                      <span><?php echo $tour->businessPage->address; ?></span>
                                  </p>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="detail-summary">
                                  <p>
                                      <i class="fa fa-share-square-o color1"></i>

                                      <span><?php echo $tour->businessPage->website; ?></span>
                                  </p>
                              </div>
                          </div>

                          <div class="col-md-6">
                              <div class="detail-summary">
                                  <p>
                                      <i class="fa fa-phone  color1"></i>
                                      <span><?php echo $tour->businessPage->phone ?></span>
                                  </p>
                              </div>
                          </div>


                      </div>
                  </div>

                </div>
            </div>

          </div>
          <aside class="col-md-4 col-xs-12">

        		<div class="box_style_1 expose">
        			<h3 class="inner">- <?php echo lang('booking'); ?> -</h3>
        			<div class="row">
        				<div class="col-md-6 col-sm-6">
        					<div class="form-group">
        						<label><i class="icon-calendar-7"></i>  <?php echo lang('select_date'); ?></label>
        						<input class="date-pick form-control" type="text">
        					</div>
        				</div>
        				<div class="col-md-6 col-sm-6">
        					<div class="form-group">
        						<label><i class=" icon-clock"></i> <?php echo lang('time'); ?></label>

                    <?php if(($tour->sessionHour1 != NULL && $tour->sessionHour1 != '') || ($tour->sessionHour2 != NULL && $tour->sessionHour2 != '') || ($tour->sessionHour3 != NULL && $tour->sessionHour3 != '')){?>
                      <select class="form-control" id="sessionHour">
                        <?php if($tour->sessionHour1 != NULL && $tour->sessionHour1 != '' && $tour->sessionHour1 != '0:00 - 0:00') {?>
                            <option value="<?php echo $tour->sessionHour1 ?>"><?php echo $tour->sessionHour1 ?></option>
                        <?php } ?>
                        <?php if($tour->sessionHour2 != NULL && $tour->sessionHour2 != '' && $tour->sessionHour2 != '0:00 - 0:00') {?>
                            <option value="<?php echo $tour->sessionHour2 ?>"><?php echo $tour->sessionHour2 ?></option>
                        <?php } ?>
                        <?php if($tour->sessionHour3 != NULL && $tour->sessionHour3 != '' && $tour->sessionHour3 != '0:00 - 0:00') {?>
                            <option value="<?php echo $tour->sessionHour3 ?>"><?php echo $tour->sessionHour3 ?></option>
                        <?php } ?>
                      </select>
                      <?php } else { ?>
                        <input class="time-pick form-control" value="12:00 AM" type="text" id="timeTour">
                      <?php } ?>
        					</div>
        				</div>
        			</div>
        			<div class="row">
        				<div class="col-md-6 col-sm-6">
        					<div class="form-group">
        						<label><?php echo lang('adults'); ?></label>
        						<div class="adults numbers-row">
        							<input type="text" value="1" class="qty2 form-control" name="quantity">
                      <div class="inc button_inc">+</div><div class="dec button_inc">-</div>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-6 col-sm-6">
        					<div class="form-group">
        						<label><?php echo lang('child'); ?></label>
        						<div class="child numbers-row">
        							<input type="text" value="0" class="qty2 form-control" name="quantity">
                      <div class="inc button_inc">+</div><div class="dec button_inc">-</div>
        						</div>
        					</div>
        				</div>
        			</div>
        			<br>
        			<table class="table table_summary">
        			<tbody>
        			<tr>
        				<td>
        					<?php echo lang('adults'); ?>
        				</td>
        				<td id="adults" class="text-right">
        					1
        				</td>
        			</tr>
        			<tr>
        				<td>
        					<?php echo lang('child'); ?>
        				</td>
        				<td id="children" class="text-right">
        					0
        				</td>
        			</tr>
        			<tr>
        				<td>
        					<?php echo lang('total'); ?>
        				</td>
        				<td id="peopleAmount" class="text-right">
        					1 x €<?php echo $tour->price; ?>
        				</td>
                <input hidden="hidden" id="sp" value="<?php echo $tour->price; ?>" data-type="<?php echo $tour->priceUnit; ?>" />
                <input hidden="hidden" id="dc" data-adults="<?php echo $tour->discountAdult; ?>" data-children="<?php echo $tour->discountChildren; ?>" />
                <input hidden="hidden" id="sId" value="<?php echo $tour->id; ?>" />
        			</tr>
        			<tr class="total">
        				<td>
        					<?php echo lang('total'); ?>
        				</td>
        				<td id="total" class="text-right">
        					€ <?php echo $tour->price; ?>
        				</td>
        			</tr>
        			</tbody>
        			</table>
        			<a class="btn_full" id="addTo"><?php echo lang('book_now'); ?></a>
        		</div><!--/box_style_1 -->
            <?php if($tour->instagramWidget && $tour->instagramWidget != ''){?>
              <div>
                <?php  echo $tour->instagramWidget; ?>
              </div>
              <?php  }  else if($tour->businessPage->enInsta && $tour->businessPage->enInsta != '') {?>
                <div>
                  <?php  echo $tour->businessPage->enInsta; ?>
                </div>
                <?php } ?>
            </aside>

          </div>
        </div>
    </div>
</section>

<section class="bottom-section gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <?php $add = $this->Ads_model->get('970X90'); ?>
                <?php if ($add): ?>
                    <?php if ($add->src != ''): ?>
                        <?php if ($add->type == 'image'): ?>
                            <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                        <?php else: ?>
                            <?php echo base64_decode($add->src); ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                <?php else: ?>
                    <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
</div>
<div id="push" style="height:0px !important"></div>
