<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <header class="background-header traditional-events-header paddingtop100 paddingbot20 header-common-height" style="background-image:url('<?php echo base_url('assets/images/transfer-procida.jpg'); ?>');">
      <div class="main-page-title">
              <div class="container">
                  <div class="row paddingtop50">
                      <div class="col-md-8 banner-title">
                          <h1 class="main-title ml9" style="bottom: 30px !important;position: absolute;left: 20px;">
                            Private Airport Transfers
                          </h1>
                      </div>
                  </div>
              </div>
          </div>
    </header>
    <div class="hidden-xs no-padding">
    	<?php $this->view('frontend/includes/booking_search_form'); ?>
    </div>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 paddingbot20">
                    <ul class="breadcrumbs list-inline">
                        <li>
                            <span class="glyphicon glyphicon-home"></span>
                        </li>
                        <li><a href="<?php echo base_url().$this->lang->lang(); ?>">Cabo San Lucas</a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li><a href="#">Private Airport Transfers</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-9 col-xs-12">
                          <h2 class="sub-title">Exclusive Transportation Services</h2>
                    </div>
                    <div class="col-md-3 col-xs-12 pull-right">

                    		<div class="col-xs-12">
                                <label for="selectedSort"><?php echo lang('filter'); ?></label>
                                <div class="input-group" id="adv-search">
                                    <input id="selectedSort" type="text" class="form-control" placeholder="<?php echo lang('all');?>" readonly/>
                                    <div class="input-group-btn">
                                        <div class="btn-group" role="group">
                                            <div class="dropdown dropdown-lg">
                                                <button type="button" class="fix-padding btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" >
                                                   <i class="fa fa-caret-down"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right no-margin dropdownFilter" >
                                                    <li><a href="javascript:void(0)" onclick="selectFilter(0, '<?php echo lang('all');?>')"><?php echo lang('all');?></a></li>
                                                  <?php foreach ($transfers_type as $tKey => $tValue): ?>
                                                    <li><a href="javascript:void(0)" onclick="selectFilter('<?= $tKey ?>', '<?= $tValue ?>');"><?= $tValue ?></a></li>
                                                  <?php endforeach; ?>
                                                  <!--   <li><a href="javascript:void(0)" onclick="selectFilter(2, '<?php echo lang('escursion');?>');">Yacht Transfer</a></li> -->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                    	</div>
                </div>
            </div>
            <div class="row paddingtop10">
              <script type="text/javascript">
                  var markers = [];
              </script>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="row">
                            <?php foreach ($transfer as $key => $transfer): ?>
                              <script type="text/javascript">
                                  markers.push(['<?php  echo addslashes($transfer->itTitle); ?>']);
                              </script>
                                  <div class="col-md-3 listing-transfer" data-type="<?php echo $transfer->transfer_type; ?>">
                                  	<a href="<?php echo base_url().$this->lang->lang()."/transfer/".$transfer->slug; ?>">
                                      <div class="project">
                                          <?php if ($transfer->thumbnail != null && $transfer->thumbnail != '' && $transfer->thumbnail != ' '): ?>
                                            <img src="<?php echo base_url() . "uploads/transfer/" . $transfer->thumbnail; ?>" class="img-responsive project-img img-rounded" alt="<?php echo  $this->lang->lang() == 'it' ?  $transfer->itTitle : $transfer->enTitle; ?>">
                                          <?php else: ?>
                                            <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                                            <img class="img-responsive project-img img-rounded" src="<?php echo $scr; ?>" alt="<?php echo  $this->lang->lang() == 'it' ?  $transfer->itTitle : $transfer->enTitle; ?>">
                                          <?php endif; ?>
                                          <!-- <label class="price">€ <?php echo $transfer->startPrice; ?></label> -->
                                      </div>
                                      </a>
                                      <div class="tab-item-with-image">
                                             <h2><a href="<?php echo base_url().$this->lang->lang()."/transfer/".$transfer->slug; ?>"><?php echo $transfer->itTitle; ?></a></h2>
                                              <p class="duration "><i class="fa colorTour fa fa-dollar"></i> <?php echo $transfer->startPrice; ?>&nbsp&nbsp&nbsp<i class="fa colorTour fa fa-clock-o"></i> <?php echo $transfer->duration; ?> &nbsp&nbsp&nbsp<i class="fa colorTour fa-users"></i> <?php echo $transfer->maxPassenger; ?></p>
                                          </div>
                                        </div>
                                    <?php endforeach; ?>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row paddingtop40"></div>
        </div>
    </section>
   <!--  <section class="map-section">
        <div id="map-canvas"></div>
        <div class="container">
        </div>
    </section> -->
    <section class="bottom-section gray">
        <div class="container">
            <div class="row">
                <?php if($blocks) {?>
                    <section class="page-content paddingtop40 paddingbot40 home-page sec-4" style="background-color:#FBFBFB">
                        <div class="container">
                            <div class="row paddingtop10">
                                <?php foreach ($blocks as $block): ?>
                                <div class="col-md-3">
                                    <a href="<?php echo $block->link; ?>">

                                        <div class="overview-item">
                                            <div class="overview-image">
                                                <img src="<?php echo base_url(); ?>uploads/block/<?php echo $block->p_image; ?>" alt="<?php echo $block->title; ?>" class="lazy2 img-responsive img-rounded">
                                            </div>
                                            <h3><?php echo $block->title; ?></h3>
                                            <p class="index-item-meta"><?php echo base64_decode($block->description); ?></p>
                                        </div>
                                    </a>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </section>
                <?php } ?>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
<style type="text/css">
section.page-content {
    background: #fff;
    padding-top: 20px !important;
}
</style>
