<header class="background-header-slider">
  <!-- ############## SLIDER START #################### -->
  <div class="tp-banner-container clearfix">
    <div class="tp-banner">
      <ul>
      	<?php if(count($fullHotel->images) > 0) {?>
	        <?php foreach ($fullHotel->images as $image) { ?>
	        	<?php if(@getimagesize($image->name)) { ?>
		          <li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
		            <img src="<?php echo $image->name; ?>" alt="<?php echo $fullHotel->enName; ?>" data-bgfit="contain" data-bgposition="center" data-bgrepeat="no-repeat" data-lazyload="<?php echo $image->name; ?>">
		          </li>
		         <?php } ?>
	          <?php } ?>
	     <?php } else { $image = 'assets/images/slider.jpg'; ?>
	    	<li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
	            <img src="<?php echo base_url() . $image; ?>" alt="splendida corricella" data-bgfit="contain" data-bgposition="center" data-bgrepeat="no-repeat" data-lazyload="<?php echo base_url() . $image; ?>">
	        </li>
	   	 <?php } ?>
        </ul>
      </div>
      <div id="detail_slider" class="main-page-title">
        <div class="container">
          <div class="row paddingtop50 banner-title">
            <h1 class="main-title">
              <?php
                switch ($this->lang->lang()) {
                  case 'it':
                    echo $fullHotel->itName;
                    break;
                  case 'en':
                    echo $fullHotel->enName;
                    break;
                  case 'du':
                    echo $fullHotel->duName;
                    break;
                }
              ?>
            </h1>
          </div>
        </div>
      </div>
    </div>
  </div>
      <!-- ############## SLIDER END #################### -->
</header>
<section class="page-title">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="row">
          <div class="col-md-12"></div>
          <div class="col-md-12 no-padding hotelStats m-tb-10">
            <div class="col-xs-4 text-center">
              <i class="fa fa-home fa-2x"></i>
              <h4><?php echo lang($fullHotel->type); ?></h4>
            </div>
            <div class="col-xs-4 text-center">
              <div class="hidden-xs">
                <?php
                if($fullHotel->stars == 6){
                  $stars = 0;
                } else {
                  $stars = $fullHotel->stars;
                }
                for($i=0; $i<$stars; $i++){ ?>
                  <i class="fa fa-star fa-2x"></i>
                <?php } ?>
                <?php if($stars == 0){ ?>
                  <i class="fa fa-star fa-2x"></i>
                <?php } ?>
              </div>
              <div class="visible-xs">
                <i class="fa fa-star fa-2x"></i>
              </div>
              <h4><?php $stars = $fullHotel->stars; if($stars == 0 || $stars == 6) { echo 'N.D. '; } else { echo $stars.' '; echo lang('stars'); } ?></h4>
            </div>
            <div class="col-xs-4 text-center">
              <i class="fa fa-bed fa-2x"></i>
              <h4><?php if(isset($rooms)){ echo count($rooms); if($rooms == 1){ echo lang('room'); } else { echo lang('rooms'); } }?> </h4>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="page-title-search">
          <div class="page-title-search-header visible-lg">
            <h3 class="search-header"><?php echo lang('verify_disponibility'); ?></h3>
          </div>
          <script src="https://hbb.bz/thirdmask/js/masck.js"></script>
          <script src="https://hbb.bz/thirdmask/js/library/jquery-ui.min.js"></script>
          <link href="https://hbb.bz/thirdmask/config_reseller/16/custom.css" rel="stylesheet">
          <link href="https://hbb.bz/thirdmask/css/library/jquery-ui.min.css" rel="stylesheet">
          <div id="hotelSearch">
            <div class="form-cn form-hotel " id="form-hotel">
              <form action="<?php echo base_url(); ?><?php echo $this->lang->lang(); ?>/HotelBB/detail/<?php echo $fullHotel->hotel_bb_id; ?>" method="post" name="formhotel">
                <input type="hidden" value="<?php echo $this->lang->lang(); ?>" name="lang">
                <div class="form-search clearfix">
                  <div class="col-sm-6 col-xs-6 no-padding-left">
                    <div class="input-group">
                      <input value="" type="text" class="form-control datepicker" autocomplete="off" name="start_date" placeholder="<?php echo lang('Check-In'); ?>">
                      <div class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 no-padding-right">
                    <div class="input-group">
                      <input value="" type="text" class="form-control datepicker" autocomplete="off" name="end_date" placeholder="<?php echo lang('Check-Out'); ?>">
                      <div class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-9 col-xs-6 m-t-20 no-padding-left">
                    <div class="form-group" id="divospitiroom">
                      <input type="text" id="ospiti" name="txtospiti" class="form-control" autocomplete="off" placeholder="Camere:1 Adulti: 2 Bambini: 0" value="">
                      <input type="hidden" id="jsonparty" name="party" value='[{"adults":2}]' />
                      <div class="popupbambini" id="boxospiti">
                        <div id="boxcamere"></div>
                        <div class="ospitibottom">
                          <a style="float:left; font-size:12px;" id="addcamera" href="#">+ Aggiungi camera</a>
                          <a class="btn btn-sm btn-purity awe-btn awe-btn-1 awe-btn-small settaospiti" id="settaospiti" href="#">OK</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-6 m-t-20 no-padding-right">
                    <button type="submit" class="btn btn-purity btn-block "><?php echo lang('Search'); ?></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-content paddingtop20">
  <div class="container">
  	<div class="row">
      <div class="col-md-9">
        <div class="col-md-12 no-padding">
          <h3 class="sub-title"><?php echo lang('description'); ?></h3>
        </div>
          <?php
            switch ($this->lang->lang()) {
              case 'it':
                echo base64_decode($fullHotel->itDescription);
                break;
              case 'en':
                echo base64_decode($fullHotel->enDescription);
                break;
              case 'du':
                echo base64_decode($fullHotel->duDescription);
                break;
            }
          ?>
      	
        <div>
          <h3><?php echo lang('room_types'); ?></h3>
        </div>
        <?php if(isset($rooms)){ ?>
          <div class="marginbottom20">
          <div class="listing-rooms">
            <?php 
							foreach ($rooms as $keyT => $typology) { 
								foreach($typology as $key => $room){
						?>
              <div class="col-xs-12 single-room no-padding m-tb-10-20">
                  <div class="col-md-4 col-xs-12">
                      <a onClick="modalRoom('room-<?php echo $key; ?>')">
                        <div class="listing-image restaurant-listing-image" style="background-image:url('<?php echo $room->photos[0]->url[0]; ?>');"></div>
                      </a>
                      <div id="<?php echo $key; ?>">
                        <div class="hidden room-<?php echo $key; ?>">
                        <div>
                          <!-- // add Corousel -->
                          <div id="carousel-room-<?php echo $key; ?>" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <?php foreach ($room->photos[0]->url as $k => $photo) { ?>
                                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $k; ?>" class="<?php echo ($k == 0) ? 'active' : ''; ?>"></li>
                              <?php } ?>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <?php foreach ($room->photos[0]->url as $k => $photo) { ?>
                                <div class="item <?php echo ($k == 0) ? 'active' : ''; ?>">
                                  <img class="img-responsive" src="<?php echo $photo; ?>" alt="">
                                </div>
                              <?php } ?>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-room-<?php echo $key; ?>" role="button" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-room-<?php echo $key; ?>" role="button" data-slide="next">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                          </div>
                        </div>
                        <div>
                          <!-- // add general room info. -->
                          <h4><strong><?php echo $room->name; ?></strong></h4>
                          <h4><strong><?php echo $room->partner_data->tratt_name; ?></strong></h4>
                          <p><?php echo $room->description; ?></p>
                          <p><?php echo $room->cancellation_policy; ?></p>
                        </div>
                      </div>
                      </div>
                  </div>
                  <div class="col-md-5 col-xs-12">
                      <h3 class="listing-title"><a onClick="modalRoom('room-<?php echo $key; ?>')"><?php echo $room->name; ?></a></h3>
                      <h4 class="listing-sub-title"><?php echo substr(html_entity_decode($room->description), 0, 200); ?></h4>
                      <p> <?php echo $room->partner_data->tratt_name; ?> </p>
                  </div>
                  <div class="col-md-3 col-xs-12">
                  	<p><?php if($room->partner_data->not_refund == 1){ echo lang('none'); }else{ echo lang('full'); }?></p>
                    <span class="colorPurity listing-price"><?php echo $room->final_price_at_checkout->amount; ?> €</span>
                    <div
                      id="info-<?php echo $key; ?>"
                      data-type="<?php echo 'hotel'; ?>"
                      data-price="<?php echo $room->final_price_at_checkout->amount; ?>"
                      data-startDate="<?php echo $start_date; ?>"
                      data-endDate="<?php echo $end_date; ?>"
                      data-rooms="<?php echo $info['rooms']; ?>"
                      data-roomName="<?php echo $room->name; ?>"
                      data-roomImage="<?php echo $room->photos[0]->url[0]; ?>"
                      data-people="<?php echo $info['people']; ?>"
                      data-children="<?php echo $info['children']; ?>">
                    </div>
                    <div
                      id="roomInfo-<?php echo $key; ?>"
                      data-hotelcode="<?php echo $room->partner_data->hotel_code; ?>"
                      data-roomcode="<?php echo $room->partner_data->room_code; ?>"
                      data-adults="<?php echo $room->partner_data->adults; ?>"
                      data-quota="<?php echo $room->partner_data->quota; ?>"
                      data-trattcode="<?php echo $room->partner_data->tratt_code;?>"
                      data-trattname="<?php echo $room->partner_data->tratt_name;?>"
                      data-offertacode="<?php if(isset($room->partner_data->offerta_code)){ echo $room->partner_data->offerta_code; } ?>"
                      data-offertatipocode="<?php if(isset($room->partner_data->offerta_tipo_code)){ echo $room->partner_data->offerta_tipo_code; } ?>"
                      data-offertaname="<?php if(isset($room->partner_data->offerta_tipo_code)){ echo $room->partner_data->offerta_name; }?>">
                    </div>
                    <a onclick="addToCart('<?php echo $key; ?>')" class="btn btn-block btn-purity btn-sm m-t-20"><?php echo lang("book_now"); ?></a>
                  </div>
                </div>
              <?php } }?>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </section>
  <section class="map-section">
    <div id="map-canvas"></div>
    <div class="container">
      <div class="map-info-box">
        <div class="line">
          <?php foreach ($places as $place) { ?>
            <h2 onclick="NearbyMap.calcRoute('<?php echo $fullHotel->itName; ?>','<?php if($this->lang->lang() == 'it') { echo $place->itTitle; } else { echo $place->enTitle; } ?>, Procida, Naples, Italy, mainPlace');"><i class="fa color1 fa-map-marker fa-2x"></i>
              <?php if($this->lang->lang() == 'it') { echo $place->itTitle; } else { echo $place->enTitle; } ?>
            </h2>
            <p>
            <?php
            	if($this->lang->lang() == 'it') {
            		echo $place->itDescription;
            	} else {
            		echo $place->enDescription;
            	}
             ?>
          <?php } ?>
          </p>
        </div>
      </div>
    </div>
  </section>

  <section class="bottom-section">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <?php $add = $this->Ads_model->get('970X90'); ?>
          <?php if ($add): ?>
            <?php if ($add->src != ''): ?>
              <?php if ($add->type == 'image'): ?>
                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
              <?php else: ?>
                <?php echo base64_decode($add->src); ?>
              <?php endif; ?>
            <?php else: ?>
              <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
            <?php endif; ?>
          <?php else: ?>
            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal roomModal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
<script>
function addToCart(divId){

  var serviceId = <?php echo $this->uri->segment(4); ?>;
  var servicePrice = $('#info-'+divId).attr('data-price');
  var type = $('#info-'+divId).attr('data-type');
  var startDate = $('#info-'+divId).attr('data-startDate');
  var endDate = $('#info-'+divId).attr('data-endDate');
  var rooms = $('#info-'+divId).attr('data-rooms');
  var roomName = $('#info-'+divId).attr('data-roomName');
  var people = $('#info-'+divId).attr('data-people');
  var children = $('#info-'+divId).attr('data-children');
  var roomImage = $('#info-'+divId).attr('data-roomImage');

  var a = moment(startDate);
  var b = moment(endDate);

  var diff = b.diff(a, 'days');
  var nights = diff;

  var hotelcode = $('#roomInfo-'+divId).attr('data-hotelcode');
  var roomcode = $('#roomInfo-'+divId).attr('data-roomcode');
  var adults = $('#roomInfo-'+divId).attr('data-adults');
  var quota = $('#roomInfo-'+divId).attr('data-quota');
  var trattcode = $('#roomInfo-'+divId).attr('data-trattcode');
  var trattname = $('#roomInfo-'+divId).attr('data-trattname');
  var offertacode = $('#roomInfo-'+divId).attr('data-offertacode');
  var offertatipocode = $('#roomInfo-'+divId).attr('data-offertatipocode');
  var offertaname = $('#roomInfo-'+divId).attr('data-offertaname');

  var data = { "serviceId": serviceId, "serviceType": type, "serviceQuantity": rooms, "nights": nights, "servicePrice": servicePrice,
               "checkIn": startDate, "checkOut": endDate, "rooms": rooms, "roomName": roomName, "roomImage": roomImage,
               "people": people, "children": children, "hotel_code": hotelcode, "room_code": roomcode, "adults": adults, "quota": quota, "tratt_code": trattcode,
               "tratt_name": trattname, "offerta_code": offertacode, "offerta_tipo_code": offertatipocode, "offerta_name": offertaname };

  $.ajax({
    url : "<?php echo base_url(); ?>Cart/add",
    type: "POST",
    data : data,
    success: function(response, textStatus, jqXHR){
      console.log(response)
      if(response) {
          try {
              var data =  JSON.parse(response);
              console.log(data);
              if(data.success == true){
                window.location = '/Cart/index';
              }
              else {
                toastr.warning(data.message);
              }
          } catch(e) {
              toastr.warning(e);

          }
      }
    },
    error: function (textStatus, errorThrown){
      console.log(textStatus)
      console.log(errorThrown)
      toastr.warning(errorThrown);
    }
  });


}

</script>
