<style>
.beach_details
{
	background-color: #f8f9fa;
    border: 3px solid #edeeef;
    border-radius: 5px;
    padding: 14px;
}
.beach_details p
{
	font-weight:bold;
	font-size:14px;
}
.beach_info
{
	padding-left:0px;
}
.beach_info li
{
	border-bottom: 1px solid #e0e0e0;
    list-style: outside none none;
    padding-bottom: 6px;
    padding-top: 6px;
	font-size:14px;
}
.beach_info li i
{
	color: #4098d4;
    font-size: 18px;
    margin-right: 6px;
}
</style>
<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <header class="background-header sights-header paddingbot20 paddingtop100" style="background-image:url('<?php echo base_url(); ?>uploads/beaches/<?php echo $beach->p_cover; ?>');">
        <div class="traditional-event-detail-title">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="main-title"><?php echo $beach->title; ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="page-content paddingbot20 sights-page">
        <!-- <div id="map-canvas-sidebar"></div> -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 paddingbot20">
                    <ul class="breadcrumbs list-inline">
                        <li>
                            <span class="glyphicon glyphicon-home"></span>
                        </li>
                        <li><a href="<?php echo base_url() . $this->lang->lang(); ?>">Procida</a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li><a href="#"><?php echo $beach->title; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row paddingbot40">
                <div class="col-md-6">
                    <div class="row">
                    <div class="col-md-12" style="margin-bottom:10px;">
                        <div id="map-canvas-sidebar" class="img-rounded"></div>
                    </div> 
                        <div class="col-md-12">
                            <?php $add = $this->Ads_model->get('468X60'); ?>
                <?php if ($add): ?>
                    <?php if ($add->src != ''): ?>
                        <?php if ($add->type == 'image'): ?>
                            <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                        <?php else: ?>
                            <?php echo base64_decode($add->src); ?>
                        <?php endif; ?>
                    <?php else: ?>
                       <img src="http://placehold.it/486x60&text=ads+/+468x60" alt="..." class="img-responsive">
                    <?php endif; ?>
                <?php else: ?>
                    <img src="http://placehold.it/486x60&text=ads+/+468x60" alt="..." class="img-responsive">
                <?php endif; ?>
							
							<br>
                            <?php echo base64_decode($beach->description); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 beach_details">
                	<p>Info utili</p>
                    <ul class="beach_info">
                    	<li><i class="fa fa-map-marker"></i><?php echo $beach->address ;?></li>
                        <li><i class="fa fa-bus"></i><?php echo $beach->bus_line ;?></li>
                        <li><i class="fa fa-info-circle"></i><?php echo $beach->privateness ;?></li>
                        <li><i class="fa fa-wheelchair"></i><?php echo $beach->accessibility ;?></li>
                    </ul> 
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- VSP_beach1 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:250px;height:250px"
     data-ad-client="ca-pub-0169409437886826"
     data-ad-slot="1026533103"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
                </div>
                <div class="col-md-3"> 
                    <?php $this->view('frontend/includes/sidebar_activities',array("order_id"=>$beach->id,"table"=>"beaches")); ?> 
                </div>
            </div>
        </div>
    </section>
    <section class="nearby-areas margintop40">
        <div class="container">
            <div class=" paddingtop20">
                <div class="col-md-12">
                    <h4 class="bold nomargin">Near Beaches</h4>
                </div>
            </div>
        </div>

        <div id="<?php echo count($nearbybeaches) > 3 ? 'nearby-areas-carousel' : ''; ?>" class="carousel slide" data-ride="carousel">
            <div class="container">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php
                    $counter = 0;
                    $active = true
                    ?>
                    <?php if (isset($nearbybeaches)): ?>
                        <?php foreach ($nearbybeaches as $nbeach): ?>
                            <?php if ($counter == 0): ?>
                                <div class="item  <?php if ($active) { echo "active"; $active = false; } ?>">
                                     <?php endif; ?>
                                     <?php $beach_loc = $this->Beach_model->getLocation($nbeach->id, $this->lang->lang()); ?>
                                <div class="col-md-4">
                                    <div class="overview-item beach-list">
                                        <a href="<?php echo base_url() . $this->lang->lang() . '/' . lang("beach") . '/' . str_replace(" ", "-", $nbeach->title); ?>">
                                            <div class="overview-image">
                                                <img src="<?php echo base_url(); ?>uploads/beaches/<?php echo $nbeach->p_thumbnail ?>" alt="" class="img-responsive img-rounded">
                                            </div>
                                            <h3><?php echo $nbeach->title; ?></h3>
                                            <p><i class="fa fa-map-marker color2"></i>&nbsp; <?php echo $beach_loc->title; ?></p>
                                        </a>
                                    </div>
                                </div>
                                <?php if ($counter == 2): ?>
                                    <?php $counter = 0; ?>
                                    </div>
                                <?php else: ?>
                                <?php $counter++; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <?php if ($counter < 2 && $counter > 0): ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
				</div>
            </div>
            <!-- Controls -->
            <?php if (count($nearbybeaches) > 3): ?>
                <a class="left carousel-control" href="#nearby-areas-carousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#nearby-areas-carousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            <?php endif; ?>
        </div>
</div>

</section>
<section class="bottom-section gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <?php $add = $this->Ads_model->get('970X90'); ?>
                <?php if ($add): ?>
                    <?php if ($add->src != ''): ?>
                        <?php if ($add->type == 'image'): ?>
                            <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                        <?php else: ?>
                            <?php echo base64_decode($add->src); ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                <?php else: ?>
                    <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>