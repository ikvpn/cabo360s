<?php

/*

 * Author: Innam Hunzai

 * Email: innamhunzai@gmail.com 

 * Project: Visit Procida

 * Version: 2.0

 * File: 

 * Description:

 */

?>

<!--################ WRAP START ################-->

<div id="wrap" class="detail-page-wrapper index"  style="margin: -20px auto 0!important">

    <div style="height:76px"></div>

    <?php $this->view('frontend/includes/booking_search_form'); ?>

    <section class="page-content">

        <div class="container">

            <div class="row">

                <div class="col-md-12 paddingbot20">

                    <ul class="breadcrumbs list-inline">

                        <li>

                            <span class="glyphicon glyphicon-home"></span>

                        </li>

                        <li><a href="<?php echo base_url() . $this->lang->lang(); ?>">Procida</a>

                        </li>

                        <li>

                            <span class="glyphicon glyphicon-chevron-right"></span>

                        </li>

                        <li><a href="#"><?php echo lang("Locations"); ?></a>

                        </li>

                    </ul>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12">

                    <h2 class="sub-title"><?php echo lang('Locations'); ?></h2>

                </div>

            </div>

            <div class="row paddingtop10"> 

                <script type="text/javascript">

                    var markers = [];

                </script>

                <?php if (isset($locations) && !empty($locations)): ?>

                    <?php foreach ($locations as $location): ?> 

                        <script type="text/javascript">

                            markers.push(['<?php  echo addslashes($location->title); ?>', <?php echo $location->lat ?>, <?php echo $location->lng ?>]);

                        </script>

                        <div class="col-md-4">

                            <a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('what-to-visit') . '/' . str_replace(' ', '-', strtolower($location->title)); ?>">

                                <div class="overview-item">

                                    <div class="overview-image">

                                        <img src="<?php echo base_url() . "uploads/locations/" . $location->p_image; ?>" alt="" class="img-responsive img-rounded" style="height: 230px !important;">

                                    </div>

                                    <h3><?php echo $location->title; ?></h3> 

                                    <p>

                                        <!--<a href="<?php /*echo base_url() . $this->lang->lang() */?>/<?php /*echo lang('events') */?>/upcoming-events.html"><?php /*echo $this->Location_model->count_orders_on_location_id($location->id, 6); */?> <?php /*echo lang('Events') */?></a>,-->

                                        <a href="<?php echo base_url() . $this->lang->lang() ?>/hotels"><?php echo $this->Location_model->count_orders_on_location_id($location->id, 1); ?> <?php echo lang('Hotels') ?></a>, 

                                        <a href="<?php echo base_url() . $this->lang->lang() ?>/<?php echo lang('eat-and-drink') ?>/resturants"><?php echo $this->Location_model->count_orders_on_location_id($location->id, 2); ?> <?php echo lang('Restaurants'); ?></a>,

                                        <a href="<?php echo base_url() . $this->lang->lang() ?>/<?php echo lang('what-to-do') ?>/shopping"><?php echo $this->Location_model->count_orders_on_location_id($location->id, 4); ?> <?php echo lang('Shops'); ?></a>

                                    </p>

                                </div>

                            </a>

                        </div>

                    <?php endforeach; ?>

                <?php else: ?>



                <?php endif; ?>

            </div>

        </div>

    </section>



    <section class="map-section">

        <div id="map-canvas"></div>

        <div class="container"> 

        </div>

    </section> 



    <section class="bottom-section hidden-xs hidden-sm">



        <div class="container">

            <div class="row">

                <div class="col-md-12 text-center">

                    <?php $add = $this->Ads_model->get('970X90'); ?>

                    <?php if ($add): ?>

                        <?php if ($add->src != ''): ?>

                            <?php if ($add->type == 'image'): ?>

                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">

                            <?php else:

                                ?>

                                <?php echo base64_decode($add->src); ?>

                            <?php endif; ?>

                        <?php else:

                            ?>

                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">

                        <?php endif; ?>

                    <?php else:

                        ?>

                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">

                    <?php endif; ?>

                </div>

            </div>

        </div>

    </section>

</div>

<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->

<div id="push"></div>



