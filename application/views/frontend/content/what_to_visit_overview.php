<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div> 

    <!--################ HEADER START ################-->
    <!--   <header class="page-title">
<div class="container">
<h2>Features</h2>
</div>
</header> -->

    <section class="page-content paddingtop40 paddingbot40 overview">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="overview-title"><?php echo lang('What_to_visit_in_Procida'); ?></h1>
                </div>
            </div>
           <?php // var_dump($loc_data);?>
            <div class="row paddingtop10">
                <?php if (isset($loc_data) && is_array($loc_data)): ?>
                    <?php if (count($loc_data) > 0): ?>
                        <?php foreach ($loc_data as $item): ?>
                            <div class="col-md-4">
                                <a href="<?php echo base_url().$this->lang->lang().'/'.lang('what-to-visit').'/'.str_replace(' ', '-', $item->enTitle) ?>">
                                <div class="overview-item">
                                    <div class="overview-image">
                                        <img src="<?php echo base_url(); ?>uploads/locations/<?php echo $item->p_image ?>" style="height:220px" alt="" class="img-responsive img-rounded">
                                    </div>
                                    <h3><?php echo $item->title; ?></h3>
                                    <p>
                                        <!--<a href="<?php /*echo base_url().$this->lang->lang() */?>/<?php /*echo lang('events') */?>/upcoming-events.html"><?php /*echo $this->Location_model->count_orders_on_location_id($item->id,6); */?> <?php /*echo lang('Events') */?></a>,-->
                                        <a href="<?php echo base_url().$this->lang->lang() ?>/hotels"><?php echo $this->Location_model->count_orders_on_location_id($item->id,1); ?> <?php echo lang('Hotels') ?></a>, 
                                        <a href="<?php echo base_url().$this->lang->lang() ?>/<?php echo lang('eat-and-drink') ?>/resturants"><?php echo $this->Location_model->count_orders_on_location_id($item->id,2); ?> <?php echo lang('Restaurants'); ?></a>,
                                        <a href="<?php echo base_url().$this->lang->lang() ?>/<?php echo lang('what-to-do') ?>/shopping"><?php echo $this->Location_model->count_orders_on_location_id($item->id,4); ?> <?php echo lang('Shops'); ?></a>
                                    </p>
                                </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?php echo lang('location_not_found_message'); ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    
    <section class="index-map">
        <div class="map-header">
            <div class="container">
                <div class="row">
                    <ul class="list-inline index-map-list">
                        <li>
                            <a href="#" class="active">
                                <h2><i class="picon picon-hotel color3"></i><?php echo lang('Hotels'); ?></h2>
                                <p><?php echo lang('Hotel_Desc'); ?></p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <h2><i class="picon picon-museum color1"></i><?php echo lang('Places_of_Interest'); ?></h2>
                                <p><?php echo lang('Places_of_Interest_Desc'); ?></p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <h2><i class="picon picon-beach-1 color2"></i><?php echo lang('Beaches') ?></h2>
                                <p><?php echo lang('Beaches_Desc'); ?></p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
    <section class="overview-tabs page-tabs paddingtop40 ">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2>
                        <span class="color2 fa fa-compass"></span>&nbsp;<?php echo lang('Routes') ?></h2>
                    <p><?php echo lang('Routes_Desc'); ?></p>

                </div>
                <div class="col-md-4">
                    <h2>
                        <span class="color2 fa fa-bank"></span>&nbsp;<?php echo lang('History'); ?></h2>
                    <p><?php echo lang('History_Desc'); ?></p>

                </div>
                <div class="col-md-4">
                    <h2>
                        <span class="color2 fa fa-child"></span>&nbsp;<?php echo lang('Activities'); ?></h2>
                    <p><?php echo lang('Activities_Desc'); ?></p>

                </div>
            </div>

        </div>

    </section>
    
    <section class="paddingtop40 paddingbot40 overview-links">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <h4><?php echo lang('Places_of_Interest'); ?></h4>
                    <ul class="list-overview fa-ul">
                        <?php $counter = 0; ?>
                        <?php if (isset($locations) || !empty($locations)): ?>
                            <?php foreach ($locations as $location): ?>
                                <?php if ($counter == 6): ?>
                                    <?php break; ?>
                                <?php endif; ?>
                                <li><i class="fa-li fa fa-angle-right"></i><a href="<?php echo base_url() . lang('what-to-visit').'/' . str_replace(' ', '-', $location->enTitle); ?>"><?php echo $location->title; ?></a></li>
                                <?php $counter++; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="col-md-2">
                    <h4><?php echo lang('Hotels') ?></h4>
                    <ul class="list-overview fa-ul">
                        <?php $counter = 0; ?>
                        <?php if (isset($hotels) || !empty($hotels)): ?>
                            <?php foreach ($hotels as $hotel): ?>                 
                                <?php $hotel_array = (array) $hotel; ?>
                                <?php if ($counter == 6): ?>
                                    <?php break; ?>
                                <?php endif; ?>
                                <li><i class="fa-li fa fa-angle-right"></i><a href="<?php echo base_url() . '/hotels'; ?>"><?php echo $hotel_array[$this->lang->lang() . 'Name']; ?></a></li>
                                <?php $counter++; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="col-md-2">
                    <h4><?php echo lang('Restaurants'); ?></h4>
                    <ul class="list-overview fa-ul">
                        <?php $counter = 0; ?>
                        <?php if (isset($resturants) || !empty($resturants)): ?>
                            <?php foreach ($resturants as $resturant): ?>                 
                                <?php $resturant_array = (array) $resturant; ?>
                                <?php if ($counter == 6): ?>
                                    <?php break; ?>
                                <?php endif; ?>
                                <li><i class="fa-li fa fa-angle-right"></i><a href="<?php echo base_url() . lang('eat-and-drink').'/restaurant/' . $resturant->pageTitle; ?>"><?php echo $resturant_array[$this->lang->lang() . 'Name']; ?></a></li>
                                <?php $counter++; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="col-md-2">
                    <h4><?php echo lang('Beaches'); ?></h4>
                    <ul class="list-overview fa-ul">
                        <?php $counter = 0; ?>
                        <?php if (isset($beaches) || !empty($beaches)): ?>
                            <?php foreach ($beaches as $beach): ?>                 
                                <?php if ($counter == 6): ?>
                                    <?php break; ?>
                                <?php endif; ?>
                                <li><i class="fa-li fa fa-angle-right"></i><a href="<?php echo base_url() . lang('beach').'/' . $beach->id; ?>"><?php echo $beach->title; ?></a></li>
                                <?php $counter++; ?>
                            <?php endforeach; ?>
                        <?php endif; ?> 
                    </ul>
                </div>
                <div class="col-md-2">
                    <h4><?php echo lang('Shops') ?></h4>
                    <ul class="list-overview fa-ul">
                         <?php $counter = 0; ?>
                        <?php if (isset($shops) || !empty($shops)): ?>
                            <?php foreach ($shops as $shop): ?>
                                <?php $shop_array = (array) $shop; ?>
                                <?php if ($counter == 6): ?>
                                    <?php break; ?>
                                <?php endif; ?>
                                <li><i class="fa-li fa fa-angle-right"></i><a href="<?php echo base_url() . lang('what-to-do').'/shopping' ?>"><?php echo $shop_array[$this->lang->lang().'Name']; ?></a></li>
                                <?php $counter++; ?>
                            <?php endforeach; ?>
                        <?php endif; ?> 
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
    <section class="bottom-section overview-tabs">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>