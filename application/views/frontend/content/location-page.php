<!--################ WRAP START ################-->
<div id="wrap" class="detail-page-wrapper" style="margin: -20px auto 0!important">
    <!--################ HEADER START ################-->
    <!--   <header class="page-title">
<div class="container">
<h2>Features</h2>
</div>
</header> -->
    <header class="background-header-slider">
        <!-- ############## SLIDER START #################### -->
        <div class="tp-banner-container clearfix">
            <div class="tp-banner">
                <ul>
                    <?php
                    //path to directory to scan
                    $directory = "uploads/locations/" . $location->id . '/';
                    //get all image files with a .jpg extension. This way you can add extension parser
                    $images = glob($directory . "{*.jpg,*.gif}", GLOB_BRACE);
                    $listImages = array();
                    foreach ($images as $image) {
                        ?>
                        <li class='location-image' data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
                            <img src="<?php echo base_url() . $image; ?>" alt="splendida corricella" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazyload="<?php echo base_url() . $image; ?>">
                        </li>
                        <?php
                    }
                    ?>



                </ul>
            </div>
        </div>
        <!-- ############## SLIDER END #################### -->
        <div class="main-page-title locationBoxTitolo">
            <div class="container">
                <div class="row paddingtop50">
                    <div class="col-md-12">
                        <h1 class="main-title locationTitolo"><?php echo $location->title; ?></h1>
                        <p class="locationSottotitolo"><?php echo $location->sott; ?></p>
                    </div>
                  
                </div>
            </div>
        </div>
    </header>

    <section class="page-title">
        <div class="container">
            <div class="row">
    
                <?php $this->view('frontend/includes/booking_search_locform2'); ?>
            </div>
        </div>
    </section>
    <section class="page-content paddingbot60 paddingtop20">
        <div class="container">
            <div class="row">
                <div class="col-md-12 testoLocation">
                    <?php echo base64_decode($location->description); ?>

                </div>
              
            </div>

        </div>
    </section>





    <section class="page-tabs">

<!-- INIZIO ATTIVITA -->

<div id="nearby-areas-carousel" class="carousel slide   attivitaLocation" data-ride="carousel">


                <div class="container">
                        <h2 class="titLoc" style="text-align:  center;"><?php  if($this->lang->lang()=="it"){echo "Cosa fare a ".$location->title;}else{echo "What to do at ".$location->title;} ?></h2>
<p  style="text-align:  center;"><?php  if($this->lang->lang()=="it"){echo "Le migliori attività selezionate da noi";}else{echo "The best activities selected by us";} ?></p>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">                                                                                                                                                              
                                                                                     
 <?php $counter = 0; $primo=" active"; ?>
                                <?php if (is_array($resturants)): ?>
                                    <?php foreach ($resturants as $resturant): ?>
                                        <?php $resturant_array = (array) $resturant; ?>
                                        <?php echo $counter == 0 ? " <div class='item"."$primo'><div class='row'>" : ""; $primo ="";?>
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                                        <div class="project">
                                                            <img src="<?php echo base_url(); ?>uploads/resturants/<?php echo $resturant->photo ?>" alt="" class="project-img" alt="...">
                                                            <span class="overlay"></span>
                                                            <div class="cnt">
                                                                <strong><?php echo lang('Starting_price'); ?></strong>
                                                                <h5>€ <?php echo $resturant->min_price ?>/<?php echo $resturant->max_price ?> pp</h5>
                                                                <a href="<?php echo base_url() . $this->lang->lang() . "/" . lang('eat-and-drink') . "/restaurant/". $resturant->pageTitle; ?>" class="btn btn-purity"><?php echo lang('View_details') ?></a>
                                                            </div>
                                                        </div>
                                                        <div class="tab-item-with-image">
                                                            <h2><a href="<?php echo base_url() . $this->lang->lang() . "/" . lang('eat-and-drink') . "/restaurant/". $resturant->pageTitle; ?>"><?php echo $resturant_array[$this->lang->lang() . 'Name'] ?></a></h2>
                                                            <p class="description"><?php echo substr(strip_tags(base64_decode($resturant_array[$this->lang->lang() . 'Description'])), 0, 45) . '...' ?></p>
                                                            <p class="price">
                                                                <i class="fa color2 fa-star"></i>
                                                                <i class="fa color2 fa-star"></i>
                                                                <i class="fa color2 fa-star"></i>
                                                                <i class="fa color2 fa-star"></i>
                                                            </p>
                                                        </div>
                                        </div>
                                        <?php $counter++; ?>
                                        <?php echo $counter == 4 ? '</div></div>' : ''; ?>
                                        <?php
                                        if ($counter == 4) {
                                            $counter = 0;
                                        }
                                        ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>

<?php foreach ($places as $place): ?>
                                        <?php $place_array = (array) $place; ?>
                                       <?php echo $counter == 0 ? " <div class='item"."$primo'><div class='row'>" : ""; $primo ="";?>
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="project">
                                                <img src="<?php echo base_url(); ?>uploads/places_of_interest/<?php echo $place->photo ?>" alt="" class="project-img" alt="...">
                                                <span class="overlay"></span>
                                                <div class="cnt">
                                                    <strong><?php echo lang('Ticket_price'); ?></strong>
                                                    <h5><?php echo $place->pAdults == 0 ? 'free' : " € ".$place->pAdults; ?> </h5>
                                                    <a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('places-of-interest') . '/' . str_replace(' ', '-', strtolower($place_array['rewrite_url_' . $this->lang->lang()] != '' ? $place_array['rewrite_url_' . $this->lang->lang()] : $place_array[$this->lang->lang() . 'Title'])); ?>" class="btn btn-purity"><?php echo lang('View_details'); ?></a>
                                                </div>

                                            </div>
                                            <div class="tab-item-with-image">
                                                <h2><a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('places-of-interest') . '/' . str_replace(' ', '-', strtolower($place_array['rewrite_url_' . $this->lang->lang()] != '' ? $place_array['rewrite_url_' . $this->lang->lang()] : $place_array[$this->lang->lang() . 'Title'])); ?>"><?php echo $place_array[$this->lang->lang() . 'Title']; ?></a></h2>
                                                <?php $desc = base64_decode($place_array[$this->lang->lang() . 'Description']); ?>
                                                <p class="description"><?php echo strlen($desc) > 44 ? substr(strip_tags($desc), 0, 43) . '...' : strip_tags($desc); ?></p>
                                                <p class="color1 bold">
                                                    <i class="fa fa-angle-right color1"></i>&nbsp;<a class="color1" href="<?php echo base_url() . $this->lang->lang() . '/' . lang('places-of-interest') . '/' . str_replace(' ', '-', strtolower($place_array['rewrite_url_' . $this->lang->lang()] != '' ? $place_array['rewrite_url_' . $this->lang->lang()] : $place_array[$this->lang->lang() . 'Title'])); ?>"><?php echo lang('Read_more') ?></a>
                                                </p>
                                            </div>
                                        </div>
                                          <?php $counter++; ?>
                                        <?php echo $counter == 4 ? '</div></div>' : ''; ?>
                                        <?php
                                        if ($counter == 4) {
                                            $counter = 0;
                                        }
                                        ?>
                                    <?php endforeach; ?>


  <?php if (is_array($hotels)): ?>
                                    <?php foreach ($hotels as $hotel): ?>
                                        <?php $hotel_array = (array) $hotel; ?>
                                     <?php echo $counter == 0 ? " <div class='item"."$primo'><div class='row'>" : ""; $primo ="";?>

                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="project">
                                                <img src="<?php echo base_url() . 'uploads/hotels/' . $hotel->photo; ?>" alt="" class="project-img" alt="...">
                                                <span class="overlay"></span>
                                                <div class="cnt">
                                                    <strong><?php echo lang('Starting_price') ?></strong>
                                                    <h5>€ <?php echo $hotel->price ?>/N</h5>
                                                    <a href='<?php echo $hotel->url ?>' alt='<?php echo $hotel->enName ?>' class="btn btn-purity"><?php echo lang('View_details') ?></a>
                                                </div>
                                            </div>
                                            <div class="tab-item-with-image">
                                                <h2><a href='<?php echo $hotel->url ?>' alt='<?php echo $hotel->enName ?>'><?php echo $hotel_array[$this->lang->lang() . 'Name'] ?></a></h2>
                                                <p class="description"><?php echo substr(strip_tags(base64_decode($hotel_array[$this->lang->lang() . 'Description'])), 0, 45) . '...' ?></p>
                                                <p class="price">
                                                    <i class="fa color2 fa-star"></i>
                                                    <i class="fa color2 fa-star"></i>
                                                    <i class="fa color2 fa-star"></i>
                                                    <i class="fa color2 fa-star"></i>
                                                </p>
                                            </div>
                                        </div>
                                        <?php $counter++; ?>
                                        <?php echo $counter == 4 ? '</div></div>' : ''; ?>
                                        <?php
                                        if ($counter == 4) {
                                            $counter = 0;
                                        }
                                        ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>

 <?php if (is_array($shops)): ?>
                                    <?php foreach ($shops as $shop): ?>
                                        <?php $shop_array = (array) $shop; ?>
                                       <?php echo $counter == 0 ? " <div class='item"."$primo'><div class='row'>" : ""; $primo ="";?>
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="project">
                                                <img src="<?php echo base_url(); ?>uploads/shopes/<?php echo $shop->photo ?>" alt="" class="project-img" alt="...">
                                                <span class="overlay"></span>
                                                <div class="cnt">
                                                    <strong><?php echo lang('Starting_price') ?></strong>
                                                    <h5><?php echo $shop->minPrice ?>/<?php echo $shop->maxPrice; ?>€</h5>
                                                    <a href="<?php echo base_url() . $this->lang->lang() . "/" . lang("what-to-do") . "/shopping/" . $shop->title ?>" class="btn btn-purity"><?php echo lang('View_details'); ?></a>
                                                </div>
                                            </div>
                                            <div class="tab-item-with-image">
                                                <h2><a href="<?php echo base_url() . $this->lang->lang() . "/" . lang("what-to-do") . "/shopping/" . $shop->title ?>"><?php echo $shop_array[$this->lang->lang() . 'Name'] ?></a></h2>
                                                <p class="description"><?php echo substr(strip_tags(base64_decode($shop_array[$this->lang->lang() . 'Description'])), 0, 45) . '...' ?></p>
                                                <p class="color5 bold">
                                                    <a href="<?php echo base_url() . $this->lang->lang() . "/" . lang("what-to-do") . "/shopping/" . $shop->title ?>"><i class="fa fa-angle-right color5"></i>&nbsp;<?php echo lang('More_details') ?></a>
                                                </p>
                                            </div>
                                        </div>
                                        <?php $counter++; ?>
                                        <?php echo $counter == 4 ? '</div></div>' : ''; ?>
                                        <?php
                                        if ($counter == 4) {
                                            $counter = 0;
                                        }
                                        ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>



                                <?php echo $counter > 0 && $counter < 4 ? '</div></div>' : ''; //chiusura ciclo attivita?>



        

                                                                                                                                                                                        </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#nearby-areas-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#nearby-areas-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>



<!-- FINE ATTIVITA -->
   

    </section>


    <section class="map-section">
        <div id="map-canvas"></div>
    </section>




<section> 

       <div class="container">
                    <div class="row">
               <div class="col-md-12">

                <?php foreach ($tours as $key => $tour) {
                    if($tour->locationName==$location->title) {?>
                <h2 class="titLoc" style="text-align:  center;"><?php  if($this->lang->lang()=="it"){echo "Prenota un tour";}else{echo "Book a tour";} ?></h2>
<p  style="text-align:  center;"><?php  if($this->lang->lang()=="it"){echo "Tour a ".$location->title;}else{echo "Tours at ".$location->title;} ?></p>
                    <?php break;}
                    else{}
                } ?>

                          <div class="row">
                            <?php foreach ($tours as $key => $tour): ?>
                           
                               <?php    if($tour->locationName==$location->title) {?>
                              <script type="text/javascript">
                                  markers.push(['<?php  echo addslashes($tour->itTitle); ?>', <?php echo $tour->lat ?>, <?php echo $tour->long ?> ]);
                              </script>
                                  <div class="col-md-3 listing-tour" data-type="<?php echo $tour->type; ?>">
                                    <a href="<?php echo base_url().$this->lang->lang()."/tour/".$tour->slug; ?>">
                                      <div class="project">
                                          <?php if ($tour->thumbnail != null && $tour->thumbnail != '' && $tour->thumbnail != ' '): ?>
                                            <img src="<?php echo base_url() . "uploads/tours/" . $tour->thumbnail; ?>" class="img-responsive project-img img-rounded" alt="<?php echo  $this->lang->lang() == 'it' ?  $tour->itTitle : $tour->enTitle; ?>">
                                          <?php else: ?>
                                            <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                                            <img class="img-responsive project-img img-rounded" src="<?php echo $scr; ?>" alt="<?php echo  $this->lang->lang() == 'it' ?  $tour->itTitle : $tour->enTitle; ?>">
                                          <?php endif; ?>
                                          <label class="price">€ <?php echo $tour->price; ?></label>
                                      </div>
                                      </a>
                                      <div class="tab-item-with-image">
                                             <h2><a href="<?php echo base_url().$this->lang->lang()."/tour/".$tour->slug; ?>"><?php echo  $this->lang->lang() == 'it' ?  $tour->itTitle : $tour->enTitle; ?></a></h2>
                                              <p class="location "><i class="fa colorTour fa-map-marker"></i> <?php echo $tour->locationName; ?></p>
                                          </div>
                                        </div>

                                    <?php  }endforeach; ?>
                              </div>
                        </div>
       </div>
                        </div>
  </section>










    <section class="nearby-areas">
        <div class="container">
            <div class="col-md-12 paddingtop20 text-center">
                <h4 class="bold nomargin"><?php echo lang('Near_by_areas'); ?></h4>
            </div>
        </div>

        <?php if (is_array($near_locations) && count($near_locations)): ?>
            <div id="nearby-areas-carousel" class="carousel slide" data-ride="carousel">
                <div class="container">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php $starter = 0; ?>
                        <?php $active = TRUE; ?>
                        <?php foreach ($near_locations as $loc): ?>
                            <?php
                            if ($loc->id == $location->id):continue;
                            endif;
                            ?>
                            <?php if ($starter == 0): ?>
                                <?php echo $active == TRUE ? "<div class='item  active'>" : "<div class='item'>"; ?>
                                <?php $active = false; ?>
                            <?php endif; ?>
                            <div class="col-md-4 col-xs-12 col-sm-6">
                                <div class="overview-item">
                                    <div class="overview-image">
                                        <div class="project">
                                            <img src="<?php echo base_url(); ?>uploads/locations/<?php echo $loc->p_image; ?>" style="height:220px" alt="" class="img-responsive img-rounded">
                                            <span class="overlay"></span>
                                            <div class="cnt">
                                                <a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('what-to-visit') . '/' . str_replace(' ', '-', strtolower($loc->enTitle)); ?>" class="btn btn-purity"><?php echo lang('View_details'); ?></a>
                                            </div>
                                        </div>

                                    </div>
                                    <h3><?php echo $loc->title ?></h3>
                                    <p>
                                        <!--<a href="<?php /*echo base_url() . $this->lang->lang() */?>/events/upcoming-events.html"><?php /*echo $this->Location_model->count_orders_on_location_id($loc->id, 6); */?> <?php /*echo lang('Events'); */?></a>,-->
                                        <a href="<?php echo base_url() . $this->lang->lang() ?>/what-to-to/shopping"><?php echo $this->Location_model->count_orders_on_location_id($loc->id, 4); ?> <?php echo lang('Shops'); ?></a>,
                                        <a href="<?php echo base_url() . $this->lang->lang() ?>/eat-and-drink/resturants"><?php echo $this->Location_model->count_orders_on_location_id($loc->id, 2); ?> <?php echo lang('Restaurants'); ?></a>,
                                        <a href="<?php echo base_url() . $this->lang->lang() ?>/where-to-stay/hotels"><?php echo $this->Location_model->count_orders_on_location_id($loc->id, 1); ?> <?php echo lang('Hotels') ?></a>
                                    </p>
                                </div>
                            </div>
                            <?php $starter++; ?>
                            <?php if ($starter == 3): ?>
                                <?php
                                echo "</div>";
                                $starter = 0;
                                ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <?php if ($starter > 0 && $starter < 3): ?>
                            <?php echo"</div>"; ?>
                        <?php endif; ?>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#nearby-areas-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#nearby-areas-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        <?php endif; ?>
    </section>

    <section class="bottom-section hidden-xs hidden-sm">

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
