<script src="https://js.stripe.com/v2/"></script>
<div id="wrap" class="detail-page-wrapper">
  <div class="menu-bg"></div>
  <?php $this->view('frontend/includes/booking_search_form'); ?>
  <section class="page-content cartview">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <?php if(!$success || ($error != null && $error != '')){ ?>
            <div class="form_title">
              <h3><strong><i class="fa fa-times"></i></strong><?php echo lang('error'); ?></h3>
              <p><?php echo $error; ?></p>
              <br />
              <br />
            </div>
          <?php } else { ?>
          <div class="form_title">
            <h3><strong><i class="fa fa-check"></i></strong><?php echo lang('thank'); ?></h3>
            <p></p>
          </div>
          <div class="step">
            <p><?php echo lang('thank_text'); ?></p>
          </div><!--End step -->
          <div class="form_title">
            <h3><strong><i class="fa fa-tag"></i></strong><?php echo lang('booking_summary'); ?></h3>
          </div>
          <?php }  ?>
          <?php if($success['hotel']) { ?>

          <div class="step">
            <table class="table confirm">
              <tbody>
                <tr>
                  <td> <strong>HOTEL</strong></td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    <strong><?php echo lang('name'); ?></strong>
                  </td>
                  <td>
                    <?php echo $bookingInfo['firstname'].' '.$bookingInfo['lastname']; ?>
                  </td>
                </tr>
                <?php foreach ($bookingInfo['rooms'] as $room) { ?>
                  <tr>
                    <td>
                      <strong>Check in</strong>
                    </td>
                    <td>
                      <?php echo $room['checkIn']; ?><br>
                    </td>
                  </tr>
                  <tr>
                    <td><strong>Check out</strong></td>
                    <td>
                      <?php echo $room['checkOut']; ?><br>
                     </td>
                  </tr>
                  <tr>
                    <td><strong>Hotel</strong></td>
                    <td><?php echo $room['hotelName']; ?></td>
                  </tr>
                  <tr>
                    <td><strong><?php echo lang('rooms'); ?></strong></td>
                    <td><?php echo $room['rooms'].' '.$room['roomName']; ?></td>
                  </tr>
                  <tr>
                    <td><strong><?php echo lang('nights'); ?></strong></td>
                    <td><?php echo $room['nights']; ?></td>
                  </tr>
                  <tr>
                    <td><strong><?php echo lang('adults'); ?></strong></td>
                    <td><?php echo $room['adults']; ?></td>
                  </tr>
                  <tr>
                    <td><strong><?php echo lang('children'); ?></strong></td>
                    <td><?php echo $room['children']; ?></td>
                  </tr>
                <?php } ?>
                <tr>
                  <td>
                    <strong><?php echo lang('payment_type'); ?></strong>
                  </td>
                  <td>
                    <?php echo $bookingInfo['paymentType']; ?>
                  </td>
                </tr>
                <tr >
                  <td>
                    <strong><?php echo lang('total_cost'); ?></strong>
                  </td>
                  <td >
                    € <?php echo $room['price']; ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div><!--End step -->

          <?php } ?>


          <?php if($success['tour']){ ?>

            <div class="step">
              <table class="table confirm">
                <tbody>
                  <tr>
                    <td> <strong>TOUR</strong></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>
                      <strong><?php echo lang('name'); ?></strong>
                    </td>
                    <td>
                      <?php echo $bookingInfo['firstname'].' '.$bookingInfo['lastname']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td> <strong>Tour Name</strong></td>
                    <td> <?php echo $bookingInfo['tour']['name']; ?> </td>
                  </tr>
                  <tr>
                    <td> <strong><?php echo lang('date'); ?></strong></td>
                    <td> <?php echo $bookingInfo['tour']['date']; ?> </td>
                  </tr>
                  <tr>
                    <td> <strong><?php echo lang('hour'); ?></strong></td>
                    <td> <?php echo $bookingInfo['tour']['hour']; ?> </td>
                  </tr>
                  <tr>
                    <td> <strong><?php echo lang('duration'); ?></strong></td>
                    <td> <?php echo $bookingInfo['tour']['duration']; ?> </td>
                  </tr>
                  <tr>
                    <td> <strong><?php echo lang('adults'); ?></strong></td>
                    <td> <?php echo $bookingInfo['tour']['adults']; ?> </td>
                  </tr>
                  <tr>
                    <td> <strong><?php echo lang('children'); ?></strong></td>
                    <td> <?php echo $bookingInfo['tour']['children']; ?> </td>
                  </tr>
                  <tr>
                    <td><strong><?php echo lang('payment_type'); ?></strong></td>
                    <td><?php echo $bookingInfo['paymentType']; ?></td>
                  </tr>
                  <tr>
                    <td><strong><?php echo lang('total_cost'); ?></strong></td>
                    <td >€ <?php echo $bookingInfo['tour']['payment']; ?></td>
                  </tr>
                </tbody>
              </table>
            </div><!--End step -->
          <?php } ?>



          <?php if($success['transfer']){ ?>

            <div class="step">
              <table class="table confirm">
                <tbody>
                  <tr>
                    <td> <strong><?php echo lang('transfer_Up'); ?></strong></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>
                      <strong><?php echo lang('name'); ?></strong>
                    </td>
                    <td>
                      <?php echo $bookingInfo['firstname'].' '.$bookingInfo['lastname']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td> <strong>Check In</strong></td>
                    <td> <?php echo $bookingInfo['checkIn']; ?> </td>
                  </tr>
                  <tr>
                    <td> <strong>Check Out</strong></td>
                    <td> <?php echo $bookingInfo['checkOut']; ?> </td>
                  </tr>
                  <tr>
                    <td> <strong><?php echo lang('people'); ?></strong></td>
                    <td> <?php echo $bookingInfo['people']; ?> </td>
                  </tr>
                  <?php if(isset($bookingInfo['returnPeople'])) : ?>
                  <tr>
                    <td> <strong><?php echo lang('return_people'); ?></strong></td>
                    <td> <?php echo $bookingInfo['returnPeople']; ?> </td>
                  </tr>
                  <?php endif; ?>
                  <tr>
                    <td> <strong><?php echo lang('forward_trip_pickup'); ?></strong></td>
                    <td> <?php echo $bookingInfo['pickupAddress']; ?> </td>
                  </tr>
                  <tr>
                    <td> <strong><?php echo lang('forward_trip_dropoff'); ?></strong></td>
                    <td> <?php echo $bookingInfo['dropoffAddress']; ?> </td>
                  </tr>
                  <tr>
                    <td> <strong><?php echo lang('return_trip_pickup'); ?></strong></td>
                    <td><?php echo $bookingInfo['pickupReturnAddress']; ?></td>
                  </tr>
                  <tr>
                    <td> <strong><?php echo lang('return_trip_dropoff'); ?></strong></td>
                    <td><?php echo $bookingInfo['dropoffReturnAddress']; ?></td>
                  </tr>
                  <tr>
                    <td><strong><?php echo lang('payment_type'); ?></strong></td>
                    <td><?php echo $bookingInfo['paymentType']; ?></td>
                  </tr>
                  <tr>
                    <td><strong><?php echo lang('total_cost'); ?></strong></td>
                    <td >€ <?php echo $bookingInfo['transferPayment']; ?></td>
                  </tr>
                </tbody>
              </table>
            </div><!--End step -->
          <?php } else { ?>
            <div class="col-lg-12 hidden-xs hidden-sm">
            <div class="row">
              <div class="form_title">
                <h3><strong><i class="fa fa-ship"></i></strong><?php echo lang('need_ferry'); ?></h3>
              </div>
              <div class="col-lg-12 step">
                <div class="col-lg-4">
                  <div class="input-group pad15">
                    <input id="desktopBookingDate" type="text" class="form-control  datepicker" name="check_in" placeholder="Departure">
                    <div class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group pad15">
                    <select id="traghettilineDesktopSearch" class="form-control tratte">
                      <?php if ($this->lang->lang() == 'en') { ?>
                        <option value=""> Route </option>
                        <optgroup label="to Ischia Island" per="True">
                          <option value="Napoli-Procida" per="True">Naples - Procida</option>
                          <option value="Napoli Beverello-Procida" per="True">Naples Beverello - Procida</option>
                          <option value="Pozzuoli-Procida" per="True">Pozzuoli - Procida</option>
                          <option value="Procida-Casamicciola" per="True">Procida - Casamicciola</option>
                          <option value="Procida-Ischia" per="True">Procida - Ischia</option>
                        </optgroup>
                        <optgroup label="from Ischia Island" per="True">
                          <option value="Casamicciola-Procida" per="False">Casamicciola - Procida</option>
                          <option value="Ischia-Procida" per="False">Ischia - Procida</option>
                          <option value="Procida-Napoli" per="False">Procida - Naples</option>
                          <option value="Procida-Napoli Beverello" per="False">Procida - Naples Beverello</option>
                          <option value="Procida-Pozzuoli" per="False">Procida - Pozzuoli</option>
                        </optgroup>
                        <?php } else { ?>
                          <option value=""> Tratta </option>
                          <optgroup label="per l'Isola di Ischia" per="True">
                            <option value="Napoli-Procida" per="True">Napoli - Procida</option>
                            <option value="Napoli Beverello-Procida" per="True">Napoli Beverello - Procida</option>
                            <option value="Pozzuoli-Procida" per="True">Pozzuoli - Procida</option>
                            <option value="Procida-Casamicciola" per="True">Procida - Casamicciola</option>
                            <option value="Procida-Ischia" per="True">Procida - Ischia</option>
                          </optgroup>
                          <optgroup label="dall'Isola di Ischia" per="True">
                            <option value="Casamicciola-Procida" per="False">Casamicciola - Procida</option>
                            <option value="Ischia-Procida" per="False">Ischia - Procida</option>
                            <option value="Procida-Napoli" per="False">Procida - Napoli</option>
                            <option value="Procida-Napoli Beverello" per="False">Procida - Napoli Beverello</option>
                            <option value="Procida-Pozzuoli" per="False">Procida - Pozzuoli</option>
                          </optgroup>
                          <?php } ?>
                        </select>
                      </div>
                  </div>
                  <div class="col-lg-4 ferryDiv">
                      <a class="btn btn-block btn-ferry-desktop" id="bookingFerryDesktop"><?php echo lang('search'); ?></a>
                    </div>
              </div>
            </div>
          </div>
            <div class="hidden-lg hidden-md" style="margin:20px 0;">
              <div>
                  <div class="input-group pad15">
                      <input id="bookingDate" type="text" class="form-control  datepicker" name="check_in" placeholder=" Departure">
                      <div class="input-group-addon">
                          <span class="fa fa-calendar"></span>
                      </div>
                  </div>
              </div>
              <div>
                <div class="form-group pad15">
                  <select id="traghettilineSearch" class="form-control tratte">
                    <?php if ($this->lang->lang() == 'en') { ?>
                      <option value=""> Route </option>
                      <optgroup label="to Ischia Island" per="True">
                        <option value="Napoli-Procida" per="True">Naples - Procida</option>
                        <option value="Napoli Beverello-Procida" per="True">Naples Beverello - Procida</option>
                        <option value="Pozzuoli-Procida" per="True">Pozzuoli - Procida</option>
                        <option value="Procida-Casamicciola" per="True">Procida - Casamicciola</option>
                        <option value="Procida-Ischia" per="True">Procida - Ischia</option>
                      </optgroup>
                      <optgroup label="from Ischia Island" per="True">
                        <option value="Casamicciola-Procida" per="False">Casamicciola - Procida</option>
                        <option value="Ischia-Procida" per="False">Ischia - Procida</option>
                        <option value="Procida-Napoli" per="False">Procida - Naples</option>
                        <option value="Procida-Napoli Beverello" per="False">Procida - Naples Beverello</option>
                        <option value="Procida-Pozzuoli" per="False">Procida - Pozzuoli</option>
                      </optgroup>
                    <?php }else{ ?>
                      <option value=""> Tratta </option>
                      <optgroup label="per l'Isola di Ischia" per="True">
                        <option value="Napoli-Procida" per="True">Napoli - Procida</option>
                        <option value="Napoli Beverello-Procida" per="True">Napoli Beverello - Procida</option>
                        <option value="Pozzuoli-Procida" per="True">Pozzuoli - Procida</option>
                        <option value="Procida-Casamicciola" per="True">Procida - Casamicciola</option>
                        <option value="Procida-Ischia" per="True">Procida - Ischia</option>
                      </optgroup>
                      <optgroup label="dall'Isola di Ischia" per="True">
                        <option value="Casamicciola-Procida" per="False">Casamicciola - Procida</option>
                        <option value="Ischia-Procida" per="False">Ischia - Procida</option>
                        <option value="Procida-Napoli" per="False">Procida - Napoli</option>
                        <option value="Procida-Napoli Beverello" per="False">Procida - Napoli Beverello</option>
                        <option value="Procida-Pozzuoli" per="False">Procida - Pozzuoli</option>
                      </optgroup>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="ferryDiv">
                <a class="btn btn-block btn-ferry" id="bookingFerry"><?php echo lang('search'); ?></a>
              </div>
          </div>
          <?php } ?>
        </div>

        <aside class="col-md-4 col-xs-12">
          <div class="box_style_1 col-xs-12">
      			<h3 class="inner"><?php echo lang('thank'); ?></h3>
      			<p><?php echo lang('thank_text_right'); ?></p>

            <a onclick="window.print()" class="btn btn-purity btn-block "><?php echo lang('print'); ?></a>
            </body>

      		</div>
      		<div class="box_style_4 col-xs-12 ">
      		    <i class="icon_set_1_icon-57"></i>
      		    <h4><?php echo lang('need_help'); ?> </h4>
      		    <a href="tel://+390818960454" class="phone">+39 081 8960454</a>
      		    <small><?php echo lang('hours_tel'); ?></small>
      		</div>
        </aside><!-- End aside -->
      </div>
    </div>
  </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
<script>
$('#bookingFerryDesktop').on('click',function() {
  var lang = '<?php echo $this->lang->lang(); ?>';
  var date = $('#desktopBookingDate').val();
  var pars = date.split("/");
  var check_day = parseInt(pars[1], 10);
  var check_month = parseInt(pars[0], 10);
    if(check_month < 10)
      check_month = '0'+check_month;
  var check_year = parseInt(pars[2], 10);
  date = check_year+'-'+check_month+'-'+check_day;
  var route = $('#traghettilineDesktopSearch option:selected').val();
  var url = '<?php echo base_url().$this->lang->lang(); ?>/info-utili/ferry-time-table?route='+route+'&date='+date;
  window.location = url;
});

$('#bookingFerry').on('click',function() {
	var lang = '<?php echo $this->lang->lang(); ?>';
	var date = $('#bookingDate').val();
	var pars = date.split("/");
	var check_day = parseInt(pars[1], 10);
	var check_month = parseInt(pars[0], 10);
			if(check_month < 10)
				check_month = '0'+check_month;
	var check_year = parseInt(pars[2], 10);
	date = check_year+'-'+check_month+'-'+check_day;
	var route = $('#traghettilineSearch option:selected').val();
	var url = '<?php echo base_url().$this->lang->lang(); ?>/info-utili/ferry-time-table?route='+route+'&date='+date;
	window.location = url;
});
</script>
