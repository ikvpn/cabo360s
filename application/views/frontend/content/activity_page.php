<!--################ WRAP START ################-->
<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <div class="hidden-xs no-padding">
    	<?php $this->view('frontend/includes/booking_search_form'); ?>
    </div>
    <?php $item_array = (array) $activity ?>

    <header class="background-header-slider">
        <!-- ############## SLIDER START #################### -->
        <div class="tp-banner-container clearfix">
            <div class="tp-banner">
                <ul>
                    <?php
                    //path to directory to scan
                    $directory = "uploads/".$activity->uploadsFolder."/" . $item_array['id'] . '/';
                    //get all image files with a .jpg extension. This way you can add extension parser
                    $images = glob($directory . "{*.jpg,*.gif}", GLOB_BRACE);
                    $listImages = array();
                    if(count($images) > 0){
                      foreach ($images as $image) {
                          ?>
                          <li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
                              <img src="<?php echo base_url() . $image; ?>" alt="splendida corricella" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazyload="<?php echo base_url() . $image; ?>">
                          </li>
                          <?php
                      }
                    }
                    else {
                      $image = 'assets/images/slider.jpg'; ?>
                      <li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
                          <img src="<?php echo base_url() . $image; ?>" alt="splendida corricella" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazyload="<?php echo base_url() . $image; ?>">
                      </li>
                        <?php
                    }

                    ?>
                </ul>
            </div>

            <div id="detail_slider" class="main-page-title">
                <div class="container">
                    <div class="row paddingtop50">
                        <div class="col-md-8 banner-title">
                        	<h5>
            				<?php
            					if($this->uri->segment(1) == 'it')
            					{	echo $activity->itSlogan;	}
            					else
            					{	echo $activity->enSlogan; 	}?></h5>
                            <h1 class="main-title"><?php echo $item_array[$this->lang->lang() . "Name"]; ?></h1>
                            <p class="hidden-xs">
                            	<span class="glyphicon glyphicon-map-marker"></span>&nbsp;<?php echo $activity->address; ?>, <a href="<?php if($activity->website != '' && $activity->website != "-" ) { echo $activity->website; }else{ echo '#'; } ?>" target="_blank"><span class="glyphicon glyphicon-new-window"></span>
                            </p></a>
                        </div>
                        <div class="col-md-4">
                            <div class="photo-credits">
                                <h5 style="height: 12px;"></h5>

                                <?php if (property_exists($activity, 'menu_price')):
                                  $price = explode('/', $activity->menu_price);
                                  $price = $price[0];
                                ?>
                                <?php else:
                                  $price = property_exists($activity ,'min_price') ? $activity->min_price : ( property_exists($activity ,'minPrice') ? $activity->minPrice : '');
                                ?>
                                <?php endif; ?>

            					          <h1><span><?php echo lang("starting_from"); ?></span> € <?php  echo $price; ?><span><?php if($activity->aPartire!=""){echo "/".$activity->aPartire;} ?></span></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ############## SLIDER END #################### -->
    </header>

    <section class="page-content paddingbot20 sights-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12 paddingbot20">
                    <ul class="breadcrumbs list-inline">
                        <li>
                            <span class="glyphicon glyphicon-home"></span>
                        </li>
                        <li><a href="<?php echo base_url() . $this->lang->lang(); ?>">Procida</a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . $this->lang->lang() . lang('what_to_do') . "/"?>escursions-and-visit">

                          <?php echo lang($activity->breadcrumb_name); ?>
                        </a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li><a href="#"><?php echo $item_array[$this->lang->lang() . "Name"]; ?></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12 paddingtop20">
                            <h2 class="sub-title"><?php echo lang("Info"); ?></h2>
                            <?php echo $item_array[$this->lang->lang() . "Description"]; ?>
                        </div>
                        <div class="col-md-12 paddingtop20">
                            <hr class="procida">
                            <div class="row">
                              <div class="col-md-6">
                                  <div class="detail-summary">
                                      <p>
                                          <strong><?php echo lang("Price_Range"); ?></strong>
                                          <?php if (property_exists($activity, 'min_price')): ?>
                                              <span>€ <?php echo $activity->min_price; ?> / <?php echo $activity->max_price; ?></span>
                                          <?php elseif (property_exists($activity, 'minPrice')): ?>
                                              <span>€ <?php echo $activity->minPrice; ?> / <?php echo $activity->maxPrice; ?></span>
                                          <?php elseif (property_exists($activity, 'menu_price')): ?>
                                              <span>€ <?php echo $activity->menu_price; ?></span>
                                          <?php endif; ?>

                                      </p>
                                  </div>
                              </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang("hours") ?></strong>
                                            <span><?php echo $activity->hours; if($activity->hours2 != NULL) { echo ' / '.$activity->hours2 ; } ?></span>
                                        </p>
                                    </div>
                                </div>
                                <?php if (property_exists($activity, 'category')): ?>
                                  <div class="col-md-6">
                                      <div class="detail-summary">
                                          <p>
                                              <strong><?php echo lang('category'); ?></strong>
                                              <span><?php echo $activity->category; ?></span>
                                          </p>
                                      </div>
                                  </div>
                                <?php endif; ?>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang("Closing_Day"); ?></strong>
                                            <span><?php
                                                echo $activity->closingDay
                                                ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang("Credit_Card");?></strong>
                                            <span>
                                                <?php $cards = explode(',', $activity->creditCards); ?>
                                                <?php foreach ($cards as $card): ?>
                                                    <?php if (file_exists("assets/images/" . trim($card) . ".png")): ?>
                                                        <img src="<?php echo base_url() . "assets/images/" . trim($card) . ".png" ?>" height="30">
                                                    <?php endif;
                                                    ?>
                                                <?php endforeach; ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang("Info"); ?></strong>
                                            <span><?php echo $activity->Info; ?></span>
                                        </p>
                                    </div>
                                </div>
                                <?php if(isset($activity->fbWidget)) {?>
                                <div class="col-md-6">
                                    <div class="detail-summary" style="margin-top: -17px;">
                            							<?php echo $activity->fbWidget; ?>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="col-md-12 paddingtop20">
                                  <?php foreach ($activity->activity_services as $key => $value): ?>
                                    <span class="label label-primary"><?php if ($this->lang->lang() == 'en') { echo $value->nameEN; } else {echo $value->nameIT;} ?></span>
                                  <?php endforeach; ?>

                                  <?php foreach ($activity->activity_tag as $key => $value): ?>
                                    <span class="label label-primary"><?php if ($this->lang->lang() == 'en') { echo $value->nameEN; } else {echo $value->nameIT;} ?></span>
                                  <?php endforeach; ?>

                                  <?php foreach ($activity->activity_categories as $key => $value): ?>
                                    <span class="label label-primary"><?php echo lang($value->table); ?></span>
                                  <?php endforeach; ?>

                                </div>



                            </div>
                        </div>

                        <div class="col-md-12 paddingtop20">
                            <h2 class="sub-title">Live from <?php echo $item_array[$this->lang->lang() . "Name"]; ?></h2>
                            <hr class="procida">
                            <?php if(isset($activity->enInsta))
            								  {
            									  echo $activity->enInsta;
            								  }
          							    ?>
                            <?php if (isset($this->Settings_model->instagram)): ?>
                                <?php echo $this->Settings_model->instagram; ?>
                                <div class="clearfix"></div>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                	<div class="paddingtop10 contact_forms text-center">
                	    <h4 class="noMarginBottom"><?php echo lang("contact_from"); ?></h4>
                	    <h3 class="noMarginTop"><strong><?php echo $activity->manager ?></strong></h3>
                      <?php if ($activity->manager_profile != null && $activity->manager_profile != ''): ?>
                        <?php $url = base_url() . "uploads/".$activity->uploadsFolder."/".$activity->manager_profile; ?>
                        <?php if (@getimagesize($url)): ?>
                            <img class="img-responsive img-circle" src="<?php echo base_url() . "uploads/".$activity->uploadsFolder."/".$activity->manager_profile ?> "/>
                        <?php else: ?>
                            <img class="img-responsive img-circle" src="<?php echo base_url() . "assets/images/user-icon-placeholder.png" ?> "/>
                        <?php endif; ?>
                      <?php else: ?>
                        <img class="img-responsive img-circle" src="<?php echo base_url() . "assets/images/user-icon-placeholder.png" ?> "/>
                      <?php endif; ?>

                	    <h3 class="phone"> <i style="color:#90B323" class="glyphicon glyphicon-earphone color2"></i> <?php echo $activity->phone ?></h3>
                	    <a href="#" class="btn btn-purity btn-lg btn-block btn-request btn-sendemail btn80" data-orderid="<?php echo $activity->order_id ?>" data-type="restaurants"><?= lang("Send_a_request");?> <span class="glyphicon glyphicon-chevron-right"></span></a>
                	</div>
                   <?php $this->view('frontend/includes/sidebar_activities',array("order_id"=>$activity->order_id,"table"=>"restaurants")); ?>
                </div>
            </div>



        </div>
    </section>
<div id="map-canvas-sidebar" class="img-rounded" style="height:450px; z-index: 999;"></div>

<?php $add = $this->Ads_model->get('970X90');?>
<?php if ($add): ?>
  <?php if ($add->src != ''): ?>
    <section class="bottom-section gray hidden-xs hidden-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php if ($add->type == 'image'): ?>
                        <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                    <?php else:?>
                        <?php echo base64_decode($add->src); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
  <?php endif; ?>
<?php endif; ?>
</div>
<div id="push"></div>
