<div id="wrap" class="detail-page-wrapper">
        <div class="menu-bg"></div>
        <?php $this->view('frontend/includes/booking_search_form'); ?>
        <section class="page-content paddingtop20">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 paddingbot20">
                        <ul class="breadcrumbs list-inline">
                            <li>
                                <span class="glyphicon glyphicon-home"></span>
                            </li>
                            <li><a href="<?php echo base_url().$this->lang->lang(); ?>">Procida</a>
                            </li>
                            <li>
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </li>
                            <li><a href="#"><?php echo lang('Traditional_Events'); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="sub-title"><?php echo lang('Traditional_Events'); ?></h2>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 ">
                        <div class="row">
                            <div class="col-md-12 paddingtop10">
                                <!-- listing item -->
                                <?php if(isset($events) && is_array($events)): ?>
                                <?php foreach($events as $event): ?>
                                <?php $event_array = (array) $event; ?>
                                   <div class="events-listing events-listing-page">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <img src="<?php echo base_url(); ?>uploads/events/<?php echo $event->thumbnail ?>" class="img-responsive" alt="">
                                        </div>
                                        <div class="col-md-6 noPaddingLeft">
                                            <div class="event-list-title">
                                                <h2><?php echo $event_array[$this->lang->lang().'Title']; ?></h2>
                                            </div>
                                            <ul class="list-inline event-info">
                                                <li>
                                                    <span class="fa fa-calendar ecolor"></span>&nbsp;<?php echo date('M d, Y', strtotime($event->startDate)) ?></li>
                                                <li>
                                                    <span class="fa fa-clock-o ecolor"></span>&nbsp;<?php echo substr($event->startTime,0,5) ?> to <?php echo substr($event->endTime,0,5) ?></li>
                                            </ul>
                                            <?php $desc = base64_decode($event_array[$this->lang->lang().'Description']); ?>
                                            <p><?php echo strlen($desc)>140?substr(strip_tags($desc),0,135).'...':strip_tags($desc);  ?></p>
                                            <a href="<?php echo base_url().$this->lang->lang().'/'.lang('events').'/traditional-event/'.$event->id ?>.html" class="btn btn-danger btn-sm"><?php echo strtolower(lang('Read_more')); ?> &nbsp;<i class="fa fa-chevron-right"></i></a>                                           
                                        </div>
                                    </div>
                                      <!--  <div class="event-list-readmore">
                                    </div> -->
                                </div>
                                <?php endforeach; ?>
                                <?php else: ?>
                                  No any events are found.
                                <?php endif; ?>
                                

                            </div>
                            <div class="col-md-12 paddingtop20">
                                <?php $add = $this->Ads_model->get('468X15'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/468x15&text=ads+/+468+x15" class="img-responsive">
                        <?php endif; ?>
                    <?php else: ?>
                       <img src="http://placehold.it/468x15&text=ads+/+468+x15" class="img-responsive">
                    <?php endif; ?>
                                
                            </div>
                            <div class="col-md-12">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 paddingtop20">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sidebar-element">
                                    <?php $add = $this->Ads_model->get('300X250'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="sidebar-element">
                                    <h4 class="sidebar-title"><?php echo lang('Events_by_Categories'); ?></h4>
                                    <ul class="list-unstyled sidebar-website">
                                        <li><a href="#"><?php echo lang('Music') ?></a><i class="fa fa-angle-right"></i>
                                        </li>
                                        <li><a href="#"><?php echo lang('Prccession'); ?></a><i class="fa fa-angle-right"></i>
                                        </li>
                                        <li><a href="#"><?php echo lang('Festivals'); ?></a><i class="fa fa-angle-right"></i>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="bottom-section gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>