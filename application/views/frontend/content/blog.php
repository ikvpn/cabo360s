<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="sub-title"><?php echo lang('Blog') ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12 paddingtop10">
                            <!-- listing item -->
                            <?php if ($posts && !empty($posts)): ?>
                                <?php foreach ($posts as $post): ?>
                                    <div class="blog-item">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="blog-image">
                                                    <img src="<?php echo base_url(); ?>uploads/blog/<?php echo $post->p_image ?>" class="lazy img-responsive img-rounded" alt="...">
                                                </div>
                                            </div>
                                            <div class="col-md-7 noPaddingLeft">
                                                <h3 class="blog-date"> <?php echo date('F d, Y', strtotime($post->date)); ?>

                                                </h3>
                                                <h2 class="blog-title">
                                                    <?php echo $post->title; ?>
                                                </h2>
                                                <p class="blog-details">
                                                 
                                                     <?php 
                                         $str = strip_tags(base64_decode($post->description));
                                         if(strlen($str)>200){
                                             echo substr($str, 0,199).' ...';
                                         }else{
                                             echo $str;
                                         }
                                      ?>
                                                </p>
                                                <a class="btn btn-purity btn-sm" href="<?php echo base_url() . $this->lang->lang().'/blog/post/' . $post->id.'/'.date('Y/m/d').'/'.  urlencode(str_replace(' ','-',$post->title)); ?>">read more <i class="fa fa-chevron-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <div class="blog-item"> <?php echo lang('Posts_Not_Found_Msg') ?> </div>
                            <?php endif; ?>
                        </div>
                       
                        <div class="col-md-12">
                             <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 paddingtop20">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="well well-small text-center">
                           
                                <?php $add = $this->Ads_model->get('300X250'); ?>
                                <?php if ($add): ?>
                                    <?php if ($add->src != ''): ?>
                                        <?php if ($add->type == 'image'): ?>
                                            <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                        <?php else: ?>
                                            <?php echo base64_decode($add->src); ?>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                                <?php endif; ?>

                            </div>
                        </div>
                         <div class="col-md-12">
                            <?php $this->view('frontend/includes/sidebar_widget_upcoming_events'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bottom-section gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>