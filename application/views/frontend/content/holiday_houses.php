<!--################ WRAP START ################-->
<style type="text/css">
    .custom-villa-main-div{
        display: flex;
        flex-wrap: wrap;
        align-content: space-between;
        height: 180px; 
    }
    .custom-villa-list-sub-div,
    .accomodation-listing-location{
        width: 100%;
    }

    @media only screen and (max-width: 991px) {
       .custom-villa-main-div {
            display: flex;
            flex-wrap: wrap;
            align-content: inherit; 
            height: inherit; 
            padding-top: 20px;
        }
    }
</style>
<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <div class="hidden-xs no-padding">
    	<?php $this->view('frontend/includes/booking_search_form'); ?>
    </div>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 paddingbot20">
                    <ul class="breadcrumbs list-inline">
                        <li>
                            <span class="glyphicon glyphicon-home"></span>
                        </li>
                        <li><a href="<?php echo base_url() . $this->lang->lang(); ?>">Cabo San Lucas</a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li><a href="#"><?php if($bb){echo 'Bed and Breakfast';}else{echo lang('Villas_in_Procida');}?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-md-push-3">
                    <div class="row">

                        <div class="col-md-12">
                            <h1 ><?php echo count($holiday_houses); ?> <?php if($bb){echo lang('BB_in_Procida');}else{echo lang('Villas_in_Procida');}?> </h1>
                        </div>

                        <div class="col-md-12 paddingtop20">
                            <!-- listing item -->
                            <?php if (isset($holiday_houses) && is_array($holiday_houses)): ?>
                                <?php if (count($holiday_houses) > 0): ?>
                                    <?php foreach ($holiday_houses as $item): ?>
                                        <?php $item_array = (array) $item; ?>
                                        <div class="listing-item listing-item-accomidation">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="listing-image">
                                                        <?php if ($item->photo != null && $item->photo != ''): ?>
                                                          <img src="<?php echo base_url() . "uploads/apartments/" . $item->photo; ?>" class="img-responsive img-rounded" alt="<?php echo $item_array[$this->lang->lang() . 'Name']; ?>">
                                                        <?php else: ?>
                                                          <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                                                          <img class="img-responsive img-rounded" src="<?php echo $scr; ?>" alt="<?php echo $item_array[$this->lang->lang() . 'Name']; ?>">
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 custom-villa-main-div">
                                                    <div class="custom-villa-list-sub-div">
                                                        <h3 class="listing-title accomodation-listing-title"><?php echo $item_array[$this->lang->lang() . 'Name']; ?><span><?php echo $item->minPrice . ' - ' . $item->maxPrice ?> $</h3>
                                                        <h4 class="listing-sub-title accomodation-listing-sub-title">
                                                            <?php echo $item->trypology; ?> |
                                                            <?php echo $item->beds ?> <?php echo $item->beds > 1 ? lang('beds') : lang('bed'); ?> |
                                                            <?php echo $item->rooms; ?> <?php echo $item->rooms > 1 ? lang('rooms') : lang('room'); ?>  |
                                                            <?php echo $item->pax ?> <?php echo strtolower(lang('Pax')) ?> |
                                                            <?php $unit = lang('day') ;
                                                            if($item->unit == 'W'){
                                                                $unit = lang('week');
                                                            }
                                                            else if($item->unit == 'M'){
                                                                $unit = lang('month');
                                                            }
                                                            else if($item->unit == 'N'){
                                                                $unit = lang('night');
                                                            }
                                                            else if($item->unit == 'Y'){
                                                                $unit = lang('year');
                                                            }
                                                            ?>
                                                            <span>a <?php echo $unit; ?></span></h4>
                                                        </div>
                                                    <p class="accomodation-listing-location">
                                                        <span class="color2 fa fa-map-marker"></span>&nbsp;<?php echo $item->locationName ?>
                                                        <?php if($bb){?>
                                                        <a href="<?php echo base_url().$this->lang->lang().'/' . lang('where-to-stay').'/bed-and-breakfast/' . $item->pageTitle; ?>" class="btn btn-sm btn-purity btn-search btn-success pull-right"><?php echo lang('details'); ?> <i class="fa fa-angle-right"></i></a>
                                                        <?php ;}else{?>
                                                        <a href="<?php echo base_url().$this->lang->lang().'/villa/' . $item->pageTitle; ?>" class="btn btn-sm btn-purity btn-search btn-success pull-right"><?php echo lang('details'); ?> <i class="fa fa-angle-right"></i></a>
                                                        <?php ;}?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <div class="alert alert-warning">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <?php if($bb){echo lang('BB_Not_Found_Msg');}else{echo lang('Apartments_Not_Found_Msg');}?>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <!-- Akhoular Hakichan fahs manimi dew khush emanimi mon mon -->
                            <?php // JU JU KAKO ;) ?>
                        </div>

                        <div class="col-md-12">
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-md-pull-9">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="sidebar-element">
                                <div class="well well-small well-filter">
                                    <div class="title nopadding"><?php echo lang('Filters'); ?></div>
                                    <hr class="small">
                                    <?php if (validation_errors()): ?>
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <?php echo validation_errors(); ?>
                                        </div>
                                    <?php endif; ?>
                                    <form method="post" class="form">
                                        <div class="form-group">
                                            <select class="form-control input-sm" name="rooms">
                                                <option value="-1"><?php echo lang('Rooms'); ?></option>
                                                <option value="1" <?php echo set_select("rooms", 1) ?>>1</option>
                                                <option value="2" <?php echo set_select("rooms", 2) ?>>2</option>
                                                <option value="3" <?php echo set_select("rooms", 3) ?>>3</option>
                                                <option value="4" <?php echo set_select("rooms", 4) ?>>4</option>
                                                <option value="5" <?php echo set_select("rooms", 5) ?>>5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control input-sm" name="beds">
                                                <option value="-1"><?php echo lang('Beds'); ?></option>
                                                <option value="1" <?php echo set_select("beds", 1) ?>>1</option>
                                                <option value="2" <?php echo set_select("beds", 2) ?>>2</option>
                                                <option value="3" <?php echo set_select("beds", 3) ?>>3</option>
                                                <option value="4" <?php echo set_select("beds", 4) ?>>4</option>
                                                <option value="5" <?php echo set_select("beds", 5) ?>>5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control input-sm" name="pax">
                                                <option value="-1"><?php echo lang('Pax'); ?></option>
                                                <option value="1" <?php echo set_select("pax", 1) ?>>1</option>
                                                <option value="2" <?php echo set_select("pax", 2) ?>>2</option>
                                                <option value="3" <?php echo set_select("pax", 3) ?>>3</option>
                                                <option value="4" <?php echo set_select("pax", 4) ?>>4</option>
                                                <option value="5" <?php echo set_select("pax", 5) ?>>5</option>
                                            </select>
                                        </div>

                                        <div class="title"><?php echo lang('Price_range'); ?></div>
                                        <hr class="small">
                                        <div class="row">
                                            <div class="col-md-6" style="padding-right:0;">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <span class="fa fa-dollar"></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm" name="minPrice" value="<?php echo set_value('minPrice'); ?>" placeholder="<?php echo lang('Min'); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="padding-left:5px;">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <span class="fa fa-dollar"></span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm" name="maxPrice" value="<?php echo set_value('maxPrice'); ?>" placeholder="<?php echo lang('Max'); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php if (isset($locations) && count($locations) > 0): ?>
                                            <div class="title"><?php echo lang('Location'); ?></div>
                                            <hr class="small">

                                            <?php foreach ($locations as $loc): ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="locations[]" <?php echo set_checkbox('locations', $loc->id) ?> value="<?php echo $loc->id; ?>"> <?php echo $loc->title; ?>
                                                    </label>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <hr class="small">
                                        <center>
                                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> <?php echo lang('Search') ?></button>
                                        </center>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="sidebar-element">
                                <?php $add = $this->Ads_model->get('234X60'); ?>
                                <?php if ($add): ?>
                                    <?php if ($add->src != ''): ?>
                                        <?php if ($add->type == 'image'): ?>
                                            <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                        <?php else: ?>
                                            <?php echo base64_decode($add->src); ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>

                        </div>
                        <div class="col-md-12 hidden-xs">
                            <div class="sidebar-element">
                                <div id="map-canvas-sidebar" class="img-rounded "></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bottom-section gray">
        <div class="container">
            <div class="row">
                <?php if($blocks) {?>
                    <section class="page-content paddingtop40 paddingbot40 home-page sec-4" style="background-color:#FBFBFB">
                        <div class="container">
                            <div class="row paddingtop10">
                                <?php foreach ($blocks as $block): ?>
                                <div class="col-md-3">
                                    <a href="<?php echo $block->link; ?>">

                                        <div class="overview-item">
                                            <div class="overview-image">
                                                <img src="<?php echo base_url(); ?>uploads/block/<?php echo $block->p_image; ?>" alt="<?php echo $block->title; ?>" class="lazy2 img-responsive img-rounded">
                                            </div>
                                            <h3><?php echo $block->title; ?></h3>
                                            <p class="index-item-meta"><?php echo base64_decode($block->description); ?></p>
                                        </div>
                                    </a>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </section>
                <?php } ?>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
