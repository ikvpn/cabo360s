<style>
.hotelListing {
  max-height: 550px;
  overflow-y: scroll;
  min-height: 550px;
}

.searchHotels .hotel-listing-item {
    padding-bottom: 20px;
    min-height: 330px;
}

.hotelListing button {
  padding: 0 !important;
  background: none !important;
  border: 0 !important;
}
</style>
<div id="wrap" class="detail-page-wrapper">
  <div class="menu-bg"></div>
  <?php $this->view('frontend/includes/booking_search_form'); ?>
  <section class="page-content paddingtop30">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="row">
                              <div class="col-md-6">
                                  <h2 class="sub-title"><?php echo count($hotels); ?> Hotels disponibili dal <?php $date = explode('/', $searchData['start_date']); echo $date[1]; ?> al <?php $date = explode('/', $searchData['end_date']); echo $date[1].'-'.$date[0].'-'.$date[2]; ?></h2>
                              </div>
                              <!--<div class="col-md-2">
                                  <div class="form-group">
                                      <select class="form-control input-sm">
                                          <option>Location</option>
                                          <option>2</option>
                                          <option>3</option>
                                          <option>4</option>
                                          <option>5</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-2">
                                  <div class="form-group">
                                      <select class="form-control input-sm">
                                          <option>Typology</option>
                                          <option>2</option>
                                          <option>3</option>
                                          <option>4</option>
                                          <option>5</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-2">

                                  <div class="form-group">
                                      <select class="form-control input-sm">
                                          <option>Rating</option>
                                          <option>2</option>
                                          <option>3</option>
                                          <option>4</option>
                                          <option>5</option>
                                      </select>
                                  </div>
                              </div>-->
                          </div>
                      </div>
                      <div class="col-sm-6 col-xs-12 paddingtop10 hotelListing">
                          <div class="row">
                              <?php if(count($hotels)<= 0){ ?>
                                <div class="col-md-12 col-xs-12">
                                  <h4><?php echo lang('Hotels_Not_Found_Message'); ?></h4>
                                </div>
                              <?php } ?>
                              <?php if(count($hotels > 0)){ for($i=0; $i<count($hotels); $i++) { ?>
                                <div class="col-md-6 col-xs-12">
                                    <div class="hotel-listing-item">
                                      <form action="<?php echo base_url(); ?><?php echo $this->lang->lang(); ?>/HotelBB/detail/<?php echo $hotels[$i]->hotel_bb_id; ?>" method="post" name="formhotel">
                                        <input type="hidden" value="<?php echo $searchData['start_date']; ?>" name="start_date">
                                        <input type="hidden" value="<?php echo $searchData['end_date']; ?>" name="end_date">
                                        <input type="hidden" value='<?php echo preg_replace('/\s+/', '', $searchData['party']); ?>' name="party">
                                        <button type="submit">
                                          <div class="hotel-listing-image">
                                            <?php if ($hotels[$i]->mainImage->name != null && $hotels[$i]->mainImage->name != '' && @getimagesize($hotels[$i]->mainImage->name)): ?>
                                              <img src="<?php echo $hotels[$i]->mainImage->name; ?>" class="img-responsive img-rounded" alt="...">
                                              <?php else: ?>
                                                <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                                                <img class="img-responsive img-rounded" src="<?php echo $scr; ?>" alt="...">
                                            <?php endif; ?>

                                              <div class="hotel-listing-price">
                                                  <strong>da € <?php echo intval($hotels[$i]->price); ?> / <?php echo $hotels[$i]->unit; ?></strong>
                                              </div>
                                          </div>
                                        </button>
                                        </form>
                                        <div class="hotel-listing-details">
                                            <h3 class="hotel-listing-title"><?php echo $hotels[$i]->itName; ?></h3>
                                            <p>

                                              <?php
                                              if(isset($hotels[$i]->stars ) && intval($hotels[$i]->stars) < 6) {
                                                for($j=0; $j<intval($fullHotels[$i]->hbb_info->stars); $j++) { ?>
                                                <span class="fa fa-star color4"></span>
                                              <?php }} ?> &nbsp; <!--<span class="reviews">(118 Reviews)</span>-->
                                            </p>
                                        </div>
                                    </div>
                                </div>
                              <?php }} ?>
                          </div>
                      </div>
                      <div class="col-sm-6 col-xs-12 paddingtop20 no-padding map-section">
                        <?php echo $map['html']; ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <?php $add = $this->Ads_model->get('970X90');?>
  <?php if ($add): ?>
    <?php if ($add->src != ''): ?>
      <section class="bottom-section gray hidden-xs hidden-sm">
          <div class="container">
              <div class="row">
                  <div class="col-md-12 text-center">
                      <?php if ($add->type == 'image'): ?>
                          <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                      <?php else:?>
                          <?php echo base64_decode($add->src); ?>
                      <?php endif; ?>
                  </div>
              </div>
          </div>
      </section>
    <?php endif; ?>
  <?php endif; ?>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
