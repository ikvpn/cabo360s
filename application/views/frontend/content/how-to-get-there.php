<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php
$this->view('frontend/includes/booking_search_form'); ?>    
    <!--  <header class="background-header sights-header paddingbot20 paddingtop100" id="map-canvas">
        <div class="traditional-event-detail-title">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="main-title">How to get there</h1>
                    </div>
                </div>
            </div>
        </div>
    </header> -->
    <div id="vesselmap">
        <div class="traditional-event-detail-title">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="main-title"><?php echo lang('how_to_get_there') ?></h1>
                    </div>
                </div>
            </div>
        </div>
<iframe src="https://www.google.com/maps/d/embed?mid=zvm0WVTz7Wtw.k7R_ihdf5r3U" width="640" height="480"></iframe>
</div>
     <!-- <div id="map-canvas"></div> -->
    <section class="page-content how-to-get-there">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php
if ($this->lang->lang() == "en"): ?>
                    <h2 class="sub-title">Arriving by plane</h2>
                    <p>From Capodichino Airport you can reach the two ports of Naples, Beverello and Calata di Massa, using the taxi or the bus. L 'Alibus, which directly connects the airport with the two ports, da''aereoporto every 20 minutes and tickets can be purchased on board. If you choose to use taxi, which takes about 15 minutes (depending on traffic) ask the predetermined rate that is about 20 euro
                        <br><strong>The ticket costs 3 € dell'Alibus
                        <br>A taxi ride costs 20 €</strong>
                    </p>

                    <h2 class="sub-title">Arriving by Train</h2>
                    <p>Central Station PIazza Garibaldi is about 2 km from Beverello and is easily accessible by taxi or bus.
                        If you wish to use the bus, bought the ticket journey inside the station and once you get out there, a hundred meters, the parking of buses and trams:
                        the most convenient way is the number 1 tram, bus 152, and the R2 bus which stops in Piazza Municipio
                        <br><br><strong>A taxi ride costs 10 €
                        <br>Hydrofil ticket costs 15 €
                        <br>Ferry ticket costs 10 €</strong>
                    </p>
                    <h2 class="sub-title">From the port of Procida</h2>
                    <p>
                        Once landed at the port of Marina Grande is the stationing of buses by which you can reach all the main island. Tickets can also be purchased on board. If you want to take a taxi, you can head to the parking which is opposite the landing.
                        <br><br><strong>A taxi ride costs 10 €
                        <br>A bus ticket costs € 1.5</strong>
                    </p>




                    <?php
elseif ($this->lang->lang() == "it"): ?>
                    <p>
Procida è raggiungibile via mare sia da Napoli, dai porti di Calata di Massa e Molo Beverello, sia da Pozzuoli. Dal primo partono solo traghetti (Caremar, Medmar) che impiegano circa 1 ora per raggiungere Procida. Dal secondo partono solo aliscafi (Caremar, Snav) e la durata della traversata è di circa 45m. Da Pozzuoli partono traghetti (Mediar, Caremar, Gestur) e la durata della traversata media è 40 min. Durante l’alta stagione consigliamo di prenotare online i biglietti onde evitare file e brutte sorprese al momento dell’imbarco.
<br><br><a href="http://www.visitprocida.it/it/orari-traghetti">> Prenota online traghetti e aliscafi</a>
                    </p>
<br><br>                    
                    <h2 class="sub-title"><i class="fa fa-plane"></i> Dall'Aereoporto di Napoli Capodichino</h2>  
                    <p>
                        Dall'Aereoporto di Capodichino si possono raggiungere i due porti di Napoli, Molo Beverello e Calata di Massa, utilizzando il taxi oppure il bus di linea . L' Alibus, che collega direttamente l'aereoporto con i due porti, parte ogni 20 minuti da''aereoporto ed il ticket è acquistabile a bordo. Se invece scegliete di servirvi del taxi, che impiega circa 15 min (a seconda del traffico) chiedete al tariffa predeterminata che è di circa 20 euro


                        <br><strong>Il biglietto dell'Alibus costa 3€
                        <br>Una corsa in Taxi costa 20€</strong>
                    </p>
<br><br>
                    <h2 class="sub-title"><i class="fa fa-train"></i> Dalla Stazione Centrale P.zza Garibladi</h2>

                    <p>La stazione centrale di PIazza Garibaldi dista circa 2 km dal Molo Beverello ed è facilmente raggiungibile sia in taxi che con il bus.
                        Se volete servirvi dei bus di linea, acquistate il ticket viaggio all'interno della stazione e una volta usciti troverete, a un centinaio di metri, lo stazionamento dei bus e dei tram:
                        i piu comodi sono il tram numero 1, il bus 152, ed il bus R2 che ferma in piazza Municipio
                        <br>
                        <strong>Una corsa in Taxi costa 10€
                        <br>   Un biglietto dell'Asliscafo costa 15€
                        <br>  Un biglietto di un Traghetto costa 10€</strong>

<br><br>
                        <h2 class="sub-title"><i class="fa fa-ship"></i> Una volta a Procida</h2>
                        <p>
                        Appena sbarcati al porto di Marina Grande c'è lo stazionamento degli autobus di linea con cui è possibile raggiungere tutte le località principali dell'isola. 
                        I biglietti si acquistano anche a bordo. Se volete prendere un taxi potete recarvi allo stazionamento che è di fronte allo sbarco.

                        <br><strong>Una corsa in Taxi costa circa 12€
                        <br>Un biglietto del Bus costa 1,5€</strong>
                    </p>
<br><br>
                    <?php
endif; ?>


                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="well well-small text-center">
                            
                                <?php
$add = $this->Ads_model->get('300X250'); ?>
                                <?php
if ($add): ?>
                                <?php
    if ($add->src != ''): ?>
                                <?php
        if ($add->type == 'image'): ?>
                                <img src="<?php
            echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php
        else: ?>
                            <?php
            echo base64_decode($add->src); ?>
                        <?php
        endif; ?>
                    <?php
    else: ?>
                    <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                <?php
    endif; ?>
            <?php
else: ?>
            <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
        <?php
endif; ?>
    </div>
</div>

</div>
</div>
</div>
</section>
<section class="bottom-section gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <?php
$add = $this->Ads_model->get('970X90'); ?>
                <?php
if ($add): ?>
                <?php
    if ($add->src != ''): ?>
                <?php
        if ($add->type == 'image'): ?>
                <img src="<?php
            echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
            <?php
        else: ?>
            <?php
            echo base64_decode($add->src); ?>
        <?php
        endif; ?>
    <?php
    else: ?>
    <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
<?php
    endif; ?>
<?php
else: ?>
    <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
<?php
endif; ?>
</div>
</div>
</div>
</section>
</div>
<div id="push"></div>