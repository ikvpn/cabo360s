<header class="background-header-slider">
  <!-- ############## SLIDER START #################### -->
  <div class="tp-banner-container clearfix">
    <div class="tp-banner">
      <ul>
        <?php
        //path to directory to scan
        $directory = "uploads/hotels/".$fullHotel->id.'/';
        //get all image files with a .jpg extension. This way you can add extension parser
        $images = glob($directory . "{*.jpg,*.gif}", GLOB_BRACE);
        $listImages = array();
        ?>
        <?php if(isset($fullHotel->image) && count($fullHotel->image) > 0) {?>
              <?php foreach ($fullHotel->image as $image) { ?>
              	<?php if(@getimagesize($image->name)) { ?>
                    <li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
                      <img src="<?php echo $image->name; ?>" alt="<?php echo $fullHotel->enName; ?>" data-bgfit="cover" data-bgposition="center" data-bgrepeat="no-repeat" data-lazyload="<?php echo $image->name; ?>">
                    </li>
                   <?php } ?>
                <?php } ?>
           <?php } elseif($images) {foreach ($images as $image){ ?>
            <li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
                <img src="<?php echo base_url() . $image; ?>" alt="<?php echo $fullHotel->enName; ?>" data-bgfit="cover" data-bgposition="center" data-bgrepeat="no-repeat" data-lazyload="<?php echo base_url() . $image; ?>">
            </li>
           <?php }} else{?>
           <?php $image = 'assets/images/slider.jpg'; ?>
          	<li data-transition="boxfade" data-slotamount="5" data-masterspeed="1200">
                  <img src="<?php echo base_url() . $image; ?>" alt="splendida corricella" data-bgfit="cover" data-bgposition="center" data-bgrepeat="no-repeat" data-lazyload="<?php echo base_url() . $image; ?>">
              </li>
          	 <?php } ?>
        </ul>
      </div>
      <div id="detail_slider" class="main-page-title">
        <div class="container">
          <div class="row paddingtop50">
            <div class="col-md-8 banner-title">
              <h1 class="main-title">
                <?php
                  switch ($this->lang->lang()) {
                    case 'it':
                      echo $fullHotel->itName;
                      break;
                    case 'en':
                      echo $fullHotel->enName;
                      break;
                    case 'du':
                      echo $fullHotel->duName;
                      break;
                    }
                    ?>
                  </h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- ############## SLIDER END #################### -->
</header>
<section class="page-title">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="row">
          <div class="col-md-12"></div>
          <div class="col-md-12 no-padding hotelStats m-tb-10">
            <div class="col-xs-4 text-center">
              <i class="fa fa-home fa-2x"></i>
              <h4><?php if(isset($fullHotel->hbb_info->type)){ echo lang($fullHotel->hbb_info->type); } else { echo "Hotel"; } ?></h4>
            </div>
            <div class="col-xs-4 text-center">
              <div class="hidden-xs">
                <?php
                if(isset($fullHotel->hbb_info)){
                  if($fullHotel->hbb_info->stars == 6){
                    $stars = 0;
                  } else {
                    $stars = $fullHotel->hbb_info->stars;
                  }
                  if(isset($fullHotel->hbb_info)){
                    for($i=0; $i<$stars; $i++){ ?>
                    <i class="fa fa-star fa-2x"></i>
                <?php }}} elseif(isset($fullHotel->stars)){
                    for($i=0; $i<$fullHotel->stars; $i++){ ?>
                    <i class="fa fa-star fa-2x"></i>
                <?}}
                else { ?>
                  <i class="fa fa-star fa-2x"></i>
                <?php } ?>
              </div>
              <div class="visible-xs">
                <i class="fa fa-star fa-2x"></i>
              </div>
              <h4><?php if(isset($fullHotel->hbb_info)){ echo $stars.' '.lang('stars'); } elseif(isset($fullHotel->stars)){ echo $fullHotel->stars.' '.lang('stars');} else { echo 'N.D. '; } ?></h4>
            </div>
            <div class="col-xs-4 text-center">
              <i class="fa fa-bed fa-2x"></i>
              <h4><?php echo lang('rooms'); ?> <?php if(isset($rooms)){ echo count($rooms); } ?> </h4>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4" <? if(!$fullHotel->hotel_bb_id){?>style="display: none;" <? ;}?>>
        <div class="page-title-search">
          <div class="page-title-search-header visible-lg">
            <h3 class="search-header"><?php echo lang('verify_disponibility'); ?></h3>
          </div>
          <script src="https://hbb.bz/thirdmask/js/masck.js"></script>
          <script src="https://hbb.bz/thirdmask/js/library/jquery-ui.min.js"></script>
          <link href="https://hbb.bz/thirdmask/config_reseller/16/custom.css" rel="stylesheet">
          <link href="https://hbb.bz/thirdmask/css/library/jquery-ui.min.css" rel="stylesheet">
          <div id="hotelSearch">
            <div class="form-cn form-hotel " id="form-hotel">
              <form action="<?php echo base_url(); ?><?php echo $this->lang->lang(); ?>/HotelBB/detail/<?php echo $fullHotel->hotel_bb_id; ?>" method="post" name="formhotel">
                <input type="hidden" value="<?php echo $this->lang->lang(); ?>" name="lang">
                <div class="form-search clearfix">
                  <div class="col-sm-6 col-xs-6 no-padding-left">
                    <div class="input-group">
                      <input value="" type="text" class="form-control datepicker" autocomplete="off" name="start_date" placeholder="<?php echo lang('Check-In'); ?>">
                      <div class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 no-padding-right">
                    <div class="input-group">
                      <input value="" type="text" class="form-control datepicker" autocomplete="off" name="end_date" placeholder="<?php echo lang('Check-Out'); ?>">
                      <div class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-9 col-xs-6 m-t-20 no-padding-left">
                    <div class="form-group" id="divospitiroom">
                      <input type="text" id="ospiti" name="txtospiti" class="form-control" autocomplete="off" placeholder="Camere:1 Adulti: 2 Bambini: 0" value="">
                      <input type="hidden" id="jsonparty" name="party" value='[{"adults":2}]' />
                      <div class="popupbambini" id="boxospiti">
                        <div id="boxcamere"></div>
                        <div class="ospitibottom">
                          <a style="float:left; font-size:12px;" id="addcamera" href="#">+ Aggiungi camera</a>
                          <a class="btn btn-sm btn-purity awe-btn awe-btn-1 awe-btn-small settaospiti" id="settaospiti" href="#">OK</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-6 m-t-20 no-padding-right">
                    <button type="submit" class="btn btn-purity btn-block "><?php echo lang('Search'); ?></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-content paddingtop20">
  <div class="container">
  	<div class="row">
      <div class="col-md-8">
        <div class="col-md-12 no-padding">
          <h3 class="sub-title"><?php echo lang('description'); ?></h3>
        </div>
          <?php
            switch ($this->lang->lang()) {
              case 'it':
                echo base64_decode($fullHotel->itDescription);
                break;
              case 'en':
                echo base64_decode($fullHotel->enDescription);
                break;
              case 'du':
                echo base64_decode($fullHotel->duDescription);
                break;
            }
          ?>
      </div>
      <div class="col-md-4">
          <? if($fullHotel->phone):?>
          <div class="paddingtop10 contact_forms text-center">
              <h4 class="noMarginBottom"><?php echo lang("contact_from"); ?></h4>
              <h3 class="noMarginTop"><strong><?php echo $fullHotel->manager ?></strong></h3>
              <?php if ($fullHotel->manager_profile != null && $fullHotel->manager_profile != ''): ?>
                  <img class="img-responsive img-circle" src="<?php echo base_url() . "uploads/hotels/".$fullHotel->manager_profile ?> "/>
              <?php else: ?>
                  <img class="img-responsive img-circle" src="<?php echo base_url() . "assets/images/user-icon-placeholder.png" ?> "/>
              <?php endif; ?>
              <h3 class="phone"> <i style="color:#90B323" class="glyphicon glyphicon-earphone color2"></i> <?php echo $fullHotel->phone ?></h3>
              <a href="#" class="btn btn-purity btn-lg btn-block btn-request btn-sendemail btn80" data-orderid="<?php echo $fullHotel->order_id ?>" data-type="restaurants"><?= lang("Send_a_request");?> <span class="glyphicon glyphicon-chevron-right"></span></a>
          </div>
          <? endif;?>
          <? if($fullHotel->tripWidget):?>
          <style type="text/css">
              .TA_excellent{
                  display: inline-block;
              }
              .contact_forms.tripWidget img{
                  height: auto;
                  width: auto;
                  margin: 0px;
                  border: none;
              }
          </style>
          <br>
          <div class="paddingtop10 contact_forms tripWidget text-center"><?=$fullHotel->tripWidget?></div>
          <br>
          <?endif;?>
      </div>
    </div>
  </section>
  <section class="map-section">
    <div id="map-canvas"></div>
    <div class="container">
      <div class="map-info-box">
        <div class="line">
          <?php foreach ($places as $place) { ?>
            <h2 onclick="NearbyMap.calcRoute('<?php echo $fullHotel->itName; ?>','<?php if($this->lang->lang() == 'it') { echo $place->itTitle; } else { echo $place->enTitle; } ?>, Procida, Naples, Italy, mainPlace');"><i class="fa color1 fa-map-marker fa-2x"></i>
              <?php if($this->lang->lang() == 'it') { echo $place->itTitle; } else { echo $place->enTitle; } ?>
            </h2>
            <p>
            <?php
            	if($this->lang->lang() == 'it') {
            		echo $place->itDescription;
            	} else {
            		echo $place->enDescription;
            	}
             ?>
          <?php } ?>
        </p>
        </div>
      </div>
    </div>
  </section>
  <section class="bottom-section">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <?php $add = $this->Ads_model->get('970X90'); ?>
          <?php if ($add): ?>
            <?php if ($add->src != ''): ?>
              <?php if ($add->type == 'image'): ?>
                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
              <?php else: ?>
                <?php echo base64_decode($add->src); ?>
              <?php endif; ?>
            <?php else: ?>
              <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
            <?php endif; ?>
          <?php else: ?>
            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>
</div>
