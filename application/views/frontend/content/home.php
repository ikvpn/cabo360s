<!--################ WRAP START ################-->
<link href="<?php echo base_url(); ?>assets/css/swiper-bundle.min.css" rel="stylesheet" type="text/css" />
<div id="wrap" class="detail-page-wrapper index">
    <div class="menu-bg"></div>

    <!--################ HEADER START ################-->
    <section class="header-video">

		<?php $this->view('frontend/includes/booking_search_form_home'); ?>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner resutant-slider">
                <div class="item active">
                <div>
                <img src="https://www.cabo360s.com/booking/assets/images/cabo.jpg" alt="private conciere services cabo" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazyload="https://www.cabo360s.com/booking/assets/images/cabo.jpg">
                <div style="background-color: rgba(0, 0, 0, 0.16);" class="overlay"></div>
                <div class="header-text intro_title">
                <div class="col-md-12 text-center" style="    text-shadow: 0px 0 4px #000;">
                <h1 class=" homeh1 animated fadeInDown" style="  font-size: 48px; text-transform: initial;   letter-spacing: 0.5px;"><?php   if($this->lang->lang()=="en") {echo "Cabo Private Concierge Services"; }else {echo "Private concierge services"; } ?></h1>
                <p class="animated fadeInDown" style="color: white;    font-weight: bold; text-transform: initial;   letter-spacing: 0.5px;    font-size: 24px;"><?php   if($this->lang->lang()=="en") {echo "Top Class Experiences and Services in Cabo San Lucas"; }else {echo "Top Class Experiences and Services in Cabo San Lucas"; } ?></p>
                <div class=""></div>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
    </section><!-- End Header video -->
		<?php $this->view('frontend/includes/booking_search_form_home_mobile'); ?>





<?php   /* CODICE ATTIVITA
                  //MICHELE tutto il ciclo modificato per non far comparire attività senza foto

    <section class="page-content paddingbot40" style="padding-top:0px">
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                  <h3 style="font-size: 32px; font-weight: 600;" align="center"><?php echo lang('Top rated'); ?></h3>
                </div>


                <?php 
      
                $indice = 0;

              for ($i=0; $i < 6 ;$ddfs++){

                
                $businessPage = $businessPages[$indice]; 


                     $urlImage = base_url().'uploads/'.$businessPage->categoryName.'/'.$businessPage->photo; 
                     if(@getimagesize($urlImage)){$indice++;$i++;}else {$indice++; continue;}


                ?>
              


               <!-- MICHELE inserito for invece di foreach e aggiunte variabili

                foreach ($businessPages as $businessPage) { ?> -->
                <?php
                  $langURL = lang(str_replace('_', '-', $businessPage->urlType));
                  $type = '';


                  switch ($businessPage->categoryID) {
                    case 5:
                      $type = 'holiday-house';
                      break;
                    case 2:
                      $type = 'restaurant';
                      break;
                    case 4:
                      $type = 'shopping';
                      break;
                    case 7:
                      $type = 'bars-and-cafe';
                      break;
                    case 8:
                      $type = 'rentals';
                      break;
                    case 9:
                      $type = 'wedding';
                      break;
                    case 10:
                      $type = lang('beach-clubs');
                      break;
                    case 11:
                      $type = 'nightlife';
                      break;
                    case 12:
                      $type = lang('act_services');;
                      break;
                  }

                  $url = $businessPage->categoryID == 1 ? base_url().$this->lang->lang().'/hotel-detail/'.$businessPage->id.'/'.str_replace(' ', '-', $businessPage->enName).'' : base_url().$this->lang->lang().'/'.$langURL.'/'.$type.'/'.$businessPage->pageTitle.'';




                ?>
                <div class="col-md-4">
                    <a href="<?php echo $url; ?>">

                        <div class="overview-item">
                            <div class="overview-image">
                                <?php $urlImage = base_url().'uploads/'.$businessPage->categoryName.'/'.$businessPage->photo; ?>
                                <?php if(@getimagesize($urlImage)) {?>
                                	<img style="min-height: 200px; max-height: 200px; object-fit:cover;" src="<?php echo base_url(); ?>uploads/<?php echo $businessPage->categoryName; ?>/<?php echo $businessPage->photo; ?>" alt="<?php if($this->lang->lang() === 'it') { echo $businessPage->itName; } else { echo $businessPage->enName; } ?>" class="img-responsive img-rounded">
                                <?php } else { ?>
                                	<?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                                	<img style="min-height: 200px; max-height: 200px; object-fit:cover;" src="<?php echo $scr; ?>" alt="<?php if($this->lang->lang() === 'it') { echo $businessPage->itName; } else { echo $businessPage->enName; } ?>" class="img-responsive img-rounded">
                                <?php } ?>
                            </div>
                            <div>
	                            <h3 style="font-size:16px"><?php if($this->lang->lang() === 'it') { echo $businessPage->itName; } else { echo $businessPage->enName; } ?></h3>
	                            <p class="index-item-meta">
                                <span><i style="color: #8AB833;" class="fa fa-map-marker"></i> <?php echo $businessPage->locationName; ?></span>
                                <span class='pull-right'><i style="color: #8AB833;" class="fa fa-tag"></i> <?php echo lang($businessPage->categoryName); ?></span>
                              </p>

                            </div>
                        </div>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>

*/ ?>


    <section>      
      <div class="container">
        <h4 style="text-align:  center; font-weight:  700; padding-bottom:   20px;"><?php   if($this->lang->lang()=="en") {echo "Book Tours"; }else {echo " Prenota un tour"; } ?> </h4>
          <div class="col-md-12">
            <div class="row">
                <div class="swiper-container">
                  <div class="swiper-wrapper">
                    <?php foreach ($tours as $key => $tour): ?>
                    <script type="text/javascript">
                       // markers.push(['<?php  echo addslashes($tour->itTitle); ?>', <?php echo $tour->lat ?>, <?php echo $tour->long ?> ]);
                    </script>
                    <div class="swiper-slide">
                        <div class="listing-tour" data-type="<?php echo $tour->type; ?>">
                          <a href="<?php echo base_url().$this->lang->lang()."/tour/".$tour->slug; ?>">
                            <div class="project">
                                <?php if ($tour->thumbnail != null && $tour->thumbnail != '' && $tour->thumbnail != ' '): ?>
                                  <img src="<?php echo base_url() . "uploads/tours/" . $tour->thumbnail; ?>" class="img-responsive project-img img-rounded" alt="<?php echo  $this->lang->lang() == 'it' ?  $tour->itTitle : $tour->enTitle; ?>">
                                <?php else: ?>
                                  <?php $scr = base_url().'assets/images/placeholder.jpg'; ?>
                                  <img class="img-responsive project-img img-rounded" src="<?php echo $scr; ?>" alt="<?php echo  $this->lang->lang() == 'it' ?  $tour->itTitle : $tour->enTitle; ?>">
                                <?php endif; ?>
                                <label class="price">$ <?php echo $tour->price; ?></label>
                            </div>
                          </a>
                          <div class="tab-item-with-image">
                            <h2><a href="<?php echo base_url().$this->lang->lang()."/tour/".$tour->slug; ?>"><?php echo  $this->lang->lang() == 'it' ?  $tour->itTitle : $tour->enTitle; ?></a></h2>
                               <?php $duration = 0; if($tour->durationDay>0){$duration = $tour->durationDay.' d';}else if($tour->durationHours>0){$duration = $tour->durationHours.' h';}else{$duration = $tour->durationMin.' m';} ?>
                            <p class="duration "><i class="fa colorTour fa-map-marker"></i> <?php echo $tour->locationName; ?></p>
                          </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                  </div>
                </div>
                  <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
          </div>                   
        </div>
    </section>

   
    <section class="overview-tabs page-tabs paddingtop40 " style="display:none">
        <div class="container" style="min-height: 1px;">
        </div>
    </section>
    <section class="paddingtop40 paddingbot40 overview-links index-tables">

        <div class="container">
            <div class="row">
            	<div class="col-md-4">
                	 <h4><?php echo lang("Locations"); ?> </h4>
                	 <table class="table table-hover table-index">
                        <tbody>
							 <?php if (isset($listLocation) && !empty($listLocation) && is_array($listLocation)): ?>
                                <?php foreach ($listLocation as $location): ?>
                                   <tr>
                                		<td><?php echo $location->title; ?></td>
                                   		<td><a rel="nofollow" href="<?php echo base_url() . $this->lang->lang() . '/' . lang('what-to-visit') . '/' . urlencode(str_replace(' ', '-', strtolower($location->title))) . ''; ?>" class="btn btn-sm pull-right" style="background-color:#F9B234;color:#fff;font-weight: 600;"><?php echo lang('more'); ?> <i class="fa fa-angle-right"></i></a>
                                   </tr>

                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                	 <h4><?php echo lang("Places_of_Interest"); ?></h4>
                	  <table class="table table-hover table-index">
                        	<tbody>
                            <?php if (isset($listPlaces) && !empty($listPlaces)): ?>
							<?php foreach ($listPlaces as $poi): ?>
                            <?php $poi_array = (array) $poi; ?>
                            <tr>
                                		<td><?php echo$poi_array[$this->lang->lang() . "Title"]; ?></td>
                        				<td><a href="<?php echo base_url() . $this->lang->lang() . '/' . str_replace(' ', '-', strtolower(lang('places-of-interest'))) . '/' . str_replace(' ', '-', strtolower($poi_array["rewrite_url_" . $this->lang->lang()])) . ''; ?>" class="btn btn-sm btn-warning pull-right"><?php echo lang('more'); ?> <i class="fa fa-angle-right"></i></a>
                                    </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                 </table>
                </div>
                <div class="col-md-4">
                	 <h4><?php echo lang('Beaches'); ?></h4>
                	 <table class="table table-hover table-index">
                        		<tbody>
					 <?php if (isset($listBeach) && !empty($listBeach)): ?>
						<?php foreach ($listBeach as $beach): ?>

                                	<tr>
                                		<td><?php echo $beach->title; ?></td>
                        				<td><a rel="nofollow" href="<?php echo base_url() . $this->lang->lang() . '/' . lang('beach') . '/' . str_replace(' ', '-', strtolower($beach->title)) . ''; ?>" class="btn btn-sm btn-purity pull-right"><?php echo lang('more'); ?> <i class="fa fa-angle-right"></i></a>
                                    </tr>

						<?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
					</table>
                </div>

            </div>
        </div>
    </section>


 <?php echo $map['html']; ?>
	<!--  VPN  -->
    <?php if($blocks) {?>
    <section class="page-content paddingtop40 paddingbot40 home-page sec-4" style="background-color:#FBFBFB">
        <div class="container">
            <div class="row paddingtop10">
            	  <?php foreach ($blocks as $block): ?>
            	<div class="col-md-3">
                    <a href="<?php echo $block->link; ?>">

                        <div class="overview-item">
                            <div class="overview-image">
                                <img src="<?php echo base_url(); ?>uploads/block/<?php echo $block->p_image; ?>" alt="<?php echo $block->title; ?>" class="lazy2 img-responsive img-rounded">
                            </div>
                            <h3><?php echo $block->title; ?></h3>
                            <p class="index-item-meta"><?php echo base64_decode($block->description); ?></p>
                        </div>
                    </a>
                </div>

                  <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php } ?>


        <section style="padding:80px 0px;">

        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center">
                    <img src="<?php echo base_url(); ?>assets/images/macbookhome.jpg" alt="" class="lazy img-responsive" style="margin-top: 25px;">
                </div>
                <div class="col-md-6">
                     <h4 style="font-weight:bold;"><?php echo lang('Footer_head') ?></h4>
                     <p><?php echo lang('Footer_content') ?></p>
                     <br />
           <ul class="footer-ul">
                      <li><?php echo lang('footer_line1') ?></li>
                        <li><?php echo lang('footer_line2') ?></li>
                        <li><?php echo lang('footer_line3') ?></li>
                     </ul>
                     <br />
                     <a href="https://www.visitprocida.com/registrati/" class="btn btn-lg btn-purity"><?php echo lang("Add-your-business");?></a>
                </div>
            </div>
        </div>
    </section>

    <section class="bottom-section hidden-xs hidden-sm">

        <div class="container">
            <div class="row">
            	<div class="row">
                    <div class="col-md-12 text-center">
                        <?php $add = $this->Ads_model->get('1170X90'); ?>
                        <?php if ($add): ?>
                            <?php if ($add->src != ''): ?>
                                <?php if ($add->type == 'image'): ?>
                                    <img style="margin-left: auto; margin-right: auto;" src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                <?php else:
                                    ?>
                                    <?php echo base64_decode($add->src); ?>
                                <?php endif; ?>
                            <?php else:
                                ?>
                                <!-- <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="..."> -->
                            <?php endif; ?>
                        <?php else:
                            ?>
                           <!-- <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="..."> -->
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
<style>

@media (max-width:480px){
   .item img {
      height: 400px !important;
  }
  .header-text {
    top: 20% !important;
  }
}
.sec-4 h3,.sec-4 p{
	text-align:center !important;
}
.overlay{
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.5); /*dim the background*/
}
.btn-home-header:hover {
  color: #FFF!important;
}
.item img{
  width: 100%;
  height: 650px;
  object-fit: cover !important;

}
.carousel {
    position: relative;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
}
.header-text {
    position: absolute;
    top: 38%;
    left: 1.8%;
    right: auto;
    width: 96.66666666666666%;
    color: #fff;
    text-transform: uppercase;
}

.header-video {
  position: relative;
  overflow: hidden;
  background: #4d536d;
  background-size: cover;
  top:-55px;
}
#hero_video {
 position: relative;
    background-size: cover;
    color:#fff;
 width: 100%;
 font-size:16px;
 display:table;
 height:100%;
 z-index:99;
 text-align:center;
 text-transform:uppercase;
}
#hero_video a.video{display:none;}
iframe {
  /*position: absolute;*/
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
}
 video {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
}
iframe {
  height: 100%;
  width: 100%;
}
video {
  width: 100%;
}
.teaser-video {
  width: 100%;
  height: auto;
}
.header-video--media {
  width: 100%;
  height: auto;
}
.intro_title {
    display: table-cell;
    vertical-align: middle;
}
.intro_title h3 {
    color: #fff;
    font-size: 40px;
    font-weight: bold;
    margin-bottom: 5px;
    text-transform: uppercase;
}
p.animated.fadeInDown{
	color: white;
    font-weight: bold;
    letter-spacing: 4px;
}
.fadeInDown {
    animation-name: fadeInDown;
}
.animated {
    animation-duration: 1s;
    animation-fill-mode: both;
}
a.button_intro.outilne, .button_intro.outline {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: 2px solid #fff;
    color: #fff;
    padding: 6px 23px;
}
a.button_intro, .button_intro {
    background: #e04f67 none repeat scroll 0 0;
    border: medium none;
    border-radius: 3px;
    color: #fff !important;
    cursor: pointer;
    display: inline-block;
    font-family: inherit;
    font-size: 12px;
    font-weight: bold;
    min-width: 150px;
    outline: medium none;
    padding: 8px 25px;
    text-align: center;
    text-transform: uppercase;
    transition: all 0.3s ease 0s;
}
a.button_intro, .button_intro {
    background: #e04f67 none repeat scroll 0 0;
    border: medium none;
    border-radius: 3px;
    color: #fff !important;
    cursor: pointer;
    display: inline-block;
    font-family: inherit;
    font-size: 12px;
    font-weight: bold;
    min-width: 150px;
    outline: medium none;
    padding: 8px 25px;
    text-align: center;
    text-transform: uppercase;
    transition: all 0.3s ease 0s;
}
#hero_video {
    color: #fff;
    font-size: 16px;
    text-align: center;
    text-transform: uppercase;
    position: absolute;
}
#search_bar_container {
    background: rgba(0, 0, 0, 0.4) none repeat scroll 0 0;
    bottom: 0;
    left: 0;
    padding: 15px 0;
    position: absolute;
    text-align: center;
    width: 100%;
}
.search_bar {
    margin: auto;
    position: relative;
    width: 80%;
}
ul.footer-ul {
  list-style-type: none;
}
ul.footer-ul {
    margin-left: -37px !important;
}
ul.footer-ul li {
  font-family: Arial;
  font-size: 15px;
}

ul.footer-ul li:before {
  content: "";
  margin-right: 10px;
  display: inline-block;
  width: 17px;
  height: 17px;
  background-color: #90B623;
  border-radius: 50%;
  margin-bottom: -2px;
}
.btn-search-home::after{
	border-style:none;
}

.swiper-container {
    width: 100%;
    height: 100%;
  }

.swiper-slide {
  text-align: center;
  font-size: 18px;
  background: #fff;

  /* Center slide text vertically */
  display: -webkit-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  -webkit-justify-content: center;
  justify-content: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  -webkit-align-items: center;
  align-items: center;
}
.swiper-button-prev{
  left: -90px;
  top: 40%;
}
.swiper-button-next{
  right: -90px;
  top: 40%;
}
:root {
    --swiper-theme-color: #3b3b3b;
}
.tab-item-with-image {
    text-align: left;
}
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/swiper-bundle.min.js"></script>
<script>
$(function() {

	// IE detect
	function iedetect(v) {

	    var r = RegExp('msie' + (!isNaN(v) ? ('\\s' + v) : ''), 'i');
		return r.test(navigator.userAgent);

	}

	// For mobile screens, just show an image called 'poster.jpg'. Mobile
	// screens don't support autoplaying videos, or for IE.
	if(screen.width < 800 || iedetect(8) || iedetect(7) || 'ontouchstart' in window) {

		(adjSize = function() { // Create function called adjSize

			$width = $(window).width(); // Width of the screen
			$height = $(window).height(); // Height of the screen

			// Resize image accordingly
			$('#container').css({
				'background-image' : 'url(poster.jpg)',
				'background-size' : 'cover',
				'width' : $width+'px',
				'height' : $height+'px'
			});

			// Hide video
			$('video').hide();

		})(); // Run instantly

		// Run on resize too
		$(window).resize(adjSize);
	}
	else {

		// Wait until the video meta data has loaded
		$('#container video').on('loadedmetadata', function() {

			var $width, $height, // Width and height of screen
				$vidwidth = this.videoWidth, // Width of video (actual width)
				$vidheight = this.videoHeight, // Height of video (actual height)
				$aspectRatio = $vidwidth / $vidheight; // The ratio the videos height and width are in

			(adjSize = function() { // Create function called adjSize

				$width = $(window).width(); // Width of the screen
				$height = $(window).height(); // Height of the screen

				$boxRatio = $width / $height; // The ratio the screen is in

				$adjRatio = $aspectRatio / $boxRatio; // The ratio of the video divided by the screen size

				// Set the container to be the width and height of the screen
				$('#container').css({'width' : $width+'px', 'height' : $height+'px'});

				if($boxRatio < $aspectRatio) { // If the screen ratio is less than the aspect ratio..
					// Set the width of the video to the screen size multiplied by $adjRatio
					$vid = $('#container video').css({'width' : $width*$adjRatio+'px'});
				} else {
					// Else just set the video to the width of the screen/container
					$vid = $('#container video').css({'width' : $width+'px'});
				}

			})(); // Run function immediately

			// Run function also on window resize.
			$(window).resize(adjSize);

		});
	}

});
</script>
<script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 3,
      spaceBetween: 30,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    //   loop: true,
    breakpoints: {
    320: {
        slidesPerView: 1,
        spaceBetween: 10,
    },
    768: {
        slidesPerView: 2,
        spaceBetween: 20,
    },
    1024: {
        slidesPerView: 3,
        spaceBetween: 30,
    },
    }
    });
  </script>