<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <header class="background-header traditional-events-header paddingtop100 paddingbot20" style="background-image:url('<?php echo base_url(); ?>uploads/events/<?php echo $this->Event_model->cover ?>');">
        <div class="traditional-event-detail-title">
            <?php $event_array = (array) $this->Event_model; ?>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="dateBox">
                            <span class="date"><?php echo date('d', strtotime($this->Event_model->startDate)); ?></span>
                            <span class="month"><?php echo date('M', strtotime($this->Event_model->startDate)); ?></span>
                        </div>
                        <h1 class="main-title dark event-title"><?php echo $event_array[$this->lang->lang() . "Title"]; ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section class="page-content paddingbot20 page-traditional-events">
        <div class="container">
            <div class="row">
                <div class="col-md-12 paddingbot20">
                    <ul class="breadcrumbs list-inline">
                        <li>
                            <span class="glyphicon glyphicon-home"></span>
                        </li>
                        <li><a href="<?php echo base_url().$this->lang->lang(); ?>">Procida</a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li><a href="<?php echo base_url().$this->lang->lang().'/traditional-events' ?>">Traditional Events</a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li><a href="#"><?php echo $event_array[$this->lang->lang() . "Title"]; ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="sub-title">Info</h2>                            
                            <p class="">
                                <?php echo $event_array[$this->lang->lang() . "Description"]; ?>
                            </p>                          
                        </div>

                        <div class="col-md-12 paddingtop20">
                            <h2 class="sub-title"><?php echo lang('Venue_Details'); ?></h2>
                            <div class="row paddingtop20">
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <i class="picon picon-location ecolor"></i>
                                            <span><?php echo $this->Event_model->getLocationName($this->Event_model->order_id,$this->lang->lang()); ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <i class="picon picon-map-marker ecolor"></i>

                                            <span><?php echo $this->Event_model->address; ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <i class="fa fa-share-square-o  ecolor"></i>
                                            <span> <a href="<?php echo $this->Event_model->web; ?>" rel="no-follow" target="_blank"><?php echo substr($this->Event_model->web, 0, 30); ?></a>...</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <i class="fa fa-envelope  ecolor"></i>
                                            <span><?php echo $this->Event_model->email; ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <i class="fa fa-phone  ecolor"></i>

                                            <span>+39<?php echo $this->Event_model->phone; ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <i class="picon picon-mobile ecolor"></i>

                                            <span><?php echo $this->Event_model->mob; ?></span>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 paddingtop20">
                            <div class="fb-comments"  data-width="100%"  data-href="<?php echo base_url() . uri_string(); ?>" data-numposts="5" data-colorscheme="light"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 nopadding">
                    <div class="well well-small">
                        <div class="well-heading"><?php echo lang('Info'); ?></div>
                        <ul class="list-unstyled">
                            <li><i class="picon picon-clock ecolor"></i><?php echo substr($this->Event_model->startTime, 0, 5); ?> - <?php echo substr($this->Event_model->endTime, 0, 5); ?></li>
                            <li><i class="picon picon-calendar ecolor"></i><?php echo date('M d, Y', strtotime($this->Event_model->startDate)); ?> to <?php date('M d, Y', strtotime($this->Event_model->endDate)); ?></li>
                            <li><i class="picon picon-map-marker ecolor"></i><?php echo $this->Event_model->getLocationName($this->Event_model->id,$this->lang->lang()); ?></li>
                            <li><i class="picon picon-info ecolor"></i>Reservation <?php echo $this->Event_model->reservation == 'no' ? 'Not' : ''; ?> Required</li>
                        </ul>
                    </div>
                    <div class="well well-small paddingtop10">
                         <?php if($this->Event_model->pAdult != 0 || $this->Event_model->pChild != 0 || $this->Event_model->pGuest != 0 && $this->Event_model->reservation == 'no'): ?>
                        

                        <div class="well-heading"><?php echo lang('Prices'); ?></div>
                        <ul class="list-unstyled">
                            <li><?php echo lang('Adults'); ?><span class="text-right"><?php echo $this->Event_model->pAdult > 0 ? $this->Event_model->pAdult . '$' : 'Free'; ?></span>
                            </li>
                            <li><?php echo lang('Childs'); ?><span class="text-right"><?php echo $this->Event_model->pChild > 0 ? $this->Event_model->pAdult . '$' : 'Free'; ?></span>
                            </li>
                            <li><?php echo lang('Guests'); ?><span class="text-right"><?php echo $this->Event_model->pGuest > 0 ? $this->Event_model->pAdult . '$' : 'Free'; ?></span>
                            </li>
                        </ul>
                         <?php else: ?>
                         <div class="well-heading">Free</div>
                         <?php $add = $this->Ads_model->get('234X60'); ?>
                        <?php if ($add): ?>
                            <?php if ($add->src != ''): ?>
                                <?php if ($add->type == 'image'): ?>
                                    <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                <?php else: ?>
                                    <?php echo base64_decode($add->src); ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <img src="http://placehold.it/234x60&text=ads/234x60" alt="" class="img-responsive">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/234x60&text=ads/234x60" alt="" class="img-responsive">
                        <?php endif; ?>

                         <?php endif; ?>
                    </div>
                    

                    <div class="well well-small paddingtop10">
                        <div class="well-heading">Book your Ticket</div>
                        <a href="#" class="btn btn-danger btn-lg btn-block btn-request btn-sendemail" data-orderid="<?php echo $this->Event_model->id ?>" data-type="events"><?php echo lang('Send_a_request'); ?><span class="glyphicon glyphicon-chevron-right"></span></a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-12">
                              <div class="well well-small text-center">
                        <?php $add = $this->Ads_model->get('468x15'); ?>
                        <?php if ($add): ?>
                            <?php if ($add->src != ''): ?>
                                <?php if ($add->type == 'image'): ?>
                                    <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                <?php else: ?>
                                    <?php echo base64_decode($add->src); ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                        <?php endif; ?>
                        </div>
                        </div>
                        <div class="col-md-12">
                            <div class="sidebar-element">
                                <div id="map-canvas-sidebar" class="img-rounded"></div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="sidebar-element">
                                <h4 class="sidebar-title"><?php echo lang('Events_by_Categories'); ?></h4>
                                <ul class="list-unstyled sidebar-website list-sidebar-events">
                                    <li><a href="#"><?php echo lang('Music'); ?></a><i class="fa fa-angle-right"></i>
                                    </li>
                                    <li><a href="#"><?php echo lang('Prccession'); ?></a><i class="fa fa-angle-right"></i>
                                    </li>
                                    <li><a href="#"><?php echo lang('Festivals'); ?></a><i class="fa fa-angle-right"></i>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bottom-section gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>