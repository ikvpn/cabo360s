

<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <!--<div id="vesselmap">
        <div class="traditional-event-detail-title">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="main-title"><?php   echo lang('departures_from_procida') ?></h1>
                    </div>
                </div>
            </div>
        </div>
        <iframe src="https://www.google.com/maps/d/embed?mid=zvm0WVTz7Wtw.k7R_ihdf5r3U" width="640" height="480"></iframe>-->
	<header class="background-header traditional-events-header paddingtop100 paddingbot20 header-common-height" style="background-image:url('<?php echo base_url('assets/images/transfer-procida.jpg'); ?>');">

		<div class="main-page-title">
            <div class="container">
                <div class="row paddingtop50">
                    <div class="col-md-8 banner-title">
                        <h1 class="main-title ml9" style="bottom: 30px !important;position: absolute;left: 20px;">
                        	<?php echo lang('private_transfer') ?>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
	</header>
</div>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <!-- <div class="col-md-7">
                    <?php
                    if ($this->lang->lang() == "en"): ?>
                    <h2 class="sub-title">Private transfers from the Airport</h2>
                    <p>From Capodichino Airport or from Central Station of Naples a collective transfer service is available that allows you to conveniently raggiugere Procida and their accommodation by taking advantage of a range of services from transport by car, taxi and hydrofoil to make simple porterage and without unforeseen your transfer there and back from the island.
                    </p>

                    <h2 class="sub-title">RATES INCLUDE:</h2>
<br>Pickup at the airport
<br>Hydrofoil / Ferry ticket
<br>Taxi / Microtaxi from the port to the hotel
<br>Children 0/2 years FREE

                    <h2 class="sub-title">PLEASE NOTE:</h2>
                    <p>
<br>- To the islands, in the absence of connections with the hydrofoil, the crossing will be made by ferry (in these cases no refunds).
<br> - In some cases and for technical / organizational reasons, travelers and bagaglipotranno traveling separately. In this case your luggage will poiconsegnati (in 3 hours) at the hotel. This situation potràriproporsi also on departure from the hotel.
<br> - The collective transfer may provide a short wait.
<br> - There are, in some places, pedestrian zones. In some of them you can take advantage, with its supplement, an additional specific transfer.




                    <?php
                    elseif ($this->lang->lang() == "it"): ?>
                    <h2 class="sub-title">Trasferimenti privati dall' Aeroporto</h2>
                    <p>Dall'Aeroporto di Capodichino o dalla Stazione Centrale di Napoli è attivo un servizio di transfer privato che consente di raggiungere comodamente Procida e la struttura ricettiva prenotata.  Sono inclusi il trasporto in auto fino al porto, imbarco passeggeri e bagagli, pick up al porto di Procida e drop off in Hotel. Questo trasferimento è prenotabile per una o più persone dello stesso nucleo e per la stessa struttura, tutti i giorni della settimana e in coincidenza con tutti i voli con arrivo/partenza per/da Roma e Napoli:
                     
                     All’uscita della sala arrivi dell’aeroporto (oppure all’inizio del binario ferroviario) un nostro incaricato attenderà l’ospite con un con un cartello e li accompagnerà in auto fino al porto di Napoli o Pozzuoli per l’imbarco sul primo traghetto disponibile.


<p> •  Dopo l’arrivo al porto di Procida un Taxi locale accoglierà gli ospiti per accompagnarli fino in Hotel.</p>


<p> •  Per le strutture ubicate a Marina di Corricella il trasferimento finisce all’ingresso pedonale (scale).</p>

<p> •  Il ritorno viene effettuato con la procedura inversa.</p>
</p>
                    
                    <h2 class="sub-title">Le tarrife includono:</h2>
					<p> • Pick-up in aeroporto</p>
					<p> • Trasferimento al porto (Napoli o Pozzuoli)</p>
					<p> • Biglietti passeggeri e bagagli</p>
					<p> • Pick-up al porto di Procida</p>
					<p> • Drop-off in hotel </p>

					<h2 class="sub-title">Nota bene:</h2>
					<p>Gli orari di partenza per Procida avvengono in concomitanza con con gli orari di partenza dei mezzi di trasporto marittimo.
</p>
					<p>Durata della traversata tra i 45 ed i 60 minuti. </p>
					<p>Il transfer collettivo può prevedere una breve attesa.</p>
					<p>Per le strutture ubicate a Marina di Corricella il trasferimento finisce all’ingresso pedonale (scale).</p>
                    <?php
                    endif; ?>

                </div> -->
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
							<div class="box_style_1 expose book-transfer-wrap">
								<h3 class="inner"> <?php echo lang('book_transfer'); ?> </h3>
								<div id="step1">
								<div class="row">
									<div class="col-md-12 col-sm-12">
										<!-- <div class="form-group">
											<label class="radio-inline">
												<input type="radio" name="transfer_type" checked value="car_transfer">
												<?php echo lang('car_transfer'); ?>
											</label>
											<label class="radio-inline">
												<input type="radio" name="transfer_type" value="yacht_transfer">
												<?php echo lang('yacht_transfer'); ?>
											</label>
										</div> -->
										<div class="radio-toolbar custom-responsive">
											<div class="item-radio-card">
											<img src="<?php echo base_url('assets/images/car_transfer.jpg');?>" alt="">
											<input type="radio" id="car_transfer" name="transfer_type" value="car_transfer" checked>
											<label for="car_transfer"><?php echo lang('car_transfer'); ?></label>
											</div>

											<div class="item-radio-card">
											<img src="<?php echo base_url('assets/images/yacht_transfer.jpg');?>" alt="">
											<input type="radio" id="yacht_transfer" name="transfer_type" value="yacht_transfer">
											<label for="yacht_transfer"><?php echo lang('yacht_transfer'); ?></label>
											</div>
										</div>
										<div class="transfer-text">
											<p id="for_car_transfer">Dall'Aeroporto di Capodichino o dalla Stazione Centrale di Napoli è attivo un servizio di transfer privato che consente di raggiungere comodamente Procida e la struttura ricettiva prenotata. Sono inclusi il trasporto in auto fino al porto, imbarco passeggeri e bagagli, pick up al porto di Procida e drop off in Hotel. All’uscita della sala arrivi dell’aeroporto (oppure all’inizio del binario ferroviario) un nostro incaricato attenderà l’ospite con un con un cartello e li accompagnerà in auto fino al porto di Napoli o Pozzuoli per l’imbarco sul primo traghetto disponibile.</p>
											<p id="for_yacht_transfer" style="display: none;">Dopo aver ritirato il Vostro bagaglio, all’uscita della sala arrivi dell’aeroporto di Roma oppure all’inizio del binario ferroviario incontrerete un nostro incaricato Easy Island con un cartello che riporterà il Vostro nome e quindi sarete accompagnato alla macchina che raggiungerà il porto di Mergellina (Napoli) per l’imbarco sullo Yacht Privato. Durata della traversata in mare 40 minuti. Dopo l’arrivo al porto di Procida un altro incaricato Easy Island con un cartello che riporterà il Vostro nome e driver vi accompagnerà alla struttura prenotata. </p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 col-sm-4">
										<div class="form-group">
											<label><i class="fa fa-calendar"></i> <?php echo lang('select_date'); ?></label>
											<input id="date" required class="date-pick form-control" data-date-format="dd-mm-yyyy" type="text">
										</div>
									</div>
									<div class="col-md-4 col-sm-4">
										<div class="form-group">
											<label><i class="fa fa-clock-o"></i> <?php echo lang('time'); ?></label>
											<input id="time" required class="time-pick form-control" value="12:00" type="text">
										</div>
									</div>
									<div class="col-md-4 col-sm-4">
										<div class="form-group">
											<label class="manage-label-line"><?php echo lang('flight_train_no'); ?></label>
											<input id="flight_train_no" class="form-control" name="flight_train_no" type="text">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3 col-sm-3">
										<div class="form-group">
											<label><?php echo lang('adults'); ?></label>
											<div class="numbers-row">
												<input type="text" value="0" id="adults" class="qty2 form-control" name="quantity" readonly>
											<div class="inc button_inc">+</div><div class="dec button_inc">-</div></div>
										</div>
									</div>
									<div class="col-md-3 col-sm-3">
										<div class="form-group">
											<label><?php echo lang('children'); ?></label>
											<div class="numbers-row">
												<input type="text" value="0" id="children" class="qty2 form-control" name="quantity" readonly>
											<div class="inc button_inc">+</div><div class="dec button_inc">-</div></div>
										</div>
									</div>
									<div class="col-md-3 col-sm-3">
										<div class="form-group">
											<label><?php echo lang('infants'); ?></label>
											<div class="numbers-row">
												<input type="text" value="0" id="infants"  class="qty2 form-control" name="quantity" readonly>
											<div class="inc button_inc">+</div><div class="dec button_inc">-</div></div>
										</div>
									</div>
									<div class="col-md-3 col-sm-3">
										<div class="form-group">
											<label><?php echo lang('pets'); ?></label>
											<div class="numbers-row">
												<input type="text" value="0" id="pets"  class="qty2 form-control" readonly>
											<div class="inc button_inc">+</div><div class="dec button_inc">-</div></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											<label><?php echo lang('pick_up_address'); ?></label>
											<select id="address" class="form-control" name="address">
											<?php 
												foreach ($pick_up_points as $key => $pick_up) :
													echo "<option value='".$key."'>".$pick_up."</option>";
												endforeach;
											?>
											</select>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											<label><?php echo lang('drop_off_address'); ?></label>
													<select id="address_2" class="form-control" name="addres_2">
											<option value=""><?= lang('choose_your_accommodation'); ?></option>
											<?php foreach($hotels as $hotel) { ?>
											<option value="<?php echo $hotel->id; ?>"><?php echo $hotel->enName; ?></option>
										    <?php } ?>
										    <?php foreach($apartments as $apartment) { ?>
											<option value="<?php echo $apartment->id; ?>"><?php echo $apartment->enName; ?></option>
											<?php } ?>
											<?php foreach($bed_and_breakfast as $bed) { ?>
											<option value="<?php echo $bed->id; ?>"><?php echo $bed->enName; ?></option>
											<?php } ?>
											</select>
										</div>
									</div>
								</div>
					            <a class="btn_collapse" data-toggle="collapse" href="#collapseForm" aria-expanded="false" aria-controls="collapseForm">
					            	<i class="fa fa-plus-circle"></i> <?php echo lang('return'); ?>
					            </a> <small>(<?php echo lang('optionally'); ?>)</small>
					            <div class="collapse" id="collapseForm">
					            	<div class="row">
										<div class="col-md-4 col-sm-4">
											<div class="form-group">
												<label><i class="fa fa-calendar"></i> <?php echo lang('select_date'); ?></label>
												<input id="returnDate" class="date-pick form-control" data-date-format="dd-mm-yyyy"  value="" type="text">
											</div>
										</div>
										<div class="col-md-4 col-sm-4">
											<div class="form-group">
												<label><i class="fa fa-clock-o"></i> <?php echo lang('time'); ?></label>
												<input id="returnTime" class="time-pick form-control" value="12:00" type="text">
											</div>
										</div>
										<div class="col-md-4 col-sm-4">
											<div class="form-group">
												<label class="manage-label-line"><?php echo lang('flight_train_no'); ?></label>
												<input id="return_flight_train_no" class="form-control" name="return_flight_train_no" type="text">
											</div>
										</div>
									</div>
								<!-- <div class="row">
									<div class="col-md-4 col-sm-4">
										<div class="form-group">
											<label><?php echo lang('adults'); ?></label>
											<div class="numbers-row">
												<input type="text" value="0" id="adultsReturn" class="qty2 form-control" name="quantity" readonly>
											<div class="inc button_inc">+</div><div class="dec button_inc">-</div></div>
										</div>
									</div>
									<div class="col-md-4 col-sm-4">
										<div class="form-group">
											<label><?php echo lang('children'); ?></label>
											<div class="numbers-row">
												<input type="text" value="0" id="childrenReturn" class="qty2 form-control" name="quantity" readonly>
											<div class="inc button_inc">+</div><div class="dec button_inc">-</div></div>
										</div>
									</div>
									<div class="col-md-4 col-sm-4">
										<div class="form-group">
											<label><?php echo lang('infants'); ?></label>
											<div class="numbers-row">
												<input type="text" value="0" id="infantsReturn" class="qty2 form-control" name="quantity" readonly>
											<div class="inc button_inc">+</div><div class="dec button_inc">-</div></div>
										</div>
									</div>
								</div> -->
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<div class="form-group">
												<label><?php echo lang('pick_up_address'); ?></label>
												<select id="address_return" class="form-control" name="address_return">
												<option value=""><?= lang('choose_your_accommodation'); ?></option>
												<?php foreach($hotels as $hotel) { ?>
												<option value="<?php echo $hotel->id; ?>"><?php echo $hotel->enName; ?></option>
												<?php } ?>
												<?php foreach($apartments as $apartment) { ?>
    											<option value="<?php echo $apartment->id; ?>"><?php echo $apartment->enName; ?></option>
    											<?php } ?>
    											<?php foreach($bed_and_breakfast as $bed) { ?>
    											<option value="<?php echo $bed->id; ?>"><?php echo $bed->enName; ?></option>
    											<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-6 col-sm-6">
											<div class="form-group">
												<label><?php echo lang('drop_off_address'); ?></label>
													<select id="address_return_2" class="form-control" name="address_return_2">
													<?php 
													foreach ($pick_up_points as $key => $pick_up) :
														echo "<option value='".$key."'>".$pick_up."</option>";
													endforeach;
												?>
												</select>
											</div>
										</div>
									</div>
					            </div><!-- End collapse form -->
					            </div>
								<div class="form-group">
									<label for="comment"><?php echo lang('special_request'); ?></label>
									<textarea class="form-control" rows="5" id="special_request"></textarea>
								</div>
					            <div id="success-msg" style="display:none;">
					            	<div class="alert text-center alert-success" role="alert">
					            		<h4><?php echo lang('done'); ?>! <br /> <?php echo lang('reservation_completed'); ?> !</h4>
					            	</div>
					            </div>
					            <div id="fail-msg" style="display:none;">
					            	<div class="alert alert-danger" role="alert">
					            		<h4> <?php echo lang('reservation_error'); ?> </h4>
					            	</div>
					            </div>
								<br>
								<table id="priceTable" class="table table_summary">
								<tbody>
								<tr>
									<td>
										<?php echo lang('adults'); ?>
									</td>
									<td id="adults-n" class="text-right">

									</td>
								</tr>
								<tr>
									<td>
										<?php echo lang('children'); ?>
									</td>
									<td id="children-n" class="text-right">

									</td>
								</tr>
								<tr>
									<td>
										<?php echo lang('total_amount'); ?>
									</td>
									<td id="total-n" class="text-right">

									</td>
								</tr>
								<tr class="total">
									<td>
										<?php echo lang('total_cost'); ?>
									</td>
									<td id="price-n" class="text-right">

									</td>
								</tr>
								</tbody>
								</table>
								<a id="bookingBtn1" class="btn  btn-purity btn-sendTransferEmail"> <?php echo lang('book_now'); ?> </a>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </section>
        <section class="bottom-section gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php
                        $add = $this->Ads_model->get('970X90'); ?>
                        <?php
                        if ($add): ?>
                        <?php
                        if ($add->src != ''): ?>
                        <?php
                        if ($add->type == 'image'): ?>
                        <img src="<?php
                        echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                        <?php
                        else: ?>
                        <?php
                        echo base64_decode($add->src); ?>
                        <?php
                        endif; ?>
                        <?php
                        else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php
                        endif; ?>
                        <?php
                        else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php
                        endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="push"></div>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/date-time-picker.css'); ?>" />
