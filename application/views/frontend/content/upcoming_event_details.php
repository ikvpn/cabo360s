<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <section class="page-content paddingbot20 upcoming-events">
        <div class="container">
          <?php $event_array = (array) $this->Event_model; ?>
            <div class="row ">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12 paddingbot20">
                            <ul class="breadcrumbs list-inline">
                                <li>
                                    <span class="glyphicon glyphicon-home"></span>
                                </li>
                                <li><a href="<?php echo base_url().$this->lang->lang(); ?>">Procida</a>
                                </li>
                                <li>
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </li>
                                <li><a href="<?php echo base_url().$this->lang->lang().'/upcoming-events' ?>"><?php echo lang('Upcoming_Events'); ?></a>
                                </li>
                                <li>
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </li>
                                <li><a href="#"><?php echo $event_array[$this->lang->lang() . 'Title'] ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row paddingtop20">
                        <div class="col-md-2">
                            <div class="dateBox">
                                <span class="date"><?php echo date('d', strtotime($this->Event_model->startDate)) ?></span>
                                <span class="month"><?php echo lang(date('M', strtotime($this->Event_model->startDate))); ?></span>
                            </div>
                        </div>
                        <div class="col-md-10 noPaddingLeft">
                            <h1 class="main-title dark event-title color2"><?php echo $event_array[$this->lang->lang() . 'Title'] ?></h1>
                        </div>
                        <div class="col-md-12">
                            <hr class="small">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="<?php echo base_url(); ?>uploads/events/<?php echo $this->Event_model->thumbnail; ?>" alt="" class="img-responsive">
                                </div>
                                <div class="col-md-6">
                                    <ul class="list-unstyled event-info list-upcoming">
                                        <li>
                                            <span class="picon-calendar color2"></span>&nbsp;<?php echo date('M d, Y', strtotime($this->Event_model->startDate)) ?></li>
                                        <li>
                                            <span class="picon-clock color2"></span>&nbsp;<?php echo substr($this->Event_model->startTime, 0, 5); ?> to <?php echo substr($this->Event_model->endTime, 0, 5); ?></li>
                                        <li>
                                            <span class="picon-map-marker color2"></span>&nbsp;<?php echo $this->Event_model->getLocationName($this->Event_model->order_id, $this->lang->lang()); ?></li>
                                    </ul>
                                    <p class="paddingtop20">
                                        <?php echo $event_array[$this->lang->lang() . "Description"]; ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 paddingtop40">
                            <h2 class="sub-title"><?php echo lang('Venue_Details'); ?></h2>

                            <hr class="procida">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang('Time'); ?></strong>
                                            <span><?php echo substr($this->Event_model->startTime, 0, 5) . ' to ' . substr($this->Event_model->endTime, 0, 5); ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang('Days'); ?></strong>
                                            <span><?php echo date('w', strtotime($this->Event_model->startDate)) ?> to <?php echo date('w', strtotime($this->Event_model->endDate)) ?> </span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang('Phone'); ?></strong>
                                            <span>+39<?php echo $this->Event_model->phone; ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang('Website'); ?></strong>
                                            <span><?php echo $this->Event_model->web; ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang('Info'); ?></strong>
                                            <span><?php echo $this->Event_model->reservation == 1 ? 'Reservation Recomended' : 'N/A'; ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang('Mobile'); ?></strong>
                                            <span>
                                                <?php echo $this->Event_model->mob ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="detail-summary">
                                        <p>
                                            <strong><?php echo lang('Address'); ?></strong>
                                            <span>
                                                <?php echo $this->Event_model->address ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 paddingtop20">
                            <div class="fb-comments"  data-width="100%"  data-href="<?php echo base_url() . uri_string(); ?>" data-numposts="5" data-colorscheme="light"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="sidebar-element">
                                <?php $add = $this->Ads_model->get('300X250'); ?>
                                <?php if ($add): ?>
                                    <?php if ($add->src != ''): ?>
                                        <?php if ($add->type == 'image'): ?>
                                            <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                        <?php else: ?>
                                            <?php echo base64_decode($add->src); ?>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="sidebar-element">
                                <h4 class="sidebar-title"><?php echo lang('Events_by_Categories'); ?></h4>
                                <ul class="list-unstyled sidebar-website list-sidebar-events">
                                    <li><a href="#"><?php echo lang('Music'); ?></a><i class="fa fa-angle-right"></i>
                                    </li>
                                    <li><a href="#"><?php echo lang('Prccession');?></a><i class="fa fa-angle-right"></i>
                                    </li>
                                    <li><a href="#"><?php echo lang('Festivals'); ?></a><i class="fa fa-angle-right"></i>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bottom-section gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>