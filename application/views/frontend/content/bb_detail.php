<div id="wrap" class="detail-page-wrapper">
  <div class="menu-bg"></div>
  <?php $this->view('frontend/includes/booking_search_form'); ?>
  <?php $item_array = (array) $this->Bed_and_breakfast_model; ?>

    <section class="page-content paddingbot20">
        <div class="container">
          <div class="row">
              <div class="col-md-12 paddingbot20">
                  <ul class="breadcrumbs list-inline">
                      <li>
                          <span class="glyphicon glyphicon-home"></span>
                      </li>
                      <li><a href="<?php echo base_url().$this->lang->lang(); ?>">Procida</a>
                      </li>
                      <li>
                          <span class="glyphicon glyphicon-chevron-right"></span>
                      </li>
                      <li><a href="<?php echo base_url().$this->lang->lang().'/'.lang('where-to-stay').'/bed-and-breakfast' ?>">Bed and Breakfast</a>
                      </li>
                      <li>
                          <span class="glyphicon glyphicon-chevron-right"></span>
                      </li>
                      <li><a href="#"><?php echo $item_array[$this->lang->lang()."Name"]; ?></a>
                      </li>
                  </ul>
              </div>
          </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <?php
                                //path to directory to scan
                                $directory = "uploads/bed_and_breakfast/" . $item_array['id'] . '/';
                                //get all image files with a .jpg extension. This way you can add extension parser
                                $images = glob($directory . "{*.jpg,*.gif}", GLOB_BRACE);
                                $listImages = array();
                            ?>
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <!--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li> -->
                                <?php
                                if(count($images) > 0){
                                    $i=0; foreach ($images as $image) {
                                ?>
                                <li data-target="#carousel-example-generic" data-slide-to="<?=$i?>" class= "<?php if($i==0){echo 'active';} ?>"></li>
                                <?php $i++;}}?>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                             
                                                             <?php if(count($images) > 0){$primo = 0;
                                    foreach ($images as $image) { ?>
                                    <div class="item <?php if($primo==0){echo ' active'; $primo=1;} ?>">
                                        <img src="<?php echo base_url() . $image; ?>">
                                    </div>
                                <?php ;};}?>

                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>



                        <!--<div class="col-md-12">
                          <img style="width: 100%;object-fit: cover;" src="<?php /*echo base_url(); */?>uploads/bed_and_breakfast/<?php /*echo $this->Bed_and_breakfast_model->photo; */?>" class="img-responsive" alt="">
                        </div>-->
                        <div class="col-md-8">
                            <h3 class="main-title dark restaurant-title"><?php echo $item_array[$this->lang->lang()."Name"]; ?></h3>
                            <ul class="list-inline restaurant-info paddingtop20">
                                <li>
                                    <span class="glyphicon glyphicon-map-marker color2"></span>&nbsp;<?php echo $this->Bed_and_breakfast_model->getLocationName($this->lang->lang()); ?></li>

                            </ul>
                        </div>
					<div class="col-md-4" style="text-align:right;">
            <?php $unit = lang('day') ;
              if($this->Bed_and_breakfast_model->unit == 'W'){
                $unit = lang('week');
              }
              else if($this->Bed_and_breakfast_model->unit == 'M'){
                  $unit = lang('month');
              }
              else if($this->Bed_and_breakfast_model->unit == 'N'){
                  $unit = lang('night');
              }
              else if($this->Bed_and_breakfast_model->unit == 'Y'){
                  $unit = lang('year');
              }
            ?>
						<h3><strong><?php echo $this->Bed_and_breakfast_model->minPrice ?> - <?php echo $this->Bed_and_breakfast_model->maxPrice ?> €</strong></h3>
						<div class="price-per"><?php echo lang('price_for'); ?> <?php echo $unit; ?></div>
					</div>

                        <div class="col-md-12">
                            <hr class="procida">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                          <strong><?php echo lang('Topology'); ?></strong>
                                          <span><?php echo $this->Bed_and_breakfast_model->trypology ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                          <strong><?php echo ucfirst(lang('bedrooms')); ?></strong>
                                          <span><?php echo $this->Bed_and_breakfast_model->rooms; ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                          <strong><?php echo lang('Beds') ?></strong>
                                          <span><?php echo $this->Bed_and_breakfast_model->beds ?></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="detail-summary">
                                        <p>
                                          <strong><?php echo lang('Pax'); ?></strong>
                                          <span><?php echo $this->Bed_and_breakfast_model->pax; ?></span>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>

					<div class="col-md-12 paddingtop20">
                            <h3><?php if($this->lang->lang() == "it"){echo "Dettagli";}else {echo "Details";} ?></h3>
						<hr class="procida">
            <?php

               echo $item_array[$this->lang->lang()."Description"];
            ?>
          </div>

					<div class="col-md-12 paddingtop20">
                            <h3>Area</h3>
						<hr class="procida">
						<div id="map-canvas-sidebar"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                	<div class="paddingtop10 contact_forms text-center">
                	    <h4 class="noMarginBottom"><?php echo lang("contact_from"); ?></h4>
                	    <h3 class="noMarginTop"><strong><?php echo $this->Bed_and_breakfast_model->manager ?></strong></h3>
                      <?php if ($this->Bed_and_breakfast_model->manager_profile != null && $this->Bed_and_breakfast_model->manager_profile != ''): ?>
                        <?php $url = base_url() . "uploads/bed_and_breakfast/".$this->Bed_and_breakfast_model->manager_profile; ?>
                        <?php if (@getimagesize($url)): ?>
                            <img class="img-responsive img-circle" src="<?php echo base_url() . "uploads/bed_and_breakfast/".$this->Bed_and_breakfast_model->manager_profile ?> "/>
                        <?php else: ?>
                            <img class="img-responsive img-circle" src="<?php echo base_url() . "assets/images/user-icon-placeholder.png" ?> "/>
                        <?php endif; ?>
                      <?php else: ?>
                        <img class="img-responsive img-circle" src="<?php echo base_url() . "assets/images/user-icon-placeholder.png" ?> "/>
                      <?php endif; ?>

                	    <h3 class="phone"> <i style="color:#90B323" class="glyphicon glyphicon-earphone color2"></i> <?php echo $this->Bed_and_breakfast_model->phone ?></h3>
                	    <a href="#" class="btn btn-purity btn-lg btn-block btn-request btn-sendemail btn80" data-orderid="<?php echo $this->Bed_and_breakfast_model->order_id ?>" data-type="restaurants"><?= lang("Send_a_request");?> <span class="glyphicon glyphicon-chevron-right"></span></a>
                	</div>
                    <? if($this->Bed_and_breakfast_model->tripWidget):?>
                        <style type="text/css">
                            .TA_excellent{
                                display: inline-block;
                            }
                            .contact_forms.tripWidget img{
                                height: auto;
                                width: auto;
                                margin: 0px;
                                border: none;
                            }
                        </style>
                        <br>
                        <div class="paddingtop10 contact_forms tripWidget text-center"><?=$this->Bed_and_breakfast_model->tripWidget?></div>
                        <br>
                    <?endif;?>
                   <?php $this->view('frontend/includes/sidebar_activities',array("order_id"=>$this->Bed_and_breakfast_model->order_id,"table"=>"restaurants")); ?>
                </div>
            </div>
        </div>
    </section>
    <section class="bottom-section gray">
      <div class="container">
          <div class="row">
              <div class="col-md-12 text-center">

                  <?php $add = $this->Ads_model->get('970X90'); ?>
          <?php if ($add): ?>
              <?php if ($add->src != ''): ?>
                  <?php if ($add->type == 'image'): ?>
                      <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                  <?php else: ?>
                      <?php echo base64_decode($add->src); ?>
                  <?php endif; ?>
              <?php else: ?>
                  <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
              <?php endif; ?>
          <?php else: ?>
              <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
          <?php endif; ?>
              </div>

          </div>

      </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
