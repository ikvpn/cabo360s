<link rel="stylesheet" type="text/css" href="http://slidebox.vdmolen.xyz/dist/css/slidebox.css">
<style type="text/css">
  .hotel-slider-view{
    height: 400px;
  }
  .pt-20{
    padding-top: 20px;
  }
  .hotel-slider-view .box-carousel{
      height: 100%;
      padding-bottom: 0px !important;
      width: 100%;
  }
  .hotel-slider-view .box-carousel .slide {
      padding: 0 !important;
      height: 100% !important;
  }
  .price-view{
    text-align: right;
  }
  .hotel-slider-view .slide-box.box-zoomed {
      z-index: 9999;
      background-color: rgb(255, 255, 255, 0.9);
  }
  .hotel-slider-view .close {
      right: 30px;
      left: auto;
      top: 2px;
      opacity: 1;
  }
  .hotel-slider-view .slide-detail{
    top: 0;
  }
  .hotel-slider-view .arrow.prev, 
  .hotel-slider-view .prev, 
  .hotel-slider-view .arrow.next, 
  .hotel-slider-view .next {
      cursor: default;
      pointer-events: none;
  } 
  .hotel-slider-view .box-zoomed .arrow.prev, 
  .hotel-slider-view .box-zoomed .prev{
      cursor: url("<?php echo base_url(); ?>assets/images/arrow-long-left.svg"),
              url("<?php echo base_url(); ?>assets/images/arrow-long-left.png"),pointer;
      pointer-events: auto;
  }
  .hotel-slider-view .box-zoomed .arrow.next, 
  .hotel-slider-view .box-zoomed .next {
      cursor: url("<?php echo base_url(); ?>assets/images/arrow-long-right.svg"),
              url("<?php echo base_url(); ?>assets/images/arrow-long-right.png"),pointer;
      pointer-events: auto;
  }
  .contact_forms img{
    margin: 25px auto;
  }
  @media screen and (max-width: 991px){
    .contact_forms{
      margin-top: 20px;
    }
  }
</style>
<div id="wrap" class="detail-page-wrapper">
  <div class="menu-bg"></div>
  <?php $this->view('frontend/includes/booking_search_form'); ?>
  <?php $item_array = (array) $this->Apartment_model; ?>
    <div class="container-fluid">
     <div class="row">
       <div class="hotel-slider-view">
        <?php
            //path to directory to scan
            $directory = "uploads/apartments/" . $item_array['id'] . '/';
            //get all image files with a .jpg extension. This way you can add extension parser
            $images = glob($directory . "{*.jpg,*.gif}", GLOB_BRACE);
            $listImages = array();
        ?>
          <div class="slide-box box-carousel">
            <img class="slide slide-active slider-first-image" style="cursor: pointer;" src="<?php echo base_url(); ?>uploads/apartments/<?php echo $this->Apartment_model->photo; ?>">
            <?php if(count($images) > 0){
              foreach ($images as $image) {
            ?>
              <img class="slide slider-sub-images" src="<?php echo base_url() . $image; ?>">
            <?php }}?>
            <div class="controls">
              <a href="#" class="close close-button">Close</a>
              <a href="#" class="prev disabled">Previous</a>
              <a href="#" class="next">Next</a>
            </div>
          </div>
          
        </div>
     </div>
   </div>
    <section class="page-content paddingbot20">
        <div class="container">
          <div class="row">
              <div class="col-md-12 paddingbot20">
                  <ul class="breadcrumbs list-inline">
                      <li>
                          <span class="glyphicon glyphicon-home"></span>
                      </li>
                      <li><a href="<?php echo base_url().$this->lang->lang(); ?>">Procida</a>
                      </li>
                      <li>
                          <span class="glyphicon glyphicon-chevron-right"></span>
                      </li>
                      <li><a href="<?php echo base_url().$this->lang->lang().'/'.lang('where-to-stay').'/holiday-houses' ?>">Villas</a>
                      </li>
                      <li>
                          <span class="glyphicon glyphicon-chevron-right"></span>
                      </li>
                      <li><a href="#"><?php echo $item_array[$this->lang->lang()."Name"]; ?></a>
                      </li>
                  </ul>
              </div>
			  <div class="col-md-8">
               <div class="row">
                  <div class="col-md-12">
                     <img style="width: 100%;object-fit: cover;" src="<?php /*echo base_url(); */?>uploads/apartments/<?php /*echo $this->Apartment_model->photo; */?>" class="img-responsive" alt="">
                  </div>
                  <div class="col-md-8">
                     <h3 class="main-title dark restaurant-title"><?php echo $item_array[$this->lang->lang()."Name"]; ?></h3>
                     <ul class="list-inline restaurant-info paddingtop20">
                        <li>
                           <span class="glyphicon glyphicon-map-marker color2"></span>&nbsp;<?php echo $this->Apartment_model->getLocationName($this->lang->lang()); ?>
                        </li>
                     </ul>
                  </div>
                  <div class="col-md-4 price-view">
                     <?php $unit = lang('day') ;
                        if($this->Apartment_model->unit == 'W'){
                          $unit = lang('week');
                        }
                        else if($this->Apartment_model->unit == 'M'){
                            $unit = lang('month');
                        }
                        else if($this->Apartment_model->unit == 'N'){
                            $unit = lang('night');
                        }
                        else if($this->Apartment_model->unit == 'Y'){
                            $unit = lang('year');
                        }
                        ?>
                     <h3><strong><?php echo $this->Apartment_model->minPrice ?> - <?php echo $this->Apartment_model->maxPrice ?> $</strong></h3>
                     <div class="price-per"><?php echo lang('price_for'); ?> <?php echo $unit; ?></div>
                  </div>
                  
                  <div class="col-md-12">
                     <hr class="procida">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="detail-summary">
                              <p>
                                 <strong><?php echo lang('Square_feet'); ?></strong>
                                 <span><?php echo $this->Apartment_model->trypology ?></span>
                              </p>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="detail-summary">
                              <p>
                                 <strong><?php echo ucfirst(lang('bedrooms')); ?></strong>
                                 <span><?php echo $this->Apartment_model->rooms; ?></span>
                              </p>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="detail-summary">
                              <p>
                                 <strong><?php echo lang('Beds') ?></strong>
                                 <span><?php echo $this->Apartment_model->beds ?></span>
                              </p>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="detail-summary">
                              <p>
                                 <strong><?php echo lang('Pax'); ?></strong>
                                 <span><?php echo $this->Apartment_model->pax; ?></span>
                              </p>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="detail-summary">
                              <p>
                                 <strong><?php echo lang('bathroom'); ?></strong>
                                 <span><?php echo $this->Apartment_model->bathrooms; ?></span>
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php if($this->Apartment_model->home_heighlights != null && $this->Apartment_model->home_heighlights != ""){ ?>
                  <div class="col-md-12 paddingtop20">
                     <h3>Villas Highlights</h3>
                     <hr class="procida">
                     <div class="row">
                        <?php 
                           $unSerializeHeighlights = unserialize($this->Apartment_model->home_heighlights); 
                           foreach ($unSerializeHeighlights as $key => $value) {
                           ?>
                        <div class="col-md-6">
                           <div class="detail-summary">
                              <p>
                                 <strong><?php echo $value['name']; ?></strong>
                              </p>
                           </div>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
                  <?php } ?>
                  <div class="col-md-12 paddingtop20">
                     <h3>Details</h3>
                     <hr class="procida">
                     <?php
                        echo $item_array[$this->lang->lang()."Description"];
                        ?>
                  </div>
                  <div class="col-md-12 paddingtop20">
                     <h3>Area</h3>
                     <hr class="procida">
                     <div id="map-canvas-sidebar"></div>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="paddingtop10 contact_forms text-center">
                  <h4 class="noMarginBottom"><?php echo lang("contact_from"); ?></h4>
                  <h3 class="noMarginTop"><strong><?php echo $this->Apartment_model->manager ?></strong></h3>
                  <?php if ($this->Apartment_model->manager_profile != null && $this->Apartment_model->manager_profile != ''): ?>
                  <?php $url = base_url() . "uploads/apartments/".$this->Apartment_model->manager_profile; ?>
                  <?php if (@getimagesize($url)): ?>
                  <img class="img-responsive img-circle" src="<?php echo base_url() . "uploads/apartments/".$this->Apartment_model->manager_profile ?> "/>
                  <?php else: ?>
                  <img class="img-responsive img-circle" src="<?php echo base_url() . "assets/images/user-icon-placeholder.png" ?> "/>
                  <?php endif; ?>
                  <?php else: ?>
                  <img class="img-responsive img-circle" src="<?php echo base_url() . "assets/images/user-icon-placeholder.png" ?> "/>
                  <?php endif; ?>
                  <h3 class="phone"> <i style="color:#90B323" class="glyphicon glyphicon-earphone color2"></i> <?php echo $this->Apartment_model->phone ?></h3>
                  <a href="#" class="btn btn-purity btn-lg btn-block btn-request btn-sendemail btn80" data-orderid="<?php echo $this->Apartment_model->order_id ?>" data-type="restaurants"><?= lang("Send_a_request");?> <span class="glyphicon glyphicon-chevron-right"></span></a>
               </div>
               <? if($this->Apartment_model->tripWidget):?>
               <style type="text/css">
                  .TA_excellent{
                  display: inline-block;
                  }
                  .contact_forms.tripWidget img{
                  height: auto;
                  width: auto;
                  margin: 0px;
                  border: none;
                  }
               </style>
               <br>
               <div class="paddingtop10 contact_forms tripWidget text-center"><?=$this->Apartment_model->tripWidget?></div>
               <br>
               <?endif;?>
               <?php $this->view('frontend/includes/sidebar_activities',array("order_id"=>$this->Apartment_model->order_id,"table"=>"restaurants")); ?>
            </div>
          </div>
            
        </div>
    </section>
    <section class="bottom-section gray">
      <div class="container">
          <div class="row">
             <?php if($blocks) {?>
                <section class="page-content paddingtop40 paddingbot40 home-page sec-4" style="background-color:#FBFBFB">
                    <div class="container">
                        <div class="row paddingtop10">
                            <?php foreach ($blocks as $block): ?>
                            <div class="col-md-3">
                                <a href="<?php echo $block->link; ?>">

                                    <div class="overview-item">
                                        <div class="overview-image">
                                            <img src="<?php echo base_url(); ?>uploads/block/<?php echo $block->p_image; ?>" alt="<?php echo $block->title; ?>" class="lazy2 img-responsive img-rounded">
                                        </div>
                                        <h3><?php echo $block->title; ?></h3>
                                        <p class="index-item-meta"><?php echo base64_decode($block->description); ?></p>
                                    </div>
                                </a>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </section>
            <?php } ?>
          </div>
      </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
<script type="text/javascript" src="http://slidebox.vdmolen.xyz/dist/scripts/slidebox-min.js"></script>
<script type="text/javascript">
//   $('.close-button').click(function(){
//     $('.slider-first-image').addClass('slide-active');
//     $('.slider-sub-images').removeClass('slide-active');
//   })
</script>