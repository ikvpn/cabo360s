<?php
/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 2.0
 * File:
 * Description:
 */
?>
<script type="text/javascript">
    var markers = [];
</script>
<!--################ WRAP START ################-->
<div id="wrap" class="detail-page-wrapper index">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 paddingbot20">
                    <ul class="breadcrumbs list-inline">
                        <li>
                            <span class="glyphicon glyphicon-home"></span>
                        </li>
                        <li><a href="<?php echo base_url() . $this->lang->lang(); ?>">Procida</a>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </li>
                        <li><a href="#"><?php echo lang("Beaches"); ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="sub-title"><?php echo lang('Beaches'); ?></h2>
                </div>
            </div>
            <div class="row paddingtop10">
                <?php if (isset($m_beaches) && !empty($m_beaches)): ?>
                    <?php foreach ($m_beaches as $beach): ?>
                        <?php $poi_array = (array) $beach; ?>
                        <script type="text/javascript">
                        <?php if($this->lang->lang() != 'it') { ?>
                            markers.push(['<?php echo addslashes($poi_array[$this->lang->lang() . 'Title']); ?>', <?php echo $beach->lat ?>, <?php echo $beach->lng ?>]);
                        <?php }else{ ?>
                        	markers.push(['<?php echo addslashes($poi_array['title']); ?>', <?php echo $beach->lat ?>, <?php echo $beach->lng ?>]);
                        <?php } ?>
                        </script>
                        <div class="col-md-4">
                            <a href="<?php echo base_url() . $this->lang->lang() . '/' . lang('beach') . '/' . str_replace(' ', '-', strtolower($beach->title)) . ''; ?>">
                                <div class="overview-item">
                                    <div class="overview-image">
                                        <img src="<?php echo base_url() . "uploads/beaches/" . $beach->p_thumbnail; ?>" alt="" class="lazy img-responsive img-rounded" style="height: 230px !important;">
                                    </div>
                                    <h3><?php echo $beach->title; ?></h3>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>

                <?php endif; ?>
            </div>
        </div>
    </section>

    <section class="map-section">
        <div id="map-canvas"></div>
        <div class="container">
        </div>
    </section>

    <section class="bottom-section hidden-xs hidden-sm">

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else:
                                ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>

                        <?php endif; ?>
                  
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>
