<div id="wrap" class="detail-page-wrapper">
    <div class="menu-bg"></div>
    <?php $this->view('frontend/includes/booking_search_form'); ?>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="sub-title"><?php echo lang('Blog'); ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12 paddingtop10">
                            <!-- listing item -->
                            <div class="blog-detail">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="blog-image">
                                            <img src="<?php echo base_url(); ?>uploads/blog/<?php echo $post->p_image ?>" class="img-responsive img-rounded" alt="...">
                                        </div>
                                        <h2 class="blog-title blog-title-single paddingtop20">
                                            <?php echo $post->title; ?>
                                        </h2>
                                        <h3 class="blog-date blog-date-single-author italic"><?php echo date('F d, Y', strtotime($post->date)); ?>, by <a href="#"><?php echo $post->author; ?></a>
                                        </h3>
                                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                        <div class="addthis_native_toolbox"></div>
                                        <?php $add = $this->Ads_model->get('468X60'); ?>
                                        <?php if ($add): ?>
                                            <?php if ($add->src != ''): ?>
                                                <?php if ($add->type == 'image'): ?>
                                                    <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                                <?php else: ?>
                                                    <?php echo base64_decode($add->src); ?>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <img src="http://placehold.it/468x60" alt="">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img src="http://placehold.it/468x60" alt="">
                                        <?php endif; ?>
                                        <!-- <hr class="small"> -->
                                        <p class="blog-details paddingtop10">
                                            <?php echo base64_decode($post->description); ?>
                                        </p>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                        <i class="picon picon-tag icon-adjust"></i>
                                        <strong><?php echo lang('tags'); ?>:</strong>
                                        <ul class="list-tags list-inline">
                                            <?php //var_dump($post); ?>
                                            <?php $tags = explode(',', $post->meta_keywords); ?>
                                            <?php foreach ($tags as $tag): ?>
                                                <li><a href="#"><?php echo $tag ?>,</a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 paddingtop20">
                            <div class="fb-comments" data-href="<?php echo base_url() . uri_string(); ?>" data-numposts="5" data-colorscheme="light"></div>
                            <br>
                        </div>

                    </div>
                </div>
                <div class="col-md-4 paddingtop20">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="sidebar-element">
                                <?php $add = $this->Ads_model->get('300X250'); ?>
                                <?php if ($add): ?>
                                    <?php if ($add->src != ''): ?>
                                        <?php if ($add->type == 'image'): ?>
                                            <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                                        <?php else: ?>
                                            <?php echo base64_decode($add->src); ?>
                                        <?php endif; ?>
                                    <?php else: ?>
                                       <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="http://placehold.it/300x250&text=300+x+250" class="img-responsive" alt="..">
                                <?php endif; ?>
                                
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="sidebar-element">
                                <h4 class="sidebar-title"><?php echo lang('Upcoming_Events'); ?></h4>
                                <?php if(isset($events) && is_array($events)): ?>
                                <?php foreach($events as $event): ?>
                                <?php $event_array = (array) $event; ?>
                                  <div class="sidebar-event">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="dateBox">
                                                <span class="date"><?php echo date('d',  strtotime($event->startDate)); ?></span>
                                                <span class="month"><?php echo date('M', strtotime($event->startDate)); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-8 noPaddingLeft">
                                            <h3 class="sidebar-event-heading"><?php echo $event_array[$this->lang->lang().'Title'] ?></h3>
                                            <p class="sidebar-event-date"><?php echo date('M d, Y', strtotime($event->startDate)); ?></p>
                                            <p class="sidebar-event-location">Marina Corricella</p>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bottom-section gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                  <?php $add = $this->Ads_model->get('970X90'); ?>
                    <?php if ($add): ?>
                        <?php if ($add->src != ''): ?>
                            <?php if ($add->type == 'image'): ?>
                                <img src="<?php echo base64_decode($add->src); ?>" class="img-responsive" alt="...">
                            <?php else: ?>
                                <?php echo base64_decode($add->src); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                        <?php endif; ?>
                    <?php else: ?>
                        <img src="http://placehold.it/970x90&text=ads+/+970+x+90" class="img-responsive" alt="...">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<!--################ PUSH WILL KEEP THE FOOTER AT BOTTOM IF YOU WANT TO CREATE OTHER PAGES ################-->
<div id="push"></div>