<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['translate_uri_dashes'] = FALSE;
$route['default_controller'] = 'welcome';
$route['upload'] = 'upload';
$route['upload/do_upload'] = 'upload/do_upload';
// URI like '/en/about' -> use controller 'about'

/* STATIC PAGES ROUTES */
$route['^(en|it|fr|du)/traghetti-napoli-procida'] = 'useful_info/index/ferry-time-table';
$route['^(en|it|fr|du)/traghetti-procida-napoli'] = 'useful_info/index/ferry-time-table';
$route['^(en|it|fr|du)/traghetti-procida-pozzuoli'] = 'useful_info/index/ferry-time-table';
$route['^(en|it|fr|du)/traghetti-pozzuoli-procida'] = 'useful_info/index/ferry-time-table';
$route['^(en|it|fr|du)/traghetti-procida-ischia'] = 'useful_info/index/ferry-time-table';
$route['^(en|it|fr|du)/traghetti-ischia-procida'] = 'useful_info/index/ferry-time-table';

$route['^(en|it|fr|du)/privacy-policy'] = "static_pages/index/privacy-policy";
$route['^(en|it|fr|du)/terms-conditions'] = "static_pages/index/terms-conditions";

/* New Page Route Example */
//$route['^(en|it|fr|du)/example-page'] = "static_pages/index/example-page";

$route['^(en|it|fr|du)/marina-chiaiolella/(.+)$'] = "what_to_visit/index/marina-chiaiolella/$2";
$route['^(en|it|fr|du)/marina-grande/(.+)$'] = "what_to_visit/index/marina-grande/$2";
$route['^(en|it|fr|du)/terra-murata/(.+)$'] = "what_to_visit/index/terra-murata/$2";
$route['^(en|it|fr|du)/marina-corricella/(.+)$'] = "what_to_visit/index/marina-corricella/$2";
$route['^(en|it|fr|du)/marina-chiaiolella'] = "what_to_visit/index/marina-chiaiolella";
$route['^(en|it|fr|du)/marina-grande'] = "what_to_visit/index/marina-grande";
$route['^(en|it|fr|du)/terra-murata'] = "what_to_visit/index/terra-murata";
$route['^(en|it|fr|du)/marina-corricella'] = "what_to_visit/index/marina-corricella";

$route['^(en|it|fr|du)/places_of_interest/(.+)$'] = "what_to_visit/index/places_of_interest/$2";
$route['^(en|it|fr|du)/places-of-interest/(.+)$'] = "what_to_visit/index/places_of_interest/$2";
$route['^(en|it|fr|du)/places-of-interest'] = "what_to_visit/index/places_of_interest";
$route['^(en|it|fr|du)/luoghi-di-interesse/(.+)$'] = "what_to_visit/index/places_of_interest/$2";
$route['^(en|it|fr|du)/luoghi-di-interesse'] = "what_to_visit/index/places_of_interest";


$route['^(en|it|fr|du)/what_to_visit/(.+)$'] = "what_to_visit/index/$2";
$route['^(en|it|fr|du)/cosa-visitare/(.+)$'] = "what_to_visit/index/$2";
$route['^(en|it|fr|du)/what-to-visit/(.+)$'] = "what_to_visit/index/$2";
$route['^(en|it|fr|du)/locations'] = "what_to_visit/index/locations";
$route['^(en|it|fr|du)/localita'] = "what_to_visit/index/locations";

$route['^(en|it|fr|du)/where_to_stay/(.+)$'] = "where_to_stay/index/$2";
$route['^(en|it|fr|du)/where-to-stay/(.+)$'] = "where_to_stay/index/$2";
$route['^(en|it|fr|du)/dove-dormire/(.+)$'] = "where_to_stay/index/$2";
$route['^(en|it|fr|du)/unterkunft/(.+)$'] = "where_to_stay/index/$2";

$route['^(en|it|fr|du)/hotels/(.+)$'] = "where_to_stay/index/hotels/$2";
$route['^(en|it|fr|du)/hotels'] = "where_to_stay/index/hotels";
$route['^(en|it|fr|du)/hotel/(.+)$'] = "where_to_stay/index/hotels/$2";
$route['^(en|it|fr|du)/hotel'] = "where_to_stay/index/hotels";

$route['^(en|it|fr|du)/hotels-detail/(.+)$'] = "where_to_stay/index/hotel-details/$2";
$route['^(en|it|fr|du)/hotel-detail/(.+)$'] = "where_to_stay/index/hotel-details/$2";

$route['^(en|it|fr|du)/eat_and_drink/(.+)$'] = "eat_and_drink/index/$2";
$route['^(en|it|fr|du)/eat-and-drink/(.+)$'] = "eat_and_drink/index/$2";
$route['^(en|it|fr|du)/mangiare-e-bere/(.+)$'] = "eat_and_drink/index/$2";

$route['^(en|it|fr|du)/what_to_do/(.+)$'] = "what_to_do/index/$2";
$route['^(en|it|fr|du)/activities/(.+)$'] = "what_to_do/index/$2";
$route['^(en|it|fr|du)/cosa-fare/(.+)$'] = "what_to_do/index/$2";
$route['^(en|it|fr|du)/aktivitäten/(.+)$'] = "what_to_do/index/$2";

$route['^(en|it|fr|du)/events/(.+)$'] = "events/index/$2";
$route['^(en|it|fr|du)/eventi/(.+)$'] = "events/index/$2";
$route['^(en|it|fr|du)/eschehen/(.+)$'] = "events/index/$2";

$route['^(en|it|fr|du)/tours/(.+)$'] = "tours/index/$2";
$route['^(en|it|fr|du)/tour/(.+)$'] = "tours/tour/$2";

$route['^(en|it|fr|du)/transfer/(.+)$'] = "transfer/index/$2";
$route['^(en|it|fr|du)/transfer/(.+)$'] = "transfer/transfer/$2";

$route['^(en|it|fr|du)/villas'] 	 = "where_to_stay/index/holiday-houses";
$route['^(en|it|fr|du)/villa/(.+)$'] = "where_to_stay/index/holiday-house/$2";

$route['^(en|it|fr|du)/beach/(.+)$'] = "beach/index/$2";
$route['^(en|it|fr|du)/spiagge/(.+)$'] = "beach/index/$2";
$route['^(en|it|fr|du)/strande/(.+)$'] = "beach/index/$2";
$route['^(en|it|fr|du)/beaches'] = "beach/index";
$route['^(en|it|fr|du)/spiagge'] = "beach/index";
$route['^(en|it|fr|du)/strande'] = "beach/index";



$route['^(en|it|fr|du)/useful_info/(.+)$'] = "useful_info/index/$2";
$route['^(en|it|fr|du)/useful-info/(.+)$'] = "useful_info/index/$2";
$route['^(en|it|fr|du)/info-utili/(.+)$'] = "useful_info/index/$2";
$route['^(en|it|fr|du)/nützliche-Infos/(.+)$'] = "useful_info/index/$2";
$route['^(en|it|fr|du)/ferry-timetable'] = "useful_info/index/ferry-time-table";
$route['^(en|it|fr|du)/orari-traghetti'] = "useful_info/index/ferry-time-table";
$route['^(en|it|fr|du)/ferry-timetable/(.+)$'] = "useful_info/index/ferry-time-table/$2";
//$route['^(it)/orari-traghetti(.+)$'] = "useful_info/index/ferry-time-table/";
//$route['^(en|it|fr|du)/ferryapi/(.+)$'] = "ferryapi/index/$2";

// '/en', '/de', '/fr' and '/nl' URIs -> use default controller
$route['^(en|it|fr|du)/(.+)$'] = "$2";
$route['^(en|it|fr|du)$'] = $route['default_controller'];

$route['^(en|it|fr|du)/HotelBB/hotelAvailability'] = "HotelBB/hotelAvailability";
$route['404_override'] = $route['default_controller'];


//$route['default_controller'] = 'pages/view';
//$route['(:any)'] = 'pages/view/$1';
//$route['news/(:any)'] = 'news/view/$1';
//$route['guest'] = 'home';
//$route['(:any)'] = 'contents/view/$1';
//$route['default_controller'] = 'pages/view';
//$route['news/create'] = 'news/create';
//============================
//$route['login_control/(:any)'] = 'login_control/curl/$1';
//$route['login_control'] = 'login_control';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
