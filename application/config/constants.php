<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
| Custom constants rules
*/

define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

/* Production */
//define('RECAPTCHA_SITE_KEY','6LdcyxsTAAAAADmrFEg6cIEr9KXHtaKuBWAUeqGa');
//define('RECAPTCHA_SECRET_KEY','6LdcyxsTAAAAAFrwDtjimnQyys-zsJ0Xoo52MRXg');
define('RECAPTCHA_SITE_KEY','6LcGiyQUAAAAAEXD-RBM0GuVZGMl0WIe9-5-f1i7');
define('RECAPTCHA_SECRET_KEY','6LcGiyQUAAAAAEPst8GmBUlvkYHpYKyFilZ_lh61');

/* Development */
//define('RECAPTCHA_SITE_KEY','6LfoZiYTAAAAABDCemaCrih8sRM_B67jvQ6bQdVZ');
//define('RECAPTCHA_SECRET_KEY','6LfoZiYTAAAAAMGwh7vfTIlcq-VgZy4fGh7O56Wn');
//

define ("PICK_UP_POINTS", json_encode(array (
    // 1 => '(CIA) Rome Airport Ciampino',
    // 2 => '(FCO) Rome Airport Fiuminico',
    // 3 => '(NAP) Naples International Airport',
    // 4 => 'Napoli Centrale railway station',
    5 => '(SJD) San José del Cabo',
    6 => '(CSL) Cabo San Lucas'
))); 

define ("CARS_TYPE", json_encode(array (
    1 => 'Taxi',
    2 => 'Ncc',
    3 => 'Mercedes',
))); 

define ("BOATS_TYPE", json_encode(array (
    1 => 'Traghetto',
    2 => 'Aliscafo',
    3 => 'Yacht privato',
))); 

define ("TRANSFERS_TYPE", json_encode(array (
    'car_transfer' => 'Car Transfer',
    'yacht_transfer' => 'Yacht Transfer',
))); 
// Live
define('STRIPE_PUBLISHABLE_KEY','pk_live_1se6kMGTofi4XirJxt7cLmLl00AqMK21XS');
define('STRIPE_SECRET_KEY','sk_live_Ia8QHOZpe70t8t50RRoaUL5R00wtbNxPdD');

// Test
// define('STRIPE_PUBLISHABLE_KEY','pk_test_V6OXPhPiNfLfmHetIHpu86qf00Iohrc9Dp');
// define('STRIPE_SECRET_KEY','sk_test_Vsf9AI6NfncJdMLL2MbAH4o100IiJrLOUF');


define('FROM_MAIL','easyislandprocida@gmail.com');
define('ADMIN_TO_MAIL','easyislandprcida@gmail.com');
define('BCC_MAIL','vpn.testings@gmail.com');