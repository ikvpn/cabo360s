<?php



/*

 * Author: Innam Hunzai

 * Email: innamhunzai@gmail.com

 * Project: Visit Procida

 * Version: 1.0

 * File:

 * Description:

 */





$lang['Boat_Rentals_in_Procida'] = 'Rentals in Procida';

$lang['Upcoming_Events'] = 'Upcoming Events';

$lang['events_not_found_mesg'] = 'Currently there are no any events to show.';

$lang['Boat_Rental_Not_Found_Msg'] = 'No any boat rental found.';

$lang['Boat_Rentals'] = 'Boat Rentals';



$lang['classes'] = "classes";

$lang['rental'] = "rental";

$lang['excusions'] = "excusions";

$lang['island tour'] = "island tour";

$lang['boat-rental'] = "boat rental";

$lang['kayak-rental'] = "kayak rental";

$lang['boat-excusions'] = "boat excusions";

$lang['moorings'] = "moorings";

$lang['starting_from']='Starting from';
