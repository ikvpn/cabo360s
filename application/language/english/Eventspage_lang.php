<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$lang['Traditional_Events'] = 'Traditional Events';
$lang['Events_by_Categories'] = 'Events by Categories';
$lang['Music'] = 'Music';
$lang['Prccession'] = 'Prccession';
$lang['Festivals'] = 'Festivals';
$lang['Info'] = 'Info';
$lang['Prices'] = 'Prices';
$lang['Venue_Details'] = 'Venue Details';
$lang['Events_by_Categories'] = 'Events by Categories';
$lang['Adults'] = 'Adults';
$lang['Childs'] = 'Child\'s';
$lang['Guests'] = 'Guests';
$lang['Send_a_request'] = 'Send a request';
$lang['Upcoming_Events'] = 'Upcoming Events';
$lang['Event'] = 'Event';
$lang['Location'] = 'Location';
$lang['Time'] = 'Time';
$lang['Date'] = 'Date';
$lang['Days'] = 'Days';
$lang['Event_Not_Found_Msg'] = 'No any event to show.';
$lang['Phone'] = 'Phone';
$lang['Website'] = 'Website';
$lang['Mobile'] = 'Mobile';
$lang['Address'] = 'Address';

$lang['January'] = 'January';
$lang['February'] = 'February';
$lang['March'] = 'March';
$lang['April'] = 'April';
$lang['May'] = 'May';
$lang['June'] = 'June';
$lang['July'] = 'July';
$lang['August'] = 'August';
$lang['September'] = 'September';
$lang['October'] = 'October';
$lang['November'] = 'November';
$lang['December'] = 'December';

$lang['Monday'] = 'Monday';
$lang['Tuesday'] = 'Tuesday';
$lang['Wednesday'] = 'Wednesday';
$lang['Thrusday'] = 'Thrusday';
$lang['Friday'] = 'Friday';
$lang['Saturday'] = 'Saturday';
$lang['Sunday'] = 'Sunday';
$lang['starting_from']='Starting from';

