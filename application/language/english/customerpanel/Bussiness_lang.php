<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['display_name'] = 'Display Name';
$lang['description'] = 'Description';
$lang['sno'] = 'S.No';
$lang['name'] = 'Name';
$lang['location'] = 'Location';
$lang['status']='Status';
$lang['actions'] = 'Actions';
$lang['edit'] = 'Edit';
$lang['save'] = 'Save';
$lang['upload_primary_image'] = 'Upload Primary Image';
$lang['primary_image'] = 'Primary Image';
$lang['upload'] = 'Upload';
$lang['bars_cafe'] = 'Bars and Cafe';
$lang['categories'] = 'Categories';
$lang['business_details'] = 'Business Details';
$lang['title'] = 'Title';
$lang['phone'] = 'Phone';
$lang['address'] = 'Address';
$lang['payment_details'] = 'Payment Details';
$lang['payment_plan'] = 'Payment Plan';
$lang['payment_option'] = 'Payment Option';
$lang['paypal'] = 'Pay pal';
$lang['offline'] = 'Offline';
$lang['billing_details'] = 'Billing Details';
$lang['email'] = 'Email';
$lang['checkout'] = 'Check Out';
