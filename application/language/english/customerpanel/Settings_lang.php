<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$lang['profile_details'] = "Profile Details";
$lang['enter_your_first_name'] = "Enter your first name";
$lang['enter_your_last_name'] = "Enter your last name";
$lang['address_line_one'] = "Address line 1";
$lang['address_line_two'] = 'Address line 2';
$lang['city_name'] = "City Name";
$lang['post_code'] = 'Post Code';
$lang['state'] = 'State';
$lang['country'] = 'country';
$lang['phone_number'] = 'Phone Number';
$lang['company_name'] = 'Company Name';

