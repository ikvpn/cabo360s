<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */
$lang['min_price'] = 'Min Price';
$lang['max_price'] = 'Max Price';
$lang['category'] = 'Category';
$lang['registerd_shops'] = 'Registerd Shops';
$lang['typology'] = "Typology";
$lang['categories'] = 'Categories';
