<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['slogan'] = 'Slogan';
$lang['phone']='Phone';
$lang['website']='Website';
$lang['minimum_price'] = 'Minimum Price';
$lang['maximum_price'] = 'Maximum Price';
$lang['cuisine'] = 'Cuisine';
$lang['topology'] = 'Topology';
$lang['credit_cards']='Credit Cards';
$lang['hours']='Working Hours';
$lang['closing_day']='Closing Day';
$lang['info']='Info';
$lang['address']='Address';
$lang['menu_price']='Menu Price';
$lang['invited_people']='Invited People';
$lang['categories'] = 'Categories';
