

<?php


$lang['invoice'] = ' Invoice';
$lang['invoice_details'] = 'Invoice Details For';
$lang['page_info'] = 'Page Information';
$lang['title'] = 'Title';
$lang['category'] = 'Category';
$lang['location'] = 'Location';
$lang['reg_date'] = 'Registered date';
$lang['cust_info'] = 'Customer Information';
$lang['cust_name'] = 'Name';
$lang['cust_phone'] = 'Phone';
$lang['cust_company'] = 'Company';
$lang['paym_type'] = 'Payment Type';
$lang['paym_plan'] = 'Payment Plan';
$lang['paym_details'] = 'Payment Details';
$lang['paym_amount'] = 'Agreement Amount';
$lang['paym_status'] = 'Status';
$lang['monthly'] = 'Monthly';
$lang['yearly'] = 'Yearly';
$lang['billing_details'] = 'Billing Details';
$lang['billing_address'] = 'Billing Address';
$lang['billing_phone'] = 'Billing Phone';
