<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['minimum_price'] = 'Minimum Price';
$lang['maximum_price'] = 'Maximum Price';
$lang['price_unit'] = 'Price Unit';
$lang['per_day'] = 'Per Day';
$lang['per_night'] = 'Per Night';
$lang['per_week'] = 'Per Week';
$lang['per_month'] = 'Per Month';
$lang['per_year'] = 'Per Year';
$lang['rooms'] = 'Rooms';
$lang['categories'] = 'Categories';
$lang['pax'] = 'Pax';
$lang['beds'] = 'Beds';
$lang['trypology'] = 'Trypology';
$lang['bathrooms'] = 'Bathrooms';
$lang['terrace'] = 'Terrace';
$lang['registred_holiday_houses'] = 'Registerd Villas';
