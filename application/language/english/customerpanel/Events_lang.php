<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */


$lang['date_time'] = 'Date and Time';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['start_time'] = 'Start Time';
$lang['end_time'] = 'End Time';
$lang['map_info'] = 'Map Info';
$lang['lat'] = 'latitude';
$lang['lng'] = 'longitude';
$lang['prices'] = 'Prices';
$lang['adults'] = "Adults";
$lang['childs'] = "Child's";
$lang['guests'] = "Guests";
$lang['info'] = 'Info';
$lang['event_type'] = 'Event Type';
$lang['category'] = 'Category';
$lang['reservation'] = 'Reservation';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['web'] = 'Web';
$lang['phone'] = 'Phone';
$lang['mobile'] = 'Mobile';
$lang['email'] = 'Email';
$lang['address'] = 'Address';
$lang['registerd_events'] = 'Registered Events';