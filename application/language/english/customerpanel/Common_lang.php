<?php

$lang['dashboard'] = 'Dashboard';
$lang['customer_panel'] = 'Customer Panel';
$lang['logout'] = 'Logout';
$lang['settings'] = 'Settings';
$lang['register_business'] = 'Register Bussiness';
$lang['register'] = 'Register';
$lang['hotels'] = 'Hotels';
$lang['resturants'] = 'Resturants';
$lang['shops'] = 'Shops';
$lang['shopes'] = 'Shops';
$lang['apartments'] = 'Villas';
$lang['events'] = 'Events';
$lang['save'] = 'Save';
$lang['cancel'] = 'Cancel';
$lang['bars_cafe'] = 'Bars and Cafe';
$lang['boat_rentals'] = 'Rentals';
$lang['weddings'] = 'Weddings';
$lang['beach_clubs'] = "Beach Club";
$lang['nightlife'] = "Nightlife";
$lang['services'] = "Excursions and Visits";
$lang['tours'] = "Tours";
$lang['bed_and_breakfast'] = "Bed and Breakfast";
$lang['agencies'] = "Agencies";
$lang['yachts'] = 'Yachts';

/* BOAT RENTALS */
$lang['classes'] = "classes";
$lang['rental'] = "rental";
$lang['excusions'] = "excusions";
$lang['island tour'] = "island tour";
$lang['boat-rental'] = "boat rental";
$lang['kayak-rental'] = "kayak rental";
$lang['boat-excusions'] = "boat excusions";
$lang['moorings'] = "moorings";

/* WEDDING */
$lang['Catering'] = "Catering";
$lang['Hiring of kitchenware'] = "Hiring of kitchenware";
$lang['Linens and materials'] = "Linens and materials";
$lang['Wedding plan'] = "Wedding plan";
$lang['Waiter service'] = "Waiter service";
$lang['Beverage'] = "Beverage";
$lang['Music'] = "Music";
$lang['Location'] = "Location";
$lang['Venue provided'] = "Venue provided";
$lang['preparation'] = "preparation";
$lang['wedding cake'] = "wedding cake";
$lang['candy'] = "candy";

$lang['KM0'] = "KM0";
$lang['Traditional'] = "Traditional";
$lang['Regional'] = "Regional";
$lang['International'] = "International";
$lang['Fusion'] = "Fusion";
$lang['Mediterranean'] = "Mediterranean";
$lang['Vegetarian'] = "Vegetarian";
$lang['Vegan'] = "Vegan";

/*BEACH CLUBS*/
$lang['sunbed rental'] = "sunbed rental";
$lang['parasol rentals'] = "parasol rentals";
$lang['bar service'] = "bar service";
$lang['dining service'] = "dining service";


/*DAYS*/
$lang['0'] = "Lunedì";
$lang['1'] = "Martedì";
$lang['2'] = "Mercoledì";
$lang['3'] = "Giovedì";
$lang['4'] = "Venerdì";
$lang['5'] = "Sabato";
$lang['6'] = "Domenica";
