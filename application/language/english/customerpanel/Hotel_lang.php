<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['price'] = 'Price';
$lang['categories'] = 'Categories';
$lang['price_unit'] = 'Price Unit';
$lang['per_day'] = 'Per Day';
$lang['per_night'] = 'Per Night';
$lang['per_week'] = 'Per Week';
$lang['per_month'] = 'Per Month';
$lang['per_year'] = 'Per Year';
