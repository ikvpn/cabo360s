<?php
$lang['Average_price'] = 'Average price';
$lang['rating'] = 'Rating';
$lang['details'] = 'Details';
$lang['Shops_Not_Found_Msg'] = 'No any shop is found in Procida.';
$lang['Upcoming_Events'] = 'Upcoming Events';
$lang['hours'] = 'Working Hours';
$lang['price_range'] = 'Price Range';
$lang['credit_card'] = 'Credit Card';
$lang['closing_day'] = 'Closing Day';
$lang['info'] = 'Info';
$lang['topology'] = 'Tipology';
$lang['Address'] = "Address";
$lang['starting_from']='Starting from';
$lang['tipology']='Tipology';
$lang['category']='Category';

$lang['hotels'] = 'Hotel';
$lang['resturants'] = 'Restaurant';
$lang['shopes'] = 'Shop';
$lang['apartments'] = 'Holiday House';
$lang['bars_cafe'] = 'Bar and Cafè';
$lang['boat_rentals'] = 'Rental';
$lang['weddings'] = 'Wedding';
$lang['beach_clubs'] = 'Beach Club';
$lang['nightlife'] = 'Nightlife';
$lang['services'] = 'Escursion and visit';
$lang['agencies'] = 'Agencies';
