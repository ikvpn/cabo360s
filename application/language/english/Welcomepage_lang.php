<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['Welcome_in_Procida'] = 'Welcome to Procida';
$lang['Hotel_for_Stelle'] = 'Hotel for Stelle';
$lang['Hotel_Deals'] = 'Hotel Deals';
$lang['Holiday_Deals'] = 'Holiday Deals';


$lang['heading']='Welcome to Procida';
$lang['subheading']="THE SMALLEST ISLAND IN THE BAY OF NAPLES";
$lang['tour']="Top tours";
$lang['tour_sub']="BOOK YOUR TOUR ONLINE";

$lang['Footer_head']="Register your business on Visit Procida";

$lang['Footer_content']="Give more visibility to your business, reaching potential customers. VisitProcida is the official guide to tourism in Procida that brings tourists and tour operators in contact. In 2016 more than 100,000 users from around the world have visited our platform";

$lang['footer_line1']="Create a personal account";
$lang['footer_line2']="Record your activities include photos and info";
$lang['footer_line3']="Go online and start receiving reservations";


$lang['Hotel_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';
$lang['Places_of_Interest_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';
$lang['Beaches_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';


$lang['Escursion'] = 'Escursion';
$lang['Guided Visit'] = 'Guided Visit';
$lang['days'] = 'days';
$lang['hours'] = 'hours';

$lang['minutes'] = 'minutes';
$lang['starting form'] = 'starting form';

$lang['T'] = 'in total';

$lang['P'] = 'per person';

$lang['Top rated'] = 'Top rated';


$lang['hotels'] = 'Hotel';
$lang['resturants'] = 'Restaurant';
$lang['shopes'] = 'Shop';
$lang['apartments'] = 'Holiday House';
$lang['bars_cafe'] = 'Bar and Cafè';
$lang['boat_rentals'] = 'Rental';
$lang['weddings'] = 'Wedding';
$lang['beach_clubs'] = 'Beach Club';
$lang['nightlife'] = 'Nightlife';
$lang['services'] = 'Escursion and visit';

$lang['add_camera'] = 'Add room';
$lang['placeholder_form'] = 'Rooms:1 Adults: 2 Children: 0';
