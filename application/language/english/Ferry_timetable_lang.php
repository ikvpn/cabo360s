<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File: en [english file]
 * Description:
 */

$lang['departures_from_procida'] = 'Departures from Procida';
$lang['how_to_get_there'] = 'How to get to Procida';
$lang['ferry_timetables'] = 'Ferry Timetables';
$lang['naples'] = 'Naples';
$lang['pozzuoli'] = 'Pozzuoli';
$lang['ischia'] = 'Ischia';
$lang['departure'] = 'Departure';
$lang['time'] = 'Time';
$lang['destination'] = 'Destination';
$lang['ship'] = 'Ship';
$lang['company'] = 'Company';
$lang['all_destination'] = 'All Destination';
$lang['search'] = 'Search';
$lang['departure'] = 'Departure';
$lang['destination'] = 'Destination';
$lang['duration'] = 'Duration';
$lang['boat'] = 'Boat';
$lang['type'] = 'type';
$lang['company'] = 'Company';
$lang['today'] = 'today';
$lang['book'] = 'Book';
$lang['price'] = 'Price';
$lang['no_routes'] = 'There is any route at the moment';