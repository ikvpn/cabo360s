<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$lang['Bars_and_cafe_in_Procida'] = 'Bars and cafe in Procida';
$lang['Bars_and_Cafe'] = 'Bars and Cafe';
$lang['Upcoming_Events'] = 'Upcoming Events';
$lang['events_not_found_mesg'] = 'Currently there are no any events to show.';
$lang['Bars_and_Cafe_Not_Found_Msg'] = 'No any bars and cafe found.';
$lang['starting_from']='Starting from';