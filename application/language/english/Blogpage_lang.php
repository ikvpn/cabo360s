<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$lang['Blog'] = 'Blog';
$lang['Post'] = 'Post';
$lang['Upcoming_Events'] = 'Upcoming Events';
$lang['events_not_found_mesg'] = 'Currently there are no any events to show.';
$lang['Posts_Not_Found_Msg'] = 'Currently there are no any posts to show.';
$lang['tags'] = 'Tags';
$lang['next_post'] = 'next post';
$lang['prev_post'] = 'prev post';
$lang['Recent_Articles'] = 'Recent Articles';
