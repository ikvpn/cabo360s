<?php



/* 

 * Author: Innam Hunzai

 * Email: innamhunzai@gmail.com 

 * Project: Visit Procida

 * Version: 1.0

 * File: 

 * Description:

 */





$lang['Weddings_in_Procida'] = 'Weddings in Procida';

$lang['Weddings'] = 'Weddings';

$lang['Upcoming_Events'] = 'Upcoming Events';

$lang['events_not_found_mesg'] = 'Currently there are no any events to show.';

$lang['Weddings_Not_Found_Msg'] = 'No caterings or services found.';



/* WEDDING */

$lang['Catering'] = "Catering";

$lang['Hiring of kitchenware'] = "Hiring of kitchenware";

$lang['Linens and materials'] = "Linens and materials";

$lang['Wedding plan'] = "Wedding plan";

$lang['Waiter service'] = "Waiter service";

$lang['Beverage'] = "Beverage";

$lang['Music'] = "Music";

$lang['Location'] = "Location";

$lang['Venue provided'] = "Venue provided";

$lang['preparation'] = "preparation";

$lang['wedding cake'] = "wedding cake";

$lang['candy'] = "candy";



$lang['KM0'] = "KM0";

$lang['Traditional'] = "Traditional";

$lang["Regional"] = "Regional";

$lang['International'] = "International";

$lang['Fusion'] = "Fusion";

$lang['Mediterranean'] = "Mediterranean";

$lang['Vegetarian'] = "Vegetarian";

$lang['Vegan'] = "Vegan";

$lang['starting_from']='Starting from';