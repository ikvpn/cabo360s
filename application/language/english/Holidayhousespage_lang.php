<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['Villas_in_Procida'] = 'Villas in Cabo San Lucas';
$lang['BB_in_Procida'] = 'Bed and Breakfast in Procida';
$lang['Filters'] = 'Filters';
$lang['Rooms'] = 'Rooms';
$lang['Beds'] = 'Beds';
$lang['Pax'] = 'Pax';
$lang['Price_range'] = 'Price range';
$lang['Min'] = "Min";
$lang['Max'] = "Max";
$lang['night'] = "night";
$lang['price_for'] = "price for";
$lang['Location'] = 'Location';
$lang['Apartments_Not_Found_Msg'] = 'No any holiday apartment is found in Procida.';
$lang['BB_Not_Found_Msg'] = 'No any bed and breakfast is found in Procida.';
$lang['details'] = 'details';
$lang['bedrooms'] = 'bed rooms';
$lang['Topology'] = 'Tipology';
$lang['Meters'] = 'Meters';
$lang['starting_from']='Starting from';
$lang['beds'] = 'beds';
$lang['bed'] = 'bed';
$lang['rooms'] = 'rooms';
$lang['room'] = 'room';
$lang['bathroom'] = 'Bathroom';
$lang['Square_feet'] = 'Square feet';
