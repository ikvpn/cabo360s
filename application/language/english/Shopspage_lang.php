<?php

$lang['Shops_in_Procida'] = 'Shops in Procida';
$lang['Location'] = 'Location';
$lang['Average_price'] = 'Average price';
$lang['rating'] = 'Rating';
$lang['details'] = 'Details';
$lang['Shops_Not_Found_Msg'] = 'No any shop is found in Procida.';
$lang['Upcoming_Events'] = 'Upcoming Events';
$lang['hours'] = 'Working Hours';
$lang['price_range'] = 'Price Range';
$lang['credit_card'] = 'Credit Card';
$lang['closing_day'] = 'Closing Day';
$lang['info'] = 'Info';
$lang['topology'] = 'Topology';
$lang['Address'] = "Address";
$lang['starting_from']='Starting from';
$lang['Filters']='Filters';
$lang['Tags']='Tags';
$lang['Activity_services']='Services';
