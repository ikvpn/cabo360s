<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$lang['What_to_visit_in_Procida'] = 'What to visit in Procida';
$lang['location_not_found_message'] = 'No any locations found in Procida';
$lang['Routes'] = 'Routes';
$lang['Routes_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';
$lang['History']='History';
$lang['History_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';
$lang['Activities'] = 'Activities';
$lang['Activities_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';
