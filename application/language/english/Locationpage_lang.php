<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$lang['Find_Hotels_in_Procida'] = 'Find Hotels in Procida';
$lang['search_check_box_msg'] = 'I haven\'t decided dates yet';
$lang['Ticket_price'] = 'Ticket price';
$lang['Average_price'] = 'Average price';
$lang['Near_by_areas'] = 'Near by areas';