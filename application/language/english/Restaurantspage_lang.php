<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['Restaurants_in_Procida'] = 'Restaurants in Procida';
$lang['Resturant_Not_Found_Msg'] = 'No any restaurant is found in Procida.';
$lang['Nightlife_Not_Found_Msg'] = 'No any nightlife is found in Procida.';
$lang['Upcoming_Events'] = 'Upcoming Events';
$lang['events_not_found_mesg'] = 'Currently there are no any upcoming events';
$lang['Restaurants'] = 'Restaurants';
$lang['starting_from']='Starting from';
$lang['contact_from']='Contact';
$lang['Filters']='Filters';
$lang['Tags']='Tags';
$lang['Activity_services']='Services';
