<?php

$lang['all'] = 'All';
$lang['tours'] = 'Tours';
$lang['escursion'] = 'Excursions';
$lang['visit'] = 'Experiences';
$lang['tours_title'] = 'Premiere Experiences in Cabo';
$lang['filter'] = 'Filter by';
$lang['from'] = 'from';
$lang['to'] = 'to';
/*DAYS*/
$lang['0'] = "Mon";
$lang['1'] = "Tue";
$lang['2'] = "Wed";
$lang['3'] = "Thu";
$lang['4'] = "Fri";
$lang['5'] = "Sat";
$lang['6'] = "Sun";

$lang['tour_org'] = 'Tour organised by';

$lang['duration'] = 'Duration:';
$lang['validity'] = 'Validity:';

$lang['days'] = 'Days:';
$lang['session1'] = 'First session';
$lang['session2'] = 'Second session';
$lang['session3'] = 'Third session';

$lang['included'] = 'What\'s included';
$lang['not_incuded'] = 'What\'s excluded';

$lang['policy'] = 'Cancellation and Reservation Policy';

$lang['starting_point'] = 'Starting point';

$lang['booking'] = 'Booking';

$lang['select_date'] = "Select a date";
$lang['time']='Time';

$lang['adults']='Adults';

$lang['child']='Children';
$lang['total']='Total amount';

$lang['book_now']='Book Now';
$lang['P']='per person';
$lang['T']='total';
