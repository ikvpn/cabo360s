<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: en [english file]
 * Description:
 */

$lang['private_transfer'] = 'Private transfer';
$lang['book_transfer'] = 'Book a transfer';
$lang['select_date'] = 'Select a date';
$lang['time'] = 'Time';
$lang['adults'] = 'Adults';
$lang['children'] = 'Children';
$lang['infants'] = 'Infants';
$lang['pets'] = 'Pets';
$lang['pick_up_address'] = 'Pick Up Address';
$lang['drop_off_address'] = 'Drop Off Address';
$lang['choose_your_accommodation'] = 'Choose your Accommodation';
$lang['return'] = 'Return';
$lang['optionally'] = 'Optionally';
$lang['last_information'] = 'Last Information';
$lang['total_amount'] = 'Total amount';
$lang['total_cost'] = 'Total cost';
$lang['next_step'] = 'Next Step';
$lang['book_now'] = 'Book Now';
$lang['restart'] = 'Restart';
$lang['done'] = 'Done';
$lang['reservation_completed'] = 'Your reservation is completed';
$lang['reservation_error'] = 'Ops.. Something goes wrong please try again or contact us';
$lang['car_transfer'] = 'Car Transfer';
$lang['shuttle_transfer'] = 'Shuttle Transfer';
$lang['yacht_transfer'] = 'Yacht Transfer';
$lang['flight_train_no'] = 'Flight / Train number';
$lang['special_request'] = 'Special Request';
$lang['Confirm_Email_Address'] = 'Confirm email address';
$lang['Phone'] = 'Telephone';
$lang['User_information'] = 'User information';
