<?php

$lang['What_to_visit'] = 'What to visit';
$lang['escursions-and-visit'] = 'escursions-and-visit';
$lang['services'] = 'Escursions and Visit';
$lang['what-to-visit'] = 'what-to-visit';
$lang['where-to-stay'] = 'where-to-stay';
$lang['places-of-interest'] = 'places-of-interest';
$lang['Locations'] = 'Locations';
$lang['locations'] = 'locations';
$lang['traditional-events'] = 'traditional-events';
$lang["Places_Of_Interest"] = 'Places of Interset';
$lang['beach'] = 'beach';
$lang['beaches'] = 'beaches';
$lang['eat-and-drink'] = 'eat-and-drink';
$lang['what-to-do'] = 'activities';
$lang['Activities'] = 'Activities';
$lang['events'] = 'events';
$lang['useful-info'] = 'useful-info';
$lang['Map_of_the_Island'] = 'Map of the Island';
$lang['Where_to_stay'] = 'Where to stay';
$lang['Hotels'] = 'Hotels';
$lang['Holiday_Houses'] = 'Villas';
$lang['Special_Offers'] = 'Special Offers';
$lang['Eat_and_drink'] = 'Eat & drink';
$lang['Restaurants'] = 'Restaurants';
$lang['restaurants'] = 'restaurants';
$lang['Bars_and_Cafe'] = 'Bars and Cafe';
$lang['Nightlife'] = 'Nightlife';
$lang['What_to_do'] = 'What to do';
$lang['Events'] = 'Events';
$lang['Useful_Info'] = 'Useful Info';
$lang['Blog'] = 'Blog';
$lang['Shops'] = "Shops";
$lang['events_and_traditions'] = 'Events and Traditions';
$lang['Sights'] = 'Sights';
$lang['Beaches'] = 'Beaches';
$lang['Spa']='Spa';
$lang['Places_of_Interest'] = 'Places of Interest';
$lang['Articles'] = 'Articles';
$lang['Our-Team'] = 'Our Team';
$lang['Add-your-business'] = 'Add your business';
$lang['Prices'] = 'Prices';
$lang['About-us'] = 'About us';
$lang['settings'] = 'Settings';
$lang['signup'] = 'Signup';
$lang['cart'] = 'Cart';

$lang['about-us'] = 'About us';
$lang['how-it-works'] = 'How it works';
$lang['privacy-policy'] = 'Privacy policy';
$lang['terms-conditions'] = 'Terms and conditions';
$lang['contact-us'] = 'Contact us';

/* COMMON LABELS */
$lang['more'] = "More";
$lang['Search'] = 'Search';
$lang['View_details'] = 'View details';
$lang['More_details'] = 'More details';
$lang['Read_more'] = 'Read more';
$lang['Reviews'] = 'Reviews';
$lang['Accommodation_Types'] = 'Accommodation Types';
$lang['Check_In'] = 'Check In';
$lang['num_room'] = 'Number of rooms';
$lang['book'] = 'Book';
$lang['all_tours'] = 'All tours';

$lang['day'] = 'day';
$lang['week'] = 'week';
$lang['month'] = 'month';
$lang['year'] = 'year';
$lang['hours'] = 'Working Hours';

$lang['Price_Range'] = "Price Range";
$lang['Cuisine'] = "Cuisine";
$lang['Credit_Card'] = "Credit Card";
$lang['Closing_Day'] = "Closing Day";
$lang['Rating'] = "Rating";
$lang["Info"] = "Info";
$lang["Services"] = "Services";
$lang['Invited_People'] = "Invited People";
$lang['read_more'] = "Read more";
$lang['Send_a_request'] = 'Send a request';

/*Search Form*/
$lang['Number_of_People'] = "Guests";
$lang["Search Hotel/Apartment in Procida"] = "Search Hotel/Apartment in Procida";
$lang['details'] = "details";
$lang['book_now'] = "Book Now";

/*Weddings*/
$lang['Weddings'] = 'Weddings';

/* Boat Rentals */
$lang['Boat_Rentals'] = 'Boat Rentals';
$lang['Boat rentals'] = 'Rentals';

/* SEND EMAIL MODAL BOX */
$lang["Request_Information"] = "Request Information";
$lang["First_Name"] = "First Name";
$lang["Last_Name"] = "Last Name";
$lang["Email_Address"] = "Email Address";
$lang["Subject"] = "Subject";
$lang["Message"] = "Message";
$lang['Send'] = "Send";
$lang['General_Informations'] = "General Informations";
$lang['Reservation'] = "Reservation";
$lang['Other'] = "Other";

/*PLACES OF INTEREST*/
$lang['Ticket_Prices']='Ticket Prices';
$lang['Adults']='Adult';
$lang['Children']='Children';
$lang['Guest']='Guest';
$lang['Reservation_Required'] = 'Reservation Required';
$lang['Public_Space'] = 'Public Space';
$lang['Temporary_closed'] = 'Temporary closed';
$lang['Free'] = 'Free';
$lang['Free_Entry'] = "Free Entry";


/*BEACH CLUBS*/
$lang["beach-clubs"] = "beach-clubs";
$lang['Beach Clubs'] = "Beach Clubs";
$lang['sunbed rental'] = "sunbed rental";
$lang['parasol_rentals'] = "parasol rentals";
$lang['bar service'] = "bar service";
$lang['dining service'] = "dining service";

$lang['In the same area'] = 'In the same area';
$lang['moorings'] = "moorings";

/* NEW BOOKING SEARCH */

$lang['accomodation_types'] = "Accomodation Types";
$lang['Check-In'] = "Check-In";
$lang['Check-Out'] = "Check-Out";
$lang['Departure'] = "Departure";
$lang['close'] = "Close";


$lang["act_services"] = "escursions-and-visit";

$lang["cookie-policy"] = "This website uses cookies to ensure you get the best experience on our website";
$lang["more-info"] = "More Info";
$lang["got-it"] = "Got it!";
$lang['ferry'] = 'ferry-timetable';