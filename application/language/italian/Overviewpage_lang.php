<?php

/* 
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$lang['What_to_visit_in_Procida'] = 'Cosa visitare a Procida';
$lang['location_not_found_message'] = 'Nessun luogo di interesse da visualizzare';
$lang['Routes'] = 'Itinerari';
$lang['Routes_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';
$lang['History']='Storia';
$lang['History_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';
$lang['Activities'] = 'Attività';
$lang['Activities_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';