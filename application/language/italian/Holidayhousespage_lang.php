<?php

/*
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['Villas_in_Procida'] = 'Ville in Cabo San Lucas';
$lang['BB_in_Procida'] = 'Bed and Breakfast a Procida';
$lang['Filters'] = 'Filtri';
$lang['Rooms'] = 'Camere';
$lang['Beds'] = 'Posti letto';
$lang['Pax'] = 'Persone';
$lang['Price_range'] = 'Prezzo';
$lang['Min'] = "Min";
$lang['Max'] = "Max";
$lang['Location'] = 'Località';
$lang['night'] = "notte";
$lang['price_for'] = "prezzo per";
$lang['Apartments_Not_Found_Msg'] = 'Nessuna casa vacanze disponibile a Procida.';
$lang['BB_Not_Found_Msg'] = 'Nessun Bed and Breakfast disponibile a Procida.';
$lang['details'] = 'Dettagli';
$lang['bedrooms'] = 'camere da letto';
$lang['Topology'] = 'Tipologia';
$lang['Meters'] = 'metri';
$lang['starting_from']='a partire da';
$lang['beds'] = 'letti';
$lang['bed'] = 'letto';
$lang['rooms'] = 'camere';
$lang['room'] = 'camera';
$lang['bathroom'] = 'Bagno';
$lang['Square_feet'] = 'Piedi quadrati';
