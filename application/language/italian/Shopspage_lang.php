<?php

$lang['Shops_in_Procida'] = 'Negozi a Procida';
$lang['Location'] = 'Location';
$lang['Average_price'] = 'Prezzo medio';
$lang['rating'] = 'Rating';
$lang['details'] = 'Dettaglio';
$lang['Shops_Not_Found_Msg'] = 'Nessun negozio trovato.';
$lang['Upcoming_Events'] = 'Prossimi Eventi';
$lang['hours'] = 'Orari';
$lang['price_range'] = 'Fascia di prezzo';
$lang['credit_card'] = 'Carta di credito';
$lang['closing_day'] = 'Giorno di Chiusura';
$lang['info'] = 'Informazioni';
$lang['topology'] = 'Tipologia';
$lang['Address'] = "Indirizzo";
$lang['starting_from']='a partire da';
$lang['Filters']='Filtri';
$lang['Tags']='Tags';
$lang['Activity_services']='Servizi';
