<?php



/* 

 * Author: Innam Hunzai

 * Email: innamhunzai@gmail.com 

 * Project: Visit Procida

 * Version: 1.0

 * File: 

 * Description:

 */





$lang['Weddings_in_Procida'] = 'Matrimoni a Procida';

$lang['Weddings'] = 'Matrimoni';

$lang['Upcoming_Events'] = 'Prossimi eventi';

$lang['events_not_found_mesg'] = 'Nessun evento in programma';

$lang['Weddings_Not_Found_Msg'] = 'Nessun wedding catering trovato.';



/* WEDDING */

$lang['Catering'] = "catering";

$lang['Hiring of kitchenware'] = "noleggio attrezzature";

$lang['Linens and materials'] = "tovaglie e biancheria";

$lang['Wedding plan'] = "Wedding plan";

$lang['Waiter service'] = "camerieri";

$lang['Beverage'] = "bevande";

$lang['Music'] = "musica";

$lang['Location'] = "location";

$lang['Venue provided'] = "Luogo provvisto";

$lang['preparation'] = "allestimento";

$lang['wedding cake'] = "torta nuziale";

$lang['candy'] = "confettata";



$lang['KM0'] = "KM0";

$lang["Regional"] = "regionale";

$lang['Traditional'] = "Tradizionale";

$lang['International'] = "Internazionale";

$lang['Fusion'] = "Fusion";

$lang['Mediterranean'] = "mediterranea";

$lang['Vegetarian'] = "vegetariana";

$lang['Vegan'] = "vegana";

$lang['starting_from']='a partire da';