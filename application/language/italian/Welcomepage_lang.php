<?php

/*
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['Welcome_in_Procida'] = 'Benvenuti a Procida';
$lang['Hotel_for_Stelle'] = 'Hotel Name';
$lang['Hotel_Deals'] = 'Hotel Deals';
$lang['Holiday_Deals'] = 'Holiday Deals';

$lang['heading']='Benvenuti a Procida';
$lang['subheading']="La più piccola isola nella baia di Napoli";
$lang['tour']="Top tours";
$lang['tour_sub']="PRENOTA IL TUO TOUR ONLINE";

$lang['Footer_head']="Inserisci la tua attività su VisitProcida";

$lang['Footer_content']="Dai visibilità alla tua attività raggiungendo potenziali clienti. VisitProcida è la guida ufficiale per il turismo a Procida che mette in contatto turisti ed operatori turistici. Nel 2016 più di 100.000 utenti da tutto il mondo hanno visitato la nostra piattaforma.";

$lang['footer_line1']="Crea un account personale";
$lang['footer_line2']="Registra la tua attività inserendo foto ed info";
$lang['footer_line3']="Vai online ed inizia a ricevere prenotazioni";


$lang['Hotel_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';
$lang['Places_of_Interest_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';
$lang['Beaches_Desc'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.';

$lang['Escursion'] = 'Escursione';
$lang['Guided Visit'] = 'Visita Guidata';
$lang['days'] = 'giorni';
$lang['hours'] = 'ore';

$lang['minutes'] = 'minuti';
$lang['starting form'] = 'a partire da';
$lang['T'] = 'in totale';

$lang['P'] = 'a persona';

$lang['Top rated'] = 'I più visitati';

$lang['hotels'] = 'Hotel';
$lang['resturants'] = 'Ristorante';
$lang['shopes'] = 'Negozio';
$lang['apartments'] = 'Casa vacanze';
$lang['bars_cafe'] = 'Bar';
$lang['boat_rentals'] = 'Noleggio';
$lang['weddings'] = 'Matrimoni';
$lang['beach_clubs'] = 'Stabilimento Balneare';
$lang['nightlife'] = 'Vita Notturna';
$lang['services'] = 'Escursioni e visite guidate';

$lang['add_camera'] = 'Aggiungi Camera';
$lang['placeholder_form'] = 'Camere:1 Adulti: 2 Bambini: 0';