<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:  it [italian file]
 * Description:
 */

$lang['departures_from_procida'] = 'Partenze da Procida';
$lang['ferry_timetables'] = 'Ferry Orario';
$lang['how_to_get_there'] = 'Come Arrivare a Procida ?';
$lang['naples'] = 'Per Napoli';
$lang['pozzuoli'] = 'Per Pozzuoli';
$lang['ischia'] = 'Per Ischia';
$lang['departure'] = 'Partenza';
$lang['time'] = 'tempo';
$lang['destination'] = 'destinazione';
$lang['ship'] = 'nave';
$lang['company'] = 'azienda';
$lang['all_destination'] = 'tutte le destinazioni';
$lang['search'] = 'Cerca';
$lang['departure'] = 'Partenza';
$lang['destination'] = 'Arrivo';
$lang['duration'] = 'Durata';
$lang['boat'] = 'Nave';
$lang['type'] = 'tipo';
$lang['company'] = 'Compagnia';
$lang['today'] = 'Oggi';
$lang['book'] = 'Prenota';
$lang['price'] = 'Prezzo';
$lang['no_routes'] = 'Nessuna tratta da mostrare';