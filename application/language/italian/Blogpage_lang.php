<?php

/* 
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$lang['Blog'] = 'Blog';
$lang['Post'] = 'Post';
$lang['Upcoming_Events'] = 'Prossimi eventi';
$lang['Posts_Not_Found_Msg'] = 'Al momento non ci sono post da visualizzare.';
$lang['tags'] = 'Tag';
$lang['next_post'] = 'prossimo post';
$lang['prev_post'] = 'post precedente';
$lang['Recent_Articles'] = 'Articoli recenti';
$lang['events_not_found_mesg'] = 'Nessun evento in programma';