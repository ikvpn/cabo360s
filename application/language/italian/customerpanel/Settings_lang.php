<?php

/* 
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */
$lang['profile_details'] = "Dettagli Profilo";
$lang['enter_your_first_name'] = "Inserisci il tuo nome";
$lang['enter_your_last_name'] = "Inserisci il tuo cognome";
$lang['address_line_one'] = "Indirizzo 1";
$lang['address_line_two'] = 'Indirizzo 2';
$lang['city_name'] = "Città";
$lang['post_code'] = 'CAP';
$lang['state'] = 'Stato';
$lang['country'] = 'Paese';
$lang['phone_number'] = 'Numero Telefono';
$lang['company_name'] = 'Nome Società';

