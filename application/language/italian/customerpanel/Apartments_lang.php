<?php

/*
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['minimum_price'] = 'Prezzo Minimo';
$lang['maximum_price'] = 'Prezzo Massimo';
$lang['price_unit'] = 'Prezzo';
$lang['per_day'] = 'Per Giorno';
$lang['per_night'] = 'Per Notte';
$lang['per_week'] = 'A Settimana';
$lang['per_month'] = 'Al Mese';
$lang['per_year'] = 'Per Anno';
$lang['rooms'] = 'Camere';
$lang['pax'] = 'Persone';
$lnag['beds'] = 'Posti';
$lang['trypology'] = 'Tipologia';
$lang['bathrooms'] = 'Bagni';
$lang['terrace'] = 'Terrazzo';
$lang['categories'] = 'Categorie';
$lang['registred_holiday_houses'] = 'Registerd Ville';
