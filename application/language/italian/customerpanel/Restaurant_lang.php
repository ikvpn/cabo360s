<?php

/*
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['slogan'] = 'Slogan';
$lang['phone']='Telefono';
$lang['website']='Website';
$lang['minimum_price'] = 'Prezzo minimo';
$lang['maximum_price'] = 'Prezzo massimo';
$lang['cuisine'] = 'Cucina';
$lang['credit_cards']='Carte di Credito';
$lang['hours']='Orari';
$lang['closing_day']='Chiuso il';
$lang['info']='Info';
$lang['categories'] = 'Categorie';
$lang['address']='Address';
$lang['menu_price']='Menu Price';
$lang['invited_people']='Invited People';
