<?php

/*
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */
$lang['min_price'] = 'Min Price';
$lang['max_price'] = 'Max Price';
$lang['category'] = 'Category';
$lang['registerd_shops'] = 'Registerd Shops';
$lang['cuisine'] = 'cucina';
$lang['typology'] = "Tipologia";
$lang['categories'] = 'Categorie';
