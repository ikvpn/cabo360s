<?php


$lang['service_registered'] = 'Agenzie Registrate';
$lang['name'] = 'Nome';
$lang['locations']='Località';
$lang['status']='Stato';
$lang['actions'] = 'Azioni';

$lang['slogan'] = 'Slogan';
$lang['phone']='Telefono';
$lang['website']='Website';
$lang['minimum_price'] = 'Prezzo minimo';
$lang['maximum_price'] = 'Prezzo massimo';
$lang['tipology'] = 'Tipologia';
$lang['credit_cards']='Carte di Credito';
$lang['hours']='Orari';
$lang['closing_day']='Chiuso il';
$lang['info']='Info';
$lang['categories'] = 'Categorie';
$lang['address']='Address';
$lang['menu_price']='Menu Price';
$lang['invited_people']='Invited People';
