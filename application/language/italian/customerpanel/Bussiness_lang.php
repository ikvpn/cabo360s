<?php

/*
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['display_name'] = 'Nome visualizzato';
$lang['description'] = 'Descrizione';
$lang['sno'] = 'S.No';
$lang['name'] = 'Nome';
$lang['location'] = 'Località';
$lang['status']='Stato';
$lang['actions'] = 'Azioni';
$lang['edit'] = 'Modifica';
$lang['save'] = 'Salva';
$lang['upload_primary_image'] = 'Carica Immagine Principale';
$lang['primary_image'] = 'Immagine Principale';
$lang['upload'] = 'Carica';
$lang['bars_cafe'] = 'Bar e Caffè';
$lang['categories'] = 'Categorie';
$lang['business_details'] = 'Dettagli attività';
$lang['title'] = 'Titolo';
$lang['phone'] = 'Telefono';
$lang['address'] = 'Indirizzo';
$lang['payment_details'] = 'Dettagli pagamento';
$lang['payment_plan'] = 'Piano';
$lang['payment_option'] = 'Opzioni pagamento';
$lang['paypal'] = 'PayPal';
$lang['offline'] = 'Offline';
$lang['billing_details'] = 'Dettagli fatturazione';
$lang['email'] = 'Email';
$lang['checkout'] = 'Check Out';
