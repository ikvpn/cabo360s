<?php

$lang['dashboard'] = 'Dashboard';
$lang['customer_panel'] = 'Panello utente';
$lang['logout'] = 'Logout';
$lang['settings'] = 'Impostazioni';
$lang['register_business'] = 'Registra la tua attività';
$lang['register'] = 'Registra la';
$lang['hotels'] = 'Hotel';
$lang['resturants'] = 'Ristoranti';
$lang['shops'] = 'Negozi';
$lang['shopes'] = 'Negozi';
$lang['apartments'] = 'Ville';
$lang['events'] = 'Eventi';
$lang['save'] = 'Salva';
$lang['cancel'] = 'Cancella';
$lang['bars_cafe'] = 'Bar e Caffè';
$lang['beds'] = 'letti';
$lang['boat_rentals'] = 'Noleggio';
$lang['weddings'] = 'Matrimoni';
$lang['beach_clubs'] = "Stabilimenti balneari";
$lang['nightlife'] = "Nightlife";
$lang['services'] = "Escursioni e Visite";
$lang['tours'] = "Tours";
$lang['bed_and_breakfast'] = "Bed and Breakfast";
$lang['agencies'] = "Agenzie";
$lang['yachts'] = 'Yachtas';

/* BOAT RENTALS */
$lang['classes'] = "corsi";
$lang['rental'] = "noleggio";
$lang['excusions'] = "escursioni";
$lang['island tour'] = "giro dell'isola";
$lang['boat-rental'] = "noleggio barche";
$lang['kayak-rental'] = "noleggio kayak";
$lang['boat-excusions'] = "escursioni in barca";
$lang['moorings'] = "ormeggio";

/* WEDDING */
$lang['Catering'] = "catering";
$lang['Hiring of kitchenware'] = "noleggio attrezzature";
$lang['Linens and materials'] = "tovaglie e biancheria";
$lang['Wedding plan'] = "Wedding plan";
$lang['Waiter service'] = "camerieri";
$lang['Beverage'] = "bevande";
$lang['Music'] = "musica";
$lang['Location'] = "location";
$lang['Venue provided'] = "Luogo provvisto";
$lang['preparation'] = "allestimento";
$lang['wedding cake'] = "torta nuziale";
$lang['candy'] = "confettata";

$lang['KM0'] = "KM0";
$lang['Traditional'] = "Tradizionale";
$lang["Regional"] = "regionale";
$lang['International'] = "Internazionale";
$lang['Fusion'] = "Fusion";
$lang['Mediterranean'] = "mediterranea";
$lang['Vegetarian'] = "vegetariana";
$lang['Vegan'] = "vegana";

/*BEACH CLUBS*/
$lang['sunbed rental'] = "noelggio lettini";
$lang['parasol rentals'] = "noleggio ombrelloni";
$lang['bar service'] = "servizio bar";
$lang['dining service'] = "servizio di ristorazione";

/*DAYS*/
$lang['0'] = "Lunedì";
$lang['1'] = "Martedì";
$lang['2'] = "Mercoledì";
$lang['3'] = "Giovedì";
$lang['4'] = "Venerdì";
$lang['5'] = "Sabato";
$lang['6'] = "Domenica";
