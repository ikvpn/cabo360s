<?php


$lang['invoice'] = ' Fatturazione';
$lang['invoice_details'] = 'Dettagli fatturazione per';
$lang['page_info'] = 'Informazioni';
$lang['title'] = 'Titolo';
$lang['category'] = 'Categoria';
$lang['location'] = 'Località';
$lang['reg_date'] = 'Data di registrazione';
$lang['cust_info'] = 'Informazioni Cliente';
$lang['cust_name'] = 'Nome';
$lang['cust_phone'] = 'Telefono';
$lang['cust_company'] = 'Azienda';
$lang['paym_type'] = 'Tipo di pagamento';
$lang['paym_plan'] = 'Piano di pagamento';
$lang['paym_details'] = 'Dettagli pagamento';
$lang['paym_amount'] = 'Costo Concordato';
$lang['paym_status'] = 'Stato';
$lang['monthly'] = 'Mensile';
$lang['yearly'] = 'Annuale';
$lang['billing_details'] = 'Dettagli di fatturazione';
$lang['billing_address'] = 'Indirizzo di fatturazione';
$lang['billing_phone'] = 'Telefono di fatturazione';
