<?php

/*
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['price'] = 'Prezzo';
$lang['categories'] = 'Categorie';
$lang['price_unit'] = 'Prezzo unità';
$lang['per_day'] = 'Per Giorno';
$lang['per_night'] = 'Per Notte';
$lang['per_week'] = 'A Settimana';
$lang['per_month'] = 'Al Mese';
$lang['per_year'] = 'Per Anno';
