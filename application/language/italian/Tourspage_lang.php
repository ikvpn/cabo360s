<?php

$lang['all'] = 'Tutti';
$lang['tours'] = 'Tours';
$lang['escursion'] = 'Escursioni';
$lang['visit'] = 'Esperienze';
$lang['filter'] = 'Filtra per';
$lang['tours_title'] = 'Tour ed Escursioni';
$lang['from'] = 'dal';
$lang['to'] = 'al';

/*DAYS*/
$lang['0'] = "Lun";
$lang['1'] = "Mar";
$lang['2'] = "Mer";
$lang['3'] = "Giov";
$lang['4'] = "Ven";
$lang['5'] = "Sab";
$lang['6'] = "Dom";

$lang['tour_org'] = 'Tour organizzato da';
$lang['duration'] = 'Durata:';

$lang['validity'] = 'Validità:';
$lang['days'] = 'Giorni:';

$lang['session1'] = 'Prima sessione';
$lang['session2'] = 'Seconda sessione';
$lang['session3'] = 'Terza sessione';

$lang['information'] = 'Informazioni';
$lang['included'] = 'Cosa è incluso';
$lang['not_incuded'] = 'Cosa è escluso';
$lang['policy'] = 'Policy di prenotazione e cancellazione';
$lang['starting_point'] = 'Luogo di incontro';
$lang['booking'] = 'Prenota';
$lang['select_date'] = "Seleziona una data";
$lang['time'] = 'Orario';
$lang['adults']='Adulti';
$lang['child']='Bambini';
$lang['total']='Costo totale';
$lang['book_now']='Prenota Ora';
$lang['P']='a persona';
$lang['T']='totale';
