<?php

/* 
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$lang['Find_Hotels_in_Procida'] = 'Trova Hotels a Procida';
$lang['search_check_box_msg'] = 'Non ho ancora deciso le date';
$lang['Ticket_price'] = 'Ingresso';
$lang['Average_price'] = 'Prezzo medio';
$lang['Near_by_areas'] = 'Nelle vicinanze';