<?php



/*

 * Author: Innam Hunzai

 * Email: innamhunzai@gmail.com

 * Project: Visit Procida

 * Version: 1.0

 * File:

 * Description:

 */





$lang['Boat_Rentals_in_Procida'] = 'Noleggi a Procida';

$lang['Upcoming_Events'] = 'Prossimi eventi';

$lang['events_not_found_mesg'] = 'Nessun evento in programma';

$lang['Boat_Rental_Not_Found_Msg'] = 'Nessuno noleggio barche trovato.';

$lang['Boat_Rentals'] = 'Noleggio Barche';



$lang['classes'] = "corsi";

$lang['rental'] = "noleggio";

$lang['excusions'] = "escursioni";

$lang['island tour'] = "giro dell'isola";

$lang['boat-rental'] = "noleggio barche";

$lang['kayak-rental'] = "noleggio kayak";

$lang['boat-excusions'] = "escursioni in barca";

$lang['moorings'] = "ormeggio";

$lang['starting_from']='a partire da';
