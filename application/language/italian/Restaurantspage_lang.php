<?php

/*
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

$lang['Restaurants_in_Procida'] = 'Ristoranti a Procida';
$lang['Resturant_Not_Found_Msg'] = 'Nessun ristorante trovato a Procida.';
$lang['Nightlife_Not_Found_Msg'] = 'Nessun nightlife trovato a Procida.';
$lang['Upcoming_Events'] = 'Prossimi Eventi';
$lang['events_not_found_mesg'] = 'Nessun evento in programma';
$lang['Restaurants'] = 'Ristoranti';
$lang['starting_from']='a partire da';
$lang['contact_from']='Contatta';
$lang['Filters']='Filtri';
$lang['Tags']='Tags';
$lang['Activity_services']='Servizi';
