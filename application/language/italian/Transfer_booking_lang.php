<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File:  it [italian file]
 * Description:
 */

$lang['private_transfer'] = 'Transfer privati';
$lang['book_transfer'] = 'Prenota un transfer';
$lang['select_date'] = 'Data';
$lang['time'] = 'Ora';
$lang['adults'] = 'Adulti';
$lang['children'] = 'Bambini';
$lang['infants'] = 'Neonati';
$lang['pets'] = 'Animali';
$lang['pick_up_address'] = 'Indirizzo partenza';
$lang['drop_off_address'] = 'Indirizzo arrivo';
$lang['choose_your_accommodation'] = 'Scegli il tuo alloggio';
$lang['return'] = 'Ritorno';
$lang['optionally'] = 'Opzionale';
$lang['last_information'] = 'Ultime informazioni';
$lang['total_amount'] = 'Prezzo totale';
$lang['total_cost'] = 'Costo totale';
$lang['next_step'] = 'Passaggio successivo';
$lang['book_now'] = 'Prenota ora';
$lang['restart'] = 'Prenota ancora';
$lang['done'] = ' ';
$lang['reservation_completed'] = 'La tua prenotazione è stata effettuata con successo';
$lang['reservation_error'] = 'Ops.. qualcosa è andato storto per favore contatta l\'amministrazione';
$lang['car_transfer'] = 'Trasferimento in auto';
$lang['shuttle_transfer'] = 'Trasferimento in navetta';
$lang['yacht_transfer'] = 'Trasferimento in yacht';
$lang['flight_train_no'] = 'Numero di volo / treno';
$lang['special_request'] = 'Richiesta speciale';
$lang['Confirm_Email_Address'] = "Conferma l'indirizzo e-mail";
$lang['Phone'] = 'Telefono';
$lang['User_information'] = 'Informazioni utente';