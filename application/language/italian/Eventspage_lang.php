<?php

/* 
 * Author: Aslam Shah
 * Email: hunzaboy@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

$lang['Traditional_Events'] = 'Eventi tradizionali';
$lang['Events_by_Categories'] = 'Eventi per categorie';
$lang['Music'] = 'Musica';
$lang['Prccession'] = 'Prccessioni';
$lang['Festivals'] = 'Festivals';
$lang['Info'] = 'Info';
$lang['Prices'] = 'Prezzi';
$lang['Venue_Details'] = 'Dettagli location';
$lang['Events_by_Categories'] = 'Eventi per categorie';
$lang['Adults'] = 'Adulti';
$lang['Childs'] = 'Bambini';
$lang['Guests'] = 'Opsiti';
$lang['Send_a_request'] = 'Invia richiesta';
$lang['Upcoming_Events'] = 'Prossimi Eventi';
$lang['Event'] = 'Eventi';
$lang['Location'] = 'Location';
$lang['Time'] = 'Ora';
$lang['Date'] = 'Data';
$lang['Days'] = 'Giorni';
$lang['Event_Not_Found_Msg'] = 'Nessun evento da visulaizzare.';
$lang['Phone'] = 'Telefono';
$lang['Website'] = 'Sitoweb';
$lang['Mobile'] = 'Mobile';
$lang['Address'] = 'Indirizzo';

$lang['January'] = 'Gennaio';
$lang['February'] = 'Febbraio';
$lang['March'] = 'Marzo';
$lang['April'] = 'Aprile';
$lang['May'] = 'Maggio';
$lang['June'] = 'Giungo';
$lang['July'] = 'Luglio';
$lang['August'] = 'Agosto';
$lang['September'] = 'Septembre';
$lang['October'] = 'Octobre';
$lang['November'] = 'Novembre';
$lang['December'] = 'Dicembre';

$lang['Monday'] = 'Lunedì';
$lang['Tuesday'] = 'Martedì';
$lang['Wednesday'] = 'Mercoledì';
$lang['Thrusday'] = 'Thrusday';
$lang['Friday'] = 'Venerdì';
$lang['Saturday'] = 'Sabato';
$lang['Sunday'] = 'Domenica';