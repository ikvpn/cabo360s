<?php

$lang['What_to_visit'] = 'Cosa visitare';
$lang['what-to-visit'] = 'cosa-visitare';
$lang['escursions-and-visit'] = 'escursioni-e-visite';
$lang['services'] = 'Escursioni e Visite';

$lang['where-to-stay'] = 'dove-dormire';
$lang['places-of-interest'] = 'luoghi-di-interesse';
$lang['Locations'] = 'Località';
$lang['locations'] = 'localita';
$lang['Places_Of_Interest'] = 'Luoghi di interesse';
$lang['traditional-events'] = 'eventi-tradizionali';
$lang['beach'] = 'spiagge';
$lang['beaches'] = 'spiagge';
$lang['eat-and-drink'] = 'mangiare-e-bere';
$lang['what-to-do'] = 'cosa-fare';
$lang['Activities'] = 'cosa fare';
$lang['events'] = 'eventi';
$lang['restaurants'] = 'ristoranti';
$lang['useful-info'] = 'info-utili';
$lang['Map_of_the_Island'] = 'Mappa dell\'isola';
$lang['Where_to_stay'] = 'Dove dormire';
$lang['Hotels'] = 'Hotels';
$lang['Holiday_Houses'] = 'Case vacanza';
$lang['Special_Offers'] = 'Offerte speciali';
$lang['Eat_and_drink'] = 'Bere e mangiare';
$lang['Restaurants'] = 'Ristoranti';
$lang['Bars_and_Cafe'] = 'Bars e Cafe';
$lang['Nightlife'] = 'Nightlife';
$lang['What_to_do'] = 'Cosa Fare';
$lang['Events'] = 'Eventi';
$lang['Useful_Info'] = 'Info Utili';
$lang['Blog'] = 'Blog';
$lang['Shops'] = "Negozi";
$lang['events_and_traditions'] = 'Eventi e Tradizioni';
$lang['Sights'] = 'Luoghi da visitare';
$lang['Beaches'] = 'Spiagge';
$lang['Spa']='Spa';
$lang['Places_of_Interest'] = 'Luoghi di interesse';
$lang['Articles'] = 'articoli';
$lang['Our-Team'] = 'La nostra squadra';
$lang['Add-your-business'] = 'Aggiungi la tua attività';
$lang['Prices'] = 'Prezzi';
$lang['About-us'] = 'Riguardo a noi';

$lang['about-us'] = 'Chi siamo';
$lang['how-it-works'] = 'Come funziona';
$lang['privacy-policy'] = 'Privacy policy';
$lang['terms-conditions'] = 'Termini e condizioni';
$lang['contact-us'] = 'Contattaci';

$lang['settings'] = 'Impostazioni';
$lang['signup'] = 'Registrati';
$lang['cart'] = 'Carrello';
$lang['go_cart'] = 'Vai al carrello';
$lang['num_room'] = 'Numero di camere';

$lang['book'] = 'Prenota';
$lang['all_tours'] = 'Tutti i tour';

/* COMMON LABELS */
$lang['more'] = "Più";
$lang['Search'] = 'Cerca';
$lang['View_details'] = 'Dettaglio';
$lang['More_details'] = 'Maggiori dettagli';
$lang['Read_more'] = 'Leggi';
$lang['Reviews'] = 'Recensioni';
$lang['Accommodation_Types'] = 'Tipologia alloggio';
$lang['Check_In'] = 'Check In';

$lang['day'] = 'giorno';
$lang['week'] = 'settimana';
$lang['month'] = 'mese';
$lang['year'] = 'anno';
$lang['hours'] = "Orari";

$lang['Price_Range'] = "Prezzo medio";
$lang['Cuisine'] = "Cucina";
$lang['Credit_Card'] = "Metodi di pagamento";
$lang['Closing_Day'] = "Giorno di chiusura";
$lang['Rating'] = "Valutazione";
$lang['Info'] = "Informazioni";
$lang["Services"] = "Servizi";
$lang["Invited_People"] = "persone invitate";
$lang['read_more'] = "Leggi di più";
$lang['Send_a_request'] = 'Invia richiesta';

/* SERACH FORM FRONT END */
$lang['Number_of_People'] = "Ospiti";
$lang["Search Hotel/Apartment in Procida"] = "Ricerca hotel / appartamento a Procida";
$lang['details'] = "Dettagli";
$lang['book_now'] = "Prenota ora";

/*WEDDINGS*/
$lang['Weddings'] = 'Matrimoni';

/* Boat Rentals */
$lang['Boat_Rentals'] = 'Noleggio';
$lang['noleggio barche'] = 'Noleggio';

/* SEND EMAIL MODAL BOX */
$lang["Request_Information"] = "Richiesta informazioni";
$lang["First_Name"] = "Nome";
$lang["Last_Name"] = "Cognome";
$lang["Email_Address"] = "Indirizzo Email";
$lang["Subject"] = "Oggetto";
$lang["Message"] = "Richiesta";
$lang['Send'] = "Invia";
$lang['General_Informations'] = "Informazioni Generali";
$lang['Reservation'] = "Prenotazioni";
$lang['Other'] = "Altro";

/*PLACES OF INTEREST*/
$lang['Ticket_Prices']='Prezzo ingresso';
$lang['Adults']='Adulti';
$lang['Children']='Bambini';
$lang['Guest']='Gruppi';
$lang['Reservation_Required'] = 'Su prenotazione';
$lang['Public_Space'] = 'Spazio pubblico';
$lang['Temporary_closed'] = 'temporaneamente chiuso';
$lang['Free'] = 'Gratuito';
$lang['Free_Entry'] = "Ingresso gratuito";


/*BEACH CLUBS*/
$lang["beach-clubs"] = "stabilimenti-balneari";
$lang['Beach Clubs'] = "stabilimenti balneari";
$lang['sunbed rental'] = "noleggio lettini";
$lang['parasol_rentals'] = "noleggio ombrelloni";
$lang['bar service'] = "servizio bar";
$lang['dining service'] = "servizio di ristorazione";

$lang['In the same area'] = 'nella stessa area';
$lang['moorings'] = "ormeggi";
$lang['starting_from']='a partire da';

/* NEW BOOKING SEARCH */

$lang['accomodation_types'] = "Tipo di alloggio";
$lang['Check-In'] = "Arrivo";
$lang['Check-Out'] = "Partenza";
$lang['Departure'] = "Partenza";
$lang['close'] = "Chiudi";

$lang["act_services"] = "escursioni-e-visite";


$lang["cookie-policy"] = "Questo sito web utilizza i cookie per assicurarsi di ottenere l'esperienza migliore sul nostro sito web";
$lang["more-info"] = "Scopri di più";
$lang["got-it"] = "OK!";
$lang['ferry'] = 'orari-traghetti';