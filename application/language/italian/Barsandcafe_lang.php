<?php


$lang['Bars_and_cafe_in_Procida'] = 'Bars e Cafe in Procida';
$lang['Bars_and_Cafe'] = 'Bars e Cafe';
$lang['Upcoming_Events'] = 'Prossimi eventi';
$lang['events_not_found_mesg'] = 'Nessun evento in programma';
$lang['Bars_and_Cafe_Not_Found_Msg'] = 'Nessun Bar o Cafè trovato a Procida.';
$lang['starting_from']='a partire da';