<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Menu_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getMenuItems($lang = 'en') {
        $res = $this->db->query("SELECT m.*,tm.name,ent.name as enName FROM menu_items as m LEFT OUTER JOIN menu_items_t tm ON(m.id = tm.menu_id AND lang='" . $lang . "') LEFT OUTER JOIN menu_items_t ent ON(m.id=ent.menu_id AND ent.lang='en') WHERE 1")->result();
        return $res;
    }
    
   
}
