<?php

if (!defined('BASEPATH')) {
    exit('No direct script access is allowed');
}

class Service_model extends CI_Model {

    public $id;
    public $enName;
    public $duName;
    public $itName;
	  public $enInsta;
    public $enSlogan;
    public $duSlogan;
    public $itSlogan;
    public $enDescription;
    public $duDescription;
    public $itDescription;
    public $phone;
    public $manager;
    public $manager_profile;
    public $website;
    public $min_price;
    public $max_price;
    public $tipology;
    public $creditCards;
    public $rating;
    public $hours;
    public $closingDay;
    public $hours2;
    public $Info;
    public $photo;
    public $order_id;
    public $cDate;
    public $lat;
    public $lng;
    public $address;
    public $categories;
    public $fbWidget;
    public $tags;
    public $activity_services;

    public function __construct() {
        parent::__construct();
    }

    public function load($id) {

        $row = $this->db->select('*')
                ->from('services')
                ->where(array('id' => $id))
                ->get()
                ->row();
        if ($row) {
            $this->id = $row->id;
            $this->enName = $row->enName;
            $this->duName = $row->duName;
            $this->itName = $row->itName;
            $this->enSlogan = $row->enSlogan;
            $this->duSlogan = $row->duSlogan;
            $this->itSlogan = $row->itSlogan;
			      $this->enInsta = $row->enInsta;
            $this->enDescription = base64_decode($row->enDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->phone = $row->phone;
            $this->manager = $row->manager;
            $this->manager_profile = $row->manager_profile;
            $this->website = $row->website;
            $this->min_price = $row->min_price;
            $this->max_price = $row->max_price;
            $this->tipology = $row->tipology;
            $this->creditCards = $row->creditCards;
            $this->rating = $row->rating;
            $this->hours = $row->hours;
            $this->hours2 = $row->hours2;
            $this->closingDay = $row->closingDay;
            $this->Info = $row->Info;
            $this->photo = $row->photo;
            $this->order_id = $row->order_id;
            $this->cDate = $row->cDate;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->address = $row->address;
            $this->categories = $row->categories;
            $this->fbWidget = $row->fbWidget;
            $this->tags = $row->tags;
            $this->activity_services = $row->activity_services;
        } else {
            $this->id = 0;
            $this->enName = $this->lat = $this->lng = $this->address = $this->duName = $this->itName = $this->enSlogan = $this->duSlogan = $this->itSlogan= $this->enInsta = $this->categories = $this->hours2 = NULL;
            $this->enDescription = $this->duDescription = $this->itDescription = $this->phone = $this->manager=$this->manager_profile= $this->website = $this->min_price = $this->max_price = NULL;
            $this->tipology = $this->creditCards = $this->rating = $this->hours = $this->closingDay = $this->Info = $this->photo = $this->order_id = $this->cDate = $this->fbWidget = $this->tags = $this->activity_services = NULL;
        }
    }

    public function loadOnOrderId($id) {

        $row = $this->db->select('*')
                ->from('services')
                ->where(array('order_id' => $id))
                ->get()
                ->row();

        if ($row) {
            $this->id = $row->id;
            $this->enName = $row->enName;
            $this->duName = $row->duName;
            $this->itName = $row->itName;
            $this->enSlogan = $row->enSlogan;
            $this->duSlogan = $row->duSlogan;
            $this->itSlogan = $row->itSlogan;
			      $this->enInsta = $row->enInsta;
            $this->enDescription = base64_decode($row->enDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->phone = $row->phone;
            $this->manager = $row->manager;
            $this->manager_profile = $row->manager_profile;
            $this->website = $row->website;
            $this->min_price = $row->min_price;
            $this->max_price = $row->max_price;
            $this->tipology = $row->tipology;
            $this->creditCards = $row->creditCards;
            $this->rating = $row->rating;
            $this->hours = $row->hours;
            $this->hours2 = $row->hours2;
            $this->closingDay = $row->closingDay;
            $this->Info = $row->Info;
            $this->photo = $row->photo;
            $this->order_id = $row->order_id;
            $this->cDate = $row->cDate;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->address = $row->address;
            $this->categories = $row->categories;
            $this->fbWidget = $row->fbWidget;
            $this->tags = $row->tags;
            $this->activity_services = $row->activity_services;
        } else {
            $this->id = $this->lat = $this->lng = $this->address = $this->enName = $this->duName = $this->itName = $this->enSlogan = $this->duSlogan = $this->enInsta = $this->categories = $this->hours2 = NULL;
            $this->itSlogan = $this->enDescription = $this->duDescription = $this->itDescription = $this->phone =$this->manager=$this->manager_profile= $this->website = $this->min_price = NULL;
            $this->max_price = $this->tipology = $this->creditCards = $this->rating = $this->hours = $this->closingDay = $this->info = $this->photo = $this->order_id = $this->cDate = $this->fbWidget = $this->tags = $this->activity_services = NULL;
        }
    }

    public function update() {
        if ($this->id == 0) {
            //insert
            $service = array('enName' => $this->enName,
                'duName' => $this->duName,
                'itName' => $this->itName,
                'enSlogan' => $this->enSlogan,
                'duSlogan' => $this->duSlogan,
                'itSlogan' => $this->itSlogan,
                'enInsta' => $this->enInsta,
                'enDescription' => base64_encode($this->enDescription),
                'duDescription' => base64_encode($this->duDescription),
                'itDescription' => base64_encode($this->itDescription),
                'phone' => $this->phone,
				        'manager' => $this->manager,
				        'manager_profile' => $this->manager_profile,
                'website' => $this->website,
                'min_price' => $this->min_price,
                'max_price' => $this->max_price,
                'tipology' => $this->tipology,
                'creditCards' => $this->creditCards,
                'rating' => $this->rating?$this->rating:0,
                'hours' => $this->hours,
                'hours2' => $this->hours2,
                'closingDay' => $this->closingDay,
                'Info' => $this->Info,
                'photo' => '',
                'order_id' => $this->order_id,
                'lat'=> $this->lat,
                'lng'=>$this->lng,
                'address'=>$this->address,
                'categories'=>$this->categories,
                'fbWidget'=>$this->fbWidget,
                'tags'=>$this->tags,
                'activity_services'=>$this->activity_services
            );
            $this->db->insert('services', $service);
            return TRUE;
        } else {
            //update
            $service = array('enName' => $this->enName,
                'duName' => $this->duName,
                'itName' => $this->itName,
                'enSlogan' => $this->enSlogan,
                'duSlogan' => $this->duSlogan,
                'itSlogan' => $this->itSlogan,
                'enInsta' => $this->enInsta,
                'enDescription' => base64_encode($this->enDescription),
                'duDescription' => base64_encode($this->duDescription),
                'itDescription' => base64_encode($this->itDescription),
                'phone' => $this->phone,
				        'manager' => $this->manager,
				        'manager_profile' => $this->manager_profile,
                'website' => $this->website,
                'min_price' => $this->min_price,
                'max_price' => $this->max_price,
                'tipology' => $this->tipology,
                'creditCards' => $this->creditCards,
                'rating' => $this->rating,
                'hours' => $this->hours,
                'hours2' => $this->hours2,
                'closingDay' => $this->closingDay,
                'Info' => $this->Info,
                'order_id' => $this->order_id,
                'lat'=> $this->lat,
                'lng'=>$this->lng,
                'address'=>$this->address,
                'categories'=>$this->categories,
                'fbWidget'=>$this->fbWidget,
                'tags'=>$this->tags,
                'activity_services'=>$this->activity_services
            );
            $this->db->where(array('id' => $this->id));
            $this->db->update('services', $service);

			      return TRUE;
        }
    }

    public function update_photo($photo) {
        $this->db->where(array('id' => $this->id));
        $this->db->update('services', Array('photo' => $photo));
        return true;
    }

    public function getActiveServices($lang) {
        $res = $this->db->select('r.*, o.title as pageTitle, lt.title as locationName')
                ->from('services AS r')
                ->join('orders AS o', 'r.order_id=o.id', 'LEFT OUTER')
                ->join('locations_t AS lt', 'lt.location_id=o.location AND lang="' . $lang . '"', 'LEFT OUTER')
                ->where(array('o.status' => 'paid'))
                ->get()
                ->result();
        return $res;
    }

    public function totalActiveServices(){
        $res = $this->db->select('r.id')
                ->from('services AS r')
                ->join('orders AS o', 'r.order_id=o.id', 'LEFT OUTER')
                ->where(array('o.status' => 'paid'))
                ->get();
        return $res->num_rows();
    }

    public function loadActivePage($order_title){
        $row = $this->db->select('id')->from('orders')->where(array('title'=>$order_title))->get()->row();
        if($row){
            $this->loadOnOrderId($row->id);
        }
    }

    public function getLocationName($lang = "en", $id){

        $res = $this->db->query("SELECT lt.title FROM locations_t as lt LEFT OUTER JOIN orders o ON (o.location=lt.location_id) WHERE lang='".$lang."' AND o.id='".$id."'")->row();

        if($res){

            return $res->title;
        }else{
            return " ";
        }
    }
}
