<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */
if (!defined('BASEPATH')) {
    exit('No direct script access is allowed');
}

class Places_of_interest_model extends CI_Model {
    public $id;
    public $enTitle;
    public $itTitle;
    public $duTitle;
    public $meta_tag_en;
    public $meta_tag_du;
    public $meta_tag_it;
    public $meta_desc_en;
    public $meta_desc_du;
    public $meta_desc_it;
    public $meta_keywords_en;
    public $meta_keywords_du;
    public $meta_keywords_it;
    public $rewrite_url_en;
    public $rewrite_url_du;
    public $rewrite_url_it;
    public $lat;
    public $lng;
    public $enDescription;
    public $itDescription;
    public $duDescription;
    public $location_id;
    public $address;
    public $web;
    public $email;
    public $phone;
    public $mobile;
    public $timing;
    public $openingDays;
    public $closingDays;
    public $reservation;
    public $pAdults;
    public $pGuests;
    public $pChilds;
    public $photo;
    public $status;

    public function __construct() {
        parent::__construct();
    }

    public function load($id = 0) {
        if ($id != 0) {
            $row = $this->db->select('*')->from('placesofinterest')->where(array('id' => $id))->get()->row();
            if ($row) {
                $this->id = $row->id;
                $this->enTitle = $row->enTitle;
                $this->itTitle = $row->itTitle;
                $this->duTitle = $row->duTitle;
                $this->meta_tag_en = $row->meta_tag_en;
                $this->meta_tag_du = $row->meta_tag_du;
                $this->meta_tag_it = $row->meta_tag_it;
                $this->meta_desc_en = $row->meta_desc_en;
                $this->meta_desc_du = $row->meta_desc_du;
                $this->meta_desc_it = $row->meta_desc_it;
                $this->meta_keywords_en = $row->meta_keywords_en;
                $this->meta_keywords_du = $row->meta_keywords_du;
                $this->meta_keywords_it = $row->meta_keywords_it;
                $this->rewrite_url_en = $row->rewrite_url_en;
                $this->rewrite_url_du = $row->rewrite_url_du;
                $this->rewrite_url_it = $row->rewrite_url_it;
                $this->lat = $row->lat;
                $this->lng = $row->lng;
                $this->enDescription = $row->enDescription;
                $this->itDescription = $row->itDescription;
                $this->duDescription = $row->duDescription;
                $this->location_id = $row->location_id;
                $this->address = $row->address;
                $this->web = $row->web;
                $this->email = $row->email;
                $this->phone = $row->phone;
                $this->mobile = $row->mobile;
                $this->timing = $row->timing;
                $this->openingDays = $row->openingDays;
                $this->closingDays = $row->closingDays;
                $this->reservation = $row->reservation;
                $this->pAdults = $row->pAdults;
                $this->pGuests = $row->pGuests;
                $this->pChilds = $row->pChilds;
                $this->photo = $row->photo;
                $this->status = $row->status;
            } else {
                $this->id = $this->enTitle = $this->itTitle = $this->duTitle = $this->enDescription = $this->itDescription = $this->duDescription = $this->location_id = $this->address = $this->web = $this->email = $this->phone = $this->mobile = $this->timing = $this->openingDays = $this->closingDays = $this->reservation = $this->pAdults = $this->pGuests = $this->pChilds = $this->photo = $this->status = 0;
            }
        } else {
            $this->id = $this->enTitle = $this->itTitle = $this->duTitle = $this->enDescription = $this->itDescription = $this->duDescription = $this->location_id = $this->address = $this->web = $this->email = $this->phone = $this->mobile = $this->timing = $this->openingDays = $this->closingDays = $this->reservation = $this->pAdults = $this->pGuests = $this->pChilds = $this->photo = $this->status = 0;
        }
    }
   
    public function save() {
        if ($this->id == 0) {
            $this->db->insert('placesofinterest', $this);
            $this->load($this->db->insert_id());
            return 'New place is inserted successfully';
        } else {
            $this->db->where(array('id' => $this->id));
            $this->db->update('placesofinterest', $this);
             return 'Place information is updated successfully';
        }
    }

    public function delete() {
        $this->db->where(array('id' => $this->id));
        $this->db->delete('placesofinterest');
        return true;
    }

    public function fetch($condition = '') {
        if ($condition == '') {
            $result = $this->db->select("*")->from('placesofinterest')->get()->result();
            return $result;
        } else {
            $result = $this->db->select("*")->from('placesofinterest')->where($condition)->get()->result();
            return $result;
        }
    }
    
    public function fetch_single($condition = '',$rewrite_cond=''){
         if ($condition == '') {
            $result = $this->db->select("*")->from('placesofinterest')->get()->row();
            return $result;
        } else {
            $result = $this->db->select("*")->from('placesofinterest')->where($condition)->get()->row();
            if(!$result){
                $result = $this->db->select("*")->from('placesofinterest')->where($rewrite_cond)->get()->row();
                return $result;
            }
            return $result;
        }
    }
    
    public function getMenuList() {
        $res = $this->db->query("SELECT * FROM placesofinterest")->result();
        if ($res) {
            return $res;
        } else {
            return FALSE;
        }
    }
}
