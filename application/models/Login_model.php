<?php
class login_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	//----------login
	public function get_login_users($slug = FALSE)
	{
	if ($slug === FALSE)
	{
		$query = $this->db->get('login_users');
		return $query->result_array();
	}

	$query = $this->db->get_where('login_users', array('name' => $slug));
	return $query->row_array();
	}
	//==============================
	
}