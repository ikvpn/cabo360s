<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script accesss is allowed');
}

class Categories extends CI_Model {

    public $id;
    public $name;
    public $type;
    public $icon;
    public $y_charges;
    public $m_charges;
    public $payment_plans;
    public $cDate;
    public $table;

    public function __construct() {
        parent::__construct();
        $this->id = $this->name = $this->type = $this->icon = $this->y_charges = $this->m_charges = $this->payment_plans = $this->cDate = null;
    }

    public function load($id) {
        $res = $this->db->select("*")
                        ->from("categories")
                        ->where(array('id' => $id))->get()->row();
        if ($res) {
            $this->id = $res->id;
            $this->name = $res->name;
            $this->type = $res->type;
            $this->icon = $res->icon;
            $this->y_charges = $res->y_charges;
            $this->m_charges = $res->m_charges;
            $this->payment_plans = $res->payment_plans;
            $this->cDate = $res->cDate;
            $this->table = $res->table;
        }
    }


    public function get($id){
        $res = $this->db->select('*')
                ->from('categories')
                ->where('id', $id)
                ->get()
                ->row();
        //echo "<pre>" ; print_r($res); die;
        return($res);
    }

    public function loadOnTableName($name){
         $res = $this->db->select("*")
                        ->from("categories")
                        ->where(array('table' => $name))->get()->row();
        if ($res) {
            $this->id = $res->id;
            $this->name = $res->name;
            $this->type = $res->type;
            $this->icon = $res->icon;
            $this->y_charges = $res->y_charges;
            $this->m_charges = $res->m_charges;
            $this->payment_plans = $res->payment_plans;
            $this->cDate = $res->cDate;
            $this->table = $res->table;
        }
    }
    public function getAllList() {
        $res = $this->db->select("*")
                        ->from("categories")
                        ->get()->result();
        return $res;
    }
    public function update($id,$data){
        $this->db->where(array('id'=>$id));
        $this->db->update('categories',$data);
    }

    public function getActivityByCategory($id, $lang){
      $res = array();

      $allCategories = ['apartments', 'bars_cafe', 'beach_clubs', 'boat_rentals', 'hotels', 'nightlife', 'resturants', 'shopes', 'weddings', 'services', 'agencies'];

      $currentCategory = $this->db->select("*")->from("categories")->where(array('id' => $id))->get()->row();

      if (($key = array_search($currentCategory->table, $allCategories)) !== false) {
           array_splice($allCategories, $key, 1);
      }

      $allActivity = array();

      foreach ($allCategories as $key => $categoryTable) {
        # code...
        $elements = $this->db->select('e.*, lt.title as locationName, o.title as pageTitle, o.category_id as categoryID, c.table as categoryName, c.type as urlType')
                ->from($categoryTable.' AS e')
                ->join('orders AS o','e.order_id=o.id','LEFT OUTER')
                ->join('categories AS c','o.category_id=c.id','LEFT OUTER')
                ->join('locations_t AS lt','lt.location_id=o.location AND lang="'.$lang.'"','LEFT OUTER')
                ->where(array('o.status'=>'paid'))
                ->get()
                ->result();

        foreach ($elements as $value) {
          $categories = explode(',', $value->categories);
          if(in_array($id,$categories)) {
            array_push($allActivity, $value );
          }
        }
      }
      return $allActivity;
    }
}
