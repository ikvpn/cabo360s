<?php


class Customer_model extends CI_Model {

    public $id;
    public $firstName;
    public $lastName;
    public $email;
    public $billingAddress1;
    public $billingAddress2;
    public $billingCity;
    public $billingPostcode;
    public $billingState;
    public $billingCountry;
    public $billingPhone;
    public $company;
    public $status;
    public $cDate;

    public function __construct() {
        parent::__construct();
    }

    public function load($id) {
        $res = $this->db->select('*')->from('customers')->where(array('id' => $id))->get()->row();
        // var_dump($res);exit();
        if ($res) {
            $this->id = $res->id;
            $this->firstName = $res->firstName;
            $this->lastName = $res->lastName;
            $this->email = $res->email;
            $this->billingAddress1 = $res->billingAddress1;
            $this->billingAddress2 = $res->billingAddress2;
            $this->billingCity = $res->billingCity;
            $this->billingPostcode = $res->billingPostcode;
            $this->billingState = $res->billingState;
            $this->billingCountry = $res->billingCountry;
            $this->billingPhone = $res->billingPhone;
            $this->company = $res->company;
            $this->status = $res->status;
            $this->cDate = $res->cDate;
        } else {
            $this->id = $this->firstName = $this->lastName = $this->email = $this->billingAddress1 = $this->billingAddress2 = $this->billingCity = $this->billingPostcode = NULL;
            $this->billingState = $this->billingCountry = $this->billingPhone = $this->company = $this->status = $this->cDate = NULL;
        }
    }

    public function signup() {
        $new_customer = array('firstName' => $this->input->post('firstName'),
            'lastName' => $this->input->post('lastName'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'status' => 'new',
						'token' => md5($this->input->post('lastName').$this->input->post('email').$this->input->post('password')),);
        $this->db->insert('customers', $new_customer);
        if ($this->db->error()) {
            return $this->db->error();
        } else {
            return TRUE;
        }
    }

    public function active() {
				$customer = $this->db->select('id,active')->from('customers')->where(array('token' => $this->input->get('token')))->get()->row_array();
				if ($customer) {
						$customer_up = array(
								'active' => 1
						);
						$this->db->where(array('id' => $customer['id']));
						$this->db->update('customers', $customer_up);
						return $customer;
				} else {
						return FALSE;
				}
    }

    public function login($email, $password) {
				$customer = $this->db->select('id,email,firstName')->from('customers')->where(array('email' => $email, 'password' => $password, 'active' => '1'))->get()->row_array();
				if ($customer && $customer['id'] != 1) {
					//echo "<pre>"; print_r($customer); die;
						$customer['type'] = 'customer';
						$this->session->set_userdata($customer);
						return TRUE;
				} else {
						return FALSE;
				}
    }

    public function update($id) {
        $customer = array('firstName' => $this->input->post('firstName'),
            'lastName' => $this->input->post('lastName'),
            'billingAddress1' => $this->input->post('billingAddress1'),
            'billingAddress2' => $this->input->post('billingAddress2'),
            'billingCity' => $this->input->post('billingCity'),
            'billingPostcode' => $this->input->post('billingPostcode'),
            'billingState' => $this->input->post('billingState'),
            'billingCountry' => $this->input->post('billingCountry'),
            'billingPhone' => $this->input->post('billingPhone'),
            'company' => $this->input->post('company'),
        );
        $this->db->where(array('id' => $id));
        $this->db->update('customers', $customer);
        if ($this->db->error()) {
            return $this->db->error();
        } else {
            return TRUE;
        }
    }

    public function getList() {
        $res = $this->db->select("*")->from('customers')->get()->result();
        return $res;
    }

    public function getOrders($category = '') {
        $res = NULL;
        if ($category == '') {
            if (($this->session->userdata('id') && $this->session->userdata('usertype') == 'admin')) {
                $res = $this->db->select('*')->from('orders')->get()->result();
            } else {
                $res = $this->db->select('*')->from('orders')->where(array('customer_id' => $this->id))->get()->result();
            }
        } else {
            if (($this->session->userdata('id') && $this->session->userdata('usertype') == 'admin')) {
                $res = $this->db->select('*')->from('orders')->where(array('category_id' => $category))->get()->result();
            }else{
               $res = $this->db->select('*')->from('orders')->where(array('customer_id' => $this->id, 'category_id' => $category))->get()->result();
            }
        }
        return $res;
    }
    public function getBusinessPage($table, $order_id){
      $res = $this->db->select('*')
            ->from($table)
            ->where(array('order_id' => $order_id))
            ->get()
            ->row();
        return $res;

    }


    public function getCategories() {

        if (($this->session->userdata('id') && $this->session->userdata('usertype') == 'admin')) {
            return $this->db->query("SELECT * FROM categories WHERE 1")->result();
        }

       $res = $this->db->query('SELECT * FROM categories WHERE id IN (SELECT category_id FROM orders WHERE customer_id = '.$this->id.')')->result();

       return $res;
    }

    public function delete($id) {
        $this->db->query("DELETE FROM hotels WHERE order_id IN(SELECT id FROM orders WHERE customer_id=" . $id . ")");
        $this->db->query("DELETE FROM resturants WHERE order_id IN(SELECT id FROM orders WHERE customer_id=" . $id . ")");
        $this->db->query("DELETE FROM apartments WHERE order_id IN(SELECT id FROM orders WHERE customer_id=" . $id . ")");
        $this->db->query("DELETE FROM shopes WHERE order_id IN(SELECT id FROM orders WHERE customer_id=" . $id . ")");
        $this->db->query("DELETE FROM orders WHERE customer_id =" . $id . "");
        $this->db->query("DELETE FROM customers WHERE id=" . $id);
        return TRUE;
    }

    public function resetpassword(){
       $check = array('email'=>$this->session->userdata('email'),
                   'password'=>md5($this->input->post('password')));
      $newPwd = md5($this->input->post('n_password'));
       $user = $this->db->select("id")->from('customers')->where($check)->get()->row();
       if($user){
           $this->db->where(array('email'=>$check['email']));
           $this->db->update('customers',array('password'=>$newPwd));
           return true;
       }
       else{
           return "Current password is invalid";
       }
    }

}
