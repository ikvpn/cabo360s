<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script access is allwoed');
}

class Yacht_model extends CI_Model {

    public $id;
    public $enName;
    public $duName;
    public $itName;
	public $enInsta;
    public $enDescription;
    public $duDescription;
    public $itDescription;
    public $dailyPrice;
    public $weeklyPrice;
    public $unit;
    public $photo;
    public $guests;
    public $bedrooms;
    public $beds;
    public $lat;
    public $lng;
    public $trypology;
    // public $bathrooms;
    // public $terrace;
    public $rates;
    public $order_id;
    public $cDate;
    public $categories;
    // public $fbWidget;
    // public $tripWidget;
    // public $tags;
    // public $activity_services;
    public $phone;
    public $manager;
    public $manager_profile;
    public $yachtHomeHeighlights;

    public function __construct() {
        parent::__construct();
    }

    public function load($id) {
        $row = $this->db->select('*')
                ->from('yachts')
                ->where(array('id' => $id))
                ->get()
                ->row();
        if ($row) {
            $this->id                   = $row->id;
            $this->enName               = $row->enName;
            $this->duName               = $row->duName;
            $this->itName               = $row->itName;
            $this->enInsta              = $row->enInsta;
            $this->enDescription        = base64_decode($row->enDescription);
            $this->duDescription        = base64_decode($row->duDescription);
            $this->itDescription        = base64_decode($row->itDescription);
            $this->dailyPrice           = $row->dailyPrice;
            $this->weeklyPrice          = $row->weeklyPrice;
            $this->phone                = $row->phone;
            $this->manager              = $row->manager;
            $this->manager_profile      = $row->manager_profile;
            $this->unit                 = $row->unit;
            $this->photo                = $row->photo;
            $this->bedrooms             = $row->bedrooms;
            $this->beds                 = $row->beds;
            $this->guests               = $row->guests;
            $this->trypology            = $row->trypology;
            // $this->bathrooms = $row->bathrooms;
            // $this->terrace = $row->terrace;
            $this->rates                = $row->rates;
            $this->order_id             = $row->order_id;
            $this->lat                  = $row->lat;
            $this->lng                  = $row->lng;
            $this->cDate                = $row->cDate;
            $this->categories           = $row->categories;
            // $this->fbWidget = $row->fbWidget;
            // $this->tripWidget = $row->tripWidget;
            // $this->tags = $row->tags;
            // $this->activity_services = $row->activity_services;
            $this->yachtHomeHeighlights = $row->yachtHomeHeighlights;
        } else {
            $this->id = $this->enName = $this->duName = $this->itName = $this->phone = $this->manager = $this->manager_profile = $this->enInsta = $this->enDescription = $this->duDescription = $this->itDescription = $this->dailyPrice = $this->weeklyPrice = NULL ;
            $this->unit = $this->photo = $this->bedrooms = $this->beds = $this->guests = $this->trypology = $this->rates = $this->order_id = $this->cDate = $this->categories = $this->yachtHomeHeighlights = NULL;
        }
    }

    public function loadOnOrderId($id) {
        $row = $this->db->select('*')
                ->from('yachts')
                ->where(array('order_id' => $id))
                ->get()
                ->row();
        if ($row) {
            $this->id                   = $row->id;
            $this->enName               = $row->enName;
            $this->duName               = $row->duName;
            $this->itName               = $row->itName;
            $this->enInsta              = $row->enInsta;
            $this->enDescription        = base64_decode($row->enDescription);
            $this->duDescription        = base64_decode($row->duDescription);
            $this->itDescription        = base64_decode($row->itDescription);
            $this->dailyPrice           = $row->dailyPrice;
            $this->weeklyPrice          = $row->weeklyPrice;
            $this->phone                = $row->phone;
            $this->manager              = $row->manager;
            $this->manager_profile      = $row->manager_profile;
            $this->unit                 = $row->unit;
            $this->photo                = $row->photo;
            $this->bedrooms             = $row->bedrooms;
            $this->beds                 = $row->beds;
            $this->guests               = $row->guests;
            $this->trypology            = $row->trypology;
            // $this->bathrooms = $row->bathrooms;
            // $this->terrace = $row->terrace;
            $this->rates                = $row->rates;
            $this->order_id             = $row->order_id;
            $this->lat                  = $row->lat;
            $this->lng                  = $row->lng;
            $this->cDate                = $row->cDate;
            $this->categories           = $row->categories;
            // $this->fbWidget = $row->fbWidget;
            // $this->tripWidget = $row->tripWidget;
            // $this->tags = $row->tags;
            // $this->activity_services = $row->activity_services;
            $this->yachtHomeHeighlights = $row->yachtHomeHeighlights;
        } else {
            $this->id = $this->enName = $this->duName = $this->itName = $this->phone = $this->manager = $this->manager_profile = $this->enInsta = $this->enDescription = $this->duDescription = $this->itDescription = $this->dailyPrice = $this->weeklyPrice = NULL ;
            $this->unit = $this->photo = $this->bedrooms = $this->beds = $this->guests = $this->trypology = $this->rates = $this->order_id = $this->cDate = $this->categories = $this->yachtHomeHeighlights = NULL;
        }
    }

    public function update() {
        if ($this->id == 0) {
            //insert
            $yacht = array(
                'enName'               => $this->enName,
                'duName'               => $this->duName,
                'itName'               => $this->itName,
                'enInsta'              => $this->enInsta,
                'enDescription'        => base64_encode($this->enDescription),
                'duDescription'        => base64_encode($this->duDescription),
                'itDescription'        => base64_encode($this->itDescription),
                'dailyPrice'           => $this->dailyPrice,
                'weeklyPrice'          => $this->weeklyPrice,
                'unit'                 => $this->unit,
                'photo'                => $this->photo ? $this->photo : "",
                'bedrooms'             => $this->bedrooms,
                'beds'                 => $this->beds,
                'guests'               => $this->guests,
                'trypology'            => $this->trypology,
                // 'bathrooms' => $this->bathrooms,
                // 'terrace' => $this->terrace,
                'phone'                => $this->phone,
				'manager'              => $this->manager,
				'manager_profile'      => $this->manager_profile,
                'rates'                => $this->rates,
                'lat'                  => $this->lat,
                'lng'                  => $this->lng,
                'order_id'             => $this->order_id,
                'categories'           => $this->categories,
                // 'fbWidget'=>$this->fbWidget,
                // 'tripWidget'=>$this->tripWidget,
                // 'tags'=>$this->tags,
                // 'activity_services'    =>$this->activity_services,
                'yachtHomeHeighlights' => $this->yachtHomeHeighlights
            );
            $this->db->insert('yachts', $yacht);
            return TRUE;
        } else {
            //update
            $yacht = array(
                'enName'               => $this->enName,
                'duName'               => $this->duName,
                'itName'               => $this->itName,
                'enInsta'              => $this->enInsta,
                'enDescription'        => base64_encode($this->enDescription),
                'duDescription'        => base64_encode($this->duDescription),
                'itDescription'        => base64_encode($this->itDescription),
                'dailyPrice'           => $this->dailyPrice,
                'weeklyPrice'          => $this->weeklyPrice,
                'unit'                 => $this->unit,
                'photo'                => $this->photo ? $this->photo : "",
                'bedrooms'             => $this->bedrooms,
                'beds'                 => $this->beds,
                'guests'               => $this->guests,
                'trypology'            => $this->trypology,
                // 'bathrooms' => $this->bathrooms,
                // 'terrace' => $this->terrace,
                'phone'                => $this->phone,
                'manager'              => $this->manager,
                'manager_profile'      => $this->manager_profile,
                'rates'                => $this->rates,
                'lat'                  => $this->lat,
                'lng'                  => $this->lng,
                'order_id'             => $this->order_id,
                'categories'           => $this->categories,
                // 'fbWidget'=>$this->fbWidget,
                // 'tripWidget'=>$this->tripWidget,
                // 'tags'=>$this->tags,
                // 'activity_services'    =>$this->activity_services,
                'yachtHomeHeighlights' => $this->yachtHomeHeighlights
            );
            $this->db->where(array('id' => $this->id));
            $this->db->update('yachts', $yacht);
            return TRUE;
        }
    }

    public function update_photo($photo) {
        $this->db->where(array('id' => $this->id));
        $this->db->update('yachts', Array('photo' => $photo));
        return true;
    }

    public function getActiveApartments($lang = 'en') {
        $page = $this->uri->segment(4)?$this->uri->segment(4):0;
        $start = 0;
        if($page >0){
         $start = (--$page) * 10;
        }
        $limit = 10;
        $res = $this->db->select('a.*, o.title as pageTitle, lt.title as locationName')
                ->from('yachts AS a')
                ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
                ->join('locations_t AS lt', 'lt.location_id=o.location AND lang="' . $lang . '"', 'LEFT OUTER')
                ->where(array('o.status' => 'paid'))
                ->limit($limit,$start)
                ->get()
                ->result();
        return $res;
    }

     public function getCustomActiveApartments($lang = 'en') {
        $page = $this->uri->segment(4)?$this->uri->segment(4):0;
        $start = 0;
        if($page >0){
         $start = (--$page) * 10;
        }
        $limit = 10;
        $conditions = array('o.status'=>'paid');
        if($this->input->post('rooms')>0){
            $conditions['a.rooms'] = $this->input->post('rooms');
        }
        if($this->input->post('beds')>0){
            $conditions['a.beds'] = $this->input->post('beds');
        }
        if($this->input->post('dailyPrice')){
            $conditions['a.dailyPrice >'] = $this->input->post('dailyPrice');
        }
        if($this->input->post('locations')){
            $this->db->where_in('o.location',$this->input->post('locations'));
        }
        $res = $this->db->select('a.*, o.title as pageTitle, lt.title as locationName')
                ->from('yachts AS a')
                ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
                ->join('locations_t AS lt', 'lt.location_id=o.location AND lang="' . $lang . '"', 'LEFT OUTER')
                ->where($conditions)
                ->limit($limit,$start)
                ->get()
                ->result();
      //  echo $this->db->last_query();exit();
        return $res;
    }

    public function totalActiveApartments() {
        $res = $this->db->select('a.id')
                ->from('yachts AS a')
                ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
                ->where(array('o.status' => 'paid'))
                ->get();
        return $res->num_rows();
    }

    public function totalCustomActiveApartments() {
         $conditions = array('o.status'=>'paid');
        if($this->input->post('rooms')>0){
            $conditions['a.rooms'] = $this->input->post('rooms');
        }
        if($this->input->post('beds')>0){
            $conditions['a.beds'] = $this->input->post('beds');
        }
        if($this->input->post('pax')>0){
            $conditions['a.pax'] = $this->input->post('pax');
        }
        $res = $this->db->select('a.id')
                ->from('yachts AS a')
                ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
                ->where($conditions)
                ->get();
        return $res->num_rows();
    }

    public function loadActivePage($order_title) {
        $row = $this->db->select('id')->from('orders')->where(array('title' => $order_title))->get()->row();
        if ($row) {
            $this->loadOnOrderId($row->id);
        }
    }

    public function getLocationName($lang = "en") {
        $res = $this->db->query("SELECT lt.title FROM locations_t as lt LEFT OUTER JOIN orders o ON (o.location=lt.location_id) WHERE lang='" . $lang . "' AND o.id='" . $this->order_id . "'")->row();
        // echo $this->db->last_query();exit();
        if ($res) {
            return $res->title;
        } else {
            return " ";
        }
    }
}
