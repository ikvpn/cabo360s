<?php


if(!defined('BASEPATH')){exit('No direct script access is allowed');}

class Beachclub_model extends CI_Model{

    public $id;
    public $enName;
    public $itName;
    public $duName;
    public $enSlogan;
    public $duSlogan;
    public $itSlogan;
	  public $enInsta;
    public $enDescription;
    public $itDescription;
    public $duDescription;
    public $photo;
    public $phone;
    public $manager;
    public $manager_profile;
    public $website;
    public $cuisine;
    public $creditCards;
    public $hours;
    public $hours2;
    public $closingDay;
    public $Info;
    public $category;
    public $minPrice;
    public $maxPrice;
    public $rating;
    public $address;
    public $order_id;
    public $cDate;
    public $lat;
    public $lng;
    public $services;
    public $categories;
    public $fbWidget;
    public $tags;
    public $activity_services;

    public function __construct() {
        parent::__construct();
    }

    public function load($id){
        $row = $this->db->select('*')->from('beach_clubs')->where(array('id'=>$id))->get()->row();
        if($row){
            $this->id = $row->id;
            $this->enName = $row->enName;
            $this->itName = $row->itName;
            $this->duName = $row->duName;
            $this->enSlogan = $row->enSlogan;
            $this->duSlogan = $row->duSlogan;
            $this->itSlogan = $row->itSlogan;
			      $this->enInsta = $row->enInsta;
            $this->enDescription = base64_decode($row->enDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->photo = $row->photo;
            $this->category = $row->category;
            $this->phone = $row->phone;
            $this->manager = $row->manager;
            $this->manager_profile = $row->manager_profile;
            $this->website = $row->website;
            $this->cuisine = $row->cuisine;
            $this->creditCards = $row->creditCards;
            $this->hours = $row->hours;
            $this->hours2 = $row->hours2;
            $this->closingDay = $row->closingDay;
            $this->Info = $row->Info;
            $this->minPrice = $row->minPrice;
            $this->maxPrice = $row->maxPrice;
            $this->rating = $row->rating;
            $this->address = $row->address;
            $this->order_id = $row->order_id;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->cDate = $row->cDate;
            $this->services = $row->services;
            $this->categories = $row->categories;
            $this->fbWidget = $row->fbWidget;
            $this->tags = $row->tags;
            $this->activity_services = $row->activity_services;
        }else{
           $this->id = $this->enName =$this->itName = $this->duName = $this->enSlogan = $this->duSlogan = $this->itSlogan= $this->enInsta =$this->manager=$this->manager_profile = NULL;
           $this->enDescription = $this->itDescription = $this->duDescription = $this->categories = $this->fbWidget = $this->tags = $this->activity_services = NULL;
           $this->photo = $this->category = $this->minPrice = $this->maxPrice = $this->rating = $this->address = $this->order_id = $this->cDate = $this->hours2 =  NULL;
        }
    }


    public function loadOnOrderId($id){
        $row = $this->db->select('*')->from('beach_clubs')->where(array('order_id'=>$id))->get()->row();
        if($row){
            $this->id = $row->id;
            $this->enName = $row->enName;
            $this->itName = $row->itName;
            $this->duName = $row->duName;
            $this->enSlogan = $row->enSlogan;
            $this->duSlogan = $row->duSlogan;
            $this->itSlogan = $row->itSlogan;
			      $this->enInsta = $row->enInsta;
            $this->enDescription = base64_decode($row->enDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->photo = $row->photo;
            $this->category = $row->category;
            $this->phone = $row->phone;
            $this->manager = $row->manager;
            $this->manager_profile = $row->manager_profile;
            $this->website = $row->website;
            $this->cuisine = $row->cuisine;
            $this->creditCards = $row->creditCards;
            $this->hours = $row->hours;
            $this->hours2 = $row->hours2;
            $this->closingDay = $row->closingDay;
            $this->Info = $row->Info;
            $this->minPrice = $row->minPrice;
            $this->maxPrice = $row->maxPrice;
            $this->rating = $row->rating;
            $this->address = $row->address;
            $this->order_id = $row->order_id;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->cDate = $row->cDate;
            $this->services = $row->services;
            $this->categories = $row->categories;
            $this->fbWidget = $row->fbWidget;
            $this->tags = $row->tags;
            $this->activity_services = $row->activity_services;
        }else{
           $this->id = $this->enName =$this->itName = $this->duName = $this->enSlogan = $this->duSlogan = $this->itSlogan= $this->enInsta =$this->manager=$this->manager_profile = NULL;
           $this->enDescription = $this->itDescription = $this->duDescription = $this->categories = $this->fbWidget = $this->tags = $this->activity_services = NULL;
           $this->photo = $this->category = $this->minPrice = $this->maxPrice = $this->rating = $this->address = $this->order_id = $this->cDate = $this->hours2 = NULL;
        }
    }

    public function update(){
        if($this->id == 0){
            $hotel = array('enName'=>$this->enName,
                           'duName'=>$this->duName,
                           'itName'=>$this->itName,
							             'enSlogan' => $this->enSlogan,
						               'duSlogan' => $this->duSlogan,
							             'itSlogan' => $this->itSlogan,
                		       'enInsta' => $this->enInsta,
                           'enDescription'=>  base64_encode($this->enDescription),
                           'duDescription'=> base64_encode($this->duDescription),
                           'itDescription'=> base64_encode($this->itDescription),
                           'category'=>$this->category,
                           'minPrice'=>$this->minPrice,
                           'maxPrice'=>$this->maxPrice,
                           'phone' => $this->phone,
							             'manager' => $this->manager,
							             'manager_profile' => $this->manager_profile,
                           'website' => $this->website,
                           'cuisine' => $this->cuisine,
                           'creditCards' => $this->creditCards,
                           'hours' => $this->hours,
                           'hours2' => $this->hours2,
                           'closingDay' => $this->closingDay,
                           'Info' => $this->Info,
                           'address' => $this->address,
                           'order_id'=>$this->order_id,
                           'photo'=>  $this->photo?$this->photo:"",
                           'rating'=> $this->photo?$this->rating:0,
                           'lat'=> $this->lat?$this->lat:0.0,
                           'lng'=>$this->lng?$this->lng:0.0,
                           'services'=>$this->services,
                           'categories'=>$this->categories,
                           'fbWidget'=>$this->fbWidget,
                           'tags'=>$this->tags,
                           'activity_services'=>$this->activity_services
                );
            $this->db->insert('beach_clubs',$hotel);
            return TRUE;
        }else{
            //update
            $hotel = array('enName'=>$this->enName,
                           'duName'=>$this->duName,
                           'itName'=>$this->itName,
							             'enSlogan' => $this->enSlogan,
							             'duSlogan' => $this->duSlogan,
							             'itSlogan' => $this->itSlogan,
               				     'enInsta' => $this->enInsta,
                           'enDescription'=>  base64_encode($this->enDescription),
                           'duDescription'=> base64_encode($this->duDescription),
                           'itDescription'=> base64_encode($this->itDescription),
                           'category'=>$this->category,
                           'phone' => $this->phone,
							             'manager' => $this->manager,
							             'manager_profile' => $this->manager_profile,
                           'website' => $this->website,
                           'cuisine' => $this->cuisine,
                           'creditCards' => $this->creditCards,
                           'hours' => $this->hours,
                           'hours2' => $this->hours2,
                           'closingDay' => $this->closingDay,
                           'Info' => $this->Info,
                           'minPrice'=>$this->minPrice,
                           'maxPrice'=>$this->maxPrice,
                           'address'=> $this->address,
                           'order_id'=>$this->order_id,
                           'photo'=>  $this->photo?$this->photo:"",
                           'rating'=> $this->photo?$this->rating:0,
                           'lat'=> $this->lat?$this->lat:0.0,
                           'lng'=>$this->lng?$this->lng:0.0,
                           'services'=>$this->services,
                           'categories'=>$this->categories,
                           'fbWidget'=>$this->fbWidget,
                           'tags'=>$this->tags,
                           'activity_services'=>$this->activity_services
                 );

            $this->db->where(array('id'=>$this->id));
            $this->db->update('beach_clubs',$hotel);
            return TRUE;
        }
    }

    public function update_photo($photo){
        $this->db->where(array('id'=>$this->id));
        $this->db->update('beach_clubs',Array('photo'=>$photo));
        return true;
    }

    public function getActiveShops($lang='en'){

        $page = $this->uri->segment(4)?$this->uri->segment(4):0;
        $start = 0;
        if($page >0){
         $start = (--$page) * 10;
        }
        $limit = 10;
        $res = $this->db->select('s.*, o.title as pageTitle, lt.title as locationName')
                ->from('beach_clubs AS s')
                ->join('orders AS o','s.order_id=o.id','LEFT OUTER')
                ->join('locations_t AS lt','lt.location_id=o.location AND lang="'.$lang.'"','LEFT OUTER')
                ->where(array('o.status'=>'paid'))
                ->limit($limit,$start)
                ->get()
                ->result();
      return $res;
    }

    public function totalActiveShops(){

        $res = $this->db->select('s.id')
                ->from('beach_clubs AS s')
                ->join('orders AS o', 's.order_id=o.id', 'LEFT OUTER')
                ->where(array('o.status' => 'paid'))
                ->get();
        return $res->num_rows();

    }


     public function loadActivePage($order_title){

        $row = $this->db->select('id')->from('orders')->where(array('title'=>$order_title))->get()->row();
        if($row){
            $this->loadOnOrderId($row->id);
        }
    }

     public function getLocationName($lang = "en", $order_id=0){
        $res = $this->db->query("SELECT lt.title FROM locations_t as lt LEFT OUTER JOIN orders o ON (o.location=lt.location_id) WHERE lang='".$lang."' AND o.id='".$order_id."'")->row();

        if($res){
            return $res->title;
        }else{
            return " ";
        }
    }
}
