<?php

if (!defined("BASEPATH")) {
    exit("No direct script access is allowed");
}

class Transfer_model extends CI_Model {

    public $id;
    public $itTitle;
    public $enTitle;
    public $categories;
    public $dateStart;
    public $dateEnd;
    public $daysAvailability;
    public $durationDay;
    public $durationHours;
    public $durationMin;
    public $sessionHour1;
    public $sessionHour2;
    public $sessionHour3;
    public $priceUnit;
    public $price;
    public $discountAdult;
    public $discountChildren;
    public $itDescription;
    public $itServicesIncluded;
    public $itServicesNotIncluded;
    public $itPolicy;
    public $enDescription;
    public $enServicesIncluded;
    public $enServicesNotIncluded;
    public $enPolicy;
    public $customerId;
    public $instagramWidget;
    public $type;
    public $thumbnail;
    public $cover;
    public $slug;
    public $businessPageId;
    // public $car_type;
    // public $boat_type;
    public $trip_type;
    public $orderId;
    public $pickup;
    // public $dropoff;
    // public $transfer_type;
    public $startPrice;


    public function __construct() {
        parent::__construct();
    }


    public function load($id, $customerId) {
        $row = $this->db->select('*')->from('transfer')->where(array('id' => $id, 'customerId' => $customerId))->get()->row();
        if ($row) {
            $this->id = $row->id;
            $this->itTitle = $row->itTitle;
            $this->enTitle = $row->enTitle;
            $this->categories = $row->categories;
            $this->dateStart = $row->dateStart;
            $this->dateEnd = $row->dateEnd;
            $this->daysAvailability = $row->daysAvailability;
            $this->itDescription = base64_decode($row->itDescription);
            $this->itServicesIncluded = base64_decode($row->itServicesIncluded);
            $this->itServicesNotIncluded = base64_decode($row->itServicesNotIncluded);
            $this->itPolicy = base64_decode($row->itPolicy);
            $this->enDescription = base64_decode($row->enDescription);
            $this->enServicesIncluded = base64_decode($row->enServicesIncluded);
            $this->enServicesNotIncluded = base64_decode($row->enServicesNotIncluded);
            $this->enPolicy = base64_decode($row->enPolicy);
            $this->customerId = $row->customerId;
            $this->thumbnail = $row->thumbnail;
            // $this->car_type = $row->car_type;
            $this->trip_type = $row->trip_type;
            // $this->boat_type = $row->boat_type;
            $this->pickup = $row->pickup;
            // $this->dropoff = $row->dropoff;
            $this->duration = $row->duration;
            $this->maxPassenger = $row->maxPassenger;
            $this->cover = $row->cover;
            $this->slug = $row->slug;
            $this->businessPageId = $row->businessPageId;
            $this->orderId = $row->orderId;
            // $this->transfer_type = $row->transfer_type;
            $this->startPrice = $row->startPrice;
        } else {
            $this->id = $this->itTitle = $this->enTitle = $this->categories = $this->dateStart = $this->slug = $this->businessPageId = $this->orderId = NULL;
            $this->dateEnd = $this->daysAvailability = $this->enServicesIncluded = NULL;
            $this->itDescription = $this->enDescription = $this->itServicesIncluded = NULL;
            $this->itServicesNotIncluded = $this->enServicesNotIncluded = $this->thumbnail = $this->cover = $this->itPolicy = $this->enPolicy = $this->customerId = NULL;
        }
    }

    public function getAllTransferOfCustomer($id) {
        return $this->db->select('*')->from('transfer')->where(array('customerId' => $id))->get()->result();
    }
    public function gettransfer($slug){
      $res = $this->db->select('transfer.*,orders.title as business_title')
              ->from('transfer')
              ->where(array('slug' => $slug))
              ->join('orders', 'orders.id=transfer.businessPageId', 'LEFT OUTER')
              ->get()
              ->row();
    /*  $order = $this->db->select('*')->from('orders')->where(array('id' => $res->orderId))->get()->row();
      $category = $this->db->select('*')->from('categories')->where(array('id' => $order->category_id))->get()->row();
      $table = $category->table;

      $businessPage = $this->db->select('*')->from($table)->where(array('id' => $res->businessPageId))->get()->row();
      $businessPage->type = $table;
      $businessPage->categoryID = $category->id;
      $businessPage->pageTitle = $order->title;
      $businessPage->urlType = $category->type;
      $res->businessPage = $businessPage;*/
       //echo "<pre>"; print_r($res); die;
      return $res;
    }

    public function fetchAll(){
        $res = $this->db->select('e.*, lt.title as locationName')
                ->distinct()
                ->from('transfer AS e')
                ->join('locations_t AS lt', 'lt.location_id=e.locationId', 'LEFT OUTER')
                ->get()
                ->result();

        return $res;
    }

    public function update() {

    	$customerId = $this->customerId;

      $transfer = array('enTitle' => $this->enTitle,
            'categories' => $this->categories,
            'itTitle' => $this->itTitle,
            'enTitle' => $this->enTitle,
            'itDescription' => base64_encode($this->itDescription),
            'enDescription' => base64_encode($this->enDescription),
            'itServicesIncluded' => base64_encode($this->itServicesIncluded),
            'itServicesNotIncluded' => base64_encode($this->itServicesNotIncluded),
            'itPolicy' => base64_encode($this->itPolicy),
            'enServicesIncluded' => base64_encode($this->enServicesIncluded),
            'enServicesNotIncluded' => base64_encode($this->enServicesNotIncluded),
            'enPolicy' => base64_encode($this->enPolicy),
            'dateStart' => $this->dateStart,
            'dateEnd' => $this->dateEnd,
            'daysAvailability' => $this->daysAvailability,
            'customerId' => $this->customerId,
            'cover'=>$this->cover,
            'businessPageId' => $this->businessPageId,
            'pickup' => $this->pickup,
            // 'dropoff' => $this->dropoff,
            'maxPassenger' => $this->maxPassenger,
            // 'car_type' => $this->car_type,
            // 'boat_type' => $this->boat_type,
            'duration' => $this->duration,
            'trip_type' => $this->trip_type,
            'businessPageId' => $this->businessPageId,
            'orderId' => $this->orderId,
            // 'transfer_type' => $this->transfer_type,
            'startPrice' => $this->startPrice
        );

        if ($this->id == 0) {
            $transfer['slug'] = $this->create_slug($transfer['itTitle']);
            $this->db->insert('transfer', $transfer);
            return $this->db->insert_id();
        } else {
            $delete_zoneData = $this->db->delete('zone', array('transfer_id' => $this->id)); 

            $zone_name = $_POST['zone_name'];
            foreach ($zone_name as $key => $value) 
            {
                $zoneData = array(
                    'transfer_id' => $this->id,
                    'zone_name' => $value,
                    'sjd_price' => $_POST['sjd_price'][$key],
                    'csl_price' => $_POST['csl_price'][$key],
                );
                $this->db->insert('zone', $zoneData);
            }
            $this->db->where(array('id' => $this->id));
            $this->db->update('transfer', $transfer);
            return TRUE;
        }
    }
    public function create_slug($name){
        $count = 0;
        $slug_name = $name = url_title($name);

        $this->db->from('transfer')->where('slug', $slug_name);
        if ($this->db->count_all_results() > 0) {
          $slug_name = $name . '-' . (++$count);
        }

        return $slug_name;
    }


    public function update_photo($data) {
        $this->db->where(array('id' => $this->id));
        $this->db->update('transfer', $data);
        return true;
    }
    public function totalActivetransfer($type){
        $res = $this->db->select('e.id')
                ->from('transfer AS e')
                ->where(array('e.type'=>$type))
                ->get();
        return $res->num_rows();
    }

    public function getActivetransfer() {
        $res = $this->db->select('*')                
                ->from('transfer')
                ->get()
                ->result();
                // echo "<pre>";
                // print_r($res);
                // exit;
        return $res;
    }
    public function getRandomtransfer($count){
      $transfer = $this->getActivetransfer();
      if (count($transfer) > $count) {
        return $this->get_random_elements($transfer, $count);
      }
      else {
          return $this->get_random_elements($transfer);
      }
    }

    function get_random_elements($array, $limit = 0 ) {
      shuffle($array);
      if ( $limit > 0 ) {
          $array = array_splice($array, 0, $limit);
      }
      return $array;
    }

    public function getLocationName($id){
         $res = $this->db->query("SELECT lt.title FROM locations_t as lt LEFT OUTER JOIN transfer t ON (t.locationId=lt.location_id) WHERE t.id='".$id."'")->row();
        if($res){
            return $res->title;
        }else{
            return " ";
        }
    }

     public function delete($id) {
        $this->db->where(array('id' => $id));
        $this->db->delete('transfer');
        return true;
    }

    public function gettransferInPeriod($checkIn, $checkOut) {

      $checkIn = date('Y-m-d' , strtotime($checkIn));
      $checkOut = date('Y-m-d', strtotime($checkOut));

      $this->db->select('*');
      $this->db->from('transfer');
      $res = $this->db->get()->result();

      $transfer = array();

      foreach ($res as $key => $transfer) {
        # code...
        $dateStart = $transfer->dateStart;
        $dateEnd = $transfer->dateEnd;

        $dateStart = date('Y-m-d', strtotime($dateStart));
        $dateEnd = date('Y-m-d', strtotime($dateEnd));

        if ($checkIn >= $dateStart && $checkOut <= $dateEnd) {
          array_push($transfer, $transfer);
        }

      }


      //echo "<pre>"; print_r($transfer); die;
      return $res;
    }
}
