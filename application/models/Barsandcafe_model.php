<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script access is allowed');
}

class Barsandcafe_model extends CI_Model {

    public $id;
    public $enName;
    public $duName;
    public $itName;
	  public $enInsta;
    public $enSlogan;
    public $duSlogan;
    public $itSlogan;
    public $enDescription;
    public $duDescription;
    public $itDescription;
    public $phone;
    public $manager;
    public $manager_profile;
    public $website;
    public $min_price;
    public $max_price;
    public $services;
    public $creditCards;
    public $rating;
    public $hours;
    public $hours2;
    public $closingDay;
    public $Info;
    public $photo;
    public $order_id;
    public $address;
    public $lat;
    public $lng;
    public $cDate;
    public $categories;
    public $fbWidget;
    public $tags;
    public $activity_services;
    public $aPartire;

    public function __construct() {
        parent::__construct();
    }

    public function load($id) {
        $row = $this->db->select('*')
                ->from('bars_cafe')
                ->where(array('id' => $id))
                ->get()
                ->row();
        if ($row) {
            $this->id = $row->id;
            $this->enName = $row->enName;
            $this->duName = $row->duName;
            $this->itName = $row->itName;
			      $this->enInsta = $row->enInsta;
            $this->enSlogan = $row->enSlogan;
            $this->duSlogan = $row->duSlogan;
            $this->itSlogan = $row->itSlogan;
            $this->enDescription = base64_decode($row->enDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->phone = $row->phone;
            $this->manager = $row->manager;
            $this->manager_profile = $row->manager_profile;
            $this->website = $row->website;
            $this->min_price = $row->min_price;
            $this->max_price = $row->max_price;
            $this->services = $row->services;
            $this->creditCards = $row->creditCards;
            $this->rating = $row->rating;
            $this->hours = $row->hours;
            $this->hours2 = $row->hours2;
            $this->closingDay = $row->closingDay;
            $this->Info = $row->Info;
            $this->photo = $row->photo;
            $this->order_id = $row->order_id;
            $this->cDate = $row->cDate;
            $this->address = $row->address;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->categories = $row->categories;
            $this->fbWidget = $row->fbWidget;
            $this->tags = $row->tags;
            $this->activity_services = $row->activity_services;
            $this->aPartire = $row->aPartire;
        } else {
            $this->id = $row->id;
            $this->enName = $this->duName = $this->itName = $this->enSlogan = $this->duSlogan = $this->itSlogan = $this->enInsta = $this->enDescription = $this->duDescription = $this->itDescription = $this->phone = $this->tags = NULL;
            $this->manager=$this->manager_profile= $this->website = $this->min_price = $this->max_price = $this->cuisine = $this->creditCards = $this->rating = $this->hours = $this->hours = $this->closingDay = $this->Info = NULL;
            $this->photo = $this->order_id = $this->cDate = $this->lat = $this->lng = $this->categories = $this->fbWidget = $this->activity_services = NULL;
        }
    }

    public function loadOnOrderId($id) {
        $row = $this->db->select('*')
                ->from('bars_cafe')
                ->where(array('order_id' => $id))
                ->get()
                ->row();
        if ($row) {
            $this->id = $row->id;
            $this->enName = $row->enName;
            $this->duName = $row->duName;
            $this->itName = $row->itName;
			      $this->enInsta = $row->enInsta;
            $this->enSlogan = $row->enSlogan;
            $this->duSlogan = $row->duSlogan;
            $this->itSlogan = $row->itSlogan;
            $this->enDescription = base64_decode($row->enDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->phone = $row->phone;
            $this->manager = $row->manager;
            $this->manager_profile = $row->manager_profile;
            $this->website = $row->website;
            $this->min_price = $row->min_price;
            $this->max_price = $row->max_price;
            $this->services = $row->services;
            $this->creditCards = $row->creditCards;
            $this->rating = $row->rating;
            $this->hours = $row->hours;
            $this->hours2 = $row->hours2;
            $this->closingDay = $row->closingDay;
            $this->Info = $row->Info;
            $this->photo = $row->photo;
            $this->order_id = $row->order_id;
            $this->cDate = $row->cDate;
            $this->address = $row->address;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->categories = $row->categories;
            $this->fbWidget = $row->fbWidget;
            $this->tags = $row->tags;
            $this->activity_services = $row->activity_services;
             $this->aPartire = $row->aPartire;
        } else {
            $this->id = $this->enName = $this->duName = $this->itName = $this->enSlogan = $this->duSlogan = $this->itSlogan = $this->enInsta = $this->enDescription = $this->duDescription = $this->itDescription = $this->tags = NULL;
            $this->phone =$this->manager=$this->manager_profile= $this->website = $this->min_price = $this->max_price = $this->cuisine = $this->creditCards = $this->rating = $this->hours = $this->hours = $this->closingDay = $this->Info = NULL;
            $this->photo = $this->order_id = $this->cDate = $this->categories = $this->fbWidget = $this->activity_services = NULL;
        }
    }

    public function update() {
        if ($this->id == 0) {
            //insert
            $item = array('enName' => $this->enName,
                'duName' => $this->duName,
                'itName' => $this->itName,
                'enSlogan' => $this->enSlogan,
                'duSlogan' => $this->duSlogan,
                'itSlogan' => $this->itSlogan,
                'enInsta' => $this->enInsta,
                'enDescription' => base64_encode($this->enDescription),
                'duDescription' => base64_encode($this->duDescription),
                'itDescription' => base64_encode($this->itDescription),
                'phone' => $this->phone,
				        'manager' => $this->manager,
				        'manager_profile' => $this->manager_profile,
                'website' => $this->website,
                'min_price' => $this->min_price,
                'max_price' => $this->max_price,
                'services' => $this->services,
                'creditCards' => $this->creditCards,
                'rating' => $this->rating,
                'hours' => $this->hours,
                'hours2' => $this->hours2,
                'closingDay' => $this->closingDay,
                'Info' => $this->Info,
                'order_id' => $this->order_id,
                'address'=>$this->address,
                'lat'=> $this->lat,
                'lng'=> $this->lng,
                'categories'=>$this->categories,
                'fbWidget'=>$this->fbWidget,
                'tags'=>$this->tags,
                'activity_services'=>$this->activity_services,
                'aPartire'=> $this->aPartire
            );
            $this->db->insert('bars_cafe', $item);
            return TRUE;
        } else {
            //update
            $item = array('enName' => $this->enName,
                'duName' => $this->duName,
                'itName' => $this->itName,
                'enSlogan' => $this->enSlogan,
                'duSlogan' => $this->duSlogan,
                'itSlogan' => $this->itSlogan,
                'enInsta' => $this->enInsta,
                'enDescription' => base64_encode($this->enDescription),
                'duDescription' => base64_encode($this->duDescription),
                'itDescription' => base64_encode($this->itDescription),
                'phone' => $this->phone,
				        'manager' => $this->manager,
				        'manager_profile' => $this->manager_profile,
                'website' => $this->website,
                'min_price' => $this->min_price,
                'max_price' => $this->max_price,
                'services' => $this->services,
                'creditCards' => $this->creditCards,
                'rating' => $this->rating,
                'hours' => $this->hours,
                'hours2' => $this->hours2,
                'closingDay' => $this->closingDay,
                'Info' => $this->Info,
                'order_id' => $this->order_id,
                'address'=>$this->address,
                'lat'=>$this->lat,
                'lng'=>$this->lng,
                'categories'=>$this->categories,
                'fbWidget'=>$this->fbWidget,
                'tags'=>$this->tags,
                'activity_services'=>$this->activity_services,
                'aPartire'=> $this->aPartire
            );
            $this->db->where(array('id' => $this->id));
            $this->db->update('bars_cafe', $item);
            return TRUE;
        }
    }

    public function update_photo($photo) {
        $this->db->where(array('id' => $this->id));
        $this->db->update('bars_cafe', Array('photo' => $photo));
        return true;
    }

    public function getActiveBarsAndCafe($lang) {
        $page = $this->uri->segment(4)?$this->uri->segment(4):0;
        $start = 0;
        if($page >0){
         $start = (--$page) * 10;
        }
        $limit = 10;
        $res = $this->db->select('bc.*, o.title as pageTitle, lt.title as locationName')
                ->from('bars_cafe AS bc')
                ->join('orders AS o', 'bc.order_id=o.id', 'LEFT OUTER')
                ->join('locations_t AS lt', 'lt.location_id=o.location AND lang="' . $lang . '"', 'LEFT OUTER')
                ->where(array('o.status' => 'paid'))
                ->limit($limit,$start)
                ->get()
                ->result();
        return $res;
    }
    public function totalActiveBarsAndCafe(){
        $res = $this->db->select('r.id')
                ->from('bars_cafe AS r')
                ->join('orders AS o', 'r.order_id=o.id', 'LEFT OUTER')
                ->where(array('o.status' => 'paid'))
                ->get();
        return $res->num_rows();
    }

    public function loadActivePage($order_title){
        $row = $this->db->select('id')->from('orders')->where(array('title'=>$order_title))->get()->row();
        if($row){
            $this->loadOnOrderId($row->id);
        }
    }

    public function getLocationName($lang = "en"){
        $res = $this->db->query("SELECT lt.title FROM locations_t as lt LEFT OUTER JOIN orders o ON (o.location=lt.location_id) WHERE lang='".$lang."' AND o.id='".$this->order_id."'")->row();
       // echo $this->db->last_query();exit();
        if($res){
            return $res->title;
        }else{
            return " ";
        }
    }
}
