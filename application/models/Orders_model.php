<?php

if (!defined('BASEPATH')) {
    exit('No direct script access is allowed.');
}

class Orders_model extends CI_Model {

    public $id;
    public $customer_id;
    public $category_id;
    public $title;
    public $phone;
    public $location;
    public $address;
    public $status;
    public $eDate;
    public $cDate;
    public $payment_plan;
    public $payment_type;
    public $firstname;
    public $lastname;
    public $email;
    public $phoneNo;
    public $street_1;
    public $street_2;
    public $city;
    public $state_booking;
    public $p_code;
    public $trainOrFlight;
    public $paymentType;
    public $total;
    public $TransferType;
    public $checkIn;
    public $checkOut;
    public $pickupAddress;
    public $dropoffAddress;
    public $people;
    public $returnPeople;
    public $infants;
    public $pets;
    public $flight_train_no;
    public $return_flight_train_no;
    public $special_request;
    public $pickupReturnAddress;
    public $dropoffReturnAddress;
    public $transferPayment;
    public $transfer;
    public $tour;
    public $serviceType;
    public $company;

    public function __construct() {
        parent::__construct();
        $this->db->query("UPDATE orders SET status='expire' WHERE eDate < NOW()");
    }

    public function load($id) {
        $res = $this->db->select('*')->from('orders')->where(array('id' => $id))->get()->row();
        if ($res) {
            $this->id = $id;
            $this->customer_id = $res->customer_id;
            $this->category_id = $res->category_id;
            $this->phone = $res->phone;
            $this->title = $res->title;
            $this->address = $res->address;
            $this->location = $res->location;
            $this->eDate = $res->eDate;
            $this->cDate = $res->cDate;
            $this->payment_plan = $res->payment_plan;
            $this->payment_type = $res->payment_type;
            $this->status = $res->status;
            $this->agreement_amount = $res->agreement_amount;
        }else{
            $this->id = $this->customer_id = $this->category_id = $this->status = $this->eDate = $this->cDate =  $this->payment_plan = $this->agreement_amount = NULL;
        }
    }

    public function addnew($order){
        $this->db->insert('orders',$order);
        $error = $this->db->error();
        if ($error['code'] != 0) {
            return $this->db->error();
        } else {
            return $this->db->insert_id();
        }
    }

    public function delete($id){
        $this->db->where(array('id'=>$id));
        $this->db->delete('orders');
    }

    public function update($id,$data){
        if(isset($data['status']) && $data['status']=='paid'){
           $this->load($id);
           if($this->payment_plan =='M'){
               // add 30 days in current date than save
               $data['eDate'] = date('Y-m-d',strtotime("+30 days"));
           }else{
               // add 1 year in current date than save
               $data['eDate'] = date('Y-m-d',  strtotime("+360 days"));
           }
        }
        $this->db->where(array('id'=>$id));
        $this->db->update('orders',$data);
    }

    public function getList($where = NULL){
        $res = NULL;
        if(count($where)>0){
             $res = $this->db->select('*')->from('orders')->where($where)->get()->result();
        }else{
             $res = $this->db->select('*')->from('orders')->get()->result();
        }
        return $res;
    }
    public function getActivityByType($tableName){
      $res = array();
      $category =  $this->db->select('*')->from('categories')->where(array('table'=>$tableName))->get()->row();
      if ($category) {
        $res = $this->db->select('*')->from('orders')->where(array('category_id'=>$category->id))->get()->result();
      }
      return $res;
    }

    public function getCategoryName($id){
        $row = $this->db->select('name')->from('categories')->where(array('id'=>$id))->get()->row();
        if($row){
            return $row->name;
        }else{
            return FALSE;
        }
    }

    public function getCategoryTableName($id){
         $row = $this->db->select('table')->from('categories')->where(array('id'=>$id))->get()->row();
        if($row){
            return $row->table;
        }else{
            return FALSE;
        }
    }

    public function getLoadedCategoryName(){
        $row = $this->db->select('name')->from('categories')->where(array('id'=>$this->category_id))->get()->row();
        if($row){
            return $row->name;
        }else{
            return FALSE;
        }
    }

    public function getLocationName($id,$lang='en'){
        $row = $this->db->select('*')->from('locations_t')->where(array('lang'=>$lang,'location_id'=>$id))->get()->row();

        if($row){
            return $row->title;
        }else{
            return FALSE;
        }
    }

    public function getLoadedLocationName(){
        $row = $this->db->query("SELECT title FROM locations l LEFT OUTER JOIN locations_t lt ON(l.id= lt.location_id AND lang='en') WHERE l.id='".$this->location."'")->row();
        if($row){
            return $row->title;
        }else{
            return FALSE;
        }
    }

    public function updateStatus(){
        $this->db->query("UPDATE orders SET status='expire' WHERE id=".$this->id." AND eDate < NOW() AND status='paid'");
        $this->load($this->id);
    }

    public function getActiveRandomBusinessPage() {

       
        $activities = array();
        $res = $this->db->select('*')
                ->from('orders AS o')
                ->where(array('o.status' => 'paid'))
                ->get()
                ->result();

        $res = $this->get_random_elements($res, 15);
    
        /* MICHELE  modificato ciclo da 6 a 15 
        for ($i=0; $i < 6 ; $i++) { */
    if(!empty($res)){
        for ($i=0; $i < 15 ; $i++) {
         
          $category = $this->db->select('*')
                      ->from('categories AS c')
                      ->where(array('c.id' => $res[$i]->category_id))
                      ->get()
                      ->row();
          $location = $this->db->select('*')
                      ->from('locations_t AS l')
                      ->where(array('l.location_id' => $res[$i]->location))
                      ->get()
                      ->row();

          $table = $category->table.' AS activity';
          $businessPage = $this->db->select('*')
                          ->from($table)
                          ->where(array('activity.order_id' => $res[$i]->id))
                          ->get()
                          ->row();
                

           if(!empty($businessPage)){
             $businessPage->categoryID = $category->id;
             $businessPage->urlType = $category->type;
             $businessPage->categoryName = $category->table;

             $businessPage->locationName = $location->title;
   		        $businessPage->pageTitle = $res[$i]->title;
             array_push($activities, $businessPage);
           }

        }
    }

        return $activities;
    }

    function get_random_elements($array, $limit = 0 ) {
      shuffle($array);
      if ( $limit > 0 ) {
          $array = array_splice($array, 0, $limit);
      }
      return $array;
    }

    public function AddOrderDetails(){
      // echo $this->checkIn; exit;

      $Order_details = array('firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'email' => $this->email,
            'phone' => $this->phoneNo,
            'street_1' => $this->street_1,
            'street_2' => $this->street_2,
            'city' => $this->city,
            'state_booking' => $this->state_booking,
            'p_code' => $this->p_code,
            'trainOrFlight' => $this->trainOrFlight,
            'paymentType' => $this->paymentType,
            'total' => $this->total,
            'TransferType' => $this->TransferType,
            'checkIn' => $this->checkIn,
            'checkOut' => $this->checkOut,
            'pickupAddress' => $this->pickupAddress,
            'dropoffAddress' => $this->dropoffAddress,
            'people' => $this->people,
            'returnPeople' => $this->returnPeople,
            'infants' => $this->infants,
            'pets' => $this->pets,
            'flight_train_no' => $this->flight_train_no,
            'return_flight_train_no' => $this->return_flight_train_no,
            'pickupReturnAddress' => $this->pickupReturnAddress,
            'dropoffReturnAddress' => $this->dropoffReturnAddress,
            'transferPayment' => $this->transferPayment,
            'serviceType' => $this->serviceType,
            'status' => 'Success',
            'company' => $this->company,
        );
      // echo "<pre>"; print_r($Order_details); exit;
      $this->db->insert('order_details', $Order_details);
      return TRUE;
    }

    public function getOrderDetails(){

       $result = $this->db->select('*')
                ->from('order_details')
                ->get()
                ->result();

                return $result;
    }

    public function getTransferServiceID($id){
       $res = $this->db->select('title')
                ->from('orders')
                ->join('transfer','transfer.businessPageId=orders.id','LEFT OUTER')
                ->where('transfer.id',$id)
                ->get()
                ->row_array();

                return $res;
    }

     public function getTourServiceID($id){
       $res = $this->db->select('title')
                ->from('orders')
                ->join('tours','tours.businessPageId=orders.id','LEFT OUTER')
                ->where('tours.id',$id)
                ->get()
                ->row_array();

                return $res;
    }
}
