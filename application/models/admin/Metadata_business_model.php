<?php



if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Metadata_business_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAll(){
        $res = $this->db->select('*')
                ->from('meta_data_businesspage')
                ->get()
                ->result();
        return($res);
    }

    public function get($id){
        $res = $this->db->select('*')
                ->from('meta_data_businesspage')
                ->where('id', $id)
                ->get()
                ->row();
        return($res);
    }

    public function getByOrderID($order_id){
        $res = $this->db->select('*')
                ->from('meta_data_businesspage')
                ->where('order_id', $order_id)
                ->get()
                ->row();
        return($res);
    }

    public function updateByOrderId($order_id, $metadata){
        $this->db->where(array('order_id'=>$order_id));
        $this->db->update('meta_data_businesspage',$metadata);
        return true;
    }
    public function add($metadata){
        $this->db->insert('meta_data_businesspage', $metadata);
        $res = $this->db->insert_id();

        return $res;
    }
    public function delete($id) {
        $this->db->where(array('id' => $id));
        $this->db->delete('meta_data_businesspage');
        return true;
    }

}
