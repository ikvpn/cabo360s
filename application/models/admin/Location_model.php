<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Location_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getLocations($limit = true, $lang = 'en') {
        $res = $this->db->query("SELECT l.*,lt.title,lt.description,en.title as enTitle FROM locations l LEFT OUTER JOIN locations_t lt ON(l.id=lt.location_id AND lt.lang='" . $lang . "') LEFT OUTER JOIN locations_t en ON(l.id=en.location_id AND en.lang='en')")->result();
        if ($res) {
            return $res;
        } else {
            return FALSE;
        }
    }
    public function getLocationForFrontEnd($title,$rewrite_url,$lang){
        $where = " WHERE tl.title='" . str_replace('_', ' ', $title) . "'";
        $res = $this->db->query("SELECT l.*, tl.title, tl.description,tl.sott, tl.meta_tag as meta_".$lang.", tl.meta_desc as meta_desc_".$lang.", tl.meta_keywords as meta_keywords_".$lang." , tl.rewrite_url as rewrite_url_".$lang.
                            " FROM locations l LEFT OUTER JOIN locations_t tl ON(l.id=tl.location_id AND tl.lang='" . $lang . "') " .
                            $where)->row();
        if ($res) {
            return $res;
        } else {
            $res = $this->db->query("SELECT l.*, tl.title, tl.description,tl.sott, tl.meta_tag as meta_".$lang.", tl.meta_desc as meta_desc_".$lang.", tl.meta_keywords as meta_keywords_".$lang." , tl.rewrite_url as rewrite_url_".$lang.
                            " FROM locations l LEFT OUTER JOIN locations_t tl ON(l.id=tl.location_id AND tl.lang='" . $lang . "') " .
                            " WHERE tl.rewrite_url='".$rewrite_url."'")->row();
            if($res){
                return $res;
            }
            return FALSE;
        }
    }

    public function getLocation($id = 0, $title = null,$rewrite_url='', $lang = false) {
        $res = null;
        $where = ' ';
        if ($id != 0) {
            $where = ' WHERE l.id=' . $id;
        } else if ($title != null) {
            if ($lang != FALSE) {
                $where = " WHERE tl.title='" . str_replace('_', ' ', $title) . "'";
            } else {
                $where = " WHERE en.title='" . str_replace('_', ' ', $title) . "'";
            }
        }
        if ($lang==false) {
            $res = $this->db->query("SELECT l.*, en.title as title_en,en.sott as sottEn, en.description as description_en, en.meta_tag as meta_en, en.meta_desc as meta_desc_en, en.meta_keywords as meta_keywords_en, en.rewrite_url as rewrite_url_en,".
                            " du.title as title_du, du.description as description_du,du.sott as sottDu, du.meta_tag as meta_du, du.meta_desc as meta_desc_du, du.meta_keywords as meta_keywords_du, du.rewrite_url as rewrite_url_du, " .
                            " it.title as title_it, it.description description_it, it.sott as sottIt, it.meta_tag as meta_it, it.meta_desc as meta_desc_it,it.meta_keywords as meta_keywords_it, it.rewrite_url as rewrite_url_it " .
                            " FROM locations l LEFT OUTER JOIN locations_t en ON(l.id=en.location_id AND en.lang='en') " .
                            " LEFT OUTER JOIN locations_t du ON(l.id=du.location_id AND du.lang='du')" .
                            " LEFT OUTER JOIN locations_t it ON(l.id=it.location_id AND it.lang='it') " .
                            $where)->row();
        } else {
            $res = $this->db->query("SELECT l.*, tl.title, tl.description,  tl.meta_tag as meta_".$lang.", tl.meta_desc as meta_desc_".$lang.", tl.meta_keywords as meta_keywords_".$lang." , tl.rewrite_url as rewrite_url_".$lang.
                            " FROM locations l LEFT OUTER JOIN locations_t tl ON(l.id=tl.location_id AND tl.lang='" . $lang . "') " .
                            $where)->row();
        }
        if ($res) {
            return $res;
        } else {
            $res = $this->db->query("SELECT l.*, tl.title, tl.description,tl.meta_tag as meta_".$lang.", tl.meta_desc as meta_desc_".$lang.", tl.meta_keywords as meta_keywords_".$lang." , tl.rewrite_url as rewrite_url_".$lang.
                            " FROM locations l LEFT OUTER JOIN locations_t tl ON(l.id=tl.location_id AND tl.lang='" . $lang . "') " .
                            " WHERE tl.rewrite_url='".$rewrite_url."'")->row();
            if($res){
                return $res;
            }
            return FALSE;
        }
    }

    public function insert() {
        $location = array(
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng')
        );
        $this->db->insert('locations', $location);
        $res = $this->db->insert_id();
        $data = array(
            array('title' => $this->input->post('title_en')?$this->input->post('title_en'):$this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_en')?$this->input->post('description_en'):$this->input->post('description_it')),
                'lang' => 'en',
                'meta_tag' => $this->input->post('meta_en')?$this->input->post('meta_en'):$this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_en')?$this->input->post('meta_desc_en'):$this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_en')?$this->input->post('meta_keywords_en'):$this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_en')?$this->input->post('rewrite_url_en'):$this->input->post('rewrite_url_it'),
                'location_id' => $res,
                'sott' => $this->input->post('sottotitolo_en')?$this->input->post('sottotitolo_en'):$this->input->post('sottotitolo_it')
            ),
            array('title' => $this->input->post('title_du')?$this->input->post('title_du'):$this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_du')?$this->input->post('description_du'):$this->input->post('description_it')),
                'lang' => 'du',
                'meta_tag' => $this->input->post('meta_du')?$this->input->post('meta_du'):$this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_du')?$this->input->post('meta_desc_du'):$this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_du')?$this->input->post('meta_keywords_du'):$this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_du')?$this->input->post('rewrite_url_du'):$this->input->post('rewrite_url_it'),
                'location_id' => $res,
                 'sott' => $this->input->post('sottotitolo_en')?$this->input->post('sottotitolo_en'):$this->input->post('sottotitolo_it')

            ),

            array('title' => $this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_it')),
                'lang' => 'it',
                'meta_tag' => $this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_it'),
                'location_id' => $res,
                'sott' => $this->input->post('sottotitolo_it')
            )
        );
        $this->insert_tdata($data);
        $this->db->insert('menu_items', array('category' => 'where_to_visit','link'=>' '));
        $m_id = $this->db->insert_id();
        $data = array(
            array('menu_id' => $m_id, 'name' => $this->input->post('title_en'), 'lang' => 'en'),
            array('menu_id' => $m_id, 'name' => $this->input->post('title_it'), 'lang' => 'it'),
            array('menu_id' => $m_id, 'name' => $this->input->post('title_du'), 'lang' => 'du')
        );
        $this->db->insert_batch('menu_items_t', $data);
        return $res;
    }

    public function insert_tdata($tdata) {
        $this->db->insert_batch('locations_t', $tdata);
    }

    public function update($id) {
        $location = array(
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng')
        );
        $this->db->where(array('id' => $id));
        $this->db->update('locations', $location);
        $this->db->where(array('location_id' => $id));
        $this->db->delete('locations_t');
        $data = array(
             array('title' => $this->input->post('title_en')?$this->input->post('title_en'):$this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_en')?$this->input->post('description_en'):$this->input->post('description_it')),
                'lang' => 'en',
                'meta_tag' => $this->input->post('meta_en')?$this->input->post('meta_en'):$this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_en')?$this->input->post('meta_desc_en'):$this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_en')?$this->input->post('meta_keywords_en'):$this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_en')?$this->input->post('rewrite_url_en'):$this->input->post('rewrite_url_it'),
                'location_id' => $id,
                'sott' => $this->input->post('sottotitolo_en')?$this->input->post('sottotitolo_en'):$this->input->post('sottotitolo_it')

            ),
            array('title' => $this->input->post('title_du')?$this->input->post('title_du'):$this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_du')?$this->input->post('description_du'):$this->input->post('description_it')),
                'lang' => 'du',
                'meta_tag' => $this->input->post('meta_du')?$this->input->post('meta_du'):$this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_du')?$this->input->post('meta_desc_du'):$this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_du')?$this->input->post('meta_keywords_du'):$this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_du')?$this->input->post('rewrite_url_du'):$this->input->post('rewrite_url_it'),
                'location_id' => $id,
                'sott' => $this->input->post('sottotitolo_en')?$this->input->post('sottotitolo_en'):$this->input->post('sottotitolo_it')
            ),

            array('title' => $this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_it')),
                'lang' => 'it',
                'meta_tag' => $this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_it'),
                'location_id' => $id,
                'sott' => $this->input->post('sottotitolo_it')

            )
        );
        $this->insert_tdata($data);
        return true;
    }

    public function update_image($id, $filedata) {
        $this->db->where(array('id' => $id));
        $this->db->update('locations', $filedata);
    }

    public function delete($id) {
        $this->db->where(array('location_id' => $id));
        $this->db->delete('locations_t');
        $this->db->where(array('id' => $id));
        $this->db->delete('locations');
        return true;
    }

    public function getMenuList($lang) {
        $res = $this->db->query("SELECT l.*,lt.title,en.title as enTitle FROM locations l LEFT OUTER JOIN locations_t lt ON(l.id=lt.location_id AND lt.lang='" . $lang . "') LEFT OUTER JOIN locations_t en ON(l.id=en.location_id AND en.lang='en')")->result();
        if ($res) {
            return $res;
        } else {
            return FALSE;
        }
    }

    public function count_orders_on_location_id($id,$cat_id){
        if($id == 0){
             $res = $this->db->query("SELECT COUNT(id) as totalHotels FROM orders WHERE status='paid' AND category_id=".$cat_id)->row();
        }else{
          $res = $this->db->query("SELECT COUNT(id) as totalHotels FROM orders WHERE status='paid' AND category_id=".$cat_id." AND location=".$id)->row();
        }
        return $res->totalHotels;
    }

    public function countBeaches(){
        $res = $this->db->query("SELECT COUNT(id) as totalBeaches FROM beaches")->row();
        return $res->totalBeaches;
    }

    public function updateURL($id,$url){
        $this->db->where(array('id' => $id));
        $this->db->update('locations', array('v_url'=>$url));
    }

    function locationMapDescription($id){
    	$this->db->where(array('location_id' => $id));
    	$res = $this->db->get('locations_t')->result();
    	return $res;
    }
    
    public function getPlaces(){
      $this->db->select();
      $this->db->from('placesofinterest');
      $res = $this->db->get()->result();
      return $res;
    }
}
