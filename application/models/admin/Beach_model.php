<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Beach_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getBeaches($limit = true, $lang = 'en') {
        $res = $this->db->query("SELECT b.*,bt.title,bt.description FROM beaches b LEFT OUTER JOIN beaches_t bt ON(b.id=bt.beach_id AND lang='" . $lang . "')")->result();
        if ($res) {
            return $res;
        } else {
            return FALSE;
        }
    }

    public function getBeach($id, $title = '', $lang = false) {
        $res = null;
        $where = ' ';
        if ($id != 0) {
            $where = ' WHERE b.id=' . $id;
        } else if ($title != '') {
            if ($lang != FALSE) {
                $where = " WHERE tb.title='" . str_replace('-', ' ', $title) . "'";
            } else {
                $where = " WHERE en.title='" . str_replace('-', ' ', $title) . "'";
            }
        }
        if (!$lang) {
            $res = $this->db->query("SELECT b.*, en.title as title_en, en.description as description_en, en.meta_tag as meta_en, en.meta_desc as meta_desc_en, en.meta_keywords as meta_keywords_en, en.rewrite_url as rewrite_url_en,en.address as address_en,en.bus_line as bus_line_en,en.accessibility as accessibility_en,en.privateness as private_en, ".
                            " du.title as title_du, du.description as description_du, du.meta_tag as meta_du, du.meta_desc as meta_desc_du, du.meta_keywords as meta_keywords_du, du.rewrite_url as rewrite_url_du,du.address as address_du,du.bus_line as bus_line_du,du.accessibility as accessibility_du,du.privateness as private_du, " .
                            " it.title as title_it, it.description as description_it, it.meta_tag as meta_it, it.meta_desc as meta_desc_it, it.meta_keywords as meta_keywords_it, it.rewrite_url as rewrite_url_it,it.address as address_it,it.bus_line as bus_line_it,it.accessibility as accessibility_it,it.privateness as private_it" .
                            " FROM beaches b LEFT OUTER JOIN beaches_t en ON(b.id=en.beach_id AND en.lang='en') " .
                            " LEFT OUTER JOIN beaches_t du ON(b.id=du.beach_id AND du.lang='du')" .
                            " LEFT OUTER JOIN beaches_t it ON(b.id=it.beach_id AND it.lang='it') " .
                            $where)->row();
        } else {
            $res = $this->db->query("SELECT b.*, tb.title, tb.description, tb.meta_tag as meta_".$lang.", tb.meta_desc as meta_desc_".$lang.", tb.meta_keywords as meta_keywords_".$lang.", tb.address as address, tb.bus_line as bus_line, tb.accessibility as accessibility, tb.privateness as privateness FROM beaches b LEFT OUTER JOIN beaches_t tb ON(b.id=tb.beach_id AND tb.lang='" . $lang . "') " .
                            $where)->row();
        }

        if ($res) {
            return $res;
        } else {
            return FALSE;
        }
    }

    public function getMenuList($lang = 'en') {
        $res = $this->db->query("SELECT b.*,bt.title,en.title as enTitle FROM beaches b LEFT OUTER JOIN beaches_t bt ON(b.id=bt.beach_id AND bt.lang='" . $lang . "') LEFT OUTER JOIN beaches_t en ON(b.id=en.beach_id AND en.lang='en')")->result();
        if ($res) {
            return $res;
        } else {
            return FALSE;
        }
    }

    public function insert() {
        $beach = array(
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
            'location_id'=>$this->input->post('location_id')
        );
        $this->db->insert('beaches', $beach);
        $res = $this->db->insert_id();
        $data = array(

          array('title' => $this->input->post('title_it'),
              'description' => base64_encode($this->input->post('description_it')),
              'lang' => 'it',
              'meta_tag' => $this->input->post('meta_it'),
              'meta_desc' => $this->input->post('meta_desc_it'),
              'meta_keywords' => $this->input->post('meta_keywords_it'),
              'rewrite_url' => $this->input->post('rewrite_url_it'),
              'beach_id' => $res,
              'address' => $this->input->post('address_it'),
              'bus_line' => $this->input->post('bus_line_it'),
              'accessibility' => $this->input->post('accessibility_it'),
              'privateness' => $this->input->post('private_it'),
            ),

            array('title' => $this->input->post('title_en')?$this->input->post('title_en'):$this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_en')?$this->input->post('description_en'):$this->input->post('description_it')),
                'lang' => 'en',
                'meta_tag' => $this->input->post('meta_en')?$this->input->post('meta_en'):$this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_en')?$this->input->post('meta_desc_en'):$this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_en')?$this->input->post('meta_keywords_en'):$this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_en')?$this->input->post('rewrite_url_en'):$this->input->post('rewrite_url_it'),
                'beach_id' => $res,
                'address' => $this->input->post('address_en')?$this->input->post('address_en'):$this->input->post('address_it'),
                'bus_line' => $this->input->post('bus_line_en')?$this->input->post('bus_line_en'):$this->input->post('bus_line_it'),
                'accessibility' => $this->input->post('accessibility_en')?$this->input->post('accessibility_en'):$this->input->post('accessibility_it'),
                'privateness' => $this->input->post('private_en')?$this->input->post('private_en'):$this->input->post('private_it'),

            ),

            array('title' => $this->input->post('title_du')?$this->input->post('title_du'):$this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_du')?$this->input->post('description_du'):$this->input->post('description_it')),
                'lang' => 'du',
                'meta_tag' => $this->input->post('meta_du')?$this->input->post('meta_du'):$this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_du')?$this->input->post('meta_desc_du'):$this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_du')?$this->input->post('meta_keywords_du'):$this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_du')?$this->input->post('rewrite_url_du'):$this->input->post('rewrite_url_it'),
                'beach_id' => $res,
                'address' => $this->input->post('address_du')?$this->input->post('address_du'):$this->input->post('address_it'),
                'bus_line' => $this->input->post('bus_line_du')?$this->input->post('bus_line_du'):$this->input->post('bus_line_it'),
                'accessibility' => $this->input->post('accessibility_du')?$this->input->post('accessibility_du'):$this->input->post('accessibility_it'),
                'privateness' => $this->input->post('private_du')?$this->input->post('private_du'):$this->input->post('private_it'),
            )
        );
        $this->insert_tdata($data);
        return $res;
    }

    public function insert_tdata($tdata) {
        $this->db->insert_batch('beaches_t', $tdata);
    }

    public function update($id) {
        $beach = array(
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
            'location_id'=>$this->input->post('location_id')
        );
        $this->db->where(array('id'=>$id));
        $this->db->update('beaches', $beach);
        $this->db->where(array('beach_id'=>$id));
        $this->db->delete('beaches_t');

        $data = array(

          array('title' => $this->input->post('title_it'),
              'description' => base64_encode($this->input->post('description_it')),
              'lang' => 'it',
              'meta_tag' => $this->input->post('meta_it'),
              'meta_desc' => $this->input->post('meta_desc_it'),
              'meta_keywords' => $this->input->post('meta_keywords_it'),
              'rewrite_url' => $this->input->post('rewrite_url_it'),
              'beach_id' => $id,
              'address' => $this->input->post('address_it'),
              'bus_line' => $this->input->post('bus_line_it'),
              'accessibility' => $this->input->post('accessibility_it'),
              'privateness' => $this->input->post('private_it'),
            ),

            array('title' => $this->input->post('title_en')?$this->input->post('title_en'):$this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_en')?$this->input->post('description_en'):$this->input->post('description_it')),
                'lang' => 'en',
                'meta_tag' => $this->input->post('meta_en')?$this->input->post('meta_en'):$this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_en')?$this->input->post('meta_desc_en'):$this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_en')?$this->input->post('meta_keywords_en'):$this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_en')?$this->input->post('rewrite_url_en'):$this->input->post('rewrite_url_it'),
                'beach_id' => $id,
                'address' => $this->input->post('address_en')?$this->input->post('address_en'):$this->input->post('address_it'),
                'bus_line' => $this->input->post('bus_line_en')?$this->input->post('bus_line_en'):$this->input->post('bus_line_it'),
                'accessibility' => $this->input->post('accessibility_en')?$this->input->post('accessibility_en'):$this->input->post('accessibility_it'),
                'privateness' => $this->input->post('private_en')?$this->input->post('private_en'):$this->input->post('private_it'),

            ),

            array('title' => $this->input->post('title_du')?$this->input->post('title_du'):$this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_du')?$this->input->post('description_du'):$this->input->post('description_it')),
                'lang' => 'du',
                'meta_tag' => $this->input->post('meta_du')?$this->input->post('meta_du'):$this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_du')?$this->input->post('meta_desc_du'):$this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_du')?$this->input->post('meta_keywords_du'):$this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_du')?$this->input->post('rewrite_url_du'):$this->input->post('rewrite_url_it'),
                'beach_id' => $id,
                'address' => $this->input->post('address_du')?$this->input->post('address_du'):$this->input->post('address_it'),
                'bus_line' => $this->input->post('bus_line_du')?$this->input->post('bus_line_du'):$this->input->post('bus_line_it'),
                'accessibility' => $this->input->post('accessibility_du')?$this->input->post('accessibility_du'):$this->input->post('accessibility_it'),
                'privateness' => $this->input->post('private_du')?$this->input->post('private_du'):$this->input->post('private_it'),
            )
        );
        
        $this->insert_tdata($data);
        return true;
    }

    public function update_cover($id,$cover){
        $this->db->where(array('id'=>$id));
        $this->db->update('beaches',array('p_cover'=>$cover));
        return true;
    }

    public function update_thumbnail($id,$thumbnail){
        $this->db->where(array('id'=>$id));
        $this->db->update('beaches',array('p_thumbnail'=>$thumbnail));
        return true;
    }

    public function delete($id) {
        $this->db->where(array('beach_id' => $id));
        $this->db->delete('beaches_t');
        $this->db->where(array('id' => $id));
        $this->db->delete('beaches');
        return true;
    }

    public function getLocation($beach_id, $lang = "en"){
       $res = $this->db->query("SELECT l.* FROM beaches b LEFT OUTER JOIN locations_t l ON(b.location_id = l.location_id AND l.lang='" . $lang . "') WHERE b.id='".$beach_id."'")->row();
        if ($res) {
            return $res;
        } else {
            return FALSE;
        }
    }

    function beachMapDescription($id){
    	$this->db->where(array('beach_id' => $id));
    	$res = $this->db->get('beaches_t')->result();
    	return $res;
    }

}
