<?php



if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tag_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function getAll(){
        $res = $this->db->select('*')
                ->from('activity_tag')
                ->get()
                ->result();
        //echo "<pre>" ; print_r($res); die;
        return($res);
    }
    public function get($id){
        $res = $this->db->select('*')
                ->from('activity_tag')
                ->where('id', $id)
                ->get()
                ->row();
        //echo "<pre>" ; print_r($res); die;
        return($res);
    }
    public function update($id, $tag){
        $this->db->where(array('id'=>$id));
        $this->db->update('activity_tag',$tag);
        return true;
    }
    public function add($tag){
        $this->db->insert('activity_tag', $tag);
        $res = $this->db->insert_id();
        return $res;
    }
    public function delete($id) {
        $this->db->where(array('id' => $id));
        $this->db->delete('activity_tag');
        return true;
    }

}
