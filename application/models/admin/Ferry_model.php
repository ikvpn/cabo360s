<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ferry_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function addDestination($destination){
        $this->db->insert('ferry_destination', $destination);
    }

    public function getDestination($name){
      $this->db->from('ferry_destination');
      $this->db->where('url_rewrite', $name);
      $res = $this->db->get();
      return $res->row_array();
    }

    public function addRoute($route){
      $this->db->insert('ferry_routes', $route);
    }

    public function findRoutes(){
      $this->db->select('*');
      $this->db->from('ferry_routes');
      $this->db->order_by('external_id', 'asc');
      $res = $this->db->get();
      return $res->result();
    }

    public function addDeparture($departures){
      $this->db->insert('ferry_departures', $departures);
    }

    public function cleanTable(){
      $query = 'TRUNCATE TABLE ferry_departures';
      $check = $this->db->simple_query($query);

      if (!$check) {
         $error = $this->db->error();
      }
    }

    public function findRoutesWithDepartures(){
      $this->db->select('*');
      $this->db->from('ferry_routes');
      $this->db->join('ferry_departures', 'ferry_routes.id = ferry_departures.ferry_routes_id');
      $res = $this->db->get();
      return $res->result();
    }

    public function findDeparturesByDate($date, $route){
      $this->db->select('*');
      $this->db->from('ferry_departures');
      $this->db->where('datapartenza', $date);
      $this->db->join('ferry_routes', 'ferry_departures.ferry_routes_id = ferry_routes.id');
      $this->db->where('descrizione', $route);
      $res = $this->db->get();
      return $res->result();
    }

    public function findByName($route){
      $this->db->select('*');
      $this->db->from('ferry_routes');
      $this->db->where('descrizione', $route);
      $res = $this->db->get();
      return $res->row_array();
    }

    public function findById($id){
      $this->db->select('*');
      $this->db->from('ferry_routes');
      $this->db->where('external_id', $id);
      $res = $this->db->get();
      return $res->row_array(); 
    }


}
