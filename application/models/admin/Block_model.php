<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Block_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getBlocks($limit = 10, $lang = 'en') {

        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $start = 0;
        if ($page > 0) {
            $start = ( --$page) * 10;
        }
        if($limit !== false ){
        $res = $this->db->select("b.*,bt.title,bt.description,bt.meta_tag, bt.meta_desc,  bt.meta_keywords,bt.rewrite_url,bt.link")
                ->from('blocks AS b')
                ->join('blocks_t AS bt', "b.id=bt.block_id AND lang='" . $lang . "'", 'LEFT OUTER')
                ->order_by('date', 'ASC')
                ->limit($limit, $start)
                ->get()
                ->result();
        }else{
            $res = $this->db->select("b.*,bt.title,bt.description,bt.meta_tag, bt.meta_desc,  bt.meta_keywords,bt.rewrite_url,bt.link")
                ->from('blocks AS b')
                ->join('blocks_t AS bt', "b.id=bt.block_id AND lang='" . $lang . "'", 'LEFT OUTER')
                ->order_by('date', 'ASC')
                ->get()
                ->result();
        }

		//$res = $this->db->query("SELECT b.*,bt.title,bt.description,bt.meta_tag, bt.meta_desc,  bt.meta_keywords,bt.rewrite_url FROM blocks b LEFT OUTER JOIN blocks_t pt ON(b.id=bt.block_id AND lang='" . $lang . "')")->result();
        if ($res) {
            return $res;
        } else {
            return FALSE;
        }
    }

    public function totalBlocks() {
        $res = $this->db->select('id')
                ->from('blocks')
                ->get();
        return $res->num_rows();
    }

    public function getBlock($id = 0, $title = '', $lang = false) {
        $res = null;
        $where = ' ';
        if ($id != 0) {
            $where = ' WHERE b.id=' . $id;
        } else if ($title != '') {
            if ($lang != FALSE) {
                $where = " WHERE tl.title='" . str_replace('_', ' ', $title) . "'";
            } else {
                $where = " WHERE en.title='" . str_replace('_', ' ', $title) . "'";
            }
        }
        if (!$lang) {
            $res = $this->db->query("SELECT b.*, en.title as title_en, en.meta_tag as meta_tag_en, en.meta_desc as meta_desc_en,  en.meta_keywords as meta_keywords_en, en.rewrite_url as rewrite_url_en , en.description as description_en,en.link as link_en ," .
                            " du.title as title_du, du.meta_tag as meta_tag_du, du.meta_desc as meta_desc_du,  du.meta_keywords as meta_keywords_du, du.rewrite_url as rewrite_url_du , du.description as description_du, du.link as link_du, " .
                            " it.title as title_it, it.meta_tag as meta_tag_it, it.meta_desc as meta_desc_it,  it.meta_keywords as meta_keywords_it, it.rewrite_url as rewrite_url_it , it.description description_it, it.link as link_it" .
                            " FROM blocks b LEFT OUTER JOIN blocks_t en ON(b.id=en.block_id AND en.lang='en') " .
                            " LEFT OUTER JOIN blocks_t du ON(b.id=du.block_id AND du.lang='du')" .
                            " LEFT OUTER JOIN blocks_t it ON(b.id=it.block_id AND it.lang='it') " .
                            $where)->row();
        } else {
            $res = $this->db->query("SELECT b.*, tb.title, tb.description, tb.meta_tag as meta_tag, tb.meta_desc as meta_desc,  tb.meta_keywords as meta_keywords, tb.rewrite_url as rewrite_url,tb.link as link" .
                            " FROM blocks b LEFT OUTER JOIN blocks_t tp ON(b.id=tb.block_id AND tb.lang='" . $lang . "') " .
                            $where)->row();
        }
		if ($res) {
            return $res;
        } else {
            return FALSE;
        }
    }

    public function insert($file) {
        $block = array(
            'status' => 'published',
            'p_image' => $file
        );
        $this->db->insert('blocks', $block);
        $res = $this->db->insert_id();
        $data = array(
            array('title' => $this->input->post('title_en') ? $this->input->post('title_en') : $this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_en')? $this->input->post('description_en') : $this->input->post('description_it')),
                'lang' => 'en',
                'block_id' => $res,
               /* 'meta_tag' => $this->input->post('meta_en'),
                'meta_desc' => $this->input->post('meta_desc_en'),
                'meta_keywords' => $this->input->post('meta_keywords_en'),
                'rewrite_url' => $this->input->post('rewrite_url_en'),*/
				'link' => $this->input->post('link_en')? $this->input->post('link_en') : $this->input->post('link_it'),
            ),
            array('title' => $this->input->post('title_du') ? $this->input->post('title_du') : $this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_du') ? $this->input->post('description_du') : $this->input->post('description_it')),
                'lang' => 'du',
                'block_id' => $res,
               /* 'meta_tag' => $this->input->post('meta_du') ? $this->input->post('meta_du') : $this->input->post('meta_en'),
                'meta_desc' => $this->input->post('meta_desc_du') ? $this->input->post('meta_desc_du') : $this->input->post('meta_desc_en'),
                'meta_keywords' => $this->input->post('meta_keywords_du') ? $this->input->post('meta_keywords_du') : $this->input->post('meta_keywords_en'),
                'rewrite_url' => $this->input->post('rewrite_url_du') ? $this->input->post('rewrite_url_du') : $this->input->post('rewrite_url_en'),
				*/'link' => $this->input->post('link_du') ? $this->input->post('link_du') : $this->input->post('link_it'),
            ),
            array('title' => $this->input->post('title_it') ,
                'description' => base64_encode($this->input->post('description_it') ),
                'lang' => 'it',
                'block_id' => $res,
               /* 'meta_tag' => $this->input->post('meta_it') ? $this->input->post('meta_it') : $this->input->post('meta_en'),
                'meta_desc' => $this->input->post('meta_desc_it') ? $this->input->post('meta_desc_it') : $this->input->post('meta_desc_en'),
                'meta_keywords' => $this->input->post('meta_keywords_it') ? $this->input->post('meta_keywords_it') : $this->input->post('meta_keywords_en'),
                'rewrite_url' => $this->input->post('rewrite_url_it') ? $this->input->post('rewrite_url_it') : $this->input->post('rewrite_url_en'),
				*/'link' => $this->input->post('link_it') ,
            )
        );
        $this->insert_tdata($data);
        return $res;
    }

    public function insert_tdata($tdata) {
        $this->db->insert_batch('blocks_t', $tdata);
    }

    public function update($file) {
        if ($file != FALSE) {
            $this->db->where(array('id' => $this->input->post('id')));
            $this->db->update('blocks', array('p_image' => $file));
        }
        $this->db->where(array('lang' => 'en', 'block_id' => $this->input->post('id')));
        $this->db->update('blocks_t', array('title' => $this->input->post('title_en')? $this->input->post('title_en') : $this->input->post('title_it'),
            'description' => base64_encode($this->input->post('description_en')? $this->input->post('description_en') : $this->input->post('description_it')),
           /* 'meta_tag' => $this->input->post('meta_en'),
            'meta_desc' => $this->input->post('meta_desc_en'),
            'meta_keywords' => $this->input->post('meta_keywords_en'),
            'rewrite_url' => $this->input->post('rewrite_url_en'),
			*/'link' => $this->input->post('link_en') ? $this->input->post('link_en') : $this->input->post('link_it'),
        ));

        $this->db->where(array('lang' => 'du', 'block_id' => $this->input->post('id')));
        $this->db->update('blocks_t', array('title' => $this->input->post('title_du') ? $this->input->post('title_du') : $this->input->post('title_it'),
            'description' => base64_encode($this->input->post('description_du') ? $this->input->post('description_du') : $this->input->post('description_it')),
           /* 'meta_tag' => $this->input->post('meta_du') ? $this->input->post('meta_du') : $this->input->post('meta_en'),
            'meta_desc' => $this->input->post('meta_desc_du') ? $this->input->post('meta_desc_du') : $this->input->post('meta_desc_en'),
            'meta_keywords' => $this->input->post('meta_keywords_du') ? $this->input->post('meta_keywords_du') : $this->input->post('meta_keywords_en'),
            'rewrite_url' => $this->input->post('rewrite_url_du') ? $this->input->post('rewrite_url_du') : $this->input->post('rewrite_url_en'),
			*/'link' => $this->input->post('link_du') ? $this->input->post('link_du') : $this->input->post('link_it'),
        ));

        $this->db->where(array('lang' => 'it', 'block_id' => $this->input->post('id')));
        $this->db->update('blocks_t', array('title' => $this->input->post('title_it') ,
            'description' => base64_encode($this->input->post('description_it') ),
           /* 'meta_tag' => $this->input->post('meta_it') ? $this->input->post('meta_it') : $this->input->post('meta_en'),
            'meta_desc' => $this->input->post('meta_desc_it') ? $this->input->post('meta_desc_it') : $this->input->post('meta_desc_en'),
            'meta_keywords' => $this->input->post('meta_keywords_it') ? $this->input->post('meta_keywords_it') : $this->input->post('meta_keywords_en'),
            'rewrite_url' => $this->input->post('rewrite_url_it') ? $this->input->post('rewrite_url_it') : $this->input->post('rewrite_url_en'),
			*/'link' => $this->input->post('link_it')
        ));
    }

    public function delete($id) {
        $this->db->where(array('block_id' => $id));
        $this->db->delete('blocks_t');
        $this->db->where(array('id' => $id));
        $this->db->delete('blocks');
        return true;
    }

}
