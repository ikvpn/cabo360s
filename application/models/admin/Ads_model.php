<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */


if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ads_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function get($name){
        $res = $this->db->select('*')
                ->from('ads')
                ->where(array('name'=>$name))
                ->get()
                ->row();
        return($res);
    }
    public function getAll(){
        $res = $this->db->select('*')
                ->from('ads')
                ->get()
                ->result();
        return($res);
    }
    public function update($id, $ads){
        $this->db->where(array('id'=>$id));
        $this->db->update('ads',$ads);
        return true;
    }
    
}
