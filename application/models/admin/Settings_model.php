<?php



/* 

 * Author: Innam Hunzai

 * Email: innamhunzai@gmail.com 

 * Project: Visit Procida

 * Version: 1.0

 * File: 

 * Description:

 */



if (!defined('BASEPATH')) {

    exit('No direct script access allowed');

}



class Settings_model extends CI_Model {

    public $api_username;

    public $api_password;

    public $api_key;

    public $instagram;

    public $radius;

    public $meta_tag_en;

    public $meta_desc_en;

    public $meta_keywords_en;

    public $meta_tag_du;

    public $meta_desc_du;

    public $meta_keywords_du;

    public $meta_tag_it;

    public $meta_desc_it;

    public $meta_keywords_it;

    

    public function __construct() {

        parent::__construct();

    }

    

    public function load(){

        $res = $this->db->select('*')->from('settings')->get()->row();

        if($res){

            $this->api_username = $res->api_username;

            $this->api_password = $res->api_password;

            $this->api_key = $res->api_key;

            $this->instagram = $res->instagram;

            $this->radius = $res->radius;

        }else{

            $this->api_username = $this->api_key = $this->api_password = NULL;

        }

    }

    

    public function getsettings(){

        $this->db->select('*');

        $this->db->from('settings');

        $this->db->where(array('id'=>'1'));

        $res = $this->db->get()->row();

        return $res;

    }

    

    public function savepaypal(){

        $data = array('api_username'=>$this->input->post('api_username'),'api_password'=>$this->input->post('api_password'),'api_key'=>$this->input->post('api_key'),'offline_payment'=>$this->input->post('offline_payment'));

        $this->db->where(array('id'=>'1'));

        $this->db->update('settings',$data);

    }

    

    public function saveinstagram(){

        $data = array('instagram'=>$this->input->post('instagram'));

        $this->db->where(array('id'=>'1'));

        $this->db->update('settings',$data);

    }

    public function savemetaSettings(){

        $data = array('meta_tag_en'=>$this->input->post('meta_tag_en'),

                      'meta_tag_du'=>$this->input->post('meta_tag_du')?$this->input->post('meta_tag_du'):$this->input->post('meta_tag_en'),

                      'meta_tag_it'=>$this->input->post('meta_tag_it')?$this->input->post('meta_tag_it'):$this->input->post('meta_tag_en'),

                      'meta_keywords_en'=>$this->input->post('meta_keywords_en'),

                      'meta_keywords_du'=>$this->input->post('meta_keywords_du')?$this->input->post('meta_keywords_du'):$this->input->post('meta_keywords_en'),

                      'meta_keywords_it'=>$this->input->post('meta_keywords_it')?$this->input->post('meta_keywords_it'):$this->input->post('meta_keywords_en'),

                      'meta_desc_en'=>$this->input->post('meta_desc_en'),

                      'meta_desc_du'=>$this->input->post('meta_desc_du')?$this->input->post('meta_desc_du'):$this->input->post('meta_desc_en'),

                      'meta_desc_it'=>$this->input->post('meta_desc_it')?$this->input->post('meta_desc_it'):$this->input->post('meta_desc_en')

            );

        $this->db->where(array('id'=>'1'));

        $this->db->update('settings',$data);

    }

    public function saveradius($radius){

        $data = array('radius'=>$radius);

        $this->db->where(array('id'=>'1'));

        $this->db->update('settings',$data);

    }

    public function resetpassword(){

       $check = array('email'=>$this->session->userdata('email'),

                   'password'=>md5($this->input->post('password')));

       $user = $this->db->select("id")->from('users')->where($check)->get()->row();

       if($user){

           $this->db->where(array('email'=>$check['email']));

           $this->db->update('users',array('password'=>$check['password']));

           return true;

       }else{

           return "Current password is invalid";

       }

        

    }

}