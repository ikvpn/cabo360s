<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Users extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function login($email, $password) {
        $res = $this->db->select('id,full_name,usertype')
                        ->from('users')
                        ->where(array('email' => $email, 'password' => md5($password),'usertype'=>'admin'))->get()->row();

        if ($res) {
            $user = Array(
            	'id' => $res->id,
                'email' => $email,
                'usertype' => $res->usertype,
                'full_name' => $res->full_name);

            
            $this->session->set_userdata($user);
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
