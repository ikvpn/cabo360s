<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */

if(!defined('BASEPATH')){exit('No direct script access is allowed');}

class Dashboard_model extends CI_Model{
    public $locations;
    public $customers;
    public $registeredBusinesses;
    public $posts;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function load(){
        $row = $this->db->select('COUNT(id) as Total')->from('locations')->get()->row();
        $this->locations = $row->Total;
        
        $row = $this->db->select('COUNT(id) as Total')->from('customers')->get()->row();
        $this->customers = $row->Total;
        
        $row = $this->db->select('COUNT(id) as Total')->from('orders')->get()->row();
        $this->businesses = $row->Total;
        
        $row = $this->db->select('COUNT(id) as Total')->from('orders')->where(array('status'=>'new'))->get()->row();
        $this->registeredBusinesses = $row->Total;
        
        $row = $this->db->select('COUNT(id) as Total')->from('posts')->get()->row();
        $this->posts = $row->Total;
      
    }
}