<?php



if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Activity_service_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function getAll(){
        $res = $this->db->select('*')
                ->from('activity_services')
                ->get()
                ->result();
        //echo "<pre>" ; print_r($res); die;
        return($res);
    }
    public function get($id){
        $res = $this->db->select('*')
                ->from('activity_services')
                ->where('id', $id)
                ->get()
                ->row();
        //echo "<pre>" ; print_r($res); die;
        return($res);
    }
    public function update($id, $tag){
        $this->db->where(array('id'=>$id));
        $this->db->update('activity_services',$tag);
        return true;
    }
    public function add($tag){
      //echo "<pre>"; print_r($tag); die;
        $this->db->insert('activity_services', $tag);
        $res = $this->db->insert_id();

        return $res;
    }
    public function delete($id) {
        $this->db->where(array('id' => $id));
        $this->db->delete('activity_services');
        return true;
    }

}
