<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Blog_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getPosts($limit = 10, $lang = 'en') {
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $start = 0;
        if ($page > 0) {
            $start = ( --$page) * 10;
        }
        if($limit !== false ){
        $res = $this->db->select("p.*,pt.title,pt.description,pt.meta_tag, pt.meta_desc,  pt.meta_keywords,pt.rewrite_url")
                ->from('posts AS p')
                ->join('posts_t AS pt', "p.id=pt.post_id AND lang='" . $lang . "'", 'LEFT OUTER')
                ->order_by('date', 'DESC')
                ->limit($limit, $start)
                ->get()
                ->result();
        }else{
            $res = $this->db->select("p.*,pt.title,pt.description,pt.meta_tag, pt.meta_desc,  pt.meta_keywords,pt.rewrite_url")
                ->from('posts AS p')
                ->join('posts_t AS pt', "p.id=pt.post_id AND lang='" . $lang . "'", 'LEFT OUTER')
                ->order_by('date', 'DESC')
                ->get()
                ->result();
        }
        //$res = $this->db->query("SELECT p.*,pt.title,pt.description,pt.meta_tag, pt.meta_desc,  pt.meta_keywords,pt.rewrite_url FROM posts p LEFT OUTER JOIN posts_t pt ON(p.id=pt.post_id AND lang='" . $lang . "')")->result();
        if ($res) {
            return $res;
        } else {
            return [];
        }
    }

    public function totalPosts() {
        $res = $this->db->select('id')
                ->from('posts')
                ->get();
        return $res->num_rows();
    }

    public function getPost($id = 0, $title = '', $lang = false) {
        $res = null;
        $where = ' ';
        if ($id != 0) {
            $where = ' WHERE p.id=' . $id;
        } else if ($title != '') {
            if ($lang != FALSE) {
                $where = " WHERE tl.title='" . str_replace('_', ' ', $title) . "'";
            } else {
                $where = " WHERE en.title='" . str_replace('_', ' ', $title) . "'";
            }
        }
        if (!$lang) {
            $res = $this->db->query("SELECT p.*, en.title as title_en, en.meta_tag as meta_tag_en, en.meta_desc as meta_desc_en,  en.meta_keywords as meta_keywords_en, en.rewrite_url as rewrite_url_en , en.description as description_en, " .
                            " du.title as title_du, du.meta_tag as meta_tag_du, du.meta_desc as meta_desc_du,  du.meta_keywords as meta_keywords_du, du.rewrite_url as rewrite_url_du , du.description as description_du, " .
                            " it.title as title_it, it.meta_tag as meta_tag_it, it.meta_desc as meta_desc_it,  it.meta_keywords as meta_keywords_it, it.rewrite_url as rewrite_url_it , it.description description_it " .
                            " FROM posts p LEFT OUTER JOIN posts_t en ON(p.id=en.post_id AND en.lang='en') " .
                            " LEFT OUTER JOIN posts_t du ON(p.id=du.post_id AND du.lang='du')" .
                            " LEFT OUTER JOIN posts_t it ON(p.id=it.post_id AND it.lang='it') " .
                            $where)->row();
        } else {
            $res = $this->db->query("SELECT p.*, tp.title, tp.description, tp.meta_tag as meta_tag, tp.meta_desc as meta_desc,  tp.meta_keywords as meta_keywords, tp.rewrite_url as rewrite_url" .
                            " FROM posts p LEFT OUTER JOIN posts_t tp ON(p.id=tp.post_id AND tp.lang='" . $lang . "') " .
                            $where)->row();
        }
        if ($res) {
            return $res;
        } else {
            return FALSE;
        }
    }

    public function insert($file) {
        $post = array('author' => $this->session->userdata('id'),
            'status' => 'published',
            'p_image' => $file,
            'category' => 0
        );
        $this->db->insert('posts', $post);
        $res = $this->db->insert_id();
        $data = array(
            array('title' => $this->input->post('title_en') ? $this->input->post('title_en') : $this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_en') ? $this->input->post('description_en') : $this->input->post('description_it')),
                'lang' => 'en',
                'post_id' => $res,
                'meta_tag' => $this->input->post('meta_en') ? $this->input->post('meta_en') : $this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_en')? $this->input->post('meta_desc_en') : $this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_en')? $this->input->post('meta_keywords_en') : $this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_en') ? $this->input->post('rewrite_url_en') : $this->input->post('rewrite_url_it')
            ),
            array('title' => $this->input->post('title_du') ? $this->input->post('title_du') : $this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_du') ? $this->input->post('description_du') : $this->input->post('description_it')),
                'lang' => 'du',
                'post_id' => $res,
                'meta_tag' => $this->input->post('meta_du') ? $this->input->post('meta_du') : $this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_du') ? $this->input->post('meta_desc_du') : $this->input->post('meta_desc_it'),
                'meta_keywords' => $this->input->post('meta_keywords_du') ? $this->input->post('meta_keywords_du') : $this->input->post('meta_keywords_it'),
                'rewrite_url' => $this->input->post('rewrite_url_du') ? $this->input->post('rewrite_url_du') : $this->input->post('rewrite_url_it')
            ),
            array('title' => $this->input->post('title_it'),
                'description' => base64_encode($this->input->post('description_it')),
                'lang' => 'it',
                'post_id' => $res,
                'meta_tag' => $this->input->post('meta_it'),
                'meta_desc' => $this->input->post('meta_desc_it') ,
                'meta_keywords' => $this->input->post('meta_keywords_it') ,
                'rewrite_url' => $this->input->post('rewrite_url_it')
            )
        );
        $this->insert_tdata($data);
        return $res;
    }

    public function insert_tdata($tdata) {
        $this->db->insert_batch('posts_t', $tdata);
    }

    public function update($file) {
        if ($file != FALSE) {
            $this->db->where(array('id' => $this->input->post('id')));
            $this->db->update('posts', array('p_image' => $file));
        }
        $this->db->where(array('lang' => 'en', 'post_id' => $this->input->post('id')));
        $this->db->update('posts_t', array('title' => $this->input->post('title_en')? $this->input->post('title_en') : $this->input->post('title_it'),
            'description' => base64_encode($this->input->post('description_en') ? $this->input->post('description_en') : $this->input->post('description_it')),
            'meta_tag' => $this->input->post('meta_en') ? $this->input->post('meta_en') : $this->input->post('meta_it'),
            'meta_desc' => $this->input->post('meta_desc_en')? $this->input->post('meta_desc_en') : $this->input->post('meta_desc_it'),
            'meta_keywords' => $this->input->post('meta_keywords_en')? $this->input->post('meta_keywords_en') : $this->input->post('meta_keywords_it'),
            'rewrite_url' => $this->input->post('rewrite_url_en')? $this->input->post('rewrite_url_en') : $this->input->post('rewrite_url_it'),
        ));

        $this->db->where(array('lang' => 'du', 'post_id' => $this->input->post('id')));
        $this->db->update('posts_t', array('title' => $this->input->post('title_du') ? $this->input->post('title_du') : $this->input->post('title_it'),
            'description' => base64_encode($this->input->post('description_du') ? $this->input->post('description_du') : $this->input->post('description_it')),
            'meta_tag' => $this->input->post('meta_du') ? $this->input->post('meta_du') : $this->input->post('meta_it'),
            'meta_desc' => $this->input->post('meta_desc_du') ? $this->input->post('meta_desc_du') : $this->input->post('meta_desc_it'),
            'meta_keywords' => $this->input->post('meta_keywords_du') ? $this->input->post('meta_keywords_du') : $this->input->post('meta_keywords_it'),
            'rewrite_url' => $this->input->post('rewrite_url_du') ? $this->input->post('rewrite_url_du') : $this->input->post('rewrite_url_it'),
        ));

        $this->db->where(array('lang' => 'it', 'post_id' => $this->input->post('id')));
        $this->db->update('posts_t', array('title' => $this->input->post('title_it') ,
            'description' => base64_encode($this->input->post('description_it')),
            'meta_tag' => $this->input->post('meta_it'),
            'meta_desc' => $this->input->post('meta_desc_it') ,
            'meta_keywords' => $this->input->post('meta_keywords_it') ,
            'rewrite_url' => $this->input->post('rewrite_url_it')
        ));
    }

    public function delete($id) {
        $this->db->where(array('post_id' => $id));
        $this->db->delete('posts_t');
        $this->db->where(array('id' => $id));
        $this->db->delete('posts');
        return true;
    }

}
