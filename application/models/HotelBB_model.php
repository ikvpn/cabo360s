<?php

if (!defined('BASEPATH')) {
    exit('No direct script access is allowed');
}

class HotelBB_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function saveNewHotel($hotel){
      $this->db->insert('hotel_bb', $hotel);
      return $this->db->insert_id();
    }

    function saveNewHotelImages($newHotelImage) {
      $this->db->insert('hotel_bb_images', $newHotelImage);
      return $this->db->insert_id();
    }

    function deleteTable($table) {
      $query = 'TRUNCATE TABLE '.$table;
      $check = $this->db->simple_query($query);

      if (!$check) {
         $error = $this->db->error(); // Has keys 'code' and 'message'
      }
    }

    function listAll(){
      $this->db->select('*');
      $this->db->from('hotel_bb');
      return $this->db->get()->result();
    }

   function associate($data){
      $hotelBBid = array('hotel_bb_id' => intval($data['hotel_bb_id']));
      $this->db->where('hotels.order_id =', $data['id']);
      $response = $this->db->update('hotels', $hotelBBid);
      if($response) {
   		    return true;
   		} else {
   		    return false;
   		}
    }

    function getHotelInfo($id){
      $this->db->select('*');
      $this->db->from('hotels');
      $this->db->where('hotels.hotel_bb_id = ', $id);
      $this->db->join('hotel_bb', 'partner_id = hotels.hotel_bb_id');
      return $this->db->get()->row();
    }

    function getHotelImages($id){
      $this->db->select('*');
      $this->db->from('hotel_bb_images');
      $this->db->where('hotel_bb_images.partner_id = ', $id);
      return $this->db->get()->result();
    }

    function getHbbInfo($id){
      $this->db->select('*');
      $this->db->from('hotel_bb');
      $this->db->where('hotel_bb.partner_id = ', $id);
      return $this->db->get()->row();
    }

    function findOne($page_id){
      $this->db->select('*');
      $this->db->from('hotels');
      $this->db->where('id = ', $page_id);
      return $this->db->get()->row();
    }

    public function insertRoomInfo($data){

      $this->db->where('hotel_code',$data['hotel_code']);
      $this->db->where('room_code',$data['room_code']);
      $this->db->where('tratt_code',$data['tratt_code']);

      $results = $this->db->get('roomInfo');

      if ($results->num_rows() > 0 ) {

        $results = $results->row();

        $id = $results->id;

        $this->db->where('id', $id);
        $this->db->where('hotel_code',$data['hotel_code']);
        $this->db->where('room_code',$data['room_code']);
        $this->db->where('tratt_code',$data['tratt_code']);

        $this->db->update('roomInfo',$data);

        return $id;

     } else {

        $this->db->insert('roomInfo', $data);
        return $this->db->insert_id();
     }
    }

    public function getRoomInfo($roomId){
      $this->db->select('hotel_code, room_code, adults, quota, tratt_code, tratt_name');
      $this->db->from('roomInfo');
      $this->db->where('id = ', $roomId);
      return $this->db->get()->row();
    }
}
