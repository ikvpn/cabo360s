<?php

if (!defined('BASEPATH')) {
    exit('No direct script access is allowed');
}

class Cart_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert($item){

      $this->db->where('cookieId',$item['cookieId']);
      $this->db->where('roomInfoId',$item['roomInfoId']);
      $this->db->where('serviceId',$item['serviceId']);
      $this->db->where('serviceType',$item['serviceType']);
      $this->db->where('checkIn',$item['checkIn']);

      $results = $this->db->get('cart');

      if ( $results->num_rows() > 0 ) {
        $this->db->where('cookieId',$item['cookieId']);
        $this->db->where('roomInfoId',$item['roomInfoId']);
        $this->db->where('serviceId',$item['serviceId']);
        $this->db->where('serviceType',$item['serviceType']);
        $this->db->where('checkIn',$item['checkIn']);
        $this->db->update('cart',$item);
     } else {
        $this->db->insert('cart', $item);
     }
      return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function getUserCart($cartCookie, $type){
      $this->db->select('*');
      $this->db->from('cart');
      $this->db->where('cookieId', $cartCookie);
      if($type != ''){
        $this->db->where('serviceType', $type);
      }
      $results = $this->db->get()->result();

      foreach ($results as $key => $item) {
        # code...
        if($item->serviceType == 'hotel'){
          $hotelID = $item->serviceId;
          $hotelName = $this->getHotelBBName($hotelID);

          $item->hotelName = $hotelName->name;
        }
        if($item->serviceType == 'tour'){
          $item->info = $this->getTour($item->serviceId);
        }
      }

      return $results;
    }

    public function checkExistElementInCart($type, $cookieId){
      $this->db->select('*');
      $this->db->from('cart');
      $this->db->where('cookieId', $cookieId);
      $this->db->where('serviceType', $type);
      $results = $this->db->get()->result();
      if(count($results) > 0){
        return true;
      }
      else{
        return false;
      }
    }

    public function getTour($tourId){
      $this->db->select('*');
      $this->db->from('tours');
      $this->db->where('id', $tourId);
      $res = $this->db->get('')->row();
      return $res;
    }

    public function deleteExtra($id, $cookieId) {
      $item = array('extraId' => NULL, 'extraType' => NULL, 'extraQuantity' => NULL, 'extraPrice' => NULL);
      $array = array('cookieId' => $cookieId, 'extraId' => $id);
      $this->db->select('*');
      $this->db->where($array);
      $this->db->update('cart', $item); // check if update multiple rows or just one.
    }

    public function deleteRow($id) {
      $array = array('id' => $id);
      $this->db->select('*');
      $this->db->where($array);
      $this->db->delete('cart');
      return ($this->db->affected_rows() != 1) ? false : true;
    }


    public function delete($cookieId) {
       $this->db->where(array('cookieId' => $cookieId));
       $this->db->delete('cart');
       return true;
   }

    public function getHotelBBName($id){
      $this->db->select('name');
      $this->db->where('partner_id', $id);
      return $this->db->get('hotel_bb')->row();
    }
}
