<?php

if (!defined("BASEPATH")) {
    exit("No direct script access is allowed");
}

class Tour_model extends CI_Model {

    public $id;
    public $itTitle;
    public $enTitle;
    public $categories;
    public $locationId;
    public $lat;
    public $long;
    public $dateStart;
    public $dateEnd;
    public $daysAvailability;
    public $durationDay;
    public $durationHours;
    public $durationMin;
    public $sessionHour1;
    public $sessionHour2;
    public $sessionHour3;
    public $priceUnit;
    public $price;
    // public $discountAdult;
    // public $discountChildren;
    public $itDescription;
    public $itServicesIncluded;
    public $itServicesNotIncluded;
    public $itPolicy;
    public $enDescription;
    public $enServicesIncluded;
    public $enServicesNotIncluded;
    public $enPolicy;
    public $customerId;
    public $instagramWidget;
    public $type;
    public $thumbnail;
    public $cover;
    public $slug;
    public $businessPageId;
    public $orderId;
    public $fixedPrice;
    public $maxParticipants;
    public $childPrice;

    public function __construct() {
        parent::__construct();
    }

    public function load($id, $customerId) {
        $row = $this->db->select('*')->from('tours')->where(array('id' => $id, 'customerId' => $customerId))->get()->row();
        if ($row) {
            $this->id = $row->id;
            $this->itTitle = $row->itTitle;
            $this->enTitle = $row->enTitle;
            $this->categories = $row->categories;
            $this->locationId = $row->locationId;
            $this->lat = $row->lat;
            $this->long = $row->long;
            $this->dateStart = $row->dateStart;
            $this->dateEnd = $row->dateEnd;
            $this->daysAvailability = $row->daysAvailability;
            $this->durationDay = $row->durationDay;
            $this->durationHours = $row->durationHours;
            $this->durationMin = $row->durationMin;
            $this->sessionHour1 = $row->sessionHour1;
            $this->sessionHour2 = $row->sessionHour2;
            $this->sessionHour3 = $row->sessionHour3;
            $this->priceUnit = $row->priceUnit;
            $this->price = $row->price;
            // $this->discountAdult = $row->discountAdult;
            // $this->discountChildren = $row->discountChildren;
            $this->itDescription = base64_decode($row->itDescription);
            $this->itServicesIncluded = base64_decode($row->itServicesIncluded);
            $this->itServicesNotIncluded = base64_decode($row->itServicesNotIncluded);
            $this->itPolicy = base64_decode($row->itPolicy);
            $this->enDescription = base64_decode($row->enDescription);
            $this->enServicesIncluded = base64_decode($row->enServicesIncluded);
            $this->enServicesNotIncluded = base64_decode($row->enServicesNotIncluded);
            $this->enPolicy = base64_decode($row->enPolicy);
            $this->customerId = $row->customerId;
            $this->thumbnail = $row->thumbnail;
            $this->cover = $row->cover;
            $this->instagramWidget = $row->instagramWidget;
            $this->type = $row->type;
            $this->slug = $row->slug;
            $this->businessPageId = $row->businessPageId;
            $this->orderId = $row->orderId;
            $this->fixedPrice = $row->fixedPrice;
            $this->maxParticipants = $row->maxParticipants;
            $this->childPrice = $row->childPrice;
        } else {
            $this->id = $this->itTitle = $this->enTitle = $this->categories = $this->locationId = $this->lat = $this->long = $this->dateStart = $this->slug = $this->businessPageId = $this->orderId = NULL;
            $this->dateEnd = $this->daysAvailability = $this->durationDay = $this->durationHours = $this->durationMin = $this->sessionHour1 = $this->sessionHour2 = $this->enServicesIncluded = NULL;
            $this->sessionHour3 = $this->priceUnit = $this->price = $this->discountAdult = $this->discountChildren = $this->itDescription = $this->enDescription = $this->itServicesIncluded = $this->fixedPrice = $this->maxParticipants = $this->childPrice = NULL;
            $this->itServicesNotIncluded = $this->enServicesNotIncluded = $this->thumbnail = $this->cover = $this->itPolicy = $this->enPolicy = $this->customerId = $this->instagramWidget = $this->type = NULL;
        }
    }

    public function getAllTourOfCustomer($id) {
        return $this->db->select('*')->from('tours')->where(array('customerId' => $id))->get()->result();
    }
    public function getTour($slug){
      $res = $this->db->select('t.*, lt.title as locationName')
              ->from('tours as t')
              ->where(array('slug' => $slug))
              ->join('locations_t AS lt', 'lt.location_id=t.locationId', 'LEFT OUTER')
              ->get()
              ->row();
      $order = $this->db->select('*')->from('orders')->where(array('id' => $res->orderId))->get()->row();
      $category = $this->db->select('*')->from('categories')->where(array('id' => $order->category_id))->get()->row();
      $table = $category->table;

      $businessPage = $this->db->select('*')->from($table)->where(array('id' => $res->businessPageId))->get()->row();
      $businessPage->type = $table;
      $businessPage->categoryID = $category->id;
      $businessPage->pageTitle = $order->title;
      $businessPage->urlType = $category->type;
      $res->businessPage = $businessPage;
       //echo "<pre>"; print_r($res); die;
      return $res;
    }

    public function fetchAll(){
        $res = $this->db->select('e.*, lt.title as locationName')
                ->distinct()
                ->from('tours AS e')
                ->join('locations_t AS lt', 'lt.location_id=e.locationId', 'LEFT OUTER')
                ->get()
                ->result();

        return $res;
    }

    public function update() {

    	$customerId = $this->customerId;

      $tour = array('enTitle' => $this->enTitle,
            'categories' => $this->categories,
            'itTitle' => $this->itTitle,
            'itDescription' => base64_encode($this->itDescription),
            'enDescription' => base64_encode($this->enDescription),
            'itServicesIncluded' => base64_encode($this->itServicesIncluded),
            'itServicesNotIncluded' => base64_encode($this->itServicesNotIncluded),
            'itPolicy' => base64_encode($this->itPolicy),
            'enServicesIncluded' => base64_encode($this->enServicesIncluded),
            'enServicesNotIncluded' => base64_encode($this->enServicesNotIncluded),
            'enPolicy' => base64_encode($this->enPolicy),
            'locationId' => $this->locationId,
            'lat' => $this->lat,
            'long' => $this->long,
            'dateStart' => $this->dateStart,
            'dateEnd' => $this->dateEnd,
            'daysAvailability' => $this->daysAvailability,
            'durationDay' => $this->durationDay,
            'durationHours' => $this->durationHours,
            'durationMin' => $this->durationMin,
            'sessionHour1' => $this->sessionHour1,
            'sessionHour2'=>$this->sessionHour2,
            'sessionHour3' => $this->sessionHour3,
            'priceUnit' => $this->priceUnit,
            'price' => $this->price,
            // 'discountAdult' => $this->discountAdult,
            // 'discountChildren' => $this->discountChildren,
            'customerId' => $this->customerId,
            'type' => $this->type,
            'instagramWidget' => $this->instagramWidget,
            'thumbnail'=>$this->thumbnail,
            'cover'=>$this->cover,
            'businessPageId' => $this->businessPageId,
            'orderId' => $this->orderId,
            'fixedPrice' => $this->fixedPrice,
            'maxParticipants' => $this->maxParticipants,
            'childPrice' => $this->childPrice
        );

        if ($this->id == 0) {
            $tour['slug'] = $this->create_slug($tour['itTitle']);
            $this->db->insert('tours', $tour);
            return $this->db->insert_id();
        } else {
            $this->db->where(array('id' => $this->id));
            $this->db->update('tours', $tour);
            return TRUE;
        }
    }
    public function create_slug($name){
        $count = 0;
        $slug_name = $name = url_title($name);

        $this->db->from('tours')->where('slug', $slug_name);
        if ($this->db->count_all_results() > 0) {
          $slug_name = $name . '-' . (++$count);
        }

        return $slug_name;
    }


    public function update_photo($data) {
        $this->db->where(array('id' => $this->id));
        $this->db->update('tours', $data);
        return true;
    }
    public function totalActiveTours($type){
        $res = $this->db->select('e.id')
                ->from('tours AS e')
                ->where(array('e.type'=>$type))
                ->get();
        return $res->num_rows();
    }

    public function getActiveTours() {
        $res = $this->db->select('t.*, lt.title as locationName')
                ->distinct()
                ->from('tours AS t')
                ->join('locations_t AS lt', 'lt.location_id=t.locationId', 'LEFT OUTER')
                ->get()
                ->result();
        return $res;
    }
    public function getRandomTour($count){
      $tours = $this->getActiveTours();
      if (count($tours) > $count) {
        return $this->get_random_elements($tours, $count);
      }
      else {
          return $this->get_random_elements($tours);
      }
    }

    function get_random_elements($array, $limit = 0 ) {
      shuffle($array);
      if ( $limit > 0 ) {
          $array = array_splice($array, 0, $limit);
      }
      return $array;
    }

    public function getLocationName($id){
         $res = $this->db->query("SELECT lt.title FROM locations_t as lt LEFT OUTER JOIN tours t ON (t.locationId=lt.location_id) WHERE t.id='".$id."'")->row();
        if($res){
            return $res->title;
        }else{
            return " ";
        }
    }

     public function delete($id) {
        $this->db->where(array('id' => $id));
        $this->db->delete('tours');
        return true;
    }

    public function getTourInPeriod($checkIn, $checkOut) {

      $checkIn = date('Y-m-d' , strtotime($checkIn));
      $checkOut = date('Y-m-d', strtotime($checkOut));

      $this->db->select('*');
      $this->db->from('tours');
      $res = $this->db->get()->result();

      $tours = array();

      foreach ($res as $key => $tour) {
        # code...
        $dateStart = $tour->dateStart;
        $dateEnd = $tour->dateEnd;

        $dateStart = date('Y-m-d', strtotime($dateStart));
        $dateEnd = date('Y-m-d', strtotime($dateEnd));

        if ($checkIn >= $dateStart && $checkOut <= $dateEnd) {
          array_push($tours, $tour);
        }

      }


      //echo "<pre>"; print_r($tours); die;
      return $res;
    }
}
