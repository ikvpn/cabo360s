<?php

if (!defined('BASEPATH')) {
    exit('No direct script access is allowed');
}

class Procida_model extends CI_Model {
    public $radius;
    public function __construct() {
        parent::__construct();
        $this->radius = $this->getradius();
        $this->radius = $this->radius<=0?0.5:$this->radius;
    }

    public function getResturants($select = 'r.*,o.location', $condition = FALSE, $limit = FALSE) {
        $sql = 'SELECT '.$select.' FROM resturants as r LEFT OUTER JOIN orders as o ON (r.order_id=o.id)';
        if ($condition !== FALSE) {
            $sql .= " WHERE " . $condition;
        }
        if($limit!== FALSE){
            $sql .= " LIMIT ".$limit;
        }
        return $this->db->query($sql)->result();
    }

    public function getHotels($select = 'h.*,o.location', $condition = FALSE, $limit = FALSE) {
         $sql = 'SELECT '.$select.' FROM hotels as h LEFT OUTER JOIN orders as o ON (h.order_id=o.id)';
        if ($condition !== FALSE) {
            $sql .= " WHERE " . $condition;
        }
        if($limit!== FALSE){
            $sql .= " LIMIT ".$limit;
        }
        return $this->db->query($sql)->result();
    }

    public function getBeaches($select = 'b.*,bt.title', $condition = FALSE, $limit = FALSE, $lang='en'){
        $sql = 'SELECT '.$select.' FROM beaches b LEFT OUTER JOIN beaches_t bt ON(b.id=bt.beach_id AND bt.lang=\''.$lang.'\')';
        if ($condition !== FALSE) {
            $sql .= " WHERE " . $condition;
        }
        if($limit!== FALSE){
            $sql .= " LIMIT ".$limit;
        }
        return $this->db->query($sql)->result();
    }

    public function getShops($select = 's.*,o.location', $condition = FALSE, $limit = FALSE){
        $sql = 'SELECT '.$select.' FROM shopes as s LEFT OUTER JOIN orders as o ON (s.order_id=o.id)';
        if ($condition !== FALSE) {
            $sql .= " WHERE " . $condition;
        }
        if($limit!== FALSE){
            $sql .= " LIMIT ".$limit;
        }
        return $this->db->query($sql)->result();
    }

    public function getUpcomingEvents($select = 'e.*,o.location', $condition = FALSE, $limit = FALSE){
         $sql = 'SELECT '.$select.' FROM events as e LEFT OUTER JOIN orders as o ON (e.order_id=o.id)';
        if ($condition !== FALSE) {
            $sql .= " WHERE " . $condition ." AND e.startDate >= CURRENT_DATE()";
        }
        if($limit!== FALSE){
            $sql .= " LIMIT ".$limit;
        }
        return $this->db->query($sql)->result();
    }

    public function getLocationName($id , $lang='en'){
         $res = $this->db->query("SELECT title FROM locations_t WHERE lang='".$lang."' AND location_id='".$id."'")->row();
        if($res){
            return $res->title;
        }else{
            return " ";
        }
    }

    public function getEmailAddress($table="locations",$orderid=1){
        if($table == "locations" || $table == "beaches" || $table == "placesofinterest" || $table=="events"){
            $result = $this->db->select("*")->from("users")->where(array("id"=>1))->get()->row();
            return array("email"=>$result->email,"full_name"=>$result->full_name);
        }else{
            $sql = "SELECT c.firstName, lastName, c.email FROM customers c INNER JOIN orders o ON(o.customer_id = c.id AND o.id=$orderid)";
            $result = $this->db->query($sql)->row();

            if($result){
                return array("email"=>$result->email,"full_name"=>$result->firstName.' '.$result->lastName);
            }
            return array("email"=>"visitprocida@gmail.com","full_name"=>"VisitProcida");
        }
    }

    public function getResturantsForSideBar($lang,$lat,$lng){
    	
        $radius = $this->radius;
        $sql = "SELECT r.id,r.enName,r.itName,r.duName,r.photo,r.min_price,r.max_price, o.title as pageTitle, lt.title as locationName,
                       111.1111 * DEGREES(ACOS(COS(RADIANS(r.lat))
                                * COS(RADIANS($lat))
  		                * COS(RADIANS(r.lng - $lng))
                                + SIN(RADIANS(r.lat))
                                * SIN(RADIANS($lat)))) AS distance_in_km
                FROM resturants AS r
                LEFT OUTER JOIN orders as o on(r.order_id = o.id)
                LEFT OUTER JOIN locations_t as lt ON(lt.location_id = o.location AND lang='$lang')
                WHERE o.status='paid'
                HAVING distance_in_km<$radius";
                
        $query = $this->db->query($sql);
        $res = $query->result();
        return $res;
    }

    public function getHotelsForSideBar($lang,$lat,$lng){
        $radius = $this->radius;
        $sql = "SELECT h.id,h.enName,h.itName,h.duName,h.photo,h.price,h.unit,h.url, o.title as pageTitle, lt.title as locationName,
                       111.1111 * DEGREES(ACOS(COS(RADIANS(h.lat))
                                * COS(RADIANS($lat))
    		                * COS(RADIANS(h.lng - $lng))
                                + SIN(RADIANS(h.lat))
                                * SIN(RADIANS($lat)))) AS distance_in_km
                FROM hotels AS h
                LEFT OUTER JOIN orders as o on(h.order_id = o.id)
                LEFT OUTER JOIN locations_t as lt ON(lt.location_id = o.location AND lang='$lang')
                WHERE o.status='paid'
                HAVING distance_in_km<$radius";
        $query = $this->db->query($sql);
        return $query->result();
    }
     public function getShopsForSideBar($lang,$lat,$lng){
        $radius = $this->radius;
        $sql = "SELECT s.id,s.enName,s.itName,s.duName,s.photo,s.minPrice,s.maxPrice, o.title as pageTitle, lt.title as locationName,
                       111.1111 * DEGREES(ACOS(COS(RADIANS(s.lat))
                                * COS(RADIANS($lat))
  		                * COS(RADIANS(s.lng - $lng))
                                + SIN(RADIANS(s.lat))
                                * SIN(RADIANS($lat)))) AS distance_in_km
                FROM shopes AS s
                LEFT OUTER JOIN orders as o on(s.order_id = o.id)
                LEFT OUTER JOIN locations_t as lt ON(lt.location_id = o.location AND lang='$lang')
                WHERE o.status='paid'
                HAVING distance_in_km<$radius";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getBarsandCafesForSideBar($lang,$lat,$lng){
        $radius = $this->radius;
        $sql = "SELECT s.id,s.enName,s.itName,s.duName,s.photo,s.min_price,s.max_price, o.title as pageTitle, lt.title as locationName,
                       111.1111 * DEGREES(ACOS(COS(RADIANS(s.lat))
                                * COS(RADIANS($lat))
  		                * COS(RADIANS(s.lng - $lng))
                                + SIN(RADIANS(s.lat))
                                * SIN(RADIANS($lat)))) AS distance_in_km
                FROM bars_cafe AS s
                LEFT OUTER JOIN orders as o on(s.order_id = o.id)
                LEFT OUTER JOIN locations_t as lt ON(lt.location_id = o.location AND lang='$lang')
                WHERE o.status='paid'
                HAVING distance_in_km<$radius";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getLocationsForSideBar($lang,$lat,$lng){
        $radius = $this->radius;
        $sql = "SELECT l.*, lt.title as locationName,
                       111.1111 * DEGREES(ACOS(COS(RADIANS(h.lat))
                                * COS(RADIANS($lat))
  		                * COS(RADIANS(h.lng - $lng))
                                + SIN(RADIANS(h.lat))
                                * SIN(RADIANS($lat)))) AS distance_in_km
                FROM locations AS l
                LEFT OUTER JOIN locations_t as lt ON(lt.location_id = l.id AND lang='$lang')
                WHERE o.status='paid'
                HAVING distance_in_km<$radius";
        $query = $this->db->query($sql);
        return $query->result();
    }

	public function getBeachClubsForSideBar($lang,$lat,$lng){
        $radius = $this->radius;
        $sql = "SELECT s.id,s.enName,s.itName,s.duName,s.photo,s.minPrice,s.maxPrice, o.title as pageTitle, lt.title as locationName,
                       111.1111 * DEGREES(ACOS(COS(RADIANS(s.lat))
                                * COS(RADIANS($lat))
  		                * COS(RADIANS(s.lng - $lng))
                                + SIN(RADIANS(s.lat))
                                * SIN(RADIANS($lat)))) AS distance_in_km
                FROM beach_clubs AS s
                LEFT OUTER JOIN orders as o on(s.order_id = o.id)
                LEFT OUTER JOIN locations_t as lt ON(lt.location_id = o.location AND lang='$lang')
                WHERE o.status='paid'
                HAVING distance_in_km<$radius";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getradius()
    {
         $CI =& get_instance();
         $CI->load->model('admin/settings_model');
         $CI->settings_model->load();
         return $CI->settings_model->radius;
    }

    public function getPlacesForSideBar($lat,$lng){
        $radius = $this->radius;
        $sql = "SELECT s.id,s.enTitle,s.itTitle,s.duTitle,s.enDescription,s.itDescription,
                       111.1111 * DEGREES(ACOS(COS(RADIANS(s.lat))
                                * COS(RADIANS($lat))
    		                * COS(RADIANS(s.lng - $lng))
                                + SIN(RADIANS(s.lat))
                                * SIN(RADIANS($lat)))) AS distance_in_km
                FROM placesofinterest AS s
                HAVING distance_in_km<$radius";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function addViewToBusinessPage($data){
      $date = date('d-m-Y');
      $date = explode('-', $date);
      $data['day'] = $date[0];
      $data['month'] = $date[1];
      $data['year'] = $date[2];

      $this->db->insert('activity_views', $data);
      return $this->db->insert_id();
    }

    public function getYearlyViewsOfBusinessPage($id, $type, $month){

      $date = date('d-m-Y');
      $date = explode('-', $date);
      $year = $date[2];

      $this->db->select('*');
      $this->db->from('activity_views');
      $this->db->where('activityId', $id);
      $this->db->where('type', $type);
      $this->db->where('year', $year);
      $this->db->where('month', $month);
      $query = $this->db->get();
      $rowcount = $query->num_rows();
      return $rowcount;
    }

    public function addContactToBusinessPage($data){
      $date = date('d-m-Y');
      $date = explode('-', $date);
      $data['day'] = $date[0];
      $data['month'] = $date[1];
      $data['year'] = $date[2];

      $this->db->select('*');
      $this->db->from('orders');
      $this->db->where('id', $data['customerId']);
      $res = $this->db->get()->row_array();

      $data['customerId'] = $res['customer_id'];

      $this->db->insert('activity_contacts', $data);
      return $this->db->insert_id();
    }

    public function getYearlyContactsOfBusinessPage($id, $month){

      $date = date('d-m-Y');
      $date = explode('-', $date);
      $year = $date[2];

      $this->db->select('*');
      $this->db->from('activity_contacts');
      $this->db->where('customerId', $id);
      $this->db->where('year', $year);
      $this->db->where('month', $month);
      $query = $this->db->get();
      $rowcount = $query->num_rows();
      return $rowcount;
  }

    public function getTour($tourId){
      $this->db->select('*');
      $this->db->from('tours');
      $this->db->where('id', $tourId);
      $res = $this->db->get('')->row();
      return $res;
    }
}
?>
