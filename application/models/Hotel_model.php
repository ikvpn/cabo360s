<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if(!defined('BASEPATH')){exit('No direct script access is allowed');}

class Hotel_model extends CI_Model{
    public $id;
    public $enName;
    public $itName;
    public $duName;
	  public $enInsta;
    public $enDescription;
    public $itDescription;
    public $duDescription;
    public $photo;
    public $price;
    public $unit;
    public $order_id;
    public $rating;
    public $url;
    public $lat;
    public $lng;
    public $stars;
    public $categories;
    public $fbWidget;
    public $tripWidget;
    public $tags;
    public $activity_services;
    public $manager;
    public $manager_profile;
    public $phone;

    public function __construct() {
        parent::__construct();
    }

    public function load($id){
        $row = $this->db->select('*')->from('hotels')->where(array('id'=>$id))->get()->row();
        if($row){
            $this->id = $row->id;
            $this->enName = $row->enName;
            $this->itName = $row->itName;
            $this->duName = $row->duName;
			      $this->enInsta = $row->enInsta;
            $this->enDescription = base64_decode($row->enDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->photo = $row->photo;
            $this->price = $row->price;
            $this->unit = $row->unit;
            $this->order_id = $row->order_id;
            $this->rating = $row->rating;
            $this->url = $row->url;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->stars = $row->stars;
            $this->categories = $row->categories;
            $this->fbWidget = $row->fbWidget;
            $this->tripWidget = $row->tripWidget;
            $this->tags = $row->tags;
            $this->activity_services = $row->activity_services;
            $this->phone = $row->phone;
            $this->manager = $row->manager;
            $this->manager_profile = $row->manager_profile;
        }else{
           $this->id = $this->enName =$this->itName = $this->duName = $this->enInsta = $this->enDescription = $this->itDescription = $this->duDescription = NULL;
           $this->photo = $this->price = $this->unit = $this->order_id = $this->rating = $this->categories = $this->fbWidget = $this->tripWidget = $this->tags = $this->activity_services = $this->phone = $this->manager = $this->manager_profile = NULL;
        }
    }

    public function loadOnOrderId($id){
        $row = $this->db->select('*')->from('hotels')->where(array('order_id'=>$id))->get()->row();
        if($row){
            $this->id = $row->id;
            $this->enName = $row->enName;
            $this->itName = $row->itName;
            $this->duName = $row->duName;
			      $this->enInsta = $row->enInsta;
            $this->enDescription = base64_decode($row->enDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->photo = $row->photo;
            $this->price = $row->price;
            $this->unit = $row->unit;
            $this->order_id = $row->order_id;
            $this->rating = $row->rating;
            $this->url = $row->url;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->stars = $row->stars;
            $this->categories = $row->categories;
            $this->fbWidget = $row->fbWidget;
            $this->tripWidget = $row->tripWidget;
            $this->tags = $row->tags;
            $this->activity_services = $row->activity_services;
            $this->phone = $row->phone;
            $this->manager = $row->manager;
            $this->manager_profile = $row->manager_profile;
        }else{
           $this->id = $this->enName =$this->itName = $this->duName = $this->enInsta = $this->enDescription = $this->itDescription = $this->duDescription = NULL;
           $this->photo = $this->price = $this->unit = $this->order_id = $this->rating = $this->categories = $this->fbWidget = $this->tripWidget = $this->tags = $this->activity_services = $this->phone = $this->manager = $this->manager_profile = NULL;
        }
    }

    public function update(){
        $hotel = array('enName'=>$this->enName,
                           'duName'=>$this->duName,
                           'itName'=>$this->itName,
                		       'enInsta' => $this->enInsta,
                           'enDescription'=>  base64_encode($this->enDescription),
                           'duDescription'=> base64_encode($this->duDescription),
                           'itDescription'=> base64_encode($this->itDescription),
                           'price'=>$this->price,
                           'unit'=>$this->unit,
                           'order_id'=>$this->order_id,
                           'url'=>$this->url,
                           'lat'=>$this->lat,
                           'lng'=>$this->lng,
                           'stars'=>$this->stars,
                           'categories'=>$this->categories,
                           'fbWidget'=>$this->fbWidget,
                           'tripWidget'=>$this->tripWidget,
                           'tags'=>$this->tags,
                           'activity_services'=>$this->activity_services,
                           'phone'=>$this->phone,
                           'manager'=>$this->manager,
                           'manager_profile'=>$this->manager_profile,
                );
        if($this->id == 0){
            $this->db->insert('hotels',$hotel);
            return TRUE;
        }else{
            $this->db->where(array('id'=>$this->id));
            $this->db->update('hotels',$hotel);
            return TRUE;
        }
    }

    public function update_photo($photo){
        $this->db->where(array('id'=>$this->id));
        $this->db->update('hotels',Array('photo'=>$photo));
        return true;
    }

    public function getActiveHotels($lang='en'){
        // $res = $this->db->select('h.*, lt.title as locationName')
        //         ->from('hotels AS h')
        //         ->join('orders AS o','h.order_id=o.id','LEFT OUTER')
        //         ->join('locations_t AS lt','lt.location_id=o.location AND lang="'.$lang.'"','LEFT OUTER')
        //         ->where(array('o.status'=>'paid'))
        //         ->get()
        //         ->result();
                
                $res = $this->db->select('DISTINCT(enName),id')->from('hotels')->group_by('enName')->get()->result();
    //    echo $this->db->last_query();
      //  var_dump($res);
    //    exit();
      return $res;
    }
    
    public function getActiveApartmentsforHotalList($lang='en'){
        // $res = $this->db->select('a.*, o.title as pageTitle, lt.title as locationName')
        // ->from('apartments AS a')
        // ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
        // ->join('locations_t AS lt', 'lt.location_id=o.location AND lang="' . $lang . '"', 'LEFT OUTER')
        // ->where(array('o.status' => 'paid'))
        // ->get()
        // ->result();
        
        
        $res = $this->db->select('DISTINCT(enName),id')->from('apartments')->group_by('enName')->get()->result();
      return $res;
    }


    public function getActivBedAndBreakfastforHotalList($lang='en'){
        // $res = $this->db->select('a.*, o.title as pageTitle, lt.title as locationName')
        //         ->from('bed_and_breakfast AS a')
        //         ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
        //         ->join('locations_t AS lt', 'lt.location_id=o.location AND lang="' . $lang . '"', 'LEFT OUTER')
        //         ->where(array('o.status' => 'paid'))
        //         ->get()
        //         ->result();
                
                
                $res = $this->db->select('DISTINCT(enName),id')->from('bed_and_breakfast')->group_by('enName')->get()
                ->result();
      return $res;
    }
    
    
     public function totalActiveHotels() {
        $res = $this->db->select('a.id')
                ->from('hotels AS a')
                ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
                ->where(array('o.status' => 'paid'))
                ->get();
        return $res->num_rows();
    }
    public function count_on_location_id($id){
        $res = $this->db->query("SELECT COUNT(id) as totalHotels FROM orders WHERE status='paid' AND category_id=1 AND location=".$id)->row();
        return $res->totalHotels;
    }

    public function getAll(){
        $this->db->select('*');
        $res = $this->db->get('hotels')->result();
        return $res;
    }

}
