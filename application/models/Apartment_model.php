<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script access is allwoed');
}

class Apartment_model extends CI_Model {

    public $id;
    public $enName;
    public $duName;
    public $itName;
	  public $enInsta;
    public $enDescription;
    public $duDescription;
    public $itDescription;
    public $minPrice;
    public $maxPrice;
    public $unit;
    public $photo;
    public $rooms;
    public $beds;
    public $pax;
    public $lat;
    public $lng;
    public $trypology;
    public $bathrooms;
    public $terrace;
    public $rates;
    public $order_id;
    public $cDate;
    public $categories;
    public $fbWidget;
    public $tripWidget;
    public $tags;
    public $activity_services;
    public $phone;
    public $manager;
    public $manager_profile;
    public $home_heighlights;

    public function __construct() {
        parent::__construct();
    }

    public function load($id) {
        $row = $this->db->select('*')
                ->from('apartments')
                ->where(array('id' => $id))
                ->get()
                ->row();
        if ($row) {
            $this->id = $row->id;
            $this->enName = $row->enName;
            $this->duName = $row->duName;
            $this->itName = $row->itName;
			      $this->enInsta = $row->enInsta;
            $this->enDescription = base64_decode($row->enDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->minPrice = $row->minPrice;
            $this->maxPrice = $row->maxPrice;
            $this->phone = $row->phone;
            $this->manager = $row->manager;
            $this->manager_profile = $row->manager_profile;
            $this->unit = $row->unit;
            $this->photo = $row->photo;
            $this->rooms = $row->rooms;
            $this->beds = $row->beds;
            $this->pax = $row->pax;
            $this->trypology = $row->trypology;
            $this->bathrooms = $row->bathrooms;
            $this->terrace = $row->terrace;
            $this->rates = $row->rates;
            $this->order_id = $row->order_id;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->cDate = $row->cDate;
            $this->categories = $row->categories;
            $this->fbWidget = $row->fbWidget;
            $this->tripWidget = $row->tripWidget;
            $this->tags = $row->tags;
            $this->activity_services = $row->activity_services;
            $this->home_heighlights = $row->home_heighlights;
        } else {
            $this->id = $this->enName = $this->duName = $this->itName = $this->phone = $this->manager = $this->manager_profile = $this->enInsta = $this->enDescription = $this->duDescription = $this->itDescription = $this->minPrice = $this->maxPrice = $this->fbWidget = $this->tripWidget = $this->tags = NULL ;
            $this->unit = $this->photo = $this->rooms = $this->beds = $this->pax = $this->trypology = $this->bathrooms = $this->terrace = $this->rates = $this->order_id = $this->cDate = $this->categories = $this-$activity_services = $this->home_heighlights = NULL;
        }
    }

    public function loadOnOrderId($id) {
        $row = $this->db->select('*')
                ->from('apartments')
                ->where(array('order_id' => $id))
                ->get()
                ->row();
        if ($row) {
            $this->id = $row->id;
            $this->enName = $row->enName;
            $this->duName = $row->duName;
            $this->itName = $row->itName;
			      $this->enInsta = $row->enInsta;
            $this->enDescription = base64_decode($row->enDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->minPrice = $row->minPrice;
            $this->maxPrice = $row->maxPrice;
            $this->unit = $row->unit;
            $this->photo = $row->photo;
            $this->rooms = $row->rooms;
            $this->beds = $row->beds;
            $this->pax = $row->pax;
            $this->trypology = $row->trypology;
            $this->bathrooms = $row->bathrooms;
            $this->terrace = $row->terrace;
            $this->rates = $row->rates;
            $this->order_id = $row->order_id;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->cDate = $row->cDate;
            $this->categories = $row->categories;
            $this->fbWidget = $row->fbWidget;
            $this->tripWidget = $row->tripWidget;
            $this->tags = $row->tags;
            $this->activity_services = $row->activity_services;
            $this->phone = $row->phone;
            $this->manager = $row->manager;
            $this->manager_profile = $row->manager_profile;
            $this->home_heighlights = $row->home_heighlights;
        } else {
            $this->id = $this->enName = $this->duName = $this->itName = $this->enInsta = $this->phone = $this->manager = $this->manager_profile = $this->enDescription = $this->duDescription = $this->itDescription = $this->minPrice = $this->maxPrice = $this->unit = $this->tags = NULL ;;
            $this->photo = $this->rooms = $this->beds = $this->pax = $this->trypology = $this->bathrooms = $this->terrace = $this->rates = $this->order_id = $this->cDate = $this->categories = $this->fbWidget = $this->tripWidget = $this->activity_services = $this->home_heighlights = NULL;
        }
    }

    public function update() {
        if ($this->id == 0) {
            //insert
            $apartment = array('enName' => $this->enName,
                'duName' => $this->duName,
                'itName' => $this->itName,
                'enInsta' => $this->enInsta,
                'enDescription' => base64_encode($this->enDescription),
                'duDescription' => base64_encode($this->duDescription),
                'itDescription' => base64_encode($this->itDescription),
                'minPrice' => $this->minPrice,
                'maxPrice' => $this->maxPrice,
                'unit' => $this->unit,
                'photo' => $this->photo?$this->photo:"",
                'rooms' => $this->rooms,
                'beds' => $this->beds,
                'pax' => $this->pax,
                'trypology' => $this->trypology,
                'bathrooms' => $this->bathrooms,
                'terrace' => $this->terrace,
                'phone' => $this->phone,
				        'manager' => $this->manager,
				        'manager_profile' => $this->manager_profile,
                'rates' => $this->rates,
                'lat' => $this->lat,
                'lng' => $this->lng,
                'order_id' => $this->order_id,
                'categories'=>$this->categories,
                'fbWidget'=>$this->fbWidget,
                'tripWidget'=>$this->tripWidget,
                'tags'=>$this->tags,
                'activity_services'=>$this->activity_services,
                'home_heighlights' => $this->home_heighlights
            );
            $this->db->insert('apartments', $apartment);
            return TRUE;
        } else {
            //update
            $apartment = array('enName' => $this->enName,
                'duName' => $this->duName,
                'itName' => $this->itName,
                'enInsta' => $this->enInsta,
                'enDescription' => base64_encode($this->enDescription),
                'duDescription' => base64_encode($this->duDescription),
                'itDescription' => base64_encode($this->itDescription),
                'minPrice' => $this->minPrice,
                'maxPrice' => $this->maxPrice,
                'unit' => $this->unit,
                'photo' => $this->photo,
                'rooms' => $this->rooms,
                'phone' => $this->phone,
				        'manager' => $this->manager,
				        'manager_profile' => $this->manager_profile,
                'beds' => $this->beds,
                'pax' => $this->pax,
                'trypology' => $this->trypology,
                'bathrooms' => $this->bathrooms,
                'terrace' => $this->terrace,
                'rates' => $this->rates,
                'lat' => $this->lat,
                'lng' => $this->lng,
                'categories'=>$this->categories,
                'fbWidget'=>$this->fbWidget,
                'tripWidget'=>$this->tripWidget,
                'tags'=>$this->tags,
                'activity_services'=>$this->activity_services,
                'home_heighlights' => $this->home_heighlights
            );
            $this->db->where(array('id' => $this->id));
            $this->db->update('apartments', $apartment);
            return TRUE;
        }
    }

    public function update_photo($photo) {
        $this->db->where(array('id' => $this->id));
        $this->db->update('apartments', Array('photo' => $photo));
        return true;
    }

    public function getActiveApartments($lang = 'en') {
        $page = $this->uri->segment(4)?$this->uri->segment(4):0;
        $start = 0;
        if($page >0){
         $start = (--$page) * 10;
        }
        $limit = 10;
        $res = $this->db->select('a.*, o.title as pageTitle, lt.title as locationName')
                ->from('apartments AS a')
                ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
                ->join('locations_t AS lt', 'lt.location_id=o.location AND lang="' . $lang . '"', 'LEFT OUTER')
                ->where(array('o.status' => 'paid'))
                ->limit($limit,$start)
                ->get()
                ->result();
        return $res;
    }

     public function getCustomActiveApartments($lang = 'en') {
        $page = $this->uri->segment(4)?$this->uri->segment(4):0;
        $start = 0;
        if($page >0){
         $start = (--$page) * 10;
        }
        $limit = 10;
        $conditions = array('o.status'=>'paid');
        if($this->input->post('rooms')>0){
            $conditions['a.rooms'] = $this->input->post('rooms');
        }
        if($this->input->post('beds')>0){
            $conditions['a.beds'] = $this->input->post('beds');
        }
        if($this->input->post('pax')>0){
            $conditions['a.pax'] = $this->input->post('pax');
        }
        if($this->input->post('minPrice')){
            $conditions['a.minPrice >'] = $this->input->post('minPrice');
        }
        if($this->input->post('maxPrice')){
            $conditions['a.maxPrice <'] = $this->input->post('maxPrice');
        }
        if($this->input->post('locations')){
            $this->db->where_in('o.location',$this->input->post('locations'));
        }
        $res = $this->db->select('a.*, o.title as pageTitle, lt.title as locationName')
                ->from('apartments AS a')
                ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
                ->join('locations_t AS lt', 'lt.location_id=o.location AND lang="' . $lang . '"', 'LEFT OUTER')
                ->where($conditions)
                ->limit($limit,$start)
                ->get()
                ->result();
      //  echo $this->db->last_query();exit();
        return $res;
    }

    public function totalActiveApartments() {
        $res = $this->db->select('a.id')
                ->from('apartments AS a')
                ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
                ->where(array('o.status' => 'paid'))
                ->get();
        return $res->num_rows();
    }

    public function totalCustomActiveApartments() {
         $conditions = array('o.status'=>'paid');
        if($this->input->post('rooms')>0){
            $conditions['a.rooms'] = $this->input->post('rooms');
        }
        if($this->input->post('beds')>0){
            $conditions['a.beds'] = $this->input->post('beds');
        }
        if($this->input->post('pax')>0){
            $conditions['a.pax'] = $this->input->post('pax');
        }
        $res = $this->db->select('a.id')
                ->from('apartments AS a')
                ->join('orders AS o', 'a.order_id=o.id', 'LEFT OUTER')
                ->where($conditions)
                ->get();
        return $res->num_rows();
    }

    public function loadActivePage($order_title) {
        $row = $this->db->select('id')->from('orders')->where(array('title' => $order_title))->get()->row();
        if ($row) {
            $this->loadOnOrderId($row->id);
        }
    }

    public function getLocationName($lang = "en") {
        $res = $this->db->query("SELECT lt.title FROM locations_t as lt LEFT OUTER JOIN orders o ON (o.location=lt.location_id) WHERE lang='" . $lang . "' AND o.id='" . $this->order_id . "'")->row();
        // echo $this->db->last_query();exit();
        if ($res) {
            return $res->title;
        } else {
            return " ";
        }
    }

}
