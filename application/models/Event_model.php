<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File: 
 * Description:
 */
if (!defined("BASEPATH")) {
    exit("No direct script access is allowed");
}

class Event_model extends CI_Model {

    public $id;
    public $enTitle;
    public $itTitle;
    public $duTitle;
    public $enDescription;
    public $itDescription;
    public $duDescription;
    public $startTime;
    public $endTime;
    public $startDate;
    public $endDate;
    public $reservation;
    public $pAdult;
    public $pGuest;
    public $pChild;
    public $web;
    public $phone;
    public $mob;
    public $email;
    public $address;
    public $lat;
    public $lng;
    public $customer_id;
    public $status;
    public $thumbnail;
    public $cover;
    public $category;
    public $location_id;
    public $type;

    public function __construct() {
        parent::__construct();
    }

    public function load($id) {
        $row = $this->db->select('*')->from('events')->where(array('id' => $id))->get()->row();
        if ($row) {
            $this->id = $row->id;
            $this->enTitle = $row->enTitle;
            $this->itTitle = $row->itTitle;
            $this->duTitle = $row->duTitle;
            $this->enDescription = base64_decode($row->enDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->startTime = $row->startTime;
            $this->endTime = $row->endTime;
            $this->startDate = $row->startDate;
            $this->endDate = $row->endDate;
            $this->reservation = $row->reservation;
            $this->pAdult = $row->pAdult;
            $this->pChild = $row->pChild;
            $this->pGuest = $row->pGuest;
            $this->web = $row->web;
            $this->phone = $row->phone;
            $this->mob = $row->mob;
            $this->email = $row->email;
            $this->address = $row->address;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->order_id = $row->order_id;
            $this->thumbnail = $row->thumbnail;
            $this->cover = $row->cover;
            $this->category = $row->category;
            $this->location_id = $row->location_id;
            $this->type = $row->type;
        } else {
            $this->id = $this->enTitle = $this->itTitle = $this->duTitle = $this->enDescription = $this->itDescription = $this->duDescription = NULL;
            $this->startTime = $this->endTime = $this->startDate = $this->endDate = $this->reservation = $this->pAdult = $this->pGuest = NULL;
            $this->pChild = $this->web = $this->phone = $this->email = $this->address = $this->lat = $this->lng = $this->customer_id = $this->thumbnail = $this->cover = NULL;
        }
    }

    public function loadOnOrderId($id) {
        $row = $this->db->select('*')->from('events')->where(array('order_id' => $id))->get()->row();
        if ($row) {
            $this->id = $row->id;
            $this->enTitle = $row->enTitle;
            $this->itTitle = $row->itTitle;
            $this->duTitle = $row->duTitle;
            $this->enDescription = base64_decode($row->enDescription);
            $this->itDescription = base64_decode($row->itDescription);
            $this->duDescription = base64_decode($row->duDescription);
            $this->startTime = $row->startTime;
            $this->endTime = $row->endTime;
            $this->startDate = $row->startDate;
            $this->endDate = $row->endDate;
            $this->reservation = $row->reservation;
            $this->pAdult = $row->pAdult;
            $this->pChild = $row->pChild;
            $this->pGuest = $row->pGuest;
            $this->web = $row->web;
            $this->phone = $row->phone;
            $this->mob = $row->mob;
            $this->email = $row->email;
            $this->address = $row->address;
            $this->lat = $row->lat;
            $this->lng = $row->lng;
            $this->order_id = $row->order_id;
            $this->thumbnail = $row->thumbnail;
            $this->cover = $row->cover;
            $this->category = $row->category;
            $this->type = $row->type;
        } else {
            $this->id = $this->enTitle = $this->itTitle = $this->duTitle = $this->enDescription = $this->itDescription = $this->duDescription = NULL;
            $this->startTime = $this->endTime = $this->startDate = $this->endDate= $this->reservation = $this->pAdult = $this->pGuest = NULL;
            $this->pChild = $this->web = $this->phone = $this->email = $this->address = $this->lat = $this->lng = $this->order_id = $this->thumbnail = $this->cover = NULL;
        }
    }

    public function fetchAll($lang='en'){
        $res = $this->db->select('e.*, lt.title as locationName')
                ->from('events AS e')
                ->join('locations_t AS lt', 'lt.location_id=e.location_id AND lang="' . $lang . '"', 'LEFT OUTER')
                ->order_by("e.startDate","desc")
                ->get()
                ->result();
        return $res;
    }
    
    public function update() {
    	
    	if(isset($this->order_id)) {
    		$order_id = $this->order_id;
    	} else {
    		$order_id = NULL;
    	}
        
        $event = array('enTitle' => $this->enTitle,
            'duTitle' => $this->duTitle,
            'itTitle' => $this->itTitle,
            'enDescription' => base64_encode($this->enDescription),
            'duDescription' => base64_encode($this->duDescription),
            'itDescription' => base64_encode($this->itDescription),
            'startTime' => $this->startTime,
            'endTime' => $this->endTime,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'reservation' => $this->reservation,
            'pAdult' => $this->pAdult,
            'pChild' => $this->pChild,
            'pGuest' => $this->pGuest,
            'web' => $this->web,
            'phone' => $this->phone,
            'mob'=>$this->mob,
            'email' => $this->email,
            'address' => $this->address,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'order_id' => $order_id,
            'category' => $this->category,
            'type' => $this->type,
            'status' => $this->status,
            'thumbnail'=>$this->thumbnail,
            'location_id'=>$this->location_id,
            'cover'=>$this->cover
        );
        
        if ($this->id == 0) {
            $this->db->insert('events', $event);
            return $this->db->insert_id();
        } else {
            $this->db->where(array('id' => $this->id));
            $this->db->update('events', $event);
            return TRUE;
        }
    }

    public function update_photo($data) {
        $this->db->where(array('id' => $this->id));
        $this->db->update('events', $data);
        return true;
    }
    public function totalActiveEvents($type){
        $res = $this->db->select('e.id')
                ->from('events AS e')
                ->where(array('e.type'=>$type))
                ->get();
        return $res->num_rows();
    }
    
    public function getActiveEvents($type, $lang = 'en') {
        $page = $this->uri->segment(4)?$this->uri->segment(4):0;
        $start = 0;
        if($page >0){
         $start = (--$page) * 10;
        }
        $limit = 10;
        $res = $this->db->select('e.*, lt.title as locationName')
                ->from('events AS e')
                ->join('locations_t AS lt', 'lt.location_id=e.location_id AND lang="' . $lang . '"', 'LEFT OUTER')
                ->where(array('e.type'=>$type))
                ->order_by("e.startDate","desc")
                ->limit($limit,$start)
                ->get()
                ->result();
        //  echo $this->db->last_query();
        //  var_dump($res);
        //   exit();
        return $res;
    }
   
    public function getLocationName($id , $lang='en'){
         $res = $this->db->query("SELECT lt.title FROM locations_t as lt LEFT OUTER JOIN events e ON (e.location_id=lt.location_id) WHERE lang='".$lang."' AND e.id='".$id."'")->row();
        if($res){
            return $res->title;
        }else{
            return " ";
        }
    }
    
     public function delete() {
        $this->db->where(array('id' => $this->id));
        $this->db->delete('events');
        return true;
    }
}
