<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tours extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->model('admin/Location_model');
        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model("Places_of_interest_model");
        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');
        $this->load->model('admin/Ads_model');
        $this->load->model('Orders_model');
        $this->lang->load('Eventspage');
        $this->load->model('Cart_model');
        $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');

        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');

        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');
        $this->load->model('Event_model');
        $this->load->model('admin/Block_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();

        $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());
    }

    public function index() {
        $this->tours_listing();
    }

    public function tours_listing() {
        $data = $this->data;
        $this->load->model('Tour_model');
        $this->lang->load('Tourspage');

        $data['tours'] = $this->Tour_model->getActiveTours();


        switch ($this->lang->lang()) {
            case 'en':
                $data['meta_keywords'] = 'cabo, tours, excursion';
                $data['meta_desc'] = 'Best Tours if Cabo San Lucas! Tours, Excursion, Guided Visit.';
                $data['meta_title'] = 'Premiere Experiences in Cabo San Lucas, Baja California del Sur';
                break;
            case 'it':
                $data['meta_keywords'] = 'procida, tours, escursioni, visite guidate';
                $data['meta_desc'] = 'Tutti i tour a Procida! Tours, Escursioni, Visite Guidate';
                $data['meta_title'] = 'Tours a Procida: tours, escursioni, visite guidate';
                break;
        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
    	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

      $data['cssfiles'] = array(
          array('type' => 'file', 'src' => base_url() . 'assets/css/label_price.css')
      );
      $data['jsfiles'] = array(
            array('type' => 'inline', 'src' => " var MAP_ID='map-canvas'; var CUR_LAT= 0; var CUR_LNG=0;"),
            array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
            array('type' => 'file', 'src' => base_url() . 'assets/js/sort_tour.js')
      );

      $data['blocks'] = $this->Block_model->getBlocks(4,$this->lang->lang());

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/tours', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    public function tour($slug) {
      $this->load->model('Tour_model');
      $this->lang->load('Tourspage');

      $data = $this->data;
      $data['tour'] = $this->Tour_model->getTour($slug);
      $data['tour_price'] = $this->db->select('fixedPrice,maxParticipants,childPrice')->from('tours')->where('slug',$slug)->get()->row();

       if ($data['tour'] == null) {
          show_404();
          return;
      }
       $data['facebook_sdk'] = '<div id="fb-root"></div>
          <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, \'script\', \'facebook-jssdk\'));</script>';

        $tour = (array)$data['tour'];

        switch ($this->lang->lang()) {
            case 'en':
                $data['meta_keywords'] = '';
                $data['meta_desc'] = '';
                $data['meta_title'] = $data['tour']->enTitle;
                break;
            case 'it':
                $data['meta_keywords'] = '';
                $data['meta_desc'] = '';
                $data['meta_title'] =$data['tour']->itTitle;
                break;

        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = [];
        }

        $data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $data['cssfiles'] = array(
            array('type' => 'file', 'src' => base_url() . 'assets/css/date-time-picker.css')
        );
        $data['jsfiles'] = array(
          array('type' => 'file', 'src' => base_url() . "assets/js/bootstrap-timepicker.js"),
          array('type' => 'file', 'src' => base_url() . "assets/js/bootstrap-datepicker.js"),

          array('type' => 'file', 'src' => base_url() . "assets/js/tour.js"),

          array('type'=>'inlince','src'=>'
              $(".map-info-box .line h2").click(function() {
                  $(".map-info-box .line p").slideUp();
                  $(this).next("p").slideToggle();
              });
              var directionsDisplay;
              var directionsService = new google.maps.DirectionsService();
              var map;

              function initialize() {
                  directionsDisplay = new google.maps.DirectionsRenderer();
                  var mainPlace = new google.maps.LatLng('.$data['tour']->lat.', '.$data['tour']->long.');
                  console.log(mainPlace);
                  var myStyles = [{
                      featureType: "poi",
                      elementType: "labels",
                      stylers: [{
                          visibility: "off"
                      }]
                  }];
                  var mapOptions = {
                      zoom: 16,
                      disableDefaultUI: true,
                      disableDoubleClickZoom: true,
                      scrollwheel: false,
                      center: mainPlace,
                      styles: myStyles
                  }
                  map = new google.maps.Map(document.getElementById(\'map-canvas-sidebar\'), mapOptions);
                  var marker = new google.maps.Marker({
                      position: mainPlace,
                      map: map
                  });
                  directionsDisplay.setMap(map);
              }

              function calcRoute(destination) {
                  var start = \'Corricella, Procida, Naples, Italy\';
                  var end = destination;
                  var request = {
                      origin: start,
                      destination: end,
                      travelMode: google.maps.TravelMode.DRIVING
                  };
                  directionsService.route(request, function(response, status) {
                      if (status == google.maps.DirectionsStatus.OK) {
                          directionsDisplay.setDirections(response);
                      }
                  });
              }
              google.maps.event.addDomListener(window, \'load\', initialize);
              google.maps.event.addDomListener(window, \'resize\', initialize);')
        );

        $data['blocks'] = $this->Block_model->getBlocks(4,$this->lang->lang());

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/tour_detail', $data);
        $this->load->view('frontend/includes/footer', $data);
    }


}
