<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */
if (!defined("BASEPATH")) {
    exit('No direct script accesss is allowed');
}

class Beach extends CI_Controller {

    private $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->model('admin/Location_model');
        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Ads_model');
        $this->load->model('Places_of_interest_model');
        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');
        $this->load->model('Cart_model');
        $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');

        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');

        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
       $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();

        $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());

    }

    public function index($id = ''){
       if($id != ''){
           $this->beach_detail($id);
           return;
       }
       $data = $this->data;
       $this->load->helper('cookie');

       $this->load->model('Procida_model');
       $cartCookie = $this->input->cookie('prcrt');

       if(!empty($cartCookie)){
         $items = $this->Cart_model->getUserCart($cartCookie, '');
         $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
         $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
         $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
         if(!empty($tours)){
           for ($i=0; $i < count($tours); $i++) {
             $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
           }
         }
       } else {
         $items = [];
         $hotelCart = [];
         $tranferCart = [];
         $tours = []; // show random tours
       }
   		$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

       switch ($this->lang->lang()) {
         case 'en':
         $data['meta_keywords'] = 'shopping, procida';
         $data['meta_desc'] = 'Procida Island Beaches: along the coast you can admire ravines, caves and coves, some of which are accessible only by sea';
         $data['meta_title'] = 'The beaches and the sea of Procida Island';
         break;


         case 'it':
         $data['meta_keywords'] = 'spiagge, baie, procida';
         $data['meta_desc'] = 'Spiagge e cale di Procida: lungo la costa dell isola si possono ammirare anfratti, grotte e calette, alcune delle quali raggiungibili solo dal mare.';
         $data['meta_title'] = 'Spiagge Procida: Le baie e le Cale più belle di Procida';
         break;


         case 'de':
         $data['meta_keywords'] = 'einkaufen, procida';
         $data['meta_desc'] = 'Einkaufen Procida: wo lokale Kunsthandwerk, Keramik und Zubehör zu kaufen';
         $data['meta_title'] = 'Die Strände und Buchten der Insel Procida';
         break;

     }
        $data['jsfiles'] = array(
              array('type' => 'inline', 'src' => " var MAP_ID='map-canvas'; var CUR_LAT= 0; var CUR_LNG=0;"),
                  array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js")
        );

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/beach-listing', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function beach_detail($id){
         $data = $this->data;
         $data['beach'] = $this->Beach_model->getBeach(0, $id,$this->lang->lang());

        if(!$data['beach']){
            show_404();
            return;
        }
        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $temp = (array) $data['beach'];
         $data['nearbybeaches'] = $this->Beach_model->getBeaches(false,$this->lang->lang());
		 $this->load->model('Procida_model');
        $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
		$data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);

		$data['meta_title'] = $temp['meta_'.$this->lang->lang()];
        $data['meta_keywords'] = $temp['meta_keywords_'.$this->lang->lang()];
        $data['meta_desc'] = $temp['meta_desc_'.$this->lang->lang()];
        $data['jsfiles'] = array(
               array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$temp['lat']."; var CUR_LNG=".$temp['lng'].";"),
               array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js")
        );

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/beach', $data);
        $this->load->view('frontend/includes/footer', $data);
    }
}
