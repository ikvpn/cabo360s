<?php

/* 
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com 
 * Project: Visit Procida
 * Version: 1.0
 * File:         labels_lang.php
 * Description:  application/language/english/
 */

$lang['What_to_visit'] = 'What to visit';
$lang['what-to-visit'] = 'what-to-visit';
$lang['where-to-stay'] = 'where-to-stay';
$lang['places-of-interest'] = 'places-of-interest';
$lang['traditional-events'] = 'traditional-events';
$lang["Places_Of_Interest"] = 'Places of Interset';
$lang['beach'] = 'beach';
$lang['eat-and-drink'] = 'eat-and-drink';
$lang['what-to-do'] = 'activities';
$lang['events'] = 'events';
$lang['useful-info'] = 'useful-info';
$lang['Map_of_the_Island'] = 'Map of the Island';
$lang['Where_to_stay'] = 'Where to stay';
$lang['Hotels'] = 'Hotels';
$lang['Holiday_Houses'] = 'Villas';
$lang['Special_Offers'] = 'Special Offers';
$lang['Eat_and_drink'] = 'Eat & drink';
$lang['Restaurants'] = 'Restaurants';
$lang['restaurants'] = 'restaurants';
$lang['Bars_and_Cafe'] = 'Bars and Cafe';
$lang['Nightlife'] = 'Nightlife';
$lang['What_to_do'] = 'What to do';
$lang['Events'] = 'Events';
$lang['Useful_Info'] = 'Useful Info';
$lang['Blog'] = 'Blog';
$lang['Shops'] = "Shops";
$lang['events_and_traditions'] = 'Events and Traditions';
$lang['Sights'] = 'Sights';
$lang['Beaches'] = 'Beaches';
$lang['Spa']='Spa';
$lang['Places_of_Interest'] = 'Places of Interest';

/* COMMON LABELS */
$lang['more'] = "More";
$lang['Search'] = 'Search';
$lang['View_details'] = 'View details';
$lang['More_details'] = 'More details';
$lang['Read_more'] = 'Read more';
$lang['Reviews'] = 'Reviews';
$lang['Accommodation_Types'] = 'Accommodation Types';
$lang['Check_In'] = 'Check In';

$lang['day'] = 'day';
$lang['week'] = 'week';
$lang['month'] = 'month';
$lang['year'] = 'year';
$lang['hours'] = 'Working Hours';

$lang['Price_Range'] = "Price Range";
$lang['Cuisine'] = "Cuisine";
$lang['Credit_Card'] = "Credit Card";
$lang['Closing_Day'] = "Closing Day";
$lang['Rating'] = "Rating";
$lang["Info"] = "Info";
$lang["Services"] = "Services";
$lang['Invited_People'] = "Invited People";
$lang['read_more'] = "read more";

/*Search Form*/
$lang['Number_of_People'] = "Number of people";
$lang["Search Hotel/Apartment in Procida"] = "Search Hotel/Apartment in Procida";
$lang['details'] = "details";

/*Weddings*/
$lang['Weddings'] = 'Weddings';

/* Boat Rentals */
$lang['Boat_Rentals'] = 'Boat Rentals';