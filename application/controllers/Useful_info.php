<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Useful_info extends CI_Controller {

    private $data;
    private $api_url = 'http://booking.traghettilines.it/webservice.asmx';
    private $user = 'info@gioiatredici.it';
    private $pass = 'tredici80079';
    private $options = array(
                'uri'=>'http://schemas.xmlsoap.org/soap/envelope/',
                'style'=>SOAP_RPC,
                'use'=>SOAP_ENCODED,
                'soap_version'=>SOAP_1_1,
                'cache_wsdl'=>WSDL_CACHE_NONE,
                'connection_timeout'=>15,
                'trace'=>true,
                'encoding'=>'UTF-8',
                'exceptions'=>true,
              );

    public function __construct() {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->model('admin/Location_model');
        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model("Places_of_interest_model");
        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
        $this->load->model('admin/Ads_model');
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');
        $this->lang->load('Blogpage');
         $this->load->model('Shope_model');
         $this->load->model('Cart_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');

        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');

        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();

        $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());
    }

    public function index($page = '') {
        switch ($page) {
            case 'ferry-time-table':
                $this->ferry_time_table();
                break;
            case 'how-to-get-there':
                $this->how_to_get_there();
                break;
            case 'transfer':
                $this->transfer();
                break;
            case 'getting-around-procida':
            	if($this->lang->lang() == 'it') {
            		redirect('info-utili/transfer');
            	} else {
            		redirect('useful-info/transfer');
            	}
            case 'getInfo':
              $this->getInfo();
              break;
            default:
                show_404();
                break;
        }
    }

    private function transfer(){
        $data = $this->data;
        $this->lang->load('Shopspage');
        $this->load->model('Shope_model');
        $this->load->model('Procida_model');
        $this->load->model('Hotel_model');
        $this->lang->load('Ferry_timetable');
        $this->lang->load('Transfer_booking');

        $data['hotels'] = $this->Hotel_model->getActiveHotels();
        $data['apartments'] = $this->Hotel_model->getActiveApartmentsforHotalList();
        $data['bed_and_breakfast'] = $this->Hotel_model->getActivBedAndBreakfastforHotalList();
        
        $data['jsfiles'] = array(
              array('type' => 'file', 'src' => base_url() . "assets/js/bootstrap-datepicker.js"),
              array('type' => 'file', 'src' => base_url() . "assets/js/bootstrap-timepicker.js"),
              array('type' => 'inline', 'src' => '
                $("input.date-pick").datepicker();
                $("input.time-pick").timepicker({minuteStep: 15,showInpunts: false, defaultTime: false, showMeridian : false});
              '),
              array('type' => 'file', 'src' => base_url() . "assets/js/transfer-booking.js")
        );

		$this->load->helper('cookie');

    $this->load->model('Procida_model');
    $cartCookie = $this->input->cookie('prcrt');

    if(!empty($cartCookie)){
      $items = $this->Cart_model->getUserCart($cartCookie, '');
      $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
      $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
      $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
      if(!empty($tours)){
        for ($i=0; $i < count($tours); $i++) {
          $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
        }
      }
    } else {
      $items = [];
      $hotelCart = [];
      $tranferCart = [];
      $tours = []; // show random tours
    }
			$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );
			switch ($this->lang->lang()) {
			        case 'en':
			          $data['meta_keywords'] = '';
			          $data['meta_desc'] = 'Private Transportation to your hotel, from San Jose del Cabo or Cabo San Lucas.';
			          $data['meta_title'] = 'Cabo Airport Transfer: book a provate transfer from the Airport to Cabo San Lucas';
			          break;

			        case 'it':
			          $data['meta_keywords'] = 'procida, transfer, aeroporto';
			          $data['meta_desc'] = 'Transfer Napoli Procida. Trasferimenti da Napoli Aeroporto per Procida con trasporto bagagli e aliscafo incluso. Prenota transfer e privati per Procida';
			          $data['meta_title'] = 'Procida Transfer: prenota transfer privati dall Aeroporto per Procida';
			          break;
            }
        $data['pick_up_points'] = json_decode (PICK_UP_POINTS);

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/getting-around-procida', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function how_to_get_there(){
        $data = $this->data;
        $this->lang->load('Shopspage');
        $this->load->model('Shope_model');
        $this->load->model('Procida_model');
        $this->lang->load('Ferry_timetable');


        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

          switch ($this->lang->lang()) {
            case 'en':
              $data['meta_keywords'] = '';
              $data['meta_desc'] = 'How to get to Procida from Naples, Pozzuoli and Ischia; ferries, hydrofoils and buses';
              $data['meta_title'] = 'How to get to Procida from Naples, Pozzuoli and Ischia';
              break;

            case 'it':
              $data['meta_keywords'] = 'procida, traghetti, aliscafi';
              $data['meta_desc'] = 'Come arrivare a Procida in auto, in treno e aereo. Informazioni sui porti e sulle compagnie di navigazione';
              $data['meta_title'] = 'Come arrivare a Procida da Napoli, Pozzuoli, Ischia';
              break;
          }

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/how-to-get-there', $data);
        $this->load->view('frontend/includes/footer', $data);

    }

    private function ferry_time_table() {
        $data = $this->data;
        $this->lang->load('Shopspage');
        $this->load->model('Shope_model');
        $this->load->model('Procida_model');
        $this->lang->load('Ferry_timetable');
        $this->load->model('admin/Ferry_model');


        $site = $this->input->get('url');

    if(empty($site)){

        $uriSegment = $this->uri->segment(2);

        if(strpos($uriSegment, 'traghetti') !== false && $uriSegment !== 'orari-traghetti'){
        	$uriSegment = explode('-', $uriSegment);
        	$route = ucfirst($uriSegment[1]).'-'.ucfirst($uriSegment[2]);
        	$date = date("Y-m-d");
        }
        else {
        	$date = $this->input->get('date');
        	$route = $this->input->get('route');

          $date = $date ? $date : date("Y-m-d");
          if (!$route) {
            $route = 'Napoli-Procida';
            if ($this->uri->segment(3) == 'ferry-time-table') {
              $data['customRoute'] = $this->uri->segment(3);
            }else{
              $data['customRoute'] = $this->uri->segment(2);
            }
          }
        }
          $routes = $this->Ferry_model->findRoutes();
          
          $data['routes'] = $routes;

          if($date !== NULL && $route !== NULL){

            $data['RoutesWithDailyDepartures'] = $this->searchDepartures($date, $route);

            $data['route'] = $route;
            $data['routeId'] = $this->Ferry_model->findByName($route);
            $data['date'] = $date;
            $data['disabled'] = '';

          } else {
            $today = date("d/m/Y");

            $RoutesWithDailyDepartures = [];
            $data['disabled'] = 'disabled';
			$data['route'] = $route;
			$data['routeId'] = $this->Ferry_model->findByName($route);
			
			


            for($i=0; $i<count($routes); $i++){
              $route = $routes[$i]->descrizione;
              
              $departures = $this->Ferry_model->findDeparturesByDate($today, $route);
              

              if(!empty($departures)){

                for ($y=0; $y < count($departures); $y++) {
                  if($departures[$y]->compagnia == 'Caremar') {
                    $departures[$y]->disabled = true;
                  }
                }

                array_push($RoutesWithDailyDepartures, $departures);
              }
            }

            $data['RoutesWithDailyDepartures'] = $RoutesWithDailyDepartures;
          }
        } else {
          $data['iframe_url'] = $site;
        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );
        	switch ($this->lang->lang()) {
        	  case 'en':

              if (isset($data['route'])) {
                $data['meta_keywords'] = 'ferries, hydrofoils, ships, Naples, Procida, Ischia';
          	    $data['meta_desc'] = 'Procida Ferries: Timetables of ferries and hydrofoils between Naples and Procida. Ferry Schedules Caremar, Snav, Medmar, Gestur';
          	    $data['meta_title'] = 'Ferries '.$data['route'].' - Ferries timetable Procida';
              }
              else {
                $data['meta_keywords'] = 'schedules, ferries, hydrofoils, ships, Naples, Procida, Ischia, Pozzuoli';
          	    $data['meta_desc'] = 'Procida Ferries: Timetables of ferries and hydrofoils between Naples and Procida. Ferry Schedules Caremar, Snav, Mediar, Gestur';
          	    $data['meta_title'] = 'Procida Ferries: ferry timetables to Procida, Bay of Naples';
              }

        	    break;

        	  case 'it':
              if (isset($data['route'])) {
                $data['meta_keywords'] = 'procida, traghetti, aliscafi';
               $data['meta_desc'] = 'Orari traghetti ed aliscafi per Procida da Napoli, Pozzuoli, Ischia. Orari Caremar, Snav, Medmar, Gestur';
               $data['meta_title'] = 'Traghetti '.$data['route'].' - Orari Traghetti Procida';
              }
              else {
                $data['meta_keywords'] = 'procida, traghetti, aliscafi';
               $data['meta_desc'] = 'Orari traghetti ed aliscafi per Procida da Napoli, Pozzuoli, Ischia. Orari Caremar, Snav, Medmar, Gestur';
               $data['meta_title'] = 'Traghetti Procida - Orari traghetti ed aliscafi da e per Procida';
              }

        	    break;
        	}



        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/useful_info_timetables', $data);
        $this->load->view('frontend/includes/footer', $data);

    }


    function searchDepartures($date, $route){
      $this->load->model('admin/Ferry_model');

      $trattaInfo = $this->Ferry_model->findByName($route);
      $tratta = $trattaInfo['external_id'];

      if ($route == 'Napoli-Procida' || $route == 'Napoli Beverello-Procida') {
        $alldata = [];
        $externalId = $trattaInfo['external_id'];
        $externalId_next = 318;
        $id_array = array('0' => $externalId,'1' => $externalId_next);
      }

      elseif ($route == 'Procida-Napoli') {
        $alldata = [];
        $externalId = $trattaInfo['external_id'];
        $externalId_next = 319;
        $id_array = array('0' => $externalId,'1' => $externalId_next);
      }

      elseif ($route == 'Ischia-Procida') {
        $alldata = [];
        $externalId = $trattaInfo['external_id'];
        $externalId_next = 1412;
        $id_array = array('0' => $externalId,'1' => $externalId_next);
      }

      elseif ($route == 'Procida-Ischia') {
        $alldata = [];
        $externalId = $trattaInfo['external_id'];
        $externalId_next = 1413;
        $id_array = array('0' => $externalId,'1' => $externalId_next);
      }
      else{
        $id_array = array('0' => $trattaInfo['external_id']);
      }

      foreach ($id_array as $key => $id) {
          $ferryInfo = $this->Ferry_model->findById($id);
          $route_name = $ferryInfo['descrizione'];
      $params = [
        'lang'          => $this->lang->lang(),
        'user'          => $this->user,
        'password'      => $this->pass,
        'dataAndata'    => $date.'T01:00:00',
        'trattaAndata'  => $tratta
      ];

      $wsdl = 'https://www.traghettilines.it/webaffiliation.asmx?wsdl';

      try {
        $soap = new SoapClient($wsdl, $this->options);
        $data = $soap->getDepartures($params);
      } catch(Exception $e) {
        die($e->getMessage());
      }

      if(isset($data->getDeparturesResult->partenza)){

        $departuresData = (array) $data->getDeparturesResult->partenza;

      }

      $data = array('0' => $departuresData);
      foreach ( $data as $key => $value) {
              foreach ($value as $skey => $svalue) {
              
                $svalue->route_name = $route_name;
                $alldata[0][] = $svalue;
              }
            }
          }
        return $alldata; 
    }

    public function transferCalculation(){
      $transfer_type    = $this->input->post('transfer_type');
      $adults           = $this->input->post('adults');
      $children         = $this->input->post('children');
      $persons          = $this->input->post('adults') + $this->input->post('children');
      $address          = $this->input->post('address');
      
      
      $pickUpPosints =  array_keys(json_decode (PICK_UP_POINTS,True));
      $json = [
        'adults'=> $adults ,
        'children'  => $children ,
        'total_person' => $persons,
        'per_person' => 0,
        'total_amount' => 0
      ];
      switch ($transfer_type) {
        case 'car_transfer':
        switch ($address) { 
          case $pickUpPosints[0]: 
              switch ($persons) { 
                case 1: 
                    $json['per_person']    = 360;
                    $json['total_amount']  =  360 * $persons;
                    break; 
                case 2: 
                    $json['per_person']    =  190;
                    $json['total_amount']  =  190 * $persons;
                    break; 
                case 3: 
                    $json['per_person']    =  140;
                    $json['total_amount']  =  140 * $persons;
                    break; 
                case 4: 
                    $json['per_person']    =  120;
                    $json['total_amount']  =  120 * $persons;
                    break; 
                case 5: 
                    $json['per_person']    =  110;
                    $json['total_amount']  =  110 * $persons;
                    break;
                case 6: 
                    $json['per_person']    =  95;
                    $json['total_amount']  =  95 * $persons;
                    break; 
                default:
                    
                    break; 
                } 
              break; 
          case $pickUpPosints[1]: 
              switch ($persons) { 
                case 1: 
                    $json['per_person']    =  360;
                    $json['total_amount']  =  360 * $persons;    
                    break; 
                case 2: 
                    $json['per_person']    =  190;
                    $json['total_amount']  =  190 * $persons;    
                    break; 
                case 3: 
                    $json['per_person']    =  140;
                    $json['total_amount']  =  140 * $persons;    
                    break; 
                case 4: 
                    $json['per_person']    =  120;
                    $json['total_amount']  =  120 * $persons;    
                    break; 
                case 5: 
                    $json['per_person']    =  110;
                    $json['total_amount']  =  110 * $persons;    
                    break;
                case 6: 
                    $json['per_person']    =  95;
                    $json['total_amount']  =  95 * $persons;    
                    break; 
                default:
                    
                    break; 
                }
              break; 
          case $pickUpPosints[2]: 
              switch ($persons) { 
                case 1: 
                    $json['per_person']    =  65;
                    $json['total_amount']  =  65 * $persons;    
                    break; 
                case 2: 
                    $json['per_person']    =  45;
                    $json['total_amount']  =  45 * $persons;    
                    break; 
                case 3: 
                    $json['per_person']    =  43;
                    $json['total_amount']  =  43 * $persons;    
                    break; 
                case 4: 
                    $json['per_person']    =  39;
                    $json['total_amount']  =  39 * $persons;    
                    break; 
                case 5: 
                    $json['per_person']    =  35;
                    $json['total_amount']  =  35 * $persons;    
                    break;
                case 6: 
                    $json['per_person']    =  35;
                    $json['total_amount']  =  35 * $persons;    
                    break; 
                default:
                    
                    break; 
                }
              break; 
          case $pickUpPosints[3]: 
              switch ($persons) { 
                case 1: 
                    $json['per_person']    =  65;
                    $json['total_amount']  =  65 * $persons;    
                    break; 
                case 2: 
                    $json['per_person']    =  45;
                    $json['total_amount']  =  45 * $persons;    
                    break; 
                case 3: 
                    $json['per_person']    =  43;
                    $json['total_amount']  =  43 * $persons;    
                    break; 
                case 4: 
                    $json['per_person']    =  39;
                    $json['total_amount']  =  39 * $persons;    
                    break; 
                case 5: 
                    $json['per_person']    =  35;
                    $json['total_amount']  =  35 * $persons;    
                    break;
                case 6: 
                    $json['per_person']    =  35;
                    $json['total_amount']  =  35 * $persons;    
                    break; 
                default:
                    
                    break; 
                }
              break; 
          }
          break;
        case 'yacht_transfer':
          switch ($address) { 
            case $pickUpPosints[0]: 
              switch ($persons) { 
                case 1: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break; 
                case 2: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break; 
                case 3: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break; 
                case 4: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break; 
                case 5: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break;
                case 6: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break; 
                default:
                    
                    break; 
                } 
              break; 
            case $pickUpPosints[1]: 
              switch ($persons) { 
                case 1: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break; 
                case 2: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break; 
                case 3: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break; 
                case 4: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break; 
                case 5: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break;
                case 6: 
                    $json['per_person']    =  1200;
                    $json['total_amount']  =  1200 * $persons;    
                    break; 
                default:
                    
                    break; 
                }
                break; 
            case $pickUpPosints[2]: 
                switch ($persons) { 
                  case 1: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break; 
                  case 2: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break; 
                  case 3: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break; 
                  case 4: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break; 
                  case 5: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break;
                  case 6: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break; 
                  default:
                      
                      break; 
                  }
                break; 
            case $pickUpPosints[3]: 
                switch ($persons) { 
                  case 1: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break; 
                  case 2: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break; 
                  case 3: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break; 
                  case 4: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break; 
                  case 5: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break;
                  case 6: 
                      $json['per_person']    =  900;
                      $json['total_amount']  =  900 * $persons;    
                      break; 
                  default:
                      
                      break; 
                  }
                break; 
            } 
          break;
      }
     echo  json_encode ($json,True);
    }

}
