<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Welcome extends CI_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->model('admin/Ads_model');
        $this->load->model('Orders_model');
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');

    }

    public function index(){


        $this->lang->load('Welcomepage');
        $this->load->model('Menu_model');
        $this->load->model('admin/Settings_model');
        $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');
        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');
        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Procida_model');
        $this->load->model('Service_model');
        $this->load->model('Agencies_model');
        $this->load->model('Nightlife_model');
		      $this->load->model('admin/Block_model');
  		$this->load->model('Cart_model');

  		$this->load->helper('cookie');
   
  $this->load->model('Tour_model');
        $this->lang->load('Tourspage');

        $data['tours'] = $this->Tour_model->getActiveTours();
	    $cartCookie = $this->input->cookie('prcrt');

	    if(!empty($cartCookie)){
	      $items = $this->Cart_model->getUserCart($cartCookie, '');
	      $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
	      $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
	      $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
        if(!empty($tours)){
          for ($i=0; $i < count($tours); $i++) {
            $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
          }
        }
	    } else {
	      $items = [];
	      $hotelCart = [];
	      $tranferCart = [];
	      $tours = []; // show random tours
	    }
  		$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );
    
  		$data['blocks'] = $this->Block_model->getBlocks(4,$this->lang->lang());
        

      $this->load->model('admin/Blog_model');

      $data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());
      

      $data['counts']['shopping'] = $this->Shope_model->totalActiveShops();

      $data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();

      $data['counts']['escursions-and-visit'] = $this->Service_model->totalActiveServices();

      $data['counts']['agencies'] = $this->Agencies_model->totalActiveServices();

      $data['counts']['watersports'] = 0;

      $data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();

      $data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

      $data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();

      $data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

      $data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();

      $data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();

      $data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();

    

      $settings = (array)$this->Settings_model->getsettings();
      

      $data['meta_keywords'] = $settings['meta_keywords_'.$this->lang->lang()];
      

      $data['meta_desc'] = $settings['meta_desc_'.$this->lang->lang()];

      $data['meta_title'] = $settings['meta_tag_'.$this->lang->lang()];

      $data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());

      $this->load->model('admin/Location_model');

      $location = $data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        
      $this->load->model('admin/Beach_model');

      $beach = $data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
    
      $this->load->model('Places_of_interest_model');

      $places=$data['m_poi'] = $this->Places_of_interest_model->getMenuList();

    

  		$rand_keys1 = array_rand($location, 5);
  		for($i=0;$i<count($rand_keys1);$i++)
  		{
  			$listLocation[]=$location[$rand_keys1[$i]];
  		}

  		$rand_keys2 = array_rand($beach, 5);
  		for($i=0;$i<count($rand_keys2);$i++)
  		{
  			$listBeach[]=$beach[$rand_keys2[$i]];
  		}

  		$rand_keys3 = array_rand($places, 5);
  		for($i=0;$i<count($rand_keys3);$i++)
  		{
  			$listPlaces[]=$places[$rand_keys3[$i]];
  		}
  		$data['listLocation']=$listLocation;
  		$data['listBeach']=$listBeach;
  		$data['listPlaces']=$listPlaces;

  		$this->load->library('googlemaps');

  		$config['center'] = '37.4419, -122.1419';
  		$config['zoom'] = 'auto';
  		$config['infowindowMaxWidth'] = 280;
  		$config['onload'] = 'onLoadMap();';
  		$config['backgroundColor'] = 'transparent';

  		$this->googlemaps->initialize($config);

  		foreach($location as $loc)
  		{
  			$descriptions = $this->Location_model->locationMapDescription($loc->id);

  			if($this->lang->lang() == 'it') {
  				$desc = base64_decode($descriptions[2]->description);
  				$desc = strip_tags($desc);
  				$desc = utf8_encode($desc);
  				$desc = str_replace("\n", "", $desc);
  				$desc = substr($desc, 0, 150);
  				$placeDescription = $desc.'...';
  				$placeDescription = str_replace("\n", "", $placeDescription);
  				$url = $descriptions[2]->rewrite_url;
  			} else {
  				$desc = base64_decode($descriptions[0]->description);
  				$desc = strip_tags($desc);
  				$desc = utf8_encode($desc);
  				$desc = str_replace("\n", "", $desc);
  				$desc = substr($desc, 0, 150);
  				$placeDescription = $desc.'...';
  				$placeDescription = str_replace("\n", "", $placeDescription);
  				$url = $descriptions[0]->rewrite_url;
  			}

  			$infoWindowHTML = '<div class="mainInfoWindow"><img class="img-responsive mapImg" src="'. base_url() .'uploads/locations/'. $loc->p_image .'" /><div class="infoWindowTitle text-center"><h4>'. $loc->title .'</h4></div><div class="infoWindowDescription">'.$placeDescription.'<a class="btn btn-ferry" href="'.base_url(). $this->lang->lang() . '/' . str_replace(' ', '-', strtolower(lang('what-to-visit'))) . '/' . str_replace(' ', '-', strtolower($url)) . '.html"> dettagli </a></div></div>';


  			$marker = array();
  			$marker['position'] = "$loc->lat, $loc->lng";
  			$marker['infowindow_content'] = "$infoWindowHTML";
  			$marker['icon'] = '/assets/images/locations.png';
  		
  		}

  		foreach($beach as $bea)
  		{
  			$descriptions = $this->Beach_model->beachMapDescription($bea->id);

  			if ($this->lang->lang() == 'it') {
  				$placeTitle = $bea->title;
  				$desc = base64_decode($descriptions[0]->description);
  				$desc = utf8_encode($desc);
  				$desc = strip_tags($desc);
  				$desc = str_replace("\n", "", $desc);
  				$desc = substr($desc, 0, 150);
  				$placeDescription = $desc.'...';
  				$placeDescription = str_replace("\n", "", $placeDescription);
  				$url = $descriptions[1]->rewrite_url;
  			} else {
  				$placeTitle = $bea->enTitle;
  				$desc = base64_decode($descriptions[1]->description);
  				$desc = utf8_encode($desc);
  				$desc = strip_tags($desc);
  				$desc = str_replace("\n", "", $desc);
  				$desc = substr($desc, 0, 150);
  				$placeDescription = $desc.'...';
  				$placeDescription = str_replace("\n", "", $placeDescription);
  				$url = $descriptions[0]->rewrite_url;
  			}

  			$infoWindowHTML = '<div class="mainInfoWindow"><img class="img-responsive mapImg" src="'. base_url() .'uploads/beaches/'. $bea->p_thumbnail .'" /><div class="infoWindowTitle text-center"><h4>'. $placeTitle .'</h4></div><div class="infoWindowDescription">'.$placeDescription.'<a class="btn btn-ferry" href="'.base_url(). $this->lang->lang() . '/' . str_replace(' ', '-', strtolower(lang('beach'))) . '/' . str_replace(' ', '-', strtolower($url)) . '.html"> dettagli </a></div></div>';

  			$marker = array();
  			$marker['position'] = "$bea->lat, $bea->lng";
  			$marker['infowindow_content'] = "$infoWindowHTML";
  			$marker['icon'] = '/assets/images/beaches.png';
  			$this->googlemaps->add_marker($marker);
  		}

  		foreach($places as $place)
  		{
  			if ($this->lang->lang() == 'it') {
  				$placeTitle = $place->itTitle;

  				$desc = base64_decode($place->itDescription);
  				$desc = strip_tags($desc);
  				$desc = utf8_encode($desc);
  				$desc = str_replace("\n", "", $desc);
  				$desc = substr($desc, 0, 150);
  				$placeDescription = $desc.'...';
  				$placeDescription = str_replace("\n", "", $placeDescription);
  				$rewrite_url = $place->rewrite_url_it;

  			} else if ($this->lang->lang() == 'du'){
  				$placeTitle = $place->duTitle;

  				$desc = base64_decode($place->duDescription);
  				$desc = strip_tags($desc);
  				$desc = utf8_encode($desc);
  				$desc = str_replace("\n", "", $desc);
  				$desc = substr($desc, 0, 150);
  				$placeDescription = $desc.'...';
  				$placeDescription = str_replace("\n", "", $placeDescription);
  				$rewrite_url = $place->rewrite_url_du;

  			} else {
  				$placeTitle = $place->enTitle;
  				$desc = base64_decode($place->enDescription);
  				$desc = strip_tags($desc);
  				$desc = utf8_encode($desc);
  				$desc = str_replace("\n", "", $desc);
  				$desc = substr($desc, 0, 150);
  				$placeDescription = $desc.'...';
  				$placeDescription = str_replace("\n", "", $placeDescription);
  				$rewrite_url = $place->rewrite_url_en;
  			}

  			$infoWindowHTML = '<div class="mainInfoWindow"><img class="img-responsive mapImg" src="'. base_url() .'uploads/places_of_interest/'. $place->photo .'" /><div class="infoWindowTitle text-center"><h4>'. $placeTitle .'</h4></div><div class="infoWindowDescription">'.$placeDescription.'<a class="btn btn-ferry" href="'.base_url(). $this->lang->lang() . '/' . str_replace(' ', '-', strtolower(lang('places-of-interest'))) . '/' . str_replace(' ', '-', strtolower($rewrite_url)) . '.html"> dettagli </a></div></div>';

  			$marker = array();

  			$marker['position'] = "$place->lat, $place->lng";
  			$marker['infowindow_content'] = "$infoWindowHTML";
  			$marker['icon'] = '/assets/images/places.png';
  			$this->googlemaps->add_marker($marker);
  		}




      
  		$data['map'] = $this->googlemaps->create_map();

    


  		/*-----------------------------------------------*/

      $this->load->model('Tour_model');
      $tours = $this->Tour_model-> getRandomTour(4);
      $data['home_tours'] = $tours;

      $dir = 'assets/video';
      $files = glob($dir . '/*.*');
      $file = array_rand($files);
      $image = explode('.',$files[$file]);
      $data['banner_video']= $image[0];

      if (count($tours) > 0){//ci sono tour non bisogna caricare il javascript del video
        $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/js/modernizr.js')

            );
      }
      else {
        $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/js/modernizr.js'),
                        array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
                Array('type' => 'file', 'src' => base_url() . 'assets/js/video_header.js'),
                Array('type' => 'inline', 'src' => " $(document).ready(function() {
    					HeaderVideo.init({
    				      container: $('.header-video'),
    				      header: $('.header-video--media'),
    				      videoTrigger: $('#video-trigger'),
    				      autoPlayVideo: false
    				    });

    				});")
            );
      }
    //   $this->load->model('admin/Orders_model'); // comment by vpn
    //   $this->load->model('customer/Orders_model');
	  
    //   $business = $this->Orders_model->getActiveRandomBusinessPage();      
    //   $data['businessPages'] = $business;    
      

      $this->load->view('frontend/includes/header', $data);

      $this->load->view('frontend/content/home', $data);

      $this->load->view('frontend/includes/footer', $data);

  }

}
