<?php
/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Blog extends CI_Controller {
    private $data;
    public function __construct() {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->model('admin/Location_model');
        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model("Places_of_interest_model");
        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
        $this->load->model('admin/Ads_model');
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');
        $this->lang->load('Blogpage');
        $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');
        $this->load->model('Cart_model');
        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');

        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();
        $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());
    }

    public function index(){
        $data = $this->data;
        $this->load->model('Procida_model');
        $data['posts'] = $this->Blog_model->getPosts(10,$this->lang->lang());
        $this->load->library('pagination');
        $config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $config['total_rows'] = $this->Blog_model->totalPosts();
        $config['per_page'] = 10;
        $config['base_url'] = base_url().$this->lang->lang().'/blog/';
        $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location','o.status=\'paid\'', 6);
        switch ($this->lang->lang()) {
            case 'en':
                $data['meta_keywords'] = 'Procida, events, festivals, concerts, exhibitions, processions, folklore';
                $data['meta_desc'] = 'All events in Procida! Processions, festivals, exhibitions, festivals, concerts, traditions.';
                $data['meta_title'] = 'Blog & News';
                break;
            case 'it':
                $data['meta_keywords'] = 'procida, eventi, manifestazioni, concerti, mostre, processioni, folclore,';
                $data['meta_desc'] = 'Tutti gli eventi a Procida! Processioni, sagre, mostre, rassegne, concerti, tradizioni folkloristiche.';
                $data['meta_title'] = 'Blog & News';
                break;
        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        $data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/blog', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    public function post($id){
        $this->load->model('Procida_model');
    		$data = $this->data;
    		$data['post'] = $this->Blog_model->getPost($id,'',$this->lang->lang());
        $temp = (array)$data['post'];
        $data['meta_keywords'] = $temp['meta_keywords'];
        $data['meta_title'] = $temp['meta_tag'];
        $data['meta_desc'] = $temp['meta_desc'];
        if(!$data['post']){
            show_404();
            return;
        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location','o.status=\'paid\'', 6);
         $data['facebook_sdk'] = '<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>';
        $this->load->model('Menu_model');
        $data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/blog-single-b', $data);
        $this->load->view('frontend/includes/footer', $data);
    }
}
