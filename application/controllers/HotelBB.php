<?php

class HotelBB extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('HotelBB_model');
        /* Loading menu data o.o */
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation_lang');
        $this->load->model('Menu_model');
        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->model('admin/Location_model');
        $this->load->model('Procida_model');
        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model("Places_of_interest_model");
        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
        $this->load->model('admin/Ads_model');
        $this->load->model('Orders_model');
        $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');
        $this->load->model('Cart_model');
        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');
        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();

        $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());
    }



    public function getHotels() {

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, "https://www.hbb.bz/api_portal/hotel_inventory");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "reseller=16&lang=it");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $server_output = curl_exec ($ch);

      curl_close ($ch);

      $server_output = json_decode($server_output);
      $hotels = $server_output->hotels;

      if(!empty($hotels)){
        $this->HotelBB_model->deleteTable('hotel_bb');
        $this->HotelBB_model->deleteTable('hotel_bb_images');

        foreach ($hotels as $hotel) {
          $newHotel = array( 'partner_id' => $hotel->partner_id,
                             'name' => $hotel->name,
                             'street' => $hotel->street,
                             'city' => $hotel->city,
                             'postal_code' => $hotel->postal_code,
                             'state' => $hotel->state,
                             'country' => $hotel->country,
                             'email' => $hotel->email,
                             'phone' => $hotel->phone,
                             'stars' => $hotel->stars,
                             'type' => $hotel->type);

          $savedHotelId = $this->HotelBB_model->saveNewHotel($newHotel);

          $newHotelImages = $hotel->images;

          foreach ($newHotelImages as $image) {
            $newHotelImage = array('hotel_bb_id' => $savedHotelId, 'partner_id' => $hotel->partner_id, 'name' => $image->name, 'ord' => $image->ord );
            $this->HotelBB_model->saveNewHotelImages($newHotelImage);
          }
        }

      }

    }

    public function search(){

      $this->lang->load('Hotelspage_lang');
      $start_date = $this->input->post('datastart');
      $end_date = $this->input->post('dataend');
      $party = $this->input->post('jsonparty');
      $party = str_replace(' ', '', $party);
      /* Switch date format from d/m/Y to Y-m-d  */
      $exploded_startDate = explode("/", $start_date);
      $newStartDate = "$exploded_startDate[2]-$exploded_startDate[0]-$exploded_startDate[1]";
      $exploded_endDate = explode("/", $end_date);
      $newEndDate = "$exploded_endDate[2]-$exploded_endDate[0]-$exploded_endDate[1]";
      $queryString = "reseller=16&lang=en&start_date=$newStartDate&end_date=$newEndDate&party=$party";

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, "https://www.hbb.bz/api_portal/hotel_availability");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $queryString);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $server_output = curl_exec ($ch);

      curl_close ($ch);

      $server_output = json_decode($server_output);

      // in $server_output we got the hotel_id and the price for that reservation.
      // we go to take the hotel information from our database.

      $this->load->library('googlemaps');

		$config['center'] = '40.7569952, 14.0120653';
		$config['zoom'] = 14;
		$config['map_height'] = '550px';
		$config['onload'] = 'onLoadMap();';
		$config['backgroundColor'] = 'transparent';

		$this->googlemaps->initialize($config);

      $hotels = array();

      foreach ($server_output->hotels as $hotel) {

          $fullHotel = $this->HotelBB_model->getHotelInfo($hotel->hotel_id);

          if(!empty($fullHotel)){
            $fullHotel->price = $hotel->price;
            $fullHotel->images = $this->HotelBB_model->getHotelImages($fullHotel->hotel_bb_id);
            $fullHotel->mainImage = $fullHotel->images[0];
            array_push($hotels, $fullHotel);
            // $infoWindowHTML = '<div class="mainInfoWindow"></div>';
            if($fullHotel->lat && $fullHotel->lng){
              $marker = array();
              $marker['position'] = "$fullHotel->lat, $fullHotel->lng";
              // $marker['infowindow_content'] = "$infoWindowHTML";
              $marker['icon'] = '/assets/images/hotels.png';
              $this->googlemaps->add_marker($marker);
            }
          }
      }

      $this->data['hotels'] = $hotels;
      $data = $this->data;
      $data['map'] = $this->googlemaps->create_map();
      $data['searchData'] = array('start_date' => $start_date, 'end_date' => $end_date, 'party' => $party);

      $this->load->helper('cookie');

      $this->load->model('Procida_model');
      $cartCookie = $this->input->cookie('prcrt');

      if(!empty($cartCookie)){
        $items = $this->Cart_model->getUserCart($cartCookie, '');
        $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
        $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
        $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
        if(!empty($tours)){
          for ($i=0; $i < count($tours); $i++) {
            $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
          }
        }
      } else {
        $items = [];
        $hotelCart = [];
        $tranferCart = [];
        $tours = []; // show random tours
      }
      	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );


      $this->load->view('frontend/includes/header', $data);
      $this->load->view('frontend/content/hotelbb-searchResults', $data);
      $this->load->view('frontend/includes/footer', $data);
    }

    public function detail(){
      $id = $this->uri->segment(4);
      $this->lang->load('Hotelspage_lang');
      $lang = $this->lang->lang();
      $this->data['fullHotel'] = $this->HotelBB_model->getHotelInfo($id);
      $this->data['fullHotel']->images = $this->HotelBB_model->getHotelImages($id);
      $start_date = $this->input->post('start_date');
      $end_date = $this->input->post('end_date');
      $party = $this->input->post('party');
      $data = $this->data;

      if(!empty($start_date)){
        $exploded_startDate = explode("/", $start_date);
        $newStartDate = "$exploded_startDate[2]-$exploded_startDate[0]-$exploded_startDate[1]";
        $exploded_endDate = explode("/", $end_date);
        $newEndDate = "$exploded_endDate[2]-$exploded_endDate[0]-$exploded_endDate[1]";
        $queryString = "hotel_id=$id&reseller=16&lang=$lang&start_date=$newStartDate&end_date=$newEndDate&party=$party";

        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $data['party'] = $party;

        $bookingInfo = json_decode($party);
        $info['rooms'] = count($bookingInfo);
        $info['people'] = 0;
        $info['children'] = 0;

        for($i=0; $i<$info['rooms']; $i++){
          $info['people'] = $info['people'] + $bookingInfo[$i]->adults;
          if(isset($bookingInfo[$i]->children)){
            for($j=0; $j<count($bookingInfo[$i]->children); $j++){
              $info['children'] = $info['children'] + 1;
            }
          }
        }

        $data['info'] = $info;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://www.hbb.bz/api_portal/booking_availability");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $queryString);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);
        $server_output = json_decode($server_output);

        $server_output = (array) $server_output;
        $data['rooms'] = (array) $server_output['room_types_array'];
      }

      $places = $this->Procida_model->getPlacesForSideBar($data['fullHotel']->lat,$data['fullHotel']->lng);

      foreach ($places as  $place) {
        # code...
        $place->itDescription = substr(strip_tags(base64_decode($place->itDescription)), 0, 150);
        $place->itDescription = $place->itDescription.'...';

        $place->enDescription = substr(strip_tags(base64_decode($place->enDescription)), 0, 150);
        $place->enDescription = $place->enDescription.'...';
      }

		  $data['places'] = $places;



      $data['cssfiles'] = array(
        array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
        array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
      );

      $data['jsfiles'] = array(
        array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
        array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
        array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js"),
        array('type' => 'file', 'src' => base_url() . "assets/js/roomDetail.js"),
        array('type' => 'file', 'src' => base_url() . "assets/js/NearbyMap.js"),
        array('type' => 'inline', 'src' => '$(document).ready(function(){ NearbyMap.init('.$data["fullHotel"]->lat.','.$data["fullHotel"]->lng.'); });')
      );

      $this->load->helper('cookie');

      $this->load->model('Procida_model');
      $cartCookie = $this->input->cookie('prcrt');

      if(!empty($cartCookie)){
        $items = $this->Cart_model->getUserCart($cartCookie, '');
        $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
        $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
        $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
        if(!empty($tours)){
          for ($i=0; $i < count($tours); $i++) {
            $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
          }
        }
      } else {
        $items = [];
        $hotelCart = [];
        $tranferCart = [];
        $tours = []; // show random tours
      }
      	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

      $this->load->view('frontend/includes/header', $data);
      $this->load->view('frontend/content/hotelbb-detail', $data);
      $this->load->view('frontend/includes/footer', $data);
    }
}
