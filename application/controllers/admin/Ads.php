<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ads extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')  && $this->session->userdata('usertype') == 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('full_name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('admin/Ads_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $config = array(
            array('field' => 'id',
                'label' => 'Add\'s ID',
                'rules' => 'required'
            ),
            array('field' => 'type',
                'label' => 'Add Type',
                'rules' => 'required'
            )

        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $src = $this->input->post('src');
            $ad = array('type' => $this->input->post('type'),
                'src' => $src != '' ? base64_encode($src) : ''
            );
            $this->Ads_model->update($this->input->post('id'), $ad);
            //success and redirect on same page
            $this->session->set_flashdata(Array('msg' => "Hai aggiornato con successo le informazioni dell'AdS",
                'type' => 'success'));
            redirect('admin/ads');
        } else {
            if (strlen(validation_errors()) > 0) {
                $data['msg'] = validation_errors();
                $data['type'] = 'danger';
            }
            $this->load->model('Categories');
            $data['all_categories'] = $this->Categories->getAllList();
            $data['ads'] = $this->Ads_model->getAll();
            $data['cpage'] = 'ads';
            $data['cssfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
            );

            $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
                Array('type' => 'inline', 'src' => " $(document).ready(function() { $('#ads-dtable').dataTable(); });")
            );
            $this->load->view('admin/includes/header',$data);
            $this->load->view('admin/includes/sidebar', $data);
            $this->load->view('admin/contents/ads/index', $data);
            $this->load->view('admin/includes/footer',$data);
        }
    }

}
