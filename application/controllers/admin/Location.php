<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Location extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')  && $this->session->userdata('usertype') == 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('full_name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('admin/Location_model');
        $this->load->helper('form');
        $data['locations'] = $this->Location_model->getLocations(false);
        $data['cpage'] = 'locations';
        $data['sub_cpage'] = 'all-locations';
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#locations-dtable').dataTable();
                                                        });")
        );
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/location/locations', $data);
        $this->load->view('admin/includes/footer');
    }

    public function add() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('admin/Location_model');
        $this->load->helper('form');
        $this->load->library('form_validation');

        $config = array(
            array('field' => 'lat','label' => 'Latitude','rules' => 'required|decimal'),
            array('field' => 'lng','label' => 'Longitude','rules' => 'required|decimal'),

            array('field' => 'title_en','label' => 'Title [en]','rules' => 'min_length[2]|max_length[50]'),
            array('field' => 'title_du','label' => 'Title [du]','rules' => 'min_length[2]|max_length[50]'),
            array('field' => 'title_it','label' => 'Title [it]','rules' => 'required|min_length[2]|max_length[50]'),

            array('field' => 'description_en','label' => 'Description [en]','rules' => 'max_length[20000]'),
            array('field' => 'description_du','label' => 'Description [du]','rules' => 'max_length[20000]'),
            array('field' => 'description_it','label' => 'Description [it]','rules' => 'required|max_length[20000]'),

            array('field' => 'meta_keywords_en','label' => 'Meta Keywords [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_keywords_du','label' => 'Meta Keywords [du]','rules' => 'max_length[250]'),
            array('field' => 'meta_keywords_it','label' => 'Meta Keywords [it]','rules' => 'required|max_length[250]'),

            array('field' => 'rewrite_url_en','label' => 'URL Rewrite [en]','rules' => 'max_length[250]'),
            array('field' => 'rewrite_url_du','label' => 'URL Rewrite [du]','rules' => 'max_length[250]'),
            array('field' => 'rewrite_url_it','label' => 'URL Rewrite [it]','rules' => 'required|max_length[250]'),

            array('field' => 'meta_en','label' => 'Meta [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_du','label' => 'Meta [du]','rules' => 'max_length[250]'),
            array('field' => 'meta_it','label' => 'Meta [it]','rules' => 'required|max_length[250]'),

            array('field' => 'meta_desc_it','label' => 'Meta Description [it]','rules' => 'required|max_length[1000]'),
            array('field' => 'meta_desc_en','label' => 'Meta Description [en]','rules' => 'max_length[1000]'),
            array('field' => 'meta_desc_du','label' => 'Meta Description [du]','rules' => 'max_length[1000]'));

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $id = $this->Location_model->insert();
            $this->session->set_flashdata(Array('msg' => 'Hai aggiunto con successo una nuova Località',
                'type' => 'success', 'model' => 'login'));
            redirect('admin/location');
        } else {

            if (strlen(validation_errors()) > 0) {
                $data['msg'] = validation_errors();
                $data['type'] = 'danger';
            }
            $data['cpage'] = 'locations';
            $data['sub_cpage'] = 'new-location';
            $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
                Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });")
            );
            $this->load->model('Categories');
            $data['all_categories'] = $this->Categories->getAllList();
            $this->load->view('admin/includes/header');
            $this->load->view('admin/includes/sidebar', $data);
            $this->load->view('admin/contents/location/add', $data);
            $this->load->view('admin/includes/footer');
        }
    }

    public function edit($id) {
        $this->load->model('admin/Location_model');
        $location = $this->Location_model->getLocation($id, NULL, false);
        if ($location) {
            $data['location'] = $location;
        } else {
            show_404();
            return;
        }

        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $config = array(
            array('field' => 'lat','label' => 'Latitude','rules' => 'required|decimal'),
            array('field' => 'lng','label' => 'Longitude','rules' => 'required|decimal'),

            array('field' => 'title_en','label' => 'Title [en]','rules' => 'min_length[2]|max_length[100]'),
            array('field' => 'title_du','label' => 'Title [du]','rules' => 'min_length[2]|max_length[100]'),
            array('field' => 'title_it','label' => 'Title [it]','rules' => 'required|min_length[2]|max_length[100]'),

            array('field' => 'description_en','label' => 'Description [en]','rules' => 'max_length[20000]'),
            array('field' => 'description_du','label' => 'Description [du]','rules' => 'max_length[20000]'),
            array('field' => 'description_it','label' => 'Description [it]','rules' => 'required|max_length[20000]'),

            array('field' => 'meta_keywords_en','label' => 'Meta Keywords [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_keywords_du','label' => 'Meta Keywords [du]','rules' => 'max_length[250]'),
            array('field' => 'meta_keywords_it','label' => 'Meta Keywords [it]','rules' => 'required|max_length[250]'),

            array('field' => 'rewrite_url_en','label' => 'URL Rewrite [en]','rules' => 'max_length[250]'),
            array('field' => 'rewrite_url_du','label' => 'URL Rewrite [du]','rules' => 'max_length[250]'),
            array('field' => 'rewrite_url_it','label' => 'URL Rewrite [it]','rules' => 'required|max_length[250]'),

            array('field' => 'meta_en','label' => 'Meta [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_du','label' => 'Meta [du]','rules' => 'max_length[250]'),
            array('field' => 'meta_it','label' => 'Meta [it]','rules' => 'required|max_length[250]'),

            array('field' => 'meta_desc_en','label' => 'Meta Description [en]','rules' => 'max_length[1000]'),
            array('field' => 'meta_desc_du','label' => 'Meta Description [du]','rules' => 'max_length[1000]'),
            array('field' => 'meta_desc_it','label' => 'Meta Description [it]','rules' => 'required|max_length[1000]'));

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->Location_model->update($id);
            $this->session->set_flashdata(Array('msg' => 'Hai aggiornato con successo la Località',
                'type' => 'success'));
            redirect('admin/location/edit/' . $id);
        } else {
            if (strlen(validation_errors()) > 0) {
                $data['msg'] = validation_errors();
                $data['type'] = 'danger';
            }
            $data['cssfiles'] = array(
                Array('type' => 'file', 'src' => '//blueimp.github.io/Gallery/css/blueimp-gallery.min.css'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/fileupload/jquery.fileupload.css'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/fileupload/jquery.fileupload-ui.css')
            );
            $data['cpage'] = 'locations';

            $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
                Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });"),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/vendor/jquery.ui.widget.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.iframe-transport.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-process.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-image.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-audio.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-video.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-validate.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-ui.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/main.js')
            );
            $this->load->model('Categories');
            $data['all_categories'] = $this->Categories->getAllList();
            $this->load->view('admin/includes/header', $data);
            $this->load->view('admin/includes/sidebar', $data);
            $this->load->view('admin/contents/location/edit', $data);
            $this->load->view('admin/includes/footer', $data);
        }
    }

    public function updatethumbnail() {
        // var_dump($_POST);exit();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('admin/Location_model');
        $config = array(
            array('field' => 'id', 'label' => 'Location ID', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $id = $this->input->post('id');
            if ($_FILES && $_FILES['p_thumbnail']['name']) {
                $config['upload_path'] = './uploads/locations/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '5000';
                $new_name = time() . $_FILES["p_thumbnail"]['name'];
                $config['file_name'] = $new_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('p_thumbnail')) {
                    $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                        'type' => 'danger'));
                    redirect('admin/location/edit/' . $id);
                } else {
                    $this->Location_model->update_image($id, array('p_image' => $new_name));
                    $this->session->set_flashdata(Array('msg' => 'hai aggiunto con successo l\'immagine di Thumbnail.',
                        'type' => 'success'));
                    redirect('admin/location/edit/' . $id);
                }
            } else {
                $this->session->set_flashdata(Array('msg' => 'Per favore seleziona l\'immagine da caricare.',
                    'type' => 'danger'));
                redirect('admin/location/edit/' . $id);
            }
        } else {
            echo validation_errors();
        }
    }

    public function updatecover() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('admin/Location_model');
        $config = array(
            array('field' => 'id', 'label' => 'Location ID', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $id = $this->input->post('id');
            if ($_FILES && $_FILES['p_cover']['name']) {
                $config['upload_path'] = './uploads/locations/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '5000';
                $new_name = time() . $_FILES["p_cover"]['name'];
                $config['file_name'] = $new_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('p_cover')) {
                    $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                        'type' => 'danger'));
                    redirect('admin/location/edit/' . $id);
                } else {
                    $this->Location_model->update_image($id, array('p_cover' => $new_name));
                    $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo l\'immagine di copertina',
                        'type' => 'success'));
                    redirect('admin/location/edit/' . $id);
                }
            } else {
                $this->session->set_flashdata(Array('msg' => 'Per favore seleziona l\'immagine da caricare',
                    'type' => 'danger'));
                redirect('admin/location/edit/' . $id);
            }
        } else {
            echo validation_errors();
        }
    }

    public function deletelocation() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'Location Id',
                'rules' => 'required|numeric'
        ));
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('admin/Location_model');
            $this->Location_model->delete($this->input->post('id'));
            $this->session->set_flashdata(Array('msg' => 'Hai eliminato con successo la Località',
                'type' => 'success'));
        }
        redirect('admin/location');
    }

    public function fileupload($id) {
        error_reporting(E_ALL | E_STRICT);
        $path = 'uploads/locations/'.$id;
        if (!file_exists($path)) {
            mkdir($path);
        }
        $params = array("upload_dir" => '/' . $path . '/', "upload_url" => '/' . $path . '/');
        $this->load->library("UploadHandler", $params);
    }

    public function updatevideourl(){
         $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('admin/Location_model');
        $config = array(
            array('field' => 'id', 'label' => 'Location ID', 'rules' => 'required|numeric'),
            array('field' => 'v_url', 'label' => 'Video URL', 'rules' => 'required|url')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $id = $this->input->post('id');
            $url = $this->input->post('v_url');
            $this->Location_model->updateURL($id,$url);
            $this->session->set_flashdata(Array('msg' => 'L\'URL del video è stata aggiornata con successo',
                    'type' => 'success'));
                redirect('admin/location/edit/' . $id);
        } else {
             $this->session->set_flashdata(Array('msg' => validation_errors(),
                    'type' => 'danger'));
                redirect('admin/location/edit/' . $id);
        }
    }
}
