<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Places_of_interest extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id') && $this->session->userdata('usertype') == 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('full_name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('Places_of_interest_model');
        $this->load->helper('form');
        $data['places'] = $this->Places_of_interest_model->fetch();
        $data['cpage'] = 'places';
        $data['sub_cpage'] = 'all-places';
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#places-dtable').dataTable();
                                                        });")
        );
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/places_of_interest/places', $data);
        $this->load->view('admin/includes/footer');
    }

    public function add() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('Places_of_interest_model');
        $this->load->model('admin/Location_model');
        $data['locations'] = $this->Location_model->getLocations();
        $this->load->helper('Form');
        $this->load->library('Form_validation');

        $config = array(
            array('field' => 'lat','label' => 'Latitude','rules' => 'required|decimal'),
            array('field' => 'lng','label' => 'Longitude','rules' => 'required|decimal'),

            array('field' => 'enTitle','label' => 'Title [en]','rules' => 'min_length[2]|max_length[100]|is_unique[placesofinterest.enTitle]'),
            array('field' => 'duTitle','label' => 'Title [du]','rules' => 'min_length[2]|max_length[100]'),
            array('field' => 'itTitle','label' => 'Title [it]','rules' => 'min_length[2]|max_length[100]'),

            array('field' => 'location_id','label' => 'Location','rules' => 'required|numeric'),

            array('field' => 'enDescription','label' => 'Description [en]','rules' => 'max_length[20000]'),
            array('field' => 'duDescription','label' => 'Description [du]','rules' => 'max_length[20000]'),
            array('field' => 'itDescription','label' => 'Description [it]','rules' => 'required|max_length[20000]'),

            array('field' => 'meta_keywords_en','label' => 'Meta Keywords [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_keywords_du','label' => 'Meta Keywords [du]','rules' => 'max_length[250]'),
            array('field' => 'meta_keywords_it','label' => 'Meta Keywords [it]','rules' => 'required|max_length[250]'),

            array('field' => 'rewrite_url_en','label' => 'URL Rewrite [en]','rules' => 'max_length[250]'),
            array('field' => 'rewrite_url_du','label' => 'URL Rewrite [du]','rules' => 'max_length[250]'),
            array('field' => 'rewrite_url_it','label' => 'URL Rewrite [it]','rules' => 'required|max_length[250]'),

            array('field' => 'meta_en','label' => 'Meta [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_du','label' => 'Meta [du]','rules' => 'max_length[250]'),
            array('field' => 'meta_it','label' => 'Meta [it]','rules' => 'required|max_length[250]'),

            array('field' => 'meta_desc_it','label' => 'Meta Description [it]','rules' => 'required|max_length[1000]'),
            array('field' => 'meta_desc_en','label' => 'Meta Description [en]','rules' => 'max_length[1000]'),
            array('field' => 'meta_desc_du','label' => 'Meta Description [du]','rules' => 'max_length[1000]'),

            array('field' => 'address','label' => 'Address','rules' => 'required|max_length[200]'),
            array('field' => 'web','label' => 'Web','rules' => 'required|max_length[60]'),
            array('field' => 'email','label' => 'Email','rules' => 'required|valid_email'),
            array('field' => 'phone','label' => 'Phone','rules' => 'required|max_length[20]'),
            array('field' => 'mobile','label' => 'Mobile','rules' => 'required|max_length[20]'),
            array('field' => 'timing','label' => 'Timing','rules' => 'required|max_length[50]'),
            array('field' => 'openingDays','label' => 'Opening Days','rules' => 'required|max_length[100]'),
            array('field' => 'closingDays','label' => 'Closing Days','rules' => 'required|max_length[100]'),
            array('field' => 'reservation','label' => 'Reservation','rules' => 'required|numeric'),
            array('field' => 'pAdults','label' => 'Adults Price','rules' => 'required|numeric'),
            array('field' => 'pGuests','label' => 'Guests Price','rules' => 'required|numeric'),
            array('field' => 'pChilds','label' => 'Childs Price','rules' => 'required|numeric'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->Places_of_interest_model->id = 0;
            $this->Places_of_interest_model->enTitle = $this->input->post('enTitle')?$this->input->post('enTitle'):$this->input->post('itTitle');
            $this->Places_of_interest_model->duTitle = $this->input->post('duTitle')?$this->input->post('duTitle'):$this->input->post('itTitle');
            $this->Places_of_interest_model->itTitle = $this->input->post('itTitle');
            $this->Places_of_interest_model->enDescription = base64_encode($this->input->post('enDescription')?$this->input->post('itDescription'):$this->input->post('enDescription'));
            $this->Places_of_interest_model->duDescription = base64_encode($this->input->post('duDescription')?$this->input->post('duDescription'):$this->input->post('itDescription'));
            $this->Places_of_interest_model->itDescription = base64_encode($this->input->post('itDescription'));
            $this->Places_of_interest_model->meta_tag_en = $this->input->post('meta_en')?$this->input->post('meta_en'):$this->input->post('meta_it');
            $this->Places_of_interest_model->meta_tag_du = $this->input->post('meta_du')?$this->input->post('meta_du'):$this->input->post('meta_it');
            $this->Places_of_interest_model->meta_tag_it = $this->input->post('meta_it');
            $this->Places_of_interest_model->meta_desc_en = $this->input->post('meta_desc_en')?$this->input->post('meta_desc_en'):$this->input->post('meta_desc_it');
            $this->Places_of_interest_model->meta_desc_du = $this->input->post('meta_desc_du')?$this->input->post('meta_desc_du'):$this->input->post('meta_desc_it');
            $this->Places_of_interest_model->meta_desc_it = $this->input->post('meta_desc_it');
            $this->Places_of_interest_model->location_id = $this->input->post('location_id');
            $this->Places_of_interest_model->meta_keywords_en = $this->input->post('meta_keywords_en')?$this->input->post('meta_keywords_en'):$this->input->post('meta_keywords_it');
            $this->Places_of_interest_model->meta_keywords_du = $this->input->post('meta_keywords_du')?$this->input->post('meta_keywords_du'):$this->input->post('meta_keywords_it');
            $this->Places_of_interest_model->meta_keywords_it = $this->input->post('meta_keywords_it');
            $this->Places_of_interest_model->rewrite_url_en = $this->input->post('rewrite_url_en')?$this->input->post('rewrite_url_en'):$this->input->post('rewrite_url_it');
            $this->Places_of_interest_model->rewrite_url_du = $this->input->post('rewrite_url_du')?$this->input->post('rewrite_url_du'):$this->input->post('rewrite_url_it');
            $this->Places_of_interest_model->rewrite_url_it = $this->input->post('rewrite_url_it');
            $this->Places_of_interest_model->lng = $this->input->post('lng');
            $this->Places_of_interest_model->lat = $this->input->post('lat');
            $this->Places_of_interest_model->address = $this->input->post('address');
            $this->Places_of_interest_model->web = $this->input->post('web');
            $this->Places_of_interest_model->email = $this->input->post('email');
            $this->Places_of_interest_model->phone = $this->input->post('phone');
            $this->Places_of_interest_model->mobile = $this->input->post('mobile');
            $this->Places_of_interest_model->timing = $this->input->post('timing');
            $this->Places_of_interest_model->openingDays = $this->input->post('openingDays');
            $this->Places_of_interest_model->closingDays = $this->input->post('closingDays');
            $this->Places_of_interest_model->reservation = $this->input->post('reservation');
            $this->Places_of_interest_model->photo = ' ';
            $this->Places_of_interest_model->pAdults = $this->input->post('pAdults');
            $this->Places_of_interest_model->pGuests = $this->input->post('pGuests');
            $this->Places_of_interest_model->pChilds = $this->input->post('pChilds');
            $this->Places_of_interest_model->status = 'active';
            $message = $this->Places_of_interest_model->save();
            $this->session->set_flashdata(Array('msg' => $message,
                'type' => 'success'));
            redirect('admin/places_of_interest/edit/' . $this->Places_of_interest_model->id);
        } else {
            if (strlen(validation_errors()) > 0) {
                $data['msg'] = validation_errors();
                $data['type'] = 'danger';
            }
            $data['cpage'] = 'places';
            $data['sub_cpage'] = 'new-place';
            $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
                Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });")
            );
            $this->load->model('Categories');
            $data['all_categories'] = $this->Categories->getAllList();
            $this->load->view('admin/includes/header');
            $this->load->view('admin/includes/sidebar', $data);
            $this->load->view('admin/contents/places_of_interest/add', $data);
            $this->load->view('admin/includes/footer');
        }
    }

    public function edit($id) {
        $this->load->model('Places_of_interest_model');
        $this->Places_of_interest_model->load($id);
        $place = $this->Places_of_interest_model;
        if ($place->id) {
            $data['place'] = $place;
        } else {
            show_404();
            return;
        }

        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('admin/Location_model');
        $data['locations'] = $this->Location_model->getLocations();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $config = array(
            array('field' => 'lat','label' => 'Latitude','rules' => 'required|decimal'),
            array('field' => 'lng','label' => 'Longitude','rules' => 'required|decimal'),

            array('field' => 'enTitle','label' => 'Title [en]','rules' => 'required|min_length[2]|max_length[100]'),
            array('field' => 'duTitle','label' => 'Title [du]','rules' => 'min_length[2]|max_length[100]'),
            array('field' => 'itTitle','label' => 'Title [it]','rules' => 'min_length[2]|max_length[100]'),

            array('field' => 'location_id','label' => 'Location','rules' => 'required|numeric'),

            array('field' => 'enDescription','label' => 'Description [en]','rules' => 'max_length[20000]'),
            array('field' => 'duDescription','label' => 'Description [du]','rules' => 'max_length[20000]'),
            array('field' => 'itDescription','label' => 'Description [it]','rules' => 'required|max_length[20000]'),

            array('field' => 'meta_keywords_en','label' => 'Meta Keywords [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_keywords_du','label' => 'Meta Keywords [du]','rules' => 'max_length[250]'),
            array('field' => 'meta_keywords_it','label' => 'Meta Keywords [it]','rules' => 'required|max_length[250]'),

            array('field' => 'rewrite_url_en','label' => 'URL Rewrite [en]','rules' => 'max_length[250]'),
            array('field' => 'rewrite_url_du','label' => 'URL Rewrite [du]','rules' => 'max_length[250]'),
            array('field' => 'rewrite_url_it','label' => 'URL Rewrite [it]','rules' => 'required|max_length[250]'),

            array('field' => 'meta_tag_en','label' => 'Meta [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_tag_du','label' => 'Meta [du]','rules' => 'max_length[250]'),
            array('field' => 'meta_tag_it','label' => 'Meta [it]','rules' => 'required|max_length[250]'),

            array('field' => 'meta_desc_it','label' => 'Meta Description [it]','rules' => 'required|max_length[1000]'),
            array('field' => 'meta_desc_en','label' => 'Meta Description [en]','rules' => 'max_length[1000]'),
            array('field' => 'meta_desc_du','label' => 'Meta Description [du]','rules' => 'max_length[1000]'),

            array('field' => 'address','label' => 'Address','rules' => 'required|max_length[200]'),
            array('field' => 'web','label' => 'Web','rules' => 'required|max_length[60]'),
            array('field' => 'email','label' => 'Email','rules' => 'required|valid_email'),
            array('field' => 'phone','label' => 'Phone','rules' => 'required|max_length[20]'),
            array('field' => 'mobile','label' => 'Mobile','rules' => 'required|max_length[20]'),
            array('field' => 'timing','label' => 'Timing','rules' => 'required|max_length[50]'),
            array('field' => 'openingDays','label' => 'Opening Days','rules' => 'required|max_length[100]'),
            array('field' => 'closingDays','label' => 'Closing Days','rules' => 'required|max_length[100]'),
            array('field' => 'reservation','label' => 'Reservation','rules' => 'required|numeric'),
            array('field' => 'pAdults','label' => 'Adults Price','rules' => 'required|numeric'),
            array('field' => 'pGuests','label' => 'Guests Price','rules' => 'required|numeric'),
            array('field' => 'pChilds','label' => 'Childs Price','rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->Places_of_interest_model->enTitle = trim($this->input->post('enTitle')?$this->input->post('enTitle'):$this->input->post('itTitle'));
            $this->Places_of_interest_model->duTitle = trim($this->input->post('duTitle')?$this->input->post('duTitle'):$this->input->post('itTitle'));
            $this->Places_of_interest_model->itTitle = trim($this->input->post('itTitle'));
            $this->Places_of_interest_model->enDescription = base64_encode($this->input->post('enDescription')?$this->input->post('enDescription'):$this->input->post('itDescription'));
            $this->Places_of_interest_model->duDescription = base64_encode($this->input->post('duDescription')?$this->input->post('duDescription'):$this->input->post('itDescription'));
            $this->Places_of_interest_model->itDescription = base64_encode($this->input->post('itDescription'));
            $this->Places_of_interest_model->meta_tag_en = $this->input->post('meta_tag_en')?$this->input->post('meta_tag_en'):$this->input->post('meta_tag_it');
            $this->Places_of_interest_model->meta_tag_du = $this->input->post('meta_tag_du')?$this->input->post('meta_tag_du'):$this->input->post('meta_tag_it');
            $this->Places_of_interest_model->meta_tag_it = $this->input->post('meta_tag_it');
            $this->Places_of_interest_model->meta_desc_en = $this->input->post('meta_desc_en')?$this->input->post('meta_desc_en'):$this->input->post('meta_desc_it');
            $this->Places_of_interest_model->meta_desc_du = $this->input->post('meta_desc_du')?$this->input->post('meta_desc_du'):$this->input->post('meta_desc_it');
            $this->Places_of_interest_model->meta_desc_it = $this->input->post('meta_desc_it');
            $this->Places_of_interest_model->meta_keywords_en = $this->input->post('meta_keywords_en')?$this->input->post('meta_keywords_en'):$this->input->post('meta_keywords_it');
            $this->Places_of_interest_model->meta_keywords_du = $this->input->post('meta_keywords_du')?$this->input->post('meta_keywords_du'):$this->input->post('meta_keywords_it');
            $this->Places_of_interest_model->meta_keywords_it = $this->input->post('meta_keywords_it');
            $this->Places_of_interest_model->rewrite_url_en = $this->input->post('rewrite_url_en')?$this->input->post('rewrite_url_en'):$this->input->post('rewrite_url_it');
            $this->Places_of_interest_model->rewrite_url_du = $this->input->post('rewrite_url_du')?$this->input->post('rewrite_url_du'):$this->input->post('rewrite_url_it');
            $this->Places_of_interest_model->rewrite_url_it = $this->input->post('rewrite_url_it');
            $this->Places_of_interest_model->lng = $this->input->post('lng');
            $this->Places_of_interest_model->lat = $this->input->post('lat');
            $this->Places_of_interest_model->location_id = $this->input->post('location_id');
            $this->Places_of_interest_model->address = trim($this->input->post('address'));
            $this->Places_of_interest_model->web = trim($this->input->post('web'));
            $this->Places_of_interest_model->email = trim($this->input->post('email'));
            $this->Places_of_interest_model->phone = trim($this->input->post('phone'));
            $this->Places_of_interest_model->mobile = trim($this->input->post('mobile'));
            $this->Places_of_interest_model->timing = trim($this->input->post('timing'));
            $this->Places_of_interest_model->openingDays = trim($this->input->post('openingDays'));
            $this->Places_of_interest_model->closingDays = trim($this->input->post('closingDays'));
            $this->Places_of_interest_model->reservation = $this->input->post('reservation');
            $this->Places_of_interest_model->photo = $this->Places_of_interest_model->photo;
            $this->Places_of_interest_model->pAdults = $this->input->post('pAdults');
            $this->Places_of_interest_model->pGuests = $this->input->post('pGuests');
            $this->Places_of_interest_model->pChilds = $this->input->post('pChilds');
            $this->Places_of_interest_model->status = 'active';
            $this->Places_of_interest_model->save();
            $this->session->set_flashdata(Array('msg' => 'Hai modificato con succcesso il Luogo di interesse!',
                'type' => 'success', 'model' => 'login'));
            redirect('admin/places_of_interest/edit/' . $id);
        } else {
            if (strlen(validation_errors()) > 0) {
                $data['msg'] = validation_errors();
                $data['type'] = 'danger';
            }
            $data['cpage'] = 'places';

            $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
                Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });")
            );
            $this->load->model('Categories');
            $data['all_categories'] = $this->Categories->getAllList();
            $this->load->view('admin/includes/header', $data);
            $this->load->view('admin/includes/sidebar', $data);
            $this->load->view('admin/contents/places_of_interest/edit', $data);
            $this->load->view('admin/includes/footer', $data);
        }
    }

    public function updatethumbnail() {
        // var_dump($_POST);exit();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Places_of_interest_model');
        $config = array(
            array('field' => 'id', 'label' => 'Place ID', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $id = $this->input->post('id');
            if ($_FILES && $_FILES['p_thumbnail']['name']) {
                $config['upload_path'] = './uploads/places_of_interest/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '5000';
                $new_name = time() . $_FILES["p_thumbnail"]['name'];
                $config['file_name'] = $new_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('p_thumbnail')) {
                    $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                        'type' => 'danger'));
                    redirect('admin/places_of_interest/edit/' . $id);
                } else {
                    $this->Places_of_interest_model->load($id);
                    $this->Places_of_interest_model->photo = $new_name;
                    $this->Places_of_interest_model->save();
                    $this->session->set_flashdata(Array('msg' => 'Hai aggiornato con successo l\'immagine primaria',
                        'type' => 'success'));
                    redirect('admin/places_of_interest/edit/' . $id);
                }
            } else {
                $this->session->set_flashdata(Array('msg' => 'Per favore seleziona l\'immagine da caricare',
                    'type' => 'danger'));
                redirect('admin/places_of_interest/edit/' . $id);
            }
        } else {
            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
            redirect('admin/places_of_interest/edit/' . $id);
        }
    }

    public function delete() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'Place ID',
                'rules' => 'required|numeric'
        ));
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('Places_of_interest_model');
            $this->Places_of_interest_model->load($this->input->post('id'));
            $this->Places_of_interest_model->delete();
            $this->session->set_flashdata(Array('msg' => 'Hai eliminato con successo il Luogo di interesse',
                'type' => 'success'));
        }
        redirect('admin/places_of_interest/');
    }

}
