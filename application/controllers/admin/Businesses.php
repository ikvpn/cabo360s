<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Businesses extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')  && $this->session->userdata('usertype') == 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('full_name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
        $this->load->helper('form');
    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['title'] = 'Attività Registrate';
        $this->load->model('Orders_model');
        $this->load->model('HotelBB_model');
        $this->load->model('admin/Metadata_business_model');
        $data['hotel_bb'] = $this->HotelBB_model->listAll();
        $pages = $this->Orders_model->getList();
        foreach ($pages as $key => $page) {
            $metadata = $this->Metadata_business_model->getByOrderID($page->id);
            $page->metadata = $metadata;
        }
        $data['pages'] = $pages;
        $data['cpage'] = 'businesses';
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#expired_pages').dataTable();
                                                                     $('#active_pages').dataTable();
                                                                     $('#new_pages').dataTable();
                                                        });")
        );
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/businesses/index', $data);
        $this->load->view('admin/includes/footer', $data);
    }
    public function activity($tableName){
      //echo "<pre>"; print_r($tableName); die;
      $data['userinfo'] = $this->userinfo;
      $data['msg'] = $this->session->flashdata('msg');
      $data['type'] = $this->session->flashdata('type');
      $data['title'] = 'Attività Registrate';
      $this->load->model('Orders_model');
      $this->load->model('HotelBB_model');
      $data['hotel_bb'] = $this->HotelBB_model->listAll();

      $this->load->model('admin/Metadata_business_model');
      $pages = $this->Orders_model->getActivityByType($tableName);

      foreach ($pages as $key => $page) {
          $metadata = $this->Metadata_business_model->getByOrderID($page->id);
          $page->metadata = $metadata;
      }

      $data['pages'] = $pages;

      $data['cpage'] = 'businesses';
      $data['sub_cpage'] = $tableName;
      $data['cssfiles'] = Array(
          Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
      );

      $data['jsfiles'] = Array(
          Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
          Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
          Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                   $('#expired_pages').dataTable();
                                                                   $('#active_pages').dataTable();
                                                                   $('#new_pages').dataTable();
                                                      });")
      );
      $this->load->model('Categories');
      $data['all_categories'] = $this->Categories->getAllList();
      $this->load->view('admin/includes/header', $data);
      $this->load->view('admin/includes/sidebar', $data);
      $this->load->view('admin/contents/businesses/index', $data);
      $this->load->view('admin/includes/footer', $data);
    }

    /*
    public function new_pages() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('Orders_model');
        $where = array('status' => 'unpaid');
        $data['pages'] = $this->Orders_model->getList($where);
        $data['cpage'] = 'businesses';
        $data['sub_cpage'] = 'new_pages';
        $data['title'] = 'New Pages';
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#expired_pages').dataTable();
                                                                     $('#active_pages').dataTable();
                                                                     $('#new_pages').dataTable();
                                                        });")
        );
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/businesses/index', $data);
        $this->load->view('admin/includes/footer', $data);
    }

    public function active_pages() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['title'] = 'Active Pages';
        $this->load->model('Orders_model');
        $where = array('status' => 'paid');
        $data['pages'] = $this->Orders_model->getList($where);
        $data['cpage'] = 'businesses';
        $data['sub_cpage'] = 'active_pages';
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#expired_pages').dataTable();
                                                                     $('#active_pages').dataTable();
                                                                     $('#new_pages').dataTable();
                                                        });")
        );
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/businesses/index', $data);
        $this->load->view('admin/includes/footer', $data);
    }

    public function expired_pages() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['title'] = 'Expired Pages';
        $this->load->model('Orders_model');
        $where = array('status' => 'expire');
        $data['pages'] = $this->Orders_model->getList($where);
        $data['cpage'] = 'businesses';
        $data['sub_cpage'] = 'expired_pages';
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#expired_pages').dataTable();
                                                                     $('#active_pages').dataTable();
                                                                     $('#new_pages').dataTable();
                                                        });")
        );
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/businesses/index', $data);
        $this->load->view('admin/includes/footer', $data);
    }
*/
    public function updatestatus() {
        $this->load->library('form_validation');
        $config = array(
            array('field' => 'id', 'label' => 'Page ID', 'rules' => 'required|numeric'),
            array('field' => 'status', 'label' => 'Page Status', 'rules' => 'required')
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $this->load->model('Orders_model');
            $this->Orders_model->load($this->input->post('id'));
            if ($this->Orders_model->id) {
                // valid page
                $this->Orders_model->update($this->input->post('id'), array('status' => $this->input->post('status')));
                $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo lo stato dell\'attività',
                    'type' => 'success', 'model' => 'login'));
            } else {
                $this->session->set_flashdata(Array('msg' => 'Non è stata trovata l\'attività',
                    'type' => 'danger'));
            }
        } else {
            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
        }
        redirect('admin/businesses/');
    }

    public function editmetadata() {
      $this->load->library('form_validation');
      $config = array(
          array('field' => 'meta_desc_it', 'label' => 'Meta description IT', 'rules' => 'required'),
          array('field' => 'meta_keywords_it', 'label' => 'Meta keywords IT', 'rules' => 'required'),
          array('field' => 'meta_title_it', 'label' => 'Meta title IT', 'rules' => 'required')
      );

      $this->form_validation->set_rules($config);

      if ($this->form_validation->run() !== FALSE) {
          $this->load->model('admin/Metadata_business_model');

          $metadata = $this->Metadata_business_model->getByOrderID($this->input->post('id'));

          $newMetadata = array(
                          'meta_desc_it' => $this->input->post('meta_desc_it'),
                          'meta_desc_en' => $this->input->post('meta_desc_en') != '' ? $this->input->post('meta_desc_en') : $this->input->post('meta_desc_it'),
                          'meta_desc_du' => $this->input->post('meta_desc_du') != '' ? $this->input->post('meta_desc_du') : $this->input->post('meta_desc_it'),
                          'meta_keywords_it' => $this->input->post('meta_keywords_it'),
                          'meta_keywords_en' => $this->input->post('meta_keywords_en') != '' ? $this->input->post('meta_keywords_en') : $this->input->post('meta_keywords_it'),
                          'meta_keywords_du' => $this->input->post('meta_keywords_du') != '' ? $this->input->post('meta_keywords_du') : $this->input->post('meta_keywords_it'),
                          'meta_title_it' => $this->input->post('meta_title_it'),
                          'meta_title_en' => $this->input->post('meta_title_en') != '' ? $this->input->post('meta_title_en') : $this->input->post('meta_title_it'),
                          'meta_title_du' => $this->input->post('meta_title_du') != '' ? $this->input->post('meta_title_du') : $this->input->post('meta_title_it'),
                        );

          if(empty($metadata)) {
            //insert
              $newMetadata['order_id'] = $this->input->post('id');


              $this->Metadata_business_model->add($newMetadata);

              $this->session->set_flashdata(Array('msg' => 'Hai aggiunto con successo i metadata',
                  'type' => 'success', 'model' => 'login'));
          }
          else {
            //update
              $this->Metadata_business_model->updateByOrderId($this->input->post('id'), $newMetadata);

              $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo i metadata',
                  'type' => 'success', 'model' => 'login'));
          }
      } else {
          $this->session->set_flashdata(Array('msg' => validation_errors(),
              'type' => 'danger'));
      }
      redirect('admin/businesses/');

    }

    public function delete() {
        $this->load->library('form_validation');
        $config = array(
            array('field' => 'id', 'label' => 'Page ID', 'rules' => 'required|numeric')
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $this->load->model('Orders_model');
            $this->Orders_model->load($this->input->post('id'));
            if ($this->Orders_model->id) {
                // valid page
                $this->Orders_model->delete($this->input->post('id'));
                $this->session->set_flashdata(Array('msg' => 'Hai eliminato con successo l\'attività',
                    'type' => 'success', 'model' => 'login'));
            } else {
                $this->session->set_flashdata(Array('msg' => 'Server returns 404.',
                    'type' => 'danger'));
            }
        } else {
            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
        }
        redirect('admin/businesses/');
    }

    public function details($id ) {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['title'] = 'Registered Pages';
        $this->load->model('Orders_model');
        $this->Orders_model->load($id);
        $page = '404';
        if($this->Orders_model->id){
            $this->load->model('customer/Customer_model');
            $this->Customer_model->load($this->Orders_model->customer_id);
            $page='admin/contents/businesses/details';
        }
        $data['cpage'] = 'businesses';
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#expired_pages').dataTable();
                                                                     $('#active_pages').dataTable();
                                                                     $('#new_pages').dataTable();
                                                        });")
        );
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view($page, $data);
        $this->load->view('admin/includes/footer', $data);
    }

    public function associateHotel() {
      $this->load->model('HotelBB_model');
      $this->load->library('form_validation');

      $hotel_bb_id = $this->input->post('hotelBB_id');
      $hotel_procida_id = $this->input->post('associateHotel_id');

      if (!empty($hotel_bb_id) && !empty($hotel_procida_id)) {
          $data = array('hotel_bb_id' => $hotel_bb_id, 'id' => $hotel_procida_id );
          $association = $this->HotelBB_model->associate($data);
          $this->session->set_flashdata( Array( 'msg' => 'Hai associato con successo un HotelBB',
              'type' => 'success', 'model' => 'login'));
      } else {
          $this->session->set_flashdata(Array('msg' => 'Server error', 'type' => 'danger'));
      }

      redirect('admin/businesses/index');
    }

}
