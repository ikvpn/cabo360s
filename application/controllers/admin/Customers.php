<?php

if(!defined('BASEPATH')){ exit('No direct script access is allowed'); }

class Customers extends CI_Controller{

    private $userinfo;

    public function __construct() {
        parent::__construct();
         if ($this->session->userdata('id')  && $this->session->userdata('usertype') == 'admin' ) {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('full_name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
        $this->load->helper('form');
    }

    public function index(){
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['title'] = 'Registered Pages';
        $this->load->model('customer/Customer_model');
        $data['customers'] = $this->Customer_model->getList();
        $data['cpage'] = 'customers';
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#customers').dataTable();
                                                        });")
        );
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/customers/index', $data);
        $this->load->view('admin/includes/footer', $data);
    }

    public function delete(){
          $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'Location Id',
                'rules' => 'required|numeric'
        ));
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('customer/Customer_model');
            $this->Customer_model->delete($this->input->post('id'));
            $this->session->set_flashdata(Array('msg' => 'Hai eliminato con successo un cliente',
                'type' => 'success'));
        }
        redirect('admin/customers');
    }
}
