<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Block extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')  && $this->session->userdata('usertype') == 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('full_name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('admin/Block_model');
        $this->load->helper('form');
        $data['blocks'] = $this->Block_model->getblocks(false);
       	$data['cpage'] = 'block';
        $data['sub_cpage'] = 'all-blocks';
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#blocks-dtable').dataTable();
                                                        });")
        );
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/block/block', $data);
        $this->load->view('admin/includes/footer', $data);
    }

    public function addblock() {
		$data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('admin/Block_model');
        $this->load->helper('Form');
        $this->load->library('Form_validation');

        $config = array(
            array('field' => 'title_en','label' => 'Title [en]','rules' => 'min_length[2]|max_length[50]'),
            array('field' => 'title_du','label' => 'Title [du]','rules' => 'min_length[2]|max_length[50]'),
            array('field' => 'title_it','label' => 'Title [it]','rules' => 'required|min_length[2]|max_length[50]'),

            array('field' => 'description_en','label' => 'Description [en]','rules' => 'max_length[20000]'),
            array('field' => 'description_du','label' => 'Description [du]','rules' => 'max_length[20000]'),
            array('field' => 'description_it','label' => 'Description [it]','rules' => 'required|max_length[20000]'),
           /* array(
                'field' => 'meta_keywords_en',
                'label' => 'Meta Keywords [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'meta_keywords_du',
                'label' => 'Meta Keywords [de]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_keywords_it',
                'label' => 'Meta Keywords [it]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'rewrite_url_en',
                'label' => 'URL Rewrite [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'rewrite_url_du',
                'label' => 'URL Rewrite [de]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'rewrite_url_it',
                'label' => 'URL Rewrite [it]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_en',
                'label' => 'Meta [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'meta_du',
                'label' => 'Meta [de]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_it',
                'label' => 'Meta [it]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_desc_it',
                'label' => 'Meta Description [it]',
                'rules' => 'max_length[1000]'
            ),
            array(
                'field' => 'meta_desc_en',
                'label' => 'Meta Description [en]',
                'rules' => 'required|max_length[1000]'
            ),
            array(
                'field' => 'meta_desc_du',
                'label' => 'Meta Description [de]',
                'rules' => 'max_length[1000]')*/
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            if ($_FILES && $_FILES['p_image']['name']) {
                $config['upload_path'] = './uploads/block/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '5000';
                $new_name = time() . $_FILES["p_image"]['name'];
                $config['file_name'] = $new_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('p_image')) {
                    $error = array('error' => $this->upload->display_errors());
                    $data['msg'] = $error;
                    $data['type'] = "danger";
                } else {
                    $id = $this->Block_model->insert($new_name);
                    $this->session->set_flashdata(Array('msg' => 'Hai aggiunto con successo un nuovo blocco',
                        'type' => 'success', 'model' => 'login'));
                    redirect('admin/block');
                }
            } else {
                //insert data without image
                $id = $this->Block_model->insert(false);
                $this->session->set_flashdata(Array('msg' => 'Hai aggiunto con successo un nuovo blocco',
                    'type' => 'success', 'model' => 'login'));
                redirect('admin/block');
            }
        } else {

            if (strlen(validation_errors()) > 0) {
                $data['msg'] = validation_errors();
                $data['type'] = 'danger';
            }
        }
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $data['cpage'] = 'block';
        $data['sub_cpage'] = 'new-block';
        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
            Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });")
        );
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/block/addblock', $data);
        $this->load->view('admin/includes/footer', $data);
    }

    public function editblock($id) {
        $this->load->model('admin/Block_model');
        $block = $this->Block_model->getBlock($id, false);
        if ($block) {
            $data['block'] = $block;
        } else {
            redirect('admin/block');
        }

        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->helper('Form');
        $this->load->library('Form_validation');
        $data['blocks'] = $this->Block_model->getblocks(false);
        $data['cpage'] = 'block';

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
            Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });")
        );
        $this->load->library('form_validation');

        $config = array(
            array('field' => 'id','label' => 'ID','rules' => 'required|numeric'),

            array('field' => 'title_en','label' => 'Title [en]','rules' => 'min_length[2]|max_length[50]'),
            array('field' => 'title_du','label' => 'Title [du]','rules' => 'min_length[2]|max_length[50]'),
            array('field' => 'title_it','label' => 'Title [it]','rules' => 'required|min_length[2]|max_length[50]'),

            array('field' => 'description_en','label' => 'Description [en]','rules' => 'max_length[20000]'),
            array('field' => 'description_du','label' => 'Description [du]','rules' => 'max_length[20000]'),
            array('field' => 'description_it','label' => 'Description [it]','rules' => 'required|max_length[20000]'),
          /*  array(
                'field' => 'meta_keywords_en',
                'label' => 'Meta Keywords [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'meta_keywords_du',
                'label' => 'Meta Keywords [de]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_keywords_it',
                'label' => 'Meta Keywords [it]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'rewrite_url_en',
                'label' => 'URL Rewrite [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'rewrite_url_du',
                'label' => 'URL Rewrite [de]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'rewrite_url_it',
                'label' => 'URL Rewrite [it]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_en',
                'label' => 'Meta [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'meta_du',
                'label' => 'Meta [de]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_it',
                'label' => 'Meta [it]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_desc_it',
                'label' => 'Meta Description [it]',
                'rules' => 'max_length[1000]'
            ),
            array(
                'field' => 'meta_desc_en',
                'label' => 'Meta Description [en]',
                'rules' => 'required|max_length[1000]'
            ),
            array(
                'field' => 'meta_desc_du',
                'label' => 'Meta Description [de]',
                'rules' => 'max_length[1000]'),
            array(
                'field' => 'meta_keywords_en',
                'label' => 'Meta Keywords [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'meta_keywords_du',
                'label' => 'Meta Keywords [de]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_keywords_it',
                'label' => 'Meta Keywords [it]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'rewrite_url_en',
                'label' => 'URL Rewrite [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'rewrite_url_du',
                'label' => 'URL Rewrite [de]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'rewrite_url_it',
                'label' => 'URL Rewrite [it]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_en',
                'label' => 'Meta [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'meta_du',
                'label' => 'Meta [de]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_it',
                'label' => 'Meta [it]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_desc_it',
                'label' => 'Meta Description [it]',
                'rules' => 'max_length[1000]'
            ),
            array(
                'field' => 'meta_desc_en',
                'label' => 'Meta Description [en]',
                'rules' => 'required|max_length[1000]'
            ),
            array(
                'field' => 'meta_desc_du',
                'label' => 'Meta Description [de]',
                'rules' => 'max_length[1000]'
			),*/
            array(
                'field' => 'link_en',
                'label' => 'Link [en]',
                'rules' => 'required'
            ),

        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            if ($_FILES && $_FILES['p_image']['name']) {
                $config['upload_path'] = './uploads/block/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '5000';
                $new_name = time() . $_FILES["p_image"]['name'];
                $config['file_name'] = $new_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('p_image')) {
                    $error = array('error' => $this->upload->display_errors());
                    $data['msg'] = $error;
                    $data['type'] = "danger";
                    var_dump($error);
                    exit();
                } else {
                    $id = $this->Block_model->update($new_name);
                    $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo il blocco',
                        'type' => 'success', 'model' => 'login'));
                    redirect('admin/block/editblock/' . $this->input->post('id'));
                }
            } else {
                //insert data without image
                $id = $this->Block_model->update(false);
                $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo il blocco',
                    'type' => 'success', 'model' => 'login'));
                redirect('admin/block/editblock/' . $this->input->post('id'));
            }
        } else {

            if (strlen(validation_errors()) > 0) {
                $data['msg'] = validation_errors();
                $data['type'] = 'danger';
            }
        }
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header');
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/block/editblock', $data);
        $this->load->view('admin/includes/footer');
    }

    public function deleteblock() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'Block Id',
                'rules' => 'required|numeric'
        ));
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('admin/Block_model');
            $this->Block_model->delete($this->input->post('id'));
            $this->session->set_flashdata(Array('msg' => 'Hai eliminato con successo il blocco',
                'type' => 'success'));
        }
        redirect('admin/block');
    }

}
