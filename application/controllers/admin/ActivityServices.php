<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ActivityServices extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')  && $this->session->userdata('usertype') == 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('full_name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('admin/Activity_service_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $config = array(
            array('field' => 'id',
                'label' => 'Service ID',
                'rules' => 'required'
            ),
            array('field' => 'service_name_it',
                'label' => 'Service name [It]',
                'rules' => 'required'
            ),
            array('field' => 'service_name_en',
                'label' => 'Service name [En]',
                'rules' => 'required'
            )

        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
          if($this->input->post('id') > 0){
            $nameIT = $this->input->post('service_name_it');
            $nameEN = $this->input->post('service_name_en');
            $tag = array('nameIT' => $nameIT,'nameEN' => $nameEN);
            $this->Activity_service_model->update($this->input->post('id'), $tag);
            //success and redirect on same page
            $this->session->set_flashdata(Array('msg' => "Hai aggiornato con successo il servizio",
                'type' => 'success'));
          }
          else {
            $nameIT = $this->input->post('service_name_it');
            $nameEN = $this->input->post('service_name_en');
            $tag = array('nameIT' => $nameIT,'nameEN' => $nameEN);
            $this->Activity_service_model->add($tag);
            //success and redirect on same page
            $this->session->set_flashdata(Array('msg' => "Hai aggiunto con successo il servizio",
                'type' => 'success'));
          }

            redirect('admin/ActivityServices');
        } else {
            if (strlen(validation_errors()) > 0) {
                $data['msg'] = validation_errors();
                $data['type'] = 'danger';
            }
            $data['tags'] = $this->Activity_service_model->getAll();
            $data['cpage'] = 'activityservices';
            $data['cssfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
            );

            $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
                Array('type' => 'inline', 'src' => " $(document).ready(function() { $('#ads-dtable').dataTable(); });")
            );
            $this->load->model('Categories');
            $data['all_categories'] = $this->Categories->getAllList();
            $this->load->view('admin/includes/header',$data);
            $this->load->view('admin/includes/sidebar', $data);
            $this->load->view('admin/contents/activity_service/index', $data);
            $this->load->view('admin/includes/footer',$data);
        }
    }
    public function delete() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'Service ID',
                'rules' => 'required|numeric'
        ));
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('admin/Activity_service_model');
            $this->Activity_service_model->delete($this->input->post('id'));
            $this->session->set_flashdata(Array('msg' => 'Hai eliminato con successo il servizio!',
                'type' => 'success'));
        }
        redirect('admin/ActivityServices');
    }


}
