<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Login extends CI_Controller {

  public function __construct() {
    parent::__construct();
    if ($this->session->userdata('id')){
      redirect('admin/dashboard');
    }
  }

  public function index(){
    $this->load->helper('form');
    $this->load->library('form_validation');
    $config = array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|email'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->input->post('password') != '' && $this->input->post('email') != ''){
//        if ($this->form_validation->run() != FALSE) {
        $this->load->model('admin/Users');
        
        $res = $this->Users->login($this->input->post('email'),$this->input->post('password'));
        if($res == '1'){  
            $this->load->helper('cookie');
            $name   = 'user_cookie';
            $value  = serialize($this->session->userdata());
            $expire = time()+3600;
            $path  = '/';
            $secure = TRUE;
            setcookie($name,'',-1,$path);
            setcookie($name,$value,$expire,$path);                 
            redirect('admin/Dashboard');           
        }else{
           $this->load->view('admin/login',array('msg'=>'Email o password non valide','type'=>'danger'));
        }
    } else {
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->view('admin/login',$data);
    }
  }

}
