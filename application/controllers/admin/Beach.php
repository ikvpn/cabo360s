<?php

if (!defined("BASEPATH")) {
    exit('No direct script access is allowed');
}

class Beach extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')  && $this->session->userdata('usertype') == 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('full_name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('admin/Beach_model');
        $this->load->helper('form');
        $data['beaches'] = $this->Beach_model->getBeaches(false);
        $data['cpage'] = 'beaches';
        $data['sub_cpage'] = 'all-beaches';
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#beaches-dtable').dataTable();
                                                        });")
        );
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/beach/beaches', $data);
        $this->load->view('admin/includes/footer');
    }

    public function add() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('admin/Beach_model');
        $this->load->model('admin/Location_model');
        $data['locations'] = $this->Location_model->getLocations();
        $this->load->helper('form');
        $this->load->library('form_validation');

        $config = array(
            array('field' => 'lat','label' => 'Latitude','rules' => 'required|decimal'),
            array('field' => 'lng','label' => 'Longitude','rules' => 'required|decimal'),

            array('field' => 'title_it','label' => 'Title [it]','rules' => 'required|min_length[2]|max_length[100]'),
            array('field' => 'title_en','label' => 'Title [en]','rules' => 'min_length[2]|max_length[100'),
            array('field' => 'title_du','label' => 'Title [de]','rules' => 'min_length[2]|max_length[100]'),

            array('field' => 'location_id','label' => 'Location','rules' => 'required|numeric'),

            array('field' => 'description_it','label' => 'Description [it]','rules' => 'required|max_length[20000]'),
            array('field' => 'description_en','label' => 'Description [en]','rules' => 'max_length[20000]'),
            array('field' => 'description_du','label' => 'Description [de]','rules' => 'max_length[20000]'),

            array('field' => 'meta_keywords_it','label' => 'Meta Keywords [it]','rules' => 'required|max_length[250]'),
            array('field' => 'meta_keywords_en','label' => 'Meta Keywords [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_keywords_du','label' => 'Meta Keywords [de]','rules' => 'max_length[250]'),

            array('field' => 'rewrite_url_it','label' => 'URL Rewrite [it]','rules' => 'required|max_length[250]'),
            array('field' => 'rewrite_url_en','label' => 'URL Rewrite [en]','rules' => 'max_length[250]'),
            array('field' => 'rewrite_url_du','label' => 'URL Rewrite [de]','rules' => 'max_length[250]'),

            array('field' => 'meta_it','label' => 'Meta [it]','rules' => 'required|max_length[250]'),
            array('field' => 'meta_en','label' => 'Meta [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_du','label' => 'Meta [de]','rules' => 'max_length[250]'),

            array('field' => 'meta_desc_it','label' => 'Meta Description [it]','rules' => 'required|max_length[1000]'),
            array('field' => 'meta_desc_en','label' => 'Meta Description [en]','rules' => 'max_length[1000]'),
            array('field' => 'meta_desc_du','label' => 'Meta Description [de]','rules' => 'max_length[1000]')
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $id = $this->Beach_model->insert();
            $this->session->set_flashdata(Array('msg' => 'Hai aggiunto con successo una nuova Spiaggia',
                'type' => 'success', 'model' => 'login'));
            redirect('admin/beach/edit/' . $id);
        } else {
            if (strlen(validation_errors()) > 0) {
                $data['msg'] = validation_errors();
                $data['type'] = 'danger';
            }
        }
        $data['cpage'] = 'beaches';
        $data['sub_cpage'] = 'new-beach';
        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
            Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });")
        );
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header');
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/beach/add', $data);
        $this->load->view('admin/includes/footer');
    }

    public function edit($id) {
        $this->load->model('admin/Beach_model');
        $beach = $this->Beach_model->getBeach($id, false);
        if ($beach) {
            $data['beach'] = $beach;
        } else {
            show_404();
            return;
        }

        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('admin/Location_model');
        $data['locations'] = $this->Location_model->getLocations();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $config = array(
            array('field' => 'lat','label' => 'Latitude','rules' => 'required|decimal'),
            array('field' => 'lng','label' => 'Longitude','rules' => 'required|decimal'),

            array('field' => 'title_it','label' => 'Title [it]','rules' => 'required|min_length[2]|max_length[100]'),
            array('field' => 'title_en','label' => 'Title [en]','rules' => 'min_length[2]|max_length[100]'),
            array('field' => 'title_du','label' => 'Title [du]','rules' => 'min_length[2]|max_length[100]'),

            array('field' => 'location_id','label' => 'Location','rules' => 'required|numeric'),

            array('field' => 'description_it','label' => 'Description [it]','rules' => 'required|max_length[20000]'),
            array('field' => 'description_en','label' => 'Description [en]','rules' => 'max_length[20000]'),
            array('field' => 'description_du','label' => 'Description [du]','rules' => 'max_length[20000]'),

            array('field' => 'meta_keywords_it','label' => 'Meta Keywords [it]','rules' => 'required|max_length[250]'),
            array('field' => 'meta_keywords_en','label' => 'Meta Keywords [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_keywords_du','label' => 'Meta Keywords [du]','rules' => 'max_length[250]'),

            array('field' => 'rewrite_url_it','label' => 'URL Rewrite [it]','rules' => 'required|max_length[250]'),
            array('field' => 'rewrite_url_en','label' => 'URL Rewrite [en]','rules' => 'max_length[250]'),
            array('field' => 'rewrite_url_du','label' => 'URL Rewrite [du]','rules' => 'max_length[250]'),

            array('field' => 'meta_it','label' => 'Meta [it]','rules' => 'required|max_length[250]'),
            array('field' => 'meta_en','label' => 'Meta [en]','rules' => 'max_length[250]'),
            array('field' => 'meta_du','label' => 'Meta [du]','rules' => 'max_length[250]'),

            array('field' => 'meta_desc_it','label' => 'Meta Description [it]','rules' => 'required|max_length[1000]'),
            array('field' => 'meta_desc_en','label' => 'Meta Description [en]','rules' => 'max_length[1000]'),
            array('field' => 'meta_desc_du','label' => 'Meta Description [du]','rules' => 'max_length[1000]'));

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->Beach_model->update($id);
            $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo la nuova spiaggia',
                'type' => 'success', 'model' => 'login'));
            redirect('admin/beach/edit/' . $id);
        } else {
            if (strlen(validation_errors()) > 0) {
                $data['msg'] = validation_errors();
                $data['type'] = 'danger';
            }
        }

        $data['cpage'] = 'beaches';

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
            Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });")
        );
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/beach/edit', $data);
        $this->load->view('admin/includes/footer', $data);
    }

    public function updatethumbnail() {
        // var_dump($_POST);exit();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('admin/Beach_model');
        $config = array(
            array('field' => 'id', 'label' => 'Beach ID', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $id = $this->input->post('id');
            if ($_FILES && $_FILES['p_thumbnail']['name']) {
                $config['upload_path'] = './uploads/beaches/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '5000';
                $new_name = time() . $_FILES["p_thumbnail"]['name'];
                $config['file_name'] = $new_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('p_thumbnail')) {
                    $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                        'type' => 'danger'));
                    redirect('admin/beach/edit/' . $id);
                } else {
                    $this->Beach_model->update_thumbnail($id, $new_name);
                    $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo l\'immagine della Thumbnail',
                        'type' => 'success'));
                    redirect('admin/beach/edit/' . $id);
                }
            } else {
                $this->session->set_flashdata(Array('msg' => 'Per favore seleziona l\'immagine da caricare',
                    'type' => 'danger'));
                redirect('admin/beach/edit/' . $id);
            }
        } else {

            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
            redirect('admin/places_of_interest/edit/' . $id);
        }
    }

    public function updatecover() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('admin/Beach_model');
        $config = array(
            array('field' => 'id', 'label' => 'Beach ID', 'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $id = $this->input->post('id');
            if ($_FILES && $_FILES['p_cover']['name']) {
                $config['upload_path'] = './uploads/beaches/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '5000';
                $new_name = time() . $_FILES["p_cover"]['name'];
                $config['file_name'] = $new_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('p_cover')) {
                    $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                        'type' => 'danger'));
                    redirect('admin/beach/edit/' . $id);
                } else {
                    $this->Beach_model->update_cover($id, $new_name);
                    $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo l\'immagine di copertina',
                        'type' => 'success'));
                    redirect('admin/beach/edit/' . $id);
                }
            } else {
                $this->session->set_flashdata(Array('msg' => 'Per favore seleziona l\'immagine da caricare',
                    'type' => 'danger'));
                redirect('admin/beach/edit/' . $id);
            }
        } else {
            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
            redirect('admin/beach/edit/' . $id);
        }
    }

    public function delete() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'Beach ID',
                'rules' => 'required|numeric'
        ));
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('admin/Beach_model');
            $this->Beach_model->delete($this->input->post('id'));
            $this->session->set_flashdata(Array('msg' => 'Hai eliminato con successo la spiaggia!',
                'type' => 'success'));
        }
        redirect('admin/beach/');
    }

}
