<?php
session_start();
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Dashboard extends CI_Controller {

    private $userinfo;
    
    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('id') && $this->session->userdata('usertype') == 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('full_name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
    }

    public function index() {
		
        $this->load->model('HotelBB_model');
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['hotel_bb'] = count($this->HotelBB_model->listAll());
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->model('admin/Dashboard_model');
        $this->Dashboard_model->load();
        $data['cpage'] = 'dashboard';
        $this->load->view('admin/includes/header');
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/dashboard', $data);
        $this->load->view('admin/includes/footer');
    }

    public function logout() {
        $user = Array('id' => '',
            'email' => '',
            'usertype' => '',
            'full_name' => '');
        $this->session->set_userdata($user);
        $this->session->set_flashdata(Array('msg' => 'Hai effettuato il logout con successo!',
            'type' => 'success'));
        setcookie('user_cookie','',-1,'/');
        redirect('admin/login');
    }

    public function settings() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['cpage'] = 'settings';
        $this->load->helper('Form');
        $this->load->model('admin/Settings_model');
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $data['settings'] = $this->Settings_model->getsettings();
        $this->load->view('admin/includes/header');
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/settings', $data);
        $this->load->view('admin/includes/footer');
    }

    public function savehomemeta() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'meta_tag_en',
                'label' => 'Meta Title [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'meta_tag_du',
                'label' => 'Meta Title [du]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_tag_it',
                'label' => 'Meta Title [it]',
                'rules' => 'max_length[250]'
            ),
            array('filed' => 'meta_desc_en',
                'label' => 'Meta Description [en]',
                'rules' => 'required|max_length[1000]'),

            array('filed' => 'meta_desc_du',
                'label' => 'Meta Description [en]',
                'rules' => 'max_length[1000]'),

            array('filed' => 'meta_desc_it',
                'label' => 'Meta Description [it]',
                'rules' => 'max_length[1000]'),
            array(
                'field' => 'meta_keywords_en',
                'label' => 'Meta Keywords [en]',
                'rules' => 'required|max_length[250]'
            ),
            array(
                'field' => 'meta_keywords_du',
                'label' => 'Meta Keywords [du]',
                'rules' => 'max_length[250]'
            ),
            array(
                'field' => 'meta_keywords_it',
                'label' => 'Meta Keywords [it]',
                'rules' => 'max_length[250]'
            ),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('admin/Settings_model');
            $this->Settings_model->savemetaSettings();
            $this->session->set_flashdata(Array('msg' => "Hai aggiornato con successo le informazioni di meta tag della homepage!",
                'type' => 'success'));
            redirect('admin/dashboard/settings');
        } else {
            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
            redirect('admin/dashboard/settings');
        }
    }

    public function savepaypal() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'api_username',
                'label' => 'Paypal API User Name',
                'rules' => 'required|max_length[100]'
            ),
            array(
                'field' => 'api_password',
                'label' => 'Paypal API Password',
                'rules' => 'required|max_length[98]'
            ),
            array(
                'field' => 'api_key',
                'label' => 'Paypal API Key',
                'rules' => 'required|max_length[98]'
            ),
            array('filed' => 'offline_payment',
                'label' => 'Offline Payment',
                'rules' => 'required|numeric')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('admin/Settings_model');
            $this->Settings_model->savepaypal();
            $this->session->set_flashdata(Array('msg' => "Hai aggiornato con successo le impostazioni dell'account Paypal",
                'type' => 'success'));
            redirect('admin/dashboard/settings');
        } else {
            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
            redirect('admin/dashboard/settings');
        }
    }

    public function savegeneralsettings(){
         $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'radius',
                'label' => 'Radius KM',
                'rules' => 'required|max_length[100]|decimal'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('admin/Settings_model');
            $this->Settings_model->saveradius($this->input->post('radius'));
            $this->session->set_flashdata(Array('msg' => "Hai aggiornato con successo le impostazioni generali",
                'type' => 'success'));
            redirect('admin/dashboard/settings');
        } else {
            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
            redirect('admin/dashboard/settings');
        }
    }
    public function resetpassword() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'password',
                'label' => 'Current Password',
                'rules' => 'required'
            ),
            array(
                'field' => 'n_password',
                'label' => 'New Password',
                'rules' => 'required|min_length[6]|max_length[30]'
            ),
            array(
                'field' => 'c_password',
                'label' => 'Confirm Password',
                'rules' => 'required|matches[n_password]'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('admin/Settings_model');
            $res = $this->Settings_model->resetpassword();
            if ($res === true) {
                $this->session->set_flashdata(Array('msg' => "Hai modificato con successo la password",
                    'type' => 'success'));
            } else {
                $this->session->set_flashdata(Array('msg' => $res,
                    'type' => 'success'));
            }
        } else {
            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
        }
        redirect('admin/dashboard/settings');
    }

}
