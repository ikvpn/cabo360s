<?php

if (!defined("BASEPATH")) {
    exit('No direct script access is allowed');
}

class Pricing extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')  && $this->session->userdata('usertype') == 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('full_name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('Categories');
        $this->load->helper('form');
        $data['categories'] = $this->Categories->getAllList();
        $data['cpage'] = 'pricing';
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#pricing-dtable').dataTable();
                                                        });")
        );
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/pricing/index', $data);
        $this->load->view('admin/includes/footer');
    }

    public function update() {
        // update price
        $config = array(
            array('field' => 'id', 'label' => 'ID', 'rules'=>'required|numeric'),
            array('field' => 'y_charges', 'label' => 'Yearly Charges','rules' => 'required|numeric'),
            array('field' => 'm_charges', 'label' => 'Monthly Charges','rules' =>'required|numeric'),
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $this->load->model('Categories');
            $this->Categories->update($this->input->post('id'), array('y_charges' => $this->input->post('y_charges'), 'm_charges' => $this->input->post('m_charges')));
            $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo il prezzo',
                'type' => 'success'));
        } else {
            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
        }
        redirect('admin/pricing/');
    }

}
