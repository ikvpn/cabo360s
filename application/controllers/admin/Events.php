<?php

if (!defined("BASEPATH")) {
    exit("No direct script access is allwoed");
}

class Events extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id') && $this->session->userdata('usertype') == 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('admin/login');
        }
        $this->load->helper('language');
        $this->lang->load('customerpanel/Common');
        $this->lang->load('customerpanel/Events');
        $this->load->model('customer/Customer_model');
        $this->load->model('Event_model');
    }

    public function index() {
         $this->load->helper('form');
        $data['userinfo'] = $this->userinfo;
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#events-dtable').dataTable();

                                                        });")
        );
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['cpage'] = 'events';
        $data['sub_cpage'] = 'all-events';
        $data['orders'] = $this->Event_model->fetchAll();
        $this->load->model('Categories');
        $data['all_categories'] = $this->Categories->getAllList();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/includes/sidebar', $data);
        $this->load->view('admin/contents/events/index', $data);
        $this->load->view('admin/includes/footer', $data);
    }

    public function add() {
         // var_dump($_REQUEST);exit();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Event_model');
        $page = 'admin/contents/events/edit';
        $this->load->model('admin/Location_model');
        $data['locations'] = $this->Location_model->getLocations();
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/datepicker/bootstrap-datepicker.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/timepicker/bootstrap-timepicker.css')
        );
        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/datepicker/bootstrap-datepicker.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/timepicker/bootstrap-timepicker.js'),
            Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                                     $('.datepicker').datepicker();
                                                                      $('.timepicker').timepicker({showMeridian: false, defaultTime: false, showInputs: false});
                                                        });")
        );

        $config = array(
            array('field' => 'id', 'label' => 'Event id', 'rules' => 'required|numeric'),

            array('field' => 'enTitle', 'label' => 'Name [en]', 'rules' => 'max_length[50]'),
            array('field' => 'duTitle', 'label' => 'Name [de]', 'rules' => 'max_length[50]'),
            array('field' => 'itTitle', 'label' => 'Name [it]', 'rules' => 'required|max_length[50]'),

            array('field' => 'enDescription', 'label' => 'Description [en]', 'rules' => 'max_length[50000]'),
            array('field' => 'duDescription', 'label' => 'Description [de]', 'rules' => 'max_length[50000]'),
            array('field' => 'itDescription', 'label' => 'Description [it]', 'rules' => 'required|max_length[50000]'),

            array('field' => 'startDate', 'label' => 'Start Date', 'rules' => 'required|callback_date_valid'),
            array('field' => 'startTime', 'label' => 'Start Time', 'rules' => 'required'),
            array('field' => 'endDate', 'label' => 'End Date', 'rules' => 'required|callback_date_valid'),
            array('field' => 'endTime', 'label' => 'End Time', 'rules' => 'required'),
            array('field' => 'lat', 'label' => 'Latitude', 'rules' => 'required|numeric'),
            array('field' => 'lng', 'label' => 'Longitude', 'rules' => 'required|numeric'),
            array('field' => 'location_id','label' => 'Location','rules' => 'required|numeric'),
            array('field' => 'pAdult', 'label' => 'Adults Price', 'rules' => 'required|numeric'),
            array('field' => 'pChild', 'label' => 'Child\'s Price', 'rules' => 'required|numeric'),
            array('field' => 'pGuest', 'label' => 'Guest Price', 'rules' => 'required|numeric'),
            array('field' => 'category', 'label' => 'Event Category', 'rules' => 'required'),
            array('field' => 'type', 'label' => 'Event Type', 'rules' => 'required'),
            array('field' => 'reservation', 'label' => 'Reservation', 'rules' => 'required'),
            array('field' => 'web', 'label' => 'Web', 'rules' => 'max_length[100]'),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'valid_email'),
            array('field' => 'phone', 'label' => 'Guest Price', 'rules' => 'max_length[15]'),
            array('field' => 'mob', 'label' => 'Guest Price', 'rules' => 'max_length[15]'),
            array('field' => 'address', 'label' => 'Address', 'rules' => 'max_length[250]'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE && $page != '404') {
            if ($this->input->post('id') != '0') {
                $this->Event_model->id = $this->input->post('id');
            }
            $this->Event_model->enTitle = $this->input->post('enTitle') ? $this->input->post('enTitle') : $this->input->post('itTitle');
            $this->Event_model->duTitle = $this->input->post('duTitle') ? $this->input->post('duTitle') : $this->input->post('itTitle');
            $this->Event_model->itTitle = $this->input->post('itTitle') ;
            $this->Event_model->enDescription = $this->input->post('enDescription') ? $this->input->post('enDescription') : $this->input->post('itDescription');
            $this->Event_model->duDescription = $this->input->post('duDescription') ? $this->input->post('duDescription') : $this->input->post('itDescription');
            $this->Event_model->itDescription = $this->input->post('itDescription');
            $sd = explode("/", $this->input->post('startDate'));
            $this->Event_model->startDate = $sd[2] . '-' . $sd[0] . '-' . $sd[1];
            $this->Event_model->startTime = $this->input->post('startTime') . ":00";
            $ed = explode("/", $this->input->post('endDate'));
            $this->Event_model->endDate = $ed[2] . '-' . $ed[0] . '-' . $ed[1];
            $this->Event_model->endTime = $this->input->post('endTime') . ":00";
            $this->Event_model->lat = $this->input->post('lat');
            $this->Event_model->lng = $this->input->post('lng');
            $this->Event_model->pAdult = $this->input->post('pAdult');
            $this->Event_model->pChild = $this->input->post('pChild');
            $this->Event_model->pGuest = $this->input->post('pGuest');
            $this->Event_model->category = $this->input->post('category');
            $this->Event_model->type = $this->input->post('type');
            $this->Event_model->reservation = $this->input->post('reservation');
            $this->Event_model->web = $this->input->post('web');
            $this->Event_model->email = $this->input->post('email');
            $this->Event_model->phone = $this->input->post('phone');
            $this->Event_model->mob = $this->input->post('mob');
            $this->Event_model->address = $this->input->post('address');
            $this->Event_model->thumbnail = $this->Event_model->thumbnail ? $this->Event_model->thumbnail : ' ';
            $this->Event_model->cover = $this->Event_model->cover ? $this->Event_model->cover : ' ';
            $this->Event_model->status = "new";
            $this->Event_model->location_id = $this->input->post('location_id');
            $id = $this->Event_model->update();
            $this->session->set_flashdata(Array('msg' => 'Hai aggiunto con successo un nuovo evento',
                'type' => 'success'));
            redirect('admin/events/edit/' . $id);
        } else {
            $data['msg'] = $this->session->flashdata('msg');
            $data['type'] = $this->session->flashdata('type');
            $data['cpage'] = 'events';
            $data['sub_cpage'] = 'new-event';
            $this->load->model('Categories');
            $data['all_categories'] = $this->Categories->getAllList();
            $this->load->view('admin/includes/header', $data);
            $this->load->view('admin/includes/sidebar', $data);
            $this->load->view($page, $data);
            $this->load->view('admin/includes/footer', $data);
        }
    }

    public function edit($id = 0) {
        // var_dump($_REQUEST);exit();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Event_model');
        $this->Event_model->load($id);
        $page = 'admin/contents/events/edit';
        $this->load->model('admin/Location_model');
        $data['locations'] = $this->Location_model->getLocations();
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/datepicker/bootstrap-datepicker.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/timepicker/bootstrap-timepicker.css')
        );
        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/datepicker/bootstrap-datepicker.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/timepicker/bootstrap-timepicker.js'),
            Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                                     $('.datepicker').datepicker();
                                                                      $('.timepicker').timepicker({showMeridian: false, defaultTime: false, showInputs: false});
                                                        });")
        );

        $config = array(
            array('field' => 'id', 'label' => 'Event id', 'rules' => 'required|numeric'),

            array('field' => 'enTitle', 'label' => 'Name [en]', 'rules' => 'max_length[50]'),
            array('field' => 'duTitle', 'label' => 'Name [de]', 'rules' => 'max_length[50]'),
            array('field' => 'itTitle', 'label' => 'Name [it]', 'rules' => 'required|max_length[50]'),

            array('field' => 'enDescription', 'label' => 'Description [en]', 'rules' => 'max_length[50000]'),
            array('field' => 'duDescription', 'label' => 'Description [de]', 'rules' => 'max_length[50000]'),
            array('field' => 'itDescription', 'label' => 'Description [it]', 'rules' => 'required|max_length[50000]'),

            array('field' => 'startDate', 'label' => 'Start Date', 'rules' => 'required|callback_date_valid'),
            array('field' => 'startTime', 'label' => 'Start Time', 'rules' => 'required'),
            array('field' => 'endDate', 'label' => 'End Date', 'rules' => 'required|callback_date_valid'),
            array('field' => 'endTime', 'label' => 'End Time', 'rules' => 'required'),
            array('field' => 'lat', 'label' => 'Latitude', 'rules' => 'required|numeric'),
            array('field' => 'lng', 'label' => 'Longitude', 'rules' => 'required|numeric'),
            array('field' => 'location_id','label' => 'Location','rules' => 'required|numeric'),
            array('field' => 'pAdult', 'label' => 'Adults Price', 'rules' => 'required|numeric'),
            array('field' => 'pChild', 'label' => 'Child\'s Price', 'rules' => 'required|numeric'),
            array('field' => 'pGuest', 'label' => 'Guest Price', 'rules' => 'required|numeric'),
            array('field' => 'category', 'label' => 'Event Category', 'rules' => 'required'),
            array('field' => 'type', 'label' => 'Event Type', 'rules' => 'required'),
            array('field' => 'reservation', 'label' => 'Reservation', 'rules' => 'required'),
            array('field' => 'web', 'label' => 'Web', 'rules' => 'max_length[100]'),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'valid_email'),
            array('field' => 'phone', 'label' => 'Guest Price', 'rules' => 'max_length[15]'),
            array('field' => 'mob', 'label' => 'Guest Price', 'rules' => 'max_length[15]'),
            array('field' => 'address', 'label' => 'Address', 'rules' => 'max_length[250]'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE && $page != '404') {
            if ($this->input->post('id') != '0') {
                $this->Event_model->id = $this->input->post('id');
            }
            $this->Event_model->enTitle = $this->input->post('enTitle') ? $this->input->post('enTitle') : $this->input->post('itTitle');
            $this->Event_model->duTitle = $this->input->post('duTitle') ? $this->input->post('duTitle') : $this->input->post('itTitle');
            $this->Event_model->itTitle = $this->input->post('itTitle');
            $this->Event_model->enDescription = $this->input->post('enDescription') ? $this->input->post('enDescription') : $this->input->post('itDescription');
            $this->Event_model->duDescription = $this->input->post('duDescription') ? $this->input->post('duDescription') : $this->input->post('itDescription');
            $this->Event_model->itDescription = $this->input->post('itDescription');
            $sd = explode("/", $this->input->post('startDate'));
            $this->Event_model->startDate = $sd[2] . '-' . $sd[0] . '-' . $sd[1];
            $this->Event_model->startTime = $this->input->post('startTime') . ":00";
            $ed = explode("/", $this->input->post('endDate'));
            $this->Event_model->endDate = $ed[2] . '-' . $ed[0] . '-' . $ed[1];
            $this->Event_model->endTime = $this->input->post('endTime') . ":00";
            $this->Event_model->lat = $this->input->post('lat');
            $this->Event_model->lng = $this->input->post('lng');
            $this->Event_model->pAdult = $this->input->post('pAdult');
            $this->Event_model->pChild = $this->input->post('pChild');
            $this->Event_model->pGuest = $this->input->post('pGuest');
            $this->Event_model->category = $this->input->post('category');
            $this->Event_model->type = $this->input->post('type');
            $this->Event_model->reservation = $this->input->post('reservation');
            $this->Event_model->web = $this->input->post('web');
            $this->Event_model->email = $this->input->post('email');
            $this->Event_model->phone = $this->input->post('phone');
            $this->Event_model->mob = $this->input->post('mob');
            $this->Event_model->address = $this->input->post('address');
            $this->Event_model->thumbnail = $this->Event_model->thumbnail ? $this->Event_model->thumbnail : ' ';
            $this->Event_model->cover = $this->Event_model->cover ? $this->Event_model->cover : ' ';
            $this->Event_model->status = "new";
            $this->Event_model->location_id = $this->input->post('location_id');
            $this->Event_model->update();
            $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo i dettagli dell\'evento.',
                'type' => 'success'));
            redirect('admin/events/edit/' . $id);
        } else {
            $data['msg'] = $this->session->flashdata('msg');
            $data['type'] = $this->session->flashdata('type');
            $data['cpage'] = 'events';
            $data['sub_cpage'] = 'all-events';
            $this->load->model('Categories');
            $data['all_categories'] = $this->Categories->getAllList();
            $this->load->view('admin/includes/header', $data);
            $this->load->view('admin/includes/sidebar', $data);
            $this->load->view($page, $data);
            $this->load->view('admin/includes/footer', $data);
        }
    }

    public function upload_image($id = 0) {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Event_model');
        $this->Event_model->load($id);
        if ($this->session->userdata('usertype') != 'admin' || !$this->Event_model->id) {
            $this->session->set_flashdata(Array('msg' => 'Non hai ancora inserito i dettagli dell\'evento',
                'type' => 'error'));
            redirect('customer/events/edit/' . $id);
        }
        if ($_FILES && $_FILES['p_image']['name']) {
            $config['upload_path'] = './uploads/events/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '5000';
            $new_name = time() . $_FILES["p_image"]['name'];
            $config['file_name'] = $new_name;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('p_image')) {
                $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                    'type' => 'danger'));
                redirect('admin/events/edit/' . $id);
            } else {
                if ($this->input->post('imageType') == 'cover') {
                    $this->Event_model->update_photo(array('cover' => $new_name));
                } else {
                    $this->Event_model->update_photo(array('thumbnail' => $new_name));
                }
                $this->session->set_flashdata(Array('msg' => 'hai caricato con successo l\'immagine primaria',
                    'type' => 'success'));
                redirect('admin/events/edit/' . $id);
            }
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore seleziona l\'immagine da caricare',
                'type' => 'danger'));
            redirect('admin/events/edit/' . $id);
        }
    }

     public function delete() {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'Place ID',
                'rules' => 'required|numeric'
        ));
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {
            $this->load->model('Event_model');
            $this->Event_model->load($this->input->post('id'));
            $this->Event_model->delete();
            $this->session->set_flashdata(Array('msg' => 'Hai eliminato con successo l\'evento. ',
                'type' => 'success'));
        }
        redirect('admin/events/');
    }
    /**
     * Validate mm/dd/yyyy
     */
    public function date_valid($date) {
        $parts = explode("/", $date);
        if (count($parts) == 3) {
            if (checkdate((int) $parts[0], (int) $parts[1], (int) $parts[2])) {
                return TRUE;
            }
        }
        $this->form_validation->set_message('date_valid', 'La data deve essere di questo formato MM/DD/YYYY');
        return false;
    }

}
