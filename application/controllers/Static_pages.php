<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Static_pages extends CI_Controller {

    public $meta = array();
    public $data = array();
    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Ads_model');
        $this->load->model('Orders_model');
        $this->load->model('Cart_model');
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');
        $this->set_meta();
    }

    public function index($page) {
        $this->lang->load('Welcomepage');
        $this->load->model('Menu_model');
         $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');

        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');

        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();
        $data = $this->data;
        $data[] = $this->get_meta($page);
        $data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->model('admin/Location_model');
        $data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model("Places_of_interest_model");
        $data['m_poi'] = $this->Places_of_interest_model->getMenuList();
         $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());
		$this->load->helper('cookie');

    $this->load->model('Procida_model');
    $cartCookie = $this->input->cookie('prcrt');

    if(!empty($cartCookie)){
      $items = $this->Cart_model->getUserCart($cartCookie, '');
      $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
      $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
      $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
      if(!empty($tours)){
        for ($i=0; $i < count($tours); $i++) {
          $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
        }
      }
    } else {
      $items = [];
      $hotelCart = [];
      $tranferCart = [];
      $tours = []; // show random tours
    }
			$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/staticpages/' . $this->lang->lang() . '/' . $page, $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function get_meta($page) {
        return isset($this->meta[$page][$this->lang->lang()]) ? $this->meta[$page][$this->lang->lang()] : array("meta_keywords" => "", "meta_desc" => "", "meta_title" => "");
    }

    private function set_meta() {      

      $this->meta['terms-conditions'] = array(
          "en" => array(
              "meta_keywords" => "privacy, policy, terms, conditions, privacy policy, terms and conditions",
              "meta_desc" => "Privacy Policy , Terms and Conditions",
              "meta_title" => "Privacy Policy | Terms and Conditions | VisitProcida.it"),

          "it" => array(
              "meta_keywords" => "privacy, policy, termini, condizioni, privacy policy, termini e condizioni",
              "meta_desc" => "Privacy Policy , Termini e Condizioni",
              "meta_title" => "Privacy Policy | Termini e Condizioni | VisitProcida.it"),

          "de" => array(
              "meta_keywords" => "privacy, policy, terms, conditions, privacy policy, terms and conditions",
              "meta_desc" => "Privacy Policy , Terms and Conditions",
              "meta_title" => "Privacy Policy | Terms and Conditions | VisitProcida.it"),
      );

      $this->meta['privacy-policy'] = array(
          "en" => array(
              "meta_keywords" => "privacy, policy, terms, conditions, privacy policy, terms and conditions",
              "meta_desc" => "Privacy Policy , Terms and Conditions",
              "meta_title" => "Privacy Policy | Terms and Conditions | VisitProcida.it"),

          "it" => array(
              "meta_keywords" => "privacy, policy, termini, condizioni, privacy policy, termini e condizioni",
              "meta_desc" => "Privacy Policy , Termini e Condizioni",
              "meta_title" => "Privacy Policy | Termini e Condizioni | VisitProcida.it"),

          "de" => array(
              "meta_keywords" => "privacy, policy, terms, conditions, privacy policy, terms and conditions",
              "meta_desc" => "Privacy Policy , Terms and Conditions",
              "meta_title" => "Privacy Policy | Terms and Conditions | VisitProcida.it"),
      );

        $this->meta['traghetti-ischia-procida'] = array(
            "en" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Ischia a Procida: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Ischia - Procida | Traghetti e Aliscafi Procida | VisitProcida.it"),

            "it" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Ischia a Procida: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Ischia - Procida | Traghetti e Aliscafi Procida | VisitProcida.it"),

            "de" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Ischia a Procida: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Ischia - Procida | Traghetti e Aliscafi Procida | VisitProcida.it")
        );

        $this->meta['traghetti-napoli-procida'] = array(
            "en" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Napoli a Procida: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Napoli - Procida | Traghetti e Aliscafi Procida | VisitProcida.it"),

            "it" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Napoli a Procida: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Napoli - Procida | Traghetti e Aliscafi Procida | VisitProcida.it"),

            "de" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Napoli a Procida: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Napoli - Procida | Traghetti e Aliscafi Procida | VisitProcida.it")
            );

        $this->meta['traghetti-pozzuoli-procida'] = array(
            "en" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Pozzuoli a Procida: Caremar, Medmar, Gestur.",
                "meta_title" => "Orari Pozzuoli - Procida | Traghetti e Aliscafi Procida | VisitProcida.it"),

            "it" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Pozzuoli a Procida: Caremar, Medmar, Gestur.",
                "meta_title" => "Orari Pozzuoli - Procida | Traghetti e Aliscafi Procida | VisitProcida.it"),

            "de" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Pozzuoli a Procida: Caremar, Medmar, Gestur.",
                "meta_title" => "Orari Pozzuoli - Procida | Traghetti e Aliscafi Procida | VisitProcida.it")
            );

        $this->meta['traghetti-procida-ischia'] = array(
            "en" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Procida a Ischia: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Procida - Ischia | Traghetti e Aliscafi Procida | VisitProcida.it"),
            "it" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Procida a Ischia: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Procida - Ischia | Traghetti e Aliscafi Procida | VisitProcida.it"),
            "de" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Procida a Ischia: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Procida - Ischia | Traghetti e Aliscafi Procida | VisitProcida.it")
            );

        $this->meta['traghetti-procida-napoli'] = array(
            "en" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Procida a Napoli: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Procida - Napoli | Traghetti e Aliscafi Procida | VisitProcida.it"),
            "it" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Procida a Napoli: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Procida - Napoli | Traghetti e Aliscafi Procida | VisitProcida.it"),
            "de" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Procida a Napoli: Caremar, Medmar, Snav.",
                "meta_title" => "Orari Procida - Napoli | Traghetti e Aliscafi Procida | VisitProcida.it")
            );

        $this->meta['traghetti-procida-pozzuoli'] = array(
            "en" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Procida a Pozzuoli: Caremar, Medmar, Gestur.",
                "meta_title" => "Orari Procida - Pozzuoli | Traghetti e Aliscafi Procida | VisitProcida.it"),
            "it" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Procida a Pozzuoli: Caremar, Medmar, Gestur.",
                "meta_title" => "Orari Procida - Pozzuoli | Traghetti e Aliscafi Procida | VisitProcida.it"),
            "de" => array(
                "meta_keywords" => "orari, traghetti, aliscafi, navi, napoli, procida, ischia, pozzuoli",
                "meta_desc" => "Orari traghetti e aliscafi da Procida a Pozzuoli: Caremar, Medmar, Gestur.",
                "meta_title" => "Orari Procida - Pozzuoli | Traghetti e Aliscafi Procida | VisitProcida.it")
            );


        /* NEW STATIC PAGE META */
        /* $this->meta['example-page'] = array(
            "en" => array(
                "meta_keywords" => "example",
                "meta_desc" => "example",
                "meta_title" => "example"),
            "it" => array(
                "meta_keywords" => "example",
                "meta_desc" => "example",
                "meta_title" => "example"),
            "de" => array(
                "meta_keywords" => "example",
                "meta_desc" => "example",
                "meta_title" => "example")
            );*/

    }

}
