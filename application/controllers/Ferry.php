<?php

class Ferry extends CI_Controller{

    private $api_url = 'http://booking.traghettilines.it/webservice.asmx';
    private $user = 'info@gioiatredici.it';
    private $pass = 'tredici80079';
    private $options = array(
                'uri'=>'http://schemas.xmlsoap.org/soap/envelope/',
                'style'=>SOAP_RPC,
                'use'=>SOAP_ENCODED,
                'soap_version'=>SOAP_1_1,
                'cache_wsdl'=>WSDL_CACHE_NONE,
                'connection_timeout'=>15,
                'trace'=>true,
                'encoding'=>'UTF-8',
                'exceptions'=>true,
              );

    public function __construct() {
        parent::__construct();
    }

    public function getDestinations(){
      $this->load->model('admin/Ferry_model');

      $params = [
        'lang'      => $this->lang->lang(),
        'user'      => $this->user,
        'password'  => $this->pass
      ];

      $wsdl = 'https://www.traghettilines.it/webaffiliation.asmx?wsdl';

      try {

        $soap = new SoapClient($wsdl, $this->options);
        $data = $soap->getDestinations($params);

      } catch(Exception $e) {
        die($e->getMessage());
      }

      if(!empty($data->getDestinationsResult)){
        $destinations = $data->getDestinationsResult->destinazione;

        for ($i=0; $i<count($destinations); $i++) {
          if($destinations[$i]->url_rewrite === 'traghetti-ischia'){
            $newDestination = array('external_id' => $destinations[$i]->id,
                                     'descrizione' => $destinations[$i]->descrizione,
                                     'url_rewrite' => $destinations[$i]->url_rewrite);

            $this->Ferry_model->addDestination($newDestination);
          }
        }
      }

    }

    public function getRoutes(){

      $this->load->model('admin/Ferry_model');

      $destId = $this->Ferry_model->getDestination('traghetti-ischia');

      $params = [
        'lang'      => $this->lang->lang(),
        'user'      => $this->user,
        'password'  => $this->pass,
        'destId'    => $destId['external_id']
      ];

      $wsdl = 'https://www.traghettilines.it/webaffiliation.asmx?wsdl';

      try {

        $soap = new SoapClient($wsdl, $this->options);
        $data = $soap->getRoutes($params);

      } catch(Exception $e) {
        die($e->getMessage());
      }

      if(!empty($data->getRoutesResult->tratta)){
        $routes = $data->getRoutesResult->tratta;
        for ($i=0; $i<count($routes); $i++) {
          $description = strtolower($routes[$i]->descrizione);
          if (strpos($description, 'procida') !== false) {

            $newRoute = array( 'ferry_destination_external_id' => $destId['external_id'],
                                     'external_id' => $routes[$i]->id,
                                     'descrizione' => $routes[$i]->descrizione,
                                     'flag_auto'   => $routes[$i]->flag_auto,
                                     'per'         => $routes[$i]->per);

            $this->Ferry_model->addRoute($newRoute);
          }
        }
      }

    }

    public function getDepartures(){
      $this->load->model('admin/Ferry_model');

      $purge = $this->input->get('clear');

      if($purge == "yes"){
        $this->Ferry_model->cleanTable();
        return;
      }

      $i = $this->input->get('i');

      if($i == "" || $i == NULL){
        return;
      }

      $date = date("Y-m-d");
      $routes = $this->Ferry_model->findRoutes();
      $countRoutes = count($routes);
      $tratta = $routes[$i]->external_id;

      $params = [
        'lang'          => $this->lang->lang(),
        'user'          => $this->user,
        'password'      => $this->pass,
        'dataAndata'    => $date.'T00:00:01',
        'trattaAndata'  => $tratta
      ];

      $wsdl = 'https://www.traghettilines.it/webaffiliation.asmx?wsdl';

      try {
        $soap = new SoapClient($wsdl, $this->options);
        $data = $soap->getDepartures($params);
      } catch(Exception $e) {
        die($e->getMessage());
      }

      if(isset($data->getDeparturesResult->partenza)){

        if(count($data->getDeparturesResult->partenza) > 1){

          $departuresData = $data->getDeparturesResult->partenza;

          foreach ($departuresData as $departureData) {

            $newDestination = array(
              'ferry_routes_id'   => $routes[$i]->id,
              'ext_id'            => $departureData->id,
              'compagnia'         => $departureData->compagnia,
              'urlLogoCompagnia'  => $departureData->urlLogoCompagnia,
              'datapartenza'      => $departureData->datapartenza,
              'orapartenza'       => $departureData->orapartenza,
              'dataarrivo'        => $departureData->dataarrivo,
              'oraarrivo'         => $departureData->oraarrivo,
              'durata'            => $departureData->durata,
              'autodisponibile'   => $departureData->autodisponibile,
              'prezzo1pax'        => $departureData->prezzo1pax,
            );

            $this->Ferry_model->addDeparture($newDestination);
          }

        } else {

          $departureData = $data->getDeparturesResult->partenza;

          $newDestination = array(
            'ferry_routes_id'   => $routes[$i]->id,
            'ext_id'            => $departureData->id,
            'compagnia'         => $departureData->compagnia,
            'urlLogoCompagnia'  => $departureData->urlLogoCompagnia,
            'datapartenza'      => $departureData->datapartenza,
            'orapartenza'       => $departureData->orapartenza,
            'dataarrivo'        => $departureData->dataarrivo,
            'oraarrivo'         => $departureData->oraarrivo,
            'durata'            => $departureData->durata,
            'autodisponibile'   => $departureData->autodisponibile,
            'prezzo1pax'        => $departureData->prezzo1pax,
          );

          $this->Ferry_model->addDeparture($newDestination);
        }

      }

    }

}
