

<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */


if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class What_to_visit extends CI_Controller {

    private $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->model('admin/Location_model');
        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model("Places_of_interest_model");
        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
        $this->load->model('admin/Ads_model');
        $this->load->model('Orders_model');
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');
        $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');
        $this->load->model('Service_model');
        $this->load->model('Agencies_model');
		$this->load->model('Cart_model');
        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');

        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();
        $this->data['counts']['escursions-and-visit'] = $this->Service_model->totalActiveServices();
        $this->data['counts']['agencies'] = $this->Agencies_model->totalActiveServices();

        $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());



    }

    public function index($title = '', $place = '') {
        switch ($title) {
            case 'overview':
                $this->overview();
                return;
            case 'locations':
                $this->location_listing();
                return;
            case 'places_of_interest':
            case 'places-of-interest':
                $this->places_of_interest($place);
                return;
            case '':
                show_404();
                return;
        }

        $this->load->model('admin/Location_model');
        $data = $this->data;

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = [];
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $data['location'] = $this->Location_model->getLocationForFrontEnd(str_replace("-", " ", $title), $title, $this->lang->lang());
        if (!$data['location']) {
            show_404();
            return;
        }
        $temp = (array) $data['location'];
        //var_dump($temp);exit();
        $data['meta_title'] = isset($temp['meta_' . $this->lang->lang()]) ? $temp['meta_' . $this->lang->lang()] : '';
        $data['meta_keywords'] = isset($temp['meta_' . $this->lang->lang()]) ? $temp['meta_keywords_' . $this->lang->lang()] : '';
        $data['meta_desc'] = isset($temp['meta_desc_' . $this->lang->lang()]) ? $temp['meta_desc_' . $this->lang->lang()] : '';
        $data['cssfiles'] = array(
            array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
            array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
        );
        $data['jsfiles'] = array(
            array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
            array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
            array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js"),
            array('type' => 'inline', 'src' => " var MAP_ID='map-canvas';  var CUR_LAT= ".$temp['lat']."; var CUR_LNG=".$temp['lng']),
            array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
        );
        $this->lang->load('Locationpage');
        $this->load->view('frontend/includes/header', $data);
        $this->load->model('Procida_model');
        $this->load->model('Places_of_interest_model');
        $data['places'] = $this->Places_of_interest_model->fetch(array('location_id' => $data['location']->id, 'status' => 'active'));
        $data['resturants'] = $this->Procida_model->getResturants("r.*,o.location,o.title as pageTitle", "o.status='paid' AND o.location=" . $data['location']->id);
        $data['hotels'] = $this->Procida_model->getHotels("h.*,o.location", "o.status='paid' AND o.location=" . $data['location']->id);
        $data['beaches'] = $this->Procida_model->getBeaches("b.*,bt.title", "b.location_id=" . $data['location']->id, FALSE, $this->lang->lang());
        $data['shops'] = $this->Procida_model->getShops("s.*,o.location,o.title", "o.status='paid' AND o.location=" . $data['location']->id);
        $data['barsandcafe'] = array();
        $data['near_locations'] = $this->Location_model->getLocations(false, $this->lang->lang());
   
  $this->load->model('Tour_model');
        $this->lang->load('Tourspage');

        $data['tours'] = $this->Tour_model->getActiveTours();

        $this->load->view('frontend/content/location-page', $data);

        $this->load->view('frontend/includes/footer', $data);
    }

    private function overview() {
        $this->lang->load('Overviewpage');
        $this->lang->load('Welcomepage');
        $this->load->model('admin/Location_model');
        $data = $this->data;

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $data['loc_data'] = $this->Location_model->getLocations(false, $this->lang->lang());
        $this->load->model('Event_model');
        $this->load->model('Menu_model');
        $data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->view('frontend/includes/header', $data);
        $this->load->model('Resturant_model');
        $data['resturants'] = $this->Resturant_model->getActiveResturants($this->lang->lang());
        $this->load->model('Hotel_model');
        $data['hotels'] = $this->Hotel_model->getActiveHotels($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $data['beaches'] = $this->Beach_model->getBeaches(true, $this->lang->lang());
        $this->load->model('Shope_model');
        $data['shops'] = $this->Shope_model->getActiveShops($this->lang->lang());
        $this->load->view('frontend/content/what_to_visit_overview', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function location_listing(){
        $data = $this->data;
        switch ($this->lang->lang()) {
          case 'en':
                    $data['meta_keywords'] = 'locations, Procida';
                    $data['meta_desc'] = 'Locations of Procida: the port of Marina Grande, the ancient village of Terra Murata, the fishing village of Marina Corricella, the port of Chiaiolella';
                    $data['meta_title'] = 'Locations and areas to visit in Procida';
                    break;
                case 'it':
                    $data['meta_keywords'] = '';
                    $data['meta_desc'] = 'Località e quartieri di Procida: il porto Marina Grande, lantico borgo di Terra Murata, il villaggio dei pescatori di Marina Corricella, il porto di  Chiaiolella';
                    $data['meta_title'] = 'Località e quartieri da visitare a Procida';
                    break;
                case 'de':
                    $data['meta_keywords'] = '';
                    $data['meta_desc'] = 'Lage von Procida und Nachbarschaften: der Hafen von Marina Grande, dem alten Dorf von Terra Murata, dem Fischerdorf Marina Corricella, den Hafen von Chiaiolella';
                    $data['meta_title'] = 'Lage und Nachbarschaften in Procida zu besuchen';
                    break;
        }

        $data['jsfiles'] = array(
              array('type' => 'inline', 'src' => " var MAP_ID='map-canvas'; var CUR_LAT= 0; var CUR_LNG=0;"),
              array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js")
        );

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );


        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/location-listing', $data);
        $this->load->view('frontend/includes/footer', $data);
        return;
    }
    private function places_of_interest($title) {
        if($title != ''){
            $this->places_of_interest_detail($title);
            return;
        }
        $data = $this->data;

        switch ($this->lang->lang()) {
            case 'en':
                $data['meta_keywords'] = 'museums, churches, procida';
                $data['meta_desc'] = 'Places of interest to visit in Procida: visiting churches, abbey, museums and natural reserves';
                $data['meta_title'] = 'What to visit in Procida: things to do in Procida Island';
                break;
            case 'it':
                $data['meta_keywords'] = 'musei, chiese, procida';
                $data['meta_desc'] = 'Luoghi di interesse da visitare a Procida: visita chiese, abbazia, musei e riserve naturali ';
                $data['meta_title'] = 'Cosa Visitare a Procida: Tutti i luoghi di interesse a Procida';
                break;
            case 'de':
                $data['meta_keywords'] = 'Museen, Kirchen, Procida';
                $data['meta_desc'] = 'Sehenswürdigkeiten in Procida zu besuchen: besuchen Kirchen, Abtei, Museen und Naturschutzgebiete';
                $data['meta_title'] = 'Was in Procida zu besuchen: Alle Sehenswürdigkeiten in Procida, Italien';
                break;
        }
           $data['jsfiles'] = array(
              array('type' => 'inline', 'src' => " var MAP_ID='map-canvas'; var CUR_LAT= 0; var CUR_LNG=0;"),
                  array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js")
        );
		$this->load->helper('cookie');

    $this->load->model('Procida_model');
    $cartCookie = $this->input->cookie('prcrt');

    if(!empty($cartCookie)){
      $items = $this->Cart_model->getUserCart($cartCookie, '');
      $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
      $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
      $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
      if(!empty($tours)){
        for ($i=0; $i < count($tours); $i++) {
          $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
        }
      }
    } else {
      $items = [];
      $hotelCart = [];
      $tranferCart = [];
      $tours = []; // show random tours
    }
			$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/places-of-interest-listing', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function places_of_interest_detail($title) {
        $this->lang->load('Welcomepage');
        $data = $this->data;
        $data['other_places'] = $this->Places_of_interest_model->fetch(array('enTitle <>' => str_replace('-', ' ', str_replace('_', ' ', $title)), 'status' => 'active'));
        $data['loc_data'] = $this->Location_model->getLocations(false, $this->lang->lang());
        $data['place'] = $this->Places_of_interest_model->fetch_single(array('enTitle' => str_replace('-', ' ', str_replace('_', ' ', $title)), 'status' => 'active'), array('rewrite_url_' . $this->lang->lang() => $title));

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $data['near_locations'] = $this->Location_model->getLocations(false, $this->lang->lang());
        if ($data['place']) {
            $temp = (array) $data['place'];
            $data['meta_keywords'] = $temp['meta_keywords_' . $this->lang->lang()];
            $data['meta_title'] = $temp['meta_tag_' . $this->lang->lang()];
            $data['meta_desc'] = $temp['meta_desc_' . $this->lang->lang()];
            $data['jsfiles'] = array(
                array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-header'; var CUR_LAT= ".$data['place']->lat."; var CUR_LNG=".$data['place']->lng.';'),
                array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
            );

            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/placesofinterest', $data);

            $this->load->view('frontend/includes/footer', $data);
        } else {
            show_404();
        }
    }

}
