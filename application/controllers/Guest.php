<?php

class Guest extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->model('');
    }

    public function index($page = "home") {
        // you might want to just autoload these two helpers
        $this->load->helper('language');
        $this->load->helper('url');

        // load language file
        $this->lang->load('Labels');

        //=========================================

        $data = array();
        $this->load->view('guest/includes/header', $data);
        $this->load->view('guest/content/' . $page, $data);
        $this->load->view('guest/includes/footer', $data);
        //$data['header'] = $this->load->view('header/head', '', TRUE);
        //$data['top_navi'] = $this->load->view('header/top_navigation', $data, TRUE);
        //$data['message'] = $message;
        // $data['content'] = $this->load->view('content/home_page', $data, TRUE);
        // $data['footer'] = $this->load->view('footer/footer', $data, TRUE);
        // $this->load->view('home', $data);
    }

    //=========================================================
    /* public function about_us($message = '')
      {
      $data = array();
      $data['header'] = $this->load->view('header/head', '', TRUE);
      $data['top_navi'] = $this->load->view('header/top_navigation', $data, TRUE);
      $data['message'] = $message;
      if (!$this->session->userdata('log')) {
      $data['user_role'] = $this->admin_model->get_user_role();
      $data['modal'] = $this->load->view('modals/login_n_register', $data, TRUE);
      }
      $data['about'] = $this->system_model->get_about_us();
      $data['content'] = $this->load->view('content/about_us', $data, TRUE);
      $data['footer'] = $this->load->view('footer/footer', $data, TRUE);
      $this->load->view('home', $data);
      }
     */
}
