<?php

require_once(APPPATH.'libraries/Stripe/init.php');

class Cart extends CI_Controller{
  // The basic logic behind this cart should be that:
  // in first time the user have no cookie about a procida cart
  // we go to create this cookie naming him like: prct
  // where we assign an uid and we use it to store information about this user in the cart table of our database
  // if the user have a cookie we load the old database information and we show to him this data.
  // if the user pay and end the process we delete all the info (??? must to check this part.)
  // An user is able to push obj in the cart in different way but the main idea is to have
  // a main category and then the other categories grouped and showed with a strategy based on what we are showing
  // let's assume we have an Hotel in the main category we go to show Transfert and Tickets as featured services
  // instehaed the opposit is displayed if a Ticket is in the main category, and so on..
  //
  // The object are pushed by an ajax call that make a simple action putting a single item in the db table every time an user
  // fire that action. While an user decide to delete this row we go to fire this action another time using ajax.
  // The Cart view display a list of items pushed in the cart table grouped by the uid stored in the user browser cookie.
  // After that user is able to proceed to pay. ( Next feature [payment gateway] )
  private $data;

  public function __construct() {
      parent::__construct();

      $this->load->model('Cart_model');
      $this->load->model('Menu_model');
      $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
      $this->load->model('admin/Location_model');
      $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
      $this->load->model('admin/Beach_model');
      $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
      $this->load->model("Places_of_interest_model");
      $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
      $this->load->model('admin/Ads_model');
      $this->load->helper('language');
      $this->load->helper('url');
      $this->lang->load('Navigation');
      $this->lang->load('Blogpage');
      $this->lang->load('Cart');
      $this->load->model('Shope_model');
      $this->load->model('Boatrentals_model');
      $this->load->model('Beachclub_model');
      $this->load->model('Weddings_model');
      $this->load->model('Apartment_model');
      $this->load->model('Hotel_model');
      $this->load->model('Cart_model');
      $this->load->model('Resturant_model');
      $this->load->model('Barsandcafe_model');
      $this->load->model('Nightlife_model');

      $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
      $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
      $this->data['counts']['watersports'] = 0;
      $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
      $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

      $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
      $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

      $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
      $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
      $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();

      $this->load->model('admin/Blog_model');
      $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());
  }

  public function index(){

    $this->load->helper('cookie');
    $this->load->model('Procida_model');
    $this->load->model('Tour_model');
    $cartCookie = $this->input->cookie('prcrt');

    if(!empty($cartCookie)){
      $items = $this->Cart_model->getUserCart($cartCookie, '');

      for ($i=0; $i < count($items); $i++) {
        # code...

        if($items[$i]->serviceType != 'transfer') {
          $items[$i]->checkIn =   date('d-m-Y', strtotime($items[$i]->checkIn));

          if ($items[$i]->serviceType != 'tour'){
            $items[$i]->checkOut =  date('d-m-Y', strtotime($items[$i]->checkOut));
          }
        }
      }

      $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
      for ($i=0; $i < count($hotelCart); $i++) {
        # code...
        $hotelCart[$i]->checkIn =   date('d-m-Y', strtotime($hotelCart[$i]->checkIn));
          $hotelCart[$i]->checkOut =  date('d-m-Y', strtotime($hotelCart[$i]->checkOut));

      }

      $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
      $pickUpPosints =  json_decode (PICK_UP_POINTS,True);
      foreach ($tranferCart as $key => $tranferCartVal) {
        $tranferCartVal->pickupAddress_id = array_search($tranferCartVal->pickupAddress,$pickUpPosints);
        $tranferCartVal->returnDropAddress_id = array_search($tranferCartVal->dropoffReturnAddress,$pickUpPosints);
      }
      $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
      if(!empty($tours)){
        for ($i=0; $i < count($tours); $i++) {
          $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
        }
      }
      if(!empty($hotelCart) && empty($tours) ){
        $hotel = $hotelCart[0];
        $checkIn = $hotel->checkIn;
        $checkOut= $hotel->checkOut;
        $toursSuggested =  $this->Tour_model->getTourInPeriod($checkIn, $checkOut);
      }
      else if(!empty($tranferCart) && empty($tours) ){
        $tranfer = $tranferCart[0];

        $checkIn = $tranfer->checkIn;
        $checkIn = explode(' ', $checkIn);
        $checkIn = $checkIn[0];

        $checkOut= $tranfer->checkOut;
        $checkOut = explode(' ', $checkOut
      );
        $checkOut = $checkOut[0];
        $toursSuggested =  $this->Tour_model->getTourInPeriod($checkIn, $checkOut);

        //echo "<pre>"; print_r($toursSuggested); die;
      }
      else {
        $toursSuggested = [];
      }
    } else {
      $items = [];
      $hotelCart = [];
      $tranferCart = [];
      $tours = [];
      $toursSuggested = [];
    }

	$this->data['items'] = $items;
	$this->data['hotelCart'] = $hotelCart;
  $this->data['toursSuggested'] = $toursSuggested;
	$this->data['tranferCart'] = $tranferCart;

  $this->data['tourCart'] = $tours;
	$data = $this->data;
  //echo "<pre>"; print_r($data['tourCart']); die;
	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );
    $data['hotels_transfer'] = $this->Hotel_model->getActiveHotels();

    $data['cssfiles'] = array(
      array('type' => 'file', 'src' => base_url() . 'assets/css/jquery.switch.css'),
      array('type' => 'file', 'src' => base_url() . 'assets/css/fontello/css/icon_set_1.css'),
      array('type' => 'file', 'src' => base_url() . 'assets/css/date-time-picker.css')
    );

    $data['jsfiles'] = array(
      array('type' => 'file', 'src' => base_url() . "assets/js/cart.js"),
      array('type' => 'file', 'src' => base_url() . "assets/js/bootstrap-datepicker.js"),
      array('type' => 'file', 'src' => base_url() . "assets/js/magnificPopup.js")
    );

    $this->load->view('frontend/includes/header', $data);
    $this->load->view('frontend/content/cart', $data);
    $this->load->view('frontend/includes/footer', $data);
  }

  public function add(){
    $this->load->helper('cookie');

    $cartCookie = $this->input->cookie('prcrt');

    if(empty($cartCookie) || $cartCookie == null ){
        $token = substr(uniqid($_SERVER['REQUEST_TIME']), 0, 10);
        $cookie = array(
                        'name'   => 'prcrt',
                        'value'  => $token,
                        'expire' => 60*60*24*7,
                        'secure' => FALSE
                        );
        $this->input->set_cookie($cookie);
        $cartCookie = $cookie['value'];
      }

    $cookieId = empty($cartCookie) ? $this->input->cookie('prcrt') : $cartCookie;

	$hotelCode = $this->input->post('hotel_code');
	$transferType = $this->input->post('transferType');
  $tourType = $this->input->post('tourType');

  if($hotelCode !== null && $hotelCode !== ""){
    if($this->Cart_model->checkExistElementInCart('hotel', $cookieId) == true){
      $message = 'Hai già un hotel nel carrello, se vuoi prenotarne un altro elimina quello attuale dal menù in alto';
      $data = array('success'=> false, 'message' => $message);
      echo json_encode($data);
      return ;
    }
    $roomInfo = array(
      'hotel_code' => $this->input->post('hotel_code'),
      'room_code' => $this->input->post('room_code'),
      'adults' => $this->input->post('adults'),
      'quota' => $this->input->post('quota'),
      'tratt_code' => $this->input->post('tratt_code'),
      'tratt_name' => $this->input->post('tratt_name'),
      'offerta_code' => $this->input->post('offerta_code'),
      'offerta_tipo_code' => $this->input->post('offerta_tipo_code'),
      'offerta_name' => $this->input->post('offerta_name')
    );

    $this->load->model('HotelBB_model');

    $roomInfoId = $this->HotelBB_model->insertRoomInfo($roomInfo);

	  $extraQuantity = $this->input->post('extraQuantity');
	  $extraId = $this->input->post('serviceId');
	  $extraType = $this->input->post('extraType');
	  $extraQuantity = $this->input->post('extraQuantity');
	  $extraPrice = $this->input->post('extraPrice');

    $item = array( 'cookieId' => $cookieId,
                   'serviceId' => $this->input->post('serviceId'),
                   'serviceType' => 'hotel',
                   'serviceQuantity' => $this->input->post('serviceQuantity'),
                   'servicePrice' => $this->input->post('servicePrice'),
                   'extraId' => $extraId,
                   'extraType' => $extraType,
                   'extraQuantity' => $extraQuantity,
                   'extraPrice' => $extraPrice,
                   'checkIn' => $this->input->post('checkIn'),
                   'checkOut' => $this->input->post('checkOut'),
                   'rooms' => $this->input->post('rooms'),
                   'roomInfoId' => $roomInfoId,
                   'roomName' => $this->input->post('roomName'),
                   'roomImage' => $this->input->post('roomImage'),
                   'nights' => $this->input->post('nights'),
                   'people' => $this->input->post('people'),
                   'children' => $this->input->post('children')
                 );


  } else if($transferType !== null  && $transferType !== "" && $transferType == true){
    if($this->Cart_model->checkExistElementInCart('transfer', $cookieId) == true){
      $message = 'Hai già un transfer nel carrello, se vuoi prenotarne un altro elimina quello attuale dal menù in alto';
      $data = array('success'=> false, 'message' => $message);
      echo json_encode($data);
      return ;
    }
    if(intval($this->input->post('serviceQuantity')) == 1){
      $checkOut = ' ';
    } else if(intval($this->input->post('serviceQuantity')) > 1){
      $checkOut = $this->input->post('returnDate').' '.$this->input->post('returnTime');
    }
    $item = array( 'cookieId' => $cookieId,
                   'serviceId' => 0,
                   'serviceType' => 'transfer',
                   'serviceQuantity' => $this->input->post('serviceQuantity'),
                   'servicePrice' => $this->input->post('price'),
                   'extraId' => null,
                   'extraType' => $this->input->post('transfer_type'),
                   'extraQuantity' => null,
                   'extraPrice' => null,
                   'checkIn' => $this->input->post('date').' '.$this->input->post('time'),
                   'checkOut' => $checkOut,
                   'rooms' => null,
                   'roomInfoId' => null,
                   'roomName' => null,
                   'roomImage' => null,
                   'nights' => null,
                   'people' => $this->input->post('people'),
                   'children' => $this->input->post('children'),
                   'infants' => $this->input->post('infants'),
                   'pets' => $this->input->post('pets'),
                   'pickupAddress' => $this->input->post('pickupAddress'),
                   'dropoffAddress' => $this->input->post('dropoffAddress'),
                   'pickupReturnAddress' => $this->input->post('pickupReturnAddress'),
                   'dropoffReturnAddress' => $this->input->post('dropoffReturnAddress'),
                   'flight_train_no' => $this->input->post('flight_train_no'),
                   'return_flight_train_no' => $this->input->post('return_flight_train_no'),
                   'special_request' => $this->input->post('special_request'),
                 );
  } else if($tourType !== null && $tourType !== "") {
    $check = $this->Cart_model->checkExistElementInCart('tour', $cookieId);
    if($check == true){
      $message = 'Hai già un tour nel carrello, se vuoi prenotarne un altro elimina quello attuale dal menù in alto';
      $data = array('success'=> false, 'message' => $message);
      echo json_encode($data);
      return ;
    }
      $item = array( 'cookieId' => $cookieId,
                     'serviceId' => $this->input->post('serviceId'),
                     'serviceType' => $this->input->post('serviceType'),
                     'serviceQuantity' => $this->input->post('serviceQuantity'),
                     'servicePrice' => $this->input->post('servicePrice'),
                     'extraId' => null,
                     'extraType' => $this->input->post('servicePriceUnit'),
                     'extraQuantity' => null,
                     'extraPrice' => null,
                     'checkIn' => $this->input->post('checkIn'),
                     'checkOut' => $this->input->post('checkOut'),
                     'rooms' => null,
                     'roomInfoId' => null,
                     'roomName' => null,
                     'roomImage' => null,
                     'nights' => null,
                     'people' => $this->input->post('people'),
                     'children' => $this->input->post('children'),
                     'pickupAddress' => null,
                     'dropoffAddress' => null,
                     'pickupReturnAddress' => null,
                     'dropoffReturnAddress' => null
                   );
  }

  $insert = $this->Cart_model->insert($item);
  $message = '';
  if($insert == false){
    $message = 'Ops, qualcosa è andato storto, contatta l\'amministrazione';
  }
  $data = array('success'=> $insert, 'message' => $message);

  echo json_encode($data);
}

  public function delete(){
    $this->load->helper('cookie');
    $cartCookie = $this->input->cookie('prcrt');

    $extraId = $this->input->post('extraId');

    if(!empty($extraId)){
      $removedExtra = $this->Cart_model->deleteExtra($extraId, $cartCookie);
    }

    $serviceId = $this->input->post('serviceId');

    if(!empty($serviceId)){
      $serviceId = $this->Cart_model->deleteRow($serviceId, $cartCookie);
    }

    $success = true;
    echo json_encode($success);
  }

  public function pay(){
    $this->load->helper('cookie');

    $this->load->model('HotelBB_model');
    $this->load->model('Procida_model');

    $this->data['hotel'] = false;

    $cartCookie = $this->input->cookie('prcrt');


    //echo "<pre>"; print_r($this->input->post()); die;

    $transferPayment = $this->input->post('transferP');
    $tourPayment = $this->input->post('tourP');
    $hotelPrices = $this->input->post('hotelP');

    $this->data['bookingInfo'] = array(
      'firstname'     => $this->input->post('firstname'),
      'lastname'	    => $this->input->post('lastname'),
      'email'         => $this->input->post('email'),
      'phone'         => $this->input->post('phone'),
      'street_1'	    => $this->input->post('street_1'),
      'street_2'	    => $this->input->post('street_2'),
      'city'	    => $this->input->post('city_booking'),
      'state_booking'	    => $this->input->post('state_booking'),
      'p_code'	    => $this->input->post('postal_code'),
      'trainOrFlight' => $this->input->post('trainOrFlight'),
      'paymentType'   => $this->input->post('cardType'),
      'total'         => intval($transferPayment) + intval($hotelPrices) + intval($tourPayment)
    );

    $success = array('hotel' => false, 'transfer' => false, 'tour' => false);

    $checkInHotel = '';
    $checkOutHotel = '';

    $tourData = '';
    $tourHour = '';
    $tourInfo = '';
    $touradults = $this->input->post('people-tour-cart');
    $tourchildren = $this->input->post('children-tour-cart');

    $transferForwardData = '';
    $transferReturnData = '';
    $pickupAddressForward = '';
    $dropoffAddressForward = '';
    $pickupAddressReturn = '';
    $dropoffAddressReturn = '';
    $peopleTranfer = $this->input->post('transfer-people-cart');

    $items = $this->Cart_model->getUserCart($cartCookie, '');



    if($hotelPrices > 0){

      $this->data['hotel'] = true;
      $ch = curl_init();
      $rooms = [];

      for($i=0; $i<count($items); $i++){
          // Create rooms array to put in $query. [partner_data]
          if($items[$i]->serviceType == 'hotel'){
            $checkInHotel = $items[$i]->checkIn;
            $checkOutHotel = $items[$i]->checkOut;
            if(intval($items[$i]->rooms) > 0){
              for ($j=1; $j <= intval($items[$i]->rooms) ; $j++) {
                $room = [];

                $room['partner_data'] = $this->HotelBB_model->getRoomInfo($items[$i]->roomInfoId);
                $adults = $room['partner_data']->adults;
                $children = $items[$i]->children;
                $this->data['bookingInfo']['rooms'][$i]['hotelName'] = $items[$i]->hotelName;
                $this->data['bookingInfo']['rooms'][$i]['roomName'] = $items[$i]->roomName;
                $this->data['bookingInfo']['rooms'][$i]['adults']   = $items[$i]->people;
                $this->data['bookingInfo']['rooms'][$i]['children'] = $items[$i]->children;
                $this->data['bookingInfo']['rooms'][$i]['rooms']    = $items[$i]->rooms; // getValue from form input
                $this->data['bookingInfo']['rooms'][$i]['nights'] = $items[$i]->nights;
                $this->data['bookingInfo']['rooms'][$i]['checkIn']  = $items[$i]->checkIn;
                $this->data['bookingInfo']['rooms'][$i]['checkOut'] = $items[$i]->checkOut;
                $this->data['bookingInfo']['rooms'][$i]['price'] = $hotelPrices;

                $party = array();
                $party['adults'] = $adults;
                $childrenArr = [];
                for ($x=0; $x < intval($children) ; $x++) {
                   array_push($childrenArr, '0');
                }
                $party['children'] = $childrenArr;
                $room['party'] = $party;
                array_push($rooms, $room);
              }
            }
          }


      }
      $checkIn = explode('/', $checkInHotel);
      $checkOut = explode('/', $checkOutHotel);

      $query = array(
        'checkin_date'  => $checkIn[2].'-'.$checkIn[0].'-'.$checkIn[1],
        'checkout_date' => $checkOut[2].'-'.$checkOut[0].'-'.$checkOut[1],
        'customer'      => array(
                              'first_name'    => $this->input->post('firstname'),
                              'last_name'     => $this->input->post('lastname'),
                              'phone_number'  => $this->input->post('phone'),
                              'email'         => $this->input->post('email'),
                              'country'       => $this->input->post('state_booking'),
                           ),
        'rooms'            => $rooms,
        'special_requests' => '( Prenotazione generata da: VisitProcida.it )',
        'payment_method'   => array(
                                    'card_type'        => $this->input->post('cardType'),
                                    'card_number'      => $this->input->post('card_number'),
                                    'cardholder_name'  => $this->input->post('name_card'),
                                    'expiration_month' => $this->input->post('expire_month'),
                                    'expiration_year'  => '20'.$this->input->post('expire_year'),
                                    'cvv'              => $this->input->post('ccv'),
                                  ),
        'final_price_at_checkout' => array('amount' => $hotelPrices, 'currency' => 'EUR')
     );

      curl_setopt($ch, CURLOPT_URL, "https://www.hbb.bz/api_portal/booking_submit");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $server_output = curl_exec ($ch);

      curl_close ($ch);
      
      //echo "<pre>"; print_r($server_output); die;

      $server_output = json_decode($server_output);
//      if($server_output->status){
//         $success['hotel'] = true;
//      } else {
//        $success['hotel'] = false;
//      }

		if($server_output->status == "Failure"){
		   $success['hotel'] = false;
		   
		   $problems = $server_output->problems;
		   
		   if(count($problems) > 0) {
		   		$success['message'] = $server_output->problems[0]->explanation;
		   		$this->data['error'] = $server_output->problems[0]->explanation;
		   }
		   
		} else if($server_output->status == "Success"){
		  $success['hotel'] = true;
		}
    }
    if ($tourPayment > 0  ) {

      if($this->input->post('suggest_tour') == 1) {
        //c'è un tour suggerito
        $tourData = $this->input->post('date_tour');
        $tourID = $this->input->post('tour_suggested_id');
        $tourInfo = $this->Procida_model->getTour($tourID);
        $tourHour =  'To be confirmed';

      }
      else {
        for ($x=0; $x < count($items); $x++) {
          # code...
          if($items[$x]->serviceType == 'tour'){
           $tourData = $items[$x]->checkIn;
           $tourHour =  $items[$x]->checkOut;
           $tourInfo = $items[$x]->info;
           $checkInHotel = $tourData;
           $checkOutHotel = '';
         }
        }
      }
    }

    if($transferPayment > 0) {
      //c'è un trasnfer suggerito
      if($this->input->post('suggest_transfer') == 1){
        $transferForwardData = date('d-m-Y', strtotime($checkInHotel));
        $transferReturnData =  $checkOutHotel != '' ? date('d-m-Y', strtotime($checkOutHotel)) : 'To be confirmed';
        $transferDestination = $this->input->post('destination_transfer');
        $transferDestination = explode('-',$transferDestination);
        $pickupAddressForward = $transferDestination[0];
        $dropoffAddressForward =  $transferDestination[1];
        $pickupAddressReturn =  $transferDestination[1];
        $dropoffAddressReturn =  $transferDestination[0];
      }
      else {
        for ($y=0; $y < count($items); $y++) {
          # code...
          if($items[$y]->serviceType == 'transfer'){
            $transferTransferType = $items[$y]->extraType;
            $transferForwardData = $items[$y]->checkIn;
            $transferReturnData =  $items[$y]->checkOut;
            $transferInfants =  $items[$y]->infants;
            $transferPets =  $items[$y]->pets;
            $transferFlightTrainNo =  $items[$y]->flight_train_no;
            $transferReturnFlightTrainNo =  $items[$y]->return_flight_train_no;
            $transferSpecialRequesto =  $items[$y]->special_request;
            $pickupAddressForward = $items[$y]->pickupAddress;
            $dropoffAddressForward = $items[$y]->dropoffAddress;
            $pickupAddressReturn = $items[$y]->pickupReturnAddress;
            $dropoffAddressReturn = $items[$y]->dropoffReturnAddress;
          }
        }
      }

    }

    $this->data['transfer'] = false;
    $this->data['tour'] = false;

    if($transferPayment > 0 || $tourPayment > 0){
       \Stripe\Stripe::setApiKey("sk_live_oeEIs39eGR7biCPDxlaWhThC003cGWiEKc"); // easyisland Live Key
      //\Stripe\Stripe::setApiKey("sk_test_Vsf9AI6NfncJdMLL2MbAH4o100IiJrLOUF"); // easyisland Test Key
      //\Stripe\Stripe::setApiKey("sk_test_i6o2iqNz4f11ZJTNl58KeTHp"); // VPN Test Key
      $amount = 0;
      if($transferPayment > 0){
          $this->data['transfer'] = true;
          $amount = ($amount + intVal($transferPayment));

          $this->data['bookingInfo']['TransferType'] = $transferTransferType;
          $this->data['bookingInfo']['checkIn'] = $transferForwardData;
          $this->data['bookingInfo']['checkOut'] = $transferReturnData;
          $this->data['bookingInfo']['pickupAddress'] = $pickupAddressForward;
          $this->data['bookingInfo']['dropoffAddress'] = $dropoffAddressForward;
          $this->data['bookingInfo']['people'] = $peopleTranfer;
          $this->data['bookingInfo']['infants'] = $transferInfants;
          $this->data['bookingInfo']['pets'] = $transferPets;
          $this->data['bookingInfo']['flight_train_no'] = $transferFlightTrainNo;
          $this->data['bookingInfo']['return_flight_train_no'] = $transferReturnFlightTrainNo;
          $this->data['bookingInfo']['special_request'] = $transferSpecialRequesto;
          $this->data['bookingInfo']['pickupReturnAddress'] = $pickupAddressReturn;
          $this->data['bookingInfo']['dropoffReturnAddress'] = $dropoffAddressReturn;
          $this->data['bookingInfo']['transferPayment'] = $transferPayment;
      }

      if($tourPayment > 0){
          $this->data['tour'] = true;
          $amount = ($amount + intVal($tourPayment));

          $this->data['bookingInfo']['tour']['name'] = $tourInfo->itTitle;
          $this->data['bookingInfo']['tour']['date'] = $tourData;
          $this->data['bookingInfo']['tour']['hour'] = $tourHour;
          $duration = $tourInfo->durationDay > 0 ? $tourInfo->durationDay.' d' : ($tourInfo->durationHours > 0 ?  $tourInfo->durationHours.' h' : $tourInfo->durationMin.' m' );
          $this->data['bookingInfo']['tour']['duration'] = $duration;
          $this->data['bookingInfo']['tour']['adults'] = $touradults;
          $this->data['bookingInfo']['tour']['children'] = $tourchildren;
          $this->data['bookingInfo']['tour']['payment'] = $tourPayment;
      }

      $amount = $amount * 100;

      try {
      	$token = $this->input->post('stripeToken');

  	    if(empty($token)){
  	      throw new Exception("The Stripe Token was not generated correctly");
		    }

        $description = 'Booking on VisitProcida - '.$this->data['bookingInfo']['firstname'].' '.$this->data['bookingInfo']['lastname'];

        $charge =\Stripe\Charge::create(
                  array(
                    "amount" => $amount,
                    "currency" => "eur",
                    "card" => $this->input->post('stripeToken'),
                    "description" => $description,
                    "receipt_email" =>   $this->data['bookingInfo']['email']

                  )
                );

        if($this->data['transfer'] == true){
          $success['transfer'] = true;
        }
        if($this->data['tour'] == true){
          $success['tour'] = true;
        }

  	  }
  	  catch (Exception $e) {
  	    $error = $e->getMessage();
  	    $this->data['error'] = $error.' - Please contact us';
        $success = false;
  	  }
    }

    $data = $this->data;
    $data['success'] = $success;

    if($success != false) {
      $mail = $this->sendMail($data['bookingInfo'], $success, $data);
      if($mail){
        $this->Cart_model->delete($cartCookie);
      } else {
        $data['error'] = 'Problem sending the emails, please print the summary and contact us';
      }
    }


    $items = [];
    $hotelCart = [];
    $tranferCart = [];
    $tours = [];

    $data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );
    $this->load->view('frontend/includes/header', $data);
    $this->load->view('frontend/content/cart_end', $data);
    $this->load->view('frontend/includes/footer', $data);

  }

  private function sendMail($bookingInfo, $success, $data){

    $config['protocol'] = 'sendmail';
    $config['charset'] = 'iso-8859-1';
    $config['wordwrap'] = TRUE;
    
    $errormessage = ''; 
    
    if($success['hotel'] == false) {
    	if (isset($success['message'])) {
    		if($success['message'] != ''){
    			// ERROR HOTEL
    			$errormessage = $success['message'];
    		}
    	}
    }
    
    $user_html_message = $this->composeMessageForUser($bookingInfo, $data, $errormessage);
    $userMail = $this->sendMailToUser($config, $bookingInfo, $user_html_message);

    $TransferMail = true;
    $transferMailTosend = false;

    $tourMail = true;
    $tourMailTosend = false;

    if($data['transfer'] && $success['transfer'] == true) {
      $transferMailTosend = true;
      $html_message = $this->composeMessage($bookingInfo);
      $TransferMail = $this->sendMailTransfer($config, $bookingInfo, $html_message);
    }
    if($data['tour'] && $success['tour'] == true){
      $tourMailTosend = true;
      $tour_html_message = $this->composeMessageTour($bookingInfo);
      $tourMail = $this->sendMailTour($config, $bookingInfo, $tour_html_message);
    }

    if($transferMailTosend == true && $TransferMail == false){
      return false;
    }
    if($tourMailTosend == true && $tourMail == false) {
      return false;
    }
    if($userMail == false){
      return false;
    }
    return true;
  }

  private function sendMailTransfer($config, $bookingInfo, $html_message){
    $this->load->library("email", $config);
    $this->load->helper("email");
    $this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('gioiapartments@gmail.com', 'VisitProcida');
		$this->email->reply_to($bookingInfo['email'], $bookingInfo['firstname'].' '.$bookingInfo['lastname']);
		//$this->email->to('sunandsea.ischia@gmail.com');
		$this->email->to('gioiapartments@gmail.com');
		$this->email->bcc("vpn.testings@gmail.com");
		$this->email->subject('Transfer Booking | Visit Procida');
        $this->email->message($html_message);
		if ($this->email->send()) {
			return true;
		} else {
			return false;
		}
  }

  private function sendMailTour($config, $bookingInfo, $html_message){
    $this->load->library("email", $config);
    $this->load->helper("email");
    $this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('gioiapartments@gmail.com', 'VisitProcida');
		$this->email->reply_to($bookingInfo['email'], $bookingInfo['firstname'].' '.$bookingInfo['lastname']);
		$this->email->to("gioiapartments@gmail.com");
		$this->email->bcc("vpn.testings@gmail.com");
		$this->email->subject('Tour Booking | Visit Procida');
        $this->email->message($html_message);
		if ($this->email->send()) {
			return true;
		} else {
			return false;
		}
  }

  private function sendMailToUser($config, $bookingInfo, $user_html_message){
    $this->load->library("email", $config);
    $this->load->helper("email");
    $this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('gioiapartments@gmail.com', 'VisitProcida');
		$this->email->reply_to($bookingInfo['email'], $bookingInfo['firstname'].' '.$bookingInfo['lastname']);
		$this->email->to($bookingInfo['email']);
		$this->email->bcc("gioiapartments@gmail.com","vpn.testings@gmail.com");
		$this->email->subject('Your Booking | Visit Procida');
        $this->email->message($user_html_message);
		if ($this->email->send()) {
			return true;
		} else {
			return false;
		}
  }

  private function composeMessageForUser($bookingInfo, $data, $errormessage){
    $fullName = $bookingInfo['firstname'].' '.$bookingInfo['lastname'];
    $html = "Hello " . $fullName. ", <br><br>";
    $html .= "Thank you for your reservation on <a href='http://visitprocida.com'>www.visitprocida.com</a> ";
    $html .= "<br>For any information please do not hesitate to contact us at the number <a href='tel:+39 081 896 0454'>+39 081 8960454</a>.<br>";
    $html .= "Here a resume of your reservation:<br><br>";

    $html .= "<table cellspacing='0' cellpadding='10' border='1' width='400'>";
    $html .= "<tr><td width='120'><b>Name</b></td><td width='240'>" . $bookingInfo['firstname'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Surname</b></td><td width='240'>" . $bookingInfo['lastname'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Train or Flight code</b></td><td width='240'>". $bookingInfo['trainOrFlight'] . "</td></tr></table>";

    if($data['hotel'] == true && $errormessage == '') {
      $html .= " <br><br>";
      $html .= "<table cellspacing='0' cellpadding='10' border='1' width='400'>";
      $html .= "<tr><td width='120'><b>HOTEL</b></td><td width='240'></td></tr>";
      if(isset($bookingInfo['rooms'])){
        $rooms = $bookingInfo['rooms'];
        foreach ($rooms as $room) {
         if(isset($room['hotelName'])){
           $html .= "<tr><td width='120'><b>Hotel Name</b></td><td width='240'>" . $room['hotelName'] . "</td></tr>";
         }
          if(isset($room['roomName'])){
            $html .= "<tr><td width='120'><b>Room name</b></td><td width='240'>" . $room['roomName'] . "</td></tr>";
          }
          if(isset($room['adults'])){
            $html .= "<tr><td width='120'><b>Adults</b></td><td width='240'>" . $room['adults'] . "</td></tr>";
          }
          if(isset($room['children'])){
            $html .= "<tr><td width='120'><b>Children</b></td><td width='240'>" . $room['children'] . "</td></tr>";
          }
          if(isset($room['nights'])){
            $html .= "<tr><td width='120'><b>Nights</b></td><td width='240'>" . $room['nights'] . "</td></tr>";
          }
          if(isset($room['checkIn'])){
            $html .= "<tr><td width='120'><b>Check In</b></td><td width='240'>" . $room['checkIn'] . "</td></tr>";
          }
          if(isset($room['checkOut'])){
            $html .= "<tr><td width='120'><b>Check Out</b></td><td width='240'>" . $room['checkOut'] . "</td></tr>";
          }
        }
      }
      $html .= "<tr><td width='120'><b>Total</b></td><td width='240'> Eur. " . $room['price'] . "</td></tr></table>";
    }
    else if($errormessage != ''){
    	$html .= " <br><br>";
    	$html .= "<b>There is an error with hotel reservation </b><br><br>";
    	$html .= $errormessage;
    	$html .= "<br>Please to contact us at <b>+390818960454</b> for support.<br>";
    	
    }

    if($data['transfer'] == true) {
      $html .= " <br><br>";
      $html .= "<table cellspacing='0' cellpadding='10' border='1' width='400'>";
      $html .= "<tr><td width='120'><b>TRANSFER</b></td><td width='240'></td></tr>";
      if(isset($bookingInfo['TransferType']) && $bookingInfo['TransferType'] == 'car_transfer'){
      $html .= "<tr><td width='120'><b>Transfer Type</b></td><td width='240'>Car Transfer</td></tr>";
      }
      if(isset($bookingInfo['TransferType']) && $bookingInfo['TransferType'] == 'yacht_transfer'){
        $html .= "<tr><td width='120'><b>Transfer Type</b></td><td width='240'>Yacht Transfer</td></tr>";
      }
      if(isset($bookingInfo['people'])){
        $html .= "<tr><td width='120'><b>People</b></td><td width='240'>" . $bookingInfo['people'] . "</td></tr>";
      }
      if(isset($bookingInfo['infants'])){
        $html .= "<tr><td width='120'><b>Infants</b></td><td width='240'>" . $bookingInfo['infants'] . "</td></tr>";
      }
      if(isset($bookingInfo['pets'])){
        $html .= "<tr><td width='120'><b>Pets</b></td><td width='240'>" . $bookingInfo['pets'] . "</td></tr>";
      }
      if(isset($bookingInfo['checkIn'])){
        $html .= "<tr><td width='120'><b>Date</b></td><td width='240'>" . $bookingInfo['checkIn'] . "</td></tr>";
      }
      if(isset($bookingInfo['pickupAddress'])){
        $html .= "<tr><td width='120'><b>Pickup Address</b></td><td width='240'>" . $bookingInfo['pickupAddress'] . "</td></tr>";
      }
      if(isset($bookingInfo['dropoffAddress'])){
        $html .= "<tr><td width='120'><b>Drop Off Address</b></td><td width='240'>" . $bookingInfo['dropoffAddress'] . "</td></tr>";
      }
      if(isset($bookingInfo['flight_train_no'])){
        $html .= "<tr><td width='120'><b>Flight/Train No</b></td><td width='240'>" . $bookingInfo['flight_train_no'] . "</td></tr>";
      }
      if(isset($bookingInfo['checkOut']) && $bookingInfo['checkOut'] != '' && $bookingInfo['checkOut'] != ' '){
        $html .= "<tr><td width='120'><b>Return Date</b></td><td width='240'>" . $bookingInfo['checkOut'] . "</td></tr>";
      }
      if(isset($bookingInfo['pickupReturnAddress'])){
        $html .= "<tr><td width='120'><b>Pickup Return Address</b></td><td width='240'>" . $bookingInfo['pickupReturnAddress'] . "</td></tr>";
      }
      if(isset($bookingInfo['dropoffReturnAddress'])){
        $html .= "<tr><td width='120'><b>Drop Off Return Address</b></td><td width='240'>" . $bookingInfo['dropoffReturnAddress'] . "</td></tr>";
      }
      if(isset($bookingInfo['return_flight_train_no'])){
        $html .= "<tr><td width='120'><b>Return Flight/Train No</b></td><td width='240'>" . $bookingInfo['return_flight_train_no'] . "</td></tr>";
      }
      $html .= "<tr><td width='120'><b>Total</b></td><td width='240'> Eur. " . $bookingInfo['transferPayment'] . "</td></tr></table>";
      if(isset($bookingInfo['special_request'])){
        $html .= "<p><b>Special Request : </b>" . $bookingInfo['special_request'] . "</p>";
      }
    }

    if($data['tour'] == true) {
      $html .= " <br><br>";
      $html .= "<table cellspacing='0' cellpadding='10' border='1' width='400'>";
      $html .= "<tr><td width='120'><b>TOUR</b></td><td width='240'></td></tr>";
      $tour = $bookingInfo['tour'];
      if(isset($tour['name'])){
        $html .= "<tr><td width='120'><b>Tour name</b></td><td width='240'>" . $tour['name'] . "</td></tr>";
      }
      if(isset($tour['adults'])){
        $html .= "<tr><td width='120'><b>Adults</b></td><td width='240'>" . $tour['adults'] . "</td></tr>";
      }
      if(isset($tour['children'])){
        $html .= "<tr><td width='120'><b>Children</b></td><td width='240'>" . $tour['children'] . "</td></tr>";
      }
      if(isset($tour['date'])){
        $html .= "<tr><td width='120'><b>Date</b></td><td width='240'>" . $tour['date'] . "</td></tr>";
      }
      if(isset($tour['hour'])){
        $html .= "<tr><td width='120'><b>Hour</b></td><td width='240'>" . $tour['hour'] . "</td></tr>";
      }
      if(isset($tour['duration'])){
        $html .= "<tr><td width='120'><b>Duration</b></td><td width='240'>" . $tour['duration'] . "</td></tr>";
      }
      $html .= "<tr><td width='120'><b>Total</b></td><td width='240'> Eur. " . $tour['payment'] . "</td></tr></table>";
    }
    $html .= " <br><br>";
    $html .= $this->signatureEmail();

    $user_html_message = $html;
    return $user_html_message;
  }

  private function composeMessage($bookingInfo){ //TRANSFER
    $fullName = $bookingInfo['firstname'].' '.$bookingInfo['lastname'];
    $html = "Hello You have a new reservation from <b>" . $fullName. "</b> on <a href='https://visitprocida.com'>www.visitprocida.com</a>. <br><br>";
    $html .= "Here a resume of reservation for one <b>Tranfer</b>:<br><br>";
    $html .= "<table cellspacing='0' cellpadding='10' border='1' width='400'>";
    $html .= "<tr><td width='120'><b>Name</b></td><td width='240'>" . $bookingInfo['firstname'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Surname</b></td><td width='240'>" . $bookingInfo['lastname'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Email</b></td><td width='240'>" . $bookingInfo['email'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Phone</b></td><td width='240'>" . $bookingInfo['phone'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Address</b></td><td width='240'>" . $bookingInfo['street_1'] ." " . $bookingInfo['street_2'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>City</b></td><td width='240'>" . $bookingInfo['city'] ."</td></tr>";
    $html .= "<tr><td width='120'><b>Postal Code</b></td><td width='240'>" . $bookingInfo['p_code'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Country</b></td><td width='240'>" . $bookingInfo['state_booking'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Train or Flight code</b></td><td width='240'>". $bookingInfo['trainOrFlight'] . "</td></tr></table>";

    $html .= " <br><br>";
    $html .= "<table cellspacing='0' cellpadding='10' border='1' width='400'>";
    $html .= "<tr><td width='120'><b>TRANSFER</b></td><td width='240'></td></tr>";
    if(isset($bookingInfo['TransferType']) && $bookingInfo['TransferType'] == 'car_transfer'){
      $html .= "<tr><td width='120'><b>Transfer Type</b></td><td width='240'>Car Transfer</td></tr>";
    }
    if(isset($bookingInfo['TransferType']) && $bookingInfo['TransferType'] == 'yacht_transfer'){
      $html .= "<tr><td width='120'><b>Transfer Type</b></td><td width='240'>Yacht Transfer</td></tr>";
    }
    if(isset($bookingInfo['people'])){
      $html .= "<tr><td width='120'><b>People</b></td><td width='240'>" . $bookingInfo['people'] . "</td></tr>";
    }
    if(isset($bookingInfo['infants'])){
      $html .= "<tr><td width='120'><b>Infants</b></td><td width='240'>" . $bookingInfo['infants'] . "</td></tr>";
    }
    if(isset($bookingInfo['pets'])){
      $html .= "<tr><td width='120'><b>Pets</b></td><td width='240'>" . $bookingInfo['pets'] . "</td></tr>";
    }
    if(isset($bookingInfo['checkIn'])){
      $html .= "<tr><td width='120'><b>Date</b></td><td width='240'>" . $bookingInfo['checkIn'] . "</td></tr>";
    }
    if(isset($bookingInfo['pickupAddress'])){
      $html .= "<tr><td width='120'><b>Pickup Address</b></td><td width='240'>" . $bookingInfo['pickupAddress'] . "</td></tr>";
    }
    if(isset($bookingInfo['dropoffAddress'])){
      $html .= "<tr><td width='120'><b>Drop Off Address</b></td><td width='240'>" . $bookingInfo['dropoffAddress'] . "</td></tr>";
    }
    if(isset($bookingInfo['flight_train_no'])){
        $html .= "<tr><td width='120'><b>Flight/Train No</b></td><td width='240'>" . $bookingInfo['flight_train_no'] . "</td></tr>";
      }
    if(isset($bookingInfo['checkOut']) && $bookingInfo['checkOut'] != '' && $bookingInfo['checkOut'] != ' '){
      $html .= "<tr><td width='120'><b>Return Date</b></td><td width='240'>" . $bookingInfo['checkOut'] . "</td></tr>";
    }
    if(isset($bookingInfo['pickupReturnAddress'])){
      $html .= "<tr><td width='120'><b>Pickup Return Address</b></td><td width='240'>" . $bookingInfo['pickupReturnAddress'] . "</td></tr>";
    }
    if(isset($bookingInfo['dropoffReturnAddress'])){
      $html .= "<tr><td width='120'><b>Drop Off Return Address</b></td><td width='240'>" . $bookingInfo['dropoffReturnAddress'] . "</td></tr>";
    }
    if(isset($bookingInfo['return_flight_train_no'])){
        $html .= "<tr><td width='120'><b>Return Flight/Train No</b></td><td width='240'>" . $bookingInfo['return_flight_train_no'] . "</td></tr>";
      }
    $html .= "<tr><td width='120'><b>Total</b></td><td width='240'> Eur. " . $bookingInfo['transferPayment'] . "</td></tr></table>";

    $html .= " </table><br>";

    if(isset($bookingInfo['special_request'])){
      $html .= "<p><b>Special Request : </b>" . $bookingInfo['special_request'] . "</p>";
    }
    $html .= $this->signatureEmail();

    return $html;
  }


  private function composeMessageTour($bookingInfo){
    $fullName = $bookingInfo['firstname'].' '.$bookingInfo['lastname'];
    $html = "Hello You have a new reservation from <b>" . $fullName. "</b> on <a href='https://visitprocida.com'>www.visitprocida.com</a>. <br><br>";
    $html .= "Here a resume of reservation for one <b>Tour</b>:<br><br>";
    $html .= "<table cellspacing='0' cellpadding='10' border='1' width='400'>";
    $html .= "<tr><td width='120'><b>Name</b></td><td width='240'>" . $bookingInfo['firstname'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Surname</b></td><td width='240'>" . $bookingInfo['lastname'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Email</b></td><td width='240'>" . $bookingInfo['email'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Phone</b></td><td width='240'>" . $bookingInfo['phone'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Address</b></td><td width='240'>" . $bookingInfo['street_1'] ." " . $bookingInfo['street_2'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>City</b></td><td width='240'>" . $bookingInfo['city'] ."</td></tr>";
    $html .= "<tr><td width='120'><b>Postal Code</b></td><td width='240'>" . $bookingInfo['p_code'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Country</b></td><td width='240'>" . $bookingInfo['state_booking'] . "</td></tr>";
    $html .= "<tr><td width='120'><b>Train or Flight code</b></td><td width='240'>". $bookingInfo['trainOrFlight'] . "</td></tr></table>";

    $html .= " <br><br>";
    $html .= "<table cellspacing='0' cellpadding='10' border='1' width='400'>";
    $html .= "<tr><td width='120'><b>TOUR</b></td><td width='240'></td></tr>";
    $tour = $bookingInfo['tour'];
    if(isset($tour['name'])){
      $html .= "<tr><td width='120'><b>Tour name</b></td><td width='240'>" . $tour['name'] . "</td></tr>";
    }
    if(isset($tour['adults'])){
      $html .= "<tr><td width='120'><b>Adults</b></td><td width='240'>" . $tour['adults'] . "</td></tr>";
    }
    if(isset($tour['children'])){
      $html .= "<tr><td width='120'><b>Children</b></td><td width='240'>" . $tour['children'] . "</td></tr>";
    }
    if(isset($tour['date'])){
      $html .= "<tr><td width='120'><b>Date</b></td><td width='240'>" . $tour['date'] . "</td></tr>";
    }
    if(isset($tour['hour'])){
      $html .= "<tr><td width='120'><b>Hour</b></td><td width='240'>" . $tour['hour'] . "</td></tr>";
    }
    if(isset($tour['duration'])){
      $html .= "<tr><td width='120'><b>Duration</b></td><td width='240'>" . $tour['duration'] . "</td></tr>";
    }
    $html .= "<tr><td width='120'><b>Total</b></td><td width='240'> Eur. " . $tour['payment'] . "</td></tr></table>";

	$html .= " <br>";
	$html .= $this->signatureEmail();
    return $html;
  }

  private function signatureEmail() {
  	$html = '<table width=351 cellspacing=0 cellpadding=0 border=0> <tr> <td style="vertical-align: top; text-align:left;color:#000000;font-size:12px;font-family:helvetica, arial"> <span style="margin-right:5px;font-size:15px;font-family:helvetica, arial;color:#000000">VisitProcida</span> <span style="margin-right:5px;font-size:12px;font-family:helvetica, arial;color:#000000">Joy Holidays Group</span><br><span style="margin-right:5px;font-size:12px;font-family:helvetica, arial;color:#000000">P.IVA 07207821211</span> <br> <span style="font:12px helvetica, arial">Email:&nbsp;<a href=mailto:visitprocida@gmail.com style=color:#3388cc;text-decoration:none>visitprocida@gmail.com</a></span> <span style="font:12px helvetica, arial">Phone:&nbsp;<a href="tel:+39 081 896 0454" style=color:#3388cc;text-decoration:none>+390818960454</a></span> <br><br> Via Flavio Gioia, 13, Procida (NA), Italy <br><br> <table cellpadding=0 cellpadding=0 border=0><tr><td style=padding-right:5px><a href="https://twitter.com/visitprocida" style="display: inline-block"><img width=40 height=40 src=https://s1g.s3.amazonaws.com/3949237f892004c237021ac9e3182b1d.png alt=Twitter style=border:none></a></td><td style=padding-right:5px><a href=https://instagram.com/visitprocida style="display: inline-block"><img width=40 height=40 src=https://s1g.s3.amazonaws.com/4c616177ca37bea6338e6964ca830de5.png alt=Instagram style=border:none></a></td><td style=padding-right:5px><a href="https://facebook.com/VisitProcida" style="display: inline-block"><img width=40 height=40 src=https://s1g.s3.amazonaws.com/23f7b48395f8c4e25e64a2c22e9ae190.png alt=Facebook style=border:none></a></td></tr></table><a href=https://www.visitprocida.com style=text-decoration:none;color:#3388cc>www.visitprocida.com</a> </td> </tr> </table> ';
  	return $html;
  }

}
