<?php

if (!defined("BASEPATH")) {
    exit("No direct script access is allwoed");
}

class Transfer extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')) {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Please login to view this page',
                'type' => 'danger', 'model' => 'login'));
            redirect('customer/login');
        }
        $this->load->helper('language');
        $this->lang->load('customerpanel/Common');
        $this->load->model('customer/Customer_model');
        $this->Customer_model->load($this->session->userdata('id'));
    }

    public function index() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $data['userinfo'] = $this->userinfo;
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#events-dtable').dataTable();

                                                        });")
        );
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['cpage'] = 'transfer';
        $data['usertype'] = $this->session->userdata('usertype');
        $data['pick_up_points'] = json_decode (PICK_UP_POINTS,true);
        $data['transfers_type'] = json_decode (TRANSFERS_TYPE,true);


        $this->load->model('Transfer_model');
        $transferData = $this->Transfer_model->getAllTransferOfCustomer($this->session->userdata('id'));

        foreach ($transferData as $transferKey => $transferValue) {
            $pickupPointsString = '';
            $pickupPoints = explode(',', $transferValue->pickup);
            foreach ($pickupPoints as $pickupKey => $pickupVal) {
                $pickupPointsString .= $data['pick_up_points'][$pickupVal].',';
            }
            $transferValue->trasfer_srting =  $pickupPointsString;
        }
        // print_r( $transferData); exit;
        $data['transfer'] =  $transferData;

        $this->load->view('customer/includes/header', $data);
        $this->load->view('customer/includes/sidebar', $data);
        $this->load->view('customer/contents/transfer/index', $data);
        $this->load->view('customer/includes/footer', $data);
    }

    public function add() {

        // echo '<pre>'; print_r( $_POST ); exit;
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Transfer_model');
        $page = 'customer/contents/transfer/edit';
        $this->load->model('admin/Location_model');
        $this->load->model('Hotel_model');
        $this->load->model('customer/Customer_model');
        $this->load->model('Orders_model');
        $this->load->model('Categories');
        $data['locations'] = $this->Location_model->getLocations();
        $this->Customer_model->load($this->session->userdata('id'));
        $orders = $this->Customer_model->getOrders();
        // $businessPages = array();
        // foreach ($orders as $key => $order) {
        //   # code...
        //   $category = $this->Categories->get($order->category_id);
        //   $table = $category->table;
        //   $businessPage = $this->Customer_model->getBusinessPage($table, $order->id);
        //   if($businessPage && count($businessPage) > 0){
        //     array_push($businessPages, $businessPage);
        //   }
        // }

        $businessPages = $this->Orders_model->getList();
       

        
        $data['business_pages'] = $businessPages;
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/datepicker/bootstrap-datepicker.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/timepicker/bootstrap-timepicker.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-tagsinput.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-multiselect.css')
        );
        // $hotels = $this->Hotel_model->getActiveHotels();
        // $apartments = $this->Hotel_model->getActiveApartmentsforHotalList();
        // $bed_and_breakfast = $this->Hotel_model->getActivBedAndBreakfastforHotalList();
        // foreach ($apartments as $apartmentsKey => $apartmentsValue) {
        //     $hotels[] = $apartmentsValue;
        // }
        //  foreach ($bed_and_breakfast as $bed_and_breakfastKey => $bed_and_breakfastValue) {
        //     $hotels[] = $bed_and_breakfastValue;
        // }
        // // print_r($hotels); exit;

        //  $data['hotels'] = $hotels;


        $data['pick_up_points'] = json_decode (PICK_UP_POINTS);
        $data['cars_type'] = json_decode (CARS_TYPE);
        $data['boats_type'] = json_decode (BOATS_TYPE);
        $data['transfers_type'] = json_decode (TRANSFERS_TYPE,true);
        $data['usertype'] = $this->session->userdata('usertype');
        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/datepicker/bootstrap-datepicker.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/timepicker/bootstrap-timepicker.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-multiselect.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-tagsinput.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/utils_dashboard.js'),
            Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                                     $('.datepicker').datepicker();
                                                                      $('.timepicker').timepicker({showMeridian: false, defaultTime: false, showInputs: false});
                                                        });")
        );
        
        $config = array(
            array('field' => 'id', 'label' => 'Event id', 'rules' => 'required|numeric'),

            array('field' => 'itTitle', 'label' => 'Name [it]', 'rules' => 'required|max_length[50]'),
            array('field' => 'enTitle', 'label' => 'Name [en]', 'rules' => 'max_length[50]'),

            array('field' => 'itDescription', 'label' => 'Description [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enDescription', 'label' => 'Description [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itServicesIncluded', 'label' => 'Included Services [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enServicesIncluded', 'label' => 'Not included services [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itServicesNotIncluded', 'label' => 'Servizi non Inclusi [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enServicesNotIncluded', 'label' => 'Servizi non Inclusi [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itPolicy', 'label' => 'reservation and cancellation policy [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'itPolicy', 'label' => 'reservation and cancellation policy [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'dateStart', 'label' => 'End Date', 'rules' => 'required|callback_date_valid'),
            array('field' => 'dateEnd', 'label' => 'End Time', 'rules' => 'required|callback_date_valid'),

            array('field' => 'days', 'label' => 'Giorni della settimana', 'rules' => 'required'),

            array('field' => 'duration','label' => 'Durata','rules' => 'required'),

            array('field' => 'pickup', 'label' => 'punto di raccolta', 'rules' => 'required'),

            array('field' => 'daysAvailability', 'label' => 'Giorni della settimana', 'rules' => 'required'),

            // array('field' => 'dropoff', 'label' => 'Luogo di scarico', 'rules' => 'required'),

            array('field' => 'maxPassenger', 'label' => 'massimo passeggero', 'rules' => 'required|numeric'),

            array('field' => 'startPrice', 'label' => 'Prezzo di partenza', 'rules' => 'required|numeric'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE && $page != '404') {
            if ($this->input->post('id') != '0') {
                $this->Transfer_model->id = $this->input->post('id');
            }
            $this->Transfer_model->enTitle = $this->input->post('enTitle') ? $this->input->post('enTitle') : $this->input->post('itTitle');
            $this->Transfer_model->itTitle = $this->input->post('itTitle') ?  $this->input->post('itTitle') : $this->input->post('enTitle');
            $this->Transfer_model->enDescription = $this->input->post('enDescription') ? $this->input->post('enDescription') : $this->input->post('itDescription');
            $this->Transfer_model->itDescription = $this->input->post('itDescription');
            $this->Transfer_model->enServicesIncluded = $this->input->post('enServicesIncluded') ? $this->input->post('enServicesIncluded') : $this->input->post('itServicesIncluded');
            $this->Transfer_model->itServicesIncluded = $this->input->post('itServicesIncluded');
            $this->Transfer_model->enServicesNotIncluded = $this->input->post('enServicesNotIncluded') ? $this->input->post('enServicesNotIncluded') : $this->input->post('itServicesNotIncluded');
            $this->Transfer_model->itServicesNotIncluded = $this->input->post('itServicesNotIncluded');
            $this->Transfer_model->enPolicy = $this->input->post('enPolicy') ? $this->input->post('enPolicy') : $this->input->post('itPolicy');
            $this->Transfer_model->itPolicy = $this->input->post('itPolicy');
            $sd = explode("/", $this->input->post('dateStart'));
            $this->Transfer_model->dateStart = $sd[2] . '-' . $sd[0] . '-' . $sd[1];
            $ed = explode("/", $this->input->post('dateEnd'));
            $this->Transfer_model->dateEnd = $ed[2] . '-' . $ed[0] . '-' . $ed[1];

            $this->Transfer_model->startPrice = $this->input->post('startPrice');
            // $this->Transfer_model->car_type = $this->input->post('car_type');
            // $this->Transfer_model->boat_type = $this->input->post('boat_type');
            $this->Transfer_model->trip_type = $this->input->post('trip_type');
            // $this->Transfer_model->transfer_type = $this->input->post('transfer_type');
            $this->Transfer_model->categories = $this->input->post('categories');
            $this->Transfer_model->daysAvailability = $this->input->post('daysAvailability');
            $this->Transfer_model->pickup = $this->input->post('pickup');
            // $this->Transfer_model->dropoff = $this->input->post('dropoff');
            $this->Transfer_model->maxPassenger = $this->input->post('maxPassenger');
            $this->Transfer_model->duration = $this->input->post('duration');
            $this->Transfer_model->thumbnail = $this->Transfer_model->thumbnail ? $this->Transfer_model->thumbnail : ' ';
            $this->Transfer_model->cover = $this->Transfer_model->cover ? $this->Transfer_model->cover : ' ';
            $this->Transfer_model->customerId = $this->session->userdata('id');
            $order_business = $this->input->post('order_business');
            $order_business = explode(',',$order_business);
            $this->Transfer_model->businessPageId = $order_business[0];
            $this->Transfer_model->orderId = $order_business[1];

            $id = $this->Transfer_model->update();
            $zone_name = $this->input->post('zone_name');
            foreach ($zone_name as $key => $value) 
            {
                $zoneData = array(
                    'transfer_id' => $id,
                    'zone_name' => $value,
                    'sjd_price' => $this->input->post('sjd_price')[$key],
                    'csl_price' => $this->input->post('csl_price')[$key],
                );
                $this->db->insert('zone', $zoneData);
            }
            
            $this->session->set_flashdata(Array('msg' => 'Hai aggiunto con successo un nuovo trasferimento',
                'type' => 'success'));
            redirect('customer/transfer/edit/' . $id);
        } else {
            $data['msg'] = $this->session->flashdata('msg');
            $data['type'] = $this->session->flashdata('type');
            $data['cpage'] = 'transfer';
            $data['sub_cpage'] = 'new-transfer';

            $this->load->view('customer/includes/header', $data);
            $this->load->view('customer/includes/sidebar', $data);
            $this->load->view($page, $data);
            $this->load->view('customer/includes/footer', $data);
        }
    }

    public function edit($id = 0) {

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Transfer_model');
        $this->Transfer_model->load($id, $this->session->userdata('id'));
        $page = 'customer/contents/transfer/edit';
        $this->load->model('admin/Location_model');
        $this->load->model('customer/Customer_model');
        $this->load->model('Hotel_model');
        $this->load->model('Categories');
        $this->load->model('Orders_model');
        $data['locations'] = $this->Location_model->getLocations();
        $data['zone'] = $this->db->select('transfer.id,zone.transfer_id,zone.zone_name,zone.sjd_price,zone.csl_price')->from('transfer')->where(array('transfer.id' => $id, 'customerId' => $this->Transfer_model->customerId))->join('zone','transfer.id=zone.transfer_id','LEFT OUTER')->get()->result();

        $this->Customer_model->load($this->session->userdata('id'));
        $orders = $this->Customer_model->getOrders();
        // $businessPages = array();
        foreach ($orders as $key => $order) {
          # code...
          $category = $this->Categories->get($order->category_id);
          $table = $category->table;
          // $businessPage = $this->Customer_model->getBusinessPage($table, $order->id);
          // if($businessPage && count($businessPage) > 0){
          //   array_push($businessPages, $businessPage);
          // }


        // $hotels = $this->Hotel_model->getActiveHotels();
        // $apartments = $this->Hotel_model->getActiveApartmentsforHotalList();
        // $bed_and_breakfast = $this->Hotel_model->getActivBedAndBreakfastforHotalList();
        // foreach ($apartments as $apartmentsKey => $apartmentsValue) {
        //     $hotels[] = $apartmentsValue;
        // }
        //  foreach ($bed_and_breakfast as $bed_and_breakfastKey => $bed_and_breakfastValue) {
        //     $hotels[] = $bed_and_breakfastValue;
        // }

        // $data['hotels'] = $hotels;
        $data['transfers_type'] = json_decode (TRANSFERS_TYPE,true);
        $data['cars_type'] = json_decode (CARS_TYPE);
        $data['boats_type'] = json_decode (BOATS_TYPE);
        $data['pick_up_points'] = json_decode (PICK_UP_POINTS);
        }
        $businessPages = $this->Orders_model->getList();
        $data['business_pages'] = $businessPages;
        $data['locations'] = $this->Location_model->getLocations();
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/datepicker/bootstrap-datepicker.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-tagsinput.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/timepicker/bootstrap-timepicker.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-multiselect.css')
        );
        $data['usertype'] = $this->session->userdata('usertype');
        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/datepicker/bootstrap-datepicker.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/timepicker/bootstrap-timepicker.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-multiselect.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-tagsinput.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/utils_dashboard.js'),
            Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                                     $('.datepicker').datepicker();
                                                                      $('.timepicker').timepicker({showMeridian: false, defaultTime: false, showInputs: false});
                                                        });")
        );

        $config = array(
             array('field' => 'id', 'label' => 'Event id', 'rules' => 'required|numeric'),

            array('field' => 'itTitle', 'label' => 'Name [it]', 'rules' => 'required|max_length[50]'),
            array('field' => 'enTitle', 'label' => 'Name [en]', 'rules' => 'max_length[50]'),

            array('field' => 'itDescription', 'label' => 'Description [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enDescription', 'label' => 'Description [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itServicesIncluded', 'label' => 'Included Services [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enServicesIncluded', 'label' => 'Not included services [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itServicesNotIncluded', 'label' => 'Servizi non Inclusi [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enServicesNotIncluded', 'label' => 'Servizi non Inclusi [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itPolicy', 'label' => 'reservation and cancellation policy [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'itPolicy', 'label' => 'reservation and cancellation policy [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'dateStart', 'label' => 'End Date', 'rules' => 'required|callback_date_valid'),
            array('field' => 'dateEnd', 'label' => 'End Time', 'rules' => 'required|callback_date_valid'),

            array('field' => 'days', 'label' => 'Giorni della settimana', 'rules' => 'required'),

            array('field' => 'duration','label' => 'Durata','rules' => 'required'),

            array('field' => 'pickup', 'label' => 'punto di raccolta', 'rules' => 'required'),

            array('field' => 'daysAvailability', 'label' => 'Giorni della settimana', 'rules' => 'required'),

            // array('field' => 'dropoff', 'label' => 'Luogo di scarico', 'rules' => 'required'),
            
            array('field' => 'maxPassenger', 'label' => 'massimo passeggero', 'rules' => 'required|numeric'),

            array('field' => 'startPrice', 'label' => 'Prezzo di partenza', 'rules' => 'required|numeric'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE && $page != '404') {
          if ($this->input->post('id') != '0') {
              $this->Transfer_model->id = $this->input->post('id');
          }
          $this->Transfer_model->enTitle = $this->input->post('enTitle') ? $this->input->post('enTitle') : $this->input->post('itTitle');
          $this->Transfer_model->itTitle = $this->input->post('itTitle') ?  $this->input->post('itTitle') : $this->input->post('enTitle'); 
          $this->Transfer_model->enDescription = $this->input->post('enDescription') ? $this->input->post('enDescription') : $this->input->post('itDescription');
          $this->Transfer_model->itDescription = $this->input->post('itDescription');
          $this->Transfer_model->enServicesIncluded = $this->input->post('enServicesIncluded') ? $this->input->post('enServicesIncluded') : $this->input->post('itServicesIncluded');
          $this->Transfer_model->itServicesIncluded = $this->input->post('itServicesIncluded');
          $this->Transfer_model->enServicesNotIncluded = $this->input->post('enServicesNotIncluded') ? $this->input->post('enServicesNotIncluded') : $this->input->post('itServicesNotIncluded');
          $this->Transfer_model->itServicesNotIncluded = $this->input->post('itServicesNotIncluded');
          $this->Transfer_model->enPolicy = $this->input->post('enPolicy') ? $this->input->post('enPolicy') : $this->input->post('itPolicy');
          $this->Transfer_model->itPolicy = $this->input->post('itPolicy');
          $sd = explode("/", $this->input->post('dateStart'));
          $this->Transfer_model->dateStart = $sd[2] . '-' . $sd[0] . '-' . $sd[1];
          $ed = explode("/", $this->input->post('dateEnd'));
          $this->Transfer_model->dateEnd = $ed[2] . '-' . $ed[0] . '-' . $ed[1];

          $this->Transfer_model->startPrice = $this->input->post('startPrice');
          // $this->Transfer_model->car_type = $this->input->post('car_type');
          // $this->Transfer_model->boat_type = $this->input->post('boat_type');
          $this->Transfer_model->trip_type = $this->input->post('trip_type');
          // $this->Transfer_model->transfer_type = $this->input->post('transfer_type');
          $this->Transfer_model->categories = $this->input->post('categories');
          $this->Transfer_model->daysAvailability = $this->input->post('daysAvailability');
          $this->Transfer_model->pickup = $this->input->post('pickup');
          // $this->Transfer_model->dropoff = $this->input->post('dropoff');
          $this->Transfer_model->maxPassenger = $this->input->post('maxPassenger');
          $this->Transfer_model->duration = $this->input->post('duration');
          $this->Transfer_model->thumbnail = $this->Transfer_model->thumbnail ? $this->Transfer_model->thumbnail : ' ';
          $this->Transfer_model->cover = $this->Transfer_model->cover ? $this->Transfer_model->cover : ' ';
          $this->Transfer_model->customerId = $this->session->userdata('id');
          $order_business = $this->input->post('order_business');
          $order_business = explode(',',$order_business);
          $this->Transfer_model->businessPageId = $order_business[0];
          $this->Transfer_model->orderId = $order_business[1];

          $this->Transfer_model->zone_name = $this->input->post('zone_name');
          $this->Transfer_model->sjd_price = $this->input->post('sjd_price');
          $this->Transfer_model->csl_price = $this->input->post('csl_price');

          /*$this->Transfer_model->priceUnit = $this->input->post('priceUnit');
          $this->Transfer_model->price = $this->input->post('price');
          $this->Transfer_model->discountAdult = $this->input->post('discountAdult');
          $this->Transfer_model->discountChildren = $this->input->post('discountChildren');
          $this->Transfer_model->thumbnail = $this->Transfer_model->thumbnail ? $this->Transfer_model->thumbnail : ' ';
          $this->Transfer_model->cover = $this->Transfer_model->cover ? $this->Transfer_model->cover : ' ';
          $this->Transfer_model->customerId = $this->session->userdata('id');
          $this->Transfer_model->instagramWidget = $this->input->post('instagramWidget');
          $order_business = $this->input->post('order_business');
*/
         /* $order_business = explode(',',$order_business);
          $this->Transfer_model->businessPageId = $order_business[0];*/

            $this->Transfer_model->update();
            $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo i dettagli dell trasferimento.',
                'type' => 'success'));
            redirect('customer/transfer/edit/' . $id);
        } else {
            $data['msg'] = $this->session->flashdata('msg');
            $data['type'] = $this->session->flashdata('type');
            $data['cpage'] = 'transfer';
            $data['sub_cpage'] = 'new-transfer';
            $this->load->view('customer/includes/header', $data);
            $this->load->view('customer/includes/sidebar', $data);
            $this->load->view($page, $data);
            $this->load->view('customer/includes/footer', $data);
        }
    }

    public function upload_image($id = 0) {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Orders_model');
        $this->load->model('Transfer_model');
        $this->Transfer_model->load($id, $this->session->userdata('id'));
        if (!$this->Transfer_model->id) {
            $this->session->set_flashdata(Array('msg' => 'Non hai ancora inserito le informazioni del trasferimento',
                'type' => 'error'));
            redirect('customer/transfer/add/');
        }
        if ($_FILES && $_FILES['p_image']['name']) {
            $directoryName = './uploads/transfer';
            if(!is_dir($directoryName)){
                
                mkdir($directoryName, 0755);
            }
            $config['upload_path'] = './uploads/transfer/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '5000';
            $new_name = time() . $_FILES["p_image"]['name'];
            $config['file_name'] = $new_name;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('p_image')) {
                $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                    'type' => 'danger'));
                redirect('customer/transfer/edit/' . $id);
            } else {
                if ($this->input->post('imageType') == 'cover') {
                    $this->Transfer_model->update_photo(array('cover' => $new_name));
                } else {
                    $this->Transfer_model->update_photo(array('thumbnail' => $new_name));
                }
                $this->session->set_flashdata(Array('msg' => 'Hai caricato con successo l\'immagine',
                    'type' => 'success'));
                redirect('customer/transfer/edit/' . $id);
            }
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore seleziona l\'immagine da caricare',
                'type' => 'danger'));
            redirect('customer/transfer/edit/' . $id);
        }
    }

    public function delete() {
       $this->load->library('form_validation');
       $config = array(
           array(
               'field' => 'id',
               'label' => 'Place ID',
               'rules' => 'required|numeric'
       ));
       $this->form_validation->set_rules($config);
       if ($this->form_validation->run() != FALSE) {
           $this->load->model('Transfer_model');
           $this->Transfer_model->delete($this->input->post('id'));
           $this->session->set_flashdata(Array('msg' => 'Hai eliminato con successo il trasferimento. ',
               'type' => 'success'));
       }
       redirect('customer/transfer/');
   }


    public function date_valid($date) {
        $parts = explode("/", $date);
        if (count($parts) == 3) {
            if (checkdate((int)$parts[0], (int)$parts[1], (int)$parts[2])) {
                return TRUE;
            }
        }
        $this->form_validation->set_message('date_valid', 'The Date field must be mm/dd/yyyy');
        return false;
    }

}
