<?php

if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Signup extends CI_Controller{

    public function __construct() {
        parent::__construct();
         if ($this->session->userdata('id') && $this->session->userdata('type') == 'customer') {
            redirect('customer/dashboard');
        } else if ($this->session->userdata('id') && $this->session->userdata('type') == 'admin') {
            redirect('admin/dashboard');
        }
    }

    public function index(){
        $this->load->helper('form');
        $this->load->library('form_validation');

        $config = array(
            array('field'=>'firstName', 'label'=>'First Name','rules'=>'required|min_length[2]|max_length[100]'),
            array('field'=>'email', 'label'=>'Email','rules'=>'required|valid_email|is_unique[customers.email]'),
            array('field'=>'password', 'label'=>'Password','rules'=>'required|min_length[6]|md5'),
            array('field'=>'c_password', 'label'=>'Confirm Password','rules'=>'required|md5')
            );

        $this->form_validation->set_rules($config);
        if($this->form_validation->run() != FALSE){
            $this->load->model('customer/Customer_model');
            $res = $this->Customer_model->signup();
            if($res['message'] != ' ' || $res['message'] != ''){
                $this->session->set_flashdata(array('msg'=>'Hai creato con successo un nuovo account!<br>Procedi alla verifica tramite il link che troverai nella mail che ti abbiamo inviato.<br>Verifica anche nello SPAM.','type'=>'success'));
								//INVIO MAIL ADMIN 
								$conf['protocol'] = 'sendmail';
								$conf['charset'] = 'iso-8859-1';
								$conf['wordwrap'] = TRUE;
								$this->load->library("email", $conf);
								$this->load->helper("email");
								$this->email->initialize($conf);
								$this->email->set_mailtype("html");
								$this->email->from('do-not-reply@visitprocida.it', 'VisitProcida');
								$this->email->to('visitprocida@gmail.com');
								$this->email->subject('New customer | Visit Procida');
								$this->email->message('New customer registration:<br><strong>Name:</strong> '.$this->input->post('firstName').'<br><strong>Surname:</strong> '.$this->input->post('lastName').'<br><strong>Email:</strong> '.$this->input->post('email').'');
								$this->email->send();														

								//INVIO MAIL CUSTOMER 
								$conf['protocol'] = 'sendmail';
								$conf['charset'] = 'iso-8859-1';
								$conf['wordwrap'] = TRUE;
								$this->load->library("email", $conf);
								$this->load->helper("email");
								$this->email->initialize($conf);
								$this->email->set_mailtype("html");
								$this->email->from('do-not-reply@visitprocida.it', 'VisitProcida');
								$this->email->to($this->input->post('email'));
								$this->email->subject('Signup | Visit Procida');
								$this->email->message('Welcome to VisitProcida.<br><br>Your registration data:<br><strong>Name:</strong> '.$this->input->post('firstName').'<br><strong>Surname:</strong> '.$this->input->post('lastName').'<br><strong>Email:</strong> '.$this->input->post('email').'<br><br>Check your email address by clicking on this <strong><a href="https://www.visitprocida.com/customer/active?token='.md5($this->input->post('lastName').$this->input->post('email').$this->input->post('password')).'">link</a></strong>');
								$this->email->send();														

                redirect('customer/login');
            }else{
              $errors['msg'] = $res['message'];
              $errors['type'] = 'danger';
              $this->load->view('customer/signup',$errors);
            }
        }else{
          $this->load->view('customer/signup');
        }
    }

}
