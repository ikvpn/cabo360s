<?php


if(!defined('BASEPATH')){exit('No direct script access is allowed');}

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id') && $this->session->userdata('type') == 'customer') {
            redirect('customer/dashboard');
        } else if ($this->session->userdata('id') && $this->session->userdata('type') == 'admin') {
            redirect('admin/dashboard');
        }
    }

    public function index() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|email'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|md5'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->input->post('password') != '' && $this->input->post('email') != ''){
            $this->load->model('customer/Customer_model');
            //echo "<pre>"; print_r(md5($this->input->post('password'))); die;
            $res = $this->Customer_model->login($this->input->post('email'), md5($this->input->post('password')));
			//echo "<pre>"; print_r($res); die;
            if ($res === TRUE || $res === 1) {
                $this->load->helper('cookie');
                $name   = 'user_cookie';
                $value  = serialize($this->session->userdata());
                $expire = time()+3600;
                $path  = '/';
                $secure = TRUE;
                setcookie($name,'',-1,$path);
                setcookie($name,$value,$expire,$path);
                redirect('customer/dashboard');
            } else {
                $this->load->view('customer/login', array('msg' => 'Email o Password errate', 'type' => 'danger'));
            }
        } else {
            $data['msg'] = $this->session->flashdata('msg');
            $data['type'] = $this->session->flashdata('type');
            if(strlen($data['msg'])>0){
             $this->load->view('customer/login', $data);
            }else{
                $this->load->view('customer/login');
            }
        }
    }

}
