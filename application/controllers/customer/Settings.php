<?php

if (!defined('BASEPATH')) {
    exit('No direct script access is allowed');
}

class Settings extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id') && $this->session->userdata('usertype') != 'admin') {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('customer/login');
        }
        $this->load->helper('language');
        $this->lang->load('customerpanel/Common');
        $this->lang->load('customerpanel/Settings');
    }

    public function index() {
        $this->load->model('customer/Customer_model');
        $this->Customer_model->load($this->session->userdata('id'));
        //var_dump($this->customer_model);exit();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $data['userinfo'] = $this->userinfo;
        $data['cpage'] = 'settings';
        $config = array(
            array('field' => 'firstName', 'label' => 'First Name', 'rules' => 'required|max_length[50]'),
            array('field' => 'lastName', 'label' => 'First Name', 'rules' => 'required|max_length[50]'),
            array('field' => 'company', 'label' => 'Company', 'rules' => 'required|max_length[50]'),
            array('field' => 'billingAddress1', 'label' => 'Address Line 1', 'rules' => 'required|max_length[100]'),
            array('field' => 'billingAddress2', 'label' => 'Address Line 2', 'rules' => 'max_length[100]'),
            array('field' => 'billingCity', 'label' => 'City', 'rules' => 'required|max_length[100]'),
            array('field' => 'billingPostcode', 'label' => 'Postal Code', 'rules' => 'required|numeric'),
            array('field' => 'billingState', 'label' => 'State', 'rules' => 'required|max_length[100]'),
            array('field' => 'billingCountry', 'label' => 'Country', 'rules' => 'required|max_length[100]'),
            array('field' => 'billingPhone', 'label' => 'Phone No', 'rules' => 'required|max_length[20]')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $this->Customer_model->update($this->session->userdata('id'));
            $this->session->set_flashdata(Array('msg' => 'hai aggiornato con successo le informazioni del tuo profilo',
                'type' => 'success'));
            redirect('customer/settings');
        } else {
            $this->load->view('customer/includes/header');
            $this->load->view('customer/includes/sidebar', $data);
            $this->load->view('customer/contents/settings', $data);
            $this->load->view('customer/includes/footer', $data);
        }
    }

    public function resetpassword() {

        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'password',
                'label' => 'Current Password',
                'rules' => 'required'
            ),
            array(
                'field' => 'n_password',
                'label' => 'New Password',
                'rules' => 'required|min_length[6]|max_length[30]'
            ),
            array(
                'field' => 'c_password',
                'label' => 'Confirm Password',
                'rules' => 'required|matches[n_password]'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE) {

            $this->load->model('customer/Customer_model');

            $res = $this->Customer_model->resetpassword();

            //echo "<pre>"; print_r('reset customer passw run valid'); die;
            if ($res === true) {
                $this->session->set_flashdata(Array('msg' => "Hai modificato con successo la password",
                    'type' => 'success'));
            } else {
                $this->session->set_flashdata(Array('msg' => $res,
                    'type' => 'success'));
            }
        }
        else {
            $this->session->set_flashdata(Array('msg' => validation_errors(),
                'type' => 'danger'));
        }
        redirect('customer/settings');
    }

}
