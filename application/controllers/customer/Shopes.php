<?php

if(!defined('BASEPATH')){exit('No direct script access is allowed');}

class Shopes extends CI_Controller{
      private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')) {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('customer/login');
        }
        $this->load->helper('language');
        $this->lang->load('customerpanel/Common');
        $this->lang->load('customerpanel/Shops');
        $this->load->model('customer/Customer_model');
        $this->lang->load('customerpanel/Restaurant');
        $this->Customer_model->load($this->session->userdata('id'));

    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#hotels-dtable').dataTable();
                                                        });")
        );
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['cpage'] = 'shopes';
        $this->load->model('Orders_model');
        $this->load->model('Categories');
        $this->Categories->loadOnTableName('shopes');
        $data['orders'] = $this->Customer_model->getOrders($this->Categories->id);
        $this->load->view('customer/includes/header', $data);
        $this->load->view('customer/includes/sidebar', $data);
        $this->load->view('customer/contents/shopes/index', $data);
        $this->load->view('customer/includes/footer', $data);
    }

    public function edit($id) {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Orders_model');
        $this->Orders_model->load($id);
        $this->load->model('Shope_model');
        $this->Shope_model->loadOnOrderId($id);

        $this->load->model('admin/Tag_model');
        $this->load->model('admin/Activity_service_model');
        $data['tags'] = $this->Tag_model->getAll();
        $data['services'] = $this->Activity_service_model->getAll();

        $this->load->model('Categories');
        $data['categories'] = $this->Categories->getAllList();
        $data['main_category'] =   $this->Orders_model->category_id;

        $page = '404';
        if ($this->Orders_model->customer_id == $this->session->userdata('id') || $this->session->userdata('usertype') == 'admin') {
            $page = 'customer/contents/shopes/edit';
            $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-multiselect.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/select_categories.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/timepicker/bootstrap-timepicker.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
                Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });")
            );
            $data['cssfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/timepicker/bootstrap-timepicker.css'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-multiselect.css')
            );
        }
        $config = array(
            array('field' => 'id', 'label' => 'shope id', 'rules' => 'required|numeric'),

            array('field' => 'enName', 'label' => 'Name [en]', 'rules' => 'max_length[50]'),
            array('field' => 'duName', 'label' => 'Name [de]', 'rules' => 'max_length[50]'),
            array('field' => 'itName', 'label' => 'Name [it]', 'rules' => 'required|max_length[50]'),

            array('field' => 'enSlogan', 'label' => 'Slogan [en]', 'rules' => 'max_length[50]'),
            array('field' => 'duSlogan', 'label' => 'Slogan [de]', 'rules' => 'max_length[50]'),
            array('field' => 'itSlogan', 'label' => 'Slogan [it]', 'rules' => 'required|max_length[50]'),

            array('field' => 'enDescription', 'label' => 'Description [en]', 'rules' => 'max_length[50000]'),
            array('field' => 'duDescription', 'label' => 'Description [de]', 'rules' => 'max_length[50000]'),
            array('field' => 'itDescription', 'label' => 'Description [it]', 'rules' => 'required|max_length[50000]'),

            array('field' => 'phone', 'label' => 'Phone', 'rules' => 'required'),
            array('field' => 'website', 'label' => 'Website', 'rules' => 'required'),
            array('field' => 'cuisine', 'label' => 'cuisine', 'rules' => 'required'),
            array('field' => 'creditCards', 'label' => 'Credit Cards', 'rules' => 'required'),
            array('field' => 'hours', 'label' => 'hours', 'rules' => 'required|max_length[250]'),
            array('field' => 'closingDay', 'label' => 'closingDay', 'rules' => 'required'),
            array('field' => 'Info', 'label' => 'Info', 'rules' => 'required'),
            array('field' => 'address', 'label' => 'Address', 'rules' => 'max_length[250]'),
            array('field' => 'minPrice', 'label' => 'Minimum Price', 'rules' => 'required|numeric'),
            array('field' => 'maxPrice', 'label' => 'Maximum Price', 'rules' => 'required|numeric'),
            array('field' => 'lat', 'label' => 'Latitude', 'rules' => 'required|decimal'),
            array('field' => 'lng', 'label' => 'Longitude', 'rules' => 'required|decimal'),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE && $page != '404') {
            if ($this->input->post('id') != '0') {
                $this->Shope_model->id = $this->input->post('id');
            }
            $this->Shope_model->enName = $this->input->post('enName')?$this->input->post('enName'):$this->input->post('itName');
            $this->Shope_model->duName = $this->input->post('duName')?$this->input->post('duName'):$this->input->post('itName');
            $this->Shope_model->itName = $this->input->post('itName');
            $this->Shope_model->enSlogan = $this->input->post('enSlogan')?$this->input->post('enSlogan'):$this->input->post('itSlogan');
            $this->Shope_model->duSlogan = $this->input->post('duSlogan')?$this->input->post('duSlogan'):$this->input->post('itSlogan');
            $this->Shope_model->itSlogan = $this->input->post('itSlogan');
            if($this->input->post('enInsta')){
      				$this->Shope_model->enInsta = $this->input->post('enInsta');
      			}
      			if($this->input->post('fbWidget')){
      				$this->Shope_model->fbWidget = $this->input->post('fbWidget');
      			}
			      $this->Shope_model->enDescription = $this->input->post('enDescription')?$this->input->post('enDescription'):$this->input->post('itDescription');
            $this->Shope_model->duDescription = $this->input->post('duDescription')?$this->input->post('duDescription'):$this->input->post('itDescription');
            $this->Shope_model->itDescription = $this->input->post('itDescription');
            $this->Shope_model->category = "";
            $this->Shope_model->phone = $this->input->post('phone');
            $this->Shope_model->website = $this->input->post('website');
            $this->Shope_model->cuisine = $this->input->post('cuisine');
            $this->Shope_model->creditCards = $this->input->post('creditCards');
            $this->Shope_model->categories = $this->input->post('selected_categories');
            $this->Shope_model->hours = $this->input->post('hours');
            $this->Shope_model->hours2 = $this->input->post('hours2');
            $this->Shope_model->closingDay = $this->input->post('closingDay');
            $this->Shope_model->Info = $this->input->post('Info');
            $this->Shope_model->minPrice = $this->input->post('minPrice');
            $this->Shope_model->maxPrice = $this->input->post('maxPrice');
            $this->Shope_model->lat = $this->input->post('lat');
            $this->Shope_model->lng = $this->input->post('lng');
            $this->Shope_model->address = $this->input->post('address')?$this->input->post('address'):"";
            $this->Shope_model->order_id = $id;
            $this->Shope_model->photo = $this->Shope_model->photo!=''?$this->Shope_model->photo:"";
            $this->Shope_model->manager = $this->input->post('manager');
            $this->Shope_model->tags = $this->input->post('tags');
            $this->Shope_model->activity_services = $this->input->post('activity_services');

            if ($_FILES && $_FILES['m_image']['name'])
            {
            	$config['upload_path'] = './uploads/shopes/';
            	$config['allowed_types'] = 'gif|jpg|png|jpeg';
            	$config['max_size'] = '5000';
            	$new_name = time() . $_FILES["m_image"]['name'];
            	$config['file_name'] = $new_name;
            	$this->load->library('upload', $config);
            	if (!$this->upload->do_upload('m_image'))
            	{
                	$this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                    'type' => 'danger'));
                redirect('customer/shopes/edit/' . $id);
            	}
            	else
            	{
                	$this->Shope_model->manager_profile=$new_name;
            	}
            }
            $this->Shope_model->update();
            $this->session->set_flashdata(Array('msg' => 'Hai aggiornato con successo le informazioni dell\'attività',
                'type' => 'success'));
            redirect('customer/shopes/edit/' . $id);
        } else {
            $data['msg'] = $this->session->flashdata('msg');
            $data['type'] = $this->session->flashdata('type');
            $data['cpage'] = 'shopes';
            $data['cssfiles'] = array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-multiselect.css'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/Gallery/css/blueimp-gallery.min.css'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/timepicker/bootstrap-timepicker.css'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/fileupload/jquery.fileupload.css'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/fileupload/jquery.fileupload-ui.css')
            );

            $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-multiselect.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/select_categories.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/timepicker/bootstrap-timepicker.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
                Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });"),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/vendor/jquery.ui.widget.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.iframe-transport.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-process.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-image.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-audio.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-video.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-validate.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-ui.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/main.js')
            );
            $data['usertype'] = $this->session->userdata('usertype');
            if($this->session->userdata('usertype') == 'admin'){
              $this->load->model('Categories');
              $data['all_categories'] = $this->Categories->getAllList();
              $this->load->view('admin/includes/header', $data);
              $this->load->view('admin/includes/sidebar', $data);
              $this->load->view($page, $data);
              $this->load->view('admin/includes/footer');
            }
            else {
              $this->load->view('customer/includes/header', $data);
               $this->load->view('customer/includes/sidebar', $data);
               $this->load->view($page, $data);
               $this->load->view('customer/includes/footer', $data);
            }
        }
    }

    public function upload_image($id = 0) {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Orders_model');
        $this->Orders_model->load($id);
        $this->load->model('Shope_model');
        $this->Shope_model->loadOnOrderId($id);
        if (($this->Orders_model->customer_id != $this->session->userdata('id') && 'admin' != $this->session->userdata('usertype')) || !$this->Shope_model->id) {
             $this->session->set_flashdata(Array('msg' => 'Non hai ancora inserito le informazioni dell\'attività',
                    'type' => 'error'));
                redirect('customer/shopes/edit/' . $id);
        }
        if ($_FILES && $_FILES['p_image']['name']) {
            $config['upload_path'] = './uploads/shopes/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '5000';
            $new_name = time() . $_FILES["p_image"]['name'];
            $config['file_name'] = $new_name;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('p_image')) {
                $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                    'type' => 'danger'));
                redirect('customer/shopes/edit/' . $id);
            } else {
                $this->Shope_model->update_photo($new_name);
                $this->session->set_flashdata(Array('msg' => 'Hai caricato con successo l\'immagine primaria',
                    'type' => 'success', 'model' => 'login'));
                redirect('customer/shopes/edit/' . $id);
            }
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore seleziona l\'immagine da caricare',
                    'type' => 'danger'));
                redirect('customer/shopes/edit/' . $id);
        }
    }
    public function fileupload($id) {
        error_reporting(E_ALL | E_STRICT);
        $path = 'uploads/shopes/'.$id;
        if (!file_exists($path)) {
            mkdir($path);
        }
        $params = array("upload_dir" => '/' . $path . '/', "upload_url" => '/' . $path . '/');
        $this->load->library("UploadHandler", $params);
    }
}
