<?php

if (!defined("BASEPATH")) {
    exit("No direct script access is allwoed");
}

class Tours extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')) {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Please login to view this page',
                'type' => 'danger', 'model' => 'login'));
            redirect('customer/login');
        }
        $this->load->helper('language');
        $this->lang->load('customerpanel/Common');
        $this->load->model('customer/Customer_model');
        $this->Customer_model->load($this->session->userdata('id'));
    }

    public function index() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $data['userinfo'] = $this->userinfo;
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );

        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#events-dtable').dataTable();

                                                        });")
        );
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['cpage'] = 'tours';
        $data['usertype'] = $this->session->userdata('usertype');

        $this->load->model('Tour_model');
        $data['tours'] = $this->Tour_model->getAllTourOfCustomer($this->session->userdata('id'));
        $this->load->view('customer/includes/header', $data);
        $this->load->view('customer/includes/sidebar', $data);
        $this->load->view('customer/contents/tours/index', $data);
        $this->load->view('customer/includes/footer', $data);
    }

    public function add() {

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Tour_model');
        $page = 'customer/contents/tours/edit';
        $this->load->model('admin/Location_model');
        $this->load->model('customer/Customer_model');
        $this->load->model('Categories');
        $data['locations'] = $this->Location_model->getLocations();
        $this->Customer_model->load($this->session->userdata('id'));
        $orders = $this->Customer_model->getOrders();
        $businessPages = array();
        foreach ($orders as $key => $order) {
          # code...
          $category = $this->Categories->get($order->category_id);
          $table = $category->table;
          $businessPage = $this->Customer_model->getBusinessPage($table, $order->id);
          if($businessPage && count($businessPage) > 0){
            array_push($businessPages, $businessPage);
          }
        }
        $data['business_pages'] = $businessPages;
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/datepicker/bootstrap-datepicker.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/timepicker/bootstrap-timepicker.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-tagsinput.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-multiselect.css')
        );
        $data['usertype'] = $this->session->userdata('usertype');
        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/datepicker/bootstrap-datepicker.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/timepicker/bootstrap-timepicker.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-multiselect.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-tagsinput.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/utils_dashboard.js'),
            Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                                     $('.datepicker').datepicker();
                                                                      $('.timepicker').timepicker({showMeridian: false, defaultTime: false, showInputs: false});
                                                        });")
        );

        $config = array(
            array('field' => 'id', 'label' => 'Event id', 'rules' => 'required|numeric'),

            array('field' => 'itTitle', 'label' => 'Name [it]', 'rules' => 'required|max_length[50]'),
            array('field' => 'enTitle', 'label' => 'Name [en]', 'rules' => 'max_length[50]'),

            array('field' => 'itDescription', 'label' => 'Description [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enDescription', 'label' => 'Description [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itServicesIncluded', 'label' => 'Included Services [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enServicesIncluded', 'label' => 'Not included services [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itServicesNotIncluded', 'label' => 'Servizi non Inclusi [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enServicesNotIncluded', 'label' => 'Servizi non Inclusi [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itPolicy', 'label' => 'reservation and cancellation policy [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'itPolicy', 'label' => 'reservation and cancellation policy [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'dateStart', 'label' => 'End Date', 'rules' => 'required|callback_date_valid'),
            array('field' => 'dateEnd', 'label' => 'End Time', 'rules' => 'required|callback_date_valid'),

            array('field' => 'lat', 'label' => 'Latitude', 'rules' => 'required|decimal'),
            array('field' => 'long', 'label' => 'Longitude', 'rules' => 'required|decimal'),

            array('field' => 'locationId','label' => 'Location','rules' => 'required|numeric'),
            array('field' => 'order_business','label' => 'Activity','rules' => 'required'),

            array('field' => 'price', 'label' => 'Adult Price', 'rules' => 'required'),
            array('field' => 'childPrice', 'label' => 'Child Price', 'rules' => 'required'),
            array('field' => 'fixedPrice', 'label' => 'Fixed Price', 'rules' => 'required'),
            array('field' => 'priceUnit', 'label' => 'Price Type', 'rules' => 'required'),

            array('field' => 'type', 'label' => 'Tour type', 'rules' => 'required|numeric'),

        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE && $page != '404') {
            if ($this->input->post('id') != '0') {
                $this->Tour_model->id = $this->input->post('id');
            }
            $this->Tour_model->enTitle = $this->input->post('enTitle') ? $this->input->post('enTitle') : $this->input->post('itTitle');
            $this->Tour_model->itTitle = $this->input->post('itTitle') ;
            $this->Tour_model->enDescription = $this->input->post('enDescription') ? $this->input->post('enDescription') : $this->input->post('itDescription');
            $this->Tour_model->itDescription = $this->input->post('itDescription');
            $this->Tour_model->enServicesIncluded = $this->input->post('enServicesIncluded') ? $this->input->post('enServicesIncluded') : $this->input->post('itServicesIncluded');
            $this->Tour_model->itServicesIncluded = $this->input->post('itServicesIncluded');
            $this->Tour_model->enServicesNotIncluded = $this->input->post('enServicesNotIncluded') ? $this->input->post('enServicesNotIncluded') : $this->input->post('itServicesNotIncluded');
            $this->Tour_model->itServicesNotIncluded = $this->input->post('itServicesNotIncluded');
            $this->Tour_model->enPolicy = $this->input->post('enPolicy') ? $this->input->post('enPolicy') : $this->input->post('itPolicy');
            $this->Tour_model->itPolicy = $this->input->post('itPolicy');
            $sd = explode("/", $this->input->post('dateStart'));
            $this->Tour_model->dateStart = $sd[2] . '-' . $sd[0] . '-' . $sd[1];
            $ed = explode("/", $this->input->post('dateEnd'));
            $this->Tour_model->dateEnd = $ed[2] . '-' . $ed[0] . '-' . $ed[1];
            $this->Tour_model->lat = $this->input->post('lat');
            $this->Tour_model->long = $this->input->post('long');
            $this->Tour_model->locationId = $this->input->post('locationId');

            $this->Tour_model->categories = $this->input->post('categories');
            $this->Tour_model->daysAvailability = $this->input->post('daysAvailability');
            $this->Tour_model->durationDay = $this->input->post('durationDay');
            $this->Tour_model->durationHours = $this->input->post('durationHours');
            $this->Tour_model->type = $this->input->post('type');
            $this->Tour_model->durationMin = $this->input->post('durationMin');
            $this->Tour_model->sessionHour1 = $this->input->post('sessionHour1');
            $this->Tour_model->sessionHour2 = $this->input->post('sessionHour2');
            $this->Tour_model->sessionHour3 = $this->input->post('sessionHour3');
            $this->Tour_model->priceUnit = $this->input->post('priceUnit');
            $this->Tour_model->price = $this->input->post('price');
            // $this->Tour_model->discountAdult = $this->input->post('discountAdult');
            // $this->Tour_model->discountChildren = $this->input->post('discountChildren');
            $this->Tour_model->thumbnail = $this->Tour_model->thumbnail ? $this->Tour_model->thumbnail : ' ';
            $this->Tour_model->cover = $this->Tour_model->cover ? $this->Tour_model->cover : ' ';
            $this->Tour_model->customerId = $this->session->userdata('id');
            $this->Tour_model->instagramWidget = $this->input->post('instagramWidget');
            $order_business = $this->input->post('order_business');
            $order_business = explode(',',$order_business);
            $this->Tour_model->businessPageId = $order_business[0];
            $this->Tour_model->orderId = $order_business[1];
            $this->Tour_model->fixedPrice = $this->input->post('fixedPrice');
            $this->Tour_model->maxParticipants = $this->input->post('maxParticipants');
            $this->Tour_model->childPrice = $this->input->post('childPrice');

            $id = $this->Tour_model->update();
            $this->session->set_flashdata(Array('msg' => 'Hai aggiunto con successo un nuovo tour',
                'type' => 'success'));
            redirect('customer/tours/edit/' . $id);
        } else {
            $data['msg'] = $this->session->flashdata('msg');
            $data['type'] = $this->session->flashdata('type');
            $data['cpage'] = 'tours';
            $data['sub_cpage'] = 'new-tour';

            $this->load->view('customer/includes/header', $data);
            $this->load->view('customer/includes/sidebar', $data);
            $this->load->view($page, $data);
            $this->load->view('customer/includes/footer', $data);
        }
    }

    public function edit($id = 0) {

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Tour_model');
        $this->Tour_model->load($id, $this->session->userdata('id'));
        $page = 'customer/contents/tours/edit';
        $this->load->model('admin/Location_model');
        $this->load->model('customer/Customer_model');
        $this->load->model('Categories');
        $data['locations'] = $this->Location_model->getLocations();
        $this->Customer_model->load($this->session->userdata('id'));
        $orders = $this->Customer_model->getOrders();
        $businessPages = array();
        foreach ($orders as $key => $order) {
          # code...
          $category = $this->Categories->get($order->category_id);
          $table = $category->table;
          $businessPage = $this->Customer_model->getBusinessPage($table, $order->id);
          if($businessPage && count($businessPage) > 0){
            array_push($businessPages, $businessPage);
          }
        }
        $data['business_pages'] = $businessPages;
        $data['locations'] = $this->Location_model->getLocations();
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/datepicker/bootstrap-datepicker.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-tagsinput.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/timepicker/bootstrap-timepicker.css'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-multiselect.css')
        );
        $data['usertype'] = $this->session->userdata('usertype');
        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/datepicker/bootstrap-datepicker.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/timepicker/bootstrap-timepicker.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-multiselect.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-tagsinput.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/utils_dashboard.js'),
            Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                                     $('.datepicker').datepicker();
                                                                      $('.timepicker').timepicker({showMeridian: false, defaultTime: false, showInputs: false});
                                                        });")
        );

        $config = array(
            array('field' => 'id', 'label' => 'Event id', 'rules' => 'required|numeric'),

            array('field' => 'itTitle', 'label' => 'Name [it]', 'rules' => 'required|max_length[50]'),
            array('field' => 'enTitle', 'label' => 'Name [en]', 'rules' => 'max_length[50]'),

            array('field' => 'itDescription', 'label' => 'Description [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enDescription', 'label' => 'Description [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itServicesIncluded', 'label' => 'Included Services [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enServicesIncluded', 'label' => 'Not included services [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itServicesNotIncluded', 'label' => 'Servizi non Inclusi [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'enServicesNotIncluded', 'label' => 'Servizi non Inclusi [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'itPolicy', 'label' => 'reservation and cancellation policy [it]', 'rules' => 'required|max_length[50000]'),
            array('field' => 'itPolicy', 'label' => 'reservation and cancellation policy [en]', 'rules' => 'max_length[50000]'),

            array('field' => 'dateStart', 'label' => 'End Date', 'rules' => 'required|callback_date_valid'),
            array('field' => 'dateEnd', 'label' => 'End Time', 'rules' => 'required|callback_date_valid'),

            array('field' => 'lat', 'label' => 'Latitude', 'rules' => 'required|decimal'),
            array('field' => 'long', 'label' => 'Longitude', 'rules' => 'required|decimal'),

            array('field' => 'locationId','label' => 'Location','rules' => 'required|numeric'),
            array('field' => 'order_business','label' => 'Activity','rules' => 'required'),

            array('field' => 'price', 'label' => 'Adult Price', 'rules' => 'required'),
            array('field' => 'childPrice', 'label' => 'Child Price', 'rules' => 'required'),
            array('field' => 'fixedPrice', 'label' => 'Fixed Price', 'rules' => 'required'),
            array('field' => 'priceUnit', 'label' => 'Price Type', 'rules' => 'required'),

            array('field' => 'type', 'label' => 'Tour type', 'rules' => 'required|numeric'),

        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE && $page != '404') {
          if ($this->input->post('id') != '0') {
              $this->Tour_model->id = $this->input->post('id');
          }
          $this->Tour_model->enTitle = $this->input->post('enTitle') ? $this->input->post('enTitle') : $this->input->post('itTitle');
          $this->Tour_model->itTitle = $this->input->post('itTitle') ;
          $this->Tour_model->enDescription = $this->input->post('enDescription') ? $this->input->post('enDescription') : $this->input->post('itDescription');
          $this->Tour_model->itDescription = $this->input->post('itDescription');
          $this->Tour_model->enServicesIncluded = $this->input->post('enServicesIncluded') ? $this->input->post('enServicesIncluded') : $this->input->post('itServicesIncluded');
          $this->Tour_model->itServicesIncluded = $this->input->post('itServicesIncluded');
          $this->Tour_model->enServicesNotIncluded = $this->input->post('enServicesNotIncluded') ? $this->input->post('enServicesNotIncluded') : $this->input->post('itServicesNotIncluded');
          $this->Tour_model->itServicesNotIncluded = $this->input->post('itServicesNotIncluded');
          $this->Tour_model->enPolicy = $this->input->post('enPolicy') ? $this->input->post('enPolicy') : $this->input->post('itPolicy');
          $this->Tour_model->itPolicy = $this->input->post('itPolicy');
          $sd = explode("/", $this->input->post('dateStart'));
          $this->Tour_model->dateStart = $sd[2] . '-' . $sd[0] . '-' . $sd[1];
          $ed = explode("/", $this->input->post('dateEnd'));
          $this->Tour_model->dateEnd = $ed[2] . '-' . $ed[0] . '-' . $ed[1];
          $this->Tour_model->lat = $this->input->post('lat');
          $this->Tour_model->long = $this->input->post('long');
          $this->Tour_model->locationId = $this->input->post('locationId');

          $this->Tour_model->categories = $this->input->post('categories');
          $this->Tour_model->daysAvailability = $this->input->post('daysAvailability');
          $this->Tour_model->durationDay = $this->input->post('durationDay');
          $this->Tour_model->durationHours = $this->input->post('durationHours');
          $this->Tour_model->type = $this->input->post('type');
          $this->Tour_model->durationMin = $this->input->post('durationMin');

          if($this->input->post('sessionHour1') == ' 00:00 - 00:00') {
              $this->Tour_model->sessionHour1 = NULL;
          }
          else {
            $this->Tour_model->sessionHour1 = $this->input->post('sessionHour1');
          }
          if($this->input->post('sessionHour2') == ' 00:00 - 00:00') {
              $this->Tour_model->sessionHour2 = NULL;
          }
          else {
            $this->Tour_model->sessionHour2 = $this->input->post('sessionHour2');
          }
          if($this->input->post('sessionHour3') == ' 00:00 - 00:00') {
              $this->Tour_model->sessionHour3 = NULL;
          }
          else {
            $this->Tour_model->sessionHour3 = $this->input->post('sessionHour3');
          }
          $this->Tour_model->priceUnit = $this->input->post('priceUnit');
          $this->Tour_model->price = $this->input->post('price');
          // $this->Tour_model->discountAdult = $this->input->post('discountAdult');
          // $this->Tour_model->discountChildren = $this->input->post('discountChildren');
          $this->Tour_model->thumbnail = $this->Tour_model->thumbnail ? $this->Tour_model->thumbnail : ' ';
          $this->Tour_model->cover = $this->Tour_model->cover ? $this->Tour_model->cover : ' ';
          $this->Tour_model->customerId = $this->session->userdata('id');
          $this->Tour_model->instagramWidget = $this->input->post('instagramWidget');
          $order_business = $this->input->post('order_business');

          $order_business = explode(',',$order_business);
          $this->Tour_model->businessPageId = $order_business[0];
          $this->Tour_model->orderId = $order_business[1];
          $this->Tour_model->fixedPrice = $this->input->post('fixedPrice');
          $this->Tour_model->maxParticipants = $this->input->post('maxParticipants');
          $this->Tour_model->childPrice = $this->input->post('childPrice');

            $this->Tour_model->update();
            $this->session->set_flashdata(Array('msg' => 'Hai modificato con successo i dettagli dell tour.',
                'type' => 'success'));
            redirect('customer/tours/edit/' . $id);
        } else {
            $data['msg'] = $this->session->flashdata('msg');
            $data['type'] = $this->session->flashdata('type');
            $data['cpage'] = 'tours';
            $data['sub_cpage'] = 'all-tours';
            $this->load->view('customer/includes/header', $data);
            $this->load->view('customer/includes/sidebar', $data);
            $this->load->view($page, $data);
            $this->load->view('customer/includes/footer', $data);
        }
    }

    public function upload_image($id = 0) {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Orders_model');
        $this->load->model('Tour_model');
        $this->Tour_model->load($id, $this->session->userdata('id'));
        if (!$this->Tour_model->id) {
            $this->session->set_flashdata(Array('msg' => 'Non hai ancora inserito le informazioni del tour',
                'type' => 'error'));
            redirect('customer/tours/add/');
        }
        if ($_FILES && $_FILES['p_image']['name']) {
            $config['upload_path'] = './uploads/tours/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '5000';
            $new_name = time() . $_FILES["p_image"]['name'];
            $config['file_name'] = $new_name;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('p_image')) {
                $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                    'type' => 'danger'));
                redirect('customer/tours/edit/' . $id);
            } else {
                if ($this->input->post('imageType') == 'cover') {
                    $this->Tour_model->update_photo(array('cover' => $new_name));
                } else {
                    $this->Tour_model->update_photo(array('thumbnail' => $new_name));
                }
                $this->session->set_flashdata(Array('msg' => 'Hai caricato con successo l\'immagine',
                    'type' => 'success'));
                redirect('customer/tours/edit/' . $id);
            }
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore seleziona l\'immagine da caricare',
                'type' => 'danger'));
            redirect('customer/tours/edit/' . $id);
        }
    }

    public function delete() {
       $this->load->library('form_validation');
       $config = array(
           array(
               'field' => 'id',
               'label' => 'Place ID',
               'rules' => 'required|numeric'
       ));
       $this->form_validation->set_rules($config);
       if ($this->form_validation->run() != FALSE) {
           $this->load->model('Tour_model');
           $this->Tour_model->delete($this->input->post('id'));
           $this->session->set_flashdata(Array('msg' => 'Hai eliminato con successo il tour. ',
               'type' => 'success'));
       }
       redirect('customer/tours/');
   }


    public function date_valid($date) {
        $parts = explode("/", $date);
        if (count($parts) == 3) {
            if (checkdate((int)$parts[0], (int)$parts[1], (int)$parts[2])) {
                return TRUE;
            }
        }
        $this->form_validation->set_message('date_valid', 'The Date field must be mm/dd/yyyy');
        return false;
    }

}
