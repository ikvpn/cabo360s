<?php


if(!defined('BASEPATH')){ exit('No direct script access is allowed');}

class Dashboard extends CI_Controller{
    private $userinfo;
    public function __construct() {
        parent::__construct();
         if ($this->session->userdata('id')) {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('customer/login');
        }
        $this->load->helper('language');
        $this->lang->load('customerpanel/Common');
        $this->load->model('customer/Customer_model');
        $this->load->model('Procida_model');
        $this->load->model('Orders_model');
        $this->Customer_model->load($this->session->userdata('id'));
    }

    public function index(){
        $data['userinfo'] = $this->userinfo;
        if($this->session->userdata('usertype')=='customer')
        if($this->Customer_model->billingAddress1=='' || $this->Customer_model->billingAddress1 == NULL){
            $this->session->set_flashdata(Array('msg' => 'Per favore completa il tuo profilo inserendo i dettagli',
                'type' => 'warning', 'model' => 'login'));
            redirect('customer/settings');
        }
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );
        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#expired_pages').dataTable();
                                                                     $('#active_pages').dataTable();
                                                                     $('#new_pages').dataTable();
                                                        });")
        );
        $data['stats'] = $this->getYearlyStats();
        $data['contactStats'] = $this->getYearlyContactStats();
        // echo "<pre>"; print_r($data['contactStats']); die;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['cpage'] = 'dashboard';
        $data['order_details'] = $this->Orders_model->getOrderDetails();
        $data['title'] = "Reservaciones";
        $this->load->view('customer/includes/header',$data);
        $this->load->view('customer/includes/sidebar', $data);
        $this->load->view('customer/contents/dashboard', $data);
        $this->load->view('customer/includes/footer',$data);
    }

    public function logout() {
        $user = Array('id' => '',
                'email' => '',
                'usertype' => '',
                'name' => '');
        $this->session->set_userdata($user);
        $this->session->set_flashdata(Array('msg' => 'Hai effettuato con successo il logout!',
                'type' => 'success'));
        setcookie('user_cookie','',-1,'/');
        redirect('customer/login');
    }

    private function getYearlyStats(){
      $stats = [];
      for ($i = 0 ; $i < 12 ; $i++) {
        $stats[$i] = 0;
      }
      $categories = $this->Customer_model->getCategories();
      foreach ($categories as $key => $category) {
        $orders = $this->Customer_model->getOrders($category->id);
        foreach ($orders as $key => $order) {
          $businessPage = $this->Customer_model->getBusinessPage($category->table, $order->id);
          if($businessPage) {
            $type = $category->table;
            for ($month = 0 ; $month < 12 ; $month++) {
              $views = $this->Procida_model->getYearlyViewsOfBusinessPage($businessPage->id, $type ,$month+1);
              $stats[$month] = $stats[$month] + $views;
            }
          }
        }
      }
      return $stats;
    }

    private function getYearlyContactStats(){
      $stats = [];

      for ($month = 0 ; $month < 12 ; $month++) {
        $stats[$month] = 0;
        $views = $this->Procida_model->getYearlyContactsOfBusinessPage($this->session->userdata('id'), $month+1);
        $stats[$month] = $stats[$month] + $views;
      }

      return $stats;
    }
}
