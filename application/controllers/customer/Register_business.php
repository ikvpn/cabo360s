<?php

if (!defined('BASEPATH')) {
    exit('No direct script access is allowed');
}

class Register_business extends CI_Controller {

    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')) {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('name'),
                'email' => $this->session->userdata('email')
            );
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',
                'type' => 'danger', 'model' => 'login'));
            redirect('customer/login');
        }
        $this->load->helper('language');
        $this->lang->load('customerpanel/Common');
        $this->lang->load('customerpanel/Bussiness');
        $this->load->model('customer/Customer_model');
        $this->Customer_model->load($this->session->userdata('id'));
    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $this->load->model('Categories');
        $data['categories'] = $this->Categories->getAllList();
        $data['cpage'] = 'register';
        $this->load->view('customer/includes/header');
        $this->load->view('customer/includes/sidebar', $data);
        $this->load->view('customer/contents/register_business', $data);
        $this->load->view('customer/includes/footer', $data);
    }

    public function newbusiness($category) {
        $this->load->model('customer/Customer_model');
        $this->Customer_model->load($this->session->userdata('id'));
        if ($this->Customer_model->billingAddress1 == '' && $this->session->userdata('usertype')!='admin') {
            $this->session->set_flashdata(array('msg' => 'Per favore inserisci i tuoi dati', 'type' => 'info'));
            redirect('customer/settings');
        }
        $this->load->model('Categories');
        $this->load->model('admin/Location_model');
        $this->load->model('admin/Settings_model');
        $data['settings'] = $this->Settings_model->getsettings();
        $data['locations'] = $this->Location_model->getLocations(false);
        $this->Categories->load($category);
        $page = '404';
        if ($this->Categories->id) {
            $page = 'customer/contents/paymentform';
        }
        $this->load->helper('form');
        $data['cpage'] = 'register';
        $this->load->library('form_validation');
        $config = array(
            array('field' => 'title', 'label' => 'Title', 'rules' => 'required|min_length[2]|max_length[60]|is_unique[orders.title]|alpha_numeric'),
            array('field' => 'phone', 'label' => 'Phone', 'rules' => 'required|numeric'),
            array('field' => 'location', 'label' => 'Location', 'rules' => 'required|numeric'),
            array('field' => 'address', 'label' => 'Address', 'rules' => 'required|max_length[250]'),
            array('field' => 'payment_plan', 'label' => 'Payment Plan', 'rules' => 'required'),
            array('field' => 'payment_type', 'label' => 'payment type', 'rules' => 'required')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $this->load->model('Orders_model');
            $order = array('customer_id' => $this->session->userdata('id'),
                'category_id' => $category,
                'status' => 'new',
                'title' => $this->input->post('title'),
                'phone' => $this->input->post('phone'),
                'location' => $this->input->post('location'),
                'address' => $this->input->post('address'),
                'payment_plan' => $this->input->post('payment_plan'),
                'agreement_amount' => $this->input->post('payment_plan') == 'M' ? $this->Categories->m_charges : $this->Categories->y_charges,
                'payment_type' => $this->input->post('payment_type'));
            $order_id = $this->Orders_model->addnew($order);

            if ($this->input->post('payment_type') == 'paypal') {
                redirect('gateways/paypal_express/pay_invoice/'.$order_id);
            } else {
                // print invoice
                $this->session->set_flashdata(array('msg' => 'Hai inserito la tua attività con successo!', 'type' => 'success'));
                redirect('customer/register_business/invoice/' . $order_id);
            }
        } else {
            $this->load->view('customer/includes/header');
            $this->load->view('customer/includes/sidebar', $data);
            $this->load->view($page, $data);
            $this->load->view('customer/includes/footer');
        }
    }

    public function invoice($order_id) {
        $this->load->model('Orders_model');
        $this->Orders_model->load($order_id);
        $data['userinfo'] = $this->userinfo;
        $data['cpage'] = 'register';
        $page = "404";

        if ($this->Orders_model->category_id) {
            $page = 'customer/contents/invoice';
        }
        $this->lang->load('customerpanel/Invoice');
        $this->load->view('customer/includes/header',$data);
        $this->load->view('customer/includes/sidebar', $data);
        $this->load->view($page, $data);
        $this->load->view('customer/includes/footer',$data);
    }
}
