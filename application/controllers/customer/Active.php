<?php

if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Active extends CI_Controller{

    public function __construct() {
        parent::__construct();
         if ($this->session->userdata('id') && $this->session->userdata('type') == 'customer') {
            redirect('customer/dashboard');
        } else if ($this->session->userdata('id') && $this->session->userdata('type') == 'admin') {
            redirect('admin/dashboard');
        }
    }

    public function index(){
			$this->load->model('customer/Customer_model');
			$res = $this->Customer_model->active();
			if($res['id']){
					$errors['msg'] = 'Utente attivato con successo!';
					$errors['type'] = 'success';
			}
			else{
				$errors['msg'] = 'Token non valido!';
				$errors['type'] = 'danger';
			}
			$this->load->view('customer/active', $errors);
    }

}