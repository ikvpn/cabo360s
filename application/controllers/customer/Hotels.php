<?php


if (!defined('BASEPATH')) {
    exit('No direct script access is allowed');
}

class Hotels extends CI_Controller {
    private $userinfo;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('id')) {
            $this->userinfo['userinfo'] = Array(
                'id' => $this->session->userdata('id'),
                'full_name' => $this->session->userdata('name'),
                'email' => $this->session->userdata('email')
            );

        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore loggati per vedere questa pagina',

                'type' => 'danger', 'model' => 'login'));
            redirect('customer/login');
        }
        $this->load->helper('Language');
        $this->lang->load('customerpanel/Common');
        $this->lang->load('customerpanel/Hotel');
        $this->load->model('customer/Customer_model');
        $this->Customer_model->load($this->session->userdata('id'));
    }

    public function index() {
        $data['userinfo'] = $this->userinfo;
        $data['cssfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/dataTables.bootstrap.css')
        );
        $data['jsfiles'] = Array(
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/jquery.dataTables.js'),
            Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/dataTables/dataTables.bootstrap.js'),
            Array('type' => 'inline', 'src' => " $(document).ready(function() {
                                                                     $('#hotels-dtable').dataTable();
                                                        });")
        );
        $data['msg'] = $this->session->flashdata('msg');
        $data['type'] = $this->session->flashdata('type');
        $data['cpage'] = 'hotels';
        $this->load->model('Orders_model');
        $this->load->model('Categories');
        $this->Categories->loadOnTableName('hotels');
        $data['orders'] = $this->Customer_model->getOrders($this->Categories->id);

        $this->load->view('customer/includes/header', $data);
        $this->load->view('customer/includes/sidebar', $data);
        $this->load->view('customer/contents/hotels/index', $data);
        $this->load->view('customer/includes/footer', $data);
    }



    public function edit($id = 0) {

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Orders_model');
        $this->Orders_model->load($id);
        $this->load->model('Hotel_model');
        $this->Hotel_model->loadOnOrderId($id);
        $this->load->model('Categories');
        $data['categories'] = $this->Categories->getAllList();
        $data['main_category'] =   $this->Orders_model->category_id;

        $this->load->model('admin/Tag_model');
        $this->load->model('admin/Activity_service_model');
        $data['tags'] = $this->Tag_model->getAll();
        $data['services'] = $this->Activity_service_model->getAll();

        $page = '404';

        if ($this->Orders_model->customer_id == $this->session->userdata('id') || $this->session->userdata('usertype') == 'admin') {
            $page = 'customer/contents/hotels/edit';
            $data['jsfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/bootstrap-multiselect.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/select_categories.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/timepicker/bootstrap-timepicker.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/ckeditor/ckeditor.js'),
                Array('type' => 'inline', 'src' => " $(function() {
                                                                    CKEDITOR.replace('en_editor');
                                                                    CKEDITOR.replace('du_editor');
                                                                    CKEDITOR.replace('it_editor');
                                                        });"),

                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/vendor/jquery.ui.widget.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.iframe-transport.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-process.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-image.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-audio.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-video.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-validate.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/jquery.fileupload-ui.js'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/js/plugins/fileupload/main.js')
            );
            $data['cssfiles'] = Array(
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/timepicker/bootstrap-timepicker.css'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/bootstrap-multiselect.css'),
                Array('type' => 'file', 'src' => '//blueimp.github.io/Gallery/css/blueimp-gallery.min.css'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/fileupload/jquery.fileupload.css'),
                Array('type' => 'file', 'src' => base_url() . 'assets/admin/css/plugins/fileupload/jquery.fileupload-ui.css')
            );
        }

        $config = array(
            array('field' => 'id', 'label' => 'hotel id', 'rules' => 'required|numeric'),

            array('field' => 'enName', 'label' => 'Name [en]', 'rules' => 'max_length[50]'),
            array('field' => 'duName', 'label' => 'Name [de]', 'rules' => 'max_length[50]'),
            array('field' => 'itName', 'label' => 'Name [it]', 'rules' => 'required|max_length[50]'),

            array('field' => 'enDescription', 'label' => 'Description [en]', 'rules' => 'max_length[50000]'),
            array('field' => 'duDescription', 'label' => 'Description [de]', 'rules' => 'max_length[50000]'),
            array('field' => 'itDescription', 'label' => 'Description [it]', 'rules' => 'required|max_length[50000]'),

            array('field' => 'price', 'label' => 'Price', 'rules' => 'required|numeric'),
            array('field' => 'unit', 'label' => 'Price Unit', 'rules' => 'required'),
            array('field'=>'url','label'=>'Web URL','rules'=>'required'),
            array('field' => 'lat', 'label' => 'Lat', 'rules' => 'decimal'),
            array('field' => 'lng', 'label' => 'Lng', 'rules' => 'decimal'),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() != FALSE && $page != '404') {
            if ($this->input->post('id') != '0') {
                $this->Hotel_model->id = $this->input->post('id');
            }
            $this->Hotel_model->enName = $this->input->post('enName')?$this->input->post('enName'):$this->input->post('itName');
            $this->Hotel_model->duName = $this->input->post('duName')?$this->input->post('duName'):$this->input->post('itName');
            $this->Hotel_model->itName = $this->input->post('itName');

            //if($this->input->post('enInsta')){
              $this->Hotel_model->enInsta = $this->input->post('enInsta');
            //}
            //if($this->input->post('fbWidget')){
              $this->Hotel_model->fbWidget = $this->input->post('fbWidget');
            //}
            //if($this->input->post('tripWidget')){
                $this->Hotel_model->tripWidget = $this->input->post('tripWidget');
            //}

            $this->Hotel_model->enDescription = $this->input->post('enDescription')?$this->input->post('enDescription'):$this->input->post('itDescription');
            $this->Hotel_model->duDescription = $this->input->post('duDescription')?$this->input->post('duDescription'):$this->input->post('itDescription');
            $this->Hotel_model->itDescription = $this->input->post('itDescription');

            $this->Hotel_model->price = $this->input->post('price');
            $this->Hotel_model->unit = $this->input->post('unit');
            $this->Hotel_model->order_id = $id;
            $this->Hotel_model->url = $this->input->post('url');
            $this->Hotel_model->categories = $this->input->post('selected_categories');
            $this->Hotel_model->lat = $this->input->post('lat');
            $this->Hotel_model->lng = $this->input->post('lng');
            $this->Hotel_model->stars = $this->input->post('stars');
            $this->Hotel_model->tags = $this->input->post('tags');
            $this->Hotel_model->activity_services = $this->input->post('activity_services');
            $this->Hotel_model->phone = $this->input->post('phone');
            $this->Hotel_model->manager = $this->input->post('manager');

            //echo "<pre>"; print_r($_FILES); die;

            if ($_FILES && $_FILES['m_image']['name'])
            {
                $config['upload_path'] = './uploads/hotels/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '5000';
                $new_name = time() . $_FILES["m_image"]['name'];
                $config['file_name'] = $new_name;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('m_image'))
                {
                    $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                        'type' => 'danger'));
                    redirect('customer/hotels/edit/' . $id);
                }
                else
                {

                    $this->Hotel_model->manager_profile = $new_name;
                }
            }

            $this->Hotel_model->update();
            $this->session->set_flashdata(Array('msg' => 'Hai aggiornato con successo le informazioni dell\'attività',
                'type' => 'success'));
            redirect('customer/hotels/edit/' . $id);
        } else {
            $data['msg'] = $this->session->flashdata('msg');
            $data['type'] = $this->session->flashdata('type');
            $data['usertype'] = $this->session->userdata('usertype');
            $data['cpage'] = 'hotels';
            if($this->session->userdata('usertype') == 'admin'){
              $this->load->model('Categories');
              $data['all_categories'] = $this->Categories->getAllList();
              $this->load->view('admin/includes/header', $data);
              $this->load->view('admin/includes/sidebar', $data);
              $this->load->view($page, $data);
              $this->load->view('admin/includes/footer');
            }
            else {
              $this->load->view('customer/includes/header', $data);
            	 $this->load->view('customer/includes/sidebar', $data);
            	 $this->load->view($page, $data);
            	 $this->load->view('customer/includes/footer', $data);
            }
        }
    }

    public function upload_image($id = 0) {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Orders_model');
        $this->Orders_model->load($id);
        $this->load->model('Hotel_model');
        $this->Hotel_model->loadOnOrderId($id);
        if (($this->Orders_model->customer_id != $this->session->userdata('id') && 'admin' != $this->session->userdata('usertype')) || !$this->Hotel_model->id) {
             $this->session->set_flashdata(Array('msg' => 'Non hai ancora inserito le informazioni dell\'attività',
                    'type' => 'error'));
                redirect('customer/hotels/edit/' . $id);
        }
        if ($_FILES && $_FILES['p_image']['name']) {
            $config['upload_path'] = './uploads/hotels/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '5000';
            $new_name = time() . $_FILES["p_image"]['name'];
            $config['file_name'] = $new_name;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('p_image')) {
                $this->session->set_flashdata(Array('msg' => $this->upload->display_errors(),
                    'type' => 'danger'));
                redirect('customer/hotels/edit/' . $id);
            } else {
                $this->Hotel_model->update_photo($new_name);
                $this->session->set_flashdata(Array('msg' => 'Hai caricato con successo l\'immagine primaria!',
                    'type' => 'success', 'model' => 'login'));
                redirect('customer/hotels/edit/' . $id);
            }
        } else {
            $this->session->set_flashdata(Array('msg' => 'Per favore seleziona l\'immagine da caricare',
                    'type' => 'danger'));
                redirect('customer/hotels/edit/' . $id);
        }
    }

    public function fileupload($id) {
        error_reporting(E_ALL | E_STRICT);
        $path = 'uploads/hotels/'.$id;
        if (!file_exists($path)) {
            mkdir($path);
        }
        $params = array("upload_dir" => '/' . $path . '/', "upload_url" => '/' . $path . '/');
        $this->load->library("uploadHandler", $params);
    }
}
