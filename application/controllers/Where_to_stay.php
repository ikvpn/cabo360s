<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Where_to_stay extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');

        $this->load->model('Menu_model');
        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());

        $this->load->model('admin/Location_model');
        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model("Places_of_interest_model");
        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
        $this->load->model('admin/Ads_model');
        $this->load->model('Orders_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Procida_model');
        $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');
        $this->load->model('Service_model');
        $this->load->model('Agencies_model');
        $this->load->model('Cart_model');
        $this->load->model('Apartment_model');
        $this->load->model('Bed_and_breakfast_model');
        $this->load->model('Hotel_model');

        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');
        $this->load->model('admin/Block_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['bed-and-breakfast'] = $this->Bed_and_breakfast_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();
        $this->data['counts']['escursions-and-visit'] = $this->Service_model->totalActiveServices();
        $this->data['counts']['agencies'] = $this->Agencies_model->totalActiveServices();

        $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3, $this->lang->lang());
    }

    public function index($page = 'hotels', $link = '') {
        switch ($page) {
            case 'hotels':
                $this->hotel_listing();
                break;
            case 'hotel-details':
                $this->hotel_detail($link);
                break;
            case 'holiday-houses':
                $this->holiday_houses();
                break;
            case 'case-vacanza':
                $this->holiday_houses();
                break;
            case 'bed-and-breakfast':
                $this->bed_and_breakfast($link);
                break;
            case 'holiday-house':
                $this->holiday_house($link);
                break;
            default:
                $data = $this->data;

                $this->load->helper('cookie');

                $this->load->model('Procida_model');
                $cartCookie = $this->input->cookie('prcrt');

                if(!empty($cartCookie)){
                  $items = $this->Cart_model->getUserCart($cartCookie, '');
                  $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
                  $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
                  $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
                  if(!empty($tours)){
                    for ($i=0; $i < count($tours); $i++) {
                      $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
                    }
                  }
                } else {
                  $items = [];
                  $hotelCart = [];
                  $tranferCart = [];
                  $tours = []; // show random tours
                }
                	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

                $this->load->view('frontend/includes/header', $data);
                $this->load->view('frontend/content/' . $page, $data);
                $this->load->view('frontend/includes/footer', $data);
                break;
        }
    }

	private function hotel_listing() {
      $this->lang->load('Hotelspage_lang');
      $this->load->model('HotelBB_model');
      $this->load->library('googlemaps');



      $config['center'] = '40.7569952, 14.0120653';
      $config['zoom'] = 14;
      $config['map_height'] = '550px';
      $config['onload'] = 'onLoadMap();';
      $config['backgroundColor'] = 'transparent';

      $this->googlemaps->initialize($config);

      $hotels = $this->Hotel_model->getActiveHotels();
      shuffle($hotels);

      $fullHotels = array();

      // $infoWindowHTML = '<div class="mainInfoWindow"></div>';

      for ($i=0; $i < count($hotels); $i++) {

        if($hotels[$i]->hotel_bb_id != NULL) {
          $hotels[$i]->hbb_info = $this->HotelBB_model->getHbbInfo($hotels[$i]->hotel_bb_id);
          $hotels[$i]->image = $this->HotelBB_model->getHotelImages($hotels[$i]->hotel_bb_id);
        }

        if($hotels[$i]->lat && $hotels[$i]->lng){
          $marker = array();
          $marker['position'] = $hotels[$i]->lat.', '.$hotels[$i]->lng;
          // $marker['infowindow_content'] = "$infoWindowHTML";
          $marker['icon'] = '/assets/images/hotels.png';
          $this->googlemaps->add_marker($marker);
        }

        array_push($fullHotels, $hotels[$i]);
      }
      $data = $this->data;
      $data['fullHotels'] = $fullHotels;
      $data['map'] = $this->googlemaps->create_map();

      $this->load->helper('cookie');

    $this->load->model('Procida_model');
    $cartCookie = $this->input->cookie('prcrt');

    if(!empty($cartCookie)){
      $items = $this->Cart_model->getUserCart($cartCookie, '');
      $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
      $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
      $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
      if(!empty($tours)){
        for ($i=0; $i < count($tours); $i++) {
          $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
        }
      }
    } else {
      $items = [];
      $hotelCart = [];
      $tranferCart = [];
      $tours = []; // show random tours
    }
			$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

      switch ($this->lang->lang()) {
          case 'en':
              $data['meta_keywords'] = 'procida, hotels, rooms, offers, reservations, accommodation, lodging, hospitality';
              $data['meta_desc'] = 'Hotels Procida: 4-stars hotels, 3 stars hotels in exclusive locations by the sea in Procida Island Italy. Book online your hotel in Procida';
              $data['meta_title'] = 'Hotel Procida | Hotels and B&Bs on Procida Island, Italy';
              break;
          case 'de':
              $data['meta_keywords'] = 'Hotels, Zimmer, Apartments, Bed, Breakfast, Procida';
              $data['meta_desc'] = 'Procida Hotels Hotels und Hotels an der See oder in der grünen Insel: 4 Sterne, 3 Sterne, Bed & Breakfast in außergewöhnlich.';
              $data['meta_title'] = 'Hotel Procida | Hotels und Pensionen auf der Insel Procida, Italien';
              break;
          case 'it':
              $data['meta_keywords'] = 'Hotel, alberghi, camere, residence, bed, breakfast, procida';
              $data['meta_desc'] = 'Hotel Procida: Hotels e alberghi sul mare o immersi nel verde dell\'isola: 4 stelle, 5 stelle, 3 stelle, Bed & Breakfast in località esclusive.';
              $data['meta_title'] = 'Hotel Procida: Dove dormire a Procida in hotels, residences e B&B';
              break;
      }

      $this->load->view('frontend/includes/header', $data);
      $this->load->view('frontend/content/hotel-listing-map', $data);
      $this->load->view('frontend/includes/footer', $data);
    }

    private function hotel_detail($page_id) {
      $this->load->model('HotelBB_model');
      $this->load->model('Procida_model');
      $this->lang->load('Hotelspage_lang');
      $hotel = $this->HotelBB_model->findOne($page_id);
      $data = $this->data;

      if($hotel->hotel_bb_id != NULL) {
        $hotel->hbb_info = $this->HotelBB_model->getHbbInfo($hotel->hotel_bb_id);
        $hotel->image = $this->HotelBB_model->getHotelImages($hotel->hotel_bb_id);
      }
      $data['fullHotel'] = $hotel;

      $temp = (array) $hotel;


      $data['near_locations'] = $this->Location_model->getLocations(false, $this->lang->lang());

      $data['cssfiles'] = array(
          array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
          array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
      );

      if($data['fullHotel']->lat === NULL || $data['fullHotel']->lng === NULL){
        $data['fullHotel']->lat = 0;
        $data['fullHotel']->lng = 0;
      }

      //$data['places'] = $this->Location_model->getPlaces();

      $places = $this->Procida_model->getPlacesForSideBar($data['fullHotel']->lat,$data['fullHotel']->lng);

      foreach ($places as  $place) {
        # code...
        $place->itDescription = substr(strip_tags(base64_decode($place->itDescription)), 0, 150);
        $place->itDescription = $place->itDescription.'...';

        $place->enDescription = substr(strip_tags(base64_decode($place->enDescription)), 0, 150);
        $place->enDescription = $place->enDescription.'...';
      }

		  $data['places'] = $places;

      $data['jsfiles'] = array(
        array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
        array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
        array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js"),
        array('type' => 'file', 'src' => base_url() . "assets/js/roomDetail.js"),
        array('type' => 'file', 'src' => base_url() . "assets/js/NearbyMap.js"),
        array('type' => 'inline', 'src' => '$(document).ready(function(){ NearbyMap.init('.$data["fullHotel"]->lat.','.$data["fullHotel"]->lng.'); });')
      );
		$this->load->helper('cookie');

    $this->load->model('Procida_model');
    $cartCookie = $this->input->cookie('prcrt');

    if(!empty($cartCookie)){
      $items = $this->Cart_model->getUserCart($cartCookie, '');
      $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
      $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
      $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
      if(!empty($tours)){
        for ($i=0; $i < count($tours); $i++) {
          $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
        }
      }
    } else {
      $items = [];
      $hotelCart = [];
      $tranferCart = [];
      $tours = []; // show random tours
    }

    $this->load->model('admin/Metadata_business_model');
    $metadata = $this->Metadata_business_model->getByOrderID($hotel->order_id);


      if($metadata != NULL) {
        switch ($this->lang->lang()) {

            case 'en':
               $data['meta_keywords'] = $metadata->meta_keywords_en;
               $data['meta_desc'] = $metadata->meta_desc_en;
               $data['meta_title'] = $metadata->meta_title_en;
               break;
           case 'it':
               $data['meta_keywords'] = $metadata->meta_keywords_it;
               $data['meta_desc'] = $metadata->meta_desc_it;
               $data['meta_title'] = $metadata->meta_title_it;
               break;
           case 'de':
               $data['meta_keywords'] = $metadata->meta_keywords_du;
               $data['meta_desc'] = $metadata->meta_desc_du;
               $data['meta_title'] = $metadata->meta_title_du;
               break;

          }
      }
      else {
        $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
        $data['meta_desc'] = strip_tags(base64_decode( $temp[$this->lang->lang() . 'Description']));
      }

			$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );
      // echo "<pre>"; print_r($data['places']); die;

      $this->load->view('frontend/includes/header', $data);
      $this->load->view('frontend/content/hotel-detail', $data);
      $this->load->view('frontend/includes/footer', $data);
    }

    private function holiday_houses() {
        $data = $this->data;

        $this->load->library('pagination');
        $config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 4;
        $config['per_page'] = 10;

        $v_config = array(
            array('field' => 'rooms',
                'label' => 'Rooms',
                'rules' => 'numeric'),
            array('field' => 'beds',
                'label' => 'Beds',
                'rules' => 'numeric'),
            array('field' => 'minPrice',
                'label' => 'Min Price',
                'rules' => 'numeric'),
            array('field' => 'maxPrice',
                'label' => 'Max Price',
                'rules' => 'numeric'),
            array('field' => 'pax',
                'label' => 'Pax',
                'rules' => 'numeric'),
            array('field' => 'locations',
                'label' => 'Locations',
                'rules' => ''),
        );
        $this->form_validation->set_rules($v_config);

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

          switch ($this->lang->lang()) {
              case 'en':
                  $data['meta_keywords'] = 'procida, hotels, rooms, offers, reservations, accommodation, lodging, hospitality';
                  $data['meta_desc'] = 'Holiday houses in Procida: apartments and villas, gates for rent in in exclusive locations in Procida';
                  $data['meta_title'] = 'Holiday houses, villas and apartments in Procida';
                  break;
              case 'de':
                  $data['meta_keywords'] = 'Hotels, Zimmer, Apartments, Bed, Breakfast, Procida';
                  $data['meta_desc'] = 'Holiday houses in Procida: apartments and villas, gates for rent in in exclusive locations in Procida';
                  $data['meta_title'] = 'Holiday houses, villas and apartments in Procida';
                  break;
              case 'it':
                  $data['meta_keywords'] = 'Hotel, alberghi, camere, residence, bed, breakfast, procida';
                  $data['meta_desc'] = 'Case vacanza Procida: affitto appartamenti e ville in zone esclusive per vacanze a Procida';
                  $data['meta_title'] = 'Case vacanza, ville ed appartamenti a Procida';
                  break;
          }

        $data['blocks'] = $this->Block_model->getBlocks(4,$this->lang->lang());

        if ($this->form_validation->run() !== FALSE) {
            $config['total_rows'] = $this->Apartment_model->totalCustomActiveApartments();
            $this->pagination->initialize($config);
            $data['holiday_houses'] = $this->Apartment_model->getCustomActiveApartments($this->lang->lang());
            $this->lang->load('Holidayhousespage');
            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/holiday_houses', $data);
            $this->load->view('frontend/includes/footer', $data);
        } else {
            $config['total_rows'] = $this->Apartment_model->totalActiveApartments();
            $this->pagination->initialize($config);
            $data['holiday_houses'] = $this->Apartment_model->getActiveApartments($this->lang->lang());
            shuffle($data['holiday_houses']);
            $this->lang->load('Holidayhousespage');
            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/holiday_houses', $data);
            $this->load->view('frontend/includes/footer', $data);
        }
    }

    private function holiday_house($page_id) {
        $data = $this->data;
        $this->Apartment_model->loadActivePage($page_id);
        if (!$this->Apartment_model->id) {
            show_404();
            return;
        }

        $temp = (array) $this->Apartment_model;

        $data['jsfiles'] = array(
              array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$this->Apartment_model->lat."; var CUR_LNG=".$this->Apartment_model->lng.';'),
              array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
        );
        // var_dump($this->apartment_model);return;
        $this->lang->load('Holidayhousespage');

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = [];
        }
        $this->load->model('admin/Metadata_business_model');
        $metadata = $this->Metadata_business_model->getByOrderID($temp['order_id']);


          if($metadata != NULL) {
            switch ($this->lang->lang()) {

                case 'en':
                   $data['meta_keywords'] = $metadata->meta_keywords_en;
                   $data['meta_desc'] = $metadata->meta_desc_en;
                   $data['meta_title'] = $metadata->meta_title_en;
                   break;
               case 'it':
                   $data['meta_keywords'] = $metadata->meta_keywords_it;
                   $data['meta_desc'] = $metadata->meta_desc_it;
                   $data['meta_title'] = $metadata->meta_title_it;
                   break;
               case 'de':
                   $data['meta_keywords'] = $metadata->meta_keywords_du;
                   $data['meta_desc'] = $metadata->meta_desc_du;
                   $data['meta_title'] = $metadata->meta_title_du;
                   break;

              }
          }
          else {
            $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
            $data['meta_desc'] = strip_tags($temp[$this->lang->lang() . 'Description']);
          }

        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

          $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
          $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
          $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
          $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
          $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
          $data['blocks'] = $this->Block_model->getBlocks(4,$this->lang->lang());
          
        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/accomodation_detail', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function bed_and_breakfast($page_id){
        if(!$page_id){
            $data = $this->data;
            $data['bb'] = true;

            $this->load->library('pagination');
            $config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
            $config['full_tag_close'] = "</ul>";
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";
            $config['use_page_numbers'] = TRUE;
            $config['uri_segment'] = 4;
            $config['per_page'] = 10;

            $v_config = array(
                array('field' => 'rooms',
                    'label' => 'Rooms',
                    'rules' => 'numeric'),
                array('field' => 'beds',
                    'label' => 'Beds',
                    'rules' => 'numeric'),
                array('field' => 'minPrice',
                    'label' => 'Min Price',
                    'rules' => 'numeric'),
                array('field' => 'maxPrice',
                    'label' => 'Max Price',
                    'rules' => 'numeric'),
                array('field' => 'pax',
                    'label' => 'Pax',
                    'rules' => 'numeric'),
                array('field' => 'locations',
                    'label' => 'Locations',
                    'rules' => ''),
            );
            $this->form_validation->set_rules($v_config);

            $this->load->helper('cookie');

            $this->load->model('Procida_model');
            $cartCookie = $this->input->cookie('prcrt');

            if(!empty($cartCookie)){
                $items = $this->Cart_model->getUserCart($cartCookie, '');
                $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
                $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
                $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
                if(!empty($tours)){
                    for ($i=0; $i < count($tours); $i++) {
                        $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
                    }
                }
            } else {
                $items = [];
                $hotelCart = [];
                $tranferCart = [];
                $tours = []; // show random tours
            }
            $data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

            switch ($this->lang->lang()) {
                case 'en':
                    $data['meta_keywords'] = 'procida, hotels, rooms, offers, reservations, accommodation, lodging, hospitality';
                    $data['meta_desc'] = 'Holiday houses in Procida: apartments and villas, gates for rent in in exclusive locations in Procida';
                    $data['meta_title'] = 'Holiday houses, villas and apartments in Procida';
                    break;
                case 'de':
                    $data['meta_keywords'] = 'Hotels, Zimmer, Apartments, Bed, Breakfast, Procida';
                    $data['meta_desc'] = 'Holiday houses in Procida: apartments and villas, gates for rent in in exclusive locations in Procida';
                    $data['meta_title'] = 'Holiday houses, villas and apartments in Procida';
                    break;
                case 'it':
                    $data['meta_keywords'] = 'Hotel, alberghi, camere, residence, bed, breakfast, procida';
                    $data['meta_desc'] = 'Case vacanza Procida: affitto appartamenti e ville in zone esclusive per vacanze a Procida';
                    $data['meta_title'] = 'Case vacanza, ville ed appartamenti a Procida';
                    break;
            }

            if ($this->form_validation->run() !== FALSE) {
                $config['total_rows'] = $this->Bed_and_breakfast_model->totalCustomActiveApartments();
                $this->pagination->initialize($config);
                $data['holiday_houses'] = $this->Bed_and_breakfast_model->getCustomActiveApartments($this->lang->lang());
                $this->lang->load('Holidayhousespage');
                $this->load->view('frontend/includes/header', $data);
                $this->load->view('frontend/content/holiday_houses', $data);
                $this->load->view('frontend/includes/footer', $data);
            } else {
                $config['total_rows'] = $this->Bed_and_breakfast_model->totalActiveApartments();
                $this->pagination->initialize($config);
                $data['holiday_houses'] = $this->Bed_and_breakfast_model->getActiveApartments($this->lang->lang());
                shuffle($data['holiday_houses']);
                $this->lang->load('Holidayhousespage');
                $this->load->view('frontend/includes/header', $data);
                $this->load->view('frontend/content/holiday_houses', $data);
                $this->load->view('frontend/includes/footer', $data);
            }
        }
        else {
            /* SINGLE */
            $data = $this->data;
            $data['bb'] = true;
            $this->Bed_and_breakfast_model->loadActivePage($page_id);
            if (!$this->Bed_and_breakfast_model->id) {
                show_404();
                return;
            }
            $temp = (array) $this->Bed_and_breakfast_model;

            $data['jsfiles'] = array(
                array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$this->Bed_and_breakfast_model->lat."; var CUR_LNG=".$this->Bed_and_breakfast_model->lng.';'),
                array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
            );
            // var_dump($this->apartment_model);return;
            $this->lang->load('Holidayhousespage');

            $this->load->helper('cookie');

            $this->load->model('Procida_model');
            $cartCookie = $this->input->cookie('prcrt');

            if(!empty($cartCookie)){
                $items = $this->Cart_model->getUserCart($cartCookie, '');
                $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
                $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
                $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
                if(!empty($tours)){
                    for ($i=0; $i < count($tours); $i++) {
                        $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
                    }
                }
            } else {
                $items = [];
                $hotelCart = [];
                $tranferCart = [];
                $tours = [];
            }
            $this->load->model('admin/Metadata_business_model');
            $metadata = $this->Metadata_business_model->getByOrderID($temp['order_id']);


            if($metadata != NULL) {
                switch ($this->lang->lang()) {

                    case 'en':
                        $data['meta_keywords'] = $metadata->meta_keywords_en;
                        $data['meta_desc'] = $metadata->meta_desc_en;
                        $data['meta_title'] = $metadata->meta_title_en;
                        break;
                    case 'it':
                        $data['meta_keywords'] = $metadata->meta_keywords_it;
                        $data['meta_desc'] = $metadata->meta_desc_it;
                        $data['meta_title'] = $metadata->meta_title_it;
                        break;
                    case 'de':
                        $data['meta_keywords'] = $metadata->meta_keywords_du;
                        $data['meta_desc'] = $metadata->meta_desc_du;
                        $data['meta_title'] = $metadata->meta_title_du;
                        break;

                }
            }
            else {
                $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
                $data['meta_desc'] = strip_tags($temp[$this->lang->lang() . 'Description']);
            }

            $data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

            $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);

            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/bb_detail', $data);
            $this->load->view('frontend/includes/footer', $data);

        }

    }

}
