<?php


if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class What_to_do extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->model('admin/Location_model');
        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model("Places_of_interest_model");
        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');
        $this->load->model('admin/Ads_model');
        $this->load->model('Orders_model');
        $this->load->model('Procida_model');
        $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');
        $this->load->model('Service_model');
        $this->load->model('Agencies_model');
        $this->load->model('Cart_model');
        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');

        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();
        $this->data['counts']['escursions-and-visit'] = $this->Service_model->totalActiveServices();
        $this->data['counts']['agencies'] = $this->Agencies_model->totalActiveServices();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();

        $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());
    }

    public function index($page = '', $single = '') {
        switch ($page) {
            case 'shopping':
                $this->shopping_listing($single);
                break;
            case 'rentals':
                $this->boat_rental($single);
                break;
            case 'wedding':
                $this->wedding($single);
                break;
            case 'beach-clubs':
            case 'stabilimenti-balneari':
                $this->beach_clubs($single);
                break;
            case 'escursions-and-visit':
            case 'escursioni-e-visite':
              $this->services($single);
              break;
            case 'agenzie':
            case 'agencies':
               $this->agencies($single);
               break;
            default:
                show_404();
                break;
        }
    }

    private function boat_rental($single) {
          if($single !=''){
           $this->boat_rental_details($single);
           return;
        }
        $data = $this->data;
        $this->lang->load('Boatrental');
        $this->load->library('pagination');
        $config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $config['per_page'] = 10;

        $data['jsfiles'] = array(
                  array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= 0; var CUR_LNG=0;"),
                  array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
                  array('type' => 'file', 'src' => base_url() . "assets/js/filter_list.js")
            );
        $this->load->model('Procida_model');

        $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);
        $data['restaurants'] = $this->Boatrentals_model->getActiveBoatRentals($this->lang->lang());
        shuffle($data['restaurants']);

        $this->load->model('Categories');
        $others = $this->Categories->getActivityByCategory(8, $this->lang->lang());
        $data['othersActivity'] = $others;
        shuffle($data['othersActivity']);

        $this->load->model('admin/Tag_model');
        $this->load->model('admin/Activity_service_model');
        $all_tags = array();
        $all_services = array();

        foreach ($data['restaurants'] as $key => $activity) {

          $tags = $activity->tags;
          if($tags != ''){
            $tags = explode(',', $tags);
            foreach ($tags as $key => $tagID) {
              $tag = $this->Tag_model->get($tagID);
              if(!in_array($tag, $all_tags)){
                array_push($all_tags, $tag);
              }
            }
          }

          $services = $activity->activity_services;
          if($services != ''){
            $services = explode(',', $services);
            foreach ($services as $key => $serviceID) {
              $service = $this->Activity_service_model->get($serviceID);
              if(!in_array($service, $all_services)){
                array_push($all_services, $service);
              }
            }
          }
        }
        foreach ($data['othersActivity'] as $key => $activity) {

          $tags = $activity->tags;
          if($tags != ''){
            $tags = explode(',', $tags);
            foreach ($tags as $key => $tagID) {
              $tag = $this->Tag_model->get($tagID);
              if(!in_array($tag, $all_tags)){
                array_push($all_tags, $tag);
              }
            }
          }

          $services = $activity->activity_services;
          if($services != ''){
            $services = explode(',', $services);
            foreach ($services as $key => $serviceID) {
              $service = $this->Activity_service_model->get($serviceID);
              if(!in_array($service, $all_services)){
                array_push($all_services, $service);
              }
            }
          }
        }
        $data['all_tags'] = $all_tags;
        $data['all_services'] = $all_services;

        switch ($this->lang->lang()) {
             case 'en':
                $data['meta_keywords'] = 'rental, boat, procida';
                $data['meta_desc'] = 'Boat rentals Procida: hire boats, yachts and kayaks for tours around Procida';
                $data['meta_title'] = 'Boat rentals and excursions in Procida Island, Italy';
                break;
            case 'it':
                $data['meta_keywords'] = 'noleggio, barche, procida';
                $data['meta_desc'] = 'Noleggio barche Procida: noleggio gozzi, gommoni, yacht, kayak per ammirare Procida dal mare';
                $data['meta_title'] = 'Noleggio ed escursioni in Barca Procida';
                break;
            case 'de':
                $data['meta_keywords'] = 'Vermietung, Boot, Procida';
                $data['meta_desc'] = 'Bootsverleih Procida: Fischerboote, Jollen, Yachten, Kajaks zu bewundern Procida vom Meer mieten';
                $data['meta_title'] = 'Bootsverleih und Ausflüge in der Insel Procida, Italien';
                break;
        }
        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $this->lang->load('Restaurantspage');
        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/boatrental_listing', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function boat_rental_details($single){
        $data = $this->data;
        $this->load->model('admin/Settings_model');
        $this->lang->load('Boatrental');
        $this->lang->load('Activitypage');
        $this->Settings_model->load();
        $this->Boatrentals_model->loadActivePage($single);
        if (!$this->Boatrentals_model->id) {
            show_404();
            return;
        }
        $temp = (array) $this->Boatrentals_model;

        $business = array('activityId' => $this->Boatrentals_model->id, 'type' => 'boat_rentals');
        $this->Procida_model->addViewToBusinessPage($business);

        $data['activity'] = $this->Boatrentals_model;
        $data['activity']->uploadsFolder = 'boat_rentals';
        $data['activity']->breadcrumb_name = 'Boat_Rentals';

        $this->load->model('Categories');
        $this->load->model('admin/Tag_model');
        $this->load->model('admin/Activity_service_model');

        $activity_categories = array();
        $activity_tag = array();
        $activity_services = array();

        $categories = $data['activity']->categories;
        if($categories != ''){
          $categories = explode(',', $categories);
          foreach ($categories as $key => $value) {
            $cat = $this->Categories->get($value);
            array_push($activity_categories, $cat);
          }
        }
        $tags = $data['activity']->tags;
        if($tags != ''){
          $tags = explode(',', $tags);
          foreach ($tags as $key => $value) {
            $tag = $this->Tag_model->get($value);
            array_push($activity_tag, $tag);
          }
        }
        $services = $data['activity']->activity_services;
        if($services != ''){
          $services = explode(',', $services);
          foreach ($services as $key => $value) {
            $service = $this->Activity_service_model->get($value);
            array_push($activity_services, $service);
          }
        }
        $data['activity']->activity_categories = $activity_categories;
        $data['activity']->activity_tag = $activity_tag;
        $data['activity']->activity_services = $activity_services;

        $this->load->model('admin/Metadata_business_model');
        $metadata = $this->Metadata_business_model->getByOrderID($data['activity']->order_id);


          if($metadata != NULL) {
            switch ($this->lang->lang()) {

                case 'en':
                   $data['meta_keywords'] = $metadata->meta_keywords_en;
                   $data['meta_desc'] = $metadata->meta_desc_en;
                   $data['meta_title'] = $metadata->meta_title_en;
                   break;
               case 'it':
                   $data['meta_keywords'] = $metadata->meta_keywords_it;
                   $data['meta_desc'] = $metadata->meta_desc_it;
                   $data['meta_title'] = $metadata->meta_title_it;
                   break;
               case 'de':
                   $data['meta_keywords'] = $metadata->meta_keywords_du;
                   $data['meta_desc'] = $metadata->meta_desc_du;
                   $data['meta_title'] = $metadata->meta_title_du;
                   break;

              }
          }
          else {
            $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
            $data['meta_desc'] = $temp[$this->lang->lang() . 'Slogan'];
          }


        $this->lang->load('Restaurantspage');
        $data['facebook_sdk'] = '<div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, \'script\', \'facebook-jssdk\'));</script>';
        $data['jsfiles'] = array(
                  array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$temp['lat']."; var CUR_LNG=".$temp['lng'].";"),
                  array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
                  array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
                  array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
                  array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js")
        );
        $data['cssfiles'] = array(
            array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
            array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
        );
        $this->load->model('Procida_model');
        $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
		    $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);

       $this->load->helper('cookie');

       $this->load->model('Procida_model');
       $cartCookie = $this->input->cookie('prcrt');

       if(!empty($cartCookie)){
         $items = $this->Cart_model->getUserCart($cartCookie, '');
         $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
         $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
         $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
         if(!empty($tours)){
           for ($i=0; $i < count($tours); $i++) {
             $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
           }
         }
       } else {
         $items = [];
         $hotelCart = [];
         $tranferCart = [];
         $tours = []; // show random tours
       }
       	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/activity_page', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function beach_clubs($single) {
        $data = $this->data;
        $this->lang->load('Shopspage');
        $this->lang->load('Activitypage');
        $this->load->model('Procida_model');
        $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);
        if ($single != '') {
            $this->Beachclub_model->loadActivePage($single);
            if (!$this->Beachclub_model->id) {
                show_404();
                return;
            }
            $temp = (array) $this->Beachclub_model;

            $business = array('activityId' => $this->Beachclub_model->id, 'type' => 'beach_clubs');
            $this->Procida_model->addViewToBusinessPage($business);

            $data['activity'] = $this->Beachclub_model;
            $data['activity']->uploadsFolder = 'beach_clubs';
            $data['activity']->breadcrumb_name = 'Beach Clubs';

            $this->load->model('Categories');
            $this->load->model('admin/Tag_model');
            $this->load->model('admin/Activity_service_model');

            $activity_categories = array();
            $activity_tag = array();
            $activity_services = array();

            $categories = $data['activity']->categories;
            if($categories != ''){
              $categories = explode(',', $categories);
              foreach ($categories as $key => $value) {
                $cat = $this->Categories->get($value);
                array_push($activity_categories, $cat);
              }
            }

            $tags = $data['activity']->tags;
            if($tags != ''){
              $tags = explode(',', $tags);
              foreach ($tags as $key => $value) {
                $tag = $this->Tag_model->get($value);
                array_push($activity_tag, $tag);
              }
            }

            $services = $data['activity']->activity_services;
            if($services != ''){
              $services = explode(',', $services);
              foreach ($services as $key => $value) {
                $service = $this->Activity_service_model->get($value);
                array_push($activity_services, $service);
              }
            }

            $data['activity']->activity_categories = $activity_categories;
            $data['activity']->activity_tag = $activity_tag;
            $data['activity']->activity_services = $activity_services;

            $this->load->model('admin/Metadata_business_model');
            $metadata = $this->Metadata_business_model->getByOrderID($data['activity']->order_id);


              if($metadata != NULL) {
                switch ($this->lang->lang()) {

                    case 'en':
                       $data['meta_keywords'] = $metadata->meta_keywords_en;
                       $data['meta_desc'] = $metadata->meta_desc_en;
                       $data['meta_title'] = $metadata->meta_title_en;
                       break;
                   case 'it':
                       $data['meta_keywords'] = $metadata->meta_keywords_it;
                       $data['meta_desc'] = $metadata->meta_desc_it;
                       $data['meta_title'] = $metadata->meta_title_it;
                       break;
                   case 'de':
                       $data['meta_keywords'] = $metadata->meta_keywords_du;
                       $data['meta_desc'] = $metadata->meta_desc_du;
                       $data['meta_title'] = $metadata->meta_title_du;
                       break;

                  }
              }
              else {
                $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
                $data['meta_desc'] = $temp[$this->lang->lang() . 'Slogan'];
              }

            $this->lang->load('Restaurantspage');
            $data['facebook_sdk'] = '<div id="fb-root"></div>
              <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, \'script\', \'facebook-jssdk\'));</script>';

            $data['jsfiles'] = array(
                  array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$temp['lat']."; var CUR_LNG=".$temp['lng'].";"),
                  array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
                array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
              array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
              array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js")
            );
            $data['cssfiles'] = array(
                array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
                array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
            );
            $this->load->model('Procida_model');
             $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
             $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
      		    $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);

            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/activity_page', $data);
            $this->load->view('frontend/includes/footer', $data);
        } else {

            $data['jsfiles'] = array(
                array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= 0; var CUR_LNG=0;"),
                array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
                array('type' => 'file', 'src' => base_url() . "assets/js/filter_list.js")
                );
            $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);
            $data['shops'] = $this->Beachclub_model->getActiveShops($this->lang->lang());
            shuffle($data['shops']);

            $this->load->model('Categories');
            $others = $this->Categories->getActivityByCategory(10, $this->lang->lang());
            $data['othersActivity'] = $others;
            shuffle($data['othersActivity']);

            $this->load->model('admin/Tag_model');
            $this->load->model('admin/Activity_service_model');
            $all_tags = array();
            $all_services = array();

            foreach ($data['shops'] as $key => $activity) {

              $tags = $activity->tags;
              if($tags != ''){
                $tags = explode(',', $tags);
                foreach ($tags as $key => $tagID) {
                  $tag = $this->Tag_model->get($tagID);
                  if(!in_array($tag, $all_tags)){
                    array_push($all_tags, $tag);
                  }
                }
              }

              $services = $activity->activity_services;
              if($services != ''){
                $services = explode(',', $services);
                foreach ($services as $key => $serviceID) {
                  $service = $this->Activity_service_model->get($serviceID);
                  if(!in_array($service, $all_services)){
                    array_push($all_services, $service);
                  }
                }
              }
            }
            foreach ($data['othersActivity'] as $key => $activity) {

              $tags = $activity->tags;
              if($tags != ''){
                $tags = explode(',', $tags);
                foreach ($tags as $key => $tagID) {
                  $tag = $this->Tag_model->get($tagID);
                  if(!in_array($tag, $all_tags)){
                    array_push($all_tags, $tag);
                  }
                }
              }

              $services = $activity->activity_services;
              if($services != ''){
                $services = explode(',', $services);
                foreach ($services as $key => $serviceID) {
                  $service = $this->Activity_service_model->get($serviceID);
                  if(!in_array($service, $all_services)){
                    array_push($all_services, $service);
                  }
                }
              }
            }
            $data['all_tags'] = $all_tags;
            $data['all_services'] = $all_services;

            switch ($this->lang->lang()) {
                  case 'en':
                    $data['meta_keywords'] = 'Beach, clubs, Procida';
                    $data['meta_desc'] = 'Best beach clubs and Solarium in Procida: discover where to spend your day at the beach';
                    $data['meta_title'] = 'Beach clubs and Solarium in Procida';
                    break;
                case 'it':
                    $data['meta_keywords'] = '';
                    $data['meta_desc'] = 'I migliori Stabilimenti balneari, Lidi e Solarium a Procida: scopri dove trascorrere la tua giornata al mare a Procida ';
                    $data['meta_title'] = 'Stabilimenti balneari, Lidi e Solarium a Procida';
                    break;
                case 'de':
                    $data['meta_keywords'] = '';
                    $data['meta_desc'] = 'Die besten Strandclubs und Solarium in Procida: entdecken, wo Sie den Tag am Strand Procida zu verbringen';
                    $data['meta_title'] = 'Beach Clubs und Solarium in Procida';
                    break;
            }

			    $this->load->helper('cookie');

          $this->load->model('Procida_model');
          $cartCookie = $this->input->cookie('prcrt');

          if(!empty($cartCookie)){
            $items = $this->Cart_model->getUserCart($cartCookie, '');
            $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
            $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
            $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
            if(!empty($tours)){
              for ($i=0; $i < count($tours); $i++) {
                $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
              }
            }
          } else {
            $items = [];
            $hotelCart = [];
            $tranferCart = [];
            $tours = []; // show random tours
          }
				$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/beachclub_listing', $data);
            $this->load->view('frontend/includes/footer', $data);
        }
    }

    private function services($single) {
        $data = $this->data;
        $this->lang->load('Servicepage');
        $this->lang->load('Activitypage');
        $this->load->model('Procida_model');
        $this->load->model('Service_Model');
        $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        if ($single != '') {
            $this->Service_Model->loadActivePage($single);
            if (!$this->Service_Model->id) {
                show_404();
                return;
            }
            $temp = (array) $this->Service_Model;

            $business = array('activityId' => $this->Service_Model->id, 'type' => 'services');
            $this->Procida_model->addViewToBusinessPage($business);

            $data['activity'] = $this->Service_Model;
            $data['activity']->uploadsFolder = 'services';
            $data['activity']->breadcrumb_name = 'services';

            $this->load->model('Categories');
            $this->load->model('admin/Tag_model');
            $this->load->model('admin/Activity_service_model');

            $activity_categories = array();
            $activity_tag = array();
            $activity_services = array();

            $categories = $data['activity']->categories;
            if($categories != ''){
              $categories = explode(',', $categories);
              foreach ($categories as $key => $value) {
                $cat = $this->Categories->get($value);
                array_push($activity_categories, $cat);
              }
            }

            $tags = $data['activity']->tags;
            if($tags != ''){
              $tags = explode(',', $tags);
              foreach ($tags as $key => $value) {
                $tag = $this->Tag_model->get($value);
                array_push($activity_tag, $tag);
              }
            }

            $services = $data['activity']->activity_services;
            if($services != ''){
              $services = explode(',', $services);
              foreach ($services as $key => $value) {
                $service = $this->Activity_service_model->get($value);
                array_push($activity_services, $service);
              }
            }

            $data['activity']->activity_categories = $activity_categories;
            $data['activity']->activity_tag = $activity_tag;
            $data['activity']->activity_services = $activity_services;

            $this->load->model('admin/Metadata_business_model');
            $metadata = $this->Metadata_business_model->getByOrderID($data['activity']->order_id);


              if($metadata != NULL) {
                switch ($this->lang->lang()) {

                    case 'en':
                       $data['meta_keywords'] = $metadata->meta_keywords_en;
                       $data['meta_desc'] = $metadata->meta_desc_en;
                       $data['meta_title'] = $metadata->meta_title_en;
                       break;
                   case 'it':
                       $data['meta_keywords'] = $metadata->meta_keywords_it;
                       $data['meta_desc'] = $metadata->meta_desc_it;
                       $data['meta_title'] = $metadata->meta_title_it;
                       break;
                   case 'de':
                       $data['meta_keywords'] = $metadata->meta_keywords_du;
                       $data['meta_desc'] = $metadata->meta_desc_du;
                       $data['meta_title'] = $metadata->meta_title_du;
                       break;

                  }
              }
              else {
                $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
                $data['meta_desc'] = $temp[$this->lang->lang() . 'Slogan'];
              }

            $data['facebook_sdk'] = '<div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, \'script\', \'facebook-jssdk\'));</script>';
            //
            // $data['cssfiles'] = array('type'=>'file','src'=>'//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
            $data['jsfiles'] = array(
              array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$temp['lat']."; var CUR_LNG=".$temp['lng'].";"),
              array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
              array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
              array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
              array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js")
        );
        $data['cssfiles'] = array(
            array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
            array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
        );
            $this->load->model('Procida_model');
            $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
		        $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            // var_dump($data['resturants']);exit();
            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/activity_page', $data);
            $this->load->view('frontend/includes/footer', $data);
        } else {

            $data['jsfiles'] = array(
                array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= 0; var CUR_LNG=0;"),
                array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
                array('type' => 'file', 'src' => base_url() . "assets/js/filter_list.js")
                );
            $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);

            $data['shops'] = $this->Service_Model->getActiveServices($this->lang->lang());
            shuffle($data['shops']);

            $this->load->model('Categories');
            $others = $this->Categories->getActivityByCategory(12, $this->lang->lang());
            $data['othersActivity'] = $others;
            shuffle($data['othersActivity']);

            $this->load->model('admin/Tag_model');
            $this->load->model('admin/Activity_service_model');
            $all_tags = array();
            $all_services = array();

            foreach ($data['shops'] as $key => $activity) {

              $tags = $activity->tags;
              if($tags != ''){
                $tags = explode(',', $tags);
                foreach ($tags as $key => $tagID) {
                  $tag = $this->Tag_model->get($tagID);
                  if(!in_array($tag, $all_tags)){
                    array_push($all_tags, $tag);
                  }
                }
              }

              $services = $activity->activity_services;
              if($services != ''){
                $services = explode(',', $services);
                foreach ($services as $key => $serviceID) {
                  $service = $this->Activity_service_model->get($serviceID);
                  if(!in_array($service, $all_services)){
                    array_push($all_services, $service);
                  }
                }
              }
            }
            foreach ($data['othersActivity'] as $key => $activity) {

              $tags = $activity->tags;
              if($tags != ''){
                $tags = explode(',', $tags);
                foreach ($tags as $key => $tagID) {
                  $tag = $this->Tag_model->get($tagID);
                  if(!in_array($tag, $all_tags)){
                    array_push($all_tags, $tag);
                  }
                }
              }

              $services = $activity->activity_services;
              if($services != ''){
                $services = explode(',', $services);
                foreach ($services as $key => $serviceID) {
                  $service = $this->Activity_service_model->get($serviceID);
                  if(!in_array($service, $all_services)){
                    array_push($all_services, $service);
                  }
                }
              }
            }
            $data['all_tags'] = $all_tags;
            $data['all_services'] = $all_services;

            switch ($this->lang->lang()) {
                  case 'en':
                    $data['meta_keywords'] = 'Escursion, tours, Procida';
                    $data['meta_desc'] = 'Excursions and Tours in Procida';
                    $data['meta_title'] = 'Excursions and Tours in Procida';
                    break;
                case 'it':
                    $data['meta_keywords'] = 'Escursioni, Visite, Procida';
                    $data['meta_desc'] = 'Escursioni e Visite a Procida';
                    $data['meta_title'] = 'Escursioni e Visite a Procida';
                    break;
                case 'de':
                    $data['meta_keywords'] = '';
                    $data['meta_desc'] = 'Ausflüge und Touren in Procida';
                    $data['meta_title'] = 'Ausflüge und Touren in Procida';
                    break;
            }

            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/services_listing', $data);
            $this->load->view('frontend/includes/footer', $data);
        }
    }


    private function agencies($single) {
        $data = $this->data;
        $this->lang->load('Agenciespage');
        $this->lang->load('Activitypage');
        $this->load->model('Procida_model');
        $this->load->model('Agencies_Model');
        $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
            $items = $this->Cart_model->getUserCart($cartCookie, '');
            $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
            $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
            $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
            if(!empty($tours)){
                for ($i=0; $i < count($tours); $i++) {
                    $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
                }
            }
        } else {
            $items = [];
            $hotelCart = [];
            $tranferCart = [];
            $tours = []; // show random tours
        }
        $data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        if ($single != '') {
            $this->Agencies_Model->loadActivePage($single);
            if (!$this->Agencies_Model->id) {
                show_404();
                return;
            }
            $temp = (array) $this->Agencies_Model;

            $business = array('activityId' => $this->Agencies_Model->id, 'type' => 'agencies');
            $this->Procida_model->addViewToBusinessPage($business);

            $data['activity'] = $this->Agencies_Model;
            $data['activity']->uploadsFolder = 'agencies';
            $data['activity']->breadcrumb_name = 'agencies';

            $this->load->model('Categories');
            $this->load->model('admin/Tag_model');
            $this->load->model('admin/Activity_service_model');

            $activity_categories = array();
            $activity_tag = array();
            $activity_services = array();

            $categories = $data['activity']->categories;
            if($categories != ''){
                $categories = explode(',', $categories);
                foreach ($categories as $key => $value) {
                    $cat = $this->Categories->get($value);
                    array_push($activity_categories, $cat);
                }
            }

            $tags = $data['activity']->tags;
            if($tags != ''){
                $tags = explode(',', $tags);
                foreach ($tags as $key => $value) {
                    $tag = $this->Tag_model->get($value);
                    array_push($activity_tag, $tag);
                }
            }

            $services = $data['activity']->activity_services;
            if($services != ''){
                $services = explode(',', $services);
                foreach ($services as $key => $value) {
                    $service = $this->Activity_service_model->get($value);
                    array_push($activity_services, $service);
                }
            }

            $data['activity']->activity_categories = $activity_categories;
            $data['activity']->activity_tag = $activity_tag;
            $data['activity']->activity_services = $activity_services;

            $this->load->model('admin/Metadata_business_model');
            $metadata = $this->Metadata_business_model->getByOrderID($data['activity']->order_id);


            if($metadata != NULL) {
                switch ($this->lang->lang()) {

                    case 'en':
                        $data['meta_keywords'] = $metadata->meta_keywords_en;
                        $data['meta_desc'] = $metadata->meta_desc_en;
                        $data['meta_title'] = $metadata->meta_title_en;
                        break;
                    case 'it':
                        $data['meta_keywords'] = $metadata->meta_keywords_it;
                        $data['meta_desc'] = $metadata->meta_desc_it;
                        $data['meta_title'] = $metadata->meta_title_it;
                        break;
                    case 'de':
                        $data['meta_keywords'] = $metadata->meta_keywords_du;
                        $data['meta_desc'] = $metadata->meta_desc_du;
                        $data['meta_title'] = $metadata->meta_title_du;
                        break;

                }
            }
            else {
                $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
                $data['meta_desc'] = $temp[$this->lang->lang() . 'Slogan'];
            }

            $data['facebook_sdk'] = '<div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, \'script\', \'facebook-jssdk\'));</script>';
            //
            // $data['cssfiles'] = array('type'=>'file','src'=>'//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
            $data['jsfiles'] = array(
                array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$temp['lat']."; var CUR_LNG=".$temp['lng'].";"),
                array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
                array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
                array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
                array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js")
            );
            $data['cssfiles'] = array(
                array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
                array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
            );
            $this->load->model('Procida_model');
            $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            // var_dump($data['resturants']);exit();
            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/activity_page', $data);
            $this->load->view('frontend/includes/footer', $data);
        } else {

            $data['jsfiles'] = array(
                array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= 0; var CUR_LNG=0;"),
                array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
                array('type' => 'file', 'src' => base_url() . "assets/js/filter_list.js")
            );
            $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);

            $data['shops'] = $this->Agencies_Model->getActiveServices($this->lang->lang());
            shuffle($data['shops']);

            $this->load->model('Categories');
            $others = $this->Categories->getActivityByCategory(14, $this->lang->lang());
            $data['othersActivity'] = $others;
            shuffle($data['othersActivity']);

            $this->load->model('admin/Tag_model');
            $this->load->model('admin/Activity_service_model');
            $all_tags = array();
            $all_services = array();

            foreach ($data['shops'] as $key => $activity) {

                $tags = $activity->tags;
                if($tags != ''){
                    $tags = explode(',', $tags);
                    foreach ($tags as $key => $tagID) {
                        $tag = $this->Tag_model->get($tagID);
                        if(!in_array($tag, $all_tags)){
                            array_push($all_tags, $tag);
                        }
                    }
                }

                $services = $activity->activity_services;
                if($services != ''){
                    $services = explode(',', $services);
                    foreach ($services as $key => $serviceID) {
                        $service = $this->Activity_service_model->get($serviceID);
                        if(!in_array($service, $all_services)){
                            array_push($all_services, $service);
                        }
                    }
                }
            }
            foreach ($data['othersActivity'] as $key => $activity) {

                $tags = $activity->tags;
                if($tags != ''){
                    $tags = explode(',', $tags);
                    foreach ($tags as $key => $tagID) {
                        $tag = $this->Tag_model->get($tagID);
                        if(!in_array($tag, $all_tags)){
                            array_push($all_tags, $tag);
                        }
                    }
                }

                $services = $activity->activity_services;
                if($services != ''){
                    $services = explode(',', $services);
                    foreach ($services as $key => $serviceID) {
                        $service = $this->Activity_service_model->get($serviceID);
                        if(!in_array($service, $all_services)){
                            array_push($all_services, $service);
                        }
                    }
                }
            }
            $data['all_tags'] = $all_tags;
            $data['all_services'] = $all_services;

            switch ($this->lang->lang()) {
                case 'en':
                    $data['meta_keywords'] = 'Escursion, tours, Procida';
                    $data['meta_desc'] = 'Excursions and Tours in Procida';
                    $data['meta_title'] = 'Excursions and Tours in Procida';
                    break;
                case 'it':
                    $data['meta_keywords'] = 'Escursioni, Visite, Procida';
                    $data['meta_desc'] = 'Escursioni e Visite a Procida';
                    $data['meta_title'] = 'Escursioni e Visite a Procida';
                    break;
                case 'de':
                    $data['meta_keywords'] = '';
                    $data['meta_desc'] = 'Ausflüge und Touren in Procida';
                    $data['meta_title'] = 'Ausflüge und Touren in Procida';
                    break;
            }
            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/agencies_listing', $data);
            $this->load->view('frontend/includes/footer', $data);
        }
    }

    private function shopping_listing($single) {
        $data = $this->data;
        $this->lang->load('Shopspage');
        $this->lang->load('Activitypage');
        $this->load->model('Procida_model');

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        if ($single != '') {
            $this->Shope_model->loadActivePage($single);
            if (!$this->Shope_model->id) {
                show_404();
                return;
            }
            $temp = (array) $this->Shope_model;

            $business = array('activityId' => $this->Shope_model->id, 'type' => 'shopes');
            $this->Procida_model->addViewToBusinessPage($business);

            $data['activity'] = $this->Shope_model;
            $data['activity']->uploadsFolder = 'shopes';
            $data['activity']->breadcrumb_name = 'Shops';

            $this->load->model('Categories');
            $this->load->model('admin/Tag_model');
            $this->load->model('admin/Activity_service_model');

            $activity_categories = array();
            $activity_tag = array();
            $activity_services = array();

            $categories = $data['activity']->categories;
            if($categories != ''){
              $categories = explode(',', $categories);
              foreach ($categories as $key => $value) {
                $cat = $this->Categories->get($value);
                array_push($activity_categories, $cat);
              }
            }

            $tags = $data['activity']->tags;
            if($tags != ''){
              $tags = explode(',', $tags);
              foreach ($tags as $key => $value) {
                $tag = $this->Tag_model->get($value);
                array_push($activity_tag, $tag);
              }
            }

            $services = $data['activity']->activity_services;
            if($services != ''){
              $services = explode(',', $services);
              foreach ($services as $key => $value) {
                $service = $this->Activity_service_model->get($value);
                array_push($activity_services, $service);
              }
            }

            $data['activity']->activity_categories = $activity_categories;
            $data['activity']->activity_tag = $activity_tag;
            $data['activity']->activity_services = $activity_services;


            $this->load->model('admin/Metadata_business_model');
            $metadata = $this->Metadata_business_model->getByOrderID($data['activity']->order_id);


              if($metadata != NULL) {
                switch ($this->lang->lang()) {

                    case 'en':
                       $data['meta_keywords'] = $metadata->meta_keywords_en;
                       $data['meta_desc'] = $metadata->meta_desc_en;
                       $data['meta_title'] = $metadata->meta_title_en;
                       break;
                   case 'it':
                       $data['meta_keywords'] = $metadata->meta_keywords_it;
                       $data['meta_desc'] = $metadata->meta_desc_it;
                       $data['meta_title'] = $metadata->meta_title_it;
                       break;
                   case 'de':
                       $data['meta_keywords'] = $metadata->meta_keywords_du;
                       $data['meta_desc'] = $metadata->meta_desc_du;
                       $data['meta_title'] = $metadata->meta_title_du;
                       break;

                  }
              }
              else {
                $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
                $data['meta_desc'] = $temp[$this->lang->lang() . 'Slogan'];
              }

            $this->lang->load('Restaurantspage');
            $data['facebook_sdk'] =  '<div id="fb-root"></div>
                                      <script>(function(d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s); js.id = id;
                                        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
                                        fjs.parentNode.insertBefore(js, fjs);
                                      }(document, \'script\', \'facebook-jssdk\'));</script>';

            $data['jsfiles'] = array(
                  array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$temp['lat'] ."; var CUR_LNG=". $temp['lng'].';'),
                  array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
                  array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
                  array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
                  array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js")
            );
            $data['cssfiles'] = array(
                array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
                array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
            );
            $this->load->model('Procida_model');
            $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
	          $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            // var_dump($data['resturants']);exit();
            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/activity_page', $data);
            $this->load->view('frontend/includes/footer', $data);
        } else {
            $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);
            $data['shops'] = $this->Shope_model->getActiveShops($this->lang->lang());
            shuffle($data['shops']);

            $this->load->model('Categories');
            $others = $this->Categories->getActivityByCategory(4, $this->lang->lang());
            $data['othersActivity'] = $others;
            shuffle($data['othersActivity']);

            $this->load->model('admin/Tag_model');
            $this->load->model('admin/Activity_service_model');
            $all_tags = array();
            $all_services = array();

            foreach ($data['shops'] as $key => $activity) {

              $tags = $activity->tags;
              if($tags != ''){
                $tags = explode(',', $tags);
                foreach ($tags as $key => $tagID) {
                  $tag = $this->Tag_model->get($tagID);
                  if(!in_array($tag, $all_tags)){
                    array_push($all_tags, $tag);
                  }
                }
              }

              $services = $activity->activity_services;
              if($services != ''){
                $services = explode(',', $services);
                foreach ($services as $key => $serviceID) {
                  $service = $this->Activity_service_model->get($serviceID);
                  if(!in_array($service, $all_services)){
                    array_push($all_services, $service);
                  }
                }
              }
            }
            foreach ($data['othersActivity'] as $key => $activity) {

              $tags = $activity->tags;
              if($tags != ''){
                $tags = explode(',', $tags);
                foreach ($tags as $key => $tagID) {
                  $tag = $this->Tag_model->get($tagID);
                  if(!in_array($tag, $all_tags)){
                    array_push($all_tags, $tag);
                  }
                }
              }

              $services = $activity->activity_services;
              if($services != ''){
                $services = explode(',', $services);
                foreach ($services as $key => $serviceID) {
                  $service = $this->Activity_service_model->get($serviceID);
                  if(!in_array($service, $all_services)){
                    array_push($all_services, $service);
                  }
                }
              }
            }
            $data['all_tags'] = $all_tags;
            $data['all_services'] = $all_services;

            $data['jsfiles'] = array(
                  array('type' => 'file', 'src' => base_url() . "assets/js/filter_list.js")
                 );

            switch ($this->lang->lang()) {

             case 'en':
                    $data['meta_keywords'] = 'shopping, procida';
                    $data['meta_desc'] = 'Shopping in Procida: where to buy local crafts, ceramics and accessories';
                    $data['meta_title'] = 'Shopping in Procida: Local handicrafts, ceramics and accessories';
		                break;
                case 'it':
                    $data['meta_keywords'] = 'shopping, procida';
                    $data['meta_desc'] = 'Shopping a Procida: dove acquistare prodotti di artigianato locale, ceramiche artistiche ed accessori';
                    $data['meta_title'] = 'Shopping a Procida: Artigianato locale, ceramiche artistiche ed accessori';
                    break;
                case 'de':
                    $data['meta_keywords'] = 'shopping, procida';
                    $data['meta_desc'] = 'Shopping in Procida: where to buy local crafts, ceramics and accessories';
                    $data['meta_title'] = 'Shopping in Procida: Local handicrafts, ceramics and accessories';
                    break;
            }

            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/shopping_listing', $data);
            $this->load->view('frontend/includes/footer', $data);
        }
    }

    private function wedding($single) {
            if($single !=''){
           $this->wedding_details($single);
           return;
        }
        $data = $this->data;
        $this->lang->load('Weddings');
        $this->load->library('pagination');
        $config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->Weddings_model->totalActiveWeddings();
        $config['per_page'] = 10;

        $data['jsfiles'] = array(
              array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= 0; var CUR_LNG=0;"),
              array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
              array('type' => 'file', 'src' => base_url() . "assets/js/filter_list.js")
        );
        $this->load->model('Procida_model');
        $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);
        $data['restaurants'] = $this->Weddings_model->getActiveWeddings($this->lang->lang());
        shuffle($data['restaurants']);

        $this->load->model('Categories');
        $others = $this->Categories->getActivityByCategory(9, $this->lang->lang());
        $data['othersActivity'] = $others;
        shuffle($data['othersActivity']);

        $this->load->model('admin/Tag_model');
        $this->load->model('admin/Activity_service_model');
        $all_tags = array();
        $all_services = array();

        foreach ($data['restaurants'] as $key => $activity) {

          $tags = $activity->tags;
          if($tags != ''){
            $tags = explode(',', $tags);
            foreach ($tags as $key => $tagID) {
              $tag = $this->Tag_model->get($tagID);
              if(!in_array($tag, $all_tags)){
                array_push($all_tags, $tag);
              }
            }
          }

          $services = $activity->activity_services;
          if($services != ''){
            $services = explode(',', $services);
            foreach ($services as $key => $serviceID) {
              $service = $this->Activity_service_model->get($serviceID);
              if(!in_array($service, $all_services)){
                array_push($all_services, $service);
              }
            }
          }
        }
        foreach ($data['othersActivity'] as $key => $activity) {

          $tags = $activity->tags;
          if($tags != ''){
            $tags = explode(',', $tags);
            foreach ($tags as $key => $tagID) {
              $tag = $this->Tag_model->get($tagID);
              if(!in_array($tag, $all_tags)){
                array_push($all_tags, $tag);
              }
            }
          }

          $services = $activity->activity_services;
          if($services != ''){
            $services = explode(',', $services);
            foreach ($services as $key => $serviceID) {
              $service = $this->Activity_service_model->get($serviceID);
              if(!in_array($service, $all_services)){
                array_push($all_services, $service);
              }
            }
          }
        }
        $data['all_tags'] = $all_tags;
        $data['all_services'] = $all_services;

        switch ($this->lang->lang()) {
             case 'en':
                    $data['meta_keywords'] = 'procida, wedding';
                    $data['meta_desc'] = 'Marriage in Procida? Choose dream location, catering and wedding planner to make your wedding in Procida unforgettable';
                    $data['meta_title'] = 'Weddings in Procida: choose your dream location, catering and wedding planner';
                    break;
                case 'it':
                    $data['meta_keywords'] = 'matrimonio, procida';
                    $data['meta_desc'] = 'Matrimonio a Procida? Scegli location da sogno, catering e wedding planner per rendere le tue nozze a Procida indimenticabili';
                    $data['meta_title'] = 'Sposarsi a Procida: scegli location da sogno, catering e wedding planner';
                    break;
                case 'de':
                    $data['meta_keywords'] = 'procida, Heirat';
                    $data['meta_desc'] = 'Heirat in Procida? Wählen Sie Traumlage, Catering und Hochzeitsplaner Ihre Hochzeit in Procida unvergesslich';
                    $data['meta_title'] = 'Get in Procida verheiratet: wählen Sie Ihre Traumlage , Catering und Hochzeitsplaner';
                    break;
        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );


        $this->lang->load('Restaurantspage');
        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/wedding_listing', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

     private function wedding_details($single){
        $data = $this->data;
        $this->load->model('admin/Settings_model');
        $this->lang->load('Weddings');
        $this->lang->load('Activitypage');
        $this->Settings_model->load();
        $this->Weddings_model->loadActivePage($single);
        if (!$this->Weddings_model->id) {
            show_404();
            return;
        }
        $temp = (array) $this->Weddings_model;

        $business = array('activityId' => $this->Weddings_model->id, 'type' => 'weddings');
        $this->Procida_model->addViewToBusinessPage($business);

        $data['activity'] = $this->Weddings_model;
        $data['activity']->uploadsFolder = 'weddings';
        $data['activity']->breadcrumb_name = 'Weddings';

        $this->load->model('Categories');
        $this->load->model('admin/Tag_model');
        $this->load->model('admin/Activity_service_model');

        $activity_categories = array();
        $activity_tag = array();
        $activity_services = array();

        $categories = $data['activity']->categories;
        if($categories != ''){
          $categories = explode(',', $categories);
          foreach ($categories as $key => $value) {
            $cat = $this->Categories->get($value);
            array_push($activity_categories, $cat);
          }
        }

        $tags = $data['activity']->tags;
        if($tags != ''){
          $tags = explode(',', $tags);
          foreach ($tags as $key => $value) {
            $tag = $this->Tag_model->get($value);
            array_push($activity_tag, $tag);
          }
        }

        $services = $data['activity']->activity_services;
        if($services != ''){
          $services = explode(',', $services);
          foreach ($services as $key => $value) {
            $service = $this->Activity_service_model->get($value);
            array_push($activity_services, $service);
          }
        }

        $data['activity']->activity_categories = $activity_categories;
        $data['activity']->activity_tag = $activity_tag;
        $data['activity']->activity_services = $activity_services;

        $this->load->model('admin/Metadata_business_model');
        $metadata = $this->Metadata_business_model->getByOrderID($data['activity']->order_id);


          if($metadata != NULL) {
            switch ($this->lang->lang()) {

                case 'en':
                   $data['meta_keywords'] = $metadata->meta_keywords_en;
                   $data['meta_desc'] = $metadata->meta_desc_en;
                   $data['meta_title'] = $metadata->meta_title_en;
                   break;
               case 'it':
                   $data['meta_keywords'] = $metadata->meta_keywords_it;
                   $data['meta_desc'] = $metadata->meta_desc_it;
                   $data['meta_title'] = $metadata->meta_title_it;
                   break;
               case 'de':
                   $data['meta_keywords'] = $metadata->meta_keywords_du;
                   $data['meta_desc'] = $metadata->meta_desc_du;
                   $data['meta_title'] = $metadata->meta_title_du;
                   break;

              }
          }
          else {
            $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
            $data['meta_desc'] = $temp[$this->lang->lang() . 'Slogan'];
          }

        $this->lang->load('Restaurantspage');
        $data['facebook_sdk'] = '<div id="fb-root"></div>
                                  <script>(function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
                                    fjs.parentNode.insertBefore(js, fjs);
                                  }(document, \'script\', \'facebook-jssdk\'));</script>';
        $data['jsfiles'] = array(
             array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$temp['lat'] ."; var CUR_LNG=". $temp['lng'].';'),
              array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
              array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
              array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
              array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js")
        );
        $data['cssfiles'] = array(
            array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
            array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
        );
        $this->load->model('Procida_model');
        $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
	       $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);

	       $this->load->helper('cookie');

         $this->load->model('Procida_model');
         $cartCookie = $this->input->cookie('prcrt');

         if(!empty($cartCookie)){
           $items = $this->Cart_model->getUserCart($cartCookie, '');
           $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
           $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
           $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
           if(!empty($tours)){
             for ($i=0; $i < count($tours); $i++) {
               $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
             }
           }
         } else {
           $items = [];
           $hotelCart = [];
           $tranferCart = [];
           $tours = []; // show random tours
         }
	       	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/activity_page', $data);
        $this->load->view('frontend/includes/footer', $data);
    }
}
