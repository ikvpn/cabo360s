<?php

if (!defined("BASEPATH")) {
    exit('No direct script accesss is allowed');
}

class Ajax extends CI_Controller {

    private $response = array("status" => 200, "message" => "Success", "content" => "");

    public function sendmail() {
        $this->load->model("Procida_model");
        $this->load->library('curl');
        $email = $this->input->post("email") ? trim($this->input->post("email")) : null;
        $subject = $this->input->post("subject") ? trim($this->input->post("subject")) : null;
        $firstname = $this->input->post("firstname") ? trim($this->input->post("firstname")) : null;
        $lastname = $this->input->post("lastname") ? trim($this->input->post("lastname")) : null;
        $message = $this->input->post("message") ? trim($this->input->post("message")) : null;
        $table = $this->input->post("type") ? trim($this->input->post("type")) : "locations";
        $orderid = $this->input->post("orderid") ? trim($this->input->post("orderid")) : "1";
        $config['protocol'] = 'sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $this->load->library("email", $config);
        $this->load->helper("email");


        if ($email == null) {
            $this->response = array("status" => 400, "message" => "Bad Request", "content" => "Email field is mandatory.");
        } elseif (!valid_email($email)) {
            $this->response = array("status" => 400, "message" => "Bad Request", "content" => "Please enter valid Email.".$email);
        } else if ($subject == null) {
            $this->response = array("status" => 400, "message" => "Bad Request", "content" => "Subject field is mandatory.");
        } else if ($firstname == null) {
            $this->response = array("status" => 400, "message" => "Bad Request", "content" => "First Name field is mandatory.");
        } else if ($lastname == null) {
            $this->response = array("status" => 400, "message" => "Bad Request", "content" => "Last Name field is mandatory.");
        } else if ($message == null) {
            $this->response = array("status" => 400, "message" => "Bad Request", "content" => "Message field is mandatory.");
        } else {
            $recaptchaResponse = $this->input->post('g_recaptcha_response');
            $userIp = $_SERVER['REMOTE_ADDR'];
            $secret = RECAPTCHA_SECRET_KEY;
            $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $recaptchaResponse . "&remoteip=" . $userIp;
            $response = $this->curl->simple_get($url);
            $status = json_decode($response, true);
            $status = 200;
            if (true) {
              // MICHELE if ($status['success']) {
                // Send Email here
                $to = $this->Procida_model->getEmailAddress($table, $orderid);
               
                $html_message = "Hi " . $to['full_name'] . ", <br><br>" .
                        "&nbsp;&nbsp; " . $firstname . " " . $lastname . " has sent to you message from contact form on <a href='http://visitprocida.it'>www.visitprocida.it</a>.<br>" .
                        "<br>" .
                        "<table>" .
                        "<tr>" .
                        "<td><b>Nome</b><td>" .
                        "<td>" . $firstname . "<td>" .
                        "</tr>" .
                        "<tr>" .
                        "<td><b>Cognome</b><td>" .
                        "<td>" . $lastname . "<td>" .
                        "</tr>" .
                        "<tr>" .
                        "<td><b>Indirizzo Email</b><td>" .
                        "<td>" . $email . "<td>" .
                        "</tr>" .
                        "<tr>" .
                        "<td><b>Page URL</b><td>" .
                        "<td><a href='" . $this->input->post("page_url") . "'>".$this->input->post("page_url")."</a><td>" .
                        "</tr>" .
                        "<tr>" .
                        "<td><b>Oggetto</b><td>" .
                        "<td>" . $subject . "<td>" .
                        "</tr>" .
                        "<tr>" .
                        "<td><b>Richiesta</b><td>" .
                        "<td>" . $message . "<td>" .
                        "</tr>" .
                        "</table>";
                $this->email->initialize($config);
                $this->email->set_mailtype("html");
                $this->email->from('do-not-reply@visitprocida.it', 'Richiesta informazioni da visitprocida');
                $this->email->reply_to($email, $firstname . " " . $lastname);            
               //$this->email->to($to['email']);
                $this->email->to($to['email']);
				        //$this->email->to('info@mdslab.org');
                $this->email->bcc("visitprocida@gmail.com");
            
                //$this->email->bcc('info@mdslab.org');
                $this->email->subject('Visit Procida | ' . $subject);
                $this->email->message($html_message);
                if ($this->email->send()) {
                  // add count to business page contacts.
                   $business = array('customerId' => $orderid);
                   $this->Procida_model->addContactToBusinessPage($business);
                   $this->response = array("status" => 200, "message" => "success", "content" => "Email message sucessfully sent.");
                } else {
                    $this->response = array("status" => 400, "message" => "Bad Request", "content" => "Failed to send email.");
                }
            } else {
                $this->response = array("status" => 400, "message" => "Bad Request", "content" => "Invalid Captcha.","status"=>$status);
            }
        }

        header('Content-Type: application/json');
        echo json_encode($this->response);
    }

    public function transferBooking() {

    $adults = $this->input->post("adults");
    $adultsReturn = $this->input->post("adultsReturn");
    $children = $this->input->post("children");
    $childrenReturn = $this->input->post("childrenReturn");
    $codeFlight = $this->input->post("codeFlight");
    $codeTrain = $this->input->post("codeTrain");
    $date = $this->input->post("date");
    $time = $this->input->post("time");
   	$returnDate = $this->input->post("returnDate");
   	$returnTime = $this->input->post("returnTime");
    $email = $this->input->post("email") ? trim($this->input->post("email")) : null;
	  $fullname = $this->input->post("fullname");
	  $phone = $this->input->post("phone");
    $people = $this->input->post("people");
    $returnPeople = $this->input->post("returnPeople");
    $price = $this->input->post("price");
    $returnPrice = $this->input->post("returnPrice");
		$totalPrice = $this->input->post("totalPrice");

		$pickupAddress = $this->input->post("pickupAddress");
		$dropoffAddress = $this->input->post("dropoffAddress");
		$pickupReturnAddress = $this->input->post("pickupReturnAddress");
		$dropoffReturnAddress = $this->input->post("dropoffReturnAddress");


    $config['protocol'] = 'sendmail';
    $config['charset'] = 'iso-8859-1';
    $config['wordwrap'] = TRUE;
    $this->load->library("email", $config);
    $this->load->helper("email");

		$html_message = "Salve, <br><br>" .
      "&nbsp;&nbsp; " . $fullname . " ha richiesto un booking da <a href='http://visitprocida.it'>www.visitprocida.it</a>.<br>" .
      "<br>" .
      "<table>" .
      "<tr>" .
      "<td><b>Indirizzo Email</b><td>" .
      "<td>" . $email . "<td>" .
      "</tr>" .
      "<tr>" .
      "<td><b>Telefono</b><td>" .
      "<td>" . $phone . "<td>" .
      "</tr>" .
      "<tr>" .
      "<td><b>Codice volo</b><td>" .
      "<td>" . $codeFlight . "<td>" .
      "</tr>" .
      "<tr>" .
      "<td><b>Codice treno</b><td>" .
      "<td>" . $codeTrain . "<td>" .
      "</tr>".
      "<tr>" .
      "<td><b>Viaggio di andata:</b><td>" .
      "</tr>" .
      "<tr>" .
      "<td><b>Persone</b><td>" .
      "<td>" . $adults . "<td>" .
      "</tr>" .
      "<tr>" .
      "<td><b>Bambini</b><td>" .
      "<td>" . $children . "<td>" .
      "</tr>" .
      "<tr>" .
      "<td><b>Data</b><td>" .
      "<td>" . $date . "<td>" .
      "</tr>" .
      "<tr>" .
      "<td><b>Orario</b><td>" .
      "<td>" . $time . "<td>" .
      "</tr>" .
      "<tr>" .
      "<td><b>Partenza da:</b><td>" .
      "<td>" . $pickupAddress . "<td>" .
      "</tr>" .
      "<tr>" .
      "<td><b>Arrivo a:</b><td>" .
      "<td>" . $dropoffAddress . "<td>" .
      "</tr>".
      "<tr>".
      "<td><b>Prezzo per l'andata (a persona)</b><td>" .
      "<td>" . $price . "<td>" .
      "</tr>";
		if($returnDate != " ") {
			$html_message .= 	"<tr>" .
								"<td><b>Viaggio di ritorno:</b><td>" .
								"</tr>" .
								"<td><b>Persone</b><td>" .
								"<td>" . $adultsReturn . "<td>" .
								"</tr>" .
								"<tr>" .
								"<td><b>Bambini</b><td>" .
								"<td>" . $childrenReturn . "<td>" .
								"</tr>" .
								"<tr>" .
								"<td><b>Data</b><td>" .
								"<td>" . $returnDate . "<td>" .
								"</tr>" .
								"<tr>" .
								"<td><b>Orario</b><td>" .
								"<td>" . $returnTime . "<td>" .
								"</tr>" .
								"<tr>" .
								"<tr>" .
								"<td><b>Partenza da:</b><td>" .
								"<td>" . $pickupReturnAddress . "<td>" .
								"</tr>" .
								"<tr>" .
								"<td><b>Arrivo a:</b><td>" .
								"<td>" . $dropoffReturnAddress . "<td>" .
								"</tr>" .
								"<tr>".
								"<td><b>Prezzo per il ritorno (a persona)</b><td>" .
								"<td>" . $returnPrice . "<td>" .
								"</tr>";
		}

		$html_message .= 	"<tr>" .
							"<td><b>Prezzo Totale:</b><td>" .
							"<td>" . $totalPrice . "<td>" .
							"</tr>" .
							"</table>";

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->from('do-not-reply@visitprocida.it', 'No Reply');
		$this->email->reply_to($email, $fullname);
		//$this->email->to('sunandsea.ischia@gmail.com');
		$this->email->to('visitprocida@gmail.com');
		//$this->email->bcc("visitprocida@gmail.com");
		//$this->email->bcc('info@mdslab.org');
		$this->email->subject('Transfer Booking | Visit Procida');
        $this->email->message($html_message);
		if ($this->email->send()) {
			$this->response = array("status" => 200, "message" => "success", "content" => "Email message sucessfully sent.", "input" => $html_message);
		} else {
			$this->response = array("status" => 400, "message" => "Bad Request", "content" => "Failed to send email.");
		}

        header('Content-Type: application/json');
        echo json_encode($this->response);
    }

}
