<?php



/*

 * Author: Innam Hunzai

 * Email: innamhunzai@gmail.com

 * Project: Visit Procida

 * Version: 1.0

 * File:

 * Description:

 */





if (!defined('BASEPATH')) {

    exit('No direct script access allowed');

}



class Events extends CI_Controller {



    public $data;



    public function __construct() {

        parent::__construct();

        $this->load->model('Menu_model');

        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());

        $this->load->model('admin/Location_model');

        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());

        $this->load->model('admin/Beach_model');

        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());

        $this->load->model("Places_of_interest_model");

        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();

        $this->load->helper('language');

        $this->load->helper('url');

        $this->lang->load('Navigation');

        $this->load->model('admin/Ads_model');

        $this->load->model('Orders_model');

        $this->lang->load('Eventspage');

        $this->load->model('Cart_model');

        $this->load->model('Shope_model');

        $this->load->model('Boatrentals_model');

        $this->load->model('Beachclub_model');

        $this->load->model('Weddings_model');



        $this->load->model('Apartment_model');

        $this->load->model('Hotel_model');



        $this->load->model('Resturant_model');

        $this->load->model('Barsandcafe_model');

        $this->load->model('Nightlife_model');

        $this->load->model('Event_model');



        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();

        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();

        $this->data['counts']['watersports'] = 0;

        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();

        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();



        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();

        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();



        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();

        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();

        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();



        $this->load->model('admin/Blog_model');

        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());

    }



    public function index($page = '', $single = '') {

        switch ($page) {

            case 'traditional-events':

            case 'eventi-tradizionali':

                $this->traditionalevents_listing();

                break;

            case 'traditional-event':

                $this->traditionalevent_details($single);

                break;

            case 'upcoming-events':

                //$this->upcomingevents_listing();
                redirect('');

                break;

            case 'upcoming-event':

                //$this->upcomingevent_details($single);
                redirect('');

                break;

            default:

                $data = $this->data;
                $this->load->helper('cookie');

                    $cartCookie = $this->input->cookie('prcrt');

                    if(!empty($cartCookie)){
                      $items = $this->Cart_model->getUserCart($cartCookie, '');
                      $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
                      $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
                      $tours = [];
                    } else {
                      $items = [];
                      $hotelCart = [];
                      $tranferCart = [];
                      $tours = []; // show random tours
                    }
                	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );


                $this->load->view('frontend/includes/header', $data);

                $this->load->view('frontend/content/' . $page, $data);

                $this->load->view('frontend/includes/footer', $data);

                break;

        }

    }



    public function traditionalevents_listing() {

        $data = $this->data;

        $this->load->model('Orders_model');

        $this->load->library('pagination');

        $config['full_tag_open'] = "<ul class='pagination pagination-sm'>";

        $config['full_tag_close'] = "</ul>";

        $config['num_tag_open'] = '<li>';

        $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";

        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";

        $config['next_tag_open'] = "<li>";

        $config['next_tagl_close'] = "</li>";

        $config['prev_tag_open'] = "<li>";

        $config['prev_tagl_close'] = "</li>";

        $config['first_tag_open'] = "<li>";

        $config['first_tagl_close'] = "</li>";

        $config['last_tag_open'] = "<li>";

        $config['last_tagl_close'] = "</li>";

        $config['use_page_numbers'] = TRUE;

        $config['uri_segment'] = 4;

        $config['total_rows'] = $this->Event_model->totalActiveEvents('Traditional');

        $config['per_page'] = 10;

        $config['base_url'] = base_url().'/events/traditional_events/';

        $this->pagination->initialize($config);

        //$event = (array)$this->event_model;

        $data['events'] = $this->Event_model->getActiveEvents("Traditional", $this->lang->lang());



        // $data['events'] = $this->event_model->getActiveEvents($this->lang->lang());

       // $data['bhotels'] = json_decode(file_get_contents('https://api.import.io/store/data/710aa610-8873-401c-a05e-05635d420974/_query?input/webpage/url=http%3A%2F%2Fwww.booking.com%2Fsearchresults.html%3Fcity%3D-125857&_user=93d6622e-51fe-4c36-9841-10bbed60af94&_apikey=93d6622e51fe4c36984110bbed60af949988a885e9d4d9d8b0053809e5fa3e7767cb4b8c612695a779ad822fdbda356e5a97a3eeb8a012097413d186aeadd873a92adf928ea2bad26fcb44b236a0987f'));

        //var_dump($data['bhotels']);exit();

        switch ($this->lang->lang()) {

            case 'en':

                $data['meta_keywords'] = 'Procida, events, festivals, concerts, exhibitions, processions, folklore';

                $data['meta_desc'] = 'All events in Procida! Processions, festivals, exhibitions, festivals, concerts, traditions.';

                $data['meta_title'] = 'Events in Procida: concerts, festivals, traditions';

                break;

            case 'de':

                $data['meta_keywords'] = 'Hotels, Zimmer, Apartments, Bed, Breakfast, Procida';

                $data['meta_desc'] = 'Procida Hotels Hotels und Hotels an der See oder in der grünen Insel: 4 Sterne, 3 Sterne, Bed & Breakfast in außergewöhnlich.';

                $data['meta_title'] = 'Hotel Procida | Hotels und Pensionen auf der Insel Procida, Italien';

                break;

            case 'it':

                $data['meta_keywords'] = 'procida, eventi, manifestazioni, concerti, mostre, processioni, folclore,';

                $data['meta_desc'] = 'Tutti gli eventi a Procida! Processioni, sagre, mostre, rassegne, concerti, tradizioni folkloristiche.';

                $data['meta_title'] = 'Eventi a Procida 2016: concerti, sagre, tradizioni';

                break;

        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
    	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );


        $this->load->view('frontend/includes/header', $data);

        $this->load->view('frontend/content/traditional_events', $data);

        $this->load->view('frontend/includes/footer', $data);

    }



    public function traditionalevent_details($id) {

        $data = $this->data;

         $this->Event_model->load($id);

         if ($this->Event_model->id == null) {

            show_404();

            return;

        }

         $data['facebook_sdk'] = '<div id="fb-root"></div>

<script>(function(d, s, id) {

  var js, fjs = d.getElementsByTagName(s)[0];

  if (d.getElementById(id)) return;

  js = d.createElement(s); js.id = id;

  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";

  fjs.parentNode.insertBefore(js, fjs);

}(document, \'script\', \'facebook-jssdk\'));</script>';



        $data['jsfiles'] = array(array('type'=>'inlince','src'=>'

    $(".map-info-box .line h2").click(function() {

        $(".map-info-box .line p").slideUp();

        $(this).next("p").slideToggle();

    });

    var directionsDisplay;

    var directionsService = new google.maps.DirectionsService();

    var map;



    function initialize() {

        directionsDisplay = new google.maps.DirectionsRenderer();

        var mainPlace = new google.maps.LatLng('.$this->Event_model->lat.', '.$this->Event_model->lng.');

        var myStyles = [{

            featureType: "poi",

            elementType: "labels",

            stylers: [{

                visibility: "off"

            }]

        }];

        var mapOptions = {

            zoom: 16,

            disableDefaultUI: true,

            disableDoubleClickZoom: true,

            scrollwheel: false,

            center: mainPlace,

            styles: myStyles

        }

        map = new google.maps.Map(document.getElementById(\'map-canvas-sidebar\'), mapOptions);

        var marker = new google.maps.Marker({

            position: mainPlace,

            map: map

        });

        directionsDisplay.setMap(map);

    }



    function calcRoute(destination) {

        var start = \'Corricella, Procida, Naples, Italy\';

        var end = destination;

        var request = {

            origin: start,

            destination: end,

            travelMode: google.maps.TravelMode.DRIVING

        };

        directionsService.route(request, function(response, status) {

            if (status == google.maps.DirectionsStatus.OK) {

                directionsDisplay.setDirections(response);

            }

        });

    }

    google.maps.event.addDomListener(window, \'load\', initialize);

    google.maps.event.addDomListener(window, \'resize\', initialize);

    '));

        $event = (array)$this->Event_model;

        /*Set Page Meta tags */

        /*Use $event variable to access ecent data. i.e $event['enName']*/

        switch ($this->lang->lang()) {

            case 'en':

                $data['meta_keywords'] = '';

                $data['meta_desc'] = '';

                $data['meta_title'] = $event['enTitle'];

                break;

            case 'it':

                $data['meta_keywords'] = '';

                $data['meta_desc'] = '';

                $data['meta_title'] = $event['itTitle'];

                break;

            case 'de':

                $data['meta_keywords'] = '';

                $data['meta_desc'] = '';

                $data['meta_title'] = $event['deTitle'];

                break;

        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );


        $this->load->view('frontend/includes/header', $data);

        $this->load->view('frontend/content/traditional_event_details', $data);

        $this->load->view('frontend/includes/footer', $data);

    }



    public function upcomingevents_listing() {

        $data = $this->data;

         $this->load->library('pagination');

        $config['full_tag_open'] = "<ul class='pagination pagination-sm'>";

        $config['full_tag_close'] = "</ul>";

        $config['num_tag_open'] = '<li>';

        $config['num_tag_close'] = '</li>';

        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";

        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";

        $config['next_tag_open'] = "<li>";

        $config['next_tagl_close'] = "</li>";

        $config['prev_tag_open'] = "<li>";

        $config['prev_tagl_close'] = "</li>";

        $config['first_tag_open'] = "<li>";

        $config['first_tagl_close'] = "</li>";

        $config['last_tag_open'] = "<li>";

        $config['last_tagl_close'] = "</li>";

        $config['use_page_numbers'] = TRUE;

        $config['uri_segment'] = 4;

        $config['total_rows'] = $this->Event_model->totalActiveEvents('General');

        $config['per_page'] = 10;

        $config['base_url'] = base_url().'/events/upcoming_events/';

        $this->pagination->initialize($config);

        $data['events'] = $this->Event_model->getActiveEvents("General", $this->lang->lang());



        /*Set Page Meta tags for upcoming events listing page */

        switch ($this->lang->lang()) {

            case 'en':

                $data['meta_keywords'] = '';

                $data['meta_desc'] = '';

                $data['meta_title'] = '';

                break;

            case 'it':

                $data['meta_keywords'] = '';

                $data['meta_desc'] = '';

                $data['meta_title'] = '';

                break;

            case 'de':

                $data['meta_keywords'] = '';

                $data['meta_desc'] = '';

                $data['meta_title'] = '';

                break;

        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );


        $this->load->view('frontend/includes/header', $data);

        $this->load->view('frontend/content/upcoming_events', $data);

        $this->load->view('frontend/includes/footer', $data);

    }



    public function upcomingevent_details($id) {

        $data = $this->data;

        $this->Event_model->load($id);

        if ($this->Event_model->id==null) {

            show_404();

            return;

        }

           $data['facebook_sdk'] = '<div id="fb-root"></div>

<script>(function(d, s, id) {

  var js, fjs = d.getElementsByTagName(s)[0];

  if (d.getElementById(id)) return;

  js = d.createElement(s); js.id = id;

  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";

  fjs.parentNode.insertBefore(js, fjs);

}(document, \'script\', \'facebook-jssdk\'));</script>';



        $event = (array)$this->model_event;

        /*Set Page Meta tags */

        /*Use $event variable to access ecent data. i.e $event['enName']*/

        switch ($this->lang->lang()) {

            case 'en':

                $data['meta_keywords'] = '';

                $data['meta_desc'] = '';

                $data['meta_title'] = $event['enTitle'];

                break;

            case 'it':

                $data['meta_keywords'] = '';

                $data['meta_desc'] = '';

                $data['meta_title'] = $event['itTitle'];

                break;

            case 'de':

                $data['meta_keywords'] = '';

                $data['meta_desc'] = '';

                $data['meta_title'] = $event['deTitle'];

                break;

        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );


        $this->load->view('frontend/includes/header', $data);

        $this->load->view('frontend/content/upcoming_event_details', $data);

        $this->load->view('frontend/includes/footer', $data);

    }



}

