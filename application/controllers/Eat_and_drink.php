<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Eat_and_drink extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->model('admin/Location_model');
        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model("Places_of_interest_model");
        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');
        $this->load->model('admin/Ads_model');
        $this->load->model('Orders_model');
        $this->load->model('Cart_model');
        $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');
        $this->load->model('Procida_model');
        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');

        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');
        $this->load->model('Service_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();
        $this->data['counts']['escursions-and-visit'] = $this->Service_model->totalActiveServices();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();
        $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());
    }

    public function index($page = '', $single = '') {
        switch ($page) {
            case 'restaurants':
                $this->resturant_listing();
                break;
            case 'restaurant':
                $this->resturant_details($single);
                break;
            case 'bars-and-cafe':
                $this->bars_and_cafe($single);
                break;
            case 'nightlife':
                $this->nightlife($single);
                break;
            default:
                $data = $this->data;
                $this->load->helper('cookie');

                    $cartCookie = $this->input->cookie('prcrt');

                    if(!empty($cartCookie)){
                      $items = $this->Cart_model->getUserCart($cartCookie, '');
                      $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
                      $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
                      $tours = [];
                    } else {
                      $items = [];
                      $hotelCart = [];
                      $tranferCart = [];
                      $tours = []; // show random tours
                    }
                	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

                $this->load->view('frontend/includes/header', $data);
                $this->load->view('frontend/content/' . $page, $data);
                $this->load->view('frontend/includes/footer', $data);
                break;
        }
    }

    private function nightlife($single) {
        $data = $this->data;
        $this->load->model('Procida_model');

        $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
        	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        if ($single != '') {
            $this->load->model('admin/Settings_model');
            $this->lang->load('Activitypage');

            $this->Settings_model->load();
            $this->Nightlife_model->loadActivePage($single);
            if (!$this->Nightlife_model->id) {
                show_404();
                return;
            }
            $temp = (array) $this->Nightlife_model;

            $business = array('activityId' => $this->Nightlife_model->id, 'type' => 'nightlife');
           $this->Procida_model->addViewToBusinessPage($business);

            $data['activity'] = $this->Nightlife_model;

            $this->load->model('Categories');
            $this->load->model('admin/Tag_model');
            $this->load->model('admin/Activity_service_model');

            $activity_categories = array();
            $activity_tag = array();
            $activity_services = array();

            $categories = $data['activity']->categories;
            if($categories != ''){
              $categories = explode(',', $categories);
              foreach ($categories as $key => $value) {
                $cat = $this->Categories->get($value);
                array_push($activity_categories, $cat);
              }
            }

            $tags = $data['activity']->tags;
            if($tags != ''){
              $tags = explode(',', $tags);
              foreach ($tags as $key => $value) {
                $tag = $this->Tag_model->get($value);
                array_push($activity_tag, $tag);
              }
            }

            $services = $data['activity']->activity_services;
            if($services != ''){
              $services = explode(',', $services);
              foreach ($services as $key => $value) {
                $service = $this->Activity_service_model->get($value);
                array_push($activity_services, $service);
              }
            }

            $data['activity']->activity_categories = $activity_categories;
            $data['activity']->activity_tag = $activity_tag;
            $data['activity']->activity_services = $activity_services;

            $data['activity']->uploadsFolder = 'nightlife';
            $data['activity']->breadcrumb_name = 'Nightlife';


            $this->load->model('admin/Metadata_business_model');
            $metadata = $this->Metadata_business_model->getByOrderID($data['activity']->order_id);


              if($metadata != NULL) {
                switch ($this->lang->lang()) {

                    case 'en':
                       $data['meta_keywords'] = $metadata->meta_keywords_en;
                       $data['meta_desc'] = $metadata->meta_desc_en;
                       $data['meta_title'] = $metadata->meta_title_en;
                       break;
                   case 'it':
                       $data['meta_keywords'] = $metadata->meta_keywords_it;
                       $data['meta_desc'] = $metadata->meta_desc_it;
                       $data['meta_title'] = $metadata->meta_title_it;
                       break;
                   case 'de':
                       $data['meta_keywords'] = $metadata->meta_keywords_du;
                       $data['meta_desc'] = $metadata->meta_desc_du;
                       $data['meta_title'] = $metadata->meta_title_du;
                       break;

                  }
              }
              else {
                $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
                $data['meta_desc'] = $temp[$this->lang->lang() . 'Slogan'];
              }

            $this->lang->load('Restaurantspage');
            $data['facebook_sdk'] = '<div id="fb-root"></div>
                                      <script>(function(d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s); js.id = id;
                                        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
                                        fjs.parentNode.insertBefore(js, fjs);
                                      }(document, \'script\', \'facebook-jssdk\'));</script>';
            //
            // $data['cssfiles'] = array('type'=>'file','src'=>'//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
            $data['cssfiles'] = array(
              array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
              array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
            );
            $data['jsfiles'] = array(
                array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
                array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
                array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js"),
                array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$temp['lat']."; var CUR_LNG=".$temp['lng'].";"),
                array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js")
            );


            $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
			      $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/activity_page', $data);
            $this->load->view('frontend/includes/footer', $data);
        } else {
            $this->load->model('Nightlife_model');
            $this->lang->load('Restaurantspage');

            $data['jsfiles'] = array(
                  array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= 0; var CUR_LNG=0;"),
                  array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
                  array('type' => 'file', 'src' => base_url() . "assets/js/filter_list.js")
                 );
            switch ($this->lang->lang()) {
                case 'en':
                    $data['meta_keywords'] = '';
                    $data['meta_desc'] = 'Restaurants Procida, Italy: best Italian restaurants and pizzerias for both lunch and dinner.';
                    $data['meta_title'] = 'Restaurants in Procida: Traditional Island Cuisine';
                    break;
                case 'it':
                    $data['meta_keywords'] = '';
                    $data['meta_desc'] = 'Ristoranti Procida: i migliori ristoranti e pizzerie a Marina Corricella, Marina chiaiolella, Marina Grande.!';
                    $data['meta_title'] = 'Ristoranti a Procida: dove mangiare a Procida';
                    break;
                case 'de':
                    $data['meta_keywords'] = '';
                    $data['meta_desc'] = 'Wo in Procida essen? Alle Restaurants mit Fotos, Menüs und Bewertungen. Wählen und buchen Sie Ihren Tisch!';
                    $data['meta_title'] = 'Restaurants in Procida Insel: Traditionelle Küche';
                    break;
            }

            $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);
            $data['restaurants'] = $this->Nightlife_model->getActiveResturants($this->lang->lang());
            shuffle($data['restaurants']);
            $this->load->model('Categories');
            $others = $this->Categories->getActivityByCategory(11, $this->lang->lang());
            $data['othersActivity'] = $others;
            shuffle($data['othersActivity']);

            $this->load->model('admin/Tag_model');
            $this->load->model('admin/Activity_service_model');
            $all_tags = array();
            $all_services = array();

            foreach ($data['restaurants'] as $key => $activity) {

              $tags = $activity->tags;
              if($tags != ''){
                $tags = explode(',', $tags);
                foreach ($tags as $key => $tagID) {
                  $tag = $this->Tag_model->get($tagID);
                  if(!in_array($tag, $all_tags)){
                    array_push($all_tags, $tag);
                  }
                }
              }

              $services = $activity->activity_services;
              if($services != ''){
                $services = explode(',', $services);
                foreach ($services as $key => $serviceID) {
                  $service = $this->Activity_service_model->get($serviceID);
                  if(!in_array($service, $all_services)){
                    array_push($all_services, $service);
                  }
                }
              }
            }
            foreach ($data['othersActivity'] as $key => $activity) {

              $tags = $activity->tags;
              if($tags != ''){
                $tags = explode(',', $tags);
                foreach ($tags as $key => $tagID) {
                  $tag = $this->Tag_model->get($tagID);
                  if(!in_array($tag, $all_tags)){
                    array_push($all_tags, $tag);
                  }
                }
              }

              $services = $activity->activity_services;
              if($services != ''){
                $services = explode(',', $services);
                foreach ($services as $key => $serviceID) {
                  $service = $this->Activity_service_model->get($serviceID);
                  if(!in_array($service, $all_services)){
                    array_push($all_services, $service);
                  }
                }
              }
            }
            $data['all_tags'] = $all_tags;
            $data['all_services'] = $all_services;

            $this->load->view('frontend/includes/header', $data);
            $this->load->view('frontend/content/nightlife_listing', $data);
            $this->load->view('frontend/includes/footer', $data);
        }
    }

    private function resturant_listing() {
        $data = $this->data;
        $this->lang->load('Restaurantspage');
        $this->load->model('Procida_model');

        $data['jsfiles'] = array(
              array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= 0; var CUR_LNG=0;"),
              array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
              array('type' => 'file', 'src' => base_url() . "assets/js/filter_list.js")
        );
        switch ($this->lang->lang()) {
            case 'en':
                $data['meta_keywords'] = '';
                $data['meta_desc'] = 'Dining on Procida Italy? The best Italian restaurants and pizzerias for both lunch and dinner.';
                $data['meta_title'] = 'Restaurants in Procida: Traditional Island Cuisine';
                break;
            case 'it':
                $data['meta_keywords'] = '';
                $data['meta_desc'] = 'Dove mangiare a Procida? Tutti i ristoranti con foto, menu e recensioni. Scegli il tuo e prenota un tavolo!';
                $data['meta_title'] = 'Ristoranti a Procida: dove mangiare';
                break;
            case 'de':
                $data['meta_keywords'] = '';
                $data['meta_desc'] = 'Wo in Procida essen? Alle Restaurants mit Fotos, Menüs und Bewertungen. Wählen und buchen Sie Ihren Tisch!';
                $data['meta_title'] = 'Restaurants in Procida Insel: Traditionelle Küche';
        }
        $this->load->model('Categories');
		    $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
			  $data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $others = $this->Categories->getActivityByCategory(2, $this->lang->lang());

        $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);
        $data['restaurants'] = $this->Resturant_model->getActiveResturants($this->lang->lang());
        shuffle($data['restaurants']);

        $data['othersActivity'] = $others;
        shuffle($data['othersActivity']);

        $this->load->model('admin/Tag_model');
        $this->load->model('admin/Activity_service_model');
        $all_tags = array();
        $all_services = array();

        foreach ($data['restaurants'] as $key => $activity) {

          $tags = $activity->tags;
          if($tags != ''){
            $tags = explode(',', $tags);
            foreach ($tags as $key => $tagID) {
              $tag = $this->Tag_model->get($tagID);
              if(!in_array($tag, $all_tags)){
                array_push($all_tags, $tag);
              }
            }
          }

          $services = $activity->activity_services;
          if($services != ''){
            $services = explode(',', $services);
            foreach ($services as $key => $serviceID) {
              $service = $this->Activity_service_model->get($serviceID);
              if(!in_array($service, $all_services)){
                array_push($all_services, $service);
              }
            }
          }
        }
        foreach ($data['othersActivity'] as $key => $activity) {

          $tags = $activity->tags;
          if($tags != ''){
            $tags = explode(',', $tags);
            foreach ($tags as $key => $tagID) {
              $tag = $this->Tag_model->get($tagID);
              if(!in_array($tag, $all_tags)){
                array_push($all_tags, $tag);
              }
            }
          }

          $services = $activity->activity_services;
          if($services != ''){
            $services = explode(',', $services);
            foreach ($services as $key => $serviceID) {
              $service = $this->Activity_service_model->get($serviceID);
              if(!in_array($service, $all_services)){
                array_push($all_services, $service);
              }
            }
          }
        }
        $data['all_tags'] = $all_tags;
        $data['all_services'] = $all_services;


        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/restaurant_listing', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function bars_and_cafe($single) {
        if ($single != '') {
            $this->bars_and_cafe_details($single);
            return;
        }
        $data = $this->data;
        $this->lang->load('Barsandcafe');
        $this->load->library('pagination');

        $data['jsfiles'] = array(
               array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= 0; var CUR_LNG=0;"),
               array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
               array('type' => 'file', 'src' => base_url() . "assets/js/filter_list.js")
            );
        $this->load->model('Procida_model');
        $data['events'] = $this->Procida_model->getUpcomingEvents('e.*,o.location', 'o.status=\'paid\'', 6);
        $data['restaurants'] = $this->Barsandcafe_model->getActiveBarsAndCafe($this->lang->lang());
        shuffle($data['restaurants']);

        $this->load->model('Categories');
        $others = $this->Categories->getActivityByCategory(7, $this->lang->lang());
        $data['othersActivity'] = $others;
        shuffle($data['othersActivity']);

        switch ($this->lang->lang()) {
            case 'en':
                $data['meta_keywords'] = '';
                $data['meta_desc'] = 'Dining on Procida Italy? The best Italian restaurants and pizzerias for both lunch and dinner.';
                $data['meta_title'] = 'Bars and Caffè in Procida: Traditional Island Cuisine';
                break;
            case 'it':
                $data['meta_keywords'] = '';
                $data['meta_desc'] = 'Dove mangiare a Procida? Tutti i ristoranti con foto, menu e recensioni. Scegli il tuo e prenota un tavolo!';
                $data['meta_title'] = 'Bar e Caffè a Procida: dove mangiare';
                break;
            case 'de':
                $data['meta_keywords'] = '';
                $data['meta_desc'] = 'Wo in Procida essen? Alle Restaurants mit Fotos, Menüs und Bewertungen. Wählen und buchen Sie Ihren Tisch!';
                $data['meta_title'] = 'Bars and Caffè in Procida Insel: Traditionelle Küche';
                break;
        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
      	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $this->load->model('admin/Tag_model');
        $this->load->model('admin/Activity_service_model');
        $all_tags = array();
        $all_services = array();

        foreach ($data['restaurants'] as $key => $activity) {

          $tags = $activity->tags;
          if($tags != ''){
            $tags = explode(',', $tags);
            foreach ($tags as $key => $tagID) {
              $tag = $this->Tag_model->get($tagID);
              if(!in_array($tag, $all_tags)){
                array_push($all_tags, $tag);
              }
            }
          }

          $services = $activity->activity_services;
          if($services != ''){
            $services = explode(',', $services);
            foreach ($services as $key => $serviceID) {
              $service = $this->Activity_service_model->get($serviceID);
              if(!in_array($service, $all_services)){
                array_push($all_services, $service);
              }
            }
          }
        }
        foreach ($data['othersActivity'] as $key => $activity) {

          $tags = $activity->tags;
          if($tags != ''){
            $tags = explode(',', $tags);
            foreach ($tags as $key => $tagID) {
              $tag = $this->Tag_model->get($tagID);
              if(!in_array($tag, $all_tags)){
                array_push($all_tags, $tag);
              }
            }
          }

          $services = $activity->activity_services;
          if($services != ''){
            $services = explode(',', $services);
            foreach ($services as $key => $serviceID) {
              $service = $this->Activity_service_model->get($serviceID);
              if(!in_array($service, $all_services)){
                array_push($all_services, $service);
              }
            }
          }
        }
        $data['all_tags'] = $all_tags;
        $data['all_services'] = $all_services;


        $this->lang->load('Restaurantspage');
        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/barsandcafe_listing', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function bars_and_cafe_details($single) {

        $data = $this->data;
        $this->load->model('admin/Settings_model');
        $this->lang->load('Activitypage');

        $this->Settings_model->load();
        $this->Barsandcafe_model->loadActivePage($single);
        if (!$this->Barsandcafe_model->id) {
            return show_404();
        }
        $temp = (array) $this->Barsandcafe_model;

        $business = array('activityId' => $this->Barsandcafe_model->id, 'type' => 'bars_cafe');
        $this->Procida_model->addViewToBusinessPage($business);

        $data['activity'] = $this->Barsandcafe_model;
        $data['activity']->uploadsFolder = 'bars_cafe';
        $data['activity']->breadcrumb_name = 'Bars_and_Cafe';

        $this->load->model('Categories');
        $this->load->model('admin/Tag_model');
        $this->load->model('admin/Activity_service_model');

        $activity_categories = array();
        $activity_tag = array();
        $activity_services = array();

        $categories = $data['activity']->categories;
        if($categories != ''){
          $categories = explode(',', $categories);
          foreach ($categories as $key => $value) {
            $cat = $this->Categories->get($value);
            array_push($activity_categories, $cat);
          }
        }

        $tags = $data['activity']->tags;
        if($tags != ''){
          $tags = explode(',', $tags);
          foreach ($tags as $key => $value) {
            $tag = $this->Tag_model->get($value);
            array_push($activity_tag, $tag);
          }
        }

        $services = $data['activity']->activity_services;
        if($services != ''){
          $services = explode(',', $services);
          foreach ($services as $key => $value) {
            $service = $this->Activity_service_model->get($value);
            array_push($activity_services, $service);
          }
        }

        $data['activity']->activity_categories = $activity_categories;
        $data['activity']->activity_tag = $activity_tag;
        $data['activity']->activity_services = $activity_services;


        $this->load->model('admin/Metadata_business_model');
        $metadata = $this->Metadata_business_model->getByOrderID($data['activity']->order_id);


          if($metadata != NULL) {
            switch ($this->lang->lang()) {

                case 'en':
                   $data['meta_keywords'] = $metadata->meta_keywords_en;
                   $data['meta_desc'] = $metadata->meta_desc_en;
                   $data['meta_title'] = $metadata->meta_title_en;
                   break;
               case 'it':
                   $data['meta_keywords'] = $metadata->meta_keywords_it;
                   $data['meta_desc'] = $metadata->meta_desc_it;
                   $data['meta_title'] = $metadata->meta_title_it;
                   break;
               case 'de':
                   $data['meta_keywords'] = $metadata->meta_keywords_du;
                   $data['meta_desc'] = $metadata->meta_desc_du;
                   $data['meta_title'] = $metadata->meta_title_du;
                   break;

              }
          }
          else {
            $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
            $data['meta_desc'] = $temp[$this->lang->lang() . 'Slogan'];
          }

        $this->lang->load('Restaurantspage');
        $data['facebook_sdk'] = '<div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, \'script\', \'facebook-jssdk\'));</script>';
        //
        // $data['cssfiles'] = array('type'=>'file','src'=>'//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
        $data['cssfiles'] = array(
            array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
            array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
        );
        $data['jsfiles'] = array(
            array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
            array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
            array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js"),
            array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$temp['lat']."; var CUR_LNG=".$temp['lng'].";"),
             array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js")
        );
        $this->load->model('Resturant_model');
        $this->load->model('Procida_model');
        $data['resturants'] =  $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $this->load->model('Hotel_model');
        $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
		    $this->load->helper('cookie');


        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = [];
        }

        $data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/activity_page', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    private function resturant_details($page_id) {
        $data = $this->data;
        $this->load->model('Procida_model');
        $this->load->model('admin/Settings_model');
        $this->lang->load('Activitypage');

        $this->Settings_model->load();
        $this->Resturant_model->loadActivePage($page_id);
        if (!$this->Resturant_model->id) {
            return show_404();
        }
        $temp = (array) $this->Resturant_model;

        $business = array('activityId' => $this->Resturant_model->id, 'type' => 'resturants');
        $this->Procida_model->addViewToBusinessPage($business);

        $data['activity'] = $this->Resturant_model;
        $data['activity']->uploadsFolder = 'resturants';
        $data['activity']->breadcrumb_name = 'Restaurants';

        $this->load->model('Categories');
        $this->load->model('admin/Tag_model');
        $this->load->model('admin/Activity_service_model');

        $activity_categories = array();
        $activity_tag = array();
        $activity_services = array();

        $categories = $data['activity']->categories;
        if($categories != ''){
          $categories = explode(',', $categories);
          foreach ($categories as $key => $value) {
            $cat = $this->Categories->get($value);
            array_push($activity_categories, $cat);
          }
        }

        $tags = $data['activity']->tags;
        if($tags != ''){
          $tags = explode(',', $tags);
          foreach ($tags as $key => $value) {
            $tag = $this->Tag_model->get($value);
            array_push($activity_tag, $tag);
          }
        }

        $services = $data['activity']->activity_services;
        if($services != ''){
          $services = explode(',', $services);
          foreach ($services as $key => $value) {
            $service = $this->Activity_service_model->get($value);
            array_push($activity_services, $service);
          }
        }

        $data['activity']->activity_categories = $activity_categories;
        $data['activity']->activity_tag = $activity_tag;
        $data['activity']->activity_services = $activity_services;

        $this->load->model('admin/Metadata_business_model');
        $metadata = $this->Metadata_business_model->getByOrderID($data['activity']->order_id);


          if($metadata != NULL) {
            switch ($this->lang->lang()) {

                case 'en':
                   $data['meta_keywords'] = $metadata->meta_keywords_en;
                   $data['meta_desc'] = $metadata->meta_desc_en;
                   $data['meta_title'] = $metadata->meta_title_en;
                   break;
               case 'it':
                   $data['meta_keywords'] = $metadata->meta_keywords_it;
                   $data['meta_desc'] = $metadata->meta_desc_it;
                   $data['meta_title'] = $metadata->meta_title_it;
                   break;
               case 'de':
                   $data['meta_keywords'] = $metadata->meta_keywords_du;
                   $data['meta_desc'] = $metadata->meta_desc_du;
                   $data['meta_title'] = $metadata->meta_title_du;
                   break;

              }
          }
          else {
            $data['meta_title'] = $temp[$this->lang->lang() . 'Name'];
            $data['meta_desc'] = $temp[$this->lang->lang() . 'Slogan'];
          }


        $this->lang->load('Restaurantspage');
        $data['facebook_sdk'] = '<div id="fb-root"></div>
                                <script>(function(d, s, id) {
                                  var js, fjs = d.getElementsByTagName(s)[0];
                                  if (d.getElementById(id)) return;
                                  js = d.createElement(s); js.id = id;
                                  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
                                  fjs.parentNode.insertBefore(js, fjs);
                                }(document, \'script\', \'facebook-jssdk\'));</script>';
      $data['cssfiles'] = array(
            array('type' => 'file', 'src' => base_url() . 'assets/rs-plugin/css/settings.css'),
            array('type' => 'file', 'src' => base_url() . 'assets/css/rev-slider.css')
        );
        $data['jsfiles'] = array(
            array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.tools.min.js"),
            array('type' => 'file', 'src' => base_url() . "assets/rs-plugin/js/jquery.themepunch.revolution.min.js"),
            array('type' => 'file', 'src' => base_url() . "assets/js/rev-slider.js"),
           array('type' => 'inline', 'src' => " var MAP_ID='map-canvas-sidebar'; var CUR_LAT= ".$temp['lat']."; var CUR_LNG=".$temp['lng'].";"),
           array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js")
        );
        $data['resturants'] = $this->Procida_model->getResturantsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['hotels'] = $this->Procida_model->getHotelsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
         $data['sb_shops'] = $this->Procida_model->getShopsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_bars_cafe'] = $this->Procida_model->getBarsandCafesForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        $data['sb_beach_clubs'] = $this->Procida_model->getBeachClubsForSideBar($this->lang->lang(),$temp['lat'],$temp['lng']);
        //var_dump($data['resturants']);exit();
        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
    	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/activity_page', $data);
        $this->load->view('frontend/includes/footer', $data);
    }
}
