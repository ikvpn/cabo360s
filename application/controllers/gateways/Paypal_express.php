<?php

/*
 * Author: Innam Hunzai
 * Email: innamhunzai@gmail.com
 * Project: Visit Procida
 * Version: 1.0
 * File:
 * Description:
 */

if (!defined('BASEPATH')) {
    exit('No direct script access is allowed');
}

class Paypal_express extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        echo "No direct script access is allowed";
    }

    public function completeauthorize() {
        $gateway = Omnipay\Omnipay::create('PayPal_Express');
        $gateway->setTestMode(true);
        $this->load->model('admin/Settings_model');
        $this->Settings_model->load();
        $gateway->setUsername($this->Settings_model->api_username);
        $gateway->setPassword($this->Settings_model->api_password);
        $gateway->setSignature($this->Settings_model->api_key);
        $order_params = $this->session->userdata('order_params');
        $response = $gateway->completePurchase($order_params)->send();
        $data = $response->getData();
        if ($data['ACK'] == "Success") {
            $this->load->model('Orders_model');
            $this->Orders_model->update($this->session->userdata('invoice_id'), array('status' => 'paid'));
            $this->session->set_flashdata(Array('msg' => 'You have successfully checked out with paypal.',
                'type' => 'success'));
            $this->session->set_userdata('order_params', '');
            $this->session->set_userdata('invoice_id', '');
            redirect('customer/register_business');
        } else {
            echo "Unable to process payment please try again";
        }
    }

    public function authorize() {
        $gateway = Omnipay\Omnipay::create('PayPal_Express');
        $gateway->setTestMode(true);
        $this->load->model('admin/Settings_model');
        $this->Settings_model->load();
        $this->load->model('Orders_model');
        $this->Orders_model->update($this->session->userdata('invoice_id'), array('status' => 'unpaid'));
        $this->session->set_flashdata(Array('msg' => 'You have successfully cancelled the order',
            'type' => 'success'));
        $this->session->set_userdata('order_params', '');
        $this->session->set_userdata('invoice_id', '');
        redirect('customer/register_business');
    }

    public function pay_invoice($invoice_id) {
        $this->load->model('customer/Customer_model');
        $this->Customer_model->load($this->session->userdata('id'));
        $this->load->model('admin/Settings_model');
        $this->Settings_model->load();
        $this->load->model('Orders_model');
        $this->Orders_model->load($invoice_id);
        if (!$this->Orders_model->id) {
            show_404();
            return;
        }
        $gateway = Omnipay\Omnipay::create('PayPal_Express');
        $gateway->setUsername($this->Settings_model->api_username);
        $gateway->setPassword($this->Settings_model->api_password);
        $gateway->setSignature($this->Settings_model->api_key);
        $gateway->setTestMode(true);

        $order_params = array(
            'brandname' => 'VisitProcida',
            'returnUrl' => base_url() . 'en/gateways/paypal_express/completeauthorize',
            'cancelUrl' => base_url() . 'en/gateways/paypal_express/authorize',
            'reqconfirmshipping' => '0',
            'noshipping' => '1',
            'allownote' => '0',
            'email' => $this->Customer_model->email,
            'amount' => $this->Orders_model->agreement_amount . '.00',
            'currency' => 'EUR'
        );
        $this->session->set_userdata('order_params', $order_params);
        $this->session->set_userdata('invoice_id', $invoice_id);
        try {
            $request = $gateway->authorize($order_params);
            $response = $gateway->purchase($order_params)->send();
        } catch (Exception $e) {
            echo "Paypal Error! cannot process with current settings.";
        }

        // Process response
        // var_dump($response);
        if ($response->isSuccessful()) {
            $this->Orders_model->update($this->Orders_model->id, array('status' => 'paid'));
        } elseif ($response->isRedirect()) {
            $response->redirect();
        } else {
            echo $response->getMessage();
        }
    }

}
