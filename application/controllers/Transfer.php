<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Transfer extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->data['menu_items'] = $this->Menu_model->getMenuItems($this->lang->lang());
        $this->load->model('admin/Location_model');
        $this->data['locations'] = $this->Location_model->getMenuList($this->lang->lang());
        $this->load->model('admin/Beach_model');
        $this->data['m_beaches'] = $this->Beach_model->getMenuList($this->lang->lang());
        $this->load->model("Places_of_interest_model");
        $this->data['m_poi'] = $this->Places_of_interest_model->getMenuList();
        $this->load->helper('language');
        $this->load->helper('url');
        $this->lang->load('Navigation');
        $this->load->model('admin/Ads_model');
        $this->load->model('Orders_model');
        $this->lang->load('Eventspage');
        $this->load->model('Cart_model');
        $this->load->model('Shope_model');
        $this->load->model('Boatrentals_model');
        $this->load->model('Beachclub_model');
        $this->load->model('Weddings_model');
        $this->load->model('Transfer_model');

        $this->load->model('Apartment_model');
        $this->load->model('Hotel_model');

        $this->load->model('Resturant_model');
        $this->load->model('Barsandcafe_model');
        $this->load->model('Nightlife_model');
        $this->load->model('Event_model');
        $this->load->model('admin/Block_model');

        $this->data['counts']['shopping'] = $this->Shope_model->totalActiveShops();
        $this->data['counts']['boat-rentals'] = $this->Boatrentals_model->totalActiveBoatRentals();
        $this->data['counts']['watersports'] = 0;
        $this->data['counts']['beach-clubs'] = $this->Beachclub_model->totalActiveShops();
        $this->data['counts']['wedding'] = $this->Weddings_model->totalActiveWeddings();

        $this->data['counts']['holiday-houses'] = $this->Apartment_model->totalActiveApartments();
        $this->data['counts']['hotels'] = $this->Hotel_model->totalActiveHotels();

        $this->data['counts']['restaurants'] = $this->Resturant_model->totalActiveResturants();
        $this->data['counts']['bars-and-cafe'] = $this->Barsandcafe_model->totalActiveBarsAndCafe();
        $this->data['counts']['nightlife'] = $this->Nightlife_model->totalActiveResturants();

        $this->load->model('admin/Blog_model');
        $this->data['articles'] = $this->Blog_model->getPosts(3,$this->lang->lang());
    }

    public function index() {
        $this->transfer_listing();
    }

    public function transfer_listing() {
        // $data = $this->data;
        $this->lang->load('Tourspage');

        $data['transfer'] = $this->Transfer_model->getActiveTransfer();
       

        $data['transfers_type'] = json_decode (TRANSFERS_TYPE,true);
        switch ($this->lang->lang()) {
            case 'en':
                $data['meta_keywords'] = 'cabo, airport, transfer, concierge';
                $data['meta_desc'] = 'Private Transportation Services to your hotel, from San Jose del Cabo or Cabo San Lucas';
                $data['meta_title'] = 'Cabo Airport Transfer: book a private transfer from the Airports to Cabo San Lucas';
                break;
            case 'it':
                $data['meta_keywords'] = 'procida, trasferimento, escursioni, visite guidate';
                $data['meta_desc'] = 'Tutti i tour a Procida! trasferimento, Escursioni, Visite Guidate';
                $data['meta_title'] = 'Trasferimento a Procida: trasferimento, escursioni, visite guidate';
                break;
        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = []; // show random tours
        }
    	$data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

      $data['cssfiles'] = array(
          array('type' => 'file', 'src' => base_url() . 'assets/css/label_price.css')
      );
      $data['jsfiles'] = array(
            array('type' => 'inline', 'src' => " var MAP_ID='map-canvas'; var CUR_LAT= 0; var CUR_LNG=0;"),
            array('type' => 'file', 'src' => base_url() . "assets/js/google-map-include.js"),
            array('type' => 'file', 'src' => base_url() . 'assets/js/sort_transfer.js')
      );

      $data['blocks'] = $this->Block_model->getBlocks(4,$this->lang->lang());

        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/transfers', $data);
        $this->load->view('frontend/includes/footer', $data);
    }

    public function transfer($slug) {
      $this->load->model('Transfer_model');
      $this->lang->load('Tourspage');
      $this->lang->load('Transfer_booking');
      $this->load->model('admin/Location_model');
      $this->load->model('Hotel_model');
      $this->load->model('customer/Customer_model');
      $this->load->model('Categories');
      $data['transfer'] = $this->Transfer_model->gettransfer($slug);
      $data['zone'] = $this->db->select('transfer.id,zone.transfer_id,zone.zone_name,zone.sjd_price,zone.csl_price')->from('transfer')->where('transfer.slug',$slug)->join('zone','transfer.id=zone.transfer_id','LEFT OUTER')->get()->result();
      // echo "<pre>";
      // print_r($data['transfer']);
      // exit;
      $data['cars_type'] = json_decode (CARS_TYPE,true);
      $data['boats_type'] = json_decode (BOATS_TYPE,true);
      $data['transfers_type'] = json_decode (TRANSFERS_TYPE,true);
      $data['pick_up_points'] = json_decode (PICK_UP_POINTS,true);

      // $hotels = $this->Hotel_model->getActiveHotels();
      //   $apartments = $this->Hotel_model->getActiveApartmentsforHotalList();
      //   $bed_and_breakfast = $this->Hotel_model->getActivBedAndBreakfastforHotalList();
      //   foreach ($apartments as $apartmentsKey => $apartmentsValue) {
      //       $hotels[] = $apartmentsValue;
      //   }
      //    foreach ($bed_and_breakfast as $bed_and_breakfastKey => $bed_and_breakfastValue) {
      //       $hotels[] = $bed_and_breakfastValue;
      //   }
      //   // print_r($hotels); exit;

      //    $data['hotels'] = $hotels;

       if ($data['transfer'] == null) {
          show_404();
          return;
      }
       $data['facebook_sdk'] = '<div id="fb-root"></div>
          <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=956805381038176";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, \'script\', \'facebook-jssdk\'));</script>';

        $transfer = (array)$data['transfer'];

        switch ($this->lang->lang()) {
            case 'en':
                $data['meta_keywords'] = '';
                $data['meta_desc'] = '';
                $data['meta_title'] = $data['transfer']->enTitle;
                break;
            case 'it':
                $data['meta_keywords'] = '';
                $data['meta_desc'] = '';
                $data['meta_title'] =$data['transfer']->itTitle;
                break;

        }

        $this->load->helper('cookie');

        $this->load->model('Procida_model');
        $cartCookie = $this->input->cookie('prcrt');

        if(!empty($cartCookie)){
          $items = $this->Cart_model->getUserCart($cartCookie, '');
          $hotelCart = $this->Cart_model->getUserCart($cartCookie, 'hotel');
          $tranferCart = $this->Cart_model->getUserCart($cartCookie, 'transfer');
          $tours = $this->Cart_model->getUserCart($cartCookie, 'tour');
          if(!empty($tours)){
            for ($i=0; $i < count($tours); $i++) {
              $tours[$i]->info = $this->Procida_model->getTour($tours[$i]->serviceId);
            }
          }
        } else {
          $items = [];
          $hotelCart = [];
          $tranferCart = [];
          $tours = [];
        }

        $data['cart'] = array( 'items' => count($items), 'hotels' => $hotelCart, 'transfer' => $tranferCart, 'tours' => $tours );

        $data['cssfiles'] = array(
            array('type' => 'file', 'src' => base_url() . 'assets/css/date-time-picker.css')
        );
        $data['jsfiles'] = array(
          array('type' => 'file', 'src' => base_url() . "assets/js/bootstrap-timepicker.js"),
          array('type' => 'file', 'src' => base_url() . "assets/js/bootstrap-datepicker.js"),

          array('type' => 'file', 'src' => base_url() . "assets/js/transfer.js"),

        //   array('type'=>'inlince','src'=>'
        //       $(".map-info-box .line h2").click(function() {
        //           $(".map-info-box .line p").slideUp();
        //           $(this).next("p").slideToggle();
        //       });
        //       var directionsDisplay;
        //       var directionsService = new google.maps.DirectionsService();
        //       var map;

              

        //       function calcRoute(destination) {
        //           var start = \'Corricella, Procida, Naples, Italy\';
        //           var end = destination;
        //           var request = {
        //               origin: start,
        //               destination: end,
        //               travelMode: google.maps.TravelMode.DRIVING
        //           };
        //           directionsService.route(request, function(response, status) {
        //               if (status == google.maps.DirectionsStatus.OK) {
        //                   directionsDisplay.setDirections(response);
        //               }
        //           });
        //       }
        //       google.maps.event.addDomListener(window, \'load\', initialize);
        //       google.maps.event.addDomListener(window, \'resize\', initialize);')
        );

        $data['blocks'] = $this->Block_model->getBlocks(4,$this->lang->lang());
        
        $this->load->view('frontend/includes/header', $data);
        $this->load->view('frontend/content/transfer_delail', $data);
        $this->load->view('frontend/includes/footer', $data);
    }


}
